<?php include 'cabecera.php'; ?>
<div class="pageHead">
	<h1><?php echo $aboutText; ?></h1>
</div>
<?php
	$i = 0;
	$query = $con->Consulta( 'select * from bannersomos where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
	while( $R = $con->Resultados( $query ) ) {
		$imgClass = ( $i % 2 ) ? 'left' : 'right';
		$txtClass = ( $i % 2 ) ? 'right' : 'left';
		$contentImg = ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) ? '<div class="playVideo" onclick="muestraVideo( \'' . $R[ 'video' ] . '\' )"><img src="thumb?src=images/bannersomos/' . $R[ 'image' ] . '&size=802x452"></div>' : '<img src="thumb?src=images/bannersomos/' . $R[ 'image' ] . '&size=802x452">';
		echo
		'<div class="seccion margen">
			<div class="contentImage ' . $imgClass . '">' . $contentImg . '</div>
			<div class="contentText">
				<div class="principal">
					<div class="texto ' . $txtClass . '">
						<h1 class="titulo">' . $R[ 'title' ] . '</h1>
						<h3>' . $R[ 'subtitle' ] . '</h3>
					</div>
				</div>
			</div>
		</div><br>';
		$i++;
	}
?>
<?php include 'pie.php'; ?>

