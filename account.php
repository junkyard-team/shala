<?php
	include 'cabecera.php';
	include 'timezones.php';
?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.tiny.cloud/1/whgpndn77kl8tuyubvkymjm30wammrg0ogwwdpelrucdglnj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
	$( document ).ready( function() {
		if ( checaSesion( 'coachId' ) || checaSesion( 'studentId' ) ) {
			traeSchedule();
			if ( checaSesion( 'coachId' ) ) {
				traeCoach();
				cargaVideos();
				cargaAudios();
				listaCircles();
				llamadasCoach();
				cargaArticulos();
				borraSesion( 'nombreSerie' );
				borraSesion( 'nombreArticleSerie' );
				borraSesion( 'videoBorra' );
				borraSesion( 'audioBorra' );
				borraSesion( 'articuloBorra' );
				borraSesion( 'nombreCircle' );
				borraSesion( 'cargandoUsuarios' );
				$( '#requestCircleUser' ).hide();
				$( '#requestCircleCoaches' ).hide();
				$( '#categoryIdSerieArticle, #categoryIdSerie, #categoryIdCircle, #categoryIdAudio, #categoryCall' ).select2( {
					placeholder: "Pick tags",
					allowClear: true
				} );
				$( '#circlesAsigned, #circlesAsignedCall, #circlesAsignedArticle, #circlesAsignedAudio' ).select2( {
					placeholder: "Choose circles who can access to this content",
					allowClear: true
				} );
				$( '#userCall' ).select2( {
					placeholder: "Choose members who can access to the call",
					allowClear: true
				} );
				$( '#coachCallAproved' ).select2( {
					placeholder: "Choose coaches who can access to the call",
					allowClear: true
				} );
				$( '#studentAproved, #studentArticleAproved, #studentCircleAproved, #studentAprovedAudio' ).select2( {
					placeholder: "Choose members who can see this content",
					allowClear: true
				} );
				$( '#coachAproved, #coachArticleAproved, #coachCircleAproved, #coachAprovedAudio' ).select2( {
					placeholder: "Choose coaches who can see this content",
					allowClear: true
				} );
				$( '#divUserCall' ).hide()
				$( '#datepicker' ).datepicker( {
					dateFormat: 'yy-mm-dd',
					minDate: 0,
					onSelect: function( dateText ) {
						$( '#fecha' ).val( dateText );
					}
				} );
				$( '#contenidoArticle' ).sortable( {
					update: function( event, ui ) {
						actualizaPosicionesArticle();
					}
				} ).disableSelection();
				$( '#contenidoVideo' ).sortable( {
					update: function( event, ui ) {
						actualizaPosiciones();
					}
				} ).disableSelection();
				$( '#contenidoAudio' ).sortable( {
					update: function( event, ui ) {
						actualizaPosiciones();
					}
				} ).disableSelection();
				$( '.horaElige' ).click( function( e ) {
					$( '#time' ).val( $( this ).attr( 'tiempo' ) );
					$( '.horaElige' ).removeClass( 'marcada' );
					$( this ).addClass( 'marcada' );
				} );
				$( '#divUserCallSelect' ).show();
				$( '#divCoachCallAproved' ).show();
				$( '#coachCallAprovedAll' ).click( function( e ) {
					validaMiembros();
				} );
				$( '#userCallAll' ).click( function( e ) {
					validaMiembros();
				} );
				/*tinymce.init( {
					selector: '#descriptionArticle',
					menubar: false,
					plugins: [
						'advlist autolink lists link image charmap print preview anchor',
						'searchreplace visualblocks code fullscreen',
						'insertdatetime media table paste imagetools wordcount'
					],
					toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
					images_upload_url: 'admin/querys/subeimagentiny'
  				} );*/
				tabContentAbierto = 'circleData';
				checaPendientes();
			}
			if ( checaSesion( 'studentId' ) ) {
				traeStudent();
				traeRespuestas();
				traeLlamadas();
				$( '#coachCallPick' ).select2( {
					placeholder: "Pick Coach",
					allowClear: true
				} );
				$( '#datepickerCall' ).datepicker( {
					dateFormat: 'yy-mm-dd',
					minDate: 0,
					onSelect: function( dateText ) {
						$( '#fechaCall' ).val( dateText );
					}
				} );
				$( '.horaElige' ).click( function( e ) {
					$( '#timeCall' ).val( $( this ).attr( 'tiempo' ) );
					$( '.horaElige' ).removeClass( 'marcada' );
					$( this ).addClass( 'marcada' );
				} );
				tabContentAbierto = 'scheduleUserData';
			}
		} else {
			vaciaSesion();
			manda( '/' );
		}
	} );
</script>
<div class="pageHead">
	<h1>GUIDE STUDIO</h1>
	<h2 id="nombrePerfil"></h2>
</div>
<div class="popup" id="serieArticleInfo">
	<a class="cierra" onclick="popup( 'serieArticleInfo', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="serieArticleForm">
		<h3>Series Info</h3>
		<input type="hidden" id="serieArticleId" class="id" value="0">
		<label for="categoryIdSerieArticle">Categories</label>
		<select id="categoryIdSerieArticle" campo="categoryId" multiple>
		<?php
			$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
			}
		?>
		</select>
		<label for="serieArticleNombre">Series Name</label>
		<input type="text" id="serieArticleNombre" class="obligatorio" required>
		<label for="descriptionSerieArticle">Series Description</label>
		<textarea id="descriptionSerieArticle" class="obligatorio" required></textarea>
		<label for="foto">Photo</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="photoArticle">
			</div>
			<div class="veinte tCenter" id="imgShowArticleSerie"></div>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Submit</button>
		</div>
	</form>
</div>
<div class="popup" id="circleInfo">
	<a class="cierra" onclick="popup( 'circleInfo', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="circleForm">
		<h3>Circle Info</h3>
		<input type="hidden" id="circleId" class="id" value="0">
		<label for="circleParentId">Parent Id</label>
		<select id="circleParentId" campo="parentId">
			<option value="0">No parent circle</option>
		<?php
			$query = $con->Consulta( 'select * from circles where clientId=' . $clientId . ' order by nombre' );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'circleId' ] . '" coachId="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . '</option>';
			}
		?>
		</select>
		<label for="categoryIdCircle">Categories</label>
		<select id="categoryIdCircle" campo="categoryId" multiple>
		<?php
			$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
			}
		?>
		</select>
		<label for="circleNombre">Circle Name</label>
		<input type="text" id="circleNombre" class="obligatorio" required>
		<label for="descriptionCircle">Circle Description</label>
		<textarea id="descriptionCircle" class="obligatorio" required></textarea>
		<label for="studentCircleAproved">Members</label>
		<select id="studentCircleAproved" campo="userId" multiple>
		<?php
			$query = $con->Consulta( 'select distinct u.* from userclient us inner join users u on(us.userId=u.userId) where us.clientId=' . $clientId );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'userId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
			}
		?>
		</select>
		<label for="coachCircleAproved">Coaches</label>
		<select id="coachCircleAproved" campo="coachId" multiple>
		<?php
			$query = $con->Consulta( 'select distinct u.* from coachesclients us inner join coaches u on(us.coachId=u.coachId) where u.statusId=2 and us.clientId=' . $clientId );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
			}
		?>
		</select>
		<label for="foto">Photo</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="photoCircle">
			</div>
			<div class="veinte tCenter" id="imgShowCircle"></div>
		</div>
		<label for="videoCircle">Video Intro</label>
		<a class="currentVideo">View uploaded Video</a>
		<input type="file" id="videoCircle" campo="video">
		<br><br>
		<div class="checkTodos">
			<input type="checkbox" id="publicCircle">
			<label for="publicCircle">Make visible to non members</label>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Submit</button>
		</div>
	</form>
</div>
<div class="popup" id="articleAlta">
	<a class="cierra" onclick="popup( 'articleAlta', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="articleForm">
		<input type="hidden" id="articleId" class="id" campo="articleId" value="0">
		<label for="titleArticle">Title</label>
		<input type="text" id="titleArticle" campo="titulo" class="obligatorio" required>
		<label for="descriptionArticle">Description</label>
		<textarea id="descriptionArticle" campo="description" class=""></textarea>
		<label for="accessArticleSerie">Series Access</label>
		<select id="accessArticleSerie" required onchange="checaAprobados( this.value )">
			<option value="">Pick an access's type</option>
			<?php
				$query = $con->Consulta( 'select * from access' );
				while( $R = $con->Resultados( $query ) ) {
					if ( $R[ 'accessId' ] != 4 ) {
						echo '<option value="' . $R[ 'accessId' ] . '">' . $R[ 'name' ] . '</option>';
					}
				}
			?>
		</select>
		<div class="eligeEstudiante">
			<br>
			<label for="studentArticleAproved">Members</label>
			<select id="studentArticleAproved" campo="userId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from userclient us inner join users u on(us.userId=u.userId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'userId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="coachArticleAproved">Coaches</label>
			<select id="coachArticleAproved" campo="coachId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from coachesclients us inner join coaches u on(us.coachId=u.coachId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="circlesAsignedArticle">Cicles</label>
			<select id="circlesAsignedArticle" campo="circleId" multiple>
			<?php
				$query = $con->Consulta( 'select * from circles where clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'circleId' ] . '">' . $R[ 'nombre' ] . '</option>';
				}
			?>
			</select>
		</div>
		<div class="eligeCircle">
		</div>
		<label for="pdfArticle">PDF</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="pdfArticle">
			</div>
			<div class="veinte tCenter" id="pdfShowArticle"></div>
		</div>
		<label for="fotoArticle">Picture</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="fotoArticle">
			</div>
			<div class="veinte tCenter" id="imgShowArticle"></div>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Save</button>
		</div>
	</form>
</div>
<div class="popup" id="videoAlta">
	<a class="cierra" onclick="popup( 'videoAlta', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="videoForm">
		<input type="hidden" id="videoId" class="id" campo="videoId" value="0">
		<label for="title">Title</label>
		<input type="text" id="titulo" campo="titulo" class="obligatorio" required>
		<label for="categoryIdSerie">Categories</label>
		<select id="categoryIdSerie" campo="categoryId" multiple>
		<?php
			$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
			}
		?>
		</select>
		<label for="description">Description</label>
		<textarea id="description" campo="description" class=""></textarea>
		<label for="accessSerie">Video Access</label>
		<select id="accessSerie" required onchange="checaAprobados( this.value )">
			<option value="">Pick an access's type</option>
			<?php
				$query = $con->Consulta( 'select * from access' );
				while( $R = $con->Resultados( $query ) ) {
					if ( $R[ 'accessId' ] != 4 ) {
						echo '<option value="' . $R[ 'accessId' ] . '">' . $R[ 'name' ] . '</option>';
					}
				}
			?>
		</select>
		<div class="eligeEstudiante">
			<br>
			<label for="studentAproved">Members</label>
			<select id="studentAproved" campo="userId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from userclient us inner join users u on(us.userId=u.userId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'userId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="coachAproved">Coaches</label>
			<select id="coachAproved" campo="coachId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from coachesclients us inner join coaches u on(us.coachId=u.coachId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="circlesAsigned">Cicles</label>
			<select id="circlesAsigned" campo="circleId" multiple>
			<?php
				$query = $con->Consulta( 'select * from circles where clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'circleId' ] . '">' . $R[ 'nombre' ] . '</option>';
				}
			?>
			</select>
		</div>
		<div class="eligeCircle">
		</div>
		<label for="fotoVideo">Picture</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="fotoVideo">
			</div>
			<div class="veinte tCenter" id="imgShowVideo"></div>
		</div>
		<label for="videoPop">Video</label>
		<a class="currentVideo">View uploaded Video</a>
		<input type="file" id="videoPop" campo="video" required>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Save</button>
		</div>
	</form>
</div>
<div class="popup" id="audioAlta">
	<a class="cierra" onclick="popup( 'audioAlta', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="audioForm">
		<input type="hidden" id="audioId" class="id" campo="audioId" value="0">
		<label for="tituloAudio">Title</label>
		<input type="text" id="tituloAudio" campo="titulo" class="obligatorio" required>
		<label for="categoryIdAudio">Categories</label>
		<select id="categoryIdAudio" campo="categoryId" multiple>
		<?php
			$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
			while( $R = $con->Resultados( $query ) ) {
				echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
			}
		?>
		</select>
		<label for="descriptionAudio">Description</label>
		<textarea id="descriptionAudio" campo="description" class=""></textarea>
		<label for="accessAudio">Audio Access</label>
		<select id="accessAudio" required onchange="checaAprobados( this.value )">
			<option value="">Pick an access's type</option>
			<?php
				$query = $con->Consulta( 'select * from access' );
				while( $R = $con->Resultados( $query ) ) {
					if ( $R[ 'accessId' ] != 4 ) {
						echo '<option value="' . $R[ 'accessId' ] . '">' . $R[ 'name' ] . '</option>';
					}
				}
			?>
		</select>
		<div class="eligeEstudiante">
			<br>
			<label for="studentAprovedAudio">Members</label>
			<select id="studentAprovedAudio" campo="userId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from userclient us inner join users u on(us.userId=u.userId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'userId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="coachAprovedAudio">Coaches</label>
			<select id="coachAprovedAudio" campo="coachId" multiple>
			<?php
				$query = $con->Consulta( 'select distinct u.* from coachesclients us inner join coaches u on(us.coachId=u.coachId) where u.statusId=2 and us.clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
				}
			?>
			</select>
			<label for="circlesAsignedAudio">Cicles</label>
			<select id="circlesAsignedAudio" campo="circleId" multiple>
			<?php
				$query = $con->Consulta( 'select * from circles where clientId=' . $clientId );
				while( $R = $con->Resultados( $query ) ) {
					echo '<option value="' . $R[ 'circleId' ] . '">' . $R[ 'nombre' ] . '</option>';
				}
			?>
			</select>
		</div>
		<div class="eligeCircle">
		</div>
		<label for="fotoAudio">Picture</label>
		<div class="fila">
			<div class="ochenta">
				<input type="file" id="fotoAudio">
			</div>
			<div class="veinte tCenter" id="imgShowAudio"></div>
		</div>
		<label for="audioFile">Audio</label>
		<input type="file" id="audioFile" campo="audio">
		<div class="full tCenter" id="audioPlay"></div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Save</button>
		</div>
	</form>
</div>
<div class="popup" id="coachCall">
	<a class="cierra" onclick="popup( 'coachCall', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="coachCallForm">
		<h3>New Live Session</h3>
		<div class="fila">
			<div class="treintaTres cPad">
				<input type="hidden" id="callId" class="obligatorio" value="0">
				<label for="categoryCall">Categories</label>
				<select id="categoryCall" campo="categoryId" multiple>
				<?php
					$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
					while( $R = $con->Resultados( $query ) ) {
						echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
					}
				?>
				</select>
				<label for="titleCall">Session Title</label>
				<input type="text" id="titleCall" class="obligatorio" required>
				<label for="descriptionCall">Session Description</label>
				<textarea id="descriptionCall" class="obligatorio" required></textarea>
				<label for="durationCall">Session Duration</label>
				<select id="durationCall">
					<option value="30">30 min</option>
					<option value="60">1 hr</option>
					<option value="90">1.5 hrs</option>
					<option value="120">2 hrs</option>
					<option value="150">2.5 hrs</option>
					<option value="180">3 hrs</option>
					<option value="210">3.5 hrs</option>
					<option value="240">4 hrs</option>
					<option value="270">4.5 hrs</option>
					<option value="300">5 hrs</option>
					<option value="330">5.5 hrs</option>
					<option value="360">6 hrs</option>
				</select>
				<label for="tipoId">Session Access</label>
				<select id="tipoId" onchange="checaLlamada( this.value )">
					<option value="1">Public</option>
					<option value="2">Private</option>
					<option value="3">Registered Users</option>
					<!-- <option value="4">Circles</option> -->
				</select>
				<div id="divUserCall">
					<label for="userCall">Members</label>
					<div id="divUserCallSelect">
						<select id="userCall" multiple>
						<?php
							$res = $con->Consulta( 'select DISTINCT u.userId, u.apellido, u.nombre from users u inner join userclient uc on(u.userId=uc.userId) where uc.clientId=' . $clientId );
							while( $R = $con->Resultados( $res ) ) {
								echo '<option value="' . $R[ 'userId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
							}
						?>
						</select>
					</div>
					<div class="checkTodos">
						<input type="checkbox" id="userCallAll">
						<label for="userCallAll">All members</label>
					</div>
					<label for="coachCallAproved">Coaches</label>
					<div id="divCoachCallAproved">
						<select id="coachCallAproved" multiple>
						<?php
							$query = $con->Consulta( 'select distinct u.* from coachesclients us inner join coaches u on(us.coachId=u.coachId) where u.statusId=2 and us.clientId=' . $clientId );
							while( $R = $con->Resultados( $query ) ) {
								echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
							}
						?>
						</select>
					</div>
					<div class="checkTodos">
						<input type="checkbox" id="coachCallAprovedAll">
						<label for="coachCallAprovedAll">All coaches</label>
					</div>
					<label for="circlesAsignedCall">Cicles</label>
					<select id="circlesAsignedCall" campo="circleId" multiple>
					<?php
						$query = $con->Consulta( 'select * from circles where clientId=' . $clientId );
						while( $R = $con->Resultados( $query ) ) {
							echo '<option value="' . $R[ 'circleId' ] . '">' . $R[ 'nombre' ] . '</option>';
						}
					?>
					</select>
				</div>
				<div class="divUserCircle">
				</div>
				<label for="fotoVideo">Image</label>
				<div class="fila">
					<div class="ochenta">
						<input type="file" id="fotoCall">
					</div>
					<div class="veinte tCenter" id="imgShowCall"></div>
				</div>
			</div>
			<div class="treintaTres">
				<label for="fecha">Date</label>
				<div id="datepicker"></div>
				<input type="hidden" id="fecha">
				<label for="timezone">Timezone</label>
				<select id="timezone">
				<?php
					foreach ( $timeZoneList as $timeZone ) {
						$timeZoneText = str_replace( '_', ' ', $timeZone );
						echo '<option value="' . $timeZone . '">' . $timeZoneText . '</option>';
					}
				?>
				</select>
			</div>
			<div class="treintaTres cPad">
				<label for="horarioScroll">Start Time</label>
				<div class="horarioScroll">
					<div class="horaElige" tiempo="06:00:00">6:00 am</div>
					<div class="horaElige" tiempo="06:30:00">6:30 am</div>
					<div class="horaElige" tiempo="07:00:00">7:00 am</div>
					<div class="horaElige" tiempo="07:30:00">7:30 am</div>
					<div class="horaElige" tiempo="08:00:00">8:00 am</div>
					<div class="horaElige" tiempo="08:30:00">8:30 am</div>
					<div class="horaElige" tiempo="09:00:00">9:00 am</div>
					<div class="horaElige" tiempo="09:30:00">9:30 am</div>
					<div class="horaElige" tiempo="10:00:00">10:00 am</div>
					<div class="horaElige" tiempo="10:30:00">10:30 am</div>
					<div class="horaElige" tiempo="11:00:00">11:00 am</div>
					<div class="horaElige" tiempo="11:30:00">11:30 am</div>
					<div class="horaElige" tiempo="12:00:00">12:00 pm</div>
					<div class="horaElige" tiempo="12:30:00">12:30 pm</div>
					<div class="horaElige" tiempo="13:00:00">1:00 pm</div>
					<div class="horaElige" tiempo="13:30:00">1:30 pm</div>
					<div class="horaElige" tiempo="14:00:00">2:00 pm</div>
					<div class="horaElige" tiempo="14:30:00">2:30 pm</div>
					<div class="horaElige" tiempo="15:00:00">3:00 pm</div>
					<div class="horaElige" tiempo="15:30:00">3:30 pm</div>
					<div class="horaElige" tiempo="16:00:00">4:00 pm</div>
					<div class="horaElige" tiempo="16:30:00">4:30 pm</div>
					<div class="horaElige" tiempo="17:00:00">5:00 pm</div>
					<div class="horaElige" tiempo="17:30:00">5:30 pm</div>
					<div class="horaElige" tiempo="18:00:00">6:00 pm</div>
					<div class="horaElige" tiempo="18:30:00">6:30 pm</div>
					<div class="horaElige" tiempo="19:00:00">7:00 pm</div>
					<div class="horaElige" tiempo="19:30:00">7:30 pm</div>
					<div class="horaElige" tiempo="20:00:00">8:00 pm</div>
					<div class="horaElige" tiempo="20:30:00">8:30 pm</div>
					<div class="horaElige" tiempo="21:00:00">9:00 pm</div>
					<div class="horaElige" tiempo="21:30:00">9:30 pm</div>
					<div class="horaElige" tiempo="22:00:00">10:00 pm</div>
					<div class="horaElige" tiempo="22:30:00">10:30 pm</div>
					<div class="horaElige" tiempo="23:00:00">11:00 pm</div>
					<div class="horaElige" tiempo="23:30:00">11:30 pm</div>
				</div>
				<input type="hidden" id="time">
				<div class="invitaGente">
					<input type="checkbox" id="inviteCall">
					<label for="inviteCall">Click here if you wish to send a meeting invite to participants</label>
				</div>
				<div class="sessionInfo"></div>
			</div>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Submit</button>
		</div>
	</form>
</div>
<div class="popup" id="studentCall">
	<a class="cierra" onclick="popup( 'studentCall', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="studentCallForm">
		<div class="fila">
			<div class="treintaTres">
				<h3>Request a Live Session</h3>
				<input type="hidden" id="callIdUser" class="obligatorio" value="0">
				<label for="titleUser">Session Description</label>
				<input type="text" id="titleUser" class="obligatorio" required>
				<label for="descriptionCallUser">Session Description</label>
				<textarea id="descriptionCallUser" class="obligatorio" required></textarea>
				<label for="coachCallPick"><?php echo $coachText; ?></label>
				<select id="coachCallPick" required>
				<?php
					$res = $con->Consulta( 'select DISTINCT u.coachId, u.apellido, u.nombre from coaches u inner join coachesclients uc on(u.coachId=uc.coachId) where u.statusId=2 and uc.clientId=' . $clientId );
					while( $R = $con->Resultados( $res ) ) {
						echo '<option value="' . $R[ 'coachId' ] . '">' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</option>';
					}
				?>
				</select>
			</div>
			<div class="treintaTres">
				<label for="fecha">Date</label>
				<div id="datepickerCall"></div>
				<input type="hidden" id="fechaCall">
				<label for="timezoneStudent">Timezone</label>
				<select id="timezoneStudent">
				<?php
					foreach ( $timeZoneList as $timeZone ) {
						$timeZoneText = str_replace( '_', ' ', $timeZone );
						echo '<option value="' . $timeZone . '">' . $timeZoneText . '</option>';
					}
				?>
				</select>
			</div>
			<div class="treintaTres cPad">
				<label for="horarioScroll">Start Time</label>
				<div class="horarioScroll">
					<div class="horaElige" tiempo="06:00:00">6:00 am</div>
					<div class="horaElige" tiempo="06:30:00">6:30 am</div>
					<div class="horaElige" tiempo="07:00:00">7:00 am</div>
					<div class="horaElige" tiempo="07:30:00">7:30 am</div>
					<div class="horaElige" tiempo="08:00:00">8:00 am</div>
					<div class="horaElige" tiempo="08:30:00">8:30 am</div>
					<div class="horaElige" tiempo="09:00:00">9:00 am</div>
					<div class="horaElige" tiempo="09:30:00">9:30 am</div>
					<div class="horaElige" tiempo="10:00:00">10:00 am</div>
					<div class="horaElige" tiempo="10:30:00">10:30 am</div>
					<div class="horaElige" tiempo="11:00:00">11:00 am</div>
					<div class="horaElige" tiempo="11:30:00">11:30 am</div>
					<div class="horaElige" tiempo="12:00:00">12:00 pm</div>
					<div class="horaElige" tiempo="12:30:00">12:30 pm</div>
					<div class="horaElige" tiempo="13:00:00">1:00 pm</div>
					<div class="horaElige" tiempo="13:30:00">1:30 pm</div>
					<div class="horaElige" tiempo="14:00:00">2:00 pm</div>
					<div class="horaElige" tiempo="14:30:00">2:30 pm</div>
					<div class="horaElige" tiempo="15:00:00">3:00 pm</div>
					<div class="horaElige" tiempo="15:30:00">3:30 pm</div>
					<div class="horaElige" tiempo="16:00:00">4:00 pm</div>
					<div class="horaElige" tiempo="16:30:00">4:30 pm</div>
					<div class="horaElige" tiempo="17:00:00">5:00 pm</div>
					<div class="horaElige" tiempo="17:30:00">5:30 pm</div>
					<div class="horaElige" tiempo="18:00:00">6:00 pm</div>
					<div class="horaElige" tiempo="18:30:00">6:30 pm</div>
					<div class="horaElige" tiempo="19:00:00">7:00 pm</div>
					<div class="horaElige" tiempo="19:30:00">7:30 pm</div>
					<div class="horaElige" tiempo="20:00:00">8:00 pm</div>
					<div class="horaElige" tiempo="20:30:00">8:30 pm</div>
					<div class="horaElige" tiempo="21:00:00">9:00 pm</div>
					<div class="horaElige" tiempo="21:30:00">9:30 pm</div>
					<div class="horaElige" tiempo="22:00:00">10:00 pm</div>
					<div class="horaElige" tiempo="22:30:00">10:30 pm</div>
					<div class="horaElige" tiempo="23:00:00">11:00 pm</div>
					<div class="horaElige" tiempo="23:30:00">11:30 pm</div>
				</div>
				<input type="hidden" id="timeCall">
			</div>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Submit</button>
		</div>
	</form>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada coachData">
				<h2 class="abreForm tCenter" form="coachForm"> <span class="rechazo"> Application Rejected</span><span class="pendienteStatus">Your application is pending. We encourage you to fill out your profile. This can speed up the application process. If you have any questions, please message us through the contact link at the bottom of the page</span></h2>
				<div class="rechazo" id="rechazoData">
					<div class="fila rechazoFila">
						<div class="sesenta"><p></p></div>
						<div class="cuarenta">
							<a class="boton inline" onclick="solicitaAprobacion()">Resend approval request</a>
						</div>
					</div>
				</div>
				<div class="tabs">
					<!-- <div abre="scheduleData" class="selected pendientes">My schedule</div> -->
					<div abre="circleData" class="selected pendientes">My circles</div>
					<div abre="videoData" class="pendientes">My videos</div>
					<div abre="audioData" class="pendientes">My audios</div>
					<div abre="callsData" class="pendientes">My sessions</div>
					<div abre="articleData" class="pendientes">My articles</div>
					<div abre="coachForm">My profile</div>
				</div>
				<form method="post" id="coachForm" class="tabContent formShow" tipo="2" enctype="multipart/form-data">
					<input type="hidden" id="coachId" value="0">
					<!-- <label for="categoryId">Category</label>
					<select id="categoryId" class="obligatorio" required>
					<?php
						echo '<option value="">Pick an option</option>';
						$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
						while( $R = $con->Resultados( $query ) ) {
							echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
						}
					?>
					</select> -->
					<label for="nombre">First Name</label>
					<input type="text" id="nombre" class="obligatorio" required>
					<label for="nombre">Last Name</label>
					<input type="text" id="apellido" class="obligatorio" required>
					<label for="phrase">Personal Phrase</label>
					<input type="text" id="phrase" required maxlength="25">
					<label for="biografia">Biography</label>
					<textarea id="biografia" class="obligatorio" required></textarea>
					<label for="email">Email</label>
					<input type="email" id="email" class="email obligatorio" required autocomplete="off">
					<label for="pass">Password</label>
					<input type="password" id="pass" class="obligatorio" required autocomplete="off">
					<label for="foto">Photo</label>
					<div class="fila">
						<div class="ochenta">
							<input type="file" id="foto">
						</div>
						<div class="veinte tCenter" id="imgShowCoach"></div>
					</div>
					<label for="foto">Header Photo</label>
					<div class="fila">
						<div class="ochenta">
							<input type="file" id="header">
						</div>
						<div class="veinte tCenter" id="imgShowCoachHead"></div>
					</div>
					<div class="label"><label for="video">Video |</label> <a class="currentVideo"><i class="fas fa-play"></i> View uploaded Video</a></div>
					<input type="file" id="video">
					<label for="facebook">Facebook</label>
					<input type="text" id="facebook">
					<label for="twitter">Twitter</label>
					<input type="text" id="twitter">
					<label for="instagram">Instagram</label>
					<input type="text" id="instagram">
					<label for="linkedin">Linkedin</label>
					<input type="text" id="linkedin">
					<label for="web">Web Page</label>
					<input type="text" id="web">
					<div class="zoomActivar">
						<br><br>
						<a onclick="iniciaZoom()" class="blue boton" rel="noopener noreferrer">Add Zoom Account</a>
					</div>
					<div class="zoomLogout">
						<a onclick="cierraZoom()" class="blue boton" rel="noopener noreferrer">Remove Zoom Account</a>
					</div>
					<input type="hidden" id="zoomSecret">
					<br><br>
					<div>
						<a type="submit" class="boton inline" onclick="$( '#coachForm' ).submit()">Update</a>
						<a class="boton inline" onclick="borraCuenta()">Delete account</a>
					</div>
				</form>
				<div class="tabContent" id="articleData">
					<!-- <label for="nombreUser">Article series</label>
					<div class="serieAdd fila">
						<div class="sesenta">
							<select id="serieArticleCoach" onchange="cargaArticulos()">
								<option value="">Pick your Series</option>
							</select>
						</div>
						<div class="cuarenta">
							<div class="divBotones">
								<a class="boton addArticleSerie"><i class="fas fa-plus"></i></a>
								<a class="boton editaArticleSerie"><i class="fas fa-edit"></i></a>
								<a class="boton borraArticleSerie"><i class="fas fa-trash"></i></a>
							</div>
						</div>
					</div> -->
					<hr>
					<table id="articuloTable">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>PDF</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody id="contenidoArticle"></tbody>
					</table>
					<br><br>
					<a class="boton" onclick="nuevoArticulo();">Add article</a>
					<div id="requestArticleTable">
						<hr><h5 align="center">Access Requested</h5>
						<table>
							<thead>
								<tr>
									<th>Action</th>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody id="contenidoArticleRequest"></tbody>
						</table>
					</div>
				</div>
				<div class="tabContent" id="circleData">
					<label for="nombreUser">Circles</label>
					<div class="serieAdd fila">
						<div class="sesenta">
							<select id="circleCoach" onchange="cargaCircle()">
								<option value="">Pick your Circle</option>
							</select>
						</div>
						<div class="cuarenta">
							<div class="divBotones">
								<a class="boton addCircle"><i class="fas fa-plus"></i></a>
								<a class="boton editaCircle" edicion="1"><i class="fas fa-edit"></i></a>
								<a class="boton borraCircle"><i class="fas fa-trash"></i></a>
							</div>
						</div>
					</div>
					<hr>
					<h3 class="tCenter" id="coachNameCircle"></h3>
					<table id="usersCircle">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>Name</th>
								
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					<br><br>
					<a class="boton editaCircle" edicion="2">Add <?php echo $miembroText; ?></a>
					<table id="coachesCircle">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					<br><br>
					<a class="boton editaCircle" edita="2">Add <?php echo $coachText; ?></a>
					<hr><br>
					<div id="requestCircleUser">
						<hr><h5 align="center"><?php echo $miembroText; ?> Join Requested</h5>
						<table>
							<thead>
								<tr>
									<th></th>
									<th>Thumb</th>
									<th>Name</th>
									<th>Approve</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<div id="requestCircleCoaches">
						<hr><h5 align="center"><?php echo $coachText; ?> Join Requested</h5>
						<table>
							<thead>
								<tr>
									<th></th>
									<th>Thumb</th>
									<th>Name</th>
									<th>Approve</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<hr>
				</div>
				<div class="tabContent" id="videoData">
					<table id="videoTable">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>Video</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody id="contenidoVideo"></tbody>
					</table>
					<br><br>
					<a class="boton" onclick="nuevoVideo();">Add video</a>
					<div id="requestTable">
						<hr><h5 align="center">Access Requested</h5>
						<table>
							<thead>
								<tr>
									<th>Action</th>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody id="contenidoRequest"></tbody>
						</table>
					</div>
				</div>
				<div class="tabContent" id="audioData">
					<table id="audioTable">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>Audio</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody id="contenidoAudio"></tbody>
					</table>
					<br><br>
					<a class="boton" onclick="nuevoAudio();">Add audio</a>
					<div id="requestAudioTable">
						<hr><h5 align="center">Access Requested</h5>
						<table>
							<thead>
								<tr>
									<th>Action</th>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody id="contenidoRequestAudio"></tbody>
						</table>
					</div>
				</div>
				<div class="tabContent" id="scheduleData">
					<br><br>
					<table id="scheduleTable">
						<thead>
							<tr>
								<th></th>
								<th>Date</th>
								<th>Time</th>
								<th>Duration</th>
								<th>Session Title</th>
								<th>Coach</th>
								<th>Thumb</th>
							</tr>
						</thead>
						<tbody id="contenidoSchedule"></tbody>
					</table>
				</div>
				<div class="tabContent" id="callsData">
					<br><br>
					<a class="boton" onclick="iniciaLlamada();"><i class="fas fa-calendar-plus"></i> Schedule a Session</a>
					<table id="callsTable">
						<thead>
							<tr>
								<th></th>
								<th>Thumb</th>
								<th>Session Title</th>
								<th>Description</th>
								<th><?php echo $miembroText; ?> Name</th>
								<th>Status</th>
								<th>Type</th>
								<th>Date</th>
								<th>Time</th>
								<th>Duration</th>
								<th>Invitation</th>
							</tr>
						</thead>
						<tbody id="contenidoCall"></tbody>
					</table>
				</div>
			</div>
			<div class="cincuenta cuentaCentrada studentData">
				<h2 class="abreForm" form="studentForm"> <span class="rechazo"> Application Rejected</span><span class="pendienteStatus">Your sign-up request is pending admin approval. We will contact you if we need more information during the process. If you have any questions, please message us through the contact link at the bottom of the page.</span></h2>
				<div class="rechazo" id="rechazoDataMiembro">
					<div class="fila rechazoFila">
						<div class="sesenta"><p></p></div>
						<div class="cuarenta">
							<a class="boton inline" onclick="solicitaAprobacion()">Resend approval request</a>
						</div>
					</div>
				</div>
				<div class="tabs">
					<div abre="scheduleUserData" class="selected pendientes">My Schedule</div>
					<?php if ( $T[ 'memberQuestionnaire' ] == 'S' ) :?>
					<div abre="misRespuestas" class="pendientes">Questionnaire</div>
					<?php endif; ?>
					<div abre="personalCalls" class="pendientes">My Sessions</div>
					<div abre="studentForm">My Profile</div>
				</div>
				<div class="tabContent" id="scheduleUserData">
					<br><br>
					<table id="scheduleUserTable">
						<thead>
							<tr>
								<th></th>
								<th>Date</th>
								<th>Time</th>
								<th>Duration</th>
								<th>Session Title</th>
								<th>Coach</th>
								<th>Thumb</th>
							</tr>
						</thead>
						<tbody id="contentSchedule"></tbody>
					</table>
				</div>
				<form method="post" id="studentForm" tipo="2" class="tabContent formShow" enctype="multipart/form-data">
					<input type="hidden" id="userId" campo="userId" value="0">
					<label for="nombreUser">First Name</label>
					<input type="text" id="nombreUser" campo="nombre" class="obligatorio" required>
					<label for="nombreUser">Last Name</label>
					<input type="text" id="apellidoUser" campo="apellido" class="obligatorio" required>
					<label for="emailUser">Email</label>
					<input type="email" id="emailUser" campo="email" class="email obligatorio" required>
					<label for="passUser">Password</label>
					<input type="password" id="passUser" campo="pass" class="obligatorio" required>
					<label for="fotoUser">Photo</label>
					<div class="fila">
						<div class="ochenta">
							<input type="file" id="fotoUser">
						</div>
						<div class="veinte tCenter" id="imgShowUser"></div>
					</div>
					<br><br>
					<div>
						<a type="submit" class="boton inline" onclick="$( '#studentForm' ).submit()">Update</a>
						<a class="boton inline" onclick="borraCuenta()">Delete account</a>
					</div>
				</form>
				<div class="tabContent" id="misRespuestas">
					<form method="post" id="preguntasForm">
						<?php
							$i = 1;
							$query = $con->Consulta( 'select * from questionnaire where clientId=' . $clientId . ' order by position asc' );
							while( $R = $con->Resultados( $query ) ) {
								echo
								'<div class="pregunta fila">
									<div class="setenta cPad">
										<label for="pregunta-' . $R[ 'questionId' ] . '">' . $i . ' - ' . $R[ 'question' ] . '</label>
									</div>
									<div class="treinta cPad">
										<select id="pregunta-' . $R[ 'questionId' ] . '" preguntaId="' . $R[ 'questionId' ] . '" required>
											<option value="">Pick an option</option>
											<option value="' . $R[ 'answerOne' ] . '">' . $R[ 'answerOne' ] . '</option>
											<option value="' . $R[ 'answerTwo' ] . '">' . $R[ 'answerTwo' ] . '</option>
											<option value="' . $R[ 'answerThree' ] . '">' . $R[ 'answerThree' ] . '</option>
											<option value="' . $R[ 'answerFour' ] . '">' . $R[ 'answerFour' ] . '</option>
											<option value="' . $R[ 'answerFive' ] . '">' . $R[ 'answerFive' ] . '</option>
										</select>
									</div>
								</div>';
								$i++;
							}
						?>
						<button type="submit" class="boton">Update</button>
					</form>
				</div>
				<div class="tabContent" id="personalCalls">
					<br><br>
					<a class="boton" onclick="popup( 'studentCall', true )"><i class="fas fa-calendar-plus"></i> Request a Session</a>
					<table id="callTable">
						<thead>
							<tr>
								<th></th>
								<th>Session Title</th>
								<th>Status</th>
								<th>Date</th>
								<th>Time</th>
							</tr>
						</thead>
						<tbody id="contentCall"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>
