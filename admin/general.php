<?php
	include( 'template_header.php' );
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
	$folder = $tabla;
	$tituloSeccion = palabraDiccionario( $tabla, $con );
	$funcionGuarda = ( $tabla == 'series' || $tabla == 'coaches' || $tabla == 'videos' || $tabla == 'bannerhome' || $tabla == 'bannersomos' || $tabla == 'posts' ) ? 'uploadVideo()' : 'guardaFormulario()';
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="../panel/assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/core.css">
		<link rel="stylesheet" href="../panel/plugins/css/dropzone.css">
		<script src="https://amrak.cdn.spotlightr.com/assets/vooplayer.js"></script>
	</head>
	<body class="<?php echo $classBody; ?>">
		<div class="iconoPicker">
			<i class="fa fa-close cierra" onclick="popIconPicker( 2 )"></i>
			<div id="iconosMuestra"></div>
		</div>
		<div id="main">
			<?php include( 'template_sidebar.php' ); ?>
			<section id="content_wrapper">
				<section class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title"><?php echo ucfirst( $tituloSeccion ); ?></span></div>
						<div class="panel-body pn">
							<div class="row admin-form">
								<div role="form" style="float: left">
									<button class="button btn-primary" onclick="iniciaFormulario()">Add</button>
								</div>
								<?php if ( $tabla == 'coaches' || $tabla == 'users' ) : ?>
								<div class="col-lg-4" style="float: right">
									<label class="field select">
										<select id="mostrarConfirmados" onchange="listado()">
											<option value="S">Confirmados</option>
											<option value="N">No Confirmados</option>
										</select>
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-lg-2" style="float: right; margin-top: -10px;">
									<label class="field tcenter" style="padding-top: 8px;">
										<label for="emailHabilitado">Emails active?</label>
										<div class="col-lg-12">
											<label class="switch block">
												<input type="checkbox" id="emailHabilitado" checked="">
												<label for="emailHabilitado" data-on="YES" data-off="NO"></label>
											</label>
										</div>
									</label>
								</div>
								<?php endif; ?>
							</div>
							<div class="tablaScroll">
								<div class="pull-right" id="paginacionTabla"></div>
								<table data-page-navigation=".pagination" data-page-size="20" class="table footable">
									<thead>
										<tr><?php armaTabla( $tabla, $con ); ?></tr>
									</thead>
									<tbody id="contenidoTabla"></tbody>
									<tfoot class="footer-menu">
										<tr>
											<td colspan="8">
												<nav class="text-right">
													<ul class="pagination hide-if-no-paging"></ul>
												</nav>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<div id="modal-data" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i><?php echo ucfirst( $tituloSeccion ); ?> Data</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="row">
							<?php armaFormulario( $tabla, $clientId, $con ); ?>
						</div>
						<?php armaAcordeones( $tabla, $con ); ?>
					</div>
					<div class="panel-footer">
						<button class="button btn-primary" onclick="<?php echo $funcionGuarda; ?>">Save</button>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php if ( $tabla == 'users' ) : ?>
		<div id="modalQuestion" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-picture-o"></i>Answers</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="row" id="respuestaDiv"></div>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php endif; ?>
		<?php if ( $tabla == 'videos' ) :?>
		<div id="modal-filtros" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Filters</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="form-group mb10 row">
							<div class="form-group mb10 row">
								<label for="inputSelect" class="col-lg-3 control-label">Filter</label>
								<div class="col-lg-8">
									<input type="hidden" id="videoFiltro" class="id" value="0">
									<label class="field select setenta">
										<select name="propiedades" id="propiedades" class="obligatorio" onchange="listaPropiedadValor( this.value )"></select><i class="arrow"></i>
									</label>
									<a class="addi" onclick="editaPropiedad( $( '#propiedades' ).val() )"><i class="fa fa-edit"></i></a>
									<a class="addi" onclick="eliminaPropiedad( $( '#propiedades' ).val() )"><i class="fa fa-minus"></i></a>
									<a class="addi" onclick="agregaPropiedad()"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="row">
							<a class="boton" onclick="guardaInfo()"><i class="fa fa-save"></i> Save Changes</a>
							<a class="boton" onclick="addElement()"><i class="fa fa-plus"></i> Add Element</a>
						</div>
						<div class="form-group mb10 row" id="listaPropiedad">
						</div>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php endif; ?>
		<?php if ( $tabla == 'coaches' || $tabla == 'users' ) :?>
		<div id="modal-coaches" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Rejection Reason</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="form-group mb10 row">
							<div class="form-group mb10 row">
								<input type="hidden" id="coachIdReason">
								<textarea id="coachReason" class="gui-input" placeholder="Enter the reject reason"></textarea>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="button btn-primary" onclick="rechazaTexto()">Send</button>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php
			include('template_footer_scripts.php');
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkwYo-1e42rL_fS43hLbGYBUYBhxqrS-A"></script>
		<script src="../panel/assets/js/demo/widgets.js"></script>
		<script src="../panel/assets/js/signature_pad.min.js"></script>
		<script src="../panel/plugins/doublescroll.js"></script>
		<script src="../panel/assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					borraSesion( 'unico' );
					if ( checaSesion( 'mandaEmail' ) ) {
						if ( obtenSesion( 'mandaEmail' ) == 'S' ) {
							$( '#emailHabilitado' ).prop( 'checked', true );
						} else {
							$( '#emailHabilitado' ).prop( 'checked', false );
						}
					}
					<?php if ( $tabla == 'coaches' || $tabla == 'users' ) : ?>
					$( '#emailHabilitado' ).change( function() {
						var mailRevisado = ( $( '#emailHabilitado' ).prop( 'checked' ) ) ? 'S' : 'N';
						seteaSesion( 'mandaEmail', mailRevisado );
					} );
					<?php endif; ?>
					$( 'a[data-event="fontSize"]' ).click( function( e ) {
						e.preventDefault();
						e.stopPropagation();
						return false;
					} );
					if ( $( '#clientId' ).length ) {
						$( '#clientId' ).val( clientId );
						$( '#contiene-clientId' ).hide();
					}
					<?php
						if ( $folder != '' ) {
							echo 'seteaSesion( \'folderActivo\', \'' . $folder . '\' );';
						}
						echo 'seteaSesion( \'tabla\', \'' . $tabla . '\' );';
					?>
					/*<?php if ( $tabla == 'videos' ) {
						echo 'seteaSesion( \'videoKey\', \'' . obtenVideoKey( $con ) . '\' );';
					} ?>*/
					seteaSesion( 'contenedor', 'modal-data' );
					$( '.fecha' ).datetimepicker( { format: "YYYY-MM-DD" } );
					$( '.summernote' ).summernote( {
						height: 300,
						focus: false,
						lang: "en-EN",
						toolbar: [
							['view', ['codeview']],
							[ 'font', [ 'bold', 'italic', 'underline', 'clear' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
							[ 'height', [ 'height' ] ],
							[ 'insert', [ 'link', 'picture', 'hr' ] ]
						],
						popover: {
							image: [
								[ 'custom', [ 'imageAttributes' ] ],
								[ 'imagesize', [ 'imageSize100', 'imageSize50', 'imageSize25' ] ],
								[ 'float', [ 'floatLeft', 'floatRight', 'floatNone' ] ],
								[ 'remove', [ 'removeMedia' ] ]
							],
						},
						lang: 'es-ES',
						imageAttributes:{
							icon:'<i class="fa fa-edit"/>',
							removeEmpty:false,
							disableUpload: false
						},
						onImageUpload: function( image ) {
							console.log( 'subiendo imagen' );
							subeSummerImg( image[ 0 ], $( this ).attr( 'id' ) );
						},
						fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
					} );
					$( '.select2-multiple' ).select2( {
						placeholder: "Choose options",
						allowClear: true
					} );
					$( '.opcion' ).select2();
					$( '.color' ).spectrum( {
						color: bgInfo,
						appendTo: $( '.color' ).parents('.sfcolor').parent(),
						containerClassName: 'sp-left'
					} );
					$( '.color' ).show();
					if ( $( '.googleMaps' ).length ) {
						var mapaId = $( '.googleMaps' ).attr( 'id' );
						console.log( mapaId );
						geocoder = new google.maps.Geocoder();
						var latlng = new google.maps.LatLng( -34.397, 150.644 );
						var mapOptions = {
							zoom: 14,
							center: latlng
						}
						map = new google.maps.Map( document.getElementById( mapaId ), mapOptions );
						$( '.buscaMapa' ).change( function() {
							if ( $( this ).val() != '' ) {
								buscaDireccion( $( this ).val() );
							}
						} );
						$( '.buscaMapa' ).keyup( function() {
							clearTimeout( typingTimer );
							if ( $( this ).val() != '' ) {
								typingTimer = setTimeout( buscaDireccion( $( this ).val() ), doneTypingIntervalMaps );
							}
						} );
					}
					if ( $( 'canvas' ).length ) {
						signaturePad = new SignaturePad(
							document.querySelector( 'canvas' ),
							{
								maxWidth: 2,
								backgroundColor: 'rgb(255,255,255)'
							}
						);
					}
					localStorage.removeItem( 'cambiaConfirmacion' );
					if ( $( '#emailConfirmed' ).length ) {
						$( '#emailConfirmed' ).change( function() {
							seteaSesion( 'cambiaConfirmacion', true );
						} );
					}
					checaUsuario();
					listado();
					<?php if ( $tabla == 'productos' || $tabla == 'videos' ) : ?>
					myDropzone = new Dropzone( "#dropZone" );
					myDropzone.options.dropZone = {
						autoProcessQueue: true,
						uploadMultiple: true,
						parallelUploads: 100,
						maxFiles: 0,
						addRemoveLinks: false,
						dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> <span class="main-text"><b>Drop your files</b> to upload</span>',
						dictResponseError: 'Uploading problems',
					};
					myDropzone.on( 'success', function( response ) {
						listaGaleria( localStorage.getItem( 'elementoId' ) );
					} );
					<?php endif; ?>
				} );
			} );
		</script>
	</body>
</html>
<?php $con->CierraConexion(); ?>