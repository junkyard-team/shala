<?php include( 'template_header.php' ); ?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="../panel/assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/core.css">
		<link rel="stylesheet" href="../panel/plugins/css/dropzone.css">
		<style>.shortLi { line-height: 60px !important; }</style>
	</head>
	<body class="<?php echo $classBody; ?>">
		<div id="main">
			<?php include( 'template_sidebar.php' ); ?>
			<section id="content_wrapper">
				<section class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-4">
									<span class="panel-title">General Content</span>
								</div>
								<div class="col-lg-8">
									<ul class="nav nav-pills shortLi pull-right">
										<li class="active"><a href="#menuTab" data-toggle="tab" aria-expanded="true">Menu</a></li>
										<li><a href="#headerTab" data-toggle="tab" aria-expanded="false">Header</a></li>
										<li><a href="#homeTab" data-toggle="tab" aria-expanded="false">Globals</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel-body pn admin-form" id="modal-data">
							<div class="tab-content br-n pn">
								<input type="hidden" id="generalId" class="id" value="">
								<input type="hidden" id="clientId" value="<?php echo $clientId; ?>">
								<div class="tab-pane active" id="menuTab">
									<div class="row">
										<div class="col-lg-6">
											<div class="section row">
												<label for="homeMenuText" class="col-lg-12 control-label">Home Menu Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="homeMenuText" placeholder="Enter a Home Menu Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="exploreMenuText" class="col-lg-12 control-label">Explore Menu Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="exploreMenuText" placeholder="Enter a Explore Menu Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="coachesMenuText" class="col-lg-12 control-label">Guides Menu Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="coachesMenuText" placeholder="Enter a Guide Menu Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="aboutMenuText" class="col-lg-12 control-label">About Menu Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="aboutMenuText" placeholder="Enter a About Menu Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="coachText" class="col-lg-12 control-label">Guide Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="coachText" placeholder="Enter a Guide Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="memberText" class="col-lg-12 control-label">Member Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="memberText" placeholder="Enter a Member Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="liveMenuText" class="col-lg-12 control-label">Live Menu Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="liveMenuText" placeholder="Enter a Live Menu Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="headerTab">
									<div class="row">
										<div class="col-lg-6">
											<div class="section row">
												<label for="headerTitle" class="col-lg-12 control-label">Title *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="headerTitle" placeholder="Enter a Header Title" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="section row">
												<label for="subtitleHeader" class="col-lg-12 control-label">Subtitle *</label>
												<div class="col-lg-12">
													<label class="field">
														<textarea id="subtitleHeader" placeholder="Enter a Subtitle Header" rows="4" class="form-control obligatorio" maxlength="500"></textarea>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="inputStandard" class="col-lg-12 control-label">Show Button? *</label>
												<div class="col-lg-12">
													<label class="field tcenter" style="padding-top: 8px;">
														<label class="switch block">
															<input type="checkbox" id="headerButtonShow" checked="" tipo="SN" value="">
															<label for="headerButtonShow" data-on="YES" data-off="NO"></label>
														</label>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="headerButtonText" class="col-lg-12 control-label">Button Text</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="headerButtonText" placeholder="Enter a Header Button Text" class="gui-input " maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="headerButtonUrl" class="col-lg-12 control-label">Button Url</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="headerButtonUrl" placeholder="Enter a Header Button Url" class="gui-input " maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="headerButtonTextColor" class="col-lg-12 control-label">Button Text Color *</label>
												<div class="col-lg-12">
													<label class="field sfcolor">
														<input id="headerButtonTextColor" type="text" placeholder="Elija un color" class="gui-input color obligatorio" onchange="seteaToHex( $( this ) )" style="display: inline-block;"><div class="sp-replacer sp-light"><div class="sp-preview"><div class="sp-preview-inner" style="background-color: rgb(235, 167, 95);"></div></div><div class="sp-dd">▼</div></div>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6" id="contiene-headerButtonFontSize">
											<div class="section row">
												<label for="headerButtonFontSize" class="col-lg-12 control-label">Button Font Size</label>
												<div class="col-lg-12">
													<label class="field select">
														<select id="headerButtonFontSize">
															<option value="">Pick an option</option>
															<option value="10px">10px</option><option value="12px">12px</option><option value="14px">14px</option><option value="16px">16px</option><option value="18px">18px</option><option value="20px">20px</option><option value="22px">22px</option><option value="24px">24px</option><option value="26px">26px</option><option value="28px">28px</option><option value="30px">30px</option><option value="32px">32px</option><option value="34px">34px</option><option value="36px">36px</option><option value="38px">38px</option><option value="40px">40px</option><option value="42px">42px</option><option value="44px">44px</option><option value="46px">46px</option><option value="48px">48px</option><option value="50px">50px</option><option value="52px">52px</option><option value="54px">54px</option><option value="56px">56px</option><option value="58px">58px</option><option value="60px">60px</option>
														</select>
														<i class="arrow"></i>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="headerButtonBackgroundColor" class="col-lg-12 control-label">Button Background Color *</label>
												<div class="col-lg-12">
													<label class="field sfcolor">
														<input id="headerButtonBackgroundColor" type="text" placeholder="Elija un color" class="gui-input color obligatorio" onchange="seteaToHex( $( this ) )" style="display: inline-block;"><div class="sp-replacer sp-light"><div class="sp-preview"><div class="sp-preview-inner" style="background-color: rgb(235, 167, 95);"></div></div><div class="sp-dd">▼</div></div>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="inputStandard" class="col-lg-12 control-label">Button Rounded *</label>
												<div class="col-lg-12">
													<label class="field tcenter" style="padding-top: 8px;">
														<label class="switch block">
															<input type="checkbox" id="headerButtonRounded" checked="" tipo="SN" value="">
															<label for="headerButtonRounded" data-on="YES" data-off="NO"></label>
														</label>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb10 row">
												<label for="videoHeader" class="col-lg-2 control-label">Video</label>
												<div class="col-lg-8">
													<label class="field prepend-icon append-button file" for="imagen"><span class="button btn-primary">Video Header</span>
														<form contiene="videoHeader">
															<input type="file" id="videoHeader" onchange="$( '#uploader-videoHeader' ).val( this.value );" class="gui-file  archivo" cargado="vacio">
															<input id="uploader-videoHeader" placeholder="Choose a file" class="gui-input noPelar" type="text" style="text-align: right;">
															<label class="field-icon"><i class="fa fa-upload"></i></label>
														</form>
													</label>
												</div>
												<div class="col-lg-2 capaimg showImg" columna="videoHeader" obligado=""><iframe allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/web_text_new.mp4?fallback=true" data-playerid="web_text_new.mp4" style="max-width:100%" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe><a class="quitaImagenShow" idel="1" style="position: absolute; font-size: 20px; margin-left: -15px; padding: 0 5px; border-radius: 50%; background-color: #ccc;"><i class="fa fa-close"></i></a></div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb10 row">
												<label for="videoMobileHeader" class="col-lg-2 control-label">Video Mobile</label>
												<div class="col-lg-8">
													<label class="field prepend-icon append-button file" for="imagen"><span class="button btn-primary">Video Mobile Header</span>
														<form contiene="videoMobileHeader">
															<input type="file" id="videoMobileHeader" onchange="$( '#uploader-videoMobileHeader' ).val( this.value );" class="gui-file  archivo" cargado="vacio">
															<input id="uploader-videoMobileHeader" placeholder="Choose a file" class="gui-input noPelar" type="text" style="text-align: right;">
															<label class="field-icon"><i class="fa fa-upload"></i></label>
														</form>
													</label>
												</div>
												<div class="col-lg-2 capaimg showImg" columna="videoMobileHeader" obligado=""><iframe allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/mobile_text_new.mp4?fallback=true" data-playerid="mobile_text_new.mp4" style="max-width:100%" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe><a class="quitaImagenShow" idel="1" style="position: absolute; font-size: 20px; margin-left: -15px; padding: 0 5px; border-radius: 50%; background-color: #ccc;"><i class="fa fa-close"></i></a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="homeTab">
									<div class="row">
										<div class="col-lg-6">
											<div class="section row">
												<label for="inputStandard" class="col-lg-12 control-label">Hide Guide Content *</label>
												<div class="col-lg-12">
													<label class="field tcenter" style="padding-top: 8px;">
														<label class="switch block">
															<input type="checkbox" id="hideCoachContent" checked="" tipo="SN" value="">
															<label for="hideCoachContent" data-on="YES" data-off="NO"></label>
														</label>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="liveSessionText" class="col-lg-12 control-label">Live Session Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="liveSessionText" placeholder="Enter a Live Session Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="processingText" class="col-lg-12 control-label">Processing Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="processingText" placeholder="Enter a Processing Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="signUpText" class="col-lg-12 control-label">Sign Up Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="signUpText" placeholder="Enter a Sign Up Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="titleCategories" class="col-lg-12 control-label">Title Categories *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="titleCategories" placeholder="Enter a Title Categories" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="section row">
												<label for="subtitleCategories" class="col-lg-12 control-label">Subtitle Categories *</label>
												<div class="col-lg-12">
													<label class="field">
														<textarea id="subtitleCategories" placeholder="Enter a Subtitle Categories" rows="4" class="form-control obligatorio" maxlength="500"></textarea>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="titleCoaches" class="col-lg-12 control-label">Title Guides *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="titleCoaches" placeholder="Enter a Title Guides" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="section row">
												<label for="subtitleCoaches" class="col-lg-12 control-label">Subtitle Guides *</label>
												<div class="col-lg-12">
													<label class="field">
														<textarea id="subtitleCoaches" placeholder="Enter a Subtitle Guides" rows="4" class="form-control obligatorio" maxlength="500"></textarea>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="titleClasses" class="col-lg-12 control-label">Title Classes *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="titleClasses" placeholder="Enter a Title Classes" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="section row">
												<label for="subtitleClasses" class="col-lg-12 control-label">Subtitle Classes *</label>
												<div class="col-lg-12">
													<label class="field">
														<textarea id="subtitleClasses" placeholder="Enter a Subtitle Classes" rows="4" class="form-control obligatorio" maxlength="500"></textarea>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="trialDaysText" class="col-lg-12 control-label">Trial Days Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="trialDaysText" placeholder="Enter a Trial Days Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="section row">
												<label for="trialDescriptionText" class="col-lg-12 control-label">Trial Description Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<textarea id="trialDescriptionText" placeholder="Enter a Trial Description Text" rows="4" class="form-control obligatorio" maxlength="500"></textarea>
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="section row">
												<label for="blogText" class="col-lg-12 control-label">Blog Text *</label>
												<div class="col-lg-12">
													<label class="field">
														<input type="text" id="blogText" placeholder="Enter a Blog Text" class="gui-input obligatorio" maxlength="200">
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<button class="button btn-primary" onclick="guardaFormulario()">Save</button>
							<br><br><br>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<?php
			include('template_footer_scripts.php');
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkwYo-1e42rL_fS43hLbGYBUYBhxqrS-A"></script>
		<script src="../panel/assets/js/demo/widgets.js"></script>
		<script src="../panel/assets/js/signature_pad.min.js"></script>
		<script src="../panel/plugins/doublescroll.js"></script>
		<script src="../panel/assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					borraSesion( 'unico' );
					$( 'a[data-event="fontSize"]' ).click( function( e ) {
						e.preventDefault();
						e.stopPropagation();
						return false;
					} );
					if ( $( '#clientId' ).length ) {
						$( '#clientId' ).val( clientId );
						$( '#contiene-clientId' ).hide();
					}
					localStorage.setItem( 'folderActivo', 'generals' );
					localStorage.setItem( 'tabla', 'generals' );
					localStorage.setItem( 'contenedor', 'modal-data' );
					$( '.fecha' ).datetimepicker( { format: "YYYY-MM-DD" } );
					$( '.summernote' ).summernote( {
						height: 300,
						focus: false,
						toolbar: [
							['view', ['codeview']],
							[ 'font', [ 'bold', 'italic', 'underline', 'clear' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
							[ 'height', [ 'height' ] ],
							[ 'insert', [ 'link', 'picture', 'hr' ] ]
						],
						popover: {
							image: [
								[ 'custom', [ 'imageAttributes' ] ],
								[ 'imagesize', [ 'imageSize100', 'imageSize50', 'imageSize25' ] ],
								[ 'float', [ 'floatLeft', 'floatRight', 'floatNone' ] ],
								[ 'remove', [ 'removeMedia' ] ]
							],
						},
						lang: 'es-ES',
						imageAttributes:{
							icon:'<i class="fa fa-edit"/>',
							removeEmpty:false,
							disableUpload: false
						},
						onImageUpload: function( image ) {
							console.log( 'subiendo imagen' );
							subeSummerImg( image[ 0 ], $( this ).attr( 'id' ) );
						},
						fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
					} );
					$( '.select2-multiple' ).select2( {
						placeholder: "Choose options",
						allowClear: true
					} );
					$( '.opcion' ).select2();
					$( '.color' ).spectrum( {
						color: bgInfo,
						appendTo: $( '.color' ).parents('.sfcolor').parent(),
						containerClassName: 'sp-left'
					} );
					$( '.color' ).show();
					checaUsuario();
					listadoUnico();
				} );
			} );
		</script>
	</body>
</html>
<?php $con->CierraConexion(); ?>