<?php include('template_header.php');?>
		<link rel="stylesheet" type="text/css" href="../panel/assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="../panel/assets/skin/default_skin/css/theme.css?v=3">
	</head>
	<body class="external-page sb-l-c sb-r-c">
		<div id="main" class="animated fadeIn">
			<section id="content_wrapper">
				<div id="canvas-wrapper">
					<canvas id="demo-canvas"></canvas>
				</div>
				<section id="content">
					<div id="login1" class="admin-form theme-primary">
						<div class="row mb15 table-layout">
							<div class="col-xs-12 va-m pln tcenter"><a href="/admin/" title="Return to Dashboard"><img src="../images/clients/<?php echo $T[ 'logotipo' ]; ?>" title="AdminDesigns Logo" class="img-responsive w250"></a></div>
						</div>
						<div class="panel panel-info mt10 br-n bg-gradient-1 mw500 mauto">
							<form id="contact" method="post" action="/">
								<div class="panel-body text-center p50 text-center">
									<div class="section">
										<p class="text-uppercase text-white ls100 fw700">User</p>
										<label for="username" class="field prepend-icon">
											<input type="text" name="usuario" id="usuario" placeholder="Enter Email" class="gui-input" onkeypress="enter( event, 'login' )">
										</label>
									</div>
									<div class="section">
										<p class="text-uppercase text-white ls100 fw700">Password</p>
										<label for="password" class="field prepend-icon">
											<input type="password" id="pass" name="pass" placeholder="Enter password" class="gui-input" onkeypress="enter( event, 'login' )">
										</label>
									</div>
									<div class="panel-footer clearfix p10 ph15">
										<a class="button btn-primary mr10 pull-right text-uppercase text-white ls100 fw700" onclick="login()">Login</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</section>
		</div>
		<?php include('template_footer_scripts.php');?>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				CanvasBG.init({
					Loc: {
						x: window.innerWidth / 2,
						y: window.innerHeight / 3.3
					},
				} );
			} );
		</script>
	</body>
</html>