var totalCompra = 0;
var footableProductos = '';
var tipoStatus = 1;
var pasoTramite = 1;
var infoCatastral = [];
var giroComplementario = [];
var tramitesElegidos = [];
var pendientes = [];
var myDropzone;
var geocoder;
var map;
var markers = [];
var doneTypingIntervalMaps = 2000;
var typingTimer;
var agregados = 0;
var cuantosInfo = 0;
var progressbar = $( '#progressbar' );
var progressLabel = $( '.progress-label' );
var Stacks = {
	stack_top_right: { "dir1": "down", "dir2": "left", "push": "top", "spacing1": 10, "spacing2": 10 },
	stack_top_left: { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 10, "spacing2": 10 },
	stack_bottom_left: { "dir1": "right", "dir2": "up", "push": "top", "spacing1": 10, "spacing2": 10 },
	stack_bottom_right: { "dir1": "left", "dir2": "up", "push": "top", "spacing1": 10, "spacing2": 10 },
	stack_bar_top: { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 },
	stack_bar_bottom: { "dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0 },
	stack_context: { "dir1": "down", "dir2": "left", "context": $( "#stack-context") },
}
var fontAwesome = [ { icon: 'fa fa-glass' }, { icon: 'fa fa-music' }, { icon: 'fa fa-search' }, { icon: 'fa fa-envelope-o' }, { icon: 'fa fa-heart' }, { icon: 'fa fa-star' }, { icon: 'fa fa-star-o' }, { icon: 'fa fa-user' }, { icon: 'fa fa-film' }, { icon: 'fa fa-th-large' }, { icon: 'fa fa-th' }, { icon: 'fa fa-th-list' }, { icon: 'fa fa-check' }, { icon: 'fa fa-times' }, { icon: 'fa fa-search-plus' }, { icon: 'fa fa-search-minus' }, { icon: 'fa fa-power-off' }, { icon: 'fa fa-signal' }, { icon: 'fa fa-cog' }, { icon: 'fa fa-trash-o' }, { icon: 'fa fa-home' }, { icon: 'fa fa-file-o' }, { icon: 'fa fa-clock-o' }, { icon: 'fa fa-road' }, { icon: 'fa fa-download' }, { icon: 'fa fa-arrow-circle-o-down' }, { icon: 'fa fa-arrow-circle-o-up' }, { icon: 'fa fa-inbox' }, { icon: 'fa fa-play-circle-o' }, { icon: 'fa fa-repeat' }, { icon: 'fa fa-refresh' }, { icon: 'fa fa-list-alt' }, { icon: 'fa fa-lock' }, { icon: 'fa fa-flag' }, { icon: 'fa fa-headphones' }, { icon: 'fa fa-volume-off' }, { icon: 'fa fa-volume-down' }, { icon: 'fa fa-volume-up' }, { icon: 'fa fa-qrcode' }, { icon: 'fa fa-barcode' }, { icon: 'fa fa-tag' }, { icon: 'fa fa-tags' }, { icon: 'fa fa-book' }, { icon: 'fa fa-bookmark' }, { icon: 'fa fa-print' }, { icon: 'fa fa-camera' }, { icon: 'fa fa-font' }, { icon: 'fa fa-bold' }, { icon: 'fa fa-italic' }, { icon: 'fa fa-text-height' }, { icon: 'fa fa-text-width' }, { icon: 'fa fa-align-left' }, { icon: 'fa fa-align-center' }, { icon: 'fa fa-align-right' }, { icon: 'fa fa-align-justify' }, { icon: 'fa fa-list' }, { icon: 'fa fa-outdent' }, { icon: 'fa fa-indent' }, { icon: 'fa fa-video-camera' }, { icon: 'fa fa-picture-o' }, { icon: 'fa fa-pencil' }, { icon: 'fa fa-map-marker' }, { icon: 'fa fa-adjust' }, { icon: 'fa fa-tint' }, { icon: 'fa fa-pencil-square-o' }, { icon: 'fa fa-share-square-o' }, { icon: 'fa fa-check-square-o' }, { icon: 'fa fa-arrows' }, { icon: 'fa fa-step-backward' }, { icon: 'fa fa-fast-backward' }, { icon: 'fa fa-backward' }, { icon: 'fa fa-play' }, { icon: 'fa fa-pause' }, { icon: 'fa fa-stop' }, { icon: 'fa fa-forward' }, { icon: 'fa fa-fast-forward' }, { icon: 'fa fa-step-forward' }, { icon: 'fa fa-eject' }, { icon: 'fa fa-chevron-left' }, { icon: 'fa fa-chevron-right' }, { icon: 'fa fa-plus-circle' }, { icon: 'fa fa-minus-circle' }, { icon: 'fa fa-times-circle' }, { icon: 'fa fa-check-circle' }, { icon: 'fa fa-question-circle' }, { icon: 'fa fa-info-circle' }, { icon: 'fa fa-crosshairs' }, { icon: 'fa fa-times-circle-o' }, { icon: 'fa fa-check-circle-o' }, { icon: 'fa fa-ban' }, { icon: 'fa fa-arrow-left' }, { icon: 'fa fa-arrow-right' }, { icon: 'fa fa-arrow-up' }, { icon: 'fa fa-arrow-down' }, { icon: 'fa fa-share' }, { icon: 'fa fa-expand' }, { icon: 'fa fa-compress' }, { icon: 'fa fa-plus' }, { icon: 'fa fa-minus' }, { icon: 'fa fa-asterisk' }, { icon: 'fa fa-exclamation-circle' }, { icon: 'fa fa-gift' }, { icon: 'fa fa-leaf' }, { icon: 'fa fa-fire' }, { icon: 'fa fa-eye' }, { icon: 'fa fa-eye-slash' }, { icon: 'fa fa-exclamation-triangle' }, { icon: 'fa fa-plane' }, { icon: 'fa fa-calendar' }, { icon: 'fa fa-random' }, { icon: 'fa fa-comment' }, { icon: 'fa fa-magnet' }, { icon: 'fa fa-chevron-up' }, { icon: 'fa fa-chevron-down' }, { icon: 'fa fa-retweet' }, { icon: 'fa fa-shopping-cart' }, { icon: 'fa fa-folder' }, { icon: 'fa fa-folder-open' }, { icon: 'fa fa-arrows-v' }, { icon: 'fa fa-arrows-h' }, { icon: 'fa fa-bar-chart' }, { icon: 'fa fa-twitter-square' }, { icon: 'fa fa-facebook-square' }, { icon: 'fa fa-camera-retro' }, { icon: 'fa fa-key' }, { icon: 'fa fa-cogs' }, { icon: 'fa fa-comments' }, { icon: 'fa fa-thumbs-o-up' }, { icon: 'fa fa-thumbs-o-down' }, { icon: 'fa fa-star-half' }, { icon: 'fa fa-heart-o' }, { icon: 'fa fa-sign-out' }, { icon: 'fa fa-linkedin-square' }, { icon: 'fa fa-thumb-tack' }, { icon: 'fa fa-external-link' }, { icon: 'fa fa-sign-in' }, { icon: 'fa fa-trophy' }, { icon: 'fa fa-github-square' }, { icon: 'fa fa-upload' }, { icon: 'fa fa-lemon-o' }, { icon: 'fa fa-phone' }, { icon: 'fa fa-square-o' }, { icon: 'fa fa-bookmark-o' }, { icon: 'fa fa-phone-square' }, { icon: 'fa fa-twitter' }, { icon: 'fa fa-facebook' }, { icon: 'fa fa-github' }, { icon: 'fa fa-unlock' }, { icon: 'fa fa-credit-card' }, { icon: 'fa fa-rss' }, { icon: 'fa fa-hdd-o' }, { icon: 'fa fa-bullhorn' }, { icon: 'fa fa-bell' }, { icon: 'fa fa-certificate' }, { icon: 'fa fa-hand-o-right' }, { icon: 'fa fa-hand-o-left' }, { icon: 'fa fa-hand-o-up' }, { icon: 'fa fa-hand-o-down' }, { icon: 'fa fa-arrow-circle-left' }, { icon: 'fa fa-arrow-circle-right' }, { icon: 'fa fa-arrow-circle-up' }, { icon: 'fa fa-arrow-circle-down' }, { icon: 'fa fa-globe' }, { icon: 'fa fa-wrench' }, { icon: 'fa fa-tasks' }, { icon: 'fa fa-filter' }, { icon: 'fa fa-briefcase' }, { icon: 'fa fa-arrows-alt' }, { icon: 'fa fa-users' }, { icon: 'fa fa-link' }, { icon: 'fa fa-cloud' }, { icon: 'fa fa-flask' }, { icon: 'fa fa-scissors' }, { icon: 'fa fa-files-o' }, { icon: 'fa fa-paperclip' }, { icon: 'fa fa-floppy-o' }, { icon: 'fa fa-square' }, { icon: 'fa fa-bars' }, { icon: 'fa fa-list-ul' }, { icon: 'fa fa-list-ol' }, { icon: 'fa fa-strikethrough' }, { icon: 'fa fa-underline' }, { icon: 'fa fa-table' }, { icon: 'fa fa-magic' }, { icon: 'fa fa-truck' }, { icon: 'fa fa-pinterest' }, { icon: 'fa fa-pinterest-square' }, { icon: 'fa fa-google-plus-square' }, { icon: 'fa fa-google-plus' }, { icon: 'fa fa-money' }, { icon: 'fa fa-caret-down' }, { icon: 'fa fa-caret-up' }, { icon: 'fa fa-caret-left' }, { icon: 'fa fa-caret-right' }, { icon: 'fa fa-columns' }, { icon: 'fa fa-sort' }, { icon: 'fa fa-sort-desc' }, { icon: 'fa fa-sort-asc' }, { icon: 'fa fa-envelope' }, { icon: 'fa fa-linkedin' }, { icon: 'fa fa-undo' }, { icon: 'fa fa-gavel' }, { icon: 'fa fa-tachometer' }, { icon: 'fa fa-comment-o' }, { icon: 'fa fa-comments-o' }, { icon: 'fa fa-bolt' }, { icon: 'fa fa-sitemap' }, { icon: 'fa fa-umbrella' }, { icon: 'fa fa-clipboard' }, { icon: 'fa fa-lightbulb-o' }, { icon: 'fa fa-exchange' }, { icon: 'fa fa-cloud-download' }, { icon: 'fa fa-cloud-upload' }, { icon: 'fa fa-user-md' }, { icon: 'fa fa-stethoscope' }, { icon: 'fa fa-suitcase' }, { icon: 'fa fa-bell-o' }, { icon: 'fa fa-coffee' }, { icon: 'fa fa-cutlery' }, { icon: 'fa fa-file-text-o' }, { icon: 'fa fa-building-o' }, { icon: 'fa fa-hospital-o' }, { icon: 'fa fa-ambulance' }, { icon: 'fa fa-medkit' }, { icon: 'fa fa-fighter-jet' }, { icon: 'fa fa-beer' }, { icon: 'fa fa-h-square' }, { icon: 'fa fa-plus-square' }, { icon: 'fa fa-angle-double-left' }, { icon: 'fa fa-angle-double-right' }, { icon: 'fa fa-angle-double-up' }, { icon: 'fa fa-angle-double-down' }, { icon: 'fa fa-angle-left' }, { icon: 'fa fa-angle-right' }, { icon: 'fa fa-angle-up' }, { icon: 'fa fa-angle-down' }, { icon: 'fa fa-desktop' }, { icon: 'fa fa-laptop' }, { icon: 'fa fa-tablet' }, { icon: 'fa fa-mobile' }, { icon: 'fa fa-circle-o' }, { icon: 'fa fa-quote-left' }, { icon: 'fa fa-quote-right' }, { icon: 'fa fa-spinner' }, { icon: 'fa fa-circle' }, { icon: 'fa fa-reply' }, { icon: 'fa fa-github-alt' }, { icon: 'fa fa-folder-o' }, { icon: 'fa fa-folder-open-o' }, { icon: 'fa fa-smile-o' }, { icon: 'fa fa-frown-o' }, { icon: 'fa fa-meh-o' }, { icon: 'fa fa-gamepad' }, { icon: 'fa fa-keyboard-o' }, { icon: 'fa fa-flag-o' }, { icon: 'fa fa-flag-checkered' }, { icon: 'fa fa-terminal' }, { icon: 'fa fa-code' }, { icon: 'fa fa-reply-all' }, { icon: 'fa fa-star-half-o' }, { icon: 'fa fa-location-arrow' }, { icon: 'fa fa-crop' }, { icon: 'fa fa-code-fork' }, { icon: 'fa fa-chain-broken' }, { icon: 'fa fa-question' }, { icon: 'fa fa-info' }, { icon: 'fa fa-exclamation' }, { icon: 'fa fa-superscript' }, { icon: 'fa fa-subscript' }, { icon: 'fa fa-eraser' }, { icon: 'fa fa-puzzle-piece' }, { icon: 'fa fa-microphone' }, { icon: 'fa fa-microphone-slash' }, { icon: 'fa fa-shield' }, { icon: 'fa fa-calendar-o' }, { icon: 'fa fa-fire-extinguisher' }, { icon: 'fa fa-rocket' }, { icon: 'fa fa-maxcdn' }, { icon: 'fa fa-chevron-circle-left' }, { icon: 'fa fa-chevron-circle-right' }, { icon: 'fa fa-chevron-circle-up' }, { icon: 'fa fa-chevron-circle-down' }, { icon: 'fa fa-html5' }, { icon: 'fa fa-css3' }, { icon: 'fa fa-anchor' }, { icon: 'fa fa-unlock-alt' }, { icon: 'fa fa-bullseye' }, { icon: 'fa fa-ellipsis-h' }, { icon: 'fa fa-ellipsis-v' }, { icon: 'fa fa-rss-square' }, { icon: 'fa fa-play-circle' }, { icon: 'fa fa-ticket' }, { icon: 'fa fa-minus-square' }, { icon: 'fa fa-minus-square-o' }, { icon: 'fa fa-level-up' }, { icon: 'fa fa-level-down' }, { icon: 'fa fa-check-square' }, { icon: 'fa fa-pencil-square' }, { icon: 'fa fa-external-link-square' }, { icon: 'fa fa-share-square' }, { icon: 'fa fa-compass' }, { icon: 'fa fa-caret-square-o-down' }, { icon: 'fa fa-caret-square-o-up' }, { icon: 'fa fa-caret-square-o-right' }, { icon: 'fa fa-eur' }, { icon: 'fa fa-gbp' }, { icon: 'fa fa-usd' }, { icon: 'fa fa-inr' }, { icon: 'fa fa-jpy' }, { icon: 'fa fa-rub' }, { icon: 'fa fa-krw' }, { icon: 'fa fa-btc' }, { icon: 'fa fa-file' }, { icon: 'fa fa-file-text' }, { icon: 'fa fa-sort-alpha-asc' }, { icon: 'fa fa-sort-alpha-desc' }, { icon: 'fa fa-sort-amount-asc' }, { icon: 'fa fa-sort-amount-desc' }, { icon: 'fa fa-sort-numeric-asc' }, { icon: 'fa fa-sort-numeric-desc' }, { icon: 'fa fa-thumbs-up' }, { icon: 'fa fa-thumbs-down' }, { icon: 'fa fa-youtube-square' }, { icon: 'fa fa-youtube' }, { icon: 'fa fa-xing' }, { icon: 'fa fa-xing-square' }, { icon: 'fa fa-youtube-play' }, { icon: 'fa fa-dropbox' }, { icon: 'fa fa-stack-overflow' }, { icon: 'fa fa-instagram' }, { icon: 'fa fa-flickr' }, { icon: 'fa fa-adn' }, { icon: 'fa fa-bitbucket' }, { icon: 'fa fa-bitbucket-square' }, { icon: 'fa fa-tumblr' }, { icon: 'fa fa-tumblr-square' }, { icon: 'fa fa-long-arrow-down' }, { icon: 'fa fa-long-arrow-up' }, { icon: 'fa fa-long-arrow-left' }, { icon: 'fa fa-long-arrow-right' }, { icon: 'fa fa-apple' }, { icon: 'fa fa-windows' }, { icon: 'fa fa-android' }, { icon: 'fa fa-linux' }, { icon: 'fa fa-dribbble' }, { icon: 'fa fa-skype' }, { icon: 'fa fa-foursquare' }, { icon: 'fa fa-trello' }, { icon: 'fa fa-female' }, { icon: 'fa fa-male' }, { icon: 'fa fa-gratipay' }, { icon: 'fa fa-sun-o' }, { icon: 'fa fa-moon-o' }, { icon: 'fa fa-archive' }, { icon: 'fa fa-bug' }, { icon: 'fa fa-vk' }, { icon: 'fa fa-weibo' }, { icon: 'fa fa-renren' }, { icon: 'fa fa-pagelines' }, { icon: 'fa fa-stack-exchange' }, { icon: 'fa fa-arrow-circle-o-right' }, { icon: 'fa fa-arrow-circle-o-left' }, { icon: 'fa fa-caret-square-o-left' }, { icon: 'fa fa-dot-circle-o' }, { icon: 'fa fa-wheelchair' }, { icon: 'fa fa-vimeo-square' }, { icon: 'fa fa-try' }, { icon: 'fa fa-plus-square-o' }, { icon: 'fa fa-space-shuttle' }, { icon: 'fa fa-slack' }, { icon: 'fa fa-envelope-square' }, { icon: 'fa fa-wordpress' }, { icon: 'fa fa-openid' }, { icon: 'fa fa-university' }, { icon: 'fa fa-graduation-cap' }, { icon: 'fa fa-yahoo' }, { icon: 'fa fa-google' }, { icon: 'fa fa-reddit' }, { icon: 'fa fa-reddit-square' }, { icon: 'fa fa-stumbleupon-circle' }, { icon: 'fa fa-stumbleupon' }, { icon: 'fa fa-delicious' }, { icon: 'fa fa-digg' }, { icon: 'fa fa-pied-piper' }, { icon: 'fa fa-pied-piper-alt' }, { icon: 'fa fa-drupal' }, { icon: 'fa fa-joomla' }, { icon: 'fa fa-language' }, { icon: 'fa fa-fax' }, { icon: 'fa fa-building' }, { icon: 'fa fa-child' }, { icon: 'fa fa-paw' }, { icon: 'fa fa-spoon' }, { icon: 'fa fa-cube' }, { icon: 'fa fa-cubes' }, { icon: 'fa fa-behance' }, { icon: 'fa fa-behance-square' }, { icon: 'fa fa-steam' }, { icon: 'fa fa-steam-square' }, { icon: 'fa fa-recycle' }, { icon: 'fa fa-car' }, { icon: 'fa fa-taxi' }, { icon: 'fa fa-tree' }, { icon: 'fa fa-spotify' }, { icon: 'fa fa-deviantart' }, { icon: 'fa fa-soundcloud' }, { icon: 'fa fa-database' }, { icon: 'fa fa-file-pdf-o' }, { icon: 'fa fa-file-word-o' }, { icon: 'fa fa-file-excel-o' }, { icon: 'fa fa-file-powerpoint-o' }, { icon: 'fa fa-file-image-o' }, { icon: 'fa fa-file-archive-o' }, { icon: 'fa fa-file-audio-o' }, { icon: 'fa fa-file-video-o' }, { icon: 'fa fa-file-code-o' }, { icon: 'fa fa-vine' }, { icon: 'fa fa-codepen' }, { icon: 'fa fa-jsfiddle' }, { icon: 'fa fa-life-ring' }, { icon: 'fa fa-circle-o-notch' }, { icon: 'fa fa-rebel' }, { icon: 'fa fa-empire' }, { icon: 'fa fa-git-square' }, { icon: 'fa fa-git' }, { icon: 'fa fa-hacker-news' }, { icon: 'fa fa-tencent-weibo' }, { icon: 'fa fa-qq' }, { icon: 'fa fa-weixin' }, { icon: 'fa fa-paper-plane' }, { icon: 'fa fa-paper-plane-o' }, { icon: 'fa fa-history' }, { icon: 'fa fa-circle-thin' }, { icon: 'fa fa-header' }, { icon: 'fa fa-paragraph' }, { icon: 'fa fa-sliders' }, { icon: 'fa fa-share-alt' }, { icon: 'fa fa-share-alt-square' }, { icon: 'fa fa-bomb' }, { icon: 'fa fa-futbol-o' }, { icon: 'fa fa-tty' }, { icon: 'fa fa-binoculars' }, { icon: 'fa fa-plug' }, { icon: 'fa fa-slideshare' }, { icon: 'fa fa-twitch' }, { icon: 'fa fa-yelp' }, { icon: 'fa fa-newspaper-o' }, { icon: 'fa fa-wifi' }, { icon: 'fa fa-calculator' }, { icon: 'fa fa-paypal' }, { icon: 'fa fa-google-wallet' }, { icon: 'fa fa-cc-visa' }, { icon: 'fa fa-cc-mastercard' }, { icon: 'fa fa-cc-discover' }, { icon: 'fa fa-cc-amex' }, { icon: 'fa fa-cc-paypal' }, { icon: 'fa fa-cc-stripe' }, { icon: 'fa fa-bell-slash' }, { icon: 'fa fa-bell-slash-o' }, { icon: 'fa fa-trash' }, { icon: 'fa fa-copyright' }, { icon: 'fa fa-at' }, { icon: 'fa fa-eyedropper' }, { icon: 'fa fa-paint-brush' }, { icon: 'fa fa-birthday-cake' }, { icon: 'fa fa-area-chart' }, { icon: 'fa fa-pie-chart' }, { icon: 'fa fa-line-chart' }, { icon: 'fa fa-lastfm' }, { icon: 'fa fa-lastfm-square' }, { icon: 'fa fa-toggle-off' }, { icon: 'fa fa-toggle-on' }, { icon: 'fa fa-bicycle' }, { icon: 'fa fa-bus' }, { icon: 'fa fa-ioxhost' }, { icon: 'fa fa-angellist' }, { icon: 'fa fa-cc' }, { icon: 'fa fa-ils' }, { icon: 'fa fa-meanpath' }, { icon: 'fa fa-buysellads' }, { icon: 'fa fa-connectdevelop' }, { icon: 'fa fa-dashcube' }, { icon: 'fa fa-forumbee' }, { icon: 'fa fa-leanpub' }, { icon: 'fa fa-sellsy' }, { icon: 'fa fa-shirtsinbulk' }, { icon: 'fa fa-simplybuilt' }, { icon: 'fa fa-skyatlas' }, { icon: 'fa fa-cart-plus' }, { icon: 'fa fa-cart-arrow-down' }, { icon: 'fa fa-diamond' }, { icon: 'fa fa-ship' }, { icon: 'fa fa-user-secret' }, { icon: 'fa fa-motorcycle' }, { icon: 'fa fa-street-view' }, { icon: 'fa fa-heartbeat' }, { icon: 'fa fa-venus' }, { icon: 'fa fa-mars' }, { icon: 'fa fa-mercury' }, { icon: 'fa fa-transgender' }, { icon: 'fa fa-transgender-alt' }, { icon: 'fa fa-venus-double' }, { icon: 'fa fa-mars-double' }, { icon: 'fa fa-venus-mars' }, { icon: 'fa fa-mars-stroke' }, { icon: 'fa fa-mars-stroke-v' }, { icon: 'fa fa-mars-stroke-h' }, { icon: 'fa fa-neuter' }, { icon: 'fa fa-facebook-official' }, { icon: 'fa fa-pinterest-p' }, { icon: 'fa fa-whatsapp' }, { icon: 'fa fa-server' }, { icon: 'fa fa-user-plus' }, { icon: 'fa fa-user-times' }, { icon: 'fa fa-bed' }, { icon: 'fa fa-viacoin' }, { icon: 'fa fa-train' }, { icon: 'fa fa-subway' }, { icon: 'fa fa-medium' } ];
function alerta( titulo, mensaje, tipo ) {
	swal( titulo, mensaje, tipo );
}
function confirmacion( titulo, pregunta, callback ) {
	swal( {
		title: titulo,
		text: pregunta,
		icon: 'warning',
		buttons: [ 'NO', 'YES' ],
		dangerMode: true,
	} ).then( ( desicion ) => {
		if ( desicion ) {
			callback();
		} else {
			return false;
		}
	} );
}
function ingresaDato( pregunta, successCallback ) {
	swal( {
		text: pregunta,
		content: 'input',
		button: { 'text': 'Continue' }
	} ).then( ( input ) => {
		successCallback( input );
	} );
}
function vacia( cual ) {
	$( '#' + cual + ' input' ).each( function() {
		$( this ).val( '' );
	} );
	$( '#' + cual + ' input[type="file"]' ).each( function() {
		$( this ).attr( 'cargado', 'vacio' );
	} );
	$( '#' + cual + ' select' ).each( function() {
		$( this ).val( $( '#' + $( this ).attr( 'id' ) + ' option:first' ).val() );
	} );
	$( '#' + cual + ' textarea' ).each( function() {
		$( this ).val( '' );
	} );
	$( '#' + cual + ' .id' ).val( '0' );
	$( '#' + cual + ' .summernote' ).code( '' );
	$( '.showImg' ).html( '' );
	$( '.select2-multiple' ).val( 0 );
	$( '#' + cual + ' select' ).trigger( 'change' );
	desmarca( cual );
}
function marca( cual ) {
	desmarca( cual );
	$( '#' + cual + ' .obligatorio' ).each( function() {
		var clase = $( this ).attr( 'class' );
		if ( '' === $( this ).val() || ( 0 >= $( this ).val() && ( clase.indexOf( 'opcion' ) != -1 ) ) || ( !$( this ).is( ':checked' ) && ( clase.indexOf( 'checar' ) != -1 ) ) ) {
			$( this ).addClass( 'vacio' );
		}
	} );
}
function desmarca( cual ) {
	$( '#' + cual + ' .vacio' ).each( function() {
		$( this ).removeClass( 'vacio' );
	} );
}
function valEmail( email ) {
	var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if ( !expr.test( email ) ) {
		return false;
	}
	else {
		return true;
	}
}
function validaRfc( rfcStr ) {
	var strCorrecta;
	strCorrecta = rfcStr;
	if ( rfcStr.length == 12 ) {
		var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
	} else {
		var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
	}
	var validRfc = new RegExp( valid );
	var matchArray = strCorrecta.match( validRfc );
	if ( matchArray == null ) {
		return false;
	} else {
		return true;
	}
}
function validaTodo( cual ) {
	desmarca( cual );
	var mal = 0;
	var lleno = false;
	$( '#' + cual + ' .obligatorio' ).each( function() {
		var clase = $( this ).attr( 'class' );
		if ( clase.indexOf( 'archivo' ) != -1 && '' == $( this ).val() ) {
			if ( 'vacio' == $( this ).attr( 'cargado' ) ) {
				alerta( 'Error', 'You must add the requested file', 'error' );
				$( this ).parent().addClass( 'vacio' );
				mal++;
				console.log( $( this ).attr( 'id' ) );
			}
		} else {
			if ( '' === $( this ).val() || ( -1 >= $( this ).val() && ( clase.indexOf( 'opcion' ) != -1 ) ) || ( !$( this ).is( ':checked' ) && ( clase.indexOf( 'checar' ) != -1 ) ) ) {
				$( this ).addClass( 'vacio' );
				mal++;
				alerta( 'Error', 'Please fill in the field ' + $( this ).attr( 'id' ), 'error' );
				console.log( $( this ).attr( 'id' ) );
			} else {
				lleno = true;
				if ( clase.indexOf( 'email' ) != -1 ) {
					if ( !valEmail( $( this ).val() ) ) {
						$( this ).addClass( 'vacio' );
						alerta( 'Error', 'Enter a valid email format', 'error' );
						mal++;
					}
				} else if ( clase.indexOf( 'rfc' ) != -1 ) {
					if ( !validaRfc( $( this ).val() ) ) {
						$( this ).addClass( 'vacio' );
						alerta( 'Error', 'Please enter a valid RFC format', 'error' );
						mal++;
					}
				}
			}
		}
	} );
	if ( mal == 0 ) {
		return true;
	} else {
		if ( !lleno ) {
			alerta( 'Error', 'Please fill in the requested information', 'error' );
		}
		return false;
	}
}
function soloNumeros( e ) {
	var keynum = window.event ? window.event.keyCode : e.which;
	if ( ( 8 === keynum ) || ( 46 === keynum ) || ( 0 === keynum ) ) {
		return true;
	} else {
		return /\d/.test( String.fromCharCode( keynum ) );
	}
}
/*function porcentaje( cuanto ) {
	console.log( cuanto );
	$( '#progressbar' ).show();
	$( '#progressbar' ).progressbar( { value: cuanto } );
}*/
function popup( contenedor ) {
	$.magnificPopup.open( {
		removalDelay: 500,
		items: {
			src: '#' + contenedor
		},
		callbacks: {
			beforeOpen: function ( e ) {
				this.st.mainClass = 'mfp-flipInY';
			}
		},
		focus: '.opcion',
		midClick: true
	} );
}
function enter( e, funcion ) {
	var tecla = ( document.all ) ? e.keyCode : e.which;
	if ( 13 === tecla ) {
		eval( funcion + '()' );
	}
}
function obtenSesion( sesionId ) { return localStorage.getItem( sesionId ); }
function seteaSesion( sesionId, value ) { localStorage.setItem( sesionId, value ); }
function borraSesion( sesionId ) { localStorage.removeItem( sesionId ); }
function checaSesion( sesionId ) { if ( localStorage.getItem( sesionId ) != null ) { return true; } else { return false; } }
function login() {
	var usuario = $( '#usuario' ).val();
	var pass = $( '#pass' ).val();
	if ( '' !== usuario && '' !== pass && valEmail( usuario ) ) {
		$.ajax( {
			type: 'POST',
			url: 'querys/login',
			data: { 'usuario': usuario, 'pass': pass, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					seteaSesion( 'usuarioId', respuesta.usuario.id );
					seteaSesion( 'usuario', respuesta.usuario.usuario );
					seteaSesion( 'nombre', respuesta.usuario.nombre );
					seteaSesion( 'plataforma', respuesta.usuario.platform );
					seteaSesion( 'email', respuesta.usuario.email );
					seteaSesion( 'logo', respuesta.usuario.logo );
					seteaSesion( 'icono', respuesta.usuario.icono );
					seteaSesion( 'vooKey', respuesta.usuario.api.vooKey );
					seteaSesion( 'stripePublic', respuesta.usuario.api.stripePublic );
					seteaSesion( 'stripePrivate', respuesta.usuario.api.stripePrivate );
					alerta( 'Success', 'Session started, redirecting ...', 'success' );
					window.setTimeout( function() {
						location.href = 'principal';
					}, 1400 );
				} else {
					alerta( 'Error', 'Incorrect username / password', 'error' );
				}
			},
			error: function( response ) {
				alerta( 'Error', 'Something is wrong, try again', 'error' );
			}
		} );
	} else {
		if ( !valEmail( usuario ) ) {
			alerta( 'Error', 'Please enter a valid email address', 'error' );
		} else {
			alerta( 'Error', 'Please fill in both fields', 'error' );
		}
	}
}
function logout() {
	borraSesion( 'vooKey' );
	borraSesion( 'stripePublic' );
	borraSesion( 'stripePrivate' );
	borraSesion( 'usuarioId' );
	borraSesion( 'usuario' );
	borraSesion( 'nombre' );
	borraSesion( 'plataforma' );
	borraSesion( 'email' );
	borraSesion( 'logo' );
	borraSesion( 'icono' );
	location.href = '/';
}
function checaUsuario() {
	if ( null !== obtenSesion( 'usuarioId' ) && 'null' !== obtenSesion( 'usuarioId' ) && 'undefined' !== obtenSesion( 'usuarioId' ) ) {
		$( '#usuarion' ).html( obtenSesion( 'nombre' ) );
		$( '.miLogo' ).attr( 'src', '../images/clients/' + obtenSesion( 'logo' ) );
		$( '#source-button' ).hide();
	} else {
		alerta( 'Error', 'Session Not Validated, Redirecting ...', 'error' );
		window.setTimeout( logout, 1400 );
	}
}
function cargando( q ) {
	if ( !q ) {
		$( '.cargando' ).fadeOut( "slow" );
		$( '.capanegra' ).fadeOut( 'slow' );
	} else {
		$( '.cargando' ).fadeIn( 'slow' );
		$( '.capanegra' ).fadeIn( 'slow' );
	}
}
function manda( url ) {
	location.href = url;
}
function iniciaFormulario() {
	var contenedor = obtenSesion( 'contenedor' );
	vacia( contenedor );
	desmarca( contenedor );
	$( '#clientId' ).val( clientId );
	limpiaMarcas();
	$( '.muestraDibujo' ).remove();
	$( '.nuevoPass' ).show();
	$( '.oldPass' ).hide();
	if ( obtenSesion( 'tabla' ) == 'coaches' || obtenSesion( 'tabla' ) == 'users' ) {
		$( '#statusId' ).val( 2 ).trigger( 'change' );
	}
	popup( contenedor );
}
function listadoUnico() {
	var infoEnvia = { 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'clientId': clientId };
	$.ajax( {
		type: 'POST',
		url: 'querys/listado',
		data: infoEnvia,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				seteaSesion( 'unico', true );
				if ( respuesta.id != 0 ) {
					elegido( respuesta.id );
				} else {
					$( '#clientId' ).val( clientId );
				}
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}
function listado() {
	if ( obtenSesion( 'tabla' ) != null && obtenSesion( 'tabla' ) != '' && obtenSesion( 'tabla' ) != 'null' ) {
		$( '#paginacionTabla' ).html( '' );
		$( '.footable' ).footable().destroy();
		cargando( true );
		var infoEnvia = { 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'clientId': clientId };
		if ( checaSesion( 'serieTmp' ) ) {
			infoEnvia[ 'serieId' ] = obtenSesion( 'serieTmp' );
		}
		if ( $( '#mostrarConfirmados' ).length ) {
			infoEnvia[ 'emailConfirmed' ] = $( '#mostrarConfirmados' ).val();
		}
		$.ajax( {
			type: 'POST',
			url: 'querys/listado',
			data: infoEnvia,
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					$( '#contenidoTabla' ).html( respuesta.html );
					var ordenamiento = ( respuesta.posicionCampo != '' ) ? false : true;
					$( '.footable' ).footable( {
						'paging': { 'enabled': true, 'container': '#paginacionTabla', 'size': 20 },
						'filtering': { 'enabled': true },
						'sorting': { 'enabled': ordenamiento }
					} );
					if ( respuesta.posicionCampo != '' ) {
						campoPosicionTabla = respuesta.posicionCampo;
						$( '#contenidoTabla' ).sortable( {
							update: function( event, ui ) {
								$( this ).children().each( function( index ) {
									$( this ).find( 'td' ).last().html( index + 1 );
								} );
								actualizaPosiciones();
							}
						} );
					}
				} else {
					alerta( 'Error', 'Something happened try again', 'error' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( 'Error', 'Something happened try again', 'error' );
				cargando( false );
			}
		} );
	} else {
		var ubicacion = location.href;
		if ( ubicacion.indexOf( 'iconos-tecnologia' ) != -1 ) {
			opcionesLista( 'tecnologias', '', '', 'tecnologias' );
		}
	}
}
function actualizaPosiciones() {
	var posicionamiento = [];
	$( '#contenidoTabla tr' ).each( function() {
		var posicion = $( this ).index() + 1;
		var elementoId = $( this ).attr( 'campoId' );
		posicionamiento.push( { 'id': elementoId, 'posicion': posicion } );
	} );
	var infoEnvia = { 'tabla': obtenSesion( 'tabla' ), 'posiciones': JSON.stringify( posicionamiento ) };
	$.ajax( {
		type: 'POST',
		url: 'querys/posiciones',
		data: infoEnvia,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				console.log( 'posiciones actualizadas' );
				console.log( infoEnvia );
				console.log( 'Bien ' + respuesta.bien );
				console.log( 'Mal ' + respuesta.mal );
			}
		}
	} );
}
function elegido( id ) {
	cargando( true );
	var contenedor = obtenSesion( 'contenedor' );
	vacia( contenedor );
	$( '.muestraDibujo' ).remove();
	$.ajax( {
		type: 'POST',
		url: 'querys/listado',
		data: { 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'id': id, 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$.each( respuesta, function( key, value ) {
					if ( key != 'status' ) {
						if ( key.indexOf( 'multiple' ) != -1 ) {
							var multiples = value;
							var mul = multiples.split( '-' );
							$( '#' + key ).val( mul );
							$( '#' + key ).trigger( 'change' );
						} else {
							var elementoHTML = $( '#' + key );
							if ( typeof elementoHTML[0] != 'undefined' ) {
								var tipoElemento = elementoHTML[0].nodeName;
								if ( tipoElemento == 'INPUT' ) {
									var subTipo = elementoHTML[0].type;
									if ( subTipo != 'checkbox' && subTipo != 'file') {
										if ( elementoHTML.hasClass( 'pass' ) ) {
											elementoHTML.val( value );
											$( '.nuevoPass' ).hide();
											$( '.oldPass' ).show();
											$( '#liga-' + key ).attr( 'onclick', 'enviaPass( \'' + respuesta.email + '\', \'' + obtenSesion( 'tabla' ) + '\' )' );
										} else {
											elementoHTML.val( value );
										}
									} else if ( subTipo == 'checkbox' ) {
										var checado = true;
										if ( value == 'S' || value == 'N' ) {
											checado = ( value == 'S' ) ? true : false;
										} else {
											checado = value;
										}
										elementoHTML.prop( 'checked', checado );
									} else if ( subTipo == 'file' ) {
										if ( value != '' && value != null && ( value.indexOf( 'jpg' ) != -1 || value.indexOf( 'png' ) != -1 || value.indexOf( 'gif' ) != -1 || value.indexOf( 'jpeg' ) != -1 || value.indexOf( 'webp' ) != -1 ) ) {
											elementoHTML.attr( 'cargado', value );
											if ( elementoHTML.parent().parent().parent().parent().find( '.showImg' ).attr( 'obligado' ) == '' ) {
												elementoHTML.parent().parent().parent().parent().find( '.showImg' ).html( '<img src="../images/' + obtenSesion( 'folderActivo' ) + '/' + value + '" class="fullimg"><a class="quitaImagenShow" idEl="' + id + '" style="position: absolute; font-size: 20px; margin-left: -15px; padding: 0 5px; border-radius: 50%; background-color: #ccc;"><i class="fa fa-close"></i></a>' );
											} else {
												elementoHTML.parent().parent().parent().parent().find( '.showImg' ).html( '<img src="../images/' + obtenSesion( 'folderActivo' ) + '/' + value + '" class="fullimg">' );
											}
										} else {
											if ( value != '' && value != null ) {
												console.log( elementoHTML );
												console.log( value );
												if ( elementoHTML.parent().parent().parent().parent().find( '.showImg' ).attr( 'obligado' ) == '' ) {
													elementoHTML.parent().parent().parent().parent().find( '.showImg' ).html( '<iframe allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' + value + '?fallback=true" data-playerId="' + value + '" style="max-width:100%" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe><a class="quitaImagenShow" idEl="' + id + '" style="position: absolute; font-size: 20px; margin-left: -15px; padding: 0 5px; border-radius: 50%; background-color: #ccc;"><i class="fa fa-close"></i></a>' );
												} else {
													elementoHTML.parent().parent().parent().parent().find( '.showImg' ).html( '<iframe allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' + value + '?fallback=true" data-playerId="' + value + '" style="max-width:100%" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe>' );
												}
											}
										}
									}
								} else if ( tipoElemento == 'TEXTAREA' ) {
									elementoHTML.val( value );
								} else if ( tipoElemento == 'DIV' ) {
									elementoHTML.code( value );
								} else if ( tipoElemento == 'SELECT' ) {
									elementoHTML.val( value ).trigger( 'change' );
								}
							}
						}
						if ( key == 'firma' && value != '' ) {
							$( 'canvas' ).attr( 'creado', 1 );
							$( '.borraDibujo' ).after( '<a href="images/voluntarios/' + value + '" class="muestraDibujo" target="_blank">Current Signature</a>' );
						}
					}
				} );
				if ( obtenSesion( 'tabla' ) == 'videos' ) {
					$( '#archivo' ).attr( 'videoId', respuesta.archivo );
				}
				if ( !checaSesion( 'unico' ) ) {
					popup( contenedor );
				}
				$( '.quitaImagenShow' ).click( function( e ) {
					e.preventDefault();
					e.stopPropagation();
					var campo = $( this ).parent().attr( 'columna' );
					var campoId = $( this ).attr( 'idEl' );
					quitaShowImg( campo, campoId );
				} );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}
function guardaFormulario() {
	var contenedor = obtenSesion( 'contenedor' );
	if ( validaTodo( contenedor ) ) {
		if ( obtenSesion( 'tabla' ) != null ) {
			cargando( true );
			var formData = new FormData();
			var formMultiple = [];
			var hayMultiple = false;
			if ( obtenSesion( 'tabla' ) == 'coaches' || obtenSesion( 'tabla' ) == 'users' ) {
				var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
				formData.append( 'mail', mailActivo );
			}
			formData.append( 'tabla', obtenSesion( 'tabla' ) );
			formData.append( 'dios', true );
			if ( typeof clientId != 'undefined' && clientId != 0 ) {
				formData.append( 'clientId', clientId );
			}
			$( '#' + contenedor + ' input[type=file]' ).each( function() {
				var field = $( this ).parent().attr( 'contiene' );
				if ( field != 'undefined' && $( this ).val() != '' ) {
					var files = $( this ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( field, value );
					} );
				}
			} );
			$( '#' + contenedor + ' input[type="text"], #' + contenedor + ' input[type="password"], #' + contenedor + ' input[type="hidden"], #' + contenedor + ' textarea, #' + contenedor + ' select' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
					if ( typeof $( this ).attr( 'destino' ) != 'undefined' ) {
						var multiples = $( this ).val();
						if ( multiples != null ) {
							multiples = multiples.join( '-' );
						}
						var info = { 'destino': $( this ).attr( 'destino' ), 'ids': multiples }
						formMultiple.push( info );
						hayMultiple = true;
					} else {
						var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
						if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
							formData.append( $( this ).attr( 'id' ), $( this ).val() );
						}
					}
				}
			} );
			$( '#' + contenedor + ' input[type="checkbox"]' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
					var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
					if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
						var valorSi = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'S' : true;
						var valorNo = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'N' : false;
						var valor = ( $( this ).is( ':checked' ) ) ? valorSi : valorNo;
						formData.append( $( this ).attr( 'id' ), valor );
					}
				}
			} );
			$( '#' + contenedor + ' .summernote' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
					var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
					if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
						formData.append( $( this ).attr( 'id' ), $( this ).code() );
					}
				}
			} );
			$( '#' + contenedor + ' canvas' ).each( function() {
				if ( !signaturePad.isEmpty() ) {
					var data = signaturePad.toDataURL('image/png');
					formData.append( $( this ).attr( 'id' ), data );
				}
			} );
			if ( obtenSesion( 'folderActivo' ) != null ) {
				formData.append( 'folderActivo', obtenSesion( 'folderActivo' ) );
			}
			if ( hayMultiple ) {
				formData.append( 'multiples', JSON.stringify( formMultiple ) );
			}
			$.ajax( {
				type: 'POST',
				url: 'querys/guarda',
				data: formData,
				processData: false,
				contentType: false,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alerta( 'Success', 'Successfully stored', 'success' );
						if ( obtenSesion( 'tabla' ) == 'coaches' && $( '#coachId' ).val() == 0 ) {
							ajustaStatus( respuesta.id, 'N' );
						}
						if ( obtenSesion( 'tabla' ) == 'users' && $( '#userId' ).val() == 0 ) {
							ajustaStatusMiembro( respuesta.id, 'N' );
						}
						if ( obtenSesion( 'tabla' ) == 'users' && checaSesion( 'cambiaConfirmacion' ) ) {
							avisaConfirmacion( 2, respuesta.id, $( '#emailConfirmed' ).prop( 'checked' ) )
						}
						$( '#' + contenedor ).magnificPopup( 'close' );
						vacia( contenedor );
						desmarca( contenedor );
						console.log( 'guardado' )
						if ( !checaSesion( 'unico' ) ) {
							listado();
						} else {
							listadoUnico();
						}
					} else if ( 'Repetido' === respuesta.status ) {
						alerta( 'Error', 'This email is already register', 'error' );
					} else {
						alerta( 'Error', 'Something happened try again', 'error' );
					}
				}
			} );
		} else {
			alerta( 'Error', 'Something happened reload the page', 'error' );
		}
	}
}
function eliminadoCoach( coachId ) {
	seteaSesion( 'eliminaCoachId', coachId );
	var seguir = confirmacion( 'Confirmation', 'Are you sure to delete this information?', continuaEliminandoCoach );
}
function continuaEliminandoCoach() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/elimina-coach',
		data: { 'coachId': obtenSesion( 'eliminaCoachId' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				alerta( 'Success', 'Successfully removed', 'success' );
				borraSesion( 'eliminaCoachId' );
				listado();
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}
function eliminado( id ) {
	seteaSesion( 'eliminaId', id );
	var seguir = confirmacion( 'Confirmation', 'Are you sure to delete this information?', continuaEliminando );
}
function continuaEliminando() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/eliminado',
		data: { 'id': obtenSesion( 'eliminaId' ), 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				alerta( 'Success', 'Successfully removed', 'success' );
				borraSesion( 'eliminaId' );
				listado();
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}
function muestraIcono( contenedor ) {
	var contenidoHtml = '';
	for( var i = 0; i < fontAwesome.length; i++ ) {
		contenidoHtml += '<i class="' + fontAwesome[i].icon + '" onclick="eligeIcono( \'' + fontAwesome[i].icon + '\', \'' + contenedor + '\' )"></i>'
	}
	$( '#iconosMuestra' ).html( contenidoHtml );
	popIconPicker( 1 );
}
function popIconPicker( cual ) {
	if ( cual == 1 ) {
		$( '.iconoPicker' ).addClass( 'popupactive' );
		$( '.capanegra' ).fadeIn( 'slow' );
	} else {
		$( '.iconoPicker' ).removeClass( 'popupactive' );
		$( '.capanegra' ).fadeOut( 'slow' );
	}
}
function eligeIcono( icono, contenedor ) {
	$( '#' + contenedor ).val( icono );
	popIconPicker( 2 );
}
function opcionesLista( tablaReal, tablaPadre, idPadre, contenedor ) {
	var parametros = { 'tablaReal': tablaReal };
	if ( tablaPadre != '' ) {
		parametros[ 'tablaPadre' ] = tablaPadre;
	}
	if ( idPadre != 0 ) {
		parametros[ 'idPadre' ] = idPadre;
	}
	$.ajax( {
		type: 'POST',
		url: 'querys/opciones',
		data: parametros,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$( '#' + contenedor ).html( respuesta.opciones );
			}
		}
	} );
}
function seteaToHex( colorPicker ) {
	var t = colorPicker.spectrum( 'get' );
	colorPicker.val( t.toHexString() );
}
function subeGaleria() {
	if ( $( '#album' ).val() != '-1' ) {
		console.log( 'entrando' );
		var files = $( '#imagenGaleria' ).prop( 'files' );
		var cuantos = files.length;
		var cuantosVan = 1;
		var cuantosBien = 0;
		var cuantosMal = 0;
		cargando( true );
		cantidadVan( 0, cuantos );
		$.each( files, function( key, value ) {
			console.log( 'Iniciando Carga ' + cuantosVan );
			porcentaje( 0 );
			var formData = new FormData();
			formData.append( key, value );
			formData.append( 'tabla', obtenSesion( 'tabla' ) );
			formData.append( 'album', obtenSesion( 'album' ) );
			formData.append( 'albumId', $( '#album' ).val() );
			$.ajax( {
				type: 'POST',
				url: 'querys/guardagaleria',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener( 'progress', function( evt ) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = percentComplete * 100;
							porcentaje( percentComplete.toFixed( 2 ) );
						}
					}, false);
					xhr.addEventListener( 'progress', function( evt ) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = percentComplete * 100;
							porcentaje( percentComplete.toFixed( 2 ) );
						}
					}, false );
					return xhr;
				},
				success: function( response ) {
					var respuesta = JSON.parse( response );
					if ( 'Success' === respuesta.status ) {
						cuantosBien++;
					} else {
						cuantosMal++;
					}
					if ( cuantosVan == cuantos ) {
						cargando( false );
						listaAlbum( $( '#album' ).val() );
					} else {
						cuantosVan++;
						cantidadVan( cuantosVan, cuantos );
						console.log( 'Bien: ' + cuantosBien );
						console.log( 'Mal: ' + cuantosMal );
					}
				}, error: function() {
					cuantosVan++;
					cuantosMal++;
					console.log( 'Bien: ' + cuantosBien );
					console.log( 'Mal: ' + cuantosMal );
				}
			} );
		} );
	} else {
		alerta( 'Error', 'You must to pick the album first', 'error' );
	}
}
function listaAlbum( album ) {
	$( '#paginacionTabla' ).html( '' );
	var infoEnvia = { 'tabla': obtenSesion( 'tabla' ), 'album': obtenSesion( 'album' ), 'id': album };
	$.ajax( {
		type: 'POST',
		url: 'querys/listagaleriapanel',
		data: infoEnvia,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$( '#contenidoTabla' ).html( respuesta.html );
				var ordenamiento = ( respuesta.posicionCampo != '' ) ? false : true;
				$( '.footable' ).footable( {
					'paging': { 'enabled': true, 'container': '#paginacionTabla', 'size': 20 },
					'filtering': { 'enabled': false },
					'sorting': { 'enabled': ordenamiento }
				} );
				if ( respuesta.posicionCampo != '' ) {
					campoPosicionTabla = respuesta.posicionCampo;
					$( '#contenidoTabla' ).sortable( {
						update: function( event, ui ) {
							$( this ).children().each( function( index ) {
								$( this ).find( 'td' ).last().html( index + 1 );
							} );
							actualizaPosiciones();
						}
					} );
				}
			}
			cargando( false );
		},
		error: function( response ) {
			cargando( false );
		}
	} );
}
function eliminaGaleria( id ) {
	seteaSesion( 'eliminaId', id );
	var seguir = confirmacion( 'Confirmation', '¿Are you sure to remove this gallery?', continuaEliminandoGaleria );
}
function continuaEliminandoGaleria() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/eliminado',
		data: { 'id': obtenSesion( 'eliminaId' ), 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				alerta( '', 'Successfully removed', 'success' );
				borraSesion( 'eliminaId' );
				listaAlbum( $( '#album' ).val() );
			} else {
				alerta( '', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( '', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}
function cantidadVan( van, son ) {
	var valor = ( van * 100 ) / son;
	$( '#cuantosVan' ).attr( 'aria-valuenow', valor );
	$( '#cuantosVan' ).css( 'width', valor + '%' );
	$( '#cuantosVanValor' ).html( van + ' / ' + son );
	if ( !$( '#porcentajeDiv' ).is( ':visible' ) ) {
		$( '#porcentajeDiv' ).show();
	}
}
function porcentaje( valor ) {
	console.log( valor );
	$( '#porcentaje' ).attr( 'aria-valuenow', valor );
	$( '#porcentaje' ).css( 'width', valor + '%' );
	$( '#porcentajeValor' ).html( valor + '% Complete' );
	if ( !$( '#porcentajeDiv' ).is( ':visible' ) ) {
		$( '#porcentajeDiv' ).show();
	}
}
function checaTipo( tipoPost ) {
	if ( tipoPost == 2 ) {
		$( '#postSelect' ).removeClass( 'oculto' );
	} else {
		$( '#postSelect' ).addClass( 'oculto' );
		$( '#postSelect' ).val( '' );
	}
}
function quitaShowImg( campo, campoId ) {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/quitaimagen',
		data: { 'campo': campo, 'id': campoId, 'tabla': obtenSesion( 'tabla' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$( '.showImg[columna="' + campo + '"]' ).html( '' );
				$( '#' + campo ).attr( 'cargado', '' );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		}
	} );
}
function addGaleria( elementoId ) {
	seteaSesion( 'elementoId', elementoId );
	myDropzone.removeAllFiles();
	myDropzone.on( 'sending', function( file, xhr, formData ) {
		formData.append( 'elementoId', elementoId );
		formData.append( 'tabla', obtenSesion( 'tabla' ) );
	} );
	listaGaleria( elementoId );
	popup( 'modalGaleria' );
}
function listaGaleria( elementoId ) {
	$.ajax( {
		type: 'POST',
		url: 'querys/listagaleria',
		data: { 'id': elementoId, 'tabla': obtenSesion( 'tabla' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$( '#galeriaListDiv' ).html( respuesta.html );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
		}
	} );
}
function eliminaGaleria( id, tabla ) {
	if ( confirm( '¿Seguro de eliminar imagen de la galeria?' ) ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/eliminagaleria',
			data: { 'id': id, 'tabla': tabla, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					listaGaleria( respuesta.elemento );
					alerta( 'Success', 'Successfully removed', 'success' );
				} else {
					alerta( 'Error', 'Something happened try again', 'error' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( 'Error', 'Something happened try again', 'error' );
				cargando( false );
			}
		} );
	}
}
function verDetalle( pedidoId ) {
	$.ajax( {
		type: 'POST',
		url: 'querys/detalle',
		data: { 'pedido': pedidoId, 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$( '#detalleTabla' ).html( respuesta.html );
				$( '#totalPedido' ).html( respuesta.total );
				popup( 'modalDetalle' );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
		}
	} );
}
function borraDibujo( canvasId ) {
	signaturePad.clear();
}
function buscaDireccion( address ) {
	limpiaMarcas();
	geocoder.geocode( { 'address': address }, function( results, status ) {
		if ( status == 'OK' ) {
			$( '.buscaMapa' ).attr( 'latitud', results[0].geometry.location.lat() );
			$( '.buscaMapa' ).attr( 'longitud', results[0].geometry.location.lng() );
			$( '#latitud' ).val( results[0].geometry.location.lat() );
			$( '#longitud' ).val( results[0].geometry.location.lng() );
			map.setCenter( results[0].geometry.location );
			var marker = new google.maps.Marker( {
				map: map,
				position: results[0].geometry.location
			} );
			markers.push( marker );
		} else {
			console.log( 'Geocode error: ' + status );
		}
	} );
}
function limpiaMarcas() {
	for ( var i = 0; i < markers.length; i++ ) {
		markers[i].setMap( null );
	}
	markers = [];
}
function iniciaFiltros( videoId ) {
	opcionesLista( 'propiedades', '', '', 'propiedades' );
	$( '#videoFiltro' ).val( videoId );
	$( '#listaPropiedad' ).html( '' );
	popup( 'modal-filtros' );
	agregados = 0;
	cuantosInfo = 0;
}
function agregaPropiedad() {
	var filtro = prompt( 'Enter the filter name' );
	if ( filtro ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/agregapropiedades',
			data: { 'id': 0, 'filtro': filtro, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					alerta( '', 'Successfully stored', 'success' );
					opcionesLista( 'propiedades', '', '', 'propiedades' );
				} else {
					alerta( '', 'Something happened try again', 'danger' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( '', 'Something happened try again', 'danger' );
				cargando( false );
			}
		} );
	}
}
function editaPropiedad( id ) {
	var filtro = prompt( 'Enter the new filter name' );
	if ( filtro ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/agregapropiedades',
			data: { 'id': id, 'filtro': filtro, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					alerta( '', 'Successfully stored', 'success' );
					opcionesLista( 'propiedades', '', '', 'propiedades' );
				} else {
					alerta( '', 'Something happened try again', 'danger' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( '', 'Something happened try again', 'danger' );
				cargando( false );
			}
		} );
	}
}
function eliminaPropiedad( propiedadId ) {
	if ( confirm( 'Are you sure to remove this filter?' ) ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/eliminapropiedad',
			data: { 'propiedad': propiedadId, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					opcionesLista( 'propiedades', '', '', 'propiedades' );
					alerta( '', 'Successfully removed', 'success' );
				} else {
					alerta( '', 'Something happened try again', 'danger' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( '', 'Something happened try again', 'danger' );
				cargando( false );
			}
		} );
	}
}
function listaPropiedadValor( filtro ) {
	if ( filtro != 0 ) {
		var video = $( '#videoFiltro' ).val();
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/listafiltro',
			data: { 'filtro': filtro, 'video': video, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					$( '#listaPropiedad' ).html( respuesta.html );
					cuantosInfo = parseInt( respuesta.cuantos );
					agregados = parseInt( respuesta.cuantos );
				} else {
					$( '#listaPropiedad' ).html( '' );
					agregados = 0;
					cuantosInfo = 0;
				}
				cargando( false );
			}
		} );
	}
}
function addElement() {
	if ( $( '#propiedades' ).val() != 0 ) {
		if ( agregados == 0 ) {
			$( '#listaPropiedad' ).html( '' );
		}
		cuantosInfo++;
		var divContent = '<div class="row" id="info-' + cuantosInfo + '" style="margin-top: 5px;" class="contieneInfo"><div class="col-lg-11" id="seleccion-' + cuantosInfo + '"><label class="field select setenta"><select id="infoSelect-' + cuantosInfo + '" class="informacion"></select><i class="arrow"></i></label><a class="addi" onclick="editValor( ' + $( '#infoSelect-' + cuantosInfo ).val() + ' )"><i class="fa fa-edit"></i></a><a class="addi" onclick="eliminaValor( ' + $( '#infoSelect-' + cuantosInfo ).val() + ' )"><i class="fa fa-minus"></i></a><a class="addi" onclick="agregaValor()"><i class="fa fa-plus"></i></a></div></div>';
		$( '#listaPropiedad' ).append( divContent );
		agregados++;
		var indiceVal = 'infoSelect-' + cuantosInfo;
		opcionesInfo( indiceVal );
	} else {
		alerta( '', 'You must first choose a filter', 'danger' );
	}
}
function agregaValor() {
	var valor = prompt( 'Enter the filter value' );
	if ( valor ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/agregavalores',
			data: { 'id': 0, 'filtro': $( '#propiedades' ).val(), 'valor': valor, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					alerta( '', 'Successfully stored', 'success' );
					opcionesInfo( 0 );
				} else {
					alerta( '', 'Something happened try again', 'danger' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( '', 'Something happened try again', 'danger' );
				cargando( false );
			}
		} );
	}
}
function editValor( id ) {
	var filtro = prompt( 'Enter the new filter value' );
	if ( filtro ) {
		cargando( true );
		$.ajax( {
			type: 'POST',
			url: 'querys/agregavalores',
			data: { 'id': id, 'filtro': $( '#propiedades' ).val(), 'valor': valor, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( 'Success' === respuesta.status ) {
					alerta( '', 'Successfully stored', 'success' );
					listaPropiedadValor( $( '#propiedades' ).val() );
				} else {
					alerta( '', 'Something happened try again', 'danger' );
				}
				cargando( false );
			},
			error: function( response ) {
				alerta( '', 'Something happened try again', 'danger' );
				cargando( false );
			}
		} );
	}
}
function eliminaInfo( id ) {
	if ( id != 0 ) {
		if ( confirm( 'Are you sure to delete this value?' ) ) {
			cargando( true );
			$.ajax( {
				type: 'POST',
				url: 'querys/eliminainfo',
				data: { 'id': id, 'clientId': clientId },
				async: true,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alerta( '', 'Successfully removed', 'success' );
						listaPropiedadValor( $( '#propiedades' ).val() );
					} else {
						alerta( '', 'There was a problem, try again', 'danger' );
					}
				},
				error: function( response ) {
					cargando( false );
					alerta( '', 'Something is wrong, try again', 'danger' );
				}
			} );
		}
	} else {
		alerta( '', 'You must first choose a value', 'danger' );
	}
}
function guardaInfo() {
	var propiedad = $( '#propiedades' ).val();
	if ( propiedad != 0 ) {
		var cuantos = 0;
		var informaciones = '';
		var video = $( '#videoFiltro' ).val();
		$( '.informacion' ).each( function( elemento ) {
			var valor = $( this ).val();
			if ( valor != '0' ) {
				if ( cuantos != 0 ) {
					informaciones += '*;*';
				}
				informaciones += valor;
				cuantos++;
			}
			console.log( valor );
		} );
		if ( cuantos > 0 ) {
			cargando( true );
			$.ajax( {
				type: 'POST',
				url: 'querys/guardainfo',
				data: { 'video': video, 'infos': informaciones, 'propiedad': propiedad, 'clientId': clientId },
				async: true,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alerta( '', 'Successfully stored', 'success' );
						listaPropiedadValor( $( '#propiedades' ).val() );
					} else {
						alerta( '', 'There was a problem, try again', 'danger' );
					}
				},
				error: function( response ) {
					cargando( false );
					alerta( '', 'Something is wrong, try again', 'danger' );
				}
			} );
		}
	} else {
		alerta( '', 'You must first choose a filter', 'danger' );
	}
}
function opcionesInfo( indice ) {
	$.ajax( {
		type: 'POST',
		url: 'querys/listaopcionesfiltro',
		data: { 'filtro': $( '#propiedades' ).val(), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				if ( indice == 0 ) {
					$( '.informacion' ).html( respuesta.html );
				} else {
					$( '#' + indice ).html( respuesta.html );
				}
			}
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function uploadVideo() {
	var contenedor = obtenSesion( 'contenedor' );
	if ( validaTodo( contenedor ) ) {
		cargando( true );
		var filesCuantos = 0;
		var formData = new FormData();
		formData.append( 'vooKey', obtenSesion( 'vooKey' ) );
		formData.append( 'create', 1 );
		formData.append( 'customS3', 0 );
		$( '#' + contenedor + ' input[type=file].spotlightr' ).each( function() {
			var files = $( this ).prop( 'files' );
			$.each( files, function( key, value ) {
				formData.append( 'file', value );
				var nombreFile = ( $( '#nombre' ).length ) ? $( '#nombre' ).val() : $( '#titulo' ).val();
				formData.append( 'name', nombreFile );
				filesCuantos++;
			} );
		} );
		if ( filesCuantos > 0 ) {
			$.ajax( {
				type: 'POST',
				url: 'https://api.vooplayer.com/api/createVideo',
				data: formData,
				processData: false,
				contentType: false,
				xhr: function() {
					var xhr = $.ajaxSettings.xhr();
					xhr.upload.onprogress = function(e) {
						porcentaje( Math.floor(e.loaded / e.total *100) );
					};
					return xhr;
				},
				success: function( response ) {
					var videoId = response.split( '/publish/' )[1];
					porcentaje( 100 );
					var formData = new FormData();
					var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
					formData.append( 'mail', mailActivo );
					if ( typeof clientId != 'undefined' && clientId != 0 ) {
						formData.append( 'clientId', clientId );
					}
					formData.append( 'tabla', obtenSesion( 'tabla' ) );
					formData.append( 'folderActivo', obtenSesion( 'folderActivo' ) );
					$( '#' + contenedor + ' .summernote' ).each( function() {
						if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
							var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
							if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
								formData.append( $( this ).attr( 'id' ), $( this ).code() );
							}
						}
					} );
					$( '#' + contenedor + ' input[type="text"], #' + contenedor + ' input[type="password"], #' + contenedor + ' input[type="hidden"], #' + contenedor + ' textarea, #' + contenedor + ' select' ).each( function() {
						if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
							var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
							if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
								formData.append( $( this ).attr( 'id' ), $( this ).val() );
							}
						}
					} );
					$( '#' + contenedor + ' input[type="checkbox"]' ).each( function() {
						if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
							var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
							if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
								var valorSi = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'S' : true;
								var valorNo = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'N' : false;
								var valor = ( $( this ).is( ':checked' ) ) ? valorSi : valorNo;
								formData.append( $( this ).attr( 'id' ), valor );
							}
						}
					} );
					$( '#' + contenedor + ' input[type=file].spotlightr' ).each( function() {
						var field = $( this ).parent().attr( 'contiene' );
						formData.append( field, videoId );
					} );
					$( '#' + contenedor + ' input[type=file].archivo' ).each( function() {
						var field = $( this ).parent().attr( 'contiene' );
						if ( field != 'undefined' && $( this ).val() != '' ) {
							var files = $( this ).prop( 'files' );
							$.each( files, function( key, value ) {
								formData.append( field, value );
							} );
						}
					} );
					$.ajax( {
						type: 'POST',
						url: 'querys/guarda',
						data: formData,
						processData: false,
						contentType: false,
						success: function( response ) {
							var respuesta = JSON.parse( response );
							cargando( false );
							if ( 'Success' === respuesta.status ) {
								alerta( 'Success', 'Successfully stored', 'success' );
								if ( obtenSesion( 'tabla' ) == 'coaches' && $( '#coachId' ).val() == 0 ) {
									ajustaStatus( respuesta.id, 'N' );
								}
								if ( obtenSesion( 'tabla' ) == 'coaches' && checaSesion( 'cambiaConfirmacion' ) ) {
									avisaConfirmacion( 1, respuesta.id, $( '#emailConfirmed' ).prop( 'checked' ) )
								}
								$( '#' + contenedor ).magnificPopup( 'close' );
								vacia( contenedor );
								desmarca( contenedor );
								listado();
							} else if ( 'Repetido' === respuesta.status ) {
								alerta( 'Error', 'This email is already register', 'error' );
							} else {
								alerta( 'Error', 'Something happened try again', 'error' );
							}
						}
					} );
				}
			} );
		} else {
			var formData = new FormData();
			var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
			formData.append( 'mail', mailActivo );
			formData.append( 'tabla', obtenSesion( 'tabla' ) );
			formData.append( 'folderActivo', obtenSesion( 'folderActivo' ) );
			if ( typeof clientId != 'undefined' && clientId != 0 ) {
				formData.append( 'clientId', clientId );
			}
			$( '#' + contenedor + ' .summernote' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
					var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
					if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
						formData.append( $( this ).attr( 'id' ), $( this ).code() );
					}
				}
			} );
			$( '#' + contenedor + ' input[type="text"], #' + contenedor + ' input[type="password"], #' + contenedor + ' input[type="hidden"], #' + contenedor + ' textarea, #' + contenedor + ' select' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
					var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
					if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
						formData.append( $( this ).attr( 'id' ), $( this ).val() );
					}
				}
			} );
			$( '#' + contenedor + ' input[type="checkbox"]' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' ) {
					var clase = ( typeof $( this ).attr( 'class' ) == 'undefined' ) ? '' : $( this ).attr( 'class' );
					if ( clase.indexOf( 'noPelar' ) == -1 || typeof clase == 'undefined' ) {
						var valorSi = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'S' : true;
						var valorNo = ( $( this ).attr( 'tipo' ) == 'SN' ) ? 'N' : false;
						var valor = ( $( this ).is( ':checked' ) ) ? valorSi : valorNo;
						formData.append( $( this ).attr( 'id' ), valor );
					}
				}
			} );
			$( '#' + contenedor + ' input[type=file].archivo' ).each( function() {
				var field = $( this ).parent().attr( 'contiene' );
				if ( field != 'undefined' && $( this ).val() != '' ) {
					var files = $( this ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( field, value );
					} );
				}
			} );
			$.ajax( {
				type: 'POST',
				url: 'querys/guarda',
				data: formData,
				processData: false,
				contentType: false,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alerta( 'Success', 'Successfully stored', 'success' );
						if ( obtenSesion( 'tabla' ) == 'coaches' && $( '#coachId' ).val() == 0 ) {
							ajustaStatus( respuesta.id, 'N' );
						}
						if ( obtenSesion( 'tabla' ) == 'coaches' && checaSesion( 'cambiaConfirmacion' ) ) {
							avisaConfirmacion( 1, respuesta.id, $( '#emailConfirmed' ).prop( 'checked' ) )
						}
						$( '#' + contenedor ).magnificPopup( 'close' );
						vacia( contenedor );
						desmarca( contenedor );
						listado();
					} else if ( 'Repetido' === respuesta.status ) {
						alerta( 'Error', 'This email is already register', 'error' );
					} else {
						alerta( 'Error', 'Something happened try again', 'error' );
					}
				}
			} );
		}
	}
}
function borraVideo( videoId, videoSpotId ) {
	if ( confirm( 'Are you sure to delete this video?, it gonna be removed from this site and also Spotlightr' ) ) {
		cargando( true );
		console.log( 'borrando video de spotlightr' );
		var ids = [];
		ids.push( videoSpotId );
		$.ajax( {
			type: 'POST',
			url: 'http://api.spotlightr.com/api/deleteVideo',
			data: { 'vooKey': obtenSesion( 'videoKey' ), 'IDs': ids, 'clientId': clientId },
			success: function( response ) {
				console.log( response );
				$.ajax( {
					type: 'POST',
					url: 'querys/eliminado',
					data: { 'id': videoId, 'tabla': 'videos', 'folder': 'videos', 'clientId': clientId },
					async: true,
					success: function( response ) {
						var respuesta = JSON.parse( response );
						if ( 'Success' === respuesta.status ) {
							alerta( 'Success', 'Successfully removed', 'success' );
							listado();
						} else {
							alerta( 'Error', 'Something happened try again', 'error' );
						}
						cargando( false );
					},
					error: function( response ) {
						alerta( 'Error', 'Something happened try again', 'error' );
						cargando( false );
					}
				} );
			}
		} );
	}
}
function enviaPass( email, tabla ) {
	if ( confirm( 'Are you sure send the password?, this action gonna change the current password' ) ) {
		$.ajax( {
			type: 'POST',
			url: 'querys/enviapass',
			data: { 'email': email, 'tabla': tabla, 'clientId': clientId },
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				if ( respuesta.status == 'Success' ) {
					alerta( '', 'Password sent correctly to email', 'success' );
				} else if ( respuesta.status == 'Pass' ) {
					alerta( 'Error', 'There was a problem assined the new pass, try again', 'error' );
				} else {
					alerta( 'Error', 'Something happened try again', 'error' );
				}
			},
			error: function( response ) {
				alerta( '', 'Something is wrong, reload the page', 'danger' );
			}
		} );
	}
}
function respuestaAlumno( userId, tipo ) {
	$.ajax( {
		type: 'POST',
		url: 'querys/obtenrespuestas',
		data: { 'userId': userId, 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				$( '#respuestaDiv' ).html( respuesta.respuestas );
				if ( tipo == 1 ) {
					popup( 'modalQuestion' );
				}
			} else {
				alerta( 'Answer not found', 'This user doesn\'t have any answer saved on database', 'error' );
			}
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function listaDetalle() {
	$.ajax( {
		type: 'POST',
		url: 'querys/listado',
		data: { 'tabla': obtenSesion( 'tabla' ), 'folder': obtenSesion( 'folderActivo' ), 'id': obtenSesion( 'id' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				$.each( respuesta, function( key, value ) {
					if ( key != 'status' ) {
						if ( key.indexOf( 'multiple' ) != -1 ) {
							var multiples = value;
							var mul = multiples.split( '-' );
							$( '#' + key ).val( mul );
							$( '#' + key ).trigger( 'change' );
						} else {
							var elementoHTML = $( '#' + key );
							var comentarioHTML = elementoHTML.attr( 'comentarios' );
							if ( typeof elementoHTML[0] != 'undefined' ) {
								if ( key.indexOf( 'multiple' ) != -1 ) {
									var multiples = value;
									var mul = multiples.split( '-' );
									elementoHTML.val( mul );
									elementoHTML.trigger( 'change' );
								} else if ( key.indexOf( 'opciones' ) != -1 ) {
									elementoHTML.val( value );
								} else if ( key.indexOf( 'pending' ) != -1 ) {
									elementoHTML.val( value );
								} else {
									if ( comentarioHTML == '' || comentarioHTML == 'nombre' || comentarioHTML == 'hoy' || comentarioHTML == 'fecha' ) {
										if ( value != null && value != '' ) {
											elementoHTML.html( value );
										}
									} else {
										if ( comentarioHTML == 'activar' ) {
											var icono = ( value == 'S' ) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>';
											elementoHTML.html( icono );
										} else if ( comentarioHTML == 'email' ) {
											var link = '<a href="mailto:' + value + '">' + value + '</a>';
											elementoHTML.html( link );
										} else if ( comentarioHTML == 'liga' && value != null ) {
											var link = '<a href="' + value + '">' + value + '</a>';
											elementoHTML.html( link );
										} else if ( comentarioHTML == 'archivo' ) {
											if ( value != '' && value != null && ( value.indexOf( 'jpg' ) != -1 || value.indexOf( 'png' ) != -1 || value.indexOf( 'gif' ) != -1 || value.indexOf( 'jpeg' ) != -1 ) ) {
												elementoHTML.html( '<img src="../images/' + obtenSesion( 'tabla' ) + '/' + value + '" style="width: 150px; height: auto; margin: 0 auto;">' );
												elementoHTML.css( 'text-align', 'center' );
											} else {
												if ( value != null ) {
													elementoHTML.html( '<iframe src="../images/' + obtenSesion( 'tabla' ) + '/' + value + '" class="showImg"></iframe>' );
													elementoHTML.css( 'text-align', 'center' );
												} else {
													elementoHTML.html( 'Not assigned' );
												}
											}
										} else if ( comentarioHTML == 'pass' ) {
											var link = '<a onclick="enviaPass( \'' + respuesta.email + '\', \'' + obtenSesion( 'tabla' ) + '\' )">Re-send Password</a>';
											elementoHTML.html( link );
										} else if ( comentarioHTML == 'spotlightr' ) {
											if ( value != null && value != '' ) {
												elementoHTML.html( '<iframe allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' + value + '?fallback=true" data-playerId="' + value + '" style="max-width:100%; width: 450px;" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe>' );
											} else {
												elementoHTML.html( 'Not assigned video' );
											}
										} else if ( comentarioHTML == 'firma' ) {
											elementoHTML.html( '<img src="../images/' + obtenSesion( 'tabla' ) + '/' + value + '" class="showImg">' );
										}
									}
								}
							}
						}
					}
				} );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function llamadasCoach() {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	$.ajax( {
		type: 'POST',
		url: 'querys/llamadas',
		data: { 'coachId': obtenSesion( 'id' ), 'clientId': clientId, 'timeZone': timeZone },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				$( '#contenidoTabla' ).html( respuesta.html );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function traeLlamadas() {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	$.ajax( {
		type: 'POST',
		url: 'querys/llamadas',
		data: { 'userId': obtenSesion( 'id' ), 'clientId': clientId, 'timeZone': timeZone },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				$( '#contenidoTabla' ).html( respuesta.html );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function listaSeries() {
	$.ajax( {
		type: 'POST',
		url: 'querys/arregloopciones',
		data: { 'tabla': 'series', 'padre': 'coachId', 'idPadre': obtenSesion( 'id' ), 'clientId': clientId },
		async: true,
		success: function( response ) {
			var opciones = '<option value="">Pick your serie</option>';
			datos = JSON.parse( response );
			$.each( datos.elementos, function(index, el) {
				opciones += '<option value="' + el.id + '">' + el.nombre + '</option>';
			} );
			$( '#serieCoach' ).html( opciones );
		},
		error: function( response ) {
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function cargaVideos() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/cargavideos',
		data: { 'coachId': obtenSesion( 'id' ) },
		async: true,
		success: function( response ) {
			cargando( false );
			var filas = '';
			datos = JSON.parse( response );
			$.each( datos.videos, function(index, el) {
				filas += '<tr>';
				filas += '<td><a onclick="muestraVideo( \'' + el.videoId + '\' )">Play the video</a></td>';
				filas += '<td>' + el.titulo + '</td>'
				filas += '<td>' + el.descripcion + '</td>';
				filas += '</tr>';
			} );
			if ( filas == '' ) {
				filas = '<tr><td colspan="4">There is no video on this serie</td></tr>';
			}
			$( '#videoTabla' ).html( filas );
		},
		error: function( response ) {
			cargando( false );
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function muestraVideo( videoId ) {
	$( '#videoPopup iframe' ).attr( 'src', 'https://amrak.cdn.spotlightr.com/publish/' + videoId + '?fallback=true' );
	$( '#videoPopup iframe' ).attr( 'data-playerId', videoId );
	popup( 'videoPopup' );
}
function avisaConfirmacion( tipo, id, status ) {
	if ( status ) {
		var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
		if ( mailActivo == 'S' ) {
			$.ajax( {
				type: 'POST',
				url: 'querys/email-confirmation',
				data: { 'tipo': tipo, 'id': id, 'status': status, 'clientId': clientId, 'mail': mailActivo },
				async: true,
				success: function( response ) {
					cargando( false );
					var respuesta = JSON.parse( response );
					if ( respuesta.status == 'Success' ) {
						console.log( 'email confirmation actualizado' );
					} else {
						console.log( 'email confirmation no actualizado' );
					}
					borraSesion( 'cambiaConfirmacion' );
				},
				error: function( response ) {
					cargando( false );
					console.log( 'email confirmation no actualizado' );
					borraSesion( 'cambiaConfirmacion' );
				}
			} );
		} else {
			console.log( 'global desactivado' );
		}
	} else {
		console.log( 'no enviando apagada' );
	}
}
function ajustaStatusUnico( status ) {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/actualiza-pendings',
		data: { 'coachId': obtenSesion( 'id' ), 'status': status, 'clientId': clientId },
		async: true,
		success: function( response ) {
			cargando( false );
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				if ( status == 'R' ) {
					$( '#coachIdReason' ).val( obtenSesion( 'id' ) );
					$( '#coachReason' ).val( '' );
					popup( 'modal-coaches' );
				} else {
					alerta( '', 'Successfully updated', 'success' );
				}
			} else {
				alerta( '', 'Something is wrong, reload the page', 'danger' );
			}
		},
		error: function( response ) {
			cargando( false );
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function ajustaStatus( coachId, status ) {
	cargando( true );
	var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
	$.ajax( {
		type: 'POST',
		url: 'querys/actualiza-pendings',
		data: { 'coachId': coachId, 'status': status, 'clientId': clientId, 'mail': mailActivo },
		async: true,
		success: function( response ) {
			cargando( false );
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				if ( status == 'R' ) {
					$( '#coachIdReason' ).val( coachId );
					$( '#coachReason' ).val( '' );
					popup( 'modal-coaches' );
				} else {
					alerta( '', 'Successfully updated', 'success' );
				}
			} else {
				alerta( '', 'Something is wrong, reload the page', 'danger' );
			}
		},
		error: function( response ) {
			cargando( false );
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function ajustaStatusMiembro( studentId, status ) {
	cargando( true );
	var mailActivo = ( checaSesion( 'mandaEmail' ) ) ? obtenSesion( 'mandaEmail' ) : 'S';
	$.ajax( {
		type: 'POST',
		url: 'querys/actualiza-pendings-member',
		data: { 'studentId': studentId, 'status': status, 'clientId': clientId, 'mail': mailActivo },
		async: true,
		success: function( response ) {
			cargando( false );
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				if ( status == 'R' ) {
					$( '#coachIdReason' ).val( studentId );
					$( '#coachReason' ).val( '' );
					popup( 'modal-coaches' );
				} else {
					alerta( '', 'Successfully updated', 'success' );
				}
			} else {
				alerta( '', 'Something is wrong, reload the page', 'danger' );
			}
		},
		error: function( response ) {
			cargando( false );
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function rechazaTexto() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/actualiza-raon',
		data: { 'tabla': localStorage.getItem( 'tabla' ),'coachId': $( '#coachIdReason' ).val(), 'razon': $( '#coachReason' ).val(), 'clientId': clientId },
		async: true,
		success: function( response ) {
			cargando( false );
			var respuesta = JSON.parse( response );
			if ( respuesta.status == 'Success' ) {
				$( '#modal-coaches' ).magnificPopup( 'close' );
				alerta( '', 'Successfully updated', 'success' );
			} else {
				alerta( '', 'Something is wrong, reload the page', 'danger' );
			}
		},
		error: function( response ) {
			cargando( false );
			alerta( '', 'Something is wrong, reload the page', 'danger' );
		}
	} );
}
function subeSummerImg( image, contenedor ) {
	var formData = new FormData();
	formData.append( 'imagen', image );
	formData.append( 'baseUrl', baseURL );
	$.ajax( {
		type: 'POST',
		url: 'querys/subeimagen',
		data: formData,
		processData: false,
		contentType: false,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				var imagenSube = $( '<img>' ).attr( 'src', respuesta.url );
				$( '#' + contenedor ).summernote( 'insertNode', imagenSube[ 0 ] );
				console.log( imagenSube[ 0 ] );
			} else {
				console.log( respuesta.archivo );
			}
		},
		error: function( data ) {
			console.log( data );
		}
	} );
}
function abreVideos( serieId ) {
	seteaSesion( 'serieTmp', serieId );
	manda( 'edita/videos' );
}
function borraCuenta( tabla, id ) {
	seteaSesion( 'tablaBorra', tabla );
	seteaSesion( 'idBorra', id );
	var seguir = confirmacion( 'Confirmation', 'Are you sure to delete this account?', continuaBorraCuenta );
}
function continuaBorraCuenta() {
	cargando( true );
	$.ajax( {
		type: 'POST',
		url: 'querys/borra-cuenta',
		data: { 'tabla': obtenSesion( 'tablaBorra' ), 'id': obtenSesion( 'idBorra' ) },
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			if ( 'Success' === respuesta.status ) {
				alerta( 'Success', 'Successfully removed', 'success' );
				borraSesion( 'tablaBorra' );
				borraSesion( 'idBorra' );
				listado();
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
			cargando( false );
		},
		error: function( response ) {
			alerta( 'Error', 'Something happened try again', 'error' );
			cargando( false );
		}
	} );
}