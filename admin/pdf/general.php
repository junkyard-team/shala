<?php
	include '../querys/conexion.php';
	include '../querys/functions.php';
	global $palabrasReservadas;
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$fecha = strftime( '%Y-%m-%d', time() );
	$mes = array( "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
	$thead = '<th class="tcenter">ID</th>';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
			if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
				$titulo = palabraDiccionario( $R[ 'columna' ], $con );
				$thead .= '<th>' . ucfirst( $titulo ). '</th>';
			} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
				$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
				$titulo = palabraDiccionario( $tablaCatalogo, $con );
				$thead .= '<th>' . ucfirst( $titulo ). '</th>';
			}
		}
	}
	$html = '<center><img src="logo.png" width="150" style="margin: 0; left: 0; right: 0"></center>';
	$html .= '<br><p style="font-size: 16px; margin: 0;" align="right">Lista de ' . ucfirst( $tabla ) . '</p>';
	$html .= '<br><br>';
	$html .=
	'<table style="width: 100%; border-collapse: collapse; font-family: Calibri; font-size: 11px;" border="1">
		<thead><tr>' . $thead . '</tr></thead><tbody>';
	$multiplesComment = '';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
			$tipoId = $R[ 'tipoDatos' ];
			if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
				$multiplesComment = $R[ 'comentarios' ];
			}
		}
		if ( $R[ 'comentarios' ] == 'html' ) {
			$tipoHtml[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'pass' ) {
			$tipoPass[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'liga' ) {
			$tipoLiga[] = $R[ 'columna' ];
		}
		if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
			$tipoOpciones[] = $R[ 'columna' ];
		}
	}
	$strList = 'select ' . $campoId;
	$filas = 0;
	$opcionesCampos = array();
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
		echo $str .'<hr><br>';
	}
	while( $R = $con->Resultados( $res ) ) {
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			echo 'COLUMNA = ' . $R[ 'columna' ] . ' | COMENTARIO = ' . $R[ 'comentarios' ] . ' | ' . ' OBLIGATORIA = ' . $R[ 'obligatoria' ] . ' | LLAVE = ' . $R[ 'esLlave' ];
		}
		if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
			if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
				echo '<br>Entro';
			}
			if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
				if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
					echo '<br>Entro a palabras reservadas o sin comentarios';
				}
				$strList .= ', ' . $R[ 'columna' ];
				$columnas[ $filas ] = $R[ 'columna' ];
				$filas++;
			} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
				if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
					echo '<br>Entro a opciones';
				}
				$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
				$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tablaCatalogo );
				$opcionesCampos[ $R[ 'columna' ] ] = array( 'tabla' => $tablaCatalogo, 'titulo' => ucfirst( $titulo ), 'campo' => $R[ 'columna' ] );
				$strList .= ', ' . $R[ 'columna' ];
				$columnas[ $filas ] = $R[ 'columna' ];
				$filas++;
			}
		}
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			echo '<hr>';
		}
	}
	if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
		var_dump( $tipoOpciones );
		var_dump( $opcionesCampos );
		var_dump( $columnas );
	}
	if ( isset( $_REQUEST[ 'idioma' ] ) ) {
		if ( !isset( $_REQUEST[ 'sucursal' ] ) ) {
			$strList .= ' from ' . $tabla . ' where idioma="' . $_REQUEST[ 'idioma' ] . '" order by ' . $campoId . ' desc';
		} else {
			$strList .= ' from ' . $tabla . ' where idioma="' . $_REQUEST[ 'idioma' ] . '" and idsucursal=' . $_REQUEST[ 'sucursal' ] . ' order by ' . $campoId . ' desc';
		}
	} else {
		if ( !isset( $_REQUEST[ 'sucursal' ] ) ) {
			$strList .= ' from ' . $tabla . ' order by ' . $campoId . ' desc';
		} else {
			$strList .= ' from ' . $tabla . ' where idsucursal=' . $_REQUEST[ 'sucursal' ] . ' order by ' . $campoId . ' desc';
		}
	}
	$res = $con->Consulta( $strList );
	$i = 0;
	while( $R = $con->Resultados( $res ) ) {
		$ligaTiene = false;
		$fila = '<tr id="' . $tabla . '-' . $R[ $campoId ] . '">';
		$eliminado = ( $tipoId == 'int' ) ? 'eliminado( ' . $R[ $campoId ] . ' )' : 'eliminado( \'' . $R[ $campoId ] . '\' )';
		$elegido = ( $tipoId == 'int' ) ? 'elegido( ' . $R[ $campoId ] . ' )' : 'elegido( \'' . $R[ $campoId ] . '\' )';
		$fila .=
			'<td class="tcenter">' . $R[ $campoId ] . '</td>';
		if ( isset( $columnas ) ) {
			for ( $indi = 0; $indi < sizeof( $columnas ); $indi++ ) {
				if ( isset( $tipoHtml ) ) {
					$informacion = ( in_array( $columnas[ $indi ], $tipoHtml ) ) ? substr( strip_tags( html_entity_decode( $R[ $columnas[ $indi ] ] ) ), 0, 100 ) : $R[ $columnas[ $indi ] ];
					if ( !in_array( $columnas[ $indi ], $tipoHtml ) && ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) ) ) {
						$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
						$fila .= '<td class="tcenter">' . $informacion . '</td>';
					} else {
						if ( isset( $tipoPass ) && $informacion != '' ) {
							$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
						}
						if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
							$ligaTiene = true;
							$informacion = '<a href="' . $informacion . '" target="_blank">' . $informacion . '</a>';
						}
						if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
							$datos = $opcionesCampos[ $columnas[ $indi ] ];
							if ( $informacion > 0 ) {
								$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>Campo Opciones Actual';
									var_dump( $datos );
									echo $campoNombre . '<br>';
								}
								if ( $campoNombre != false ) {
									if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
										echo 'Si trae campo nombre<br>';
									}
									$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
									$resOpcion = $con->Consulta( $strOpcion );
									if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
										echo $strOpcion;
									}
									if ( $con->Cuantos( $resOpcion ) > 0 ) {
										$OP = $con->Resultados( $resOpcion );
										$informacion = $OP[ $campoNombre ];
									}
								}
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>';
								}
							} else {
								$informacion = 'Unassigned';
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>No se asigno valor a campo opciones<hr>';
								}
							}
						}
						$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . substr( $informacion, 0, 200 ) . ' ...</div>' ) : $informacion;
						$fila .= '<td>' . $informacion . '</td>';
					}
				} else {
					$informacion = $R[ $columnas[ $indi ] ];
					if ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) ) {
						$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
						$fila .= '<td class="tcenter">' . $informacion . '</td>';
					} else {
						if ( isset( $tipoPass ) ) {
							$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
						}
						if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
							$ligaTiene = true;
							$informacion = '<a href="' . $informacion . '" target="_blank">' . $informacion . '</a>';
						}
						if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
							$datos = $opcionesCampos[ $columnas[ $indi ] ];
							if ( $informacion > 0 ) {
								$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>Campo Opciones Actual';
									var_dump( $datos );
									echo $campoNombre . '<br>';
								}
								if ( $campoNombre != false ) {
									if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
										echo 'Si trae campo nombre<br>';
									}
									$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
									$resOpcion = $con->Consulta( $strOpcion );
									if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
										echo $strOpcion;
									}
									if ( $con->Cuantos( $resOpcion ) > 0 ) {
										$OP = $con->Resultados( $resOpcion );
										$informacion = $OP[ $campoNombre ];
									}
								}
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>';
								}
							} else {
								$informacion = 'Unassigned';
								if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
									echo '<hr>No se asigno valor a campo opciones<hr>';
								}
							}
						}
						$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . substr( $informacion, 0, 200 ) . ' ...</div>' ) : $informacion;
						$fila .= '<td>' . $informacion . '</td>';
					}
				}
			}
		}
		$fila .= '</tr>';
		$i++;
		$html .= $fila;
	}
	if( $i == 0 ) {
		$html .= '<tr><td colspan="' . ( $filas + 1 ) . '">No hay datos agregados actualmente</td></tr>';
	}
	$html .= '</tbody></table><br><br><p style="font-size:8px" align="right">Fecha de Impresión: ' . $fecha . '</p>';
	require_once 'vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->showWatermarkImage = true;
	$mpdf->WriteHTML( $html );
	$filename = limpialo( $tabla, 'min' ) . '.pdf';
	$mpdf->Output( $filename, 'I' );
?>