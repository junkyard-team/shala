<?php
	include '../querys/conexion.php';
	include '../querys/functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$fecha = strftime( '%Y-%m-%d', time() );
	$mes = array( "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
	$html = '<center><img src="logo.png" width="" style="margin: 0; left: 0; right: 0"></center>';
	$html .= '<br><p style="font-size: 16px; margin: 0;" align="right">Lista de ' . ucfirst( $_REQUEST[ 'tabla' ] ) . '</p>';
	$html .= '<br><br>';
	$html .=
	'<table style="width: 100%; border-collapse: collapse; font-family: Calibri; font-size: 11px;" border="1">
		<tr>
			<th style="color: #C00000; width: 20%;">#</th>
			<th style="color: #C00000; width:30%;">Nombre ' . ucfirst( $_REQUEST[ 'tabla' ] ) . '</th>
			<th style="color: #C00000; width: 50%;">Usuarios Relacionados</th>
		</tr>';
	$campoId = getCampoId( $_REQUEST[ 'tabla' ], $con );
	$str = 'select * from ' . $_REQUEST[ 'tabla' ] . ' order by ' . $campoId . ' asc';
	$res = $con->Consulta( $str );
	$e = 1;
	while( $R = $con->Resultados( $res ) ) {
		$html .=
		'<tr>
			<td style="text-align:center">' . $e . '</td>
			<td>' . $R[ 'nombre' ] . '</td>
			<td>';
		$str1 = 'select * from usuarios where ' . $campoId . '=' . $R[ $campoId ];
		$res1= $con->Consulta($str1);
		$i = 1;
		while( $A = $con->Resultados( $res1 ) ) {
			$html .= $i . '.- ' . $A[ 'nombre' ] . ' - ' . $A[ 'email' ] . '<br>';
			$i++;
		}
		if ( $i == 1 ) {
			$html .= 'Ninguno';
		}
		$html .=
			'</td>
		</tr>';
		$e++;
	}
	if( $e == 1 ) {
		$html .= '<tr><td colspan=3>No hay ' . ucfirst( $_REQUEST[ 'tabla' ] ) . ' registrados</td></tr>';
	}
	$html .= '</table><br><br><p style="font-size:8px" align="right">Fecha de Impresión: ' . $fecha . '</p>';
	require_once 'vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->showWatermarkImage = true;
	$mpdf->WriteHTML( $html );
	$filename = limpialo( $_REQUEST[ 'tabla' ], 'min' ) . '.pdf';
	$mpdf->Output( $filename, 'I' );
?>