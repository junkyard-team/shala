<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$campoIdPadre = ( isset( $_REQUEST[ 'padre' ] ) ) ? $_REQUEST[ 'padre' ] : '';
	$campoId = '';
	$campoNombre = '';
	$opcionesHtml = array();
	$idiomaCampo = '';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'nombre' ) {
			$campoNombre = $R[ 'columna' ];
		}
		if ( preg_match('/\bidioma\b/', $R[ 'comentarios' ] ) ) {
			$idiomaCampo = $R[ 'columna' ];
		}
	}
	$orderIdioma = ( $idiomaCampo != '' ) ? $idiomaCampo . ' asc, ' : '';
	if ( $campoIdPadre == '' ) {
		$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where clientId=' . $_REQUEST[ 'clientId' ] . ' order by ' . $orderIdioma . $campoId . ' asc';
	} else {
		$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where ' . $campoIdPadre . '=' . $_REQUEST[ 'idPadre' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] . ' order by ' . $orderIdioma . $campoId . ' asc';
	}
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$opcionesHtml[] = array( 'id' => $R[ $campoId ], 'nombre' => $R[ $campoNombre ] );
	}
	$status = array( 'status' => 'Success', 'elementos' => $opcionesHtml );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>