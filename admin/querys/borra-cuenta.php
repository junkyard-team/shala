<?php
	include '../../panel/querys/conexion.php';
	$con = new Conexion();
	$con->AbreConexion();
	$campoId = ( $_REQUEST[ 'tabla' ] == 'users' ) ? 'userId' : 'coachId';
	$res = $con->Consulta( 'select * from ' . $_REQUEST[ 'tabla' ] . ' where ' . $campoId . '=' . $_REQUEST[ 'id' ] );
	$R = $con->Resultados( $res );
	$emailParts = explode( '@', $R[ 'email' ] );
	$newEmail = $emailParts[ 0 ] . '-deactivated-' . date( 'Y-m-d' ) . '@' . $emailParts[ 1 ];
	$res = $con->Consulta( 'update ' . $_REQUEST[ 'tabla' ] . ' set statusId=1, email="' . $newEmail . '" where ' . $campoId . '=' . $_REQUEST[ 'id' ] );
	if ( $res ) {
		$status = array( 'status' => 'Success' );
	} else {
		$status = array( 'status' => 'Error' );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>