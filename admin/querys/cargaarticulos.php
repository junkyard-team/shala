<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$videosArray = array();
	$serieRequest = array();
	if ( isset( $_REQUEST[ 'articleId' ] ) ) {
		$res = $con->Consulta( 'select * from articles where articleId=' . $_REQUEST[ 'videoId' ] );
	} else {
		$res = $con->Consulta( 'select * from articles where coachId=' . $_REQUEST[ 'coachId' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] . ' order by orden asc' );
	}
	while( $R = $con->Resultados( $res ) ) {
		$videosArray[] = array(
			'id' => $R[ 'articleId' ],
			'clientId' => $R[ 'clientId' ],
			'coachId' => $R[ 'coachId' ],
			'serieId' => $R[ 'serieId' ],
			'pdf' => $R[ 'pdf' ],
			'imagen' => $R[ 'imagen' ],
			'titulo' => $R[ 'titulo' ],
			'descripcion' => ( isset( $_REQUEST[ 'articleId' ] ) ) ? strip_tags( $R[ 'description' ] ) : substr( $R[ 'description' ], 0, 200 )
		);
		$resRequest = $con->Consulta( 'select s.requestArticleId, u.* from articlerequest s inner join users u on (s.userId=u.userId) where s.articleId=' . $R[ 'articleId' ] );
		while( $S = $con->Resultados( $resRequest ) ) {
			$serieRequest[] = array(
				'id' => $S[ 'requestArticleId' ],
				'userId' => $S[ 'userId' ],
				'nombre' => $S[ 'nombre' ] . ' ' . $S[ 'apellido' ],
				'email' => $S[ 'email' ]
			);
		}
	}
	$status = array( 'status' => 'Success', 'articulos' => $videosArray, 'request' => $serieRequest );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>