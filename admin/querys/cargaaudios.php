<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$videosArray = array();
	$serieRequest = array();
	if ( isset( $_REQUEST[ 'audioId' ] ) ) {
		$res = $con->Consulta( 'select * from audios where audioId=' . $_REQUEST[ 'audioId' ] );
	} else {
		$res = $con->Consulta( 'select * from audios where coachId=' . $_REQUEST[ 'coachId' ] );
	}
	while( $R = $con->Resultados( $res ) ) {
		$videoInfo = array(
			'id' => $R[ 'audioId' ],
			'audio' => $R[ 'audio' ],
			'clientId' => $R[ 'clientId' ],
			'coachId' => $R[ 'coachId' ],
			'titulo' => $R[ 'titulo' ],
			'descripcion' => $R[ 'description' ],
			'imagen' => $R[ 'imagen' ]
		);
		$cats = array();
		$resCat = $con->Consulta( 'select c.* from audiocategories v inner join categories c on(v.categoryId=c.categoryId) where v.audioId=' . $R[ 'audioId' ] );
		while( $C = $con->Resultados( $resCat ) ) {
			$cats[] = array( 'id' => $C[ 'categoryId' ], 'nombre' => $C[ 'nombre' ] );
		}
		$videoInfo[ 'tags' ] = $cats;
		$videosArray[] = $videoInfo;
		$resAccess = $con->Consulta( 'select s.requestAudioId, u.* from audiorequest s inner join users u on (s.userId=u.userId) where s.audioId=' . $R[ 'audioId' ] );
		while( $RES = $con->Resultados( $resAccess ) ) {
			$serieRequest[] = array(
				'id' => $RES[ 'requestAudioId' ],
				'userId' => $RES[ 'userId' ],
				'nombre' => $RES[ 'nombre' ] . ' ' . $RES[ 'apellido' ],
				'email' => $RES[ 'email' ]
			);
		}
	}
	$status = array( 'status' => 'Success', 'audios' => $videosArray, 'request' => $serieRequest );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>