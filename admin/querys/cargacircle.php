<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$callArray= array();
	$serieArray = array();
	$userRequest = array();
	$coachRequest = array();
	$userApproved = array();
	$coachApproved = array();
	$res = $con->Consulta( 'select * from circles where circleId=' . $_REQUEST[ 'circleId' ] );
	$C = $con->Resultados( $res );
	$res = $con->Consulta( 'select * from coaches where coachId=' . $C[ 'coachId' ] );
	$R = $con->Resultados( $res );
	$coach = $R[ 'nombre' ] . ' ' . $R[ 'apellido' ];
	$res = $con->Consulta( 'select s.* from seriecircle sc inner join series s on(sc.serieId=s.serieId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$serieArray[] = array(
			'id' => $R[ 'serieId' ],
			'nombre' => $R[ 'nombre' ],
			'photo' => $R[ 'photo' ]
		);
	}
	$res = $con->Consulta( 'select s.* from callscircle sc inner join calls s on(sc.callId=s.callId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$callArray[] = array(
			'id' => $R[ 'callId' ],
			'nombre' => $R[ 'title' ],
			'photo' => $R[ 'image' ]
		);
	}
	$res = $con->Consulta( 'select s.* from coachcircle sc inner join coaches s on(sc.coachId=s.coachId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$coachApproved[] = array(
			'id' => $R[ 'coachId' ],
			'nombre' => $R[ 'nombre' ] . ' ' . $R[ 'apellido' ],
			'photo' => $R[ 'foto' ]
		);
	}
	$res = $con->Consulta( 'select s.* from usercircle sc inner join users s on(sc.userId=s.userId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$userApproved[] = array(
			'id' => $R[ 'userId' ],
			'nombre' => $R[ 'nombre' ] . ' ' . $R[ 'apellido' ],
			'photo' => $R[ 'foto' ]
		);
	}
	$res = $con->Consulta( 'select DISTINCT s.* from circlecoachrequest sc inner join coaches s on(sc.coachId=s.coachId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$coachRequest[] = array(
			'id' => $R[ 'coachId' ],
			'nombre' => $R[ 'nombre' ] . ' ' . $R[ 'apellido' ],
			'photo' => $R[ 'foto' ]
		);
	}
	$res = $con->Consulta( 'select DISTINCT s.* from circlerequest sc inner join users s on(sc.userId=s.userId) where circleId=' . $_REQUEST[ 'circleId' ] );
	while( $R = $con->Resultados( $res ) ) {
		$userRequest[] = array(
			'id' => $R[ 'userId' ],
			'nombre' => $R[ 'nombre' ] . ' ' . $R[ 'apellido' ],
			'photo' => $R[ 'foto' ]
		);
	}
	$status = array(
		'status' => 'Success',
		'coach' => $coach,
		'calls' => $callArray,
		'series' => $serieArray,
		'userRequest' => $userRequest,
		'coachRequest' => $coachRequest,
		'users' => $userApproved,
		'coaches' => $coachApproved,
	);
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>