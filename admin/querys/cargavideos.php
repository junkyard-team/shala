<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$videosArray = array();
	$serieRequest = array();
	if ( isset( $_REQUEST[ 'videoId' ] ) ) {
		$res = $con->Consulta( 'select * from videos where videoId=' . $_REQUEST[ 'videoId' ] );
	} else {
		$res = $con->Consulta( 'select * from videos where coachId=' . $_REQUEST[ 'coachId' ] . ' order by orden asc' );
	}
	while( $R = $con->Resultados( $res ) ) {
		$videoInfo = array(
			'id' => $R[ 'videoId' ],
			'videoId' => $R[ 'video' ],
			'clientId' => $R[ 'clientId' ],
			'coachId' => $R[ 'coachId' ],
			'video' => $R[ 'video' ],
			'titulo' => $R[ 'titulo' ],
			'descripcion' => $R[ 'description' ],
			'imagen' => $R[ 'imagen' ]
		);
		$cats = array();
		$resCat = $con->Consulta( 'select c.* from videocategories v inner join categories c on(v.categoryId=c.categoryId) where v.videoId=' . $R[ 'videoId' ] );
		while( $C = $con->Resultados( $resCat ) ) {
			$cats[] = array( 'id' => $C[ 'categoryId' ], 'nombre' => $C[ 'nombre' ] );
		}
		$videoInfo[ 'tags' ] = $cats;
		$videosArray[] = $videoInfo;
		$resAccess = $con->Consulta( 'select s.requestVideoId, u.* from videorequest s inner join users u on (s.userId=u.userId) where s.videoId=' . $R[ 'videoId' ] );
		while( $RES = $con->Resultados( $resAccess ) ) {
			$serieRequest[] = array(
				'id' => $RES[ 'requestVideoId' ],
				'userId' => $RES[ 'userId' ],
				'nombre' => $RES[ 'nombre' ] . ' ' . $RES[ 'apellido' ],
				'email' => $RES[ 'email' ]
			);
		}
	}
	$status = array( 'status' => 'Success', 'videos' => $videosArray, 'request' => $serieRequest );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>