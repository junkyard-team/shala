<?php
	$date = new DateTime( $_REQUEST[ 'fechaHora' ], new DateTimeZone( $_REQUEST[ 'currentTimeZone' ] ));
	echo $date->format('Y-m-d H:i:sP') . "\n";
	echo '<br>';
	$date->setTimezone( new DateTimeZone( 'Europe/Stockholm' ) );
	echo $date->format( 'Y-m-d H:i:sP' ) . "\n";
?>