<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$folderActivo = $_REQUEST[ 'folder' ] . '/';
	$ruta = '../../images/' . $folderActivo;
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and ( COLUMN_COMMENT="archivo" or COLUMN_KEY="PRI" ) order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	$i = 0;
	$strList = 'select ';
	$archivoTiene = false;
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
			$tipoId = $R[ 'tipoDatos' ];
		} else {
			$archivoTiene = true;
			if ( $i == 0 ) {
				$strList .= $R[ 'columna' ];
			} else {
				$strList .= ', ' . $R[ 'columna' ];
			}
			$i++;
		}
	}
	if ( $tipoId == 'int' ) {
		$strList .= ' from ' . $tabla . ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
	} else {
		$strList .= ' from ' . $tabla . ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
	}
	$resList = $con->Consulta( $strList );
	if ( $tipoId == 'int' ) {
		$str = 'delete from ' . $tabla. ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
	} else {
		$str = 'delete from ' . $tabla. ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
	}
	$res = $con->Consulta( $str );
	if ( $res ) {
		if ( $archivoTiene ) {
			while( $R = $con->Resultados( $resList ) ) {
				foreach ( $R as $key => $value ) {
					if ( !is_numeric( $key ) && !is_null( $value ) ) {
						if ( file_exists( $ruta . $value ) ) {
							unlink( $ruta . $value );
						}
					}
				}
			}
		}
		$status = array( 'status' => 'Success' );
	} else {
		$status = array( 'status' => 'Error' );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>