<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	if ( !is_null( $_REQUEST[ 'coachId' ] ) && $_REQUEST[ 'coachId' ] != '' ) {
		$res = $con->Consulta( 'select * from cambioperfil where coachId=' . $_REQUEST[ 'coachId' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
		if ( $con->Cuantos( $res ) > 0 ) {
			$str = 'update coachesclients set pending="S" where clientId=' . $_REQUEST[ 'clientId' ] . ' and coachId=' . $_REQUEST[ 'coachId' ];
			$res = $con->Consulta( $str );
			if ( $res ) {
				$res = $con->Consulta( 'delete from cambioperfil where coachId=' . $_REQUEST[ 'coachId' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
				$mail = avisoPendingApprovalCoach( $_REQUEST[ 'coachId' ], $_REQUEST[ 'clientId' ], $con );
				$status = array( 'status' => 'Success', 'mail' => $mail );
			} else {
				$status = array( 'status' => 'Error' );
			}
		} else {
			$status = array( 'status' => 'Cambios' );
		}
	} else {
		$res = $con->Consulta( 'select * from cambioperfilmiembro where userId=' . $_REQUEST[ 'studentId' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
		if ( $con->Cuantos( $res ) > 0 ) {
			$str = 'update userclient set pending="S" where clientId=' . $_REQUEST[ 'clientId' ] . ' and userId=' . $_REQUEST[ 'studentId' ];
			$res = $con->Consulta( $str );
			if ( $res ) {
				$res = $con->Consulta( 'delete from cambioperfilmiembro where userId=' . $_REQUEST[ 'studentId' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
				$mail = avisoPendingApprovalMember( $_REQUEST[ 'studentId' ], $_REQUEST[ 'clientId' ], $con );
				$status = array( 'status' => 'Success', 'mail' => $mail );
			} else {
				$status = array( 'status' => 'Error' );
			}
		} else {
			$status = array( 'status' => 'Cambios' );
		}
	}
	echo json_encode( $status );
	$con->CierraConexion();
?>