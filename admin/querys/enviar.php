<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$body = '<h2>Contact Request from Site</h2><br><table><tr><th>Field</th><th>Value</th></tr>';
	foreach ($_REQUEST as $param_name => $param_val) {
		if ( strpos( $param_name, '_' ) == false && strpos( $param_name, 'PHP' ) == false ) {
			if ( $param_name != 'clientId' ) {
				$valor = ( $param_val == 'undefined' ) ? '' : $param_val;
				$body .= '<tr><td>' . strtoupper( $param_name ) . '</td><td>' . $valor . '</td></tr>';
			}
		}
	}
	$correoManda = '';
	$str = 'select email from clients where clientId=' . $_REQUEST[ 'clientId' ];
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$correoManda = $R[ 'email' ];
	}
	$textos = getEmailText( 2, $_REQUEST[ 'clientId' ], $con );
	$statusMail = mandaMail( $correoManda, 'Contact Shala', $textos[ 'titulo' ], $textos[ 'mensaje' ] . $body, $_REQUEST[ 'clientId' ] , $con );
	if ( $statusMail == 'Enviado' ) {
		$status = array( 'status' => 'Success' );
	} else {
		$status = array( 'status' => 'Error', 'error' => $statusMail );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>