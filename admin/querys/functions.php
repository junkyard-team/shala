<?php
	function encripta( $q ) {
		$qEncoded = encriptaDesencripta( 'encrypt', $q );
		return( $qEncoded );
	}
	function desencripta( $q ) {
		$qDecoded = encriptaDesencripta( 'decrypt', $q );
		return( $qDecoded );
	}
	function encriptaDesencripta( $action, $string ) {
		$output = false;
		$encrypt_method = 'AES-256-CBC';
		$secret_key = 'qJB0rGtIn5UB1xG03efyCp';
		$secret_iv = 'qJB0rGtIn5UB1xG03efyCpqJB0rGtIn5UB1xG03efyCp';
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
		if ( $action == 'encrypt' ) {
			$output = openssl_encrypt( $string, $encrypt_method, $key, 0, $iv );
			$output = base64_encode( $output );
		} else if ( $action == 'decrypt' ) {
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}
		return $output;
	}
	function mesTexto( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'Enero';
			case '02': return 'Febrero';
			case '03': return 'Marzo';
			case '04': return 'Abril';
			case '05': return 'Mayo';
			case '06': return 'Junio';
			case '07': return 'Julio';
			case '08': return 'Agosto';
			case '09': return 'Septiembre';
			case '10': return 'Octubre';
			case '11': return 'Noviembre';
			case '12': return 'Diciembre';
			default: return '--';
		}
	}
	function monthText( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'January';
			case '02': return 'February';
			case '03': return 'March';
			case '04': return 'April';
			case '05': return 'May';
			case '06': return 'June';
			case '07': return 'July';
			case '08': return 'August';
			case '09': return 'September';
			case '10': return 'Octuber';
			case '11': return 'November';
			case '12': return 'December';
			default: return '--';
		}
	}
	function moisText( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'janvier';
			case '02': return 'février';
			case '03': return 'mars';
			case '04': return 'avril';
			case '05': return 'mai';
			case '06': return 'juin';
			case '07': return 'juillet';
			case '08': return 'août';
			case '09': return 'septembre';
			case '10': return 'octobre';
			case '11': return 'novembre';
			case '12': return 'décembre';
			default: return '--';
		}
	}
	function formatoFechaTiempo( $fecha, $lang ) {
		$elementos = explode( ' ', $fecha );
		$mifecha = explode( '-', $elementos[0] );
		$miHora = explode( ':', $elementos[1] );
		$hora24 = ( $miHora[ 0 ] <= 12 ) ? $miHora[ 0 ] : ( 12 - $miHora[ 0 ] );
		$amPM = ( $miHora[ 0 ] < 12 ) ? 'AM' : 'PM';
		if ( $lang == 'ES' ) {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = mesTexto( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ] . ' - ' . $hora24 . ':' . $miHora [ 1 ] . ' ' . $amPM;
				return $lafecha;
			}
		} else if ( $lang == 'FR' ) {
			if ( moisText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = moisText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ] . ' - ' . $hora24 . ':' . $miHora [ 1 ] . ' ' . $amPM;
				return $lafecha;
			}
		} else {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = monthText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ] . ' - ' . $hora24 . ':' . $miHora [ 1 ] . ' ' . $amPM;
				return $lafecha;
			}
		}
	}
	function formatoFecha( $fecha, $lang ) {
		$mifecha = explode( '-', $fecha );
		if ( $lang == 'ES' ) {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = mesTexto( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		} else if ( $lang == 'FR' ) {
			if ( moisText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = moisText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		} else {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = monthText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		}
	}
	function obtenHora( $fecha ) {
		ereg( '([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})', $fecha, $hora );
		$lahora = $hora[ 2 ] . ':' . $hora[ 3 ];
		return $lahora;
	}
	function formatoFechaHora( $arg ) {
		$fecha = $arg;
		ereg( '([0-9]{2,2})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})', $fecha, $mifecha );
		$fechahora = $mifecha[ 3 ] . '.' . mesTexto( $mifecha[ 2 ] ) . '.' . $mifecha[ 1 ] . ' ' . $mifecha[ 4 ] . ':' . $mifecha[ 5 ];
		if ( mesTexto( $mifecha[ 2 ] ) == 'Err' ) {
			return '--';
		} else {
			return $fechahora;
		}
	}
	function limpialo( $cadena, $tipo ) {
		$cadenaLimpia = preg_replace( '[\s+]','-', trim( stripslashes( $cadena ) ) );
		$cadenaLimpia = str_replace( ' ', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '#', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '@', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '*', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( ',', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '&', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '=', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '(', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( ')', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '.', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '’', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( "''", '', $cadenaLimpia );
		$cadenaLimpia = str_replace( "'", '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '”', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( 'ñ', 'n', $cadenaLimpia );
		$cadenaLimpia = str_replace( 'Ñ', 'N', $cadenaLimpia );
		$cadenaLimpia = str_replace( '"', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '/', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '�','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '¿','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '?','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '+','-', $cadenaLimpia );
		$cadenaLimpia = str_replace( ' ', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '°', '', $cadenaLimpia );
		$cadenaLimpia = str_replace(
			array( 'á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú' ),
			array( 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' ),
			$cadenaLimpia
		);
		$cadenaLimpia = str_replace( '--', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '--', '-', $cadenaLimpia );
		$cadenaLimpia = ( $tipo == 'min' ) ? strtolower( $cadenaLimpia ) : ( ( $tipo == 'may' ) ? strtoupper( $cadenaLimpia ) : $cadenaLimpia );
		$cadenaLimpia = mb_substr( $cadenaLimpia, 0, 490, 'UTF-8' );
		return $cadenaLimpia;
	}
	$palabrasReservadas = array( 'archivo', 'html', 'activar', 'nombre', 'fecha', 'pass', 'editable', 'rfc', 'email', 'idioma', 'icono', 'liga', 'color', 'hoy', 'gps', 'posicion', 'mapa', 'dibujo', 'spotlightr', 'fontSize' );
	function armaOpciones( $tabla, $clientId, $con ) {
		$campoId = '';
		$campoNombre = '';
		$html = '';
		$tieneClientId = false;
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'S' ) {
				$campoId = $R[ 'columna' ];
			}
			if ( $R[ 'comentarios' ] == 'nombre' ) {
				$campoNombre = $R[ 'columna' ];
			}
			if ( $R[ 'columna' ] == 'clientId' ) {
				$tieneClientId = true;
			}
		}
		if ( $campoId != '' && $campoNombre != '' ) {
			if ( $tabla != 'coaches' && $tabla != 'series' && $tabla != 'users' ) {
				if ( $tieneClientId ) {
					$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where clientId=' . $clientId . ' order by ' . $campoId . ' asc';
				} else {
					$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' order by ' . $campoId . ' asc';
				}
			} else if ( $tabla == 'coaches' ) {
				$str = 'select DISTINCT c.* from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
			} else if ( $tabla == 'users' ) {
				$str = 'select DISTINCT c.* from users c inner join userclient cc on(c.userId=cc.userId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
			} else if ( $tabla == 'series' ) {
				$coaches = array();
				$str = 'select DISTINCT c.coachId from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
				$res = $con->Consulta( $str );
				while( $R = $con->Resultados( $res ) ) {
					$coaches[] = $R[ 'coachId' ];
				}
				$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where coachId in(' . implode( ',', $coaches ) . ') order by ' . $campoId . ' asc';
			}
			$res = $con->Consulta( $str );
			while( $R = $con->Resultados( $res ) ) {
				$html .= '<option value="' . $R[ $campoId ] . '">' . palabraDiccionario( $R[ $campoNombre ], $con ) . '</option>';
			}
		}
		return $html;
	}
	function armaListaCheckbox( $tabla, $clientId, $con ) {
		$campoId = '';
		$campoNombre = '';
		$html = '';
		$tieneClientId = false;
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'S' ) {
				$campoId = $R[ 'columna' ];
			}
			if ( $R[ 'comentarios' ] == 'nombre' ) {
				$campoNombre = $R[ 'columna' ];
			}
			if ( $R[ 'columna' ] == 'clientId' ) {
				$tieneClientId = true;
			}
		}
		if ( $campoId != '' && $campoNombre != '' ) {
			if ( $tabla != 'coaches' && $tabla != 'series' && $tabla != 'users' ) {
				if ( $tieneClientId ) {
					$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where clientId=' . $clientId . ' order by ' . $campoId . ' asc';
				} else {
					$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' order by ' . $campoId . ' asc';
				}
			} else if ( $tabla == 'coaches' ) {
				$str = 'select DISTINCT c.* from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
			} else if ( $tabla == 'users' ) {
				$str = 'select DISTINCT c.* from users c inner join userclient cc on(c.userId=cc.userId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
			} else if ( $tabla == 'series' ) {
				$coaches = array();
				$str = 'select DISTINCT c.coachId from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where cc.clientId=' . $clientId . ' order by c.apellido asc, c.nombre asc';
				$res = $con->Consulta( $str );
				while( $R = $con->Resultados( $res ) ) {
					$coaches[] = $R[ 'coachId' ];
				}
				$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where coachId in(' . implode( ',', $coaches ) . ') order by ' . $campoId . ' asc';
			}
			$res = $con->Consulta( $str );
			while( $R = $con->Resultados( $res ) ) {
				$html .= '<li><input type="checkbox" class="opcionMultiple" tabla="' . $tabla . '" value="' . $R[ $campoId ] . '"> ' . palabraDiccionario( $R[ $campoNombre ], $con ) . '</li>';
			}
		}
		return $html;
	}
	function getCampoId( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_KEY = "PRI"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function getCampoNombre( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT="nombre"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function getCampoPosicion( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT = "posicion"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function armaTabla( $tabla, $con ) {
		global $palabrasReservadas;
		$html = '<th class="tcenter">Action</th><th class="tcenter">ID</th>';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by ORDINAL_POSITION asc';
		$res = $con->Consulta( $str );
		$campoPosicion = getCampoPosicion( $tabla, $con );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' && ( $campoPosicion == false || $campoPosicion != $R[ 'columna' ] ) ) {
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					if ( $R[ 'columna' ] == 'emailConfirmed' ) {
						$html .= '<th>Email confirmed</th>';
					} else {
						$titulo = palabraDiccionario( $R[ 'columna' ], $con );
						$titulo = ( trim( $titulo ) == 'Date' && $R[ 'comentarios' ] == 'hoy' ) ? 'Date Created' : $titulo;
						$html .= '<th>' . ucfirst( $titulo ). '</th>';
					}
				} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
					$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
					$titulo = palabraDiccionario( $tablaCatalogo, $con );
					$html .= '<th>' . ucfirst( $titulo ). '</th>';
				}
			}
		}
		if ( $campoPosicion != false ) {
			$html .= '<th>Order</th>';
		}
		if ( $tabla == 'coaches' || $tabla == 'users' ) {
			$html .= '<th>Status</th>';
		}
		echo $html;
	}
	function armaDetalle( $tabla, $clientId, $con ) {
		global $palabrasReservadas;
		$camposHtml = '';
		$comboMultiple = '';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"oculto"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' ) {
				$claseObligatorio = ( $R[ 'obligatoria' ] == 'S' ) ? 'obligatorio' : '';
				$asterisko = ( $R[ 'obligatoria' ] == 'S' ) ? ' *' : '';
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					$titulo = palabraDiccionario( $R[ 'columna' ], $con );
					$titulo = ( trim( $titulo ) == 'Date' && $R[ 'comentarios' ] == 'hoy' ) ? 'Date Created' : $titulo;
					if ( $R[ 'tamano' ] > 200 || ( $R[ 'comentarios' ] == 'archivo' || $R[ 'comentarios' ] == 'spotlightr' ) ) {
						$camposHtml .=
						'<div class="col-lg-12">
							<div class="section row">
								<label for="inputStandard" class="control-label">' . ucfirst( $titulo ) . '</label>
								<div id="' . $R[ 'columna' ] . '" comentarios="' . $R[ 'comentarios' ] . '" class="divInput"><br></div>
							</div>
						</div>';
					} else {
						$camposHtml .=
						'<div class="col-lg-6">
							<div class="section row">
								<label for="inputStandard" class="control-label">' . ucfirst( $titulo ) . '</label>
								<div id="' . $R[ 'columna' ] . '" comentarios="' . $R[ 'comentarios' ] . '" class="divInput"><br></div>
							</div>
						</div>';
					}
				} else {
					if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
						$titulo = palabraDiccionario( $R[ 'columna' ], $con );
						$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
						$camposHtml .=
						'<div class="col-lg-6">
							<div class="section row">
								<label for="inputStandard" class="control-label">' . ucfirst( $titulo ) . '</label>
								<select id="' . $R[ 'columna' ] . '" disabled>
									<option value="-1">Pick an option</option>
									' . armaOpciones( $tablaCatalogo, $clientId, $con ) . '
								</select>
							</div>
						</div>';
					}
				}
			} else {
				if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
					$comentarioLimpio = str_replace( 'editable;', '', $R[ 'comentarios' ] );
					$variosMultiples = explode( ';', $comentarioLimpio );
					for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
						if ( $variosMultiples[ $i ] != '' ) {
							$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
							$tablas = explode( ',', $infoTablas );
							$tablaCatalogo = $tablas[0];
							$tablaDestino = $tablas[1];
							$titulo = palabraDiccionario( $tablaCatalogo, $con );
							$titulo = ( $tabla == 'sucursal' ) ? 'Negocios Listado Inferior' : $titulo;
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="inputStandard" class="control-label">' . ucfirst( $titulo ) . '</label>
									<select multiple="multiple" destino="' . $tablaDestino . '" class="select2-multiple form-control select-primary" id="multiple-' . $i . '" disabled>
										' . armaOpciones( $tablaCatalogo, $clientId, $con ) . '
									</select>
								</div>
							</div>';
						}
					}
				}
			}
		}
		if ( $tabla == 'coaches' ) {
			$camposHtml .=
			'<div class="col-lg-6 ">
				<div class="section row">
					<label for="inputStandard" class="control-label">Application Status</label>
					<div class="col-lg-12 admin-form">
						<label class="field select">
							<select id="pending" onchange="ajustaStatusUnico( this.value )">
								<option value="N">Approve</option>
								<option value="S">Pending</option>
								<option value="R">Reject</option>
							</select>
							<i class="arrow"></i>
						</label>
					</div>
				</div>
			</div>';
		}
		echo $camposHtml . $comboMultiple;
	}
	function armaFormulario( $tabla, $clientId, $con ) {
		global $palabrasReservadas;
		$camposHtml = '';
		$comboMultiple = '';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and ( COLUMN_COMMENT<>"hoy" and COLUMN_COMMENT<>"oculto" )';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' ) {
				$claseObligatorio = ( $R[ 'obligatoria' ] == 'S' ) ? 'obligatorio' : '';
				$asterisko = ( $R[ 'obligatoria' ] == 'S' ) ? ' *' : '';
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					if ( $R[ 'comentarios' ] != 'idioma' ) {
						$titulo = palabraDiccionario( $R[ 'columna' ], $con );
						if ( $R[ 'comentarios' ] == 'activar' ) {
							$titulo = ( $R[ 'columna' ] == 'estado' ) ? 'Autorizado' : $titulo;
							$tipoActiva = ( $R[ 'tipoDatos' ] == 'char' ) ? 'SN' : 'TF';
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="inputStandard" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field tcenter" style="padding-top: 8px;">
											<label class="switch block">
												<input type="checkbox" id="' . $R[ 'columna' ] . '" checked="" tipo="' . $tipoActiva . '">
												<label for="' . $R[ 'columna' ] . '" data-on="YES" data-off="NO"></label>
											</label>
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'archivo' || $R[ 'comentarios' ] == 'spotlightr' ) {
							$camposHtml .=
							'<div class="col-lg-12">
								<div class="form-group mb10 row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-2 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-8">
										<label class="field prepend-icon append-button file" for="imagen"><span class="button btn-primary">' . ucfirst( $titulo ) . '</span>
											<form contiene="' . $R[ 'columna' ] . '">
												<input type="file" id="' . $R[ 'columna' ] . '" onchange="$( \'#uploader-' . $R[ 'columna' ] . '\' ).val( this.value );" class="gui-file ' . $claseObligatorio . ' ' . $R[ 'comentarios' ] . '">
												<input id="uploader-' . $R[ 'columna' ] . '" placeholder="Choose a file" class="gui-input noPelar" type="text" style="text-align: right;">
												<label class="field-icon"><i class="fa fa-upload"></i></label>
											</form>
										</label>
									</div>
									<div class="col-lg-2 capaimg showImg" columna="' . $R[ 'columna' ] . '" obligado="' . $claseObligatorio . '"></div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'html' ) {
							$camposHtml .=
							'<div class="col-lg-12">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<div style="height: 400px;" class="summernote" id="' . $R[ 'columna' ] . '"></div>
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'fecha' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' fecha">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'color' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field sfcolor">
											<input id="' . $R[ 'columna' ] . '" type="text" placeholder="Choose a color" class="gui-input color ' . $claseObligatorio . '" onchange="seteaToHex( $( this ) )">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'icono' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . '" readonly onclick="muestraIcono( \'' . $R[ 'columna' ] . '\' )">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'rfc' || $R[ 'comentarios' ] == 'email' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' ' . $R[ 'comentarios' ] . '" maxlength="' . $R[ 'tamano' ]. '">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'mapa' ) {
							$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<textarea id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" rows="2" class="form-control ' . $claseObligatorio . ' buscaMapa" latitud="" longitud="" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '></textarea>
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="section row">
										<div style="height: 200px;" class="googleMaps" id="mapa-' . $R[ 'columna' ] . '" campoMapa="' . $R[ 'columna' ] . '"></div>
									</div>
								</div>';
						} else if ( $R[ 'comentarios' ] == 'pass' ) {
							$camposHtml .=
								'<div class="col-lg-6">
									<div class="section row nuevoPass">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' pass" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '>
											</label>
										</div>
									</div>
									<div class="section row oldPass">
										<label for="liga-' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<a onclick="" id="liga-' . $R[ 'columna' ] . '">Re-send Password</a>
										</div>
									</div>
								</div>';
						} else if ( $R[ 'comentarios' ] == 'fontSize' ) {
							$opcionesFont = '';
							for ( $i = 10; $i <= 60; $i++ ) { $opcionesFont .= '<option value="' . $i . 'px">' . $i . 'px</option>'; $i++; }
							$camposHtml .=
							'<div class="col-lg-6" id="contiene-' . $R[ 'columna' ] . '">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . '</label>
									<div class="col-lg-12">
										<label class="field select">
											<select id="' . $R[ 'columna' ] . '">
												<option value="">Pick an option</option>
												' . $opcionesFont . '
											</select>
											<i class="arrow"></i>
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'Dibujo' ) {
							$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<a href="#" class="noHref borraDibujo" onclick="borraDibujo( \'' . $R[ 'columna' ] . '\' )">Delete ' . ucfirst( $titulo ) . '</a>
										<canvas id="' . $R[ 'columna' ] . '" class="dibujaCanvas ' . $claseObligatorio . '" creado="0" width="512" height="200"></canvas>
									</div>
								</div>';
						} else {
							$funcionOn = ( $R[ 'tipoDatos' ] == 'int' || $R[ 'tipoDatos' ] == 'decimal' ) ? 'onkeypress="return soloNumeros( event );"' : '';
							if ( $R[ 'tamano' ] > 200 ) {
								$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<textarea id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" rows="4" class="form-control ' . $claseObligatorio . '" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '></textarea>
											</label>
										</div>
									</div>
								</div>';
							} else {
								$camposHtml .=
								'<div class="col-lg-6">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . '" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '>
											</label>
										</div>
									</div>
								</div>';
							}
						}
					}
				} else {
					if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
						$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
						$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tablaCatalogo );
						$titulo = ( $tabla == 'sucursal' && $tablaCatalogo == 'negocio' ) ? 'Negocio Slide Principal' : $titulo;
						$titulo = ( $tabla == 'miembros' && $tablaCatalogo == 'tipoviajero' ) ? 'Tipo de Viajero' : $titulo;
						$titulo = palabraDiccionario( $titulo, $con );
						$camposHtml .=
						'<div class="col-lg-6" id="contiene-' . $R[ 'columna' ] . '">
							<div class="section row">
								<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
								<div class="col-lg-12">
									<label class="field select">
										<select id="' . $R[ 'columna' ] . '" class="' . $claseObligatorio . ' opcion">
											<option value="-1">Pick an option</option>
											' . armaOpciones( $tablaCatalogo, $clientId, $con ) . '
										</select>
										<i class="arrow"></i>
									</label>
								</div>
							</div>
						</div>';
					}
				}
			} else {
				if ( preg_match('/\beditable\b/', $R[ 'comentarios' ] ) ) {
					$titulo = palabraDiccionario( $R[ 'columna' ], $con );
					$camposHtml .=
					'<div class="col-lg-12">
						<div class="section row">
							<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . ' *</label>
							<div class="col-lg-12">
								<label class="field">
									<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" class="gui-input oblitagorio" maxlength="' . $R[ 'tamano' ]. '">
									<span style="font-size: 10px;">Verifique esta información al crearla ya que no podra ser editada</span>
								</label>
							</div>
						</div>
					</div>';
				} else {
					$camposHtml .= '<input type="hidden" id="' . $R[ 'columna' ] . '" class="id" value="0">';
				}
				if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
					$comentarioLimpio = str_replace( 'editable;', '', $R[ 'comentarios' ] );
					$variosMultiples = explode( ';', $comentarioLimpio );
					for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
						if ( $variosMultiples[ $i ] != '' ) {
							$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
							$tablas = explode( ',', $infoTablas );
							$tablaCatalogo = $tablas[0];
							$tablaDestino = $tablas[1];
							$titulo = palabraDiccionario( $tablaCatalogo, $con );
							$titulo = ( $tabla == 'sucursal' ) ? 'Negocios Listado Inferior' : $titulo;
							/*if ( isset( $_REQUEST[ 'tipo' ] ) && $_REQUEST[ 'tipo' ] == 3 ) {
								$comboMultiple .=
								'<div class="col-lg-6">
									<ul class="eligeMultiple">
										' . armaListaCheckbox( $tablaCatalogo, $clientId, $con ) . '
									</ul>
								</div>';
							} else {*/
								$comboMultiple .=
								'<div class="col-lg-6">
									<div class="section row">
										<label for="multiple-' . $i . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . '</label>
										<div class="col-lg-12">
											<select multiple="multiple" destino="' . $tablaDestino . '" class="select2-multiple form-control select-primary obligatorio" id="multiple-' . $i . '">
												' . armaOpciones( $tablaCatalogo, $clientId, $con ) . '
											</select>
										</div>
									</div>
								</div>';
							//}
						}
					}
				}
			}
		}
		echo $camposHtml . $comboMultiple;
	}
	function armaAcordeones( $tabla, $con ) {
		global $palabrasReservadas;
		$grupoAnterior = '';
		$i = 0;
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
				if ( !in_array( $R[ 'comentarios' ], $palabrasReservadas ) && $R[ 'comentarios' ] != '' && !preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) && !preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) && !preg_match('/\bimagenes\b/', $R[ 'comentarios' ] ) ) {
					if ( $grupoAnterior != $R[ 'comentarios' ] ) {
						$grupoAnterior = $R[ 'comentarios' ];
						$i = 0;
					}
					$miembros[ $grupoAnterior ][ $i ] = array( 'campo' => $R[ 'columna' ], 'tipo' => $R[ 'tipoDatos' ], 'tamano' => $R[ 'tamano' ] );
					$i++;
				}
			}
		}
		if ( isset( $miembros ) ) {
			foreach ( $miembros as $grupo => $elementos ) {
				$miembroId = limpialo( $grupo, 'min' );
				$html =
				'<div id="' . $miembroId . '" class="panel-group accordion">
					<div class="panel">
						<div class="panel-heading"><a data-toggle="collapse" data-parent="#' . $miembroId . '" href="#' . $miembroId . '-cont" class="accordion-toggle accordion-icon link-unstyled collapsed" aria-expanded="false">' . $grupo . '</a></div>
						<div id="' . $miembroId . '-cont" class="panel-collapse collapse" aria-expanded="false">
							<div class="panel-body">
								<div role="form" class="admin-form" id="' . $miembroId . 'Info">';
								for ( $i = 0; $i < sizeof( $elementos ); $i++ ) {
									$funcionOn = ( $elementos[ $i ][ 'tipo' ] == 'int' || $elementos[ $i ][ 'tipo' ] == 'decimal' ) ? 'onkeypress="return soloNumeros( event );"' : '';
									$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $elementos[ $i ][ 'campo' ] );
									$isFecha = ( $elementos[ $i ][ 'tipo' ] == 'date' || $elementos[ $i ][ 'tipo' ] == 'datetime' || $elementos[ $i ][ 'tipo' ] == 'timestamp' ) ? 'fecha' : '';
									$html .=
									'<div class="form-group row">
										<label for="' . $elementos[ $i ][ 'campo' ] . '" class="col-lg-3 control-label">' . ucfirst( $titulo ) . '</label>
										<div class="col-lg-9">
											<label class="field">
												<input type="text" id="' . $elementos[ $i ][ 'campo' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input alineado ' . $isFecha . '" ' . $funcionOn . ' maxlength="' . $elementos[ $i ][ 'tamano' ]. '">
											</label>
										</div>
									</div>';
								}
								$html .=
								'</div>
							</div>
						</div>
					</div>
				</div>';
			}
			echo $html;
		}
	}
	function mandaMailNew( $email, $nombre, $asunto, $html, $clientId, $con ) {
		if ( !class_exists( 'PHPMailer' ) ) {
			include 'phpmailer/src/Exception.php';
			include 'phpmailer/src/PHPMailer.php';
			include 'phpmailer/src/SMTP.php';
			include 'phpmailer/src/OAuth.php';
		}
		$dominio = 'shala.us';
		$res1 = $con->Consulta( 'select * from clients where clientId=' . $clientId );
		$T = $con->Resultados( $res1 );
		$body =
		'<style> @import url( "https://fonts.googleapis.com/css?family=Montserrat:400,700" );</style>
		<div style="font-family: \'Montserrat\', sans-serif !important;" text-align: center; background-color: #eef0f3; color: #7F878D; margin-top: 0px!important; margin-bottom: 0px!important; margin-right: 0px!important; margin-left: 0px!important; padding-top: 0px; padding-bottom: 0px; padding-right: 0px; padding-left: 0px; font-size: 14px;">
			<center style="width: 100%; table-layout: fixed; background-color: #eef0f3">
				<br>
				<div style="padding: 5px 10px; background-color: #ccc; color: #000; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%;" width="600">
					<table border="0" width="100%" style="width: 100%; background-color: #ccc; color: #000;padding: 8px;">
						<tr valign="middle" style="background-color: #ccc; color: #000;padding: 8px;">
							<td align="center" style="text-align: center; background-color: #ccc; color: #000; padding: 8px;"><img src="http://' . $T[ 'url' ] . '.' . $dominio . '/images/clients/' . $T[ 'logotipo' ] . '" width="100" style="width: 100px;"></td>
						</tr>
					</table>
				</div>
				<div style="background-color: #fff; color: #000; text-align: left; padding: 10px; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%; font-size: 14px;" width="600">' . $html . '</div>
				<br>
			</center>
		</div>';
		if ( !isset( $mail ) ) {
			$mail = new PHPMailer;
			$mail->SMTPDebug = 2;
        	$mail->isSMTP(true);
        	$mail->Mailer = 'smtp';
        	$mail->SMTPAuth = true;
        	$mail->SMTPSecure = "ssl";
        	$mail->Port = 465;
        	$mail->Host = "smtp.mailgun.org";
        	$mail->Username = 'info@shala.us';
        	$mail->Password = 'af20c9cbf5679324c647f9aa760c384b-054ba6b6-8e636d53';
		}
		$mail->SetFrom( 'info@shala.us', $T[ 'nombre' ] );
		$mail->Subject = $asunto;
		$mail->AddAddress( $email, $nombre );
		$mail->AddReplyTo( $T[ 'email' ], $T[ 'nombre' ] );
		$mail->AddBCC( 'elopez@junkyard.mx' );
		$mail->addCustomHeader(
			'X-Mailer: ' . $dominio . '/ PHP/' . phpversion(),
			'Message-ID: <' . gmdate( 'YmdHs' ) . '@' . $dominio . '/>',
			'Sender: ' . $dominio . '/',
			'Sent: ' . date( 'd-m-Y' ),
			'Organization: Shala',
			'Content-Transfer-encoding: 8bit'
		);
		$mail->AddCC( $T[ 'email' ] );
		$mail->Body = $body;
		$mail->IsHTML( true );
		$mail->CharSet = 'UTF-8';
		$mail->AltBody = $asunto;
		if ( $mail -> Send() ) {
			$status = 'Enviado';
		} else {
			$status = $mail->ErrorInfo;
		}
		return $status;
	}
	function mandaMail( $email, $nombre, $asunto, $html, $clientId, $con ) {
		if ( !class_exists( 'PHPMailer' ) ) { include 'class.phpmailer.php'; }
		$dominio = 'shala.us';
		$res1 = $con->Consulta( 'select * from clients where clientId=' . $clientId );
		$T = $con->Resultados( $res1 );
		$body =
		'<style> @import url( "https://fonts.googleapis.com/css?family=Montserrat:400,700" );</style>
		<div style="font-family: \'Montserrat\', sans-serif !important;" text-align: center; background-color: #eef0f3; color: #7F878D; margin-top: 0px!important; margin-bottom: 0px!important; margin-right: 0px!important; margin-left: 0px!important; padding-top: 0px; padding-bottom: 0px; padding-right: 0px; padding-left: 0px; font-size: 14px;">
			<center style="width: 100%; table-layout: fixed; background-color: #eef0f3">
				<br>
				<div style="padding: 5px 10px; background-color: #ccc; color: #000; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%;" width="600">
					<table border="0" width="100%" style="width: 100%; background-color: #ccc; color: #000;padding: 8px;">
						<tr valign="middle" style="background-color: #ccc; color: #000;padding: 8px;">
							<td align="center" style="text-align: center; background-color: #ccc; color: #000; padding: 8px;"><img src="http://' . $T[ 'url' ] . '.' . $dominio . '/images/clients/' . $T[ 'logotipo' ] . '" width="100" style="width: 100px;"></td>
						</tr>
					</table>
				</div>
				<div style="background-color: #fff; color: #000; text-align: left; padding: 10px; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%; font-size: 14px;" width="600">' . $html . '</div>
				<br>
			</center>
		</div>';
		$mail = new PHPMailer();
		$mail->Host = $dominio . "/";
		$mail->From = 'no-reply@' . $dominio;
		$mail->FromName = $T[ 'nombre' ];
		$mail->Subject = $asunto;
		$mail->AddAddress( $email, $nombre );
		$mail->AddReplyTo( $T[ 'email' ], $T[ 'nombre' ] );
		$mail->AddBCC( 'elopez@junkyard.mx' );
		$mail->addCustomHeader(
			'X-Mailer: ' . $dominio . '/ PHP/' . phpversion(),
			'Message-ID: <' . gmdate( 'YmdHs' ) . '@' . $dominio . '/>',
			'Sender: ' . $dominio . '/',
			'Sent: ' . date( 'd-m-Y' ),
			'Organization: Shala',
			'Content-Transfer-encoding: 8bit'
		);
		$mail->AddCC( $T[ 'email' ] );
		$mail->Body = $body;
		$mail->IsHTML( true );
		$mail->CharSet = 'UTF-8';
		$mail->AltBody = $asunto;
		if ( $mail -> Send() ) {
			$status = 'Enviado';
		} else {
			$status = $mail->ErrorInfo;
		}
		return $status;
	}
	function archivoCorrecto( $archivo, $rutaArchivo ) {
		if ( !is_null( $archivo ) && $archivo != '' && file_exists( $rutaArchivo ) ) {
			return true;
		} else {
			return false;
		}
	}
	function palabraDiccionario( $palabra, $con ) {
		$texto = $palabra;
		$Query = $con->Consulta( 'select * from diccionario' );
		while( $U = $con->Resultados( $Query ) ) {
			if ( strtoupper( $palabra ) == strtoupper( $U[ 'campo' ] ) ) {
				$texto = $U[ 'etiqueta' ];
			}
		}
		$texto = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $texto );
		return $texto;
	}
	function obtenProductoId( $url, $tipo, $con ) {
		$productoId = 0;
		$strQuery = 'select * from productos';
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'nombre' ], 'min' ) == $url ) {
				$productoId = $U[ 'idproducto' ];
			}
		}
		return $productoId;
	}
	function obtenEmbedUrl( $url ) {
		$finalUrl = $url;
		if ( strpos( $url, 'facebook.com' ) !== false ) {
			$finalUrl = 'https://www.facebook.com/plugins/video.php?href=' . rawurlencode( $url ) . '&show_text=1&width=200';
		} else if ( strpos( $url, 'vimeo.com' ) !== false ) {
			$videoId = explode( 'vimeo.com/', $url )[1];
			if ( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://player.vimeo.com/video/' . $videoId;
		} else if( strpos( $url, 'youtube.com' ) !== false ) {
			$videoId = explode( 'v=',$url )[1];
			if( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
		} else if ( strpos( $url, 'youtu.be' ) !== false ) {
			$videoId = explode( 'youtu.be/', $url )[1];
			if ( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
		}
		return $finalUrl;
	}
	function obtenCodigoVideo( $url ) {
		$finalUrl = $url;
		if ( strpos( $url, 'iframe' ) == false ) {
			if ( strpos( $url, 'facebook.com' ) !== false ) {
				$finalUrl = '<iframe src="https://www.facebook.com/plugins/video.php?href=' . rawurlencode( $url ) . '&show_text=1&width=200"></iframe>';
			} else if ( strpos( $url, 'vimeo.com' ) !== false ) {
				$videoId = explode( 'vimeo.com/', $url )[1];
				if ( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = '<iframe src="https://player.vimeo.com/video/' . $videoId . '"></iframe>';
			} else if( strpos( $url, 'youtube.com' ) !== false ) {
				$videoId = explode( 'v=',$url )[1];
				if( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = '<iframe src="https://www.youtube.com/embed/' . $videoId . '"></iframe>';
			} else if ( strpos( $url, 'youtu.be' ) !== false ) {
				$videoId = explode( 'youtu.be/', $url )[1];
				if ( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = '<iframe src="https://www.youtube.com/embed/' . $videoId . '"></iframe>';
			} else {
				$finalUrl = $url;
			}
		}
		return $finalUrl;
	}
	function obtenVideoKey( $con ) {
		$apiKey = null;
		$strQuery = 'select * from sucursal limit 1';
		$queryScur = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $queryScur ) ) {
			$apiKey = $U[ 'spotlightrApiKey' ];
		}
		return $apiKey;
	}
	function obtenClienteId( $urlHost, $dominios, $con ) {
		$clienteUrl = $urlHost;
		$clienteId = 0;
		foreach ( $dominios as $dominio ) {
			$clienteUrl = str_replace( $dominio, '', $clienteUrl );
		}
		$espacios = explode( '.', $clienteUrl );
		if ( sizeof( $espacios ) == 1 ) {
			$query = $con->Consulta( 'select * from platforms where url="' . $espacios[ 0 ] . '"' );
			while( $U = $con->Resultados( $query ) ) {
				$clienteId = $U[ 'clientId' ];
			}
		} else {
			$query = $con->Consulta( 'select * from clients where url="' . $espacios[ 0 ] . '"' );
			while( $U = $con->Resultados( $query ) ) {
				$clienteId = $U[ 'clientId' ];
			}
		}
		return $clienteId;
	}
	function obtenPlatformId( $urlHost, $dominios, $con ) {
		$clienteUrl = $urlHost;
		$clienteId = 0;
		foreach ( $dominios as $dominio ) {
			$clienteUrl = str_replace( $dominio, '', $clienteUrl );
		}
		$espacios = explode( '.', $clienteUrl );
		if ( sizeof( $espacios ) == 1 ) {
			$query = $con->Consulta( 'select * from platforms where url="' . $espacios[ 0 ] . '"' );
			while( $U = $con->Resultados( $query ) ) {
				$clienteId = $U[ 'clientId' ];
			}
		} else {
			$query = $con->Consulta( 'select * from platforms where url="' . $espacios[ 1 ] . '"' );
			while( $U = $con->Resultados( $query ) ) {
				$clienteId = $U[ 'clientId' ];
			}
		}
		return $clienteId;
	}
	function obtenCoachName( $coachId ) {
		global $con;
		$resClients = $con->Consulta( 'select nombre, apellido from coaches where coachId=' . $coachId );
		$R = $con->Resultados( $resClients );
		return $R[ 'nombre' ] . ' ' . $R[ 'apellido' ];
	}
	function obtenCoachData( $url, $clientId, $con ) {
		$data = array();
		$idsSearch = array();
		$resClients = $con->Consulta( 'select coachId from coachesclients where clientId=' . $clientId );
		while( $R = $con->Resultados( $resClients ) ) {
			if ( !in_array( $R[ 'coachId' ], $idsSearch ) ) {
				$idsSearch[] = $R[ 'coachId' ];
			}
		}
		$strQuery = 'select * from coaches where coachId in ( ' . implode( ',', $idsSearch ) . ' )';
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			$urlArma = limpialo( $U[ 'apellido' ], 'min' ) . '-' . limpialo( $U[ 'nombre' ], 'min' );
			if ( $urlArma == $url ) {
				$data = $U;
			}
		}
		return $data;
	}
	function obtenCircleData( $url, $con ) {
		$data = array();
		$strQuery = 'select * from circles';
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'nombre' ], 'min' ) == $url ) {
				$data = $U;
			}
		}
		return $data;
	}
	function obtenSerieData( $url, $con ) {
		$data = array();
		$strQuery = 'select * from series';
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'nombre' ], 'min' ) == $url ) {
				$data = $U;
			}
		}
		return $data;
	}
	function obtenPostData( $url, $clientId, $con ) {
		$data = array();
		$strQuery = 'select * from posts where clientId=' . $clientId;
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'title' ], 'min' ) == $url ) {
				$data = $U;
			}
		}
		return $data;
	}
	function generateRandomString( $length ) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen( $characters );
		$randomString = '';
		for ( $i = 0; $i < $length; $i++ ) {
			$randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
		}
		return $randomString;
	}
	function isMobile() {
		return preg_match( "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}
	function avisoPendingCoach( $coachId, $clientId, $status, $con ) {
		if ( $status != 'S' ) {
			$res = $con->Consulta( 'select * from coaches where coachId=' . $coachId );
			$R = $con->Resultados( $res );
			$typeId = ( $status == 'R' ) ? 3 : 4;
			$textos = getEmailText( $typeId, $clientId, $con );
			$status = mandaMail( $R[ 'email' ], $R[ 'nombre' ], $textos[ 'titulo' ], $textos[ 'mensaje' ], $clientId, $con );
			return $status;
		}
	}
	function avisoPendingMember( $studentId, $clientId, $status, $con ) {
		if ( $status != 'S' ) {
			$res = $con->Consulta( 'select * from users where userId=' . $studentId );
			$R = $con->Resultados( $res );
			$typeId = ( $status == 'R' ) ? 9 : 8;
			$textos = getEmailText( $typeId, $clientId, $con );
			$status = mandaMail( $R[ 'email' ], $R[ 'nombre' ], $textos[ 'titulo' ], $textos[ 'mensaje' ], $clientId, $con );
			return $status;
		}
	}
	function avisoRequestCoach( $requestId, $clientId, $con ) {
		$res = $con->Consulta( 'select sr.coachId from serierequest s inner join series sr on(s.serieId=sr.serieId) where s.requestSerieId=' . $requestId );
		$R = $con->Resultados( $res );
		$resCoach = $con->Consulta( 'select * from coaches where coachId=' . $R[ 'coachId' ] );
		$C = $con->Resultados( $res );
		$textos = getEmailText( 6, $clientId, $con );
		$status = mandaMail( $C[ 'email' ], $C[ 'nombre' ], $textos[ 'titulo' ], $textos[ 'mensaje' ], $clientId, $con );
		return $status;
	}
	function confirmacionMail( $cliente, $tabla, $token, $con ) {
		$html = '';
		$res = $con->Consulta( 'select * from clients where clientId=' . $cliente );
		$R = $con->Resultados( $res );
		$ligaCliente = 'http://' . $R[ 'url' ] . '.shala.us';
		$res = $con->Consulta( 'update ' . $tabla . ' set emailConfirmed="S" where email="' . desencripta( $token ) . '"' );
		if ( $res ) {
			if ( $tabla == 'coaches' ) {
				$res = $con->Consulta( 'select * from coaches where email="' . desencripta( $token ) . '"' );
				$R = $con->Resultados( $res );
				$textos = getEmailText( 5, $cliente, $con );
				$status = mandaMail( $R[ 'email' ], $R[ 'nombre' ], $textos[ 'titulo' ], $textos[ 'mensaje' ], $clientId, $con );
				avisoPendingApprovalCoach( $R[ 'coachId' ], $cliente, $con );
			} else {
				$res = $con->Consulta( 'select * from users where email="' . desencripta( $token ) . '"' );
				$R = $con->Resultados( $res );
				avisoPendingApprovalMember( $R[ 'userId' ], $cliente, $con );
			}
			$html = '<h2 class="tCenter">Your email has been confirmed, please login to your account now</h2><br><br><a href="' . $ligaCliente . '/dashboard/login" class="boton">Login</a>';
		} else {
			$html = '<h2 class="tCenter">We have a problem to confirm your account, please contact us and try again</h2><br><br><a href="' . $ligaCliente . '/sign-in" class="boton">Try register again</a>';
		}
		echo $html;
	}
	function confirmaEmail( $id, $tabla, $clientId, $con ) {
		$campoId = ( $tabla == 'coaches' ) ? 'coachId': 'userId';
		$res = $con->Consulta( 'select * from ' . $tabla . ' where ' . $campoId . '=' . $id );
		$R = $con->Resultados( $res );
		$textos = getEmailText( 12, $clientId, $con );
		$urlConfirma = 'http://www.shala.us/confirmacion/' . $clientId . '/' . $tabla . '/' . encripta( $R[ 'email' ] );
		$status = mandaMail( $R[ 'email' ], $R[ 'nombre' ], $textos[ 'titulo' ], $textos[ 'mensaje' ] . '<br><p><a href="' . $urlConfirma . '">' . $urlConfirma . '</a></p>', $clientId, $con );
		return $status;
	}
	function avisoPendingApprovalCoach( $coachId, $clientId, $con ) {
		$res = $con->Consulta( 'select * from coaches where coachId=' . $coachId );
		$R = $con->Resultados( $res );
		$textos = getEmailText( 10, $clientId, $con );
		$status = mandaMail( null, null, $textos[ 'titulo' ], 'We receive an approval request from coach ' . $R[ 'apellido' ] . ' ' . $R[ 'nombre' ] . '<br>' . $textos[ 'mensaje' ], $clientId, $con );
		return $status;
	}
	function avisoPendingApprovalMember( $studentId, $clientId, $con ) {
		$res = $con->Consulta( 'select * from users where userId=' . $studentId );
		$R = $con->Resultados( $res );
		$textos = getEmailText( 11, $clientId, $con );
		$status = mandaMail( null, null, $textos[ 'titulo' ], 'We receive an approval request from member ' . $R[ 'apellido' ] . ' ' . $R[ 'nombre' ] . '<br>' . $textos[ 'mensaje' ], $clientId, $con );
		return $status;
	}
	function getDefaultText() {
		$defaultText = array(
			'homeMenuText' => 'Home',
			'exploreMenuText' => 'Classes',
			'coachesMenuText' => 'Guides',
			'aboutMenuText' => 'About Us',
			'processingText' => 'Processing',
			'liveMenuText' => 'Live',
			'liveSessionText' => 'Live Sessions',
			'signUpText' => 'Sign Up',
			'memberText' => 'Member',
			'blogText' => 'Blog',
			'coachText' => 'Guide',
			'signUpCoachText' => 'Become a Guide',
			'headerTitle' => 'Align. Heal. Act.',
			'subtitleHeader' => 'Upgrade your human software to unlock your full potential and find your flow',
			'trialDaysText' => 'TRY IT FREE FOR 14 DAYS',
			'trialDescriptionText' => 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.',
			'titleClasses' => 'PROGRAMS',
			'subtitleClasses' => 'FIND A PROGRAM TO ACHIEVE YOUR GOALS',
			'videoHeader' => 'video-header.mp4',
			'videoMobileHeader' => 'video-header.mp4',
			'titleCategories' => 'WORK ON YOUR INNER SELF',
			'subtitleCategories' => 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.',
			'titleCoaches' => 'MEET OUR GUIDES',
			'subtitleCoaches' => 'Get started on a journey that inspires creativity and connection through classes offered by world class guides both online and in-person'
		);
		return $defaultText;
	}
	function checaSeccion( $seccion, $clientId, $con ) {
		$homeId = 0;
		$resHome = $con->Consulta( 'select * from home' );
		while( $H = $con->Resultados( $resHome ) ) {
			if ( strtolower( $H[ 'nombre' ] ) == $seccion ) {
				$homeId = $H[ 'homeId' ];
			}
		}
		if ( $homeId != 0 ) {
			$muestra = false;
			$resHome = $con->Consulta( 'select * from homeplatform where clientId=' . $clientId . ' and homeId=' . $homeId );
			while( $H = $con->Resultados( $resHome ) ) {
				$muestra = true;
			}
			return $muestra;
		} else {
			return true;
		}
	}
	function checaPage( $pageId, $clientId, $con ) {
		$muestra = false;
		$resHome = $con->Consulta( 'select * from clientpages where clientId=' . $clientId . ' and pageId=' . $pageId );
		while( $H = $con->Resultados( $resHome ) ) {
			$muestra = true;
		}
		return $muestra;
	}
	function existeDato( $tabla, $clientId, $con ) {
		$status = false;
		$res = $con->Consulta( 'select * from ' . $tabla . ' where clientId=' . $clientId );
		while( $R = $con->Resultados( $res ) ) {
			$status = true;
		}
		return $status;
	}
	function getMenuClass( $image ) {
		$rutaImg = $image;
		if ( file_exists( $rutaImg ) ) {
			list( $width, $height ) = getimagesize( $rutaImg );
			if ( $width > 0 ) {
				$nuevoAlto = ( 220 * $height ) / $width;
				$margin = ( $nuevoAlto - 20 ) / 2;
				return 'margin-top: ' . $margin . 'px';
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
	function getMainClass( $image ) {
		$rutaImg = $image;
		if ( file_exists( $rutaImg ) ) {
			list( $width, $height ) = getimagesize( $rutaImg );
			if ( $height > 0 ) {
				$nuevoAlto = ( 220 * $height ) / $width;
				$margin = $nuevoAlto + 10;
				return 'padding-top: ' . $margin . 'px';
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
	function getEmailText( $typeId, $clientId, $con ) {
		$emailData = array();
		$res = $con->Consulta( 'select * from email where clientId=' . $clientId . ' and typeId=' . $typeId );
		$cuantos = $con->Cuantos( $res );
		$R = $con->Resultados( $res );
		$resC = $con->Consulta( 'select * from clients where clientId=' . $clientId );
		$C = $con->Resultados( $resC );
		if ( $cuantos > 0 ) {
			$emailData[ 'titulo' ] = $R[ 'subject' ];
			$emailData[ 'mensaje' ] = $R[ 'generalText' ];
		} else {
			switch ( $typeId ) {
				case 1:
					$emailData[ 'titulo' ] = 'Password Recovery';
					$emailData[ 'mensaje' ] = '<h3>Password Recovery</h3><p>We have received a password reset request, please copy / paste the following link on the browser to continue:</p>';
				break;
				case 2:
					$emailData[ 'titulo' ] = 'Web Page Contact Request';
					$emailData[ 'mensaje' ] = '';
				break;
				case 3:
					$emailData[ 'titulo' ] = 'Sign up result';
					$emailData[ 'mensaje' ] = '<h3>Sorry</h3><p>We decide to reject your enrollment to our platform, you could find the reason on your coach profile</p>';
				break;
				case 4:
					$emailData[ 'titulo' ] = 'Sign up result';
					$emailData[ 'mensaje' ] = '<h3>Welcome to ' . $C[ 'nombre' ]. '</h3><p>Your account has been approved. You can access our platform at <a href="https://' . $C[ 'url' ]. '.shala.us/dashboard/">' . $C[ 'nombre' ]. ' Dashboard</a></p>';
				break;
				case 5:
					$emailData[ 'titulo' ] = 'Successfully sign up';
					$emailData[ 'mensaje' ] = '<h3>Sign up Request</h3><p>We have received a coach sign up request, please be patient while we review and approve your request</p>';
				break;
				case 55:
					$emailData[ 'titulo' ] = 'Successfully sign up';
					$emailData[ 'mensaje' ] = '<h3>Sign up Request</h3><p>We have received a student sign up request, please be patient while we review and approve your request</p>';
				break;
				case 6:
					$emailData[ 'titulo' ] = 'Serie access requested';
					$emailData[ 'mensaje' ] = '<h3>Access Request</h3><p>We have received an access request for one of your series</p>';
				break;
				case 7:
					$emailData[ 'titulo' ] = 'Password Recovery';
					$emailData[ 'mensaje' ] = '<h3>Password Recovery</h3><p>We have received a password reset request, please copy / paste the following link on the browser to continue:</p>';
				break;
				case 8:
					$emailData[ 'titulo' ] = 'Sign up result';
					$emailData[ 'mensaje' ] = '<h3>Welcome</h3><p>Congratulations, your account has been approved. You can now access the content on the platform.</p>';
				break;
				case 9:
					$emailData[ 'titulo' ] = 'Sign up result';
					$emailData[ 'mensaje' ] = '<h3>Sorry</h3><p>We decide to reject your enrollment to our platform, you could find the reason on your member profile</p>';
				break;
				case 10:
					$emailData[ 'titulo' ] = 'Approval request received';
					$emailData[ 'mensaje' ] = '';
				break;
				case 11:
					$emailData[ 'titulo' ] = 'Approval request received';
					$emailData[ 'mensaje' ] = '';
				break;
				case 12:
					$emailData[ 'titulo' ] = 'Email Confirmation';
					$emailData[ 'mensaje' ] = '<h3>Email Confirmation</h3><p>Thank you for signing up with us. We will need to confirm your email address. Please click the following link to continue.</p>';
				break;
				case 13:
					$emailData[ 'titulo' ] = 'Session Invitation';
					$emailData[ 'mensaje' ] = '<h3>You have a scheduled session</h3><p>We have received a notification that you were invited to a session</p>';
				break;
			}
		}
		return $emailData;
	}
	function getCircles( $coachId, $coachName, $clientId, $clientPlatformId, $con ) {
		$coachWhere = '';
		if ( !is_null( $coachId ) ) {
			$coachWhere = ' and c.coachId=' . $coachId;
		}
		$res = $con->Consulta( 'select c.*, t.nombre as coach from circles c inner join coaches t on(c.coachId=t.coachId) where ( c.clientId=' . $clientId . ' or c.clientId=' . $clientPlatformId . ' ) ' . $coachWhere );
		if ( $con->Cuantos( $res ) > 0 ) {
			echo '<br><h2 class="serieTitulo">' . $coachName . '\'s Circles</h2><div class="swiper-container"><div class="swiper-wrapper">';
			while( $R = $con->Resultados( $res ) ) {
				echo
				'<div class="swiper-slide circles" circleId="' . $R[ 'circleId' ] . '" coachId="' . $R[ 'coachId' ] . '">
					<img src="thumb?src=images/circles/' . $R[ 'photo' ] . '&size=450x450" class="fullImg fotoMeet" onclick="manda( \'/circles/' . limpialo( $R[ 'nombre' ], 'min' ) . '\' )">
					<h4 class="tCenter" onclick="manda( \'/circles/' . limpialo( $R[ 'nombre' ], 'min' ) . '\' )">' . $R[ 'nombre' ] . '</h4>
				</div>';
			}
			echo '</div></div>';
		}
	}
	function armaCall( $R, $timeZone ) {
		$enlace = ( is_null( $R[ 'url' ] ) ) ? 'Pending Approval' : ( ( $R[ 'disponible' ] == 'S' ) ? '<a href="' . $R[ 'url' ] . '" target="_blank">Join now</a>' : 'Waiting for Sessions\'s time' );
		$imagen = ( !is_null( $R[ 'image' ] ) && $R[ 'image' ] != '' ) ? 'images/calls/' . $R[ 'image' ] : 'images/coach-call.jpg';
		$tiempoModificado = zoneConvertion( $R[ 'tiempoCall' ], $R[ 'timezone' ], $timeZone );
		$piezasFecha = explode( ' ', $tiempoModificado );
		$dateDiff = time() - strtotime( $tiempoModificado );
		$dias = abs( round( $dateDiff / (60 * 60 * 24) ) );
		if ( $dias > 7 ) {
			$fechaMuestra = formatoFechaTiempo( $tiempoModificado, 'EN' );
		} else {
			$diaSemanaHoy = date('w', time() );
			$diaSemana = date('w', strtotime( $R[ 'solofecha' ] ) );
			$diasText = '';
			$listaDias = [ 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ];
			switch ( $dias ) {
				case 0: if ( $diaSemanaHoy == $diaSemana ) { $diasText = 'TODAY'; } else { $diasText = 'TOMORROW'; } break;
				case 1: $diasText = 'TOMORROW'; break;
				default: $diasText = $listaDias[ $diaSemana ]; break;
			}
			$fechaMuestra = $diasText . ' ' . substr( $piezasFecha[1], 0, 5 ) . ' HRS';
		}
		$claseShow = '';
		if ( $R[ 'tipoId' ] == 2 ) {
			$claseShow = 'privateSesion ';
			$resP = $con->Consulta( 'select * from usercall where callId=' . $R[ 'callId' ] );
			while( $P = $con->Resultados( $resP ) ) {
				$claseShow .= 'member-' . $P[ 'userId' ] . ' ';
			}
			$resP = $con->Consulta( 'select * from coachcall where callId=' . $R[ 'callId' ] );
			while( $P = $con->Resultados( $resP ) ) {
				$claseShow .= 'coach-' . $P[ 'coachId' ] . ' ';
			}
		}
		return
		'<div class="swiper-slide categoriaHome meetings ' . $claseShow . ' coach-' . $R[ 'coachId' ] . '" callId="' . $R[ 'callId' ] . '" coachId="' . $R[ 'coachId' ] . '">
			<img src="thumb?src=' . $imagen . '&size=300x450" class="fullImg fotoMeet" onclick="traeLlamada( ' . $R[ 'callId' ] . ' )">
			<p class="tiempoMeet" fecha="' . $piezasFecha[0] . '">' . $fechaMuestra . '</p>
			<span>' . $R[ 'coach' ] . '</span>
			<h4 onclick="traeLlamada( ' . $R[ 'callId' ] . ' )">' . $R[ 'title' ] . '</h4>
		</div>';
	}
	function getCallsCircle( $circleId, $timeZone, $con ) {
		$html = '';
		$res = $con->Consulta( 'select c.*, CONCAT(t.nombre, " ", t.apellido) as coach, c.fecha as tiempoCall, DATE(c.fecha) as solofecha, TIME(c.fecha) as soloTiempo, IF(c.fecha<=now() AND c.fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible from callscircle cc inner join calls c on(cc.callId=c.callId) inner join coaches t on(c.coachId=t.coachId) where c.fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR) and cc.circleId=' . $circleId . ' order by c.fecha asc' );
		if ( $con->Cuantos( $res ) > 0 ) {
			$html .= '<h2 class="serieTitulo">Live</h2>';
			$html .= '<br><div class="swiper-container"><div class="swiper-wrapper">';
			while( $R = $con->Resultados( $res ) ) {
				$html .= armaCall( $R, $timeZone );
			}
			$html .= '</div></div>';
		}
		return $html;
	}
	function getCalls( $coachId, $clientId, $clientPlatformId, $timeZone, $con ) {
		$html = '';
		$coachWhere = '';
		if ( !is_null( $coachId ) && $coachId != '' ) {
			$coachWhere = ' and ( c.coachId=' . $coachId . ' or allCoaches="S" )';
		}
		$res = $con->Consulta( 'select *, CONCAT(t.nombre, " ", t.apellido) as coach, c.fecha as tiempoCall, DATE(c.fecha) as solofecha, TIME(c.fecha) as soloTiempo, IF(c.fecha<=now() AND c.fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible from calls c inner join coaches t on(c.coachId=t.coachId) where c.fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR) and ( c.clientId=' . $clientId . ' or c.clientId=' . $clientPlatformId . ' ) and tipoId<>4 ' . $coachWhere . ' order by c.fecha asc' );
		if ( $con->Cuantos( $res ) > 0 ) {
			$html .= '<h2 class="serieTitulo">Live</h2>';
			$html .= '<br><div class="swiper-container"><div class="swiper-wrapper">';
			while( $R = $con->Resultados( $res ) ) {
				$html .= armaCall( $R, $timeZone );
			}
			$html .= '</div></div>';
		}
		return $html;
	}
	function getCoachCircle( $circleId, $con ) {
		$res = $con->Consulta( 'select t.* from circles c inner join coaches t on(c.coachId=t.coachId) where c.circleId=' . $circleId );
		$R = $con->Resultados( $res );
		armaCoach( 'images/coaches/' . $R[ 'foto' ], 'coach/' . limpialo( $R[ 'apellido' ], 'min' ) . '-' . limpialo( $R[ 'nombre' ], 'min' ), $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] );
		$res = $con->Consulta( 'select t.* from coachcircle c inner join coaches t on(c.coachId=t.coachId) where c.circleId=' . $circleId );
		while( $R = $con->Resultados( $res ) ) {
			armaCoach( 'images/coaches/' . $R[ 'foto' ], 'coach/' . limpialo( $R[ 'apellido' ], 'min' ) . '-' . limpialo( $R[ 'nombre' ], 'min' ), $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] );
		}
	}
	function armaCoach( $coachImage, $coachUrl, $coachName ) {
		echo
		'<div class="swiper-slide circles">
			<img src="thumb?src=' . $coachImage . '&size=300x300" class="fullImg coachPhoto" onclick="manda( \'' . $coachUrl . '\' )">
			<h4 class="tCenter" onclick="manda( ' . $coachUrl . '\' )">' . $coachName . '</h4>
		</div>';
	}
	function minutesDiff( $date ) {
		$time = new DateTime( $date );
		$diff = $time->diff( new DateTime() );
		$minutes = ( $diff->days * 24 * 60 ) + ( $diff->h * 60 ) + $diff->i;
		return $minutes;
	}
	function coachesLink( $clientId, $con ) {
		$link = 'instructors';
		$res = $con->Consulta( 'select DISTINCT c.* from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where cc.clientId=' . $clientId . ' and cc.pending="N" order by c.apellido asc, c.nombre asc' );
		if ( $con->Cuantos( $res ) == 1 ) {
			$R = $con->Resultados( $res );
			$link = 'coach/' . limpialo( $R[ 'apellido' ], 'min' ) . '-' . limpialo( $R[ 'nombre' ], 'min' );
		}
		return $link;
	}
	function zoneConvertion( $time, $cur_zone, $req_zone ) {
		$date = new DateTime( $time, new DateTimeZone( $cur_zone ) );
		$newDate = $date->setTimezone( new DateTimeZone( $req_zone ) );
		return $date->format( 'Y-m-d H:i:s' );
	}
	function userCircles( $circleId, $con ) {
		$usersArray = array();
		$res = $con->Consulta( 'select userId from usercircle where circleId=' . $circleId );
		while( $R = $con->Resultados( $res ) ) {
			$usersArray[] = $R[ 'userId' ];
		}
		return implode( ',', $usersArray );
	}
	function coachesCircles( $circleId, $con ) {
		$coachArray = array();
		$res = $con->Consulta( 'select coachId from coachcircle where circleId=' . $circleId );
		while( $R = $con->Resultados( $res ) ) {
			$coachArray[] = $R[ 'coachId' ];
		}
		return implode( ',', $coachArray );
	}
?>