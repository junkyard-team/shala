<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = '';
	$folderActivo = '';
	$archivoSubida = 'No Trae';
	$multiples = '';
	$continuaGuardando = true;
	$multiplesComment = '';
	if ( $_REQUEST[ 'tabla' ] == 'coaches' || $_REQUEST[ 'tabla' ] == 'users' ) {
		if ( $_REQUEST[ 'tabla' ] == 'coaches' ) {
			if ( isset( $_REQUEST[ 'coachId' ] ) ) {
				$resClients = $con->Consulta( 'select * from coaches where email="' . $_REQUEST[ 'email' ] . '" and coachId<>' . $_REQUEST[ 'coachId' ] );
			} else {
				$resClients = $con->Consulta( 'select * from coaches where email="' . $_REQUEST[ 'email' ] . '"' );
			}
			while( $R = $con->Resultados( $resClients ) ) { $continuaGuardando = false; }
			$resClients = $con->Consulta( 'select * from users where email="' . $_REQUEST[ 'email' ] . '"' );
			while( $R = $con->Resultados( $resClients ) ) { $continuaGuardando = false; }
		} else {
			if ( isset( $_REQUEST[ 'userId' ] ) ) {
				$resClients = $con->Consulta( 'select * from users where email="' . $_REQUEST[ 'email' ] . '" and userId<>' . $_REQUEST[ 'userId' ] );
			} else {
				$resClients = $con->Consulta( 'select * from users where email="' . $_REQUEST[ 'email' ] . '"' );
			}
			while( $R = $con->Resultados( $resClients ) ) { $continuaGuardando = false; }
			$resClients = $con->Consulta( 'select * from coaches where email="' . $_REQUEST[ 'email' ] . '"' );
			while( $R = $con->Resultados( $resClients ) ) { $continuaGuardando = false; }
		}
	}
	if ( $continuaGuardando ) {
		if ( $_REQUEST ) {
			foreach ( $_REQUEST as $key => $value ) {
				if ( $key == 'tabla' ) {
					$tabla = $value;
				} else if ( $key == 'folderActivo' ) {
					$folderActivo = $value . '/';
				} else if ( $key == 'multiples' ) {
					$multiples = json_decode( $value );
				}
			}
		}
		if ( $_FILES ) {
			foreach ( $_FILES as $file => $array ) {
				$allowed = array( 'png', 'jpg', 'gif', 'pdf', 'jpeg', 'webp', 'mp4', 'ttf', 'otf', 'woff', 'eot', 'mp3', 'ogg', 'mpeg' );
				if ( isset( $_FILES[ $file ] ) && $_FILES[ $file ][ 'error' ] == 0 ) {
					$extension = strtolower( pathinfo( $_FILES[ $file ][ 'name' ], PATHINFO_EXTENSION ) );
					if ( !in_array( $extension, $allowed ) ) {
						$archivoSubida = 'Extension no permitida';
					} else {
						$ruta = '../../images/' . $folderActivo;
						if ( !file_exists( $ruta ) ) {
	    					mkdir( $ruta, 0777 );
	    				}
						$temporal = str_replace( $extension, '', $_FILES[ $file ][ 'name' ] );
						$nombre_fichero = limpialo( $temporal, 'min' ) . time() . '.' . $extension;
						if ( move_uploaded_file( $_FILES[ $file ][ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
							$archivos[ $file ] = $nombre_fichero;
							$archivoSubida = 'OK';
						} else {
							$archivoSubida = 'No Guardo Archivo';
						}
					}
				} else {
					$archivoSubida = 'El archivo subio mal';
				}
			}
		}
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '"';
		$res = $con->Consulta( $str );
		$insert = 'insert into ' . $tabla. ' (';
		$update = 'update ' . $tabla . ' set ';
		$fieldId = '';
		$values = '';
		$updateIniciado = 0;
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' ) {
				if ( isset( $archivos[ $R[ 'columna' ] ] ) ) {
					if ( $values == '' ) {
						$values = '"' . $archivos[ $R[ 'columna' ] ] . '"';
						$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' );
						$updateIniciado++;
						$insert .= $R[ 'columna' ];
					} else {
						$values .= ', "' . $archivos[ $R[ 'columna' ] ] . '"';
						$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' );
						$updateIniciado++;
						$insert .= ', ' . $R[ 'columna' ];
					}
				} else {
					if ( $R[ 'comentarios' ] != 'archivo' ) {
						if ( isset( $_REQUEST[ $R[ 'columna' ] ] ) ) {
							$valorRecibido = ( $R[ 'comentarios' ] == 'html' ) ? htmlentities( $_REQUEST[ $R[ 'columna' ] ] ) : $_REQUEST[ $R[ 'columna' ] ];
							$valorRecibido = ( $R[ 'comentarios' ] == 'pass' ) ? encripta( $_REQUEST[ $R[ 'columna' ] ] ) : $_REQUEST[ $R[ 'columna' ] ];
							if ( $values == '' ) {
								$insert .= $R[ 'columna' ];
								if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'mediumtext' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'text' ) {
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' );
									$values = '"' . $con->escapaTexto( $valorRecibido ) . '"';
									$updateIniciado++;
								} else {
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $valorRecibido . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $valorRecibido . ' ' );
									$values = $valorRecibido;
									$updateIniciado++;
								}
							} else {
								$insert .= ', ' . $R[ 'columna' ];
								if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'mediumtext' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'text' ) {
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' );
									$updateIniciado++;
									$values .= ', "' . $con->escapaTexto( $valorRecibido ) . '"';
								} else {
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $valorRecibido . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $valorRecibido . ' ' );
									$updateIniciado++;
									$values .= ', ' . $valorRecibido;
								}
							}
						} else {
							if ( $R[ 'obligatoria' ] == 'S' && is_null( $R[ 'valorDefault' ] ) ) {
								if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'text' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'mediumtext' ) {
									if ( $values == '' ) {
										$insert .= $R[ 'columna' ];
										$values = '""';
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="" ' ) : ( ', ' . $R[ 'columna' ] . '="" ' );
										$updateIniciado++;
									} else {
										$insert .= ', ' . $R[ 'columna' ];
										$values .= ', ""';
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="" ' ) : ( ', ' . $R[ 'columna' ] . '="" ' );
										$updateIniciado++;
									}
								} else if ( $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' ) {
									$fecha = ( $R[ 'tipoDatos' ] == 'date' ) ? 'Curdate()' : 'now()';
									if ( $values == '' ) {
										$insert .= $R[ 'columna' ];
										$values = $fecha;
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $fecha . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $fecha . ' ' );
										$updateIniciado++;
									} else {
										$insert .= ', ' . $R[ 'columna' ];
										$values .= ', ' . $fecha;
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $fecha . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $fecha . ' ' );
										$updateIniciado++;
									}
								} else {
									if ( $values == '' ) {
										$insert .= $R[ 'columna' ];
										$values .= '0';
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=0 ' ) : ( ', ' . $R[ 'columna' ] . '=0 ' );
										$updateIniciado++;
									} else {
										$insert .= ', ' . $R[ 'columna' ];
										$values .= ', 0';
										$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=0 ' ) : ( ', ' . $R[ 'columna' ] . '=0 ' );
										$updateIniciado++;
									}
								}
							}
						}
					} else {
						if ( $R[ 'obligatoria' ] == 'S' && is_null( $R[ 'valorDefault' ] ) ) {
							if ( $values == '' ) {
								$insert .= $R[ 'columna' ];
								$values = '""';
							} else {
								$insert .= ', ' . $R[ 'columna' ];
								$values .= ', ""';
							}
						}
					}
				}
			} else {
				$id = $_REQUEST[ $R[ 'columna' ] ];
				$fieldId = $R[ 'columna' ];
				$tipoId = $R[ 'tipoDatos' ];
				if ( preg_match('/\beditable\b/', $R[ 'comentarios' ] ) ) {
					if ( $values == '' ) {
						$insert .= $R[ 'columna' ];
						if ( $tipoId == 'int' ) {
							$values .= $_REQUEST[ $R[ 'columna' ] ];
						} else {
							$values .= '"' . $_REQUEST[ $R[ 'columna' ] ] . '"';
						}
					} else {
						$insert .= ', ' . $R[ 'columna' ];
						if ( $tipoId == 'int' ) {
							$values .= ', ' . $_REQUEST[ $R[ 'columna' ] ];
						} else {
							$values .= ', "' . $_REQUEST[ $R[ 'columna' ] ] . '"';
						}
					}
				}
				if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
					$multiplesComment = $R[ 'comentarios' ];
				}
			}
		}
		if ( $_REQUEST[ 'tabla' ] == 'pedidos' ) {
			$resPedido = $con->Consulta( 'select * from pedidos where idpedido=' . $_REQUEST[ 'idpedido' ] );
			$P = $con->Resultados( $resPedido );
		}
		$insert .= ' ) values( ' . $values . ')';
		if ( $id == 0 ) {
			$strNew = $insert;
		} else {
			$strNew = $update . ' where ' . $fieldId . '=' . $id;
		}
		$resNew = $con->Consulta( $strNew );
		if ( $resNew ) {
			$mail = '';
			$nuevo = ( $id == 0 ) ? true : false;
			$id = ( $id == 0 ) ? $con->Ultimo() : $id;
			if ( $_REQUEST[ 'tabla' ] == 'coaches' || $_REQUEST[ 'tabla' ] == 'users' ) {
				$tablaClients = ( $tabla != 'coaches' ) ? 'userclient' : 'coachesclients';
				if ( isset( $_REQUEST[ 'clientId' ] ) && $nuevo ) {
					$con->Consulta( 'insert into ' . $tablaClients . ' ( clientId, ' . $fieldId . ' ) values( ' . $_REQUEST[ 'clientId' ] . ', ' . $id . ' )' );
					if ( !isset( $_REQUEST[ 'mail' ] ) || $_REQUEST[ 'mail' ] == 'S' ) {
						$mail = confirmaEmail( $id, $_REQUEST[ 'tabla' ], $_REQUEST[ 'clientId' ], $con );
					}
				}
			}
			if ( $_REQUEST[ 'tabla' ] == 'serierequest' ) {
				$mail = avisoRequestCoach( $id, $_REQUEST[ 'clientId' ], $con );
			}
			if ( $multiples != '' ) {
				foreach ( $multiples as $multiple ) {
					$str = 'delete from ' . $multiple->destino . ' where ' . $fieldId . '=' . $id;
					$res = $con->Consulta( $str );
					$strMultiple = 'insert into ' . $multiple->destino . '( ' . $fieldId;
					$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $multiple->destino . '" and COLUMN_KEY<>"PRI" and COLUMN_NAME<>"' . $fieldId . '"';
					$res = $con->Consulta( $str );
					while( $R = $con->Resultados( $res ) ) {
						$strMultiple .= ', ' . $R[ 'columna' ];
					}
					$strMultiple .= ' ) values( ';
					$idsCatalogo = explode( '-', $multiple->ids );
					for ( $i = 0; $i < sizeof( $idsCatalogo ); $i++ ) {
						$str = $strMultiple . $id . ', ' . $idsCatalogo[ $i ] . ' )';
						$res = $con->Consulta( $str );
					}
				}
			} else {
				if ( $multiplesComment != '' ) {
					$variosMultiples = explode( ';', $multiplesComment );
					for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
						if ( $variosMultiples[ $i ] != '' ) {
							$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
							$tablas = explode( ',', $infoTablas );
							$tablaCatalogo = $tablas[0];
							$tablaDestino = $tablas[1];
							$str = 'delete from ' . $tablaDestino . ' where ' . $fieldId . '=' . $id;
							$res = $con->Consulta( $str );
						}
					}
				}
			}
			$status = array( 'status' => 'Success', 'archivo' => $archivoSubida, 'id' => $id, 'mail' => $mail );
		} else {
			$status = array( 'status' => 'Error', 'archivo' => $archivoSubida );
		}
	} else {
		$status = array( 'status' => 'Repetido' );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>