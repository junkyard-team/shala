<?php
	ini_set( 'default_charset', 'utf-8' );
	header( 'Content-Type: text/html; charset=utf-8' );
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$multiplesComment = '';
	$posicionCampo = '';
	$idiomaCampo = '';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
			$tipoId = $R[ 'tipoDatos' ];
			if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
				$multiplesComment = $R[ 'comentarios' ];
			}
		}
		if ( $R[ 'comentarios' ] == 'html' ) {
			$tipoHtml[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'icono' ) {
			$tipoIcono[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'pass' ) {
			$tipoPass[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'activar' ) {
			$tipoCheck[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'liga' ) {
			$tipoLiga[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'posicion' ) {
			$posicionCampo = $R[ 'columna' ];
		}
		if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
			$tipoOpciones[] = $R[ 'columna' ];
		}
		if ( preg_match('/\bidioma\b/', $R[ 'comentarios' ] ) ) {
			$idiomaCampo = $R[ 'columna' ];
		}
	}
	if ( isset( $_REQUEST[ 'id' ] ) ) {
		if ( $tipoId == 'int' ) {
			$str = 'select * from ' . $tabla . ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
		} else {
			$str = 'select * from ' . $tabla . ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
		}
		$res = $con->Consulta( $str );
		$R = $con->Resultados( $res );
		$status = array(
			'status' => 'Success'
		);
		foreach ( $R as $key => $value ) {
			if ( !is_numeric( $key ) ) {
				if ( isset( $tipoHtml ) ) {
					$valor = ( in_array( $key, $tipoHtml ) ) ? html_entity_decode( $value ) : $value;
				} else {
					$valor = $value;
				}
				if ( isset( $tipoPass ) && $value != '' ) {
					$valor = ( in_array( $key, $tipoPass ) ) ? desencripta( $value ) : $value;
				}
				$status[ $key ] = $valor;
			}
		}
		if ( $multiplesComment != '' ) {
			$variosMultiples = explode( ';', $multiplesComment );
			for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
				if ( $variosMultiples[ $i ] != '' ) {
					$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
					$tablas = explode( ',', $infoTablas );
					$tablaCatalogo = $tablas[0];
					$tablaDestino = $tablas[1];
					$catalogoId = getCampoId( $tablaCatalogo, $con );
					if ( $catalogoId != '' ) {
						if ( $tipoId == 'int' ) {
							$str = 'select ' . $catalogoId . ' from ' . $tablaDestino . ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
						} else {
							$str = 'select ' . $catalogoId . ' from ' . $tablaDestino . ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
						}
						$res = $con->Consulta( $str );
						$mt = 0;
						while( $C = $con->Resultados( $res ) ) {
							$multiples[ $mt ] = $C[ $catalogoId ];
							$mt++;
						}
						$multipleList = '';
						if ( isset( $multiples ) ) {
							$multipleList = implode( '-', $multiples );
						}
						$status[ 'multiple-' . $i ] = $multipleList;
						unset( $multiples );
					}
				}
			}
		}
		if ( $tabla == 'coaches' ) {
			$res = $con->Consulta( 'select * from coachesclients where coachId=' . $_REQUEST[ 'id' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
			$C = $con->Resultados( $res );
			$status[ 'pending' ] = $C[ 'pending' ];
			/*$emailZoom = '';
			$clientPublic = 'go3Ws0HIQa6AuY1P4wIZDg';
			$clientSecret = '7ZybvDrXXQjpPpEqHj09cDKCLWuk8Kib';
			if ( !is_null( $R[ 'zoomToken' ] ) && $R[ 'zoomToken' ] != '' ) {
				$request_url = 'https://api.zoom.us/v2/users/me';
		        $headers = array( 'Authorization: Bearer ' . $R[ 'zoomToken' ] );
		        $ch = curl_init();
		        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
		        curl_setopt( $ch, CURLOPT_URL, $request_url );
		        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		        $response = curl_exec( $ch );
		        $err = curl_error( $ch );
		        curl_close( $ch );
		        if( $response ) {
		        	$datos = json_decode( $response );
		        	var_dump( $datos );
		        	if ( isset( $datos->email ) && $datos->email != '' ) {
		        		$emailZoom = $datos->email;
			        } else {
			        	if ( isset( $datos->code ) && $datos->code == 124 ) {
			        		$request_url = 'https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token=' . $R[ 'zoomToken' ];
					        $basicAuth = base64_encode( $clientPublic . ':' . $clientSecret );
					        $headers = array(
					        	'Authorization: Basic ' . $basicAuth,
					        	'Content-Type: application/x-www-form-urlencoded'
					        );
					        $ch = curl_init();
					        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
					        curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
					        curl_setopt( $ch, CURLOPT_URL, $request_url );
					        curl_setopt( $ch, CURLOPT_POST, 1 );
					        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
					        $response = curl_exec( $ch );
					        $err = curl_error( $ch );
					        curl_close( $ch );
					        if( $response ) {
					        	$datos = json_decode( $response );
					        	if ( !isset( $datos->error ) || $datos->error == '' ) {
					        		$request_url = 'https://api.zoom.us/v2/users/me';
							        $headers = array( 'Authorization: Bearer ' . $R[ 'zoomToken' ] );
							        $ch = curl_init();
							        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
							        curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
							        curl_setopt( $ch, CURLOPT_URL, $request_url );
							        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
							        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
							        $response = curl_exec( $ch );
							        $err = curl_error( $ch );
							        curl_close( $ch );
							        if( $response ) {
							        	$datos = json_decode( $response );
							        	var_dump( $datos );
							        	if ( isset( $datos->email ) && $datos->email != '' ) {
							        		$emailZoom = $datos->email;
								        }
								    }
						        }
					        }
			        	}
			        }
		        }
			} else {
				$http = ( isset( $_SERVER[ 'HTTPS' ] ) ) ? 'https' : 'http';
				$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
				if ( !is_null( $R[ 'zoomAuth' ] ) && $R[ 'zoomAuth' ] != '' ) {
					$request_url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' . $R[ 'zoomAuth' ] . '&redirect_uri=' . $base . '/zoom-activate?coachId=' . $_REQUEST[ 'id' ];
			        $basicAuth = base64_encode( $clientPublic . ':' . $clientSecret );
			        $header = array(
			        	'Authorization: Basic ' . $basicAuth,
			        	'Content-Type: application/x-www-form-urlencoded'
			        );
			        $chDos = curl_init();
			        curl_setopt( $chDos, CURLOPT_HTTPHEADER, $header );
			        curl_setopt( $chDos, CURLOPT_RETURNTRANSFER,1 );
			        curl_setopt( $chDos, CURLOPT_URL, $request_url );
			        curl_setopt( $chDos, CURLOPT_POST, 1 );
			        curl_setopt( $chDos, CURLOPT_SSL_VERIFYPEER, false );
			        $response = curl_exec( $chDos );
			        $err = curl_error( $chDos );
			        curl_close( $chDos );
			        if( $response ) {
			        	$datos = json_decode( $response );
			        	var_dump( $datos );
			        	if ( !isset( $datos->error ) || $datos->error == '' ) {
				        	$query = $con->Consulta( 'update coaches set zoomToken="' . $datos->access_token . '" where coachId=' . $_REQUEST[ 'coachId' ] );
				        	$request_url = 'https://api.zoom.us/v2/users/me';
					        $headers = array( 'Authorization: Bearer ' . $datos->access_token );
					        $ch = curl_init();
					        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
					        curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
					        curl_setopt( $ch, CURLOPT_URL, $request_url );
					        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
					        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
					        $response = curl_exec( $ch );
					        $err = curl_error( $ch );
					        curl_close( $ch );
					        if( $response ) {
					        	$datos = json_decode( $response );
					        	if ( isset( $datos->email ) && $datos->email != '' ) {
					        		$emailZoom = $datos->email;
						        }
					        }
				        }
			        }
				}
			}
			$status[ 'zoom' ] = $emailZoom;*/
		}
		if ( $tabla == 'users' ) {
			$res = $con->Consulta( 'select * from userclient where userId=' . $_REQUEST[ 'id' ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
			$C = $con->Resultados( $res );
			$status[ 'pending' ] = $C[ 'pending' ];
		}
		if ( $tabla == 'calls' ) {
			$tiempoModificado = ( !is_null( $R[ 'timezone' ] ) ) ? zoneConvertion( $R[ 'fecha' ], $R[ 'timezone' ], $_REQUEST[ 'timeZone' ] ) : $R[ 'fecha' ];
			$partesFecha = explode( ' ', $tiempoModificado );
			$status[ 'fechaNormal' ] = formatoFechaTiempo( $tiempoModificado, 'EN' );
			$dateDiff = time() - strtotime( $partesFecha[ 0 ] );
			$dias = abs( round( $dateDiff / (60 * 60 * 24) ) );
			if ( $dias > 7 ) {
				$fechaMuestra = formatoFechaTiempo( $tiempoModificado, 'ES' );
			} else {
				$diaSemanaHoy = date('w', time() );
				$diaSemana = date('w', strtotime( $partesFecha[ 0 ] ) );
				$diasText = '';
				$listaDias = [ 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ];
				switch ( $dias ) {
					case 0: if ( $diaSemanaHoy == $diaSemana ) { $diasText = 'TODAY'; } else { $diasText = 'TOMORROW'; } break;
					case 1: $diasText = 'TOMORROW'; break;
					default: $diasText = $listaDias[ $diaSemana ]; break;
				}
				$fechaMuestra = $diasText . ' ' . substr( $partesFecha[ 1 ], 0, 5 ) . ' HRS';
			}
			if ( isset( $_REQUEST[ 'timeZone' ] ) ) {
				date_default_timezone_set( $_REQUEST[ 'timeZone' ] );
			}
			$diferencia = ( strtotime( $tiempoModificado ) - time() ) / 60;
			$status[ 'diferencia' ] = $diferencia;
			$status[ 'fechaFormateada' ] = $fechaMuestra;
			$res = $con->Consulta( 'select * from coaches where coachId=' . $R[ 'coachId' ] );
			$C = $con->Resultados( $res );
			$status[ 'coach' ] = $C[ 'nombre' ] . ' ' . $C[ 'apellido' ];
			$enlace = ( is_null( $R[ 'url' ] ) ) ? '' : ( ( $diferencia > ( $R[ 'duration' ] * -1 ) && $diferencia <= 5 ) ? '<a href="' . $R[ 'url' ] . '" class="boton" target="_blank">Join now</a>' : '<a href="#" class="noHref boton deshabilitado" onclick="llamadaAntes()">Join now</a>' );
			$status[ 'link' ] = $enlace;
			$status[ 'suscrito' ] = false;
			if ( isset( $_REQUEST[ 'studentId' ] ) ) {
				$res = $con->Consulta( 'select * from usercall where callId=' . $_REQUEST[ 'id' ] . ' and userId=' . $_REQUEST[ 'studentId' ] );
				while( $C = $con->Resultados( $res ) ) {
					$status[ 'suscrito' ] = true;
				}
			}
			if ( isset( $_REQUEST[ 'coachId' ] ) ) {
				if ( $R[ 'coachId' ] == $_REQUEST[ 'coachId' ] ) {
					$status[ 'suscrito' ] = true;
				} else {
					$res = $con->Consulta( 'select * from coachcall where callId=' . $_REQUEST[ 'id' ] . ' and coachId=' . $_REQUEST[ 'coachId' ] );
					while( $C = $con->Resultados( $res ) ) {
						$status[ 'suscrito' ] = true;
					}
				}
			}
			$status[ 'circuloAcceso' ] = false;
			if ( $R[ 'tipoId' ] == 4 ) {
				if ( isset( $_REQUEST[ 'circleId' ] ) ) {
					if ( isset( $_REQUEST[ 'coachId' ] ) ) {
						$res = $con->Consulta( 'select * from coachcircle where circleId=' . $_REQUEST[ 'circleId' ] . ' and coachId=' . $_REQUEST[ 'coachId' ] );
						while( $C = $con->Resultados( $res ) ) {
							$status[ 'circuloAcceso' ] = true;
						}
					} else if ( isset( $_REQUEST[ 'studentId' ] ) ) {
						$res = $con->Consulta( 'select * from usercircle where circleId=' . $_REQUEST[ 'circleId' ] . ' and userId=' . $_REQUEST[ 'studentId' ] );
						while( $C = $con->Resultados( $res ) ) {
							$status[ 'circuloAcceso' ] = true;
						}
					}
				}
			}
			$status[ 'fecha' ] = $tiempoModificado;
		}
	} else {
		global $palabrasReservadas;
		$strList = 'select ' . $campoId;
		$filas = 0;
		$opcionesCampos = array();
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by ORDINAL_POSITION asc';
		$res = $con->Consulta( $str );
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			echo $str .'<hr><br>';
		}
		while( $R = $con->Resultados( $res ) ) {
			if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
				echo 'COLUMNA = ' . $R[ 'columna' ] . ' | COMENTARIO = ' . $R[ 'comentarios' ] . ' | ' . ' OBLIGATORIA = ' . $R[ 'obligatoria' ] . ' | LLAVE = ' . $R[ 'esLlave' ];
			}
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
				if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
					echo '<br>Entro';
				}
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
						echo '<br>Entro a palabras reservadas o sin comentarios';
					}
					$strList .= ', ' . $R[ 'columna' ];
					$columnas[ $filas ] = $R[ 'columna' ];
					$filas++;
				} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
					if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
						echo '<br>Entro a opciones';
					}
					$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
					$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tablaCatalogo );
					$opcionesCampos[ $R[ 'columna' ] ] = array( 'tabla' => $tablaCatalogo, 'titulo' => ucfirst( $titulo ), 'campo' => $R[ 'columna' ] );
					$strList .= ', ' . $R[ 'columna' ];
					$columnas[ $filas ] = $R[ 'columna' ];
					$filas++;
				}
			}
			if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
				echo '<hr>';
			}
		}
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			var_dump( $tipoOpciones );
			var_dump( $opcionesCampos );
			var_dump( $columnas );
		}
		$ordenamiento = ( $idiomaCampo != '' ) ? $idiomaCampo . ' asc, ' : '';
		$ordenamiento .= ( $posicionCampo != '' ) ? $posicionCampo . ' asc, ' : '';
		if ( $tabla != 'coaches' && $tabla != 'users' ) {
			if ( $tabla == 'videos' && isset( $_REQUEST[ 'serieId' ] ) ) {
				$strList .= ' from ' . $tabla . ' where clientId=' . $_REQUEST[ 'clientId' ] . ' and serieId=' . $_REQUEST[ 'serieId' ] . ' order by ' . $ordenamiento . $campoId . ' desc';
			} else {
				$strList .= ' from ' . $tabla . ' where clientId=' . $_REQUEST[ 'clientId' ] . ' order by ' . $ordenamiento . $campoId . ' desc';
			}
		} else {
			$idsSearch = array();
			$tablaClients = ( $tabla != 'coaches' ) ? 'userclient' : 'coachesclients';
			$resClients = $con->Consulta( 'select ' . $campoId . ' from ' . $tablaClients . ' where clientId=' . $_REQUEST[ 'clientId' ] );
			while( $R = $con->Resultados( $resClients ) ) {
				if ( !in_array( $R[ $campoId ], $idsSearch ) ) {
					$idsSearch[] = $R[ $campoId ];
				}
			}
			$emailConfirmed = ( isset( $_REQUEST[ 'emailConfirmed' ] ) ) ? $_REQUEST[ 'emailConfirmed' ] : 'S';
			$strList .= ' from ' . $tabla . ' where ' . $campoId . ' in ( ' . implode( ',', $idsSearch ) . ' ) and emailConfirmed="' . $emailConfirmed . '" order by ' . $ordenamiento . $campoId . ' desc';
		}
		$res = $con->Consulta( $strList );
		$html = '';
		$i = 0;
		$idUnico = 0;
		while( $R = $con->Resultados( $res ) ) {
			$ligaTiene = false;
			$fila = '<tr id="' . $tabla . '-' . $R[ $campoId ] . '" campoId="' . $R[ $campoId ] . '">';
			$eliminado = ( $tipoId == 'int' ) ? 'eliminado( ' . $R[ $campoId ] . ' )' : 'eliminado( \'' . $R[ $campoId ] . '\' )';
			$iconElimina = '<i class="fa fa-trash" onclick="' . $eliminado . '"></i>';
			if ( $tabla == 'coaches' || $tabla == 'users' ) {
				//$eliminado = 'eliminadoCoach( ' . $R[ $campoId ] . ' )';
				$eliminado = 'borraCuenta( \'' . $tabla . '\', ' . $R[ $campoId ] . ' )';
				$iconElimina = ( $R[ 'statusId' ] == 1 ) ? '' : '<i class="fa fa-trash" onclick="' . $eliminado . '"></i>';
			}
			$idUnico = $R[ $campoId ];
			$elegido = ( $tipoId == 'int' ) ? 'elegido( ' . $R[ $campoId ] . ' )' : 'elegido( \'' . $R[ $campoId ] . '\' )';
			$preguntasIcon = ( $tabla == 'users' ) ? '<i class="fa fa-question" title="Ver respuestas" onclick="respuestaAlumno( ' . $R[ $campoId ] . ', 1 )"></i>' : '';
			$visualizaIcon = ( $tabla == 'users' || $tabla == 'coaches' ) ? '<i class="fa fa-eye" title="Ver detalle" onclick="manda( \'visualiza/' . $tabla . '/' . $R[ $campoId ] . '\' )"></i></a>' : '';
			$videoIcon = ( $tabla == 'series' ) ? '<i class="fa fa-video-camera" title="Ver Videos" onclick="abreVideos( ' . $R[ $campoId ] . ' )"></i>' : '';
			$style = ( $tabla == 'users' ) ? 'style="width: 130px;"' : '';
			$catPadre = '';
			$fila .=
				'<td class="tcenter iconos">
					<div ' . $style . '>
						' . $iconElimina . '
						<i class="fa fa-edit" onclick="' . $elegido . '"></i>
						' . $preguntasIcon . '
						' . $visualizaIcon . '
						' . $videoIcon . '
					</div>
				</td>
				<td class="tcenter">' . $R[ $campoId ] . '</td>' . $catPadre;
			if ( isset( $columnas ) ) {
				for ( $indi = 0; $indi < sizeof( $columnas ); $indi++ ) {
					if ( $posicionCampo != $columnas[ $indi ] ) {
						if ( $tabla == 'videos' && $columnas[ $indi ] == 'spotlightr' ) {
							$fila .= '<td><script src="https://amrak.cdn.spotlightr.com/assets/vooplayer.js"></script><a class="fancyboxIframe vooplayer" href="https://amrak.cdn.spotlightr.com/publish/' . $R[ $columnas[ $indi ] ] . '" data-playerId="' . $R[ $columnas[ $indi ] ] . '" data-fancybox-type="iframe">Play the video</a></td>';
						} else {
							if ( isset( $tipoHtml ) ) {
								$informacion = ( in_array( $columnas[ $indi ], $tipoHtml ) ) ? '<div class="contentHTML">' . mb_substr( strip_tags( html_entity_decode( $R[ $columnas[ $indi ] ] ) ), 0, 200, 'utf-8' ) . '</div>' : $R[ $columnas[ $indi ] ];
								if ( !in_array( $columnas[ $indi ], $tipoHtml ) && ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) || preg_match('/\jpeg\b/', $informacion ) || preg_match('/\JPG\b/', $informacion ) ) ) {
									$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
									$fila .= '<td class="tcenter">' . $informacion . '</td>';
								} else {
									if ( isset( $tipoPass ) && $informacion != '' ) {
										if ( in_array( $columnas[ $indi ], $tipoPass ) && ( $tabla == 'coaches' || $tabla == 'users' ) ) {
											$informacion = '<a onclick="enviaPass( \'' . $R[ 'email' ] . '\', \'' . $tabla . '\' )">Re-send Password</a>';
										} else {
											$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
										}
									}
									if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
										$ligaTiene = true;
										$informacion = '<a href="' . $informacion . '" target="_blank" class="ligaAdmin">' . $informacion . '</a>';
									}
									if ( isset( $tipoCheck ) && in_array( $columnas[ $indi ], $tipoCheck ) ) {
										$informacion = ( $informacion == 'S' ) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
									}
									if ( isset( $tipoIcono ) && in_array( $columnas[ $indi ], $tipoIcono ) ) {
										$informacion = '<i class="' . $informacion . '"></i>';
									}
									if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
										$datos = $opcionesCampos[ $columnas[ $indi ] ];
										if ( $informacion > 0 ) {
											$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>Campo Opciones Actual';
												var_dump( $datos );
												echo $campoNombre . '<br>';
											}
											if ( $campoNombre != false ) {
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo 'Si trae campo nombre<br>';
												}
												$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
												$resOpcion = $con->Consulta( $strOpcion );
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo $strOpcion;
												}
												if ( $con->Cuantos( $resOpcion ) > 0 ) {
													$OP = $con->Resultados( $resOpcion );
													$informacion = $OP[ $campoNombre ];
												}
											}
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>';
											}
										} else {
											$informacion = 'Unassigned';
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>No se asigno valor a campo opciones<hr>';
											}
										}
									}
									$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . mb_substr( $informacion, 0, 200, 'utf-8' ) . ' ...</div>' ) : $informacion;
									$fila .= '<td>' . $informacion . '</td>';
								}
							} else {
								$informacion = $R[ $columnas[ $indi ] ];
								if ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) ) {
									$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
									$fila .= '<td class="tcenter">' . $informacion . '</td>';
								} else {
									if ( isset( $tipoPass ) && $informacion != '' ) {
										if ( in_array( $columnas[ $indi ], $tipoPass ) && ( $tabla == 'coaches' || $tabla == 'users' ) ) {
											$informacion = '<a onclick="enviaPass( \'' . $R[ 'email' ] . '\', \'' . $tabla . '\' )">Re-send Password</a>';
										} else {
											$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
										}
									}
									if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
										$ligaTiene = true;
										$informacion = '<a href="' . $informacion . '" target="_blank" class="ligaAdmin">' . $informacion . '</a>';
									}
									if ( isset( $tipoCheck ) && in_array( $columnas[ $indi ], $tipoCheck ) ) {
										$informacion = ( $informacion == 'S' ) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
									}
									if ( isset( $tipoIcono ) && in_array( $columnas[ $indi ], $tipoIcono ) ) {
										$informacion = '<i class="' . $informacion . '"></i>';
									}
									if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
										$datos = $opcionesCampos[ $columnas[ $indi ] ];
										if ( $informacion > 0 ) {
											$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>Campo Opciones Actual';
												var_dump( $datos );
												echo $campoNombre . '<br>';
											}
											if ( $campoNombre != false ) {
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo 'Si trae campo nombre<br>';
												}
												$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
												$resOpcion = $con->Consulta( $strOpcion );
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo $strOpcion;
												}
												if ( $con->Cuantos( $resOpcion ) > 0 ) {
													$OP = $con->Resultados( $resOpcion );
													$informacion = $OP[ $campoNombre ];
												}
											}
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>';
											}
										} else {
											$informacion = 'Unassigned';
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>No se asigno valor a campo opciones<hr>';
											}
										}
									}
									$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . mb_substr( $informacion, 0, 200, 'utf-8' ) . ' ...</div>' ) : $informacion;
									$fila .= '<td>' . $informacion . '</td>';
								}
							}
						}
					}
				}
			}
			if ( $posicionCampo != '' ) {
				$fila .= '<td>' . ( $i + 1 ) . '</td>';
			}
			if ( $tabla == 'coaches' || $tabla == 'users' ) {
				if ( $tabla == 'coaches' ) {
					$funcion = 'ajustaStatus';
					$resClientCoach = $con->Consulta( 'select * from coachesclients where coachId=' . $R[ $campoId ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
				} else {
					$funcion = 'ajustaStatusMiembro';
					$resClientCoach = $con->Consulta( 'select * from userclient where userId=' . $R[ $campoId ] . ' and clientId=' . $_REQUEST[ 'clientId' ] );
				}
				$CC = $con->Resultados( $resClientCoach );
				$fila .=
				'<td>
					<select id="pending-' . $R[ $campoId ] . '" onchange="' . $funcion . '( ' . $R[ $campoId ] . ', this.value )">
						<option value="N">Approve</option>
						<option value="S">Pending</option>
						<option value="R">Reject</option>
					</select>
					<script>$( \'#pending-' . $R[ $campoId ] . '\' ).val( \'' . $CC[ 'pending' ] . '\' );</script>
				</td>';
			}
			$fila .= '</tr>';
			$i++;
			$html .= $fila;
		}
		if( $i == 0 ) {
			$html = '<td colspan="' . $filas . '">No hay datos agregados actualmente</td>';
		}
		$status = array( 'status' => 'Success', 'html' => $html, 'posicionCampo' => $posicionCampo, 'id' => $idUnico );
	}
	$con->CierraConexion();
	echo json_encode( $status, JSON_UNESCAPED_UNICODE );
	exit();
?>