<?php
	include '../../panel/querys/conexion.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '<hr><h4>Filter\'s Value</h4>';
	$i = 0;
	$str = 'select * from propiedadvideo c inner join valores i on(c.valorId=i.valorId) where i.propiedadId=' . $_REQUEST[ 'filtro' ] . ' and c.videoId=' . $_REQUEST[ 'video' ];
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$html .=
		'<div class="row" id="info-' . $R[ 'valorId' ] . '" style="margin-top: 5px;">
			<div class="col-lg-11" style="width: 90%; float: left;">
				<label class="field select setenta">
					<select id="infoSelect-' . $i . '" class="informacion">';
				$html .= '<option value="0">Pick a property value</option>';
				$str1 = 'select * from valores where propiedadId=' . $_REQUEST[ 'filtro' ];
				$res1 = $con->Consulta( $str1 );
				while( $O = $con->Resultados( $res1 ) ) {
					$elegido = ( $O[ 'valorId' ] == $R[ 'valorId' ] ) ? 'selected' : '';
					$html .= '<option value="' . $O[ 'valorId' ] . '" ' . $elegido . '>' . $O[ 'valor' ] . '</option>';
				}
				$html .= '</select><i class="arrow"></i>
				</label>
				<a class="addi" onclick="editValor( $( \'#infoSelect-' . $i . '\' ).val() )"><i class="fa fa-edit"></i></a>
				<a class="addi" onclick="eliminaValor( $( \'#infoSelect-' . $i . '\' ).val() )"><i class="fa fa-minus"></i></a>
				<a class="addi" onclick="agregaValor()"><i class="fa fa-plus"></i></a>
			</div>
			<a class="addi" onclick="eliminaInfo( ' . $R[ 'propVideoId' ] . ' )"><i class="fa fa-trash"></i></a>
		</div>';
		$i++;
	}
	if( $i == 0 ) {
		$status = array( 'status' => 'Error' );
	} else {
		$status = array( 'status' => 'Success', 'html' => $html, 'cuantos' => $i );
	}
	echo json_encode( $status );
	$con->CierraConexion();
?>