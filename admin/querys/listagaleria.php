<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$tabla = ( $_REQUEST[ 'tabla' ] == 'videos' ) ? 'galeria' : 'galeriaproducto';
	$campo = ( $_REQUEST[ 'tabla' ] == 'videos' ) ? 'videoId' : 'productoId';
	$str = 'select * from ' . $tabla . ' where ' . $campo . '=' . $_REQUEST[ 'id' ];
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$html .=
		'<div class="col-lg-4" id="imagen-' . $R[ 'galeriaId' ] . '">
			<i class="fa fa-close iconoBorra" onclick="eliminaGaleria( ' . $R[ 'galeriaId' ] . ', \'' . $_REQUEST[ 'tabla' ] . '\' )"></i>
			<div class="capagaleria">
				<img src="../images/' . $tabla . '/' . $R[ 'imagen' ] . '" class="fullimg">
			</div>
		</div>';
	}
	if( $html == '' ) {
		$html = '<h3 style="text-align: center;">No hay Imagenes agregadas</h3>';
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>