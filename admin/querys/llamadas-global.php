<?php
	include '../../panel/querys/conexion.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	if ( isset( $_REQUEST[ 'coachId' ] ) ) {
		$res = $con->Consulta( 'select c.*, IF(c.fecha>=now() AND c.fecha<=DATE_ADD(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(c.fecha) as fechas, Time(c.fecha) as tiempo, ct.nombre as cliente from calls c inner join clients ct on(c.clientId=ct.clientId) where c.coachId=' . $_REQUEST[ 'coachId' ] . ' or allCoaches="S"' );
		while( $R = $con->Resultados( $res ) ) {
			$type = $R[ 'tipoId' ] == 1 ? 'Public' : 'Private';
			$persona = 'N/A';
			if ( $R[ 'tipoId' ] == 2 ) {
				$resUser = $con->Consulta( 'select us.apellido, us.nombre from usercall u inner join users us on(u.userId=us.userId) where u.callId=' . $R[ 'callId' ] );
				while( $U = $con->Resultados( $resUser ) ) {
					$persona = $U[ 'apellido' ] . ' ' . $U[ 'nombre' ];
				}
			}
			$enlace = ( is_null( $R[ 'url' ] ) ) ? '<a onclick="statusCall( 1, ' . $R[ 'callId' ] . ' )"><i class="fas fa-check"></i> Approve</a> | <a onclick="statusCall( 2, ' . $R[ 'callId' ] . ' )"><i class="fas fa-times"></i> Reject</a>' : ( ( $R[ 'disponible' ] == 'S' ) ? '<a href="' . $R[ 'url' ] . '" target="_blank">Join Meeting</a>' : 'Approved' );
			$html .= '<tr><td>' . $R[ 'title' ] . '</td><td>' . $R[ 'description' ] . '</td><td>' . $persona . '</td><td>' . $enlace . '</td><td>' . $type . '</td><td>' . $R[ 'fechas' ] . '</td><td>' . $R[ 'tiempo' ] . '</td><td>' . $R[ 'duration' ] . '</td></tr>';
		}
	}
	if ( isset( $_REQUEST[ 'userId' ] ) ) {
		$res = $con->Consulta( 'select *, IF(fecha>=now() AND fecha<=DATE_ADD(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(fecha) as fechas, Time(fecha) as tiempo from calls' );
		while( $R = $con->Resultados( $res ) ) {
			$permitido = false;
			$resUser = $con->Consulta( 'select * from usercall where userId=' . $_REQUEST[ 'userId' ] . ' and callId=' . $R[ 'callId' ] );
			while( $U = $con->Resultados( $resUser ) ) {
				$permitido = true;
			}
			if ( $permitido || $R[ 'allMembers' ] == 'S' ) {
				$enlace = ( is_null( $R[ 'url' ] ) ) ? 'Requested' : ( ( $R[ 'disponible' ] == 'S' ) ? '<a href="' . $R[ 'url' ] . '" target="_blank">Join Meeting</a>' : 'Approved' );
				$html .= '<tr><td>' . $R[ 'title' ] . '</td><td>' . $enlace . '</td><td>' . $R[ 'fechas' ] . '</td><td>' . $R[ 'tiempo' ] . '</td></tr>';
			}
		}
	}
	if ( $html == '' ) {
		$html .= '<tr><td colspan="4" align="center">There is no currently calls scheduled</td></tr>';
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>