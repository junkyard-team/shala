<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$cuantasColumnas = 4;
	if ( isset( $_REQUEST[ 'coachId' ] ) ) {
		$res = $con->Consulta( 'select *, IF(fecha>=now(), "S","N") as invitable, IF(fecha<=now() AND fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(fecha) as fechas, Time(fecha) as tiempo from calls where clientId=' . $_REQUEST[ 'clientId' ] . ' and ( coachId=' . $_REQUEST[ 'coachId' ] . ' or allCoaches="S" ) order by callId desc' );
		while( $R = $con->Resultados( $res ) ) {
			switch ( $R[ 'tipoId' ] ) {
				case 1: $type = 'Public'; break;
				case 2: $type = 'Private'; break;
				case 3: $type = 'Registered Users'; break;
				case 4: $type = 'Circle'; break;
			}
			$persona = 'N/A';
			if ( $R[ 'tipoId' ] == 2 ) {
				$resUser = $con->Consulta( 'select us.apellido, us.nombre from usercall u inner join users us on(u.userId=us.userId) where u.callId=' . $R[ 'callId' ] );
				while( $U = $con->Resultados( $resUser ) ) {
					$persona = $U[ 'apellido' ] . ' ' . $U[ 'nombre' ];
				}
			}
			$tiempoModificado = ( !is_null( $R[ 'timezone' ] ) ) ? zoneConvertion( $R[ 'fecha' ], $R[ 'timezone' ], $_REQUEST[ 'timeZone' ] ) : $R[ 'fecha' ];
			$piezaFecha = explode( ' ', $tiempoModificado );
			$imagenCall = ( !is_null( $R[ 'image' ] ) && $R[ 'image' ] != '' ) ? '<img src="thumb?src=images/calls/' . $R[ 'image' ] . '&size=70x50">' : '';
			$enlace = ( is_null( $R[ 'url' ] ) ) ? '<a onclick="statusCall( 1, ' . $R[ 'callId' ] . ' )"><i class="fas fa-check"></i> Approve</a> | <a onclick="statusCall( 2, ' . $R[ 'callId' ] . ' )"><i class="fas fa-times"></i> Reject</a>' : ( ( $R[ 'disponible' ] == 'S' ) ? '<a href="' . $R[ 'url' ] . '" target="_blank">Join Meeting</a>' : 'Approved' );
			$invita = ( $R[ 'tipoId' ] == 2 && $R[ 'invitable' ] == 'S' ) ? '<a onclick="sendInvitation( ' . $R[ 'callId' ] . ' )">Send Invitations</a>' : '---';
			$html .=
			'<tr>
				<td><i class="fas fa-trash cPointer" onclick="borraLlamada( ' . $R[ 'callId' ] . ' )"></i><i class="fas fa-edit cPointer" onclick="editaLlamada( ' . $R[ 'callId' ] . ' )"></i></td>
				<td>' . $imagenCall . '</td>
				<td>' . $R[ 'title' ] . '</td>
				<td>' . $R[ 'description' ] . '</td>
				<td>' . $persona . '</td>
				<td>' . $enlace . '</td>
				<td>' . $type . '</td>
				<td>' . $piezaFecha[0] . '</td>
				<td>' . $piezaFecha[1] . '</td>
				<td>' . $R[ 'duration' ] . '</td>
				<td>' . $invita . '</td>
			</tr>';
		}
		$cuantasColumnas = 9;
	}
	if ( isset( $_REQUEST[ 'userId' ] ) ) {
		$res = $con->Consulta( 'select *, IF(fecha<=now() AND fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(fecha) as fechas, Time(fecha) as tiempo from calls where clientId=' . $_REQUEST[ 'clientId' ] );
		while( $R = $con->Resultados( $res ) ) {
			$permitido = false;
			$resUser = $con->Consulta( 'select * from usercall where userId=' . $_REQUEST[ 'userId' ] . ' and callId=' . $R[ 'callId' ] );
			while( $U = $con->Resultados( $resUser ) ) {
				$permitido = true;
			}
			if ( $permitido || $R[ 'allMembers' ] == 'S' ) {
				$tiempoModificado = zoneConvertion( $R[ 'fecha' ], $R[ 'timezone' ], $_REQUEST[ 'timeZone' ] );
				$piezaFecha = explode( ' ', $tiempoModificado );
				$enlace = ( is_null( $R[ 'url' ] ) ) ? 'Requested' : ( ( $R[ 'disponible' ] == 'S' ) ? '<a href="' . $R[ 'url' ] . '" target="_blank">Join Meeting</a>' : 'Approved' );
				$html .= '<tr><td><i class="fas fa-edit cPointer" onclick="editaLlamada( ' . $R[ 'callId' ] . ' )"></i></td><td>' . $R[ 'title' ] . '</td><td>' . $enlace . '</td><td>' . $piezaFecha[0] . '</td><td>' . $piezaFecha[1] . '</td></tr>';
			}
		}
	}
	if ( $html == '' ) {
		$html .= '<tr><td colspan="' . $cuantasColumnas . '" align="center">There is no currently sessions scheduled</td></tr>';
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>