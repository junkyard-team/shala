<?php
	include 'cabecera.php';
	$R = obtenCoachData( $_REQUEST[ 'url' ], $con );
?>
<div class="coachHead" style="background-image: url( 'images/coaches/<?php echo $R[ 'header' ]; ?>' );">
	<div class="contenidoHead">
		<h1><?php echo $R[ 'nombre' ] . ' ' . $R[ 'apellido' ]; ?></h1>
		<div class="socialNetwork">
		<?php if ( !is_null( $R[ 'facebook' ] ) && $R[ 'facebook' ] != '' ) : ?>
			<a href="<?php echo $R[ 'facebook' ]; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
		<?php endif; ?>
		<?php if ( !is_null( $R[ 'twitter' ] ) && $R[ 'twitter' ] != '' ) : ?>
			<a href="<?php echo $R[ 'twitter' ]; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
		<?php endif; ?>
		<?php if ( !is_null( $R[ 'web' ] ) && $R[ 'web' ] != '' ) : ?>
			<a href="<?php echo $R[ 'web' ]; ?>" target="_blank"><i class="fas fa-globe"></i></a>
		<?php endif; ?>
		</div>
	</div>
</div>
<div class="seccion borderBottom">
	<div class="principal">
		<div class="infoCoach">
			<div class="imagenProfile">
				<div class="imgCentro">
					<img src="thumb?src=images/coaches/<?php echo $R[ 'foto' ]; ?>&size=200x200" class="fullImg">
				</div>
			</div>
			<div class="textoCoach">
				<div><?php echo $R[ 'biografia' ]; ?></div>
				<br>
				<div>
				<?php if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) : ?>
				<a class="tCenter coachLink" onclick="muestraVideo( '<?php echo $R[ 'video' ]; ?>' )">View Video Profile</a>
				<?php endif; ?>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>
<div class="seccion margen sessionList maestroDentro padShort" coach="<?php echo $R[ 'coachId' ]; ?>">
	<div class="principal">
		<?php
			$res = $con->Consulta( 'select *, IF(fecha<=now() AND fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible from calls where fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR) and tipoId=1 and clientId=' . $clientId . ' and coachId=' . $R[ 'coachId' ] . ' order by fecha asc' );
			if ( $con->Cuantos( $res ) > 0 ) {
				echo '<h1 class="titulo tCenter centraDiv">' . $R[ 'nombre' ] . '\'s Upcoming Sessions</h1><br><div class="swiper-container"><div class="swiper-wrapper">';
				while( $S = $con->Resultados( $res ) ) {
					$enlace = ( is_null( $S[ 'url' ] ) ) ? 'Pending Approval' : ( ( $S[ 'disponible' ] == 'S' ) ? '<a href="' . $S[ 'url' ] . '" target="_blank">Join now</a>' : 'Waiting for Sessions\'s time' );
					$imagen = ( !is_null( $S[ 'image' ] ) && $S[ 'image' ] != '' ) ? 'images/calls/' . $S[ 'image' ] : 'images/coach-call.jpg';
					echo
					'<div class="swiper-slide categoriaHome meetings">
						<img src="thumb?src=' . $imagen . '&size=300x450" class="fullImg fotoMeet">
						<p class="tiempoMeet">' . formatoFechaTiempo( $S[ 'fecha' ], 'ES' ) . '</p>
						<h4>' . $S[ 'title' ] . '</h4>
						<p class="link">' . $enlace . '</p>
					</div>';
				}
				echo '</div></div>';
			}
		?>
	</div>
</div>
<div class="seccion">
	<div class="principal">
		<?php
			$query = $con->Consulta( 'select * from series where coachId=' . $R[ 'coachId' ] . ' order by nombre asc' );
			if ( $con->Cuantos( $query ) > 0 ) {
				echo '<h2 class="serieTitulo">' . $R[ 'nombre' ] . '\'s Series</h2><div class="fila">';
				while( $S = $con->Resultados( $query ) ) {
					$imagenSerie = ( !is_null( $S[ 'photo' ] ) && $S[ 'photo' ] != '' ) ? 'images/series/' . $S[ 'photo' ] : 'images/video-general.jpg';
					$intoVideo = ( !is_null( $S[ 'video' ] ) && $S[ 'video' ] != '' ) ? '<a onclick="muestraVideo( \'' . $S[ 'video' ] . '\' )" class="introLink"><i class="fas fa-play"></i> VIEW INTRO</a>' : '';
					echo
					'<div class="cincuenta cPadBig categoriaHome videoCoach">
						<a href="serie/' . limpialo( $S[ 'nombre' ], 'min' ) . '">
							<img src="thumb?src=' . $imagenSerie . '&size=400x240" class="fullImg" style="border: 1px solid #000">
						</a>
						<div class="fila">
							<div class="sesenta"><h3 class="tituloSerie"><a href="serie/' . limpialo( $S[ 'nombre' ], 'min' ) . '">' . $S[ 'nombre' ] . '</a></h3></div>
							<div class="cuarenta tRight">' . $intoVideo . '</div>
						</div>
						<p>' . $S[ 'description' ] . '</p>
					</div>';
				}
				echo '</div>';
			}
		?>
	</div>
</div>
<?php include 'pie.php'; ?>