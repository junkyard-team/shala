<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tablaReal' ];
	$campoIdPadre = '';
	$campoId = '';
	$campoNombre = '';
	$opcionesHtml = '<option value="-1">Pick an option</option>';
	if ( isset( $_REQUEST[ 'tablaPadre' ] ) ) {
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $_REQUEST[ 'tablaPadre' ] . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'S' ) {
				$campoIdPadre = $R[ 'columna' ];
			}
		}
	}
	$idiomaCampo = '';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'nombre' ) {
			$campoNombre = $R[ 'columna' ];
		}
		if ( preg_match('/\bidioma\b/', $R[ 'comentarios' ] ) ) {
			$idiomaCampo = $R[ 'columna' ];
		}
	}
	$orderIdioma = ( $idiomaCampo != '' ) ? $idiomaCampo . ' asc, ' : '';
	if ( $campoIdPadre == '' ) {
		$str = 'select Distinct ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' order by ' . $orderIdioma . $campoNombre . ' asc';
	} else {
		$str = 'select Distinct ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' where ' . $campoIdPadre . '=' . $_REQUEST[ 'idPadre' ] . ' order by ' . $orderIdioma . $campoNombre . ' asc';
	}
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$opcionesHtml .= '<option value="' . $R[ $campoId ] . '">' . $R[ $campoNombre ] . '</option>';
	}
	$status = array( 'status' => 'Success', 'opciones' => $opcionesHtml );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>