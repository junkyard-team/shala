<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_KEY="PRI"';
	$res = $con->Consulta( $str );
	$fieldId = '';
	while( $R = $con->Resultados( $res ) ) {
		$fieldId = $R[ 'columna' ];
	}
	if ( $fieldId != '' ) {
		$strNew = 'select ' . $_REQUEST[ 'campo' ] . ' from ' . $tabla . ' where ' . $fieldId . '=' . $_REQUEST[ 'id' ];
		$resNew = $con->Consulta( $strNew );
		$R = $con->Resultados( $resNew );
		$strNew = 'update ' . $tabla . ' set ' . $_REQUEST[ 'campo' ] . '="" where ' . $fieldId . '=' . $_REQUEST[ 'id' ];
		$resNew = $con->Consulta( $strNew );
		if ( $resNew ) {
			$status = array( 'status' => 'Success' );
			$archivoQuita = '../../images/' . $tabla . '/' . $R[ $_REQUEST[ 'campo' ] ];
			if ( file_exists( $archivoQuita ) ) {
				unlink( $archivoQuita );
			}
		} else {
			$status = array( 'status' => 'Error' );
		}
	} else {
		$status = array( 'status' => 'Error' );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>