<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$permitidoId = array();
	$tabla = ( isset( $_REQUEST[ 'studentId' ] ) ) ? 'usercall' : 'coachcall';
	$campoId = ( isset( $_REQUEST[ 'studentId' ] ) ) ? 'userId' : 'coachId';
	$id = ( isset( $_REQUEST[ 'studentId' ] ) ) ? $_REQUEST[ 'studentId' ] : $_REQUEST[ 'coachId' ];
	$resUser = $con->Consulta( 'select * from ' . $tabla . ' where ' . $campoId . '=' . $id );
	while( $U = $con->Resultados( $resUser ) ) {
		$permitidoId[] = $U[ 'callId' ];
	}
	if ( !empty( $permitidoId ) ) {
		$res = $con->Consulta( 'select *, IF(fecha<=now() AND fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(fecha) as fechas, Time(fecha) as tiempo from calls where clientId=' . $_REQUEST[ 'clientId' ] . ' and callId in ( ' . implode( ',', $permitidoId ). ' ) and fecha>=now() order by callId desc' );
		while( $R = $con->Resultados( $res ) ) {
			$resCoach = $con->Consulta( 'select nombre, apellido from coaches where coachId=' . $R[ 'coachId' ] );
			$C = $con->Resultados( $resCoach );
			$imagenCall = ( !is_null( $R[ 'image' ] ) && $R[ 'image' ] != '' ) ? '<img src="thumb?src=images/calls/' . $R[ 'image' ] . '&size=70x50" onclick="traeLlamada( ' . $R[ 'callId' ] . ' )">' : '';
			$tiempoModificado = zoneConvertion( $R[ 'fecha' ], $R[ 'timezone' ], $_REQUEST[ 'timeZone' ] );
			$piezaFecha = explode( ' ', $tiempoModificado );
			$html .=
			'<tr>
				<td><i class="fas fa-trash cPointer" onclick="registraLlamada( ' . $R[ 'callId' ] . ' )"></i> <i class="fas fa-search" onclick="traeLlamada( ' . $R[ 'callId' ] . ' )"></i></td>
				<td align="center">' . $piezaFecha[ 0 ] . '</td>
				<td align="center">' . $piezaFecha[ 1 ] . '</td>
				<td align="center">' . $R[ 'duration' ] . '</td>
				<td align="center">' . $R[ 'title' ] . '</td>
				<td align="center">' . $C[ 'nombre' ] . ' ' . $C[ 'apellido' ] . '</td>
				<td align="center">' . $imagenCall . '</td>
			</tr>';
		}
	}
	if ( isset( $_REQUEST[ 'coachId' ] ) ) {
		$res = $con->Consulta( 'select *, IF(fecha<=now() AND fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR),"S","N") as disponible, Date(fecha) as fechas, Time(fecha) as tiempo from calls where clientId=' . $_REQUEST[ 'clientId' ] . ' and coachId=' . $_REQUEST[ 'coachId' ] . ' and fecha>=now() order by callId desc' );
		while( $R = $con->Resultados( $res ) ) {
			$resCoach = $con->Consulta( 'select nombre, apellido from coaches where coachId=' . $R[ 'coachId' ] );
			$C = $con->Resultados( $resCoach );
			$imagenCall = ( !is_null( $R[ 'image' ] ) && $R[ 'image' ] != '' ) ? '<img src="thumb?src=images/calls/' . $R[ 'image' ] . '&size=70x50" onclick="traeLlamada( ' . $R[ 'callId' ] . ' )">' : '';
			$enlace = '<a onclick="traeLlamada( ' . $R[ 'callId' ] . ' )"><i class="fas fa-search"></i> View Details</a>';
			$tiempoModificado = zoneConvertion( $R[ 'fecha' ], $R[ 'timezone' ], $_REQUEST[ 'timeZone' ] );
			$piezaFecha = explode( ' ', $tiempoModificado );
			$html .=
			'<tr>
				<td><i class="fas fa-trash cPointer" onclick="registraLlamada( ' . $R[ 'callId' ] . ' )"></i> <i class="fas fa-search" onclick="traeLlamada( ' . $R[ 'callId' ] . ' )"></i></td>
				<td align="center">' . $piezaFecha[ 0 ] . '</td>
				<td align="center">' . $piezaFecha[ 1 ] . '</td>
				<td align="center">' . $R[ 'duration' ] . '</td>
				<td align="center">' . $R[ 'title' ] . '</td>
				<td align="center">' . $C[ 'nombre' ] . ' ' . $C[ 'apellido' ] . '</td>
				<td align="center">' . $imagenCall . '</td>
			</tr>';
		}
	}
	if ( $html == '' ) {
		$html .= '<tr><td colspan="8" align="center">There is no currently sessions scheduled</td></tr>';
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>