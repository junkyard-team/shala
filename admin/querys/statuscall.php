<?php
	include '../../panel/querys/conexion.php';
	$con = new Conexion();
	$con->AbreConexion();
	$http = ( isset( $_SERVER[ 'HTTPS' ] ) ) ? 'https' : 'http';
	$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
	if ( $_REQUEST[ 'tipo' ] == 1 ) {
		$resCall = $con->Consulta( 'select * from calls where callId=' . $_REQUEST[ 'callId' ] );
		$C = $con->Resultados( $resCall );
		if ( isset( $_REQUEST[ 'token' ] ) ) {
			include 'zoom-class.php';
			$zoomClass = new ZoomPHP( $base );
			if ( !is_null( $C[ 'zoomId' ] ) && $C[ 'zoomId' ] != '' ) {
				$zoomMeeting = $zoomClass->deleteMeeting( $C[ 'zoomId' ], $_REQUEST[ 'token' ] );
				$resDel = $con->Consulta( 'update calls set url=null, zoomId=null where callId=' . $_REQUEST[ 'callId' ] );
			}
			try {
				$meetingData = array(
					'start_date' => $C[ 'fecha' ],
					'meetingTopic' => $C[ 'title' ],
					'duration' => $C[ 'duration' ],
					'timezone' => $C[ 'timezone' ]
				);
				$zoomMeeting = $zoomClass->createMeeting( $meetingData, $_REQUEST[ 'token' ] );
				if ( isset( $zoomMeeting->start_url ) ) {
					$res = $con->Consulta( 'update calls set url="' . $zoomMeeting->start_url . '", zoomId="' . $zoomMeeting->id . '" where callId=' . $_REQUEST[ 'callId' ] );
					$status = array( 'status' => 'Success', 'zoom' => $zoomMeeting );
				} else {
					$status = array( 'status' => 'Error', 'zoom' => $zoomMeeting );
				}
		    } catch ( Exception $ex ) {
		        $status = array( 'status' => 'Error', 'zoom' => $ex );
		    }
		} else {
			$res = $con->Consulta( 'select * from clients where clientId=' . $_REQUEST[ 'clientId' ] );
			$R = $con->Resultados( $res );
			include '../../zoom/zoom.php';
			$zoom_meeting = new Zoom_Api( $R[ 'zoomPublic' ], $R[ 'zoomSecret' ] );
			if ( !is_null( $C[ 'zoomId' ] ) && $C[ 'zoomId' ] != '' ) {
				$zoomMeeting = $zoom_meeting->deleteMeeting( $C[ 'zoomId' ] );
				$resDel = $con->Consulta( 'update calls set url=null, zoomId=null where callId=' . $_REQUEST[ 'callId' ] );
			}
			try {
				$date = new DateTime();
				$timeZone = $date->getTimezone();
				$z = $zoom_meeting->createAMeeting(
					array(
						'start_date' => $C[ 'fecha' ],
						'meetingTopic' => $C[ 'title' ],
						'duration' => $C[ 'duration' ],
						'timezone' => $C[ 'timezone' ]
					)
				);
				if ( isset( $z->start_url ) ) {
					$res = $con->Consulta( 'update calls set url="' . $z->start_url . '", zoomId="' . $z->id . '" where callId=' . $_REQUEST[ 'callId' ] );
					$status = array( 'status' => 'Success', 'zoom' => $z );
				} else {
					$status = array( 'status' => 'Error', 'zoom' => $z );
				}
		    } catch ( Exception $ex ) {
		        $status = array( 'status' => 'Error', 'zoom' => $ex );
		    }
		}
	} else if ( $_REQUEST[ 'tipo' ] == 2 ) {
		$res = $con->Consulta( 'update calls set url=null, zoomId=null where callId=' . $_REQUEST[ 'callId' ] );
		if ( $res ) {
			$status = array( 'status' => 'Success' );
		} else {
			$status = array( 'status' => 'Error' );
		}
	} else {
		$res = $con->Consulta( 'delete from calls where callId=' . $_REQUEST[ 'callId' ] );
		if ( $res ) {
			$status = array( 'status' => 'Success' );
		} else {
			$status = array( 'status' => 'Error' );
		}
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>