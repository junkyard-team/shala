<?php
	include 'functions.php';
	if ( $_FILES ) {
		foreach ( $_FILES as $file => $array ) {
			$allowed = array( 'png', 'jpg', 'gif', 'pdf', 'jpeg' );
			if ( isset( $_FILES[ $file ] ) && $_FILES[ $file ][ 'error' ] == 0 ) {
				$extension = strtolower( pathinfo( $_FILES[ $file ][ 'name' ], PATHINFO_EXTENSION ) );
				if ( !in_array( $extension, $allowed ) ) {
					$status = array( 'status' => 'Error', 'archivo' => 'No es imagen' );
				} else {
					$ruta = '../../images/summernote/';
					if ( !file_exists( $ruta ) ) {
    					mkdir( $ruta, 0777 );
    				}
					$temporal = str_replace( $extension, '', $_FILES[ $file ][ 'name' ] );
					$nombre_fichero = limpialo( $temporal, 'min' ) . '.' . $extension;
					if ( move_uploaded_file( $_FILES[ $file ][ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
						$urlImagen = $_REQUEST[ 'baseUrl' ] . '/images/summernote/' . $nombre_fichero;
						$status = array( 'status' => 'Success', 'url' => $urlImagen );
					} else {
						$status = array( 'status' => 'Error', 'archivo' => 'No Guardo Archivo' );
					}
				}
			} else {
				$status = array( 'status' => 'Error', 'archivo' => 'Subio mal' );
			}
		}
	}
	echo json_encode( $status );
	exit();
?>