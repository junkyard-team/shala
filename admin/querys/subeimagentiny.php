<?php
	include 'functions.php';
	if ( $_FILES ) {
		foreach ( $_FILES as $file => $array ) {
			$allowed = array( 'png', 'jpg', 'gif', 'pdf', 'jpeg' );
			if ( isset( $_FILES[ $file ] ) && $_FILES[ $file ][ 'error' ] == 0 ) {
				$extension = strtolower( pathinfo( $_FILES[ $file ][ 'name' ], PATHINFO_EXTENSION ) );
				if ( !in_array( $extension, $allowed ) ) {
					$status = array( 'status' => 'Error', 'archivo' => 'No es imagen' );
				} else {
					$ruta = '../../images/tinymce/';
					if ( !file_exists( $ruta ) ) {
    					mkdir( $ruta, 0777 );
    				}
					$temporal = str_replace( $extension, '', $_FILES[ $file ][ 'name' ] );
					$nombre_fichero = limpialo( $temporal, 'min' ) . '.' . $extension;
					if ( move_uploaded_file( $_FILES[ $file ][ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
						$http = ( isset( $_SERVER[ 'HTTPS' ] ) ) ? 'https' : 'http';
						$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
						$urlImagen = $base . '/images/tinymce/' . $nombre_fichero;
						$status = array( 'status' => 'Success', 'location' => $urlImagen );
						echo json_encode( $status );
						exit();
					} else {
						header( 'HTTP/1.1 500 Server Error' );
					}
				}
			} else {
				header( 'HTTP/1.1 500 Server Error' );
			}
		}
	}
?>