<?php
	include '../../panel/querys/conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$cuantasColumnas = 4;
	$destinatarios = array();
	$coaches = array();
	$users = array();
	$res = $con->Consulta( 'select *, Date(fecha) as fechas, Time(fecha) as tiempo from calls where callId=' . $_REQUEST[ 'callId' ] );
	$R = $con->Resultados( $res );
	$resUser = $con->Consulta( 'select u.userId, us.apellido, us.nombre, us.email from usercall u inner join users us on(u.userId=us.userId) where u.callId=' . $R[ 'callId' ] );
	while( $U = $con->Resultados( $resUser ) ) {
		if ( !in_array( $U[ 'userId' ], $users ) ) {
			$destinatarios[] = array( 'nombre' => $U[ 'apellido' ] . ' ' . $U[ 'nombre' ], 'email' => $U[ 'email' ] );
			$users[] = $U[ 'userId' ];
		}
	}
	$resUser = $con->Consulta( 'select u.coachId, us.apellido, us.nombre, us.email from coachcall u inner join coaches us on(u.coachId=us.coachId) where u.callId=' . $R[ 'callId' ] );
	while( $U = $con->Resultados( $resUser ) ) {
		if ( !in_array( $U[ 'coachId' ], $coaches ) ) {
			$destinatarios[] = array( 'nombre' => $U[ 'apellido' ] . ' ' . $U[ 'nombre' ], 'email' => $U[ 'email' ] );
			$coaches[] = $U[ 'coachId' ];
		}
	}
	$resCircle = $con->Consulta( 'select * from callscircle where callId=' . $_REQUEST[ 'callId' ] );
	while( $C = $con->Resultados( $resCircle ) ) {
		$resUser = $con->Consulta( 'select u.userId, us.apellido, us.nombre, us.email from usercircle u inner join users us on(u.userId=us.userId) where u.circleId=' . $C[ 'circleId' ] );
		while( $U = $con->Resultados( $resUser ) ) {
			if ( !in_array( $U[ 'userId' ], $users ) ) {
				$destinatarios[] = array( 'nombre' => $U[ 'apellido' ] . ' ' . $U[ 'nombre' ], 'email' => $U[ 'email' ] );
				$users[] = $U[ 'userId' ];
			}
		}
		$resUser = $con->Consulta( 'select u.coachId, us.apellido, us.nombre, us.email from coachcircle u inner join coaches us on(u.coachId=us.coachId) where u.circleId=' . $C[ 'circleId' ] );
		while( $U = $con->Resultados( $resUser ) ) {
			if ( !in_array( $U[ 'coachId' ], $coaches ) ) {
				$destinatarios[] = array( 'nombre' => $U[ 'apellido' ] . ' ' . $U[ 'nombre' ], 'email' => $U[ 'email' ] );
				$coaches[] = $U[ 'coachId' ];
			}
		}
	}
	if ( !empty( $destinatarios ) ) {
		$res1 = $con->Consulta( 'select * from clients where clientId=' . $_REQUEST[ 'clientId' ] );
		$T = $con->Resultados( $res1 );
		$textos = getEmailText( 13, $_REQUEST[ 'clientId' ], $con );
		$url = '?location=' . $T[ 'nombre' ] . ' Web&description='. $R[ 'title' ] . ' session&fecha=' . $R[ 'fecha' ] . '&summary=Scheduled session with ' . obtenCoachName( $R[ 'coachId' ] ) . '&timezone=' . $R[ 'timezone' ] . '&duration=' . $R[ 'duration' ] . '&client=' . limpialo( $T[ 'nombre' ], 'min' );
		$bodyInvitarion =
		'<br>
		<p><b>Session Information</b></p>
		<p>' . $R[ 'title' ] . ' session with the coach ' . obtenCoachName( $R[ 'coachId' ] ) . ' the next ' . formatoFecha( $R[ 'fechas' ], 'EN' ) . ' at ' . $R[ 'tiempo' ] . ' (' . $R[ 'timezone' ] . ')</p>
		<p><a href="' . $_REQUEST[ 'baseUrl' ] . '/download-calendar' . $url . '" download>DOWNLOAD ICS CALENDAR</p>';
		echo $bodyInvitarion;
	} else {
		echo 'Llamada sin usuarios';
	}
	exit();
?>