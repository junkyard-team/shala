<?php
	class ZoomPHP {
		public $base = '';
		private $clientPublic = 'go3Ws0HIQa6AuY1P4wIZDg';
		private $clientSecret = '7ZybvDrXXQjpPpEqHj09cDKCLWuk8Kib';
		public function __construct( $baseUrl ) { $this->base = $baseUrl; }
		public function getToken( $code, $coachId ) {
			try {
				$request_url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' . $code . '&redirect_uri=' . $this->base . '/zoom-activate?coachId=' . $coachId;
				$basicAuth = base64_encode( $this->clientPublic . ':' . $this->clientSecret );
				$headers = array(
					'Authorization: Basic ' . $basicAuth,
					'Content-Type: application/x-www-form-urlencoded'
				);
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
				curl_setopt( $ch, CURLOPT_URL, $request_url );
				curl_setopt( $ch, CURLOPT_POST, 1 );
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				$response = curl_exec( $ch );
				$err = curl_error( $ch );
				curl_close( $ch );
				if( !$response ) {
					return array( 'status' => false, 'response' => $response );
				} else {
					$datos = json_decode( $response );
					if ( isset( $datos->error ) && $datos->error != '' ) {
						return array( 'status' => false, 'response' => $datos->error );
					} else {
						return array( 'status' => true, 'response' => $datos );
					}
				}
			} catch ( Exception $ex ) {
				return array( 'status' => false, 'response' => $ex );
			}
		}
		public function refreshToken( $token ) {
			try {
				$request_url = 'https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token=' . $token;
				$basicAuth = base64_encode( $this->clientPublic . ':' . $this->clientSecret );
				$headers = array(
					'Authorization: Basic ' . $basicAuth,
					'Content-Type: application/x-www-form-urlencoded'
				);
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
				curl_setopt( $ch, CURLOPT_URL, $request_url );
				curl_setopt( $ch, CURLOPT_POST, 1 );
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				$response = curl_exec( $ch );
				$err = curl_error( $ch );
				curl_close( $ch );
				if( $response ) {
					$datos = json_decode( $response );
					if ( !isset( $datos->error ) || $datos->error == '' ) {
						return $datos;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} catch ( Exception $ex ) {
				return false;
			}
		}
		public function getUserData( $token ) {
			try {
				$request_url = 'https://api.zoom.us/v2/users/me';
				$headers = array(
	            	'Authorization: Bearer ' . $token,
	            	'content-type: application/json'
	            );
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
				curl_setopt( $ch, CURLOPT_URL, $request_url );
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				$response = curl_exec( $ch );
				$err = curl_error( $ch );
				curl_close( $ch );
				if ( $response ) {
					$datos = json_decode( $response );
					if ( isset( $datos->email ) && $datos->email != '' ) {
						return array( 'status' => true, 'response' => $datos );
					} else {
						return array( 'status' => false, 'response' => $datos );
					}
				} else {
					return array( 'status' => false, 'response' => $response );
				}
			} catch ( Exception $ex ) {
				return array( 'status' => false, 'response' => $ex );
			}
		}
		public function createMeeting( $data, $token ) {
			$post_time = $data[ 'start_date' ];
			$start_time = gmdate( "Y-m-d\TH:i:s", strtotime( $post_time ) );
            $createAMeetingArray = array();
            if ( !empty( $data[ 'alternative_host_ids' ] ) ) {
                if ( count( $data[ 'alternative_host_ids' ] ) > 1 ) {
                    $alternative_host_ids = implode( ",", $data[ 'alternative_host_ids' ] );
                } else {
                    $alternative_host_ids = $data[ 'alternative_host_ids' ][0];
                }
            }
            $createAMeetingArray[ 'topic' ] = $data[ 'meetingTopic' ];
            $createAMeetingArray[ 'agenda' ] = ! empty( $data['agenda'] ) ? $data['agenda'] : "";
            $createAMeetingArray[ 'type' ] = ! empty( $data['type'] ) ? $data['type'] : 2; //Scheduled
            $createAMeetingArray[ 'start_time' ] = $start_time;
            $createAMeetingArray[ 'timezone' ] = $data['timezone'];
            $createAMeetingArray[ 'password' ] = ! empty( $data['password'] ) ? $data['password'] : "";
            $createAMeetingArray[ 'duration' ] = ! empty( $data['duration'] ) ? $data['duration'] : 60;
            $createAMeetingArray[ 'settings' ] = array(
                'join_before_host' => ! empty( $data['join_before_host'] ) ? true : false,
                'host_video' => ! empty( $data['option_host_video'] ) ? true : false,
                'participant_video' => ! empty( $data['option_participants_video'] ) ? true : false,
                'mute_upon_entry' => ! empty( $data['option_mute_participants'] ) ? true : false,
                'enforce_login' => ! empty( $data['option_enforce_login'] ) ? true : false,
                'auto_recording' => ! empty( $data['option_auto_recording'] ) ? $data['option_auto_recording'] : "none",
                'alternative_hosts' => isset( $alternative_host_ids ) ? $alternative_host_ids : ""
            );
            $request_url = 'https://api.zoom.us/v2/users/me/meetings';
            $headers = array(
            	'Authorization: Bearer ' . $token,
            	'content-type: application/json'
            );
            $postFields = json_encode( $createAMeetingArray );
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
			curl_setopt( $ch, CURLOPT_URL, $request_url );
			curl_setopt( $ch, CURLOPT_POST, 1);
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
			$response = curl_exec( $ch );
			$err = curl_error( $ch );
			curl_close( $ch );
			if( !$response ) {
				return $err;
			}
			return json_decode( $response );
		}
		public function deleteMeeting( $meetingId, $token ) {
            try {
				$request_url = 'https://api.zoom.us/v2/meetings/' . $meetingId;
				$headers = array(
	            	'Authorization: Bearer ' . $token,
	            	'content-type: application/json'
	            );
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
				curl_setopt( $ch, CURLOPT_URL, $request_url );
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				$response = curl_exec( $ch );
				$httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
				$err = curl_error( $ch );
				curl_close( $ch );
				if ( $response ) {
					if ( $httpcode == 204 ) {
						return array( 'status' => true, 'response' => $datos );
					} else {
						return array( 'status' => false, 'response' => $datos );
					}
				} else {
					return array( 'status' => false, 'response' => $response );
				}
			} catch ( Exception $ex ) {
				return array( 'status' => false, 'response' => $ex );
			}
		}
	}
?>