<?php
	include '../../panel/querys/conexion.php';
	include 'zoom-class.php';
	$http = ( isset( $_SERVER[ 'HTTPS' ] ) ) ? 'https' : 'http';
	$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
	$zoomClass = new ZoomPHP( $base );
	$zoomToken = $zoomClass->refreshToken( $_REQUEST[ 'token' ] );
	if ( !$zoomToken ) {
		$status = array( 'status' => 'Error' );
	} else {
		$con = new Conexion();
		$con->AbreConexion();
		$query = $con->Consulta( 'update coaches set zoomToken="' . $zoomToken->access_token . '", zoomRefresh="' . $zoomToken->refresh_token . '" where coachId=' . $_REQUEST[ 'coachId' ] );
		$status = array( 'status' => 'Success', 'token' => $zoomToken->access_token, 'refresh' => $zoomToken->refresh_token );
	}
	echo json_encode( $status );
	exit();
?>