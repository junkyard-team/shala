<?php
	session_start();
    include '../panel/querys/conexion.php';
    include 'querys/functions.php';
    $con = new Conexion();
    $con->AbreConexion();
    $dominios = array( '.shala.junkyard.mx', '.shala.local', '.shala.us', '.live', '.localshala_system.', '.consciousmission.com' );
	$http = 'http';
	if ( isset( $_SERVER[ 'HTTPS' ] ) ) {
		$http = 'https';
	}
	$url = $http . '://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ];
	$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
    $baseAdmin = $base . '/admin/';
    $clientId = obtenClienteId( $_SERVER[ 'HTTP_HOST' ], $dominios, $con );
    $clientPlatformId = obtenPlatformId( $_SERVER[ 'HTTP_HOST' ], $dominios, $con );
    $sideBarType = 2; // 1 = Extendido | 2 = Minificado
    $classBody = ( $sideBarType == 2 ) ? 'sb-l-o' : 'sb-l-m';
    if ( $clientId == 0 ) {
        $T[ 'logotipo' ] = null;
        header( 'Location: /' );
    } else {
        $query = $con->Consulta( 'select * from clients where clientId=' . $clientId );
        $T = $con->Resultados( $query );
    }
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Panel de Administración</title>
    <base href="<?php echo $baseAdmin; ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="icon" href="../panel/img/favicon.ico" type="image/x-icon">
    <style>
        :root {
            --primario: #49bb04;
            --secundario: #3ea003;
        }
        .miLogo { width: 150px !important; height: auto !important; }
    </style>
    <script>var clientId = <?php echo $clientId; ?>; var baseURL = '<?php echo $base; ?>';</script>
    <link rel="stylesheet" type="text/css" href="../panel/assets/admin-tools/admin-forms/css/admin-forms.css?v=5">