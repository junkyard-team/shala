			<aside id="sidebar_left" class="nano nano-light affix">
				<div class="navbar-branding"><a href="principal" class="navbar-brand"><img src="../images/clients/<?php echo $T[ 'logotipo' ]; ?>" class="miLogo"></a><span id="toggle_sidemenu_l" class="ad ad-lines"></span></div>
				<div class="sidebar-left-content nano-content">
					<header class="sidebar-header">
						<div class="sidebar-widget author-widget">
							<div class="media">
								<div class="media-body">
									<div class="media-links"></div>
									<div class="media-author" id="usuarion"></div><a href="#" onclick="logout()">Logout</a>
								</div>
							</div>
						</div>
					</header>
					<ul class="nav sidebar-menu">
						<li class="sidebar-label pt30">Menu</li>
						<li id="manejoProductos"><a href="#" class="accordion-toggle"><span class="fa fa-globe"></span><span class="sidebar-title">Webpage Content</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="tabla/slidehome">Home Slider</a></li>
								<li><a href="tabla/bannerhome">Home Banners</a></li>
								<li><a href="tabla/bannersomos">About Content</a></li>
								<li><a href="tabla/posts">Blog Posts</a></li>
								<li><a href="tabla/gallery">Gallery</a></li>
							</ul>
						</li>
						<li id="manejoProductos"><a href="#" class="accordion-toggle"><span class="fa fa-building"></span><span class="sidebar-title">Platform Setup</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="individual/clients">Setup</a></li>
								<li><a href="individual/appearance">Custom Style</a></li>
								<li><a href="generals">General Contents</a></li>
								<li><a href="tabla/email">Email Content</a></li>
								<li><a href="tabla/questionnaire">Questionnaire Setup</a></li>
							</ul>
						</li>
						<li id="manejoServicios"><a href="#" class="accordion-toggle"><span class="fa fa-users"></span><span class="sidebar-title">Users</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="tabla/coaches">Guides</a></li>
								<!-- <li><a href="tabla/categories">Guide Categories</a></li> -->
								<!-- <li><a href="edita/calls">ZOOM Calls</a></li> -->
								<!-- <li><a href="tabla/levels">Contents Levels</a></li> -->
								<!-- <li><a href="tabla/series">Series</a></li> -->
								<li><a href="tabla/users">Members</a></li>
								<!-- <li><a href="tabla/answers">Students Answers</a></li> -->
							</ul>
						</li>
					</ul>
				</div>
			</aside>