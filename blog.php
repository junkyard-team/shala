<?php include 'cabecera.php'; ?>
<div class="pageHead">
	<h1><?php echo $blogText; ?></h1>
</div>
<?php
	$queryCat = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' or clientId=' . $clientPlatformId . ' order by nombre asc' );
	while( $C = $con->Resultados( $queryCat ) ) {
		$query = $con->Consulta( 'select * from posts where categoryId=' . $C[ 'categoryId' ] . ' and ( clientId=' . $clientId . ' or clientId=' . $clientPlatformId . ' ) order by fecha desc' );
		if ( $con->Cuantos( $query ) > 0 ) {
			echo '<div class="seccion"><h3 class="titulo">' . $C[ 'nombre' ] . '</h3><div class="swiper-container"><div class="swiper-wrapper">';
			while( $R = $con->Resultados( $query ) ) {
				$contentImg = ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) ? '<div class="playVideo blogPost"><img src="thumb?src=images/posts/' . $R[ 'image' ] . '&size=802x452" class="fullImg"></div>' : '<img src="thumb?src=images/posts/' . $R[ 'image' ] . '&size=802x452" class="fullImg">';
				echo
				'<div class="swiper-slide" postId="' . $R[ 'postId' ] . '" video="' . $R[ 'video' ] . '" onclick="manda( \'post/' . limpialo( $R[ 'title' ], 'min' ) . '\' )" style="cursor: pointer;">
					' . $contentImg . '
					<h4>' . $R[ 'title' ] . '</h4>
				</div>';
			}
			echo '</div><div class="swiper-button-prev botonesSwipe"><i class="fas fa-chevron-left"></i></div><div class="swiper-button-next botonesSwipe"><i class="fas fa-chevron-right"></i></div></div></div>';
		}
	}
?>
<?php include 'sessionlist.php'; ?>
<?php include 'pie.php'; ?>