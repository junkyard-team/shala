<?php
	session_start();
	include 'panel/querys/conexion.php';
	include 'admin/querys/functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$dominios = array( '.shala.junkyard.mx', '.shala.local', '.shala.us', '.live' );
	$http = ( isset( $_SERVER[ 'HTTPS' ] ) ) ? 'https' : 'http';
	$url = $http . '://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ];
	$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ];
	$nombreArchivo = basename( $_SERVER[ 'PHP_SELF' ] );
	$clientId = obtenClienteId( $_SERVER[ 'HTTP_HOST' ], $dominios, $con );
	$clientPlatformId = obtenPlatformId( $_SERVER[ 'HTTP_HOST' ], $dominios, $con );
	$query = $con->Consulta( 'select * from shala' );
	$SH = $con->Resultados( $query );
	$query = $con->Consulta( 'select c.memberQuestionnaire, c.accessPassword, p.nombre as plataforma, if(c.logotipo is not null,c.logotipo,p.logotipo) as logoFinal, if(c.logotipo is not null,"clients","platforms") as folderLogo, if(c.icono is not null,c.icono,p.icono) as iconoFinal, if(c.icono is not null,"clients","platforms") as folderIcono, if(c.fontFile is not null,"clients","platforms") as folderFont, if(c.fontFile is not null,c.fontFile,p.fontFile) as fontFileFinal, if(c.fontFileHead is not null,"clients","platforms") as folderFontHead, if(c.fontFileHead is not null,c.fontFileHead,p.fontFileHead) as fontFileHeadFinal from clients c inner join platforms p on(c.platformId=p.platformId) where c.clientId=' . $clientId );
	$T = $con->Resultados( $query );
	$folderLogo = 'clients';
	$tipoCliente = 1;
	$tipoAcceso = ( isMobile() ) ? 1 : 2;
	if ( is_null( $T ) || !isset( $T ) ) {
		$folderLogo = 'platforms';
		$tipoCliente = 2;
		$plataforma = explode( '.', $_SERVER[ 'HTTP_HOST' ] )[0];
		$query = $con->Consulta( 'select "S" as memberQuestionnaire, null as accessPassword, nombre as plataforma, logotipo as logoFinal, "platforms" as folderLogo, "platforms" as folderIcono, icono as iconoFinal, fontFile as fontFileFinal, fontFileHead as fontFileHeadFinal, "platforms" as folderFont, "platforms" as folderFontHead from platforms where nombre="' . $plataforma . '"' );
		$T = $con->Resultados( $query );
	}
	include 'traducciones.php';
	$queryGal = $con->Consulta( 'select * from gallery where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
	$cuantosGal = $con->Cuantos( $query );
	$query = $con->Consulta( 'select * from bannersomos where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
	$cuantosSomos = $con->Cuantos( $query );
?>
<!DOCTYPE html>
<html lang="<?php echo $_SESSION[ 'lang' ]; ?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $T[ 'plataforma' ]; ?></title>
		<base href="<?php echo $base; ?>" />
		<meta name="viewport" content="width=device-width">
		<meta name="author" content="Junkyard">
		<link rel="shortcut icon" href="images/<?php echo $T[ 'folderIcono' ]; ?>/<?php echo $T[ 'iconoFinal' ]; ?>" />
		<link rel="canonical" href="<?php echo $url; ?>" />
		<link href="css/fonts/fontawesome/css/all.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
		<link href="css/jquery.bxslider.css" rel="stylesheet" />
		<style id="<?php echo $T[ 'fontFileHeadFinal' ]; ?>">
			<?php if ( !is_null( $T[ 'fontFileFinal' ] ) && $T[ 'fontFileFinal' ] != '' ): ?>
			@font-face{ font-family: 'PlatformWebFont'; src: url( images/<?php echo $T[ 'folderFont' ]; ?>/<?php echo $T[ 'fontFileFinal' ]; ?> ) }
			<?php endif; ?>
			<?php if ( !is_null( $T[ 'fontFileHeadFinal' ] ) && $T[ 'fontFileHeadFinal' ] != '' ): ?>
			@font-face{ font-family: 'PlatformWebFontHead'; src: url( images/<?php echo $T[ 'folderFontHead' ]; ?>/<?php echo $T[ 'fontFileHeadFinal' ]; ?> ) }
			<?php endif; ?>
		</style>
		<?php if ( is_null( $T[ 'fontFileFinal' ] ) || $T[ 'fontFileFinal' ] == '' ): ?>
		<link rel="stylesheet" href="css/fonts.css?v=4">
		<?php endif; ?>
		<?php if ( is_null( $T[ 'fontFileHeadFinal' ] ) || $T[ 'fontFileHeadFinal' ] == '' ): ?>
		<link rel="stylesheet" href="css/fontshead.css?v=3">
		<?php endif; ?>
		<link rel="stylesheet" href="css/style.css?v=<?php echo time(); ?>">
		<?php if ( !is_null( $T[ 'fontFileFinal' ] ) && $T[ 'fontFileFinal' ] != '' ): ?>
		<style>.menu li a { font-size: 15px; }</style>
		<?php endif; ?>
		<style>
			.botonHead { background-color: <?php echo $botonBackgroundColor; ?> !important; color: <?php echo $botonTextColor; ?> !important; border-radius: <?php echo $botonRounded; ?>; font-size: <?php echo $buttonSize; ?> !important; }
			.botonHead:hover { color: <?php echo $botonBackgroundColor; ?> !important; background-color: <?php echo $botonTextColor; ?> !important; }
		</style>
		<style>
		<?php
			$query = $con->Consulta( 'select * from appearance where clientId=' . $clientId );
			while( $R = $con->Resultados( $query ) ) {
				if ( !is_null( $R[ 'h1' ] ) && $R[ 'h1' ] != '' ) { echo 'h1 { font-size: ' . $R[ 'h1' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h2' ] ) && $R[ 'h2' ] != '' ) { echo 'h2 { font-size: ' . $R[ 'h2' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h3' ] ) && $R[ 'h3' ] != '' ) { echo 'h3 { font-size: ' . $R[ 'h3' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h4' ] ) && $R[ 'h4' ] != '' ) { echo 'h4 { font-size: ' . $R[ 'h4' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h5' ] ) && $R[ 'h5' ] != '' ) { echo 'h5 { font-size: ' . $R[ 'h5' ] . ' !important; }'; }
				if ( !is_null( $R[ 'p' ] ) && $R[ 'p' ] != '' ) { echo 'p { font-size: ' . $R[ 'p' ] . ' !important; }'; }
				if ( !is_null( $R[ 'boton' ] ) && $R[ 'boton' ] != '' ) { echo '.boton, .coachLink { font-size: ' . $R[ 'boton' ] . ' !important; }'; }
				if ( !is_null( $R[ 'buttonRounded' ] ) && $R[ 'buttonRounded' ] != 'N' ) { echo '.boton { border-radius: 15px !important; }'; }
				if ( !is_null( $R[ 'buttonColor' ] ) && $R[ 'buttonColor' ] != '' ) { echo '.boton { background-color: ' . $R[ 'buttonColor' ] . ' !important; }'; }
				if ( !is_null( $R[ 'buttonColorText' ] ) && $R[ 'buttonColorText' ] != '' ) { echo '.boton { color: ' . $R[ 'buttonColorText' ] . ' !important; }'; }
				if ( !is_null( $R[ 'menuLink' ] ) && $R[ 'menuLink' ] != '' ) { echo '.menu li a { font-size: ' . $R[ 'menuLink' ] . ' !important; }'; }
				echo '@media screen and ( max-width: 780px ) {';
				if ( !is_null( $R[ 'h1Mobile' ] ) && $R[ 'h1Mobile' ] != '' ) { echo 'h1 { font-size: ' . $R[ 'h1Mobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h2Mobile' ] ) && $R[ 'h2Mobile' ] != '' ) { echo 'h2 { font-size: ' . $R[ 'h2Mobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h3Mobile' ] ) && $R[ 'h3Mobile' ] != '' ) { echo 'h3 { font-size: ' . $R[ 'h3Mobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h4Mobile' ] ) && $R[ 'h4Mobile' ] != '' ) { echo 'h4 { font-size: ' . $R[ 'h4Mobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'h5Mobile' ] ) && $R[ 'h5Mobile' ] != '' ) { echo 'h5 { font-size: ' . $R[ 'h5Mobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'pMobile' ] ) && $R[ 'pMobile' ] != '' ) { echo 'p { font-size: ' . $R[ 'pMobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'botonMobile' ] ) && $R[ 'botonMobile' ] != '' ) { echo '.boton, .coachLink { font-size: ' . $R[ 'botonMobile' ] . ' !important; }'; }
				if ( !is_null( $R[ 'menuLinkMobile' ] ) && $R[ 'menuLinkMobile' ] != '' ) { echo '.menu li a { font-size: ' . $R[ 'menuLinkMobile' ] . ' !important; }'; }
				echo '}';
			}
		?>
		</style>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		<script>
			var urlActual = "<?php echo $url; ?>";
			var archivoUsado = "<?php echo $nombreArchivo; ?>";
			var idiomaActual = '<?php echo $_SESSION[ 'lang' ]; ?>';
			var idiomaId = '<?php echo $_SESSION[ 'langId' ]; ?>';
			var clientId = <?php echo $clientId; ?>;
			var clientPlatformId = <?php echo $clientPlatformId; ?>;
			var vooKey = '<?php echo $SH[ 'spotlightrApiKey' ]; ?>';
			var tipoCliente = <?php echo $tipoCliente; ?>;
			var tipoAcceso = <?php echo $tipoAcceso; ?>;
			var cuestionarioActivo = '<?php echo $T[ 'memberQuestionnaire' ]; ?>';
			var securePass = <?php if ( !is_null( $T[ 'accessPassword' ] ) && trim( $T[ 'accessPassword' ] ) != '' ) { echo 'true'; } else { echo 'false'; } ?>;
		</script>
	</head>
	<body>
		<?php if ( !is_null( $T[ 'accessPassword' ] ) && trim( $T[ 'accessPassword' ] ) != '' ) : ?>
		<div class="loginSite">
			<div class="principal">
				<div class="fila">
					<div class="cincuenta cuentaCentrada" id="cuadroSesion">
						<form method="post" id="loginSite" class="formShow" enctype="multipart/form-data">
							<label for="accessPassword">Site Password</label>
							<input type="password" id="accessPassword" class="obligatorio" required>
							<br><br>
							<button type="submit" class="boton">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="capaNegra" onclick="cierraPop()"></div>
		<div class="cargando">
			<i class="fas fa-spinner fa-spin" style="color: #ccc;"></i><br>
			<h3><?php echo $procesando; ?></h3>
			<span id="porcentajeValor" style="display: none; width: 100%; height: 20px; text-align: center; color: #fff; padding: 10px; background-color: #666; border-radius: 3px;"></span>
			<a class="stopUpload"><i class="fas fa-times"></i>Cancel Upload</a>
		</div>
		<div class="popup video" id="videoPopup">
			<a class="cierra" onclick="popup( 'videoPopup', false )"><i class="fas fa-times"></i></a>
			<iframe src="" data-playerId="" allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" watch-type="" url-params="" frameborder="0" scrolling="no"></iframe>
		</div>
		<div class="popup llamadaPop" id="llamadaPopup">
			<a class="cierra back" onclick="popup( 'llamadaPopup', false )"><i class="fas fa-arrow-left"></i></a>
			<div class="fondo"></div>
			<div class="fila">
				<div class="detalleCall">
					<div class="infoCall"></div>
					<div class="imagenCall">
						<span></span>
						<img src="">
					</div>
				</div>
			</div>
		</div>
		<?php if ( $nombreArchivo != 'contra.php' && $nombreArchivo != 'confirma.php' ) : ?>
		<header>
			<a href="/"><img src="images/<?php echo $T[ 'folderLogo' ]; ?>/<?php echo $T[ 'logoFinal' ]; ?>" class="logoImg"></a>
			<a href="#" class="bMobile"><i class="fas fa-bars"></i></a>
			<div class="dMenu">
				<a href="#" class="cierraMobile"><i class="fas fa-times"></i></a>
				<ul class="menu" style="<?php echo getMenuClass( 'images/' . $T[ 'folderLogo' ] . '/' . $T[ 'logoFinal' ] ); ?>">
					<li><a href="/"><?php echo $homeText; ?><span></span></a></li>
					<?php if ( $cuantosSomos > 0 && checaPage( 2, $clientId, $con ) ) : ?>
					<li><a href="about"><?php echo $aboutText; ?><span></span></a></li>
					<?php endif; ?>
					<?php if ( checaPage( 1, $clientId, $con ) ): ?>
					<li><a href="explore"><?php echo $exploraText; ?><span></span></a></li>
					<?php endif; ?>
					<?php if ( $muestraCoach == 'S' && checaPage( 4, $clientId, $con ) ) : ?>
					<li><a href="<?php echo coachesLink( $clientId, $con ); ?>"><?php echo $instructores; ?><span></span></a></li>
					<?php endif; ?>
					<?php if ( checaPage( 3, $clientId, $con ) ): ?>
					<li><a href="sessions"><?php echo $liveMenuText; ?><span></span></a></li>
					<?php endif; ?>
					<?php if ( checaPage( 5, $clientId, $con ) && $cuantosGal > 0 ): ?>
					<li><a href="gallery">Gallery<span></span></a></li>
					<?php endif; ?>
					<?php if ( $tipoCliente == 1 ) : ?>
					<li id="iniciaLink"><a href="login">Log in<span></span></a></li>
					<li id="cuentaLink"><a href="sign-in"><?php echo $registroText; ?><span></span></a></li>
					<li id="cierraLink"><a onclick="logout()">Log Out<span></span></a></li>
					<?php endif; ?>
					<!-- <li>
						<a><?php echo $idiomaActual[ $_SESSION[ 'lang' ] ]; ?><span></span></a>
						<ul class="lenguaje">
							<li><a href="/?lang=EN">English</a></li>
							<li><div class="linea"></div></li>
							<li><a href="/?lang=FR">Français</a></li>
							<li><div class="linea"></div></li>
							<li><a href="/?lang=ES">Español</a></li>
						</ul>
					</li> -->
				</ul>
			</div>
		</header>
		<?php else : ?>
		<header>
		</header>
		<?php endif; ?>
		<main class="general" style="<?php echo getMainClass( 'images/' . $T[ 'folderLogo' ] . '/' . $T[ 'logoFinal' ] ); ?>">