<?php
	include 'cabecera.php';
	$R = obtenCircleData( $_REQUEST[ 'url' ], $con );
	$categoria = '';
	$queryCat = $con->Consulta( 'select c.nombre from circlecategories s inner join categories c on(s.categoryId=c.categoryId) where s.circleId=' . $R[ 'circleId' ] );
	while( $CAT = $con->Resultados( $queryCat ) ) { $categoria = '<h4>' .$CAT[ 'nombre' ] . '</h4>'; }
?>
<input type="hidden" id="circleIdActive" value="<?php echo $R[ 'circleId' ]; ?>">
<div class="seccion">
	<div class="principal">
		<div class="filaFlex">
			<div class="doble">
				<?php
					if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) {
						echo
						'<iframe allowtransparency="true" class="videoFrame" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' . $R[ 'video' ] . '?fallback=true" frameborder="0" scrolling="no"> </iframe>';
					} else {
						$imagenSerie = ( !is_null( $R[ 'photo' ] ) && $R[ 'photo' ] != '' ) ? 'images/series/' . $R[ 'photo' ] : 'images/video-general.jpg';
						echo '<img src="' . $imagenSerie . '" class="fullImg">';
					}
				?>
			</div>
			<div class="doble datosSerie">
				<h1><?php echo $R[ 'nombre' ]; ?></h1>
				<?php echo $categoria; ?>
				<div class="lineaGris"></div>
				<p><?php echo $R[ 'description' ]; ?></p>
				<br>
				<a class="tCenter circuloLink" onclick="requestCircle()" coachId="<?php echo $R[ 'coachId' ]; ?>" usuarios="<?php echo userCircles( $R[ 'circleId' ], $con ); ?>" coaches="<?php echo coachesCircles( $R[ 'circleId' ], $con ); ?>">Request to join</a>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="seccion margen sessionList padShort sinMargen">
	<div class="principal">
		<h2 class="serieTitulo">Guides</h2>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php getCoachCircle( $R[ 'circleId' ], $con ); ?>
			</div>
		</div>
	</div>
</div>
<div class="seccion margen sessionList padShort sinMargen">
	<div class="principal">
		<div id="sesionListAjax"></div>
	</div>
</div>
<div class="seccion">
	<div class="principal">
	<?php
		$query = $con->Consulta( 'select s.* from seriecircle sc inner join series s on(sc.serieId=s.serieId) where sc.circleId=' . $R[ 'circleId' ] . ' and clientId=' . $clientId . ' order by nombre asc' );
		if ( $con->Cuantos( $query ) > 0 ) {
			echo '<h2 class="serieTitulo">Series</h2><div class="fila">';
			while( $S = $con->Resultados( $query ) ) {
				$imagenSerie = ( !is_null( $S[ 'photo' ] ) && $S[ 'photo' ] != '' ) ? 'images/series/' . $S[ 'photo' ] : 'images/video-general.jpg';
				$intoVideo = ( !is_null( $S[ 'video' ] ) && $S[ 'video' ] != '' ) ? '<a onclick="muestraVideo( \'' . $S[ 'video' ] . '\' )" class="introLink"><i class="fas fa-play"></i> VIEW INTRO</a>' : '';
				$categoria = '';
				$queryCat = $con->Consulta( 'select c.nombre from seriescategories s inner join categories c on(s.categoryId=c.categoryId) where s.serieId=' . $S[ 'serieId' ] );
				while( $CAT = $con->Resultados( $queryCat ) ) { $categoria = '<span>' .$CAT[ 'nombre' ] . '</span>'; }
				$queryCat = $con->Consulta( 'select * from videos where serieId=' . $S[ 'serieId' ] );
				$cuantos = $con->Cuantos( $queryCat );
				$descriptionText = ( strlen( $S[ 'description' ] ) > 160 ) ? substr( $S[ 'description' ], 0, 160 ) . '...' : $S[ 'description' ];
				echo
				'<div class="cincuenta cPadBig categoriaHome videoCoach">
					' . $categoria . '
					<!-- <a href="#" onclick="accesoSerie( ' . $S[ 'serieId' ] . ', ' . $S[ 'coachId' ] . ', ' . $S[ 'accessId' ] . ', \'' . limpialo( $S[ 'nombre' ], 'min' ) . '\' )" class="noHref"> -->
					<a href="series/' . limpialo( $S[ 'nombre' ], 'min' ) . '">
						<img src="thumb?src=' . $imagenSerie . '&size=400x240" class="fullImg" style="border: 1px solid #000">
					</a>
					<!-- <div class="cuantos"><i class="fas fa-play"></i> ' . $cuantos . '</div> -->
					<div class="fila">
						<div class="sesenta"><h3 class="tituloSerie"><a href="#" onclick="accesoSerie( ' . $S[ 'serieId' ] . ', ' . $S[ 'coachId' ] . ', ' . $S[ 'accessId' ] . ', \'' . limpialo( $S[ 'nombre' ], 'min' ) . '\' )" class="noHref">' . $S[ 'nombre' ] . '</a></h3></div>
						<!--<div class="cuarenta tRight">' . $intoVideo . '</div> -->
						<div class="cuarenta tRight"><a class="introLink"><i class="fas fa-play"></i> ' . $cuantos . ' Videos</a></div>
					</div>
					<p>' . $descriptionText . '</p>
				</div>';
			}
			echo '</div>';
		}
	?>
	</div>
</div>
<?php include 'pie.php'; ?>