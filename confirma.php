<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1>Email Confirmed</h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<?php confirmacionMail( $_REQUEST[ 'clientId' ], $_REQUEST[ 'tabla' ], $_REQUEST[ 'token' ], $con ); ?>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>