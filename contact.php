<?php include 'cabecera.php'; ?>
<div class="pageHead">
	<h1>Contact Us</h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<h2 class="abreForm">Send us a message <i class="fas fa-envelope"></i></h2>
				<form method="post" id="contactForm">
					<label for="nombre">Name</label>
					<input type="text" id="nombre" class="obligatorio" required>
					<label for="telefono">Phone</label>
					<input type="text" id="telefono">
					<label for="email">Email</label>
					<input type="email" id="email" class="email obligatorio" required>
					<label for="mensaje">Message</label>
					<textarea id="mensaje" class="obligatorio" required></textarea>
					<br><br>
					<button type="submit" class="boton">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>