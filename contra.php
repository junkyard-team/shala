<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1>Password Recover</h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
			<?php
				$valido = false;
				$query = $con->Consulta( 'select * from passcambio where tipo=' . $_REQUEST[ 'tipo' ] . ' and id=' . $_REQUEST[ 'id' ] . ' and token="' . $_REQUEST[ 'token' ] . '"' );
				while( $R = $con->Resultados( $query ) ) {
					$valido = true;
				}
				if ( $valido ) {
			?>
				<form method="post" id="passForm" class="formShow" enctype="multipart/form-data">
					<label for="passUser">Enter your new password</label>
					<input type="password" id="passUser" campo="pass" class="obligatorio" required>
					<label for="passUser">Repeat your new password</label>
					<input type="password" id="passRepeat" campo="pass" class="obligatorio" required>
					<input type="hidden" id="tipo" value="<?php echo $_REQUEST[ 'tipo' ]; ?>">
					<input type="hidden" id="id" value="<?php echo $_REQUEST[ 'id' ]; ?>">
					<br><br>
					<button type="submit" class="boton">Update</button>
				</form>
			<?php
				} else {
					echo '<h2>This link has already been used</h2>';
				}
			?>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>