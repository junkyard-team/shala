function getCookie(e) {
    const t = `; ${document.cookie}`.split(`; ${e}=`);
    if (2 === t.length) return t.pop().split(";").shift()
}
window.isMac = window.isMac ? window.isMac : navigator.platform.match(/(Mac)/i), window.safariVersion = window.safariVersion ? window.safariVersion : navigator.userAgent.match(/Version\/[\d\.]+.*Safari/), window.isSafari = window.isSafari ? window.isSafari : !!safariVersion, window.iOS = window.iOS ? window.iOS : /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream, window.videoWrapperClass = window.videoWrapperClass ? window.videoWrapperClass : "video-player-container", window.iframeParentClass = window.iframeParentClass ? window.iframeParentClass : "spotlightr-id", window.callbackFunctions = window.callbackFunctions ? window.callbackFunctions : [], window.apiRoute = window.apiRoute ? window.apiRoute : "publish", window.viewIdMap = window.viewIdMap ? window.viewIdMap : {}, window.popupIds = window.popupIds ? window.popupIds : {
    popupIds: []
}, window.queryParams = window.location.search, window.mainParentWrapperClass = window.mainParentWrapperClass ? window.mainParentWrapperClass : "spotlightr-id", window.lastScrollValue = window.lastScrollValue ? window.lastScrollValue : 0, window.doNotScrollVideos = window.doNotScrollVideos ? window.doNotScrollVideos : [], window.settingsRatio = window.settingsRatio ? window.settingsRatio : {}, window.vooplayerScripts = window.vooplayerScripts ? window.vooplayerScripts : [], window.parsedIframeIndexes = window.parsedIframeIndexes ? window.parsedIframeIndexes : [], window.popupIframeMessageCallbacks = window.popupIframeMessageCallbacks ? window.popupIframeMessageCallbacks : [], window.channelIframeMessageCallbacks = window.channelIframeMessageCallbacks ? window.channelIframeMessageCallbacks : [], window.popupClickCallbacks = window.popupClickCallbacks ? window.popupClickCallbacks : [], window.channelClickCallbacks = window.channelClickCallbacks ? window.channelClickCallbacks : [], window.vooJsApiCallbacks = window.vooJsApiCallbacks ? window.vooJsApiCallbacks : {}, window.videoRatiosById = window.videoRatiosById ? window.videoRatiosById : {}, window.playerSettingsById = window.playerSettingsById ? window.playerSettingsById : {}, window.popupPlaceholderSizeById = window.popupPlaceholderSizeById ? window.popupPlaceholderSizeById : {}, window.channelPlaceholderSizeById = window.channelPlaceholderSizeById ? window.channelPlaceholderSizeById : {}, window.onScrollListeners = window.onScrollListeners ? window.onScrollListeners : {}, window.onWindowFocusListeners = window.onWindowFocusListeners ? window.onWindowFocusListeners : {}, window.smartScrollPausedVideo = window.smartScrollPausedVideo ? window.smartScrollPausedVideo : {}, window.smartFocusPausedVideo = window.smartFocusPausedVideo ? window.smartFocusPausedVideo : {}, window.vooplayerScripts.push(window.vooplayerScripts.length);
var contactCookie = getCookie("vooPlayerContact");

function httpGetAsync(e, t) {
    var a = new XMLHttpRequest;
    console.log('e',e)
    a.onreadystatechange = function() {
        4 === a.readyState && 200 === a.status && t && t(a.responseText)
    }, a.open("GET", e, !0), a.send(null)
}
window.vooAPI = function(e, t, a, o) {
    var i = e;
    if ("*" != e && e && (e % 1 != 0 ? e = atob(e) : i = btoa(e)), o) {
        var n = e + "-" + t;
        window.vooJsApiCallbacks[n] = o
    }
    setTimeout(function() {
        window.postMessageToIframe({
            videoId: i,
            api: !0,
            vidID: i,
            method: t,
            params: a
        })
    }, 500)
}, window.spotlightrAPI = window.vooAPI, window.handleVooIdParentResize = ((e, t) => {
    setInterval(function() {
        var a = e.parentNode;
        "floating-wrapper" === a.getAttribute("id") && (a = a.parentNode);
        var o = window.Util.getElementStyle(a),
            i = parseInt(a.getAttribute("last-width-value")),
            n = parseInt(a.getAttribute("last-height-value"));
        i === parseInt(o.width) && n === parseInt(o.height) || (a.setAttribute("last-width-value", parseInt(o.width)), a.setAttribute("last-height-value", parseInt(o.height)), window.setPlayerWrapperSize(e, t))
    }, 800)
}), window.handleDataPlayerIdAttribute = ((e, t) => {
    if (e) {
        if (!t) {
            let t = e.getAttribute("data-playerid");
            return t || (t = e.getAttribute("data-playerId")), t
        }
        return e.setAttribute("data-playerid", t), e.setAttribute("data-playerId", t), e
    }
}), window.handlePopupTypeAttribute = (e => {
    if (!e) return;
    return e.getAttribute("data-fancybox-type")
}), window.listenMessagesFromIframe = window.listenMessagesFromIframe ? window.listenMessagesFromIframe : function(e) {
    var t = window.addEventListener ? "addEventListener" : "attachEvent";
    (0, window[t])("attachEvent" == t ? "onmessage" : "message", function(t) {
        var a = window.handleDataPlayerIdAttribute(e);
        if (window.oneVideoPerTime && t.data && t.data.data && "oneVideoPerView" == t.data.data.name && t.data.data.videoId != a && window.postMessageToIframe({
                videoId: a
            }), t.data && t.data.data && "queryParams" == t.data.data.name && queryParams && window.postMessageToIframe({
                videoId: a,
                queryParams: queryParams
            }), t.data && t.data.data && "contactCookie" == t.data.data.name && contactCookie && window.postMessageToIframe({
                videoId: a,
                contactCookie: contactCookie
            }), ("vooPlayer-ready" === t.data || t.data && t.data.data && "vooPlayer-ready" === t.data.data) && (window.postMessageToIframe({
                videoId: a
            }), window.popupIframeMessageCallbacks[a] && setTimeout(function() {
                window.popupIframeMessageCallbacks[a](a)
            }, 600), window.channelIframeMessageCallbacks[a] && setTimeout(function() {
                window.channelIframeMessageCallbacks[a](a)
            }, 600), document.dispatchEvent(new CustomEvent("vooPlayerReady", {
                detail: {
                    video: a
                }
            }))), t.data && t.data.data && "handlePopupClick" === t.data.data.name && a === t.data.data.videoId && (window.videoRatiosById[a] = 1.7778, window.handlePopupClick(a, !0)), t.data && t.data.data && "handleChannelClick" === t.data.data.name && a === t.data.data.videoId && window.handleChannelClick(a, !0), t.data && t.data.data && "handleChannelVideoOpened" === t.data.data.name && a === t.data.data.videoId && window.togglePoupupCloserButton(document.querySelector("." + window.iframeParentClass), !0), t.data && t.data.data && "handleChannelVideoClosed" === t.data.data.name && a === t.data.data.videoId && window.togglePoupupCloserButton(document.querySelector("." + window.iframeParentClass)), t.data && t.data.data && t.data.data.name, t.data && t.data.data && "playerSettings" === t.data.data.name && a === t.data.data.videoId && (window.playerSettingsById[a] = t.data.data.playerSettings, window.handlePlayerSettingsUpdate(t.data.data.playerSettings, a)), t.data && t.data.data && t.data.data.api && t.data.data.callbackId && window.vooJsApiCallbacks && window.vooJsApiCallbacks[t.data.data.callbackId]) {
            var o = t.data.data.callbackId,
                i = t.data.data;
            o.indexOf("getDuration") >= 0 && i.returnValue && (i = i.returnValue), window.vooJsApiCallbacks[t.data.data.callbackId](i)
        }
        t.data && t.data.data && "fullscreen-toggle" === t.data.data.name && a === t.data.data.videoId && window.handlecustomFullscreen(a, t.data.data.isFullScreen), t.data && t.data.data && "playlistToggle" === t.data.data.name && (t.data.data.value ? e.parentNode.setAttribute("mobile-playlist-aspect", !0) : e.parentNode.setAttribute("mobile-playlist-aspect", !1), window.setPlayerWrapperSize(e.parentNode))
    }, !1)
}, window.postMessageToIframe = window.postMessageToIframe ? window.postMessageToIframe : function(e) {
    var t = [];
    for (var a in e) t[a] = e[a];
    var o = window.Util.DOM.$getElement("iframe");
    for (var i of o) {
        var n = window.handleDataPlayerIdAttribute(i);
        n && ("*" == e.videoId || n === e.videoId ? i.contentWindow.postMessage(t, "*") : (n % 1 != 0 && (n = atob(n)), "*" != e.videoId && n !== e.videoId || i.contentWindow.postMessage(t, "*")))
    }
}, window.handlePlayerSettingsUpdate = window.handlePlayerSettingsUpdate ? window.handlePlayerSettingsUpdate : function(e, t) {
    e && e.border && window.handleCustomBorder(e.border, t), e && e.SMARTScroll && (window.onScrollListeners[t + "-smart-scroll-listener"] = function() {
        window.smartScrollHandle(t)
    }), e && e.SMARTFocus && (window.onWindowFocusListeners[t + "-smart-focus-listener"] = function(e) {
        window.smartFocusHandle(t, e)
    })
}, window.smartScrollHandle = window.smartScrollHandle ? window.smartScrollHandle : function(e) {
    var t = window.Util.DOM.$getElement('*[data-playerid="' + e + '"]')[0];
    if (t = t || window.Util.DOM.$getElement('*[data-playerId="' + e + '"]')[0]) {
        var a = window.isOnScreen(t.parentNode, 100);
        a || window.smartScrollPausedVideo[e] || (window.smartScrollPausedVideo[e] = !0, console.log("send message to publish app to pause video"), window.postMessageToIframe({
            videoId: e,
            dataPlayerId: e,
            videoFocusChange: !0,
            isLost: !0
        })), a && window.smartScrollPausedVideo[e] && (window.smartScrollPausedVideo[e] = !1, console.log("send message to publish app to play video"), window.postMessageToIframe({
            videoId: e,
            dataPlayerId: e,
            videoFocusChange: !0,
            isLost: !1
        }))
    }
}, window.smartFocusHandle = window.smartFocusHandle ? window.smartFocusHandle : function(e, t) {
    t || window.smartFocusPausedVideo[e] || (window.smartFocusPausedVideo[e] = !0, console.log("send message to publish app to pause video"), window.postMessageToIframe({
        videoId: e,
        dataPlayerId: e,
        videoFocusChange: !0,
        isLost: !0
    })), t && window.smartFocusPausedVideo[e] && (window.smartFocusPausedVideo[e] = !1, console.log("send message to publish app to play video"), window.postMessageToIframe({
        videoId: e,
        dataPlayerId: e,
        videoFocusChange: !0,
        isLost: !1
    }))
}, window.handleCustomBorder = window.handleCustomBorder ? window.handleCustomBorder : function(e, t) {
    if (e) {
        var a = String(e.width) + e.units + " " + e.type + " " + e.color,
            o = window.Util.DOM.$getElement('*iframe[data-playerid="' + t + '"]')[0];
        (o = o || window.Util.DOM.$getElement('*iframe[data-playerId="' + t + '"]')[0]) && (o.style.border = a)
    }
}, window.handlecustomFullscreen = window.handlecustomFullscreen ? window.handlecustomFullscreen : function(e, t) {
    var a = Util.DOM.$getElement('*[data-playerid="' + e + '"]');
    if (!(a = a || Util.DOM.$getElement('*[data-playerId="' + e + '"]')) || !a[0]) return;
    a = a[0];
    let o = [];
    Util.DOM.$loopThroughParents(a, function(e) {
        e && o.push(e)
    }), o = o.reverse();
    for (var i = 0; i < o.length; i++) window.handleParentFullscreenBehavior(o[i], t);
    if (t) {
        if (!a.parentNode.getAttribute("default-background")) {
            var n = Util.getElementStyle(a.parentNode);
            a.parentNode.setAttribute("default-top", n.top), a.parentNode.setAttribute("default-left", n.left), a.parentNode.setAttribute("default-background", n.backgroundColor), a.parentNode.setAttribute("fullscreen-wrapper", "true")
        }
        a.parentNode.style.position = "fixed", a.parentNode.style.top = "0", a.parentNode.style.left = "0", a.parentNode.style.backgroundColor = "black"
    } else a.parentNode.style.top = a.getAttribute("default-top"), a.parentNode.style.left = a.getAttribute("default-left"), a.parentNode.style.backgroundColor = a.getAttribute("default-background"), window.handleWindowResize(a.parentNode);
    window.toggleScroll(t)
}, window.toggleScroll = window.toggleScroll ? window.toggleScroll : function(e) {
    var t = ["document", "window", "body"],
        a = document.querySelector("body");
    if (e) {
        var o = Util.getElementStyle(a);
        a.setAttribute("default-overflow", o.overflow), document.querySelector("body").style.overflow = "hidden"
    } else a.style.overflow = a.getAttribute("default-overflow");
    for (var i = 0; i < t.length; i++) {
        var n = t[i];
        e ? (Util.DOM.$on(n, "DOMMouseScroll", function(e) {
            e.preventDefault()
        }), Util.DOM.$on(n, "mousewheel", function(e) {
            e.preventDefault()
        }), Util.DOM.$on(n, "touchmove", function(e) {
            e.preventDefault()
        }), Util.DOM.$on(n, "scroll", function(e) {
            e.preventDefault()
        })) : (Util.DOM.$off(n, "DOMMouseScroll"), Util.DOM.$off(n, "mousewheel"), Util.DOM.$off(n, "touchmove"), Util.DOM.$off(n, "scroll"), Util.DOM.$off(n, "touchstart"))
    }
}, window.handleParentFullscreenBehavior = window.handleParentFullscreenBehavior ? window.handleParentFullscreenBehavior : function(e, t) {
    if (e)
        if (t) {
            var a = Util.getElementStyle(e);
            e.setAttribute("default-zIndex", a.zIndex), e.setAttribute("default-position", a.position), e.setAttribute("default-width", a.width), e.setAttribute("default-height", a.height), e.style.zIndex = "9999999", e.style.position = "relative", e.style.width = "100%", e.style.height = "100%"
        } else e.style.zIndex = e.getAttribute("default-zIndex"), e.style.position = e.getAttribute("default-position"), e.style.width = e.getAttribute("default-width"), e.style.height = e.getAttribute("default-height")
}, window.Util = {
    DOM: {
        $createNode: function(e) {
            let t = document.createElement("div");
            return t.innerHTML = e, t.children[0]
        },
        $appendChild: function(e, t) {
            e && e.appendChild(t)
        },
        $removeNode: function(e) {
            e && e.parentNode.removeChild(e)
        },
        $wrapElement: function(e, t) {
            e.parentNode.insertBefore(t, e), t.appendChild(e)
        },
        $insertBefore: function(e, t) {
            e && e.parentNode.insertBefore(t, e)
        },
        $insertAfter: function(e, t) {
            e && e.parentNode.insertBefore(t, e.nextSibling)
        },
        $checkExistingClass: function(e, t) {
            if (e) return e.classList ? e.classList.contains(t) : new RegExp("\\b" + t + "\\b").test(e.className)
        },
        $addHTMLClass: function(e, t) {
            if (!e) return;
            Util.DOM.$checkExistingClass(e, t) || (e.classList ? e.classList.add(t) : e.className += " " + t)
        },
        $deleteHTMLClass: function(e, t) {
            if (!e) return;
            Util.DOM.$checkExistingClass(e, t) && (e.classList ? e.classList.remove(t) : e.className = e.className.replace(new RegExp("\\b" + t + "\\b", "g"), ""))
        },
        $on: function(e, t, a) {
            if (e) return e.addEventListener ? e.addEventListener(t, a, !1) : e.attachEvent ? e.attachEvent("on" + t, a) : e["on" + t] = a
        },
        $off: function(e, t, a) {
            if (e) return e.addEventListener ? e.removeEventListener(t, a, !1) : void e.detachEvent("on" + t, a)
        },
        $onPressEnter: function(e, t) {},
        $getElement: function(e) {
            let t = !1;
            switch ("*" === e[0] ? "QUERY" : "#" === e[0] ? "ID" : "." === e[0] ? "CLASS" : "TAG") {
                case "ID":
                    e = e.replace("#", ""), t = document.getElementById(e);
                    break;
                case "CLASS":
                    e = e.replace(".", ""), t = document.getElementsByClassName(e);
                    break;
                case "TAG":
                    t = document.getElementsByTagName(e);
                    break;
                case "QUERY":
                    e = e.replace("*", ""), t = document.querySelectorAll(e)
            }
            return t
        },
        $checkIfVisible: function(e, t) {
            const a = (t = t || document).querySelector(e);
            return a.offsetWidth > 0 && a.offsetHeight > 0
        },
        $bindScript: function(e, t) {
            const a = document.createElement("script");
            a.type = "text/javascript", t && (a.onload = t), a.src = e, Util.DOM.$appendChild(Util.DOM.$getElement("head")[0], a)
        },
        $bindStyleToHead: e => {
            const t = document.head || document.getElementsByTagName("head")[0],
                a = document.createElement("style");
            t.appendChild(a), a.type = "text/css", a.styleSheet ? a.styleSheet.cssText = e : a.appendChild(document.createTextNode(e))
        },
        $loopThroughParents: (e, t, a) => {
            if (e) {
                var o = e.parentNode;
                a && a.push(o), t && t(o), "BODY" !== e.tagName && Util.DOM.$loopThroughParents(o, t, a)
            }
        }
    },
    loopThroughItems: function(e, t) {
        for (var a in e) t(e[a], a)
    },
    getElementStyle: function(e) {
        if (e) return window.getComputedStyle ? getComputedStyle(e, null) : e.currentStyle
    },
    getURLParameters: function(e, t) {
        if (e) {
            var a = e.getAttribute("src") || e.getAttribute("href"),
                o = a && !t ? a : "?" + e.getAttribute("url-params"),
                i = {},
                n = o.indexOf("?");
            if (n < 0) return i;
            for (var d = o.substring(n + 1).split("&"), r = 0; r < d.length; r++) {
                var l = d[r].split("=");
                l[0] && l[1] && (i[l[0]] = l[1])
            }
            return i
        }
    }
}, window.wrapIframe = window.wrapIframe ? window.wrapIframe : function(e) {
    var t = window.Util.DOM.$createNode('<div style="max-width: 100%; max-height: 100%;" class="' + window.iframeParentClass + '"></div>');
    e && t && window.Util.DOM.$wrapElement(e, t)
}, window.isOnScreen = window.isOnScreen ? window.isOnScreen : function(e, t) {
    var a = e.getBoundingClientRect(),
        o = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return t = t || 0, !(a.bottom < t || a.top - o >= t)
}, window.convertUrlObjectToAttribute = function(e) {
    var t = "";
    if (!e) return t;
    for (var a in e) t += a + "=" + e[a] + "&";
    return t
}, window.wrapChannel = window.wrapChannel ? window.wrapChannel : function(e) {
    var t = window.Util.getURLParameters(e),
        a = window.handleDataPlayerIdAttribute(e),
        o = document.createElement("div"),
        i = ".channel-container { width: 100%; height: 100%; overflow: hidden; border-radius: 4px; cursor: pointer; position: relative; transition: all 0.4s; } .channel-container.opened { width: 100%; height: 100% !important;max-width: 100% !important; max-height: 100% !important; min-height: 100% !important; top: 0;left: 0;overflow: auto; border-radius: 0px; z-index: 99999; position: fixed; background-color: #333333bb;display: flex; justify-content: center; align-items: center; padding: 50px; box-sizing: border-box; } .channel-container ." + window.iframeParentClass + " { transition: all 0.2s; } .channel-container.opened ." + window.iframeParentClass + ' { position: relative; display: flex; flex-direction: column; justify-content: center; align-items: center; width: calc(100% - 60px) !important;max-width: 60%;min-height: calc(100% - 10px);} /*.channel-container::after {position: absolute; top: 0%; left: 0%; width: 100%; height: 100%; content: "Play the video"; display: flex; align-items: center; justify-content: center; text-align: center; font-size: 16px; font-weight: bold; }*/ .channel-container.opened::after { display: none; } .close-floating-button i { line-height: 1; } .close-floating-button { top: 10px; right: 15px; padding-right: 0px; position:absolute; background-color: transparent !important;float:left; margin:5px 0 0 5px; user-select:none; outline:none; border:0; display:inline-block; -webkit-box-align:center; align-items:center; min-height:36px; white-space:nowrap; cursor:pointer; overflow:hidden; transition:box-shadow 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), background-color 0.4s cubic-bezier(0.25, 0.8, 0.25, 1)} @media only screen and (max-width: 620px) { .channel-container.opened { padding: 0;}}@media only screen and (max-width: 620px) { .channel-container.opened .' + window.iframeParentClass + " { max-width: 90%;width: 100% !important;max-width: 60%;min-height: calc(100% - 0px);}}";
    window.Util.DOM.$bindStyleToHead(i), o.className = "channel-container", o.id = "channel-" + a, window.handleDataPlayerIdAttribute(o, a);
    var n = window.convertUrlObjectToAttribute(t);
    return n = n.replace(/&amp;/g, "&"), o.innerHTML = '<iframe allow="autoplay" class="spotlightr video-player-container" data-playerId="' + a + '" data-playerid="' + a + '" allowtransparency="true" style="max-width:100%; "name="videoPlayerframe" url-params="' + n + '" allowfullscreen="true" src="" frameborder="0" scrolling="no"> </iframe>', Util.DOM.$insertAfter(e, o), Util.DOM.$removeNode(e), o.querySelector("iframe")
}, window.wrapPopup = window.wrapPopup ? window.wrapPopup : function(e) {
    var t = e.getAttribute("href") ? e.getAttribute("href") : "",
        a = window.Util.getURLParameters(e),
        o = window.handlePopupTypeAttribute(e),
        i = "";
    "image" != o && "text" != o || (i = "display: none; ");
    var n = window.handleDataPlayerIdAttribute(e),
        d = document.createElement("div"),
        r = ".popup-container { width: 100%; height: 100%; " + i + "overflow: hidden; border-radius: 4px; cursor: pointer; position: relative; transition: all 0.4s; } .popup-container.opened { width: 100%; height: 100%; max-width: 100% !important; max-height: 100% !important; top: 0;left: 0;overflow: hidden; border-radius: 0px; z-index: 99999; position: fixed; background-color: #333333bb;display: flex; justify-content: center; align-items: center; padding: 30px; } .popup-container ." + window.iframeParentClass + " { transition: all 0.2s; } .popup-container.opened ." + window.iframeParentClass + ' { position: relative; display: flex; flex-direction: column; justify-content: center; align-items: center; width: calc(100% - 60px) !important;max-width: 60%;} /*.popup-container::after {position: absolute; top: 0%; left: 0%; width: 100%; height: 100%; content: "Play the video"; display: flex; align-items: center; justify-content: center; text-align: center; font-size: 16px; font-weight: bold; }*/ .popup-container.opened::after { display: none; } .close-floating-button i { line-height: 1; } .close-floating-button { top: -65px; right: 0px; padding-right: 0px; position:absolute; background-color: transparent !important;float:left; margin:5px 0 0 5px; user-select:none; outline:none; border:0; display:inline-block; -webkit-box-align:center; align-items:center; min-height:36px; white-space:nowrap; cursor:pointer; overflow:hidden; transition:box-shadow 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), background-color 0.4s cubic-bezier(0.25, 0.8, 0.25, 1)} @media only screen and (max-width: 620px) { .popup-container.opened .' + window.iframeParentClass + " { max-width: 90%;}}";
    window.Util.DOM.$bindStyleToHead(r), d.className = "popup-container", d.id = "popup-" + n, window.handleDataPlayerIdAttribute(d, n);
    var l = window.convertUrlObjectToAttribute(a);
    return l = l.replace(/&amp;/g, "&"), d.innerHTML = '<iframe allow="autoplay" class="spotlightr video-player-container" data-playerId="' + n + '" data-playerid="' + n + '" allowtransparency="true" style="max-width:100%; "name="videoPlayerframe" url-params="' + l + '" allowfullscreen="true" src=' + t + ' frameborder="0" scrolling="no"> </iframe>', Util.DOM.$insertAfter(e, d), Util.DOM.$removeNode(e), d.querySelector("iframe")
}, window.openPopup = window.openPopup ? window.openPopup : function(e) {
    window.handlePopupClick(e, !0)
}, window.handlePopupClick = window.handlePopupClick ? window.handlePopupClick : function(e, t) {
    var a = {};
    a = t ? (a = document.querySelector('.popup-container[data-playerid="' + e + '"]')) || document.querySelector('.popup-container[data-playerId="' + e + '"]') : e.currentTarget;
    var o = window.handleDataPlayerIdAttribute(a),
        i = "." + window.iframeParentClass,
        n = a.querySelector(i),
        d = window.Util.DOM.$createNode(a.outerHTML);
    n.parentNode.style.opacity = 0, d.innerHTML = "", setTimeout(function() {
        window.Util.DOM.$deleteHTMLClass(d, "popup-container"), window.Util.DOM.$addHTMLClass(d, "popup-placeholder"), window.Util.DOM.$insertBefore(a, d), window.popupPlaceholderSizeById[o] = {
            width: n.offsetWidth,
            height: n.offsetHeight
        }, setTimeout(() => {
            Util.DOM.$addHTMLClass(a, "opened"), window.setPlayerWrapperSize(n), window.togglePoupupCloserButton(a.querySelector("." + window.iframeParentClass)), window.postMessageToIframe({
                videoId: window.handleDataPlayerIdAttribute(a),
                popupOpened: !0
            }), window.popupClickCallbacks[o] = window.Util.DOM.$on(a, "click", function(e) {
                window.closePopup(e.currentTarget)
            }), n.querySelector("iframe").style.width = "100%", setTimeout(function() {
                window.setPlayerWrapperSize(n), n.parentNode.style.opacity = 1
            }, 250)
        }, 250)
    }, 250)
}, window.handleChannelClick = window.handleChannelClick ? window.handleChannelClick : function(e, t) {
    var a = {};
    a = t ? (a = document.querySelector('.channel-container[data-playerid="' + e + '"]')) || document.querySelector('.channel-container[data-playerId="' + e + '"]') : e.currentTarget;
    var o = window.handleDataPlayerIdAttribute(a),
        i = "." + window.iframeParentClass,
        n = a.querySelector(i),
        d = window.Util.DOM.$createNode(a.outerHTML);
    n.parentNode.style.opacity = 0, d.innerHTML = "", setTimeout(function() {
        window.Util.DOM.$deleteHTMLClass(d, "channel-container"), window.Util.DOM.$addHTMLClass(d, "channel-placeholder"), window.Util.DOM.$insertBefore(a, d), window.channelPlaceholderSizeById[o] = {
            width: n.offsetWidth,
            height: n.offsetHeight
        }, setTimeout(() => {
            Util.DOM.$addHTMLClass(a, "opened"), window.togglePoupupCloserButton(a.querySelector("." + window.iframeParentClass)), window.postMessageToIframe({
                videoId: window.handleDataPlayerIdAttribute(a),
                channelOpened: !0
            }), window.channelClickCallbacks[o] = window.Util.DOM.$on(a, "click", function(e) {
                window.closeChannel(e.currentTarget)
            });
            var e = n.querySelector("iframe");
            e.style.width = "100%", e.setAttribute("scrolling", "yes"), setTimeout(function() {
                n.parentNode.style.opacity = 1
            }, 250)
        }, 250)
    }, 250)
}, window.togglePoupupCloserButton = window.togglePoupupCloserButton ? window.togglePoupupCloserButton : function(e, t) {
    var a = window.Util.DOM.$getElement(".close-floating-button")[0];
    if (!a && !t) {
        var o = document.createElement("button"),
            i = document.createElement("i");
        i.innerHTML = '<img src="https://app.spotlightr.com/assets/image/close-icon.png" />', e.style.position = "relative", window.Util.DOM.$addHTMLClass(o, "close-floating-button"), window.Util.DOM.$appendChild(o, i), window.Util.DOM.$appendChild(e, o)
    }
    a && t && window.Util.DOM.$removeNode(a)
}, window.closePopup = window.closePopup ? window.closePopup : function(e) {
    if (e && e.preventDefault) {
        e.preventDefault(), e.stopPropagation();
        var t = e.currentTarget
    }
    var a = t ? t.parentNode.parentNode : e,
        o = window.handleDataPlayerIdAttribute(a),
        i = a.querySelector("." + window.iframeParentClass),
        n = a.parentNode.querySelector(".popup-placeholder");
    if (n) {
        var d = {
            width: n.offsetWidth
        };
        window.togglePoupupCloserButton(a.querySelector("." + window.iframeParentClass), !0), Util.DOM.$removeNode(n), Util.DOM.$removeNode(t), Util.DOM.$deleteHTMLClass(a, "opened"), window.videoRatiosById[o] = 1.7778, window.setPlayerWrapperSize(i, d), window.postMessageToIframe({
            videoId: o,
            popupClosed: !0,
            popupOpened: !1
        }), window.Util.DOM.$off(a, "click", window.popupClickCallbacks[o])
    }
}, window.closeChannel = window.closeChannel ? window.closeChannel : function(e) {
    if (e && e.preventDefault) {
        e.preventDefault(), e.stopPropagation();
        var t = e.currentTarget
    }
    var a = t ? t.parentNode.parentNode : e,
        o = window.handleDataPlayerIdAttribute(a),
        i = a.querySelector("." + window.iframeParentClass),
        n = i.querySelector("iframe"),
        d = a.parentNode.querySelector(".channel-placeholder"),
        r = {
            width: d.offsetWidth
        };
    window.togglePoupupCloserButton(a.querySelector("." + window.iframeParentClass), !0), Util.DOM.$removeNode(d), Util.DOM.$removeNode(t), Util.DOM.$deleteHTMLClass(a, "opened"), n.setAttribute("scrolling", "no"), window.setPlayerWrapperSize(i, r), window.postMessageToIframe({
        videoId: o,
        channelClosed: !0,
        channelOpened: !1
    }), window.Util.DOM.$off(a, "click", window.channelClickCallbacks[o])
}, window.handleWindowResize = window.handleWindowResize ? window.handleWindowResize : function(e, t) {
    var a = e ? e.parentNode : null;
    a && window.Util.DOM.$checkExistingClass(a, "channel-container") && window.Util.DOM.$checkExistingClass(a, "opened") || window.setPlayerWrapperSize(e, t)
}, window.setPlayerWrapperSize = window.setPlayerWrapperSize ? window.setPlayerWrapperSize : function(e, t) {
    if (e && !e.getAttribute("fullscreen-wrapper")) {
        var a = e.querySelector("iframe"),
            o = window.Util.getElementStyle(e),
            i = parseInt(o.width),
            n = (parseInt(o.height), a.getAttribute("src")),
            d = window.handleDataPlayerIdAttribute(a),
            r = (a.getAttribute("watch-type"), window.Util.getURLParameters(a), {}),
            l = window.videoRatiosById[d] ? parseFloat(window.videoRatiosById[d]) : 1.7778,
            s = e,
            w = s.querySelector(".floating-wrapper");
        if (w && window.Util.DOM.$checkExistingClass(w, "active-floating")) {
            var p = window.Util.getElementStyle(w),
                c = parseInt(p.width) / 1.7778;
            w.style.height = String(c) + "px"
        }
        n = a.getAttribute("url-params") ? a.getAttribute("url-params").split("&") : [];
        for (var u = 0; u < n.length; u++) {
            var h = n[u].split("=");
            r[h[0]] = h[1]
        }
        window.Util.DOM.$checkExistingClass(e.parentNode, "popup-container") && window.Util.DOM.$checkExistingClass(e.parentNode, "opened") && (e.parentNode.style.height = "100%", e.style.maxWidth = "60%"), window.Util.DOM.$checkExistingClass(e.parentNode, "popup-container") && !window.Util.DOM.$checkExistingClass(e.parentNode, "opened") && (e.style.maxWidth = "100%"), s.style.setProperty("width", "100%"), o = window.Util.getElementStyle(s);
        var f = (i = t && t.width ? parseInt(t.width) : parseInt(o.width)) / l;
        r.aspect && (f = i / parseFloat(r.aspect).toFixed(2)), "true" == s.getAttribute("mobile-playlist-aspect") ? s.style.setProperty("height", String(parseInt(f) + 225) + "px", "important") : s.style.setProperty("height", f + "px", "important"), t && t.width && (i = t.width, s.style.setProperty("width", i + "px", "important"), t.maxWidth && s.style.setProperty("max-width", i + "px", "important")), t && t.height && (f = t.height, s.style.setProperty("height", f + "px", "important"), t.maxHeight && s.style.setProperty("max-height", f + "px", "important"))
    }
}, window.floatingVideoScrollHandle = window.floatingVideoScrollHandle ? window.floatingVideoScrollHandle : function(e, t) {
    parseInt(window.pageYOffset);
    var a = window.Util.DOM.$checkExistingClass(e.parentNode, window.iframeParentClass) ? e.parentNode : e.parentNode.parentNode,
        o = parseInt(a.offsetHeight),
        i = a.getBoundingClientRect(),
        n = o - -1 * parseInt(i.top) - 100 < 0,
        d = window.Util.getURLParameters(e, !0),
        r = d.float ? d.float.split("-") : "",
        l = r[0],
        s = r[1],
        w = window.Util.DOM.$checkExistingClass(e.parentNode, "floating-wrapper") ? e.parentNode : null,
        p = {};
    if (w) {
        if (n && !window.Util.DOM.$checkExistingClass(w, "active-floating")) {
            var c = w;
            switch (p = {
                    position: "fixed",
                    zIndex: "99999",
                    width: s,
                    minWidth: s
                }, l) {
                case "right":
                    p.bottom = "50px", p.right = "50px";
                    break;
                case "left":
                    p.bottom = "50px", p.left = "50px";
                    break;
                case "topRight":
                    p.top = "50px", p.right = "50px";
                    break;
                case "top":
                    p.top = "0px", p.left = "0px";
                    break;
                default:
                    p.top = "0px"
            }
            for (var u in p) c.style[u] = p[u];
            var h = window.Util.getElementStyle(c),
                f = parseInt(h.width) / 1.7778;
            c.style.height = String(f) + "px", window.Util.DOM.$addHTMLClass(w, "active-floating")
        }
        if (!n && window.Util.DOM.$checkExistingClass(w, "active-floating"))
            for (var u in window.Util.DOM.$deleteHTMLClass(w, "active-floating"), p = {
                    position: "static",
                    width: "100%",
                    minWidth: "100%",
                    height: "100%"
                }) w.style[u] = p[u]
    }
}, window.Util.DOM.$on(document, "DOMContentLoaded", function() {
    window.Util.loopThroughItems(window.vooplayerScripts, function(e) {
        if (!(window.parsedIframeIndexes.indexOf(e) >= 0)) {
            var t = "." + window.videoWrapperClass,
                a = "." + window.iframeParentClass,
                o = window.Util.DOM.$getElement(t)[e],
                i = window.Util.DOM.$getElement("*.fancyboxIframe.spotlightr")[e];
            i = i || window.Util.DOM.$getElement("*.fancyboxIframe.spotlightr")[0];
            var n = window.Util.DOM.$getElement("*.channelIframe.spotlightr")[e];
            n = n || window.Util.DOM.$getElement("*.channelIframe.spotlightr")[0];
            var d = window.Util.getURLParameters(o);
            if (i = (i = i || window.Util.DOM.$getElement("*.fancyboxIframe.videoPlayer")[e]) || window.Util.DOM.$getElement("*.fancyboxIframe.videoPlayer")[0], n = (n = n || window.Util.DOM.$getElement("*.channelIframe.videoPlayer")[e]) || window.Util.DOM.$getElement("*.channelIframe.videoPlayer")[0], !o && i) {
                var r = i.getAttribute("data-playerid"),
                    l = i.getAttribute("data-aspect");
                window.Util.DOM.$checkExistingClass(i, "spotlightr") || window.Util.DOM.$addHTMLClass(i, "spotlightr");
                var s = (o = window.wrapPopup(i)).parentNode.parentNode.querySelectorAll(".popup-container")[e];
                window.videoRatiosById[r] = l || 1.7778, window.setPlayerWrapperSize(s), window.handleVooIdParentResize(s), window.popupIframeMessageCallbacks[window.handleDataPlayerIdAttribute(o)] = function(e) {
                    window.postMessageToIframe({
                        videoId: e,
                        popup: !0
                    })
                }
            }
            if (queryParams && window.postMessageToIframe({
                    videoId: r,
                    queryParams: queryParams
                }), !o && n) {
                window.Util.DOM.$checkExistingClass(n, "spotlightr") || window.Util.DOM.$addHTMLClass(n, "spotlightr");
                var w = (o = window.wrapChannel(n)).parentNode.parentNode.querySelector(".channel-container");
                window.setPlayerWrapperSize(w), window.handleVooIdParentResize(w), window.channelIframeMessageCallbacks[window.handleDataPlayerIdAttribute(o)] = function(e) {
                    window.postMessageToIframe({
                        videoId: e,
                        channel: !0
                    })
                }
            }
            if (window.listenMessagesFromIframe(o), window.wrapIframe(o), o && !o.getAttribute("src")) {
                var p = n ? "watch/gallery/" : "watch/";
                o.setAttribute("src", "https://app.spotlightr.com/" + p + o.getAttribute("data-playerid"))
            }
            if (d && d.float) {
                var c = window.Util.DOM.$createNode('<div class="floating-wrapper" style="width: 100%; height: 100%;"></div>');
                window.Util.DOM.$wrapElement(o, c), window.onScrollListeners[window.handleDataPlayerIdAttribute(o) + "-float-listener"] = function(e) {
                    window.floatingVideoScrollHandle(o, e)
                }
            }
            var u = window.Util.DOM.$getElement(a),
                h = u[u.length - 1],
                f = h.querySelector("iframe"),
                g = {
                    width: !!f.width && f.width,
                    maxWidth: !!f.width && f.width,
                    height: !!f.height && f.height,
                    maxHeight: !!f.height && f.height
                };
            window.setPlayerWrapperSize(h, g), window.handleVooIdParentResize(h, g), o.style.width = g.width ? g.width : "100%", o.style.height = g.height ? g.height : "100%", window.onresize = function() {
                window.handleWindowResize(h, g)
            }, window.parsedIframeIndexes.push(e)
        }
    })
}), window.Util.DOM.$on(window, "scroll", function(e) {
    if (window.onScrollListeners)
        for (var t in window.onScrollListeners) window.onScrollListeners[t](e)
}), window.Util.DOM.$on(document, "visibilitychange", function(e) {
    if ("visible" === document.visibilityState) {
        if (window.onWindowFocusListeners)
            for (var t in window.onWindowFocusListeners) window.onWindowFocusListeners[t](!0)
    } else if (window.onWindowFocusListeners)
        for (var t in window.onWindowFocusListeners) window.onWindowFocusListeners[t](!1)
});