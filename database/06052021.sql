-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-05-2021 a las 03:42:17
-- Versión del servidor: 5.6.49-cll-lve
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shala_system`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access`
--

CREATE TABLE `access` (
  `accessId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `access`
--

INSERT INTO `access` (`accessId`, `name`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered users');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessadmin`
--

CREATE TABLE `accessadmin` (
  `accessId` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessadmin`
--

INSERT INTO `accessadmin` (`accessId`, `nombre`) VALUES
(7, 'generalSetup'),
(8, 'platformsManage'),
(9, 'webContent'),
(10, 'userManage'),
(11, 'platformUsers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessuseradmin`
--

CREATE TABLE `accessuseradmin` (
  `accessUserId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `accessId` int(11) NOT NULL COMMENT 'opciones=accessadmin'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessuseradmin`
--

INSERT INTO `accessuseradmin` (`accessUserId`, `tipoId`, `accessId`) VALUES
(11, 2, 7),
(12, 2, 8),
(13, 2, 9),
(14, 2, 11),
(15, 2, 10),
(16, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `usuario` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL COMMENT 'pass'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`adminId`, `tipoId`, `nombre`, `usuario`, `pass`) VALUES
(1, 2, 'Administrator', 'admin', 'MGRzOGtMVXcxRDJHR3piQ0NyMVdJVEtoWGE5RWVrL3p6blFMdEtyQTErST0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `answerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `questionId` int(11) NOT NULL COMMENT 'opciones=questionnaire',
  `answer` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`answerId`, `clientId`, `userId`, `questionId`, `answer`) VALUES
(36, 3, 3, 5, 'Three'),
(35, 3, 3, 4, 'Two'),
(34, 3, 3, 3, 'One'),
(33, 3, 3, 2, 'Two'),
(12, 3, 4, 1, 'One'),
(13, 3, 4, 2, 'Two'),
(14, 3, 4, 3, 'Three'),
(15, 3, 4, 4, 'Four'),
(16, 3, 4, 5, 'Five'),
(32, 3, 3, 1, 'Four'),
(22, 3, 5, 1, 'One'),
(23, 3, 5, 2, 'Two'),
(24, 3, 5, 3, 'Three'),
(25, 3, 5, 4, 'Four'),
(26, 3, 5, 5, 'Five'),
(27, 3, 6, 1, 'One'),
(28, 3, 6, 2, 'Two'),
(29, 3, 6, 3, 'Three'),
(30, 3, 6, 4, 'Four'),
(31, 3, 6, 5, 'Five'),
(37, 3, 7, 1, 'One'),
(38, 3, 7, 2, 'Two'),
(39, 3, 7, 3, 'Two'),
(40, 3, 7, 4, 'Four'),
(41, 3, 7, 5, 'Two'),
(42, 3, 8, 1, 'One'),
(43, 3, 8, 2, 'Two'),
(44, 3, 8, 3, 'Three'),
(45, 3, 8, 4, 'Four'),
(46, 3, 8, 5, 'Two'),
(47, 3, 9, 1, 'One'),
(48, 3, 9, 2, 'Three'),
(49, 3, 9, 3, 'Two'),
(50, 3, 9, 4, 'Two'),
(51, 3, 9, 5, 'One'),
(52, 3, 10, 1, 'One'),
(53, 3, 10, 2, 'Two'),
(54, 3, 10, 3, 'Three'),
(55, 3, 10, 4, 'Four'),
(56, 3, 10, 5, 'Five'),
(57, 3, 11, 1, 'Two'),
(58, 3, 11, 2, 'Four'),
(59, 3, 11, 3, 'Three'),
(60, 3, 11, 4, 'Two'),
(61, 3, 11, 5, 'Three'),
(62, 3, 12, 1, 'Four'),
(63, 3, 12, 2, 'Two'),
(64, 3, 12, 3, 'Three'),
(65, 3, 12, 4, 'Three'),
(66, 3, 12, 5, 'Three'),
(67, 3, 13, 1, 'One'),
(68, 3, 13, 2, 'Two'),
(69, 3, 13, 3, 'Three'),
(70, 3, 13, 4, 'Four'),
(71, 3, 13, 5, 'Five'),
(72, 3, 15, 1, 'Two'),
(73, 3, 15, 2, 'One'),
(74, 3, 15, 3, 'Three'),
(75, 3, 15, 4, 'Two'),
(76, 3, 15, 5, 'Four'),
(77, 3, 16, 1, 'Three'),
(78, 3, 16, 2, 'Two'),
(79, 3, 16, 3, 'Two'),
(80, 3, 16, 4, 'Two'),
(81, 3, 16, 5, 'Three'),
(82, 4, 17, 6, 'One'),
(83, 4, 18, 6, 'Two'),
(84, 3, 19, 1, 'Four'),
(85, 3, 19, 2, 'Four'),
(86, 3, 19, 3, 'Four'),
(87, 3, 19, 4, 'Five'),
(88, 3, 19, 5, 'One'),
(89, 7, 22, 7, 'Lodging'),
(90, 7, 22, 8, 'Referral'),
(91, 7, 23, 7, 'Retreat'),
(92, 7, 23, 8, 'Referral'),
(93, 8, 24, 9, 'Two'),
(94, 8, 24, 10, 'Four'),
(95, 8, 24, 11, 'One'),
(96, 8, 24, 12, 'Four'),
(97, 8, 24, 13, 'Five'),
(98, 3, 26, 1, 'Three'),
(99, 3, 26, 2, 'One'),
(100, 3, 26, 3, 'Two'),
(101, 3, 26, 4, 'Four'),
(102, 3, 26, 5, 'Five'),
(103, 8, 27, 9, 'Two'),
(104, 8, 27, 10, 'Three'),
(105, 8, 27, 11, 'Three'),
(106, 8, 27, 12, 'Four'),
(107, 8, 27, 13, 'Five'),
(108, 8, 28, 9, 'One'),
(109, 8, 28, 10, 'Three'),
(110, 8, 28, 11, 'Two'),
(111, 8, 28, 12, 'Four'),
(112, 8, 28, 13, 'Five'),
(116, 3, 30, 1, 'Two'),
(117, 3, 30, 2, 'One'),
(118, 3, 30, 3, 'Three'),
(119, 3, 30, 4, 'Five'),
(120, 3, 30, 5, 'Four'),
(121, 8, 31, 9, 'Two'),
(122, 8, 31, 10, 'Three'),
(123, 8, 31, 11, 'Three'),
(124, 8, 31, 12, 'Five'),
(125, 8, 31, 13, 'Two'),
(126, 8, 32, 9, 'Three'),
(127, 8, 32, 10, 'Five'),
(128, 8, 32, 11, 'One'),
(129, 8, 32, 12, 'Two'),
(130, 8, 32, 13, 'Three'),
(131, 8, 33, 9, 'Two'),
(132, 8, 33, 10, 'Two'),
(133, 8, 33, 11, 'Two'),
(134, 8, 33, 12, 'Three'),
(135, 8, 33, 13, 'Three'),
(136, 3, 34, 1, 'One'),
(137, 3, 34, 2, 'One'),
(138, 3, 34, 3, 'Two'),
(139, 3, 34, 4, 'Two'),
(140, 3, 34, 5, 'Two'),
(141, 3, 35, 1, 'Two'),
(142, 3, 35, 2, 'One'),
(143, 3, 35, 3, 'Two'),
(144, 3, 35, 4, 'Two'),
(145, 3, 36, 1, 'Online search'),
(146, 3, 37, 1, 'Invited by a Guide/Coach'),
(147, 3, 38, 1, 'Invited by a Guide/Coach'),
(148, 3, 39, 1, 'Invited by a Guide/Coach'),
(149, 3, 40, 1, 'Invited by a Guide/Coach'),
(150, 3, 41, 1, 'Invited by a Guide/Coach'),
(151, 3, 42, 1, 'Invited by a Guide/Coach'),
(152, 3, 43, 1, 'Invited by a Guide/Coach'),
(153, 3, 45, 1, 'Invited by a Guide/Coach'),
(154, 3, 46, 1, 'Invited by a Guide/Coach'),
(155, 3, 47, 1, 'Invited by a Guide/Coach'),
(156, 3, 48, 1, 'Invited by a Guide/Coach'),
(157, 3, 49, 1, 'Invited by a Guide/Coach'),
(158, 3, 50, 1, 'Invited by a Guide/Coach'),
(159, 3, 51, 1, 'Invited by a Guide/Coach'),
(160, 3, 52, 1, 'Invited by a Guide/Coach');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appearance`
--

CREATE TABLE `appearance` (
  `appearanceId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `h1` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h1Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `p` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `pMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `boton` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `botonMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `buttonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `buttonColor` varchar(10) DEFAULT NULL COMMENT 'color',
  `buttonColorText` varchar(10) DEFAULT NULL COMMENT 'color',
  `menuLink` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `menuLinkMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `appearance`
--

INSERT INTO `appearance` (`appearanceId`, `clientId`, `h1`, `h1Mobile`, `h2`, `h2Mobile`, `h3`, `h3Mobile`, `h4`, `h4Mobile`, `h5`, `h5Mobile`, `p`, `pMobile`, `boton`, `botonMobile`, `buttonRounded`, `buttonColor`, `buttonColorText`, `menuLink`, `menuLinkMobile`) VALUES
(3, 10, '28px', NULL, '24px', NULL, '22px', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24px', NULL, 'N', NULL, NULL, NULL, NULL),
(4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16px', NULL, 'S', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `articleId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`articleId`, `clientId`, `coachId`, `serieId`, `titulo`, `imagen`, `description`, `orden`) VALUES
(1, 3, 1, 1, 'Intentions and commitments - the keys for your path ahead', 'andrew-petrov-hopnkqoc0dg-unsplash.jpg', '<h1><strong>To be added</strong></h1>\n<p>bla vla vla</p>\n<p>sdsd</p>\n<p>sd</p>\n<p>sds</p>\n<p>ds</p>', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleserierequest`
--

CREATE TABLE `articleserierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseries`
--

CREATE TABLE `articleseries` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;',
  `clientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseries`
--

INSERT INTO `articleseries` (`serieId`, `clientId`, `coachId`, `accessId`, `nombre`, `description`, `photo`) VALUES
(1, 3, 1, 1, 'Design Your Future Now', 'A journey into consciously designing your future and manifesting it into your lived reality', 'aditya-saxena-_mixhvl_wza-unsplash-1.jpg'),
(2, 3, 9, 3, 'Sound Healing', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseriescategories`
--

CREATE TABLE `articleseriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseriescategories`
--

INSERT INTO `articleseriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(38, 1, 4),
(37, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannerhome`
--

CREATE TABLE `bannerhome` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `buttonText` varchar(200) NOT NULL,
  `buttonUrl` varchar(200) NOT NULL COMMENT 'liga'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannerhome`
--

INSERT INTO `bannerhome` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`, `buttonText`, `buttonUrl`) VALUES
(8, 7, 'RENT SPACE', 'Rent our facilities for retreats, workshops and special events. Nierika is situated in a peaceful and quiet river valley where the song of birds and the whisper of the wind inspire tranquility and introspection. Our ecological facilities and permaculture gardens help you feel immersed in nature and community.', 'pagoda-bed-room-pic.jpg', NULL, 'Learn more', '/about'),
(3, 3, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(4, 3, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore'),
(5, 4, 'FOR EVERY SPACE, AT ANY PACE\r\n', 'From the comfort of your living room to outside spaces, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you.\r\n', 'for-every-ephata.jpg', NULL, 'Explore classes', '/explore'),
(6, 4, 'HIGHLY ENGAGING CONTENT\r\n', 'We understand the needs of children and designed our programs accordingly in byte size videos, personalized live sessions, engaging group workshops and fun exercises.\r\n', 'highly-engaging-ephata.jpg', NULL, 'Explore classes', '/explore'),
(12, 7, 'Retreats', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'img_6822-will-need-cropping2.jpg', NULL, 'Learn more', '/explore'),
(13, 8, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(14, 8, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannersomos`
--

CREATE TABLE `bannersomos` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(10000) NOT NULL COMMENT 'html',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannersomos`
--

INSERT INTO `bannersomos` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`) VALUES
(7, 7, ' ', 'At Nierika, we provide a natural path to recovery and healing that helps you grow from your current challenge into the new phase in life.\n\nWe create an environment for deep self-exploration that allows new insights to emerge from within.\n\nOur client-centered approach has demonstrated effectiveness in conditions such as chronic depression, substance abuse, eating disorders, anxiety, loss and grief issues, PTSD, spiritual emergency and other mental, emotional and behavioral challenges.', 'pond.jpg', NULL),
(8, 7, 'Our History', 'Over the last decade, Centro Nierika has hosted an impressive range of events.  We provide a safe, beautiful and ecological space for holistic retreats, workshops, therapies, ceremonies, family gatherings, staff retreats and other special events.\n\nIndigenous healers from North & South America, internationally recognized psychologists, educators and persons seeking alternative paths to healing find Nierika the perfect setting for deep and transformative work.', 'centre-int-view-through-mirror-1-copy.jpg', NULL),
(9, 7, 'The Region', 'Our sanctuary is nested in a lush subtropical valley, at about 5,577′ (1,700 m) elevation in the Sierra Mountains.  The climate is moderate with mild humidity, pleasantly warm day temperatures that cool down at night. Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally.', 'img_7398.jpg', NULL),
(10, 7, 'Getting here', 'Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally. You can also take Uber, Didi or local taxi from the airport or the city to arrive here. Fares range from 700 to1500 pesos for a one way trip depending on demand. We recommend avoiding night time travel due to poor visibility on the mountainous roads.', 'mappng.png', NULL),
(11, 7, 'The Facilities', 'The center provides  individual and/or shared comfortable accommodations, architectural spaces, hot solar showers, wireless internet, healthy and delicious meals prepared with organic produce, use of an art studio and yoga/meditation facilities as well as many acres of organic orchards and gardens. You will experience the serenity of the surrounding nature reserve setting, high standards of cleanliness, comfort and a unique style.', 'dinning-room-main-pic-6.jpg', NULL),
(12, 7, 'ACCOMMODATION', 'We can lodge up to 40 people in various types of spaces. Our dormitories are a thermal adobe building with 2 sections (Men & Women) with a capacity of 8 beds each. We also have individual or shared rooms (2 beds), either with personal or shared bathroom. Our suites are queen size, private bathroom, some with and extra individual bed. We also have two bedroom (one queen and two singles) apartment with bathroom, with small living room and kitchen.', 'pool-suite-pic-4-new.jpg', NULL),
(13, 7, 'Food and Water', 'We provide a mostly organic menu with lots of fresh farm-to-table ingredients. Our cooks are masterful in preparing family-style Mexican food, that is healthy, flavorful and vegetarian-friendly. Meals are included in the cost of accommodation.\n\nNierika’s permaculture gardens are thriving with avocado, citrus, and coffee orchards, fresh greens and other vegetables as well as medicinal herbs. All our water comes straight from Mountain springs and goes through a chemical-free filtration process.', 'alfresco-shot-1.jpg', NULL),
(14, 7, 'Workspaces', 'Center Space with terrace – An architecturally designed roofted terrace with sliding doors connecting to the altar and music room.\n\nThe Rooftop – Terrace with amazing valley view, suitable for group yoga and meditation.\n\nThe Cave of Visions – Inspire that a rock we could not move, we created a man-made cave with rock paintings that transport you to a mystical and magical space. It also has a fire place.', 'home_n_0007_zen_6897-edit-edit-scaled.jpg', NULL),
(15, 7, 'Teocali', 'A sacred geometry designed circular prayer & ceremonial room, with the possibility of having a fire inside, where countless ceremonies of different traditions have been held.', 'tucali-wide-shot.jpg', NULL),
(16, 7, 'Our Gardens', 'Immerse yourself in the beautiful natural landscapes filled with exotic flowers and ancient trees, suited for a process of profound inner growth and contemplation. Enjoy practicing organic gardening, seeding edible plants and farming in our lovely gardens and orchards, allowing life to grow in the natural path. Our gardens also provide a wonderful opportunity for giving back to Mother Earth by learning how to grow your own food while you help us harvest for our kitchen or sow new seeds.', 'gardens.jpg', NULL),
(17, 7, 'Temictla ', 'The Nierika project extends over 30 acres and is comprised by two sections divided by a river. Temictla, in nahuatl, means the place which is beyond dreams. It holds a reconstructed pyramid and an amphitheater field extends into the mountains where diverse public events have been held, such as Council of Visions, musical concerts and sundance, children camp, ideal for bigger groups and festivals. Temictla is excellent for morning walks up the mountain, and for outdoor group activities.', 'img_7398.jpg', NULL),
(18, 7, 'Nearby Nierika', 'Discover Historical Towns, Sacred Sites & Mythical Surroundings. Nestled in the Chichinautzin natural reserve 2 hours south of Mexico city, we are located in a lush subtropical watershed rich valley with springs and waterfalls. Ancient myths and archetypes live on in nearby historic world-heritage sites, temples and spiritual pilgrimage routes.', 'surroundings.jpg', NULL),
(20, 8, 'Section 1', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', '42-15216471.jpg', NULL),
(21, 8, 'Section 2', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', 'azulik_web_cover.jpg', 'MTEwNzI1NA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls`
--

CREATE TABLE `calls` (
  `callId` int(11) NOT NULL COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `image` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(2000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL COMMENT 'liga',
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipollamada',
  `fecha` datetime NOT NULL,
  `timezone` varchar(200) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '60'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls`
--

INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `image`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(2, 3, 1, 1, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/92589029676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXdGOUh2VXRRbFpkTklxNm9TZ0xYSzFOSkdkWWFNSWJVdXZoaElGRERiZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhYThpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE3OTMsImlhdCI6MTYwOTM0NDU5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DJJ4yaMyKdFUbHeKB-e-eENfqwIzbdGMNrnoYFBKqB4', 2, '2020-12-30 13:14:20', NULL, 60),
(4, 3, 1, 1, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/95415883734?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiU3R2U3FZRFRGN0h5a2w3YmJqU09RNFFsWDdLaXlnX0hhWmxCU05wMzVBQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhdXBSQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE4NzQsImlhdCI6MTYwOTM0NDY3NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cYvkrWMZfJ8CPJd5VbtBNsIESW2SDtpMC5k0kYDFXUg', 2, '2020-12-30 11:11:10', NULL, 60),
(5, 3, 1, 1, NULL, 'Call Test Set', NULL, 'https://zoom.us/s/97839866400?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiekZqeV85X0JoclpzWldDRmV0b0tJRTBpYjQwZV83akJFZ1htMV9QZE1PTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBiWXdpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTIwNDYsImlhdCI6MTYwOTM0NDg0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.568iWC_NSRtrEk35lAKeKCGKDq8k0cTDA1-QG-Q7gb4', 1, '2020-12-30 11:14:02', NULL, 60),
(6, 3, 1, 1, NULL, 'Yoga class', NULL, 'https://zoom.us/s/93979434891?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidmVzRmM0UlctVGxnN3d3WUJrMUluZU4tcmdaY3NHR2F2Q0Y3X3ZEVWNYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwRGZ2QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU2MjksImlhdCI6MTYwOTM0ODQyOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.kls1b_sCbUZYBv6KyLw7rBGo2rS_yEy2R4gMK0S-NSs', 2, '2020-12-30 12:14:00', NULL, 60),
(7, 3, 1, 1, NULL, 'Test Global', NULL, 'https://zoom.us/s/93734417213?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNU8zMTNuc0paV0J4OWdpNVlDNEQwNGpqLWtmU1Bub2prbWN4SEY3VnhnZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwWEZ3QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU3MTAsImlhdCI6MTYwOTM0ODUxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RftmAUITz5rnehnjCRPM-7Ytl4bBPd4tF4PD-zBuLVE', 1, '2020-12-31 12:14:54', NULL, 60),
(8, 3, 1, 1, NULL, 'Test Global test', NULL, 'https://zoom.us/s/95152785050?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNFpLZV83NzcyR3pmMlo4anc4MVMwSk1TbGFqc0pGaUJhZUl0S3R3SDlTay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBzN19YQUJKMUFBQUEiLCJleHAiOjE2MDkzNTY2NDcsImlhdCI6MTYwOTM0OTQ0NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.QtFFjV6iADF3HXvQa-gae3Tq0TjDGMKhjuydjwxnb0I', 1, '2020-12-31 00:00:00', NULL, 60),
(9, 3, 1, 1, NULL, 'Private Yoga Class', NULL, 'https://zoom.us/s/93360745123?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiY203RmVCQ3NOaUlsSkcyTDJJRnRsbWJCZHh0dWtiLWlSb2pyMm1fblZacy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA3MF85QUJKMUFBQUEiLCJleHAiOjE2MDkzNjA1NTEsImlhdCI6MTYwOTM1MzM1MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ufxODqV6_RdyIGJsY5jKA1ZnUpbyXqO8IxbXgjKNIY0', 2, '2020-12-30 13:35:00', NULL, 60),
(10, 3, 1, 1, NULL, 'Private Class for Tomorrow', NULL, 'https://zoom.us/s/96485903856?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibDBWWTFCZ0pRbDBSeHpfbF95X0JuZk1xVXFKVURmcFMwbzBXYUF3QWJKdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA4T3lBQUJKMUFBQUEiLCJleHAiOjE2MDkzNjA2NTYsImlhdCI6MTYwOTM1MzQ1NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.z4jNyIbpTLfzprVVKitK1hho8qDacy58IAW09luV5Is', 2, '2020-12-31 11:30:00', NULL, 60),
(12, 3, 1, 1, NULL, 'Yoga Beginners', NULL, 'https://zoom.us/s/93845368332?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVllGMGQ4eVFRcEgyLU93eEh2eDJDVTBwblhiZEw2Si0wbGV5cklBZDhWMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTAtX3FYQUJKMUFBQUEiLCJleHAiOjE2MDkzNjEzODEsImlhdCI6MTYwOTM1NDE4MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.os9YSdyOiUyXEoBJdvPc9AdwpaSVG0_gx9M7wKs0xKo', 1, '2020-12-30 13:55:00', NULL, 60),
(13, 3, 3, 1, NULL, 'Workshop', NULL, 'https://zoom.us/s/97563617492?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoia05rSlJ0Q0sxdi1aeDFfUVZESi1oeXRZZ1VNT3h1NVlmTGdZV0xVbDJUUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpWai1WQUJKMUFBQUEiLCJleHAiOjE2MTAxMzkwNDksImlhdCI6MTYxMDEzMTg0OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ532tN12H1MdALQ1uXbewIsy4j6brhBMqBPP-Umyi0', 1, '2021-01-08 14:00:00', NULL, 60),
(14, 3, 3, 1, NULL, 'Intro', NULL, 'https://zoom.us/s/93574307471?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoieDVVVDR4a2RuTG1IdXZCWkIwOFl4bXVXSjE4TkRoY0JNeU9kblQ4NE5KMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpYZkIzQUJKMUFBQUEiLCJleHAiOjE2MTAxMzk1NTMsImlhdCI6MTYxMDEzMjM1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.dHC_tOib4tV0EcxRlY66jcLQF_0WAn_y49MQZ1Petj0', 2, '2021-01-08 14:00:00', NULL, 60),
(15, 3, 1, 0, NULL, 'Testing call', NULL, 'https://zoom.us/s/92511603346?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibHdzZlAzMjh5NDBKemRITUUzaWFkWVN0WlJlYzhkUEE4MnctcHlRbVdSYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNRdGFMQUJKMUFBQUEiLCJleHAiOjE2MTA0NzMzMjEsImlhdCI6MTYxMDQ2NjEyMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JHA5cmEiCQF8nVFnk9f04nsUS5brj0u_mx4ZXcf-iKY', 2, '2021-01-15 11:00:00', NULL, 60),
(44, 3, 9, 4, NULL, 'Sound Healing ', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', 'https://zoom.us/s/99428845676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWHVRalVCLW9tMFBWdWtkRDAwMEdQdlVoVFh3UnJleU82T0JBcGVnZEVBby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZG45aTJwQUJKMUFBQUEiLCJleHAiOjE2MTIzNjQxMjIsImlhdCI6MTYxMjM1NjkyMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ9Tqr4sVWM1KG825lRAPnj8UONvXoA_VdJXeK2TVEQ', 1, '2021-02-04 08:00:00', NULL, 45),
(17, 3, 1, 1, NULL, 'Public Call For Now', 'Testing call', 'https://zoom.us/s/97018231393?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYUdEZDctcnljOE1uZGxPVThRT1NwbUJtMEFwelBTZU96cG1vaFdRX2xQUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNteHFqQUJKMUFBQUEiLCJleHAiOjE2MTA0NzkxMDUsImlhdCI6MTYxMDQ3MTkwNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.47s_8lJyH8LOPfJcnsw8ldKiadMXzRbSIxILNftX5kI', 1, '2021-01-12 12:15:00', NULL, 15),
(18, 3, 15, 1, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/91585109126?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0lMWVRQU2hMZlhlQ1Y4bnB0OVhjUG5rdFRDc2Vtb2pyd2JXbHVOSTBvNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY1ZfVnhoQUJKMUFBQUEiLCJleHAiOjE2MTA5ODg4NjEsImlhdCI6MTYxMDk4MTY2MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.eRfEN9p6VbtOaErGGtHzlCwNcV-nI0KJMdIvvT6R_jQ', 1, '2021-01-18 10:00:00', NULL, 30),
(19, 3, 3, 1, NULL, 'Intro class', 'Desc', 'https://zoom.us/s/99792834871?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicmxIM2lOYXdyeDU0SVRINkJTazhVdGFYYXVFRGlra002LVQ2Zk1aWGdFay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JHd3FJQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ2OTMsImlhdCI6MTYxMTA2NzQ5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.O5FoUKEjo9qXIoFIN8lRdqzz26HyZmQc8KmLoUnpOEc', 2, '2021-01-20 12:30:00', NULL, 30),
(20, 3, 3, 0, NULL, 'Consultation', NULL, 'https://zoom.us/s/96228600796?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZmcyd3lwZDFHZWdia2VuSWRJUU5mN1p5Q1ZVOFVRcVVCWHREeFB0RGVBUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JITjJEQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ4MTIsImlhdCI6MTYxMTA2NzYxMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ILj3fwYSfXzyLqXDtQzjYudTJCTiOsQ98IZE0ACVYfs', 2, '2021-01-20 18:00:00', NULL, 60),
(21, 3, 3, 1, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/93007611295?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXV2c2REMk5pS1Q3d1VtNlR5OWxwSllaMlFmaXFUbVphaFlMS1NHMk02OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JIa3RVQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ5MDYsImlhdCI6MTYxMTA2NzcwNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.LNGXXRB3OKMHPaK4I466gKp0uTzb4bE9x2uQkM3vHEY', 1, '2021-01-19 10:00:00', NULL, 30),
(23, 3, 3, 2, NULL, 'Public Session', 'Test', 'https://zoom.us/s/94332087831?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicUVCV09yNGRYcWNaQmdnOFF1ai12dUpRcHBsZzBNR19kNWRJcWtsTXgxYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY24wYmVoQUJKMUFBQUEiLCJleHAiOjE2MTEyODc5OTEsImlhdCI6MTYxMTI4MDc5MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DoeF-sRkk2ss-Kj4VVr-eYrmwIAv0z9tKOH0122ynNo', 1, '2021-01-21 19:00:00', NULL, 60),
(30, 3, 3, 1, 'roman-videopng.png', 'Breathwork for Innovation ', 'Test', 'https://zoom.us/s/99732096017?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWW9wRlBzYzNucnBLQlVUeU11V21IZzJiWEtidXFSWE81TFZIeVJyUmtYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJYZU8zQUJKMUFBQUEiLCJleHAiOjE2MTE1MzIwNTgsImlhdCI6MTYxMTUyNDg1OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PSGjJ5rhfBE-rHLjacizH7OuV3S_dk2f3t5LS9w8OzI', 1, '2021-01-24 17:00:00', NULL, 30),
(26, 3, 3, 1, 'roman-videopng.png', 'Breathwork for Memory and Focus', 'Join this 20 minute session to transcend your state of mind and achieve high levels of memory and focus', 'https://zoom.us/s/97025013577?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZVZuNVBMV0lkamJyakZIejFaSmhNQ2VTVmMyV1REd3hhN2ROa2hpb29OVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJFdHlBQUJKMUFBQUEiLCJleHAiOjE2MTE1MjcxNDEsImlhdCI6MTYxMTUxOTk0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gFeRyHl4YGbz6Hb1ozDJCnK5ek0Mhgeax_MEhKmcLeY', 1, '2021-01-24 15:30:00', NULL, 30),
(35, 3, 3, 4, 'iw_demopng.png', 'Innerworks Platform Demo', 'Exclusive presentation for the coach team to demonstrate features in the first version of the Innerworks platfrom.', 'https://zoom.us/s/93689176598?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoid1E2RzJUSVprZ1RHMDJ5S0U4RFREVDVHQ2RITmRUZmlJS0FsamtyaGpSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzV0bXNsQUJKMUFBQUEiLCJleHAiOjE2MTE1ODgxOTIsImlhdCI6MTYxMTU4MDk5MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ZDoClv4qr5KvX0BbZYE1FH1v28WiNy-NvcnUFhhqh3I', 1, '2021-01-30 06:30:00', NULL, 60),
(36, 3, 2, 0, NULL, 'Consultation', NULL, NULL, 2, '2021-01-26 13:30:00', NULL, 60),
(37, 3, 3, 1, NULL, 'User Onboarding', 'Onboarding session for coaches and students to join the Innerworks program', 'https://zoom.us/s/92731884092?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWmUyOEZnMDhMM1h3ME9UREl2NGJTS0dyd293UzdKX2ZkdzlMSHVKNlNEdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzU1OFhLQUJKMUFBQUEiLCJleHAiOjE2MTE1OTE0MjYsImlhdCI6MTYxMTU4NDIyNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.8VsbQ9B8pCwq-pspewAzemABM4PmqXyvpo_Gw7bAFlM', 1, '2021-01-31 18:00:00', NULL, 60),
(38, 4, 18, 5, 'highly_engaging_content.jpg', 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/97092458414?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSjAzdWJCS2xiTHJ4V0lIRWVVVmRvZnpUdGd0UW1rbGN5eTJRZ093aEhoZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtYX1h5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3MzgsImlhdCI6MTYxMTg2MDUzOCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JG3ngE2Klb6yIcbs8_IxTDyEyQOxMOQ-8HpiE0_zEKA', 1, '2021-01-28 12:00:00', NULL, 30),
(39, 4, 18, 5, 'highly_engaging_content.jpg', 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/96476527234?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiLV9EYXZNRlVLVHoxS01nZ1R4cnlRVG9Cc3JLZDV1S0RyQmMteXRNS0RaZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtZQUU5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3NDEsImlhdCI6MTYxMTg2MDU0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.94iOjlG0MAyYcWtwH08GC0YdOPepOswWGCCHosHe5ZU', 1, '2021-01-28 12:00:00', NULL, 30),
(40, 4, 19, 5, NULL, 'First Session', 'This is a test for sessions', 'https://zoom.us/s/94600546115?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiam5uWkMxaVFKY2pxdnVNeHF0NENKNjUyNmZkYk5XOVRxM3VXc2U1a2RIcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtqUEtMQUJKMUFBQUEiLCJleHAiOjE2MTE4NzA2ODcsImlhdCI6MTYxMTg2MzQ4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.I1rwWT1Zg9Qk0cGkhWNFPZGZ-zJ6bLdQCMTWlTInrAI', 1, '2021-01-29 02:00:00', NULL, 60),
(41, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/99729884957?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDVoZHFGSjFPNGdMMlFTVm93MERqdXVVbEp5MDA4Nm9YS210WTVnVEVFMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMEw3QUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Nvzqu35EA6WRjqnRIGFM4Ffl7jwr7kv0Tnj0hRj8_Nc', 1, '2021-02-16 02:00:00', NULL, 60),
(42, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/95030990013?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0FjRXFkc0hjR3NHWENnUmRnYnRfYUJtekIyOW5GS1Z6bExvUXpZckV4by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMFBWQUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.zWmvF41PErMh0aqgz9YFPyXdnziSVd_8xXrxpGNSRAw', 1, '2021-02-16 02:00:00', NULL, 60),
(43, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/96112508917?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXpSS0NMVTVad2t5RmRZdlZPRTJCS054d2x5YVFuOWNxSVJxY19XRlNsTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalNJUmtMQUJKMUFBQUEiLCJleHAiOjE2MTg0NDAyODcsImlhdCI6MTYxODQzMzA4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.24uc757ZB0XUsrlPTBjpeu96VJwaIOS97w0uTHTgHUc', 2, '2021-04-16 12:00:00', 'America/Adak', 60),
(45, 3, 3, 2, 'img_3153jpg.jpg', 'Demo Schedule', 'Bla bla bla', 'https://zoom.us/s/98430952016?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQlRGbTM5WmdoeTZVR2lkVFNfeXRIQXZXbkt5eDlpNV9aRGNaNUJFZklXby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4SmRrQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3NDMsImlhdCI6MTYxMjg5MDU0MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.4R08l4-g8dQCsjqsc4QDfoQi63iulelNcxiAWhbpa0U', 1, '2021-02-09 00:30:00', NULL, 30),
(46, 3, 3, 1, 'img_3154jpg.jpg', 'Demo test', 'bla bla', 'https://zoom.us/s/98236149012?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOHNIZFZXSkV4amFTRVpiTFZ5NWlOZ3AxVEZDajJ6SWY3czMtTFllcW5Ldy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4VzFoQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3OTgsImlhdCI6MTYxMjg5MDU5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.HAzgvFAeHKDcxmjJdpKHicWEqq3ZqtbXL0RYkl4O7sA', 1, '2021-02-09 12:30:00', NULL, 30),
(47, 3, 3, 0, NULL, 'Yoga Intro 1', NULL, NULL, 2, '2021-02-10 06:00:00', NULL, 60),
(48, 3, 3, 1, 'dy001484.jpg', 'Feb 16 Session', 'Testing session', 'https://zoom.us/s/93142513286?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT0ItQzc0cU14bDdsMkhmS0o2aGx5OWtKU0JNR3VlR0FCOE92LWFOZDhGay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZXIySmJOQUJKMUFBQUEiLCJleHAiOjE2MTM1MDMwMzQsImlhdCI6MTYxMzQ5NTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gLGFEYvoiOyKXeUYJCVVulGabFDFjkfwnrxPqxKv-ew', 1, '2021-02-16 12:30:00', NULL, 30),
(49, 3, 3, 2, 'img_3153jpg.jpg', 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96232917912?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVB4S0lUM3hrdWhvYThSVkZ5Z05oMklLclFOd3ZHaG9vakJwUHZoSnc3SS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDcFY2QUJKMUFBQUEiLCJleHAiOjE2MTM4NDE4NTUsImlhdCI6MTYxMzgzNDY1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.xQhfPLbBk6OGkmS1mYN8SmUXOVeJX8k4ceqOPXyIbTs', 2, '2021-02-24 02:00:00', NULL, 30),
(50, 3, 3, 2, 'img_3154jpg.jpg', 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96365592232?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNE9sMUZTWnBicWZBWWl4VDM1VUl1SkZ6S0p0VHptZVZZaEowWVdFLTU5NC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDLXZWQUJKMUFBQUEiLCJleHAiOjE2MTM4NDE5NDIsImlhdCI6MTYxMzgzNDc0MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cvc7_GEYd53DEePa7SvyqeBlaiw7aZNcfkK8JdKiiqU', 1, '2021-02-23 14:30:00', NULL, 30),
(51, 3, 3, 2, NULL, 'February 23', 'This is a description test', 'https://zoom.us/s/98979398236?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNTZlTURiRWlWVXV6ZlJCU2V4bFhNeWlwaGt2M0VtbGdDblRRUXI2bWVCSS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFVQmZIQUJKMUFBQUEiLCJleHAiOjE2MTQxMTQ4NDUsImlhdCI6MTYxNDEwNzY0NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gdUVNvT5aQwVH7isWmRK9dF5fYPcVPjvQhw1SYv9I1w', 1, '2021-02-23 13:30:00', NULL, 60),
(52, 3, 30, 3, 'img_0007jpg.jpg', 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(53, 3, 30, 3, 'img_0007jpg.jpg', 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(54, 3, 30, 3, '325692_3417.jpg', 'Feb 23 Live', 'Feb 23 Live', 'https://zoom.us/s/92701510519?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiX2Z3QUJIYTAxR2JfbG5DZnBSbTBwOXdmNzl5Q3V5ZVdkWWZsdm5DbmYyQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiaE56QUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4MTAsImlhdCI6MTYxNDEwOTYxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.m2DU2aY3PevRhw1Vy6zPfhvZL7lUEBL9qLBK-YnesnM', 1, '2021-02-24 14:00:00', NULL, 60),
(55, 3, 3, 4, 'cb067372.jpg', 'Feb 23 Session', 'Feb 23 Session', 'https://zoom.us/s/99410747909?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiN0hCM0stWWlZc1pqZXd5cWYwOGhQYkM5SGszS3Q1MTFJX3FhQ3hONjEwZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiMmhkQUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4OTgsImlhdCI6MTYxNDEwOTY5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Lp62CuODwpHnVwJjv22YsuVKjz3wXicgtkRnEzx3AwE', 1, '2021-02-24 17:00:00', NULL, 60),
(56, 3, 3, 4, 'cool20080526_001.jpg', 'Test Feb 26', 'Test', 'https://zoom.us/s/91456256719?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYkZXeHQyOWZzTDVJaUZMVHBETnVHTUNia09Idkx3Q1dxcGQtVkNuYUJTcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZmY3b09wQUJKMUFBQUEiLCJleHAiOjE2MTQzNzY4ODYsImlhdCI6MTYxNDM2OTY4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NDSuy6MNRxRWSpkPVjh1JMUaLWeqEggBNvK4tVKpAb0', 1, '2021-02-27 18:00:00', NULL, 60),
(57, 3, 3, 1, 'huge.jpg', 'Big Image', 'test', 'https://zoom.us/s/92286786524?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiV0F4R19YX0RPemQyVW5Sa3RwSnB4Nk9zTWlLVmk0RG5kdGdJSjFSZWJwdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnQzV0hCQUJKMUFBQUEiLCJleHAiOjE2MTQ2MTA2NDQsImlhdCI6MTYxNDYwMzQ0NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Ause1FBZXJe47EMpqIyEWOa7dYDH8_NhqFpafxWn19g', 1, '2021-03-03 01:00:00', NULL, 60),
(58, 3, 3, 1, 'tlm120720kg-138-small.jpg', 'Breath of Fire', 'To begin, take a comfortable seated position. \n\nClose your eyes, chin parallel to the floor, face and shoulders relaxed.  \n\nThis exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. \n\nAfter the sharp exhale, relax and allow the breath to enter all on its own. \n\nFocus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/95107350138?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQk1iaWcyYVZjUzZaWWJGZXhveV92R1RodEhWWFdTLUoweWZ0TTl4RTF0TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnp5ejhmQUJKMUFBQUEiLCJleHAiOjE2MTQ3MTAxMTksImlhdCI6MTYxNDcwMjkxOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.l8HImoZ9lOkcXySCtxwu4H5u-JJCy3sDlXUVgCP592g', 0, '2021-03-31 13:00:00', NULL, 30),
(59, 3, 3, 2, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome. ', 'https://zoom.us/s/94619340642?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDQySDE1VzcyRmYtQnVJNmdxQ1hMMzd0eGtOaE9MMDE2aE9GaThjY0pBZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjBFNTA5QUJKMUFBQUEiLCJleHAiOjE2MTQ3MTQ4NjIsImlhdCI6MTYxNDcwNzY2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.hcRxbXkGzi0LWM5QZKzzCzHFeTRXorXhkVE1FXeCkrQ', 0, '2021-03-29 12:00:00', NULL, 30),
(60, 3, 3, 1, 'tlm120720kg-138-small.jpg', 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome.', 'https://zoom.us/s/94210581725?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUVgwS1R3aGxVbHBqbnBObXdDdUU3RkFYTmZJczFIeGNrZzVYWGdZZkZvMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdDa05aQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzE2ODksImlhdCI6MTYxNDgyNDQ4OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.lXTQZERJ97gpIypW5dRYSKJQyc7HMU8dHIENt8W_lTc', 1, '2021-03-08 09:00:00', NULL, 30),
(61, 3, 3, 1, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', 'Breath of Fire', 'To begin, take a comfortable seated position. Close your eyes, chin parallel to the floor, face and shoulders relaxed. This exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. After the sharp exhale, relax and allow the breath to enter all on its own. Focus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/93208341462?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibWdHVVhZd1llbEZRaHBqa2ZxNl9FYW9TRWpNbzlkSFFUR0dTTWF3SlRWZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdFSXZhQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzIxMDEsImlhdCI6MTYxNDgyNDkwMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NpC8J3khl4pZrV-UIhZ5B4rmUA_N4uKOfRlWeTVRB94', 1, '2021-03-15 09:00:00', NULL, 30),
(62, 7, 32, 10, 'cueva-venados-1-small.jpg', 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99083961397?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOUdtUmNWY1lPbWRzZElvMDVwNzE4ZTVWdEROUVJkZVNEMUZrc1E2U0pXdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl83ZzhPQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM3MjcsImlhdCI6MTYxNDkwNjUyNywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OhcMwfpoWJKTAXwbEv0oj0MAtuyIID4-LZtEkcJ0AVo', 1, '2021-03-06 18:00:00', NULL, 30),
(63, 7, 32, 10, 'entrada-fmm-003_temazcal.jpg', 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99753362893?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidHpiNjZqYW9BcW40b2htck9KelB1ZmRtODhQckdEek9nSUlPaW1yNmdYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl84R1lvQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM4ODAsImlhdCI6MTYxNDkwNjY4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.tR2XCG06_woHLOsKAKz-aorU6eBSXTE2zWm9rKZpfH8', 1, '2021-03-09 18:00:00', NULL, 30),
(64, 7, 33, 9, 'img_6822-will-need-cropping.jpg', 'Sound Healing', 'Sound healing works on vibration. Everything is a vibration and you tune your body like you tune an instrument. Different instruments are set to certain frequencies. Sound healing allows your body to heal itself by slowing down your brain waves, which affect every cell in your body, shifting them from diseased to being in ease. It’s just aligning it with whatever you need.', 'https://zoom.us/s/95606205299?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNzNkSE15YWhIU3JMZnA2RWI5UDZKdjRneWIzbmNWQ19sZlJPTWQ5ZGM2RS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl85UTY1QUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQxODYsImlhdCI6MTYxNDkwNjk4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.-h0YU96soev1JT63E6u-dYExTbWbHRG_u5a0UHoG0fc', 1, '2021-03-08 19:00:00', NULL, 30),
(65, 7, 33, 10, 'gws-temazcalero-or-mexican-shaman.jpg', 'Temezcal / Sweatlodge', 'A temazcal is a traditional Mexican steam bath that is in many ways similar to the Native American sweat lodge. Besides promoting physical well-being and healing, the temazcal is also a ritual and spiritual practice in which traditional healing methods are used to encourage reflection and introspection. While the body rids itself of toxins through sweating, the spirit is renewed through ritual. The temazcal is thought to represent the womb and people coming out of the bath are, in a symbolic sense, re-born.', 'https://zoom.us/s/91081953418?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUUd6RVB2b0hTb0dCY09NVngxZ3hrbF9DalZNclBGNlNtVnNhaTlfMEl1by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl8tV1JzQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQ0NzAsImlhdCI6MTYxNDkwNzI3MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.d5RPnKjj3A8FyixJSHewQL2ymjqo2vLwIykH7AoiIU4', 1, '2021-04-13 16:44:06', NULL, 30),
(68, 7, 34, 9, 'arttherapy3png.png', 'Art Therapy', 'Art therapy is founded on the belief that self-expression through artistic creation has therapeutic value for those who are healing or seeking deeper understanding of themselves and their personalities. According to the American Art Therapy Association, art therapists are trained to understand the roles that color, texture, and various art media can play in the therapeutic process and how these tools can help reveal one’s thoughts, feelings, and psychological disposition. Art therapy integrates psychotherapy and some form of visual arts as a specific, stand-alone form of therapy, but it is also used in combination with other types of therapy.', 'https://zoom.us/s/94586952459?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTFp6anZWNGFPYk5FX2VBYjVxSzZoejVqNVhEanp1Q2JNcmNYZjJkcUZHZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZ0FEV2lfQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTU3ODIsImlhdCI6MTYxNDkwODU4MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.efQkT-LwkZVszGw0t9TDOQQkJJID7ecKM_JXXRvMu1E', 1, '2021-03-06 15:00:00', NULL, 30),
(72, 8, 29, 21, 'ih212972.jpg', 'Live session', 'bla blabla', 'https://zoom.us/s/93474495279?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSzJaMHJJaG9xdXY3cTJSV0Q1cnRpbkpEZEpRR091S0JBb2dEOUlyU1ZpZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaURfZzYzQUJKMUFBQUEiLCJleHAiOjE2MTcxMjkzNjgsImlhdCI6MTYxNzEyMjE2OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.5kp9Ns_qAPHp9ehqKFotD5YE8dfPsOdlZfPKhVKMAdg', 1, '2021-04-01 09:00:00', NULL, 30),
(73, 3, 44, 4, 'student-using.jpg', 'Onboarding of Guides', 'We will introduce the latest version of the digital platform to our guide community and outline what we are building ahead so you can get started with creating and sharing content.', 'https://zoom.us/s/91932063976?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYnFfM3NPYVV0cFdCNFZNNDNqZnRRWXo2MUY4LTVtWVVwZmhnUDBXM0YzVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajdraTJMQUJKMUFBQUEiLCJleHAiOjE2MTkxMzU1NjQsImlhdCI6MTYxOTEyODM2NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Vm-La3a8rnHO-tGPIDDH9q4M_rZwkHWcnjcZoGEuP5o', 1, '2021-04-23 14:30:00', 'Europe/Stockholm', 90),
(71, 8, 29, 21, 'video-final.png', 'Updated call', 'This is a call test after edited', 'https://zoom.us/s/92279692885?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXF0MnpYc0hUTTM0cUpBSjYyNVBPTUxMZjRmQ252MVMxUU5GbHZzLUpTMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaGY0XzJPQUJKMUFBQUEiLCJleHAiOjE2MTY1MjM2ODAsImlhdCI6MTYxNjUxNjQ4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.yCJ1TLQDrdZfVokvp5uSsxSpEoK-YeKfCGY1cKJ_GE0', 1, '2021-03-24 08:30:00', NULL, 90),
(74, 3, 6, 2, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx.jpg', 'Weekly Meditation', 'This is a weekly series for all levels to explore themselves and move into stillness for body and mind through different styles of meditation. Welcome!  ', 'https://zoom.us/s/99886617431?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSThuSlZNVWZEaUF3NndFWW1UanZFeEJHNzMyT1JwVnAzU2FraFkyUS1HOC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYa3QxYzB3QUJKMUFBQUEiLCJleHAiOjE2MTk5Nzg4NTcsImlhdCI6MTYxOTk3MTY1NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ymLCLSnUlibEOZty54APvAD29gEwX6ddmFX26wWoPkI', 1, '2021-05-05 11:30:00', 'Europe/Stockholm', 30),
(75, 3, 37, 4, 'vials-of-samples-with-positive-results-from-patien-r2rhlfz.jpg', 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/94486212662?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVlHNVRIUTh2NGNwZl95X1RWYmxLV3JYbnV2R2dkdmhXSFZkeXlpREFtby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1MdTE1QUJKMUFBQUEiLCJleHAiOjE2MTgzNDA1MzAsImlhdCI6MTYxODMzMzMzMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.WEHLdgTEx_LfOQ6PJYwVEWdee5nzr9JvyZVjR2I2oAY', 3, '2021-04-14 09:00:00', NULL, 30),
(76, 3, 37, 4, 'video-conference-dd9bdje.jpg', 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/98664949383?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUU5iR3R2T0hoNEU0eFVKU2g3VXc1Y3A4b0k0elFtQ05nck4zWnJEak5kYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1QX0c0QUJKMUFBQUEiLCJleHAiOjE2MTgzNDE2NDYsImlhdCI6MTYxODMzNDQ0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PivqL82KElgtp7yC51y5E_J-hPeEOnnfwbSNqzIpub4', 2, '2021-04-14 09:00:00', NULL, 30),
(77, 8, 29, 21, '42-16624306.jpg', 'Session test 1', 'Session test 1 Session test 1 Session test 1', 'https://zoom.us/s/91903233225?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSXRDQk5XT1hKbXBNeEdnd3BkOU9LYTVVTEF0NW1rbzN6VlYyOG84VEE2Zy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1YTWFHQUJKMUFBQUEiLCJleHAiOjE2MTgzNDM1MzUsImlhdCI6MTYxODMzNjMzNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.pAXJygXfjQ3G2jBoRpCZ1HFPlIctaaV7Go4jXnGkLts', 3, '2021-04-14 09:30:00', NULL, 30);
INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `image`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(78, 3, 37, 4, 'video-conference-dd9bdje.jpg', 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/99889707394?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT1cya1JmV25pUEpxX2ExT205d3VOZGpwUzR2Wno5SF9ZM0Z3eGZSdEVicy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalI1ck5TQUJKMUFBQUEiLCJleHAiOjE2MTg0MzY0NjAsImlhdCI6MTYxODQyOTI2MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OV8pih55NLxVea4sZ34sbeOc7d3OEi66eCTbaCw58j4', 3, '2021-04-16 08:00:00', 'America/Adak', 30),
(79, 3, 6, 0, NULL, 'Consultation', 'TBD', NULL, 2, '2021-04-16 07:00:00', 'America/Adak', 60),
(80, 3, 6, 1, NULL, 'Art Therapy', 'TBD', 'https://zoom.us/s/99288103152?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiM2t2a20tYU51eWxXWlJMSGtOTTV1M1l0aG5xLUhvUmg5cy12bFZqcUNyVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalRTNTVBQUJKMUFBQUEiLCJleHAiOjE2MTg0NTk4NTEsImlhdCI6MTYxODQ1MjY1MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9._Udb1sXLU5bk5d1CIxxkrPYY1fpie45_a32yhAssNcw', 2, '2021-04-15 07:30:00', 'America/Adak', 30),
(81, 3, 37, 1, NULL, 'Test', 'TBD', 'https://zoom.us/s/92351401707?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTVVXd0VHNDBMRzFNTGktZmRFeUtkUl9Hd2VETl93Zjh5NXFzODV1TWNHcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajNUWjJRQUJKMUFBQUEiLCJleHAiOjE2MTkwNjM5NjIsImlhdCI6MTYxOTA1Njc2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RLfL6gR4U6-Qx3AOH_yb4WY9ke4vrcfU-NUNdbzfmhk', 2, '2021-04-22 07:00:00', 'Europe/Stockholm', 30),
(82, 3, 44, 2, NULL, 'Art Therapy', 'tbd', 'https://zoom.us/s/96927082395?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNW9rdWlVem5wS1NDTDAzSDZnRV9XNVJ3R19WYnFUZFZmc29hcHktWkNBYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYai05cW5uQUJKMUFBQUEiLCJleHAiOjE2MTkxOTI0ODEsImlhdCI6MTYxOTE4NTI4MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.sqNJU3j58DZfQjZ5Oi2QeZvF_4tid_hgFomQALHtaas', 2, '2021-04-28 08:00:00', 'Europe/Stockholm', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfil`
--

CREATE TABLE `cambioperfil` (
  `cambioId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfilmiembro`
--

CREATE TABLE `cambioperfilmiembro` (
  `cambioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `descripcion` varchar(1000) NOT NULL,
  `imagen` varchar(200) NOT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`categoryId`, `clientId`, `nombre`, `descripcion`, `imagen`) VALUES
(1, 3, 'Breathwork', 'Learn breathwork exercises that calm your mind, balance your nervous system and move you into a state of creativity and productivity', 'category-breathwork.jpg'),
(2, 3, 'Meditation', 'Practice meditation exercises that cultivate a state of equanimity and openness, bringing you in alignment with others and closer to your purpose', 'category-meditation.jpg'),
(3, 3, 'Psychotherapy', 'Consult with us and connect to your true authentic self through personal healing, focused and results oriented psychotherapy. ', 'category-psychotherapy.jpg'),
(4, 3, 'Connection', 'Discover the tools that help you relate, empathize and build trust in relationships so that share and co-create boldly and freely', 'category-connection.jpg'),
(5, 4, 'Purpose Approach', 'Children discover their individuality and explore what they want for themselves and the world through this series of classes and workshops.', 'challenge-mission-and-achievement-a-little-girl-is-v9dvn29.jpg'),
(6, 4, 'Passion based learning', 'We work with children to identify their interests and curate personalized exercises that lead them to seek learning out of passion. ', 'reading-is-her-big-passion-smp242b.jpg'),
(7, 4, 'Self-Efficacy', 'Self-efficacy classes develop the self-management skills that enable children to become resourceful, persistent and confident.', 'father-with-small-children-working-outdoors-in-gar-kkd5msl.jpg'),
(8, 4, 'Academics', 'Our academics classes explores art and culture and connects them with science, language, math and social studies taught at school.', 'lovely-little-girl-is-dancing-in-headphones-tqsuaje.jpg'),
(9, 7, 'Integral therapy', 'Gestalt, Integral and Transpersonal Psychotherapy, Naturopathic medicine, Therapeutic Massages, Horticultural therapy, Drug Addiction Treatment & Harm Reduction', 'cueva-venados-1-small.jpg'),
(11, 7, 'Specialized Workshops', 'Yoga, Meditation, Breathwork, Dreamwork, Family Constellation, Permaculture & Farming, Team Building and Art Therapy', 'yoga-terrace-space-2-small.jpg'),
(10, 7, 'Indigenous medicine', 'Traditional medicinal plants, Medicinal Herbs, Temazcal / Sweatlodge, Healing ceremonies, Medicine Wheel, Diets and Detox', 'entrada-fmm-003_temazcal.jpg'),
(12, 7, 'Consultations', 'Individual psychotherapy sessions for depression, anxiety, addiction, eating disorders, PTSD, existential crisis, difficult grief issues and life transitions', 'nierika-int-_img_5866.jpg'),
(20, 9, 'Business Development', 'Develop a business plan, find the right talent, build a marketing funnel, scale users, monetize your service, reach beyond digital and realize your vision!', 'tasks-for-business-development-8v2bzghjpg.jpg'),
(19, 9, 'Brand and Design', 'Build brand strategy and design language, connect them with how you think, speak, act and appear, and merge online with offline experiences ', 'hand-holding-notebook-with-drew-brand-logo-creativ-p8htw8w.jpg'),
(18, 9, 'Content Creation', 'Write compelling content, shoot your first video, speak confidently in front of the camera, create explainer videos and manage your audience on Zoom!', 'talk-show-at-online-radio-station-9fmcxz4.jpg'),
(17, 9, 'Platform Setup', 'Set up your platform, customize your content, administer your users, start LIVE programming and publish your website with your own domain name!', 'software-engineers-working-on-project-and-programm-xzp2tp7.jpg'),
(21, 8, 'Test', 'This a category test', 'video-final.gif');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientpages`
--

CREATE TABLE `clientpages` (
  `clientPageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pageId` int(11) NOT NULL COMMENT 'opciones=pages'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientpages`
--

INSERT INTO `clientpages` (`clientPageId`, `clientId`, `pageId`) VALUES
(89, 3, 4),
(88, 3, 3),
(87, 3, 2),
(86, 3, 1),
(9, 1, 1),
(10, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 2, 1),
(14, 2, 2),
(15, 2, 3),
(16, 2, 4),
(17, 4, 1),
(18, 4, 2),
(19, 4, 3),
(20, 4, 4),
(21, 7, 1),
(22, 7, 2),
(23, 7, 3),
(24, 7, 4),
(62, 8, 5),
(61, 8, 4),
(60, 8, 3),
(59, 8, 2),
(29, 9, 1),
(30, 9, 2),
(31, 9, 3),
(32, 9, 4),
(40, 10, 4),
(39, 10, 3),
(38, 10, 2),
(37, 10, 1),
(58, 8, 1),
(63, 11, 1),
(64, 11, 2),
(65, 11, 3),
(66, 11, 4),
(67, 12, 2),
(68, 12, 3),
(69, 12, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `clientId` int(11) NOT NULL COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;',
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `accessPassword` varchar(200) DEFAULT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `memberQuestionnaire` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `zoomPublic` varchar(200) DEFAULT NULL,
  `zoomSecret` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`clientId`, `platformId`, `nombre`, `accessPassword`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `pass`, `memberQuestionnaire`, `zoomPublic`, `zoomSecret`) VALUES
(1, 1, 'Shopify', NULL, 'logo-inner.png', 'logo-inner.png', NULL, NULL, 'archive', 'client@test.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(2, 2, 'EphataClient', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'clients', 'client@ephata.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(3, 1, 'Innerworks', NULL, 'innerworks_logo_5-innerworks-university.png', 'innerworks_logo_black-innerworks-university.png', NULL, NULL, 'innerworks', 'roman@romanf.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(4, 2, 'Ephata University', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'ephata', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(7, 3, 'Nierika', NULL, 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', NULL, NULL, 'nierika', 'universidad@nierika.com', 'dmlHaXNmUTN6bVZreEpHMURQQkxCYnhRUEJNTjEyRG51Z3NybEtPaC81Yz0=', 'S', NULL, NULL),
(8, 4, 'Test University', 'testpassword', 'logo-azulik-black.png', 'zenrise_logo_03-test-university.png', NULL, NULL, 'test', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(9, 5, 'Shala', NULL, 'logo-shala.png', 'logo-shala.png', NULL, NULL, 'shala', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', NULL, NULL),
(10, 6, 'Eleutheria', NULL, 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', NULL, 'lexendmega-regular.ttf', 'eleutheria', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coacharticleserie`
--

CREATE TABLE `coacharticleserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachcall`
--

CREATE TABLE `coachcall` (
  `coachCallId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `callId` int(11) NOT NULL COMMENT 'opciones=calls'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachcall`
--

INSERT INTO `coachcall` (`coachCallId`, `coachId`, `callId`) VALUES
(4, 44, 81);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coaches`
--

CREATE TABLE `coaches` (
  `coachId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `phrase` varchar(1000) DEFAULT NULL,
  `biografia` varchar(1000) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `header` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `facebook` varchar(200) DEFAULT NULL COMMENT 'liga',
  `twitter` varchar(200) DEFAULT NULL COMMENT 'liga',
  `instagram` varchar(200) DEFAULT NULL COMMENT 'liga',
  `linkedin` varchar(200) DEFAULT NULL COMMENT 'liga',
  `web` varchar(200) DEFAULT NULL COMMENT 'liga',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `showHome` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coaches`
--

INSERT INTO `coaches` (`coachId`, `statusId`, `nombre`, `apellido`, `phrase`, `biografia`, `foto`, `header`, `video`, `facebook`, `twitter`, `instagram`, `linkedin`, `web`, `email`, `pass`, `showHome`, `emailConfirmed`, `fecha`) VALUES
(1, 2, 'Nils', 'von Heijne', 'Holistic coach', 'Nils is a holistic coach, advisor, shaman and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker, energy worker and sound healer.\n\nMore info at nilsvonheijne.com', 'dsc03589.jpeg', 'dsc03749.jpeg', 'MTA3MTY3NA==', 'https://www.facebook.com/nilsvonheijne', 'https://twitter.com/nilsvonheijne', 'https://www.instagram.com/nilsvonheijne/', 'https://www.linkedin.com/in/nilsvonheijne/', 'https://nilsvonheijne.com/', 'nils@innerworks.io', 'TnlwYUZVV01lNEUxTjRCNDNZT3dHQT09', 'S', 'S', '2021-04-29'),
(2, 1, 'Roshi', 'Derakshan', NULL, 'Licensed Psychotherapist and dedicated to helping people connect to their true authentic self through personal healing, focused and results oriented psychotherapy. Licenced Life coach and work as a ”hybrid” where you get the best of a therapist and coach to break free from what’s holding you back to push towards where you want to be.', 'roshi-2png.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rome7f-deactivated-2021-04-22@gmail.com', 'VjJxelJkZC9VSUIwK0kyTHo4QUZyUT09', 'N', 'S', '2021-03-23'),
(5, 2, 'Jessika', 'Klingspor', NULL, 'Jessika is Swedish entrepreneur and business owner living in Barcelona since 2001. She is currently collaborating with the Barcelona City Council building new alliances between The Nordic\nCountries and Barcelona within the sector of innovation. She is co-creator within the Innerworks Community with aim to bridge the gap between the corporate world and the spiritual world. Planning to launch Innerworks Barcelona and World of Wisdom Barcelona 2021.', 'jessklingspor1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jessika.klingspor@nordicsinbarcelona.com', 'WWY5STRKWUNnc0t1L1kvWjF0NG1QZz09', 'S', 'S', '2021-04-22'),
(38, 2, 'Roman', 'Fernandez', NULL, 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'linkedin.jpg', 'tlm120720kg-112-small.jpg', NULL, 'https://www.facebook.com/romanspade/', NULL, NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'roman@romanf.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', '2021-03-20'),
(4, 2, 'Nils', 'von Heijne', NULL, 'Nils is a holistic coach, advisor and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker and sound healer.', 'nilspng.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nils@wishful.se', 'UnZQaGdVYUdqdVBDTlYxS0JhUVhiUT09', 'S', 'S', '2021-01-17'),
(6, 2, 'Mattis', 'Norrvidd', 'Meditation coach', 'Mattis is a heartdriven environmental engineer who supports individuals, groups and companies to thrive through courses in self leadership, inspirational talks, meditations and coaching. He uses curiosity and clearity of mind to develop inner and outer connection.', 'mattis.png', 'capture_mnpng.png', NULL, 'https://www.facebook.com/mattis.hansson.1', NULL, NULL, NULL, 'https://insighttimer.com/mattis.norrvidd', 'mattis@comecloser.nu', 'cG5oOTNKb1ZZOGlHbnN2d3ZQUXJSQT09', 'S', 'S', '2021-04-22'),
(7, 2, 'Therese', 'Lyander', NULL, 'Therese is a transformational life coach, space holder and emotional guide. In her toolbox you will find\ndetox, fasting, breathwork, trauma therapy, meditation, creative feminine leadership and inspiration', 'teresa.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'therese@innerworks.io', 'elV5VzRzYnYreGVmQXo0OGJROFlIdz09', 'S', 'S', '2021-01-18'),
(8, 2, 'Johan', 'Reunanen', NULL, 'Johan is a coach, business advisor and changemaker. Besides being a partner at Scandinavian managament consultancy, Cordial, focusing on sustainable transformation, Johan shares his message of sobriety and loving leadership through talks, podcasts and writing', 'johan-r-1.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'johan@innerworks.io', 'WGJ6aiswWW5IMmhSR1o0Rzh2aTFCUT09', 'S', 'S', '2021-01-18'),
(9, 2, 'Faviana', 'Vangelius', NULL, 'Within the InnerWorks community Faviana does Sound Healing and Reiki. Beyond being one of InnerWorks co-founders, she also does Community Building and Explorer in Chief (Experience Manager).', 'faviana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'faviana@innerworks.io', 'WHc4UGNrckIyZTFrbWlJMFdsdFlTUT09', 'S', 'S', '2021-04-23'),
(10, 2, 'Daniel', 'Mueller-Gonzalez', NULL, 'Daniel is a Wim Hof Method instructor, breath guide and human evolution coach based in Stockholm.', 'daniel.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hello@danielmueller.se', 'QUtQTU56RGltQUZ4WWhnS0FUbExzUT09', 'S', 'S', '2021-04-22'),
(11, 2, 'Alexander', 'Holmberg', NULL, 'Alexander is a personal coach in Stockholm.', 'holmberg.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alexander@innerworks.io', 'S296aXBoNk9uNVR6NjlKZTVMWm1iQT09', 'S', 'S', '2021-01-18'),
(12, 2, 'Fredrik', 'Edlund', NULL, 'Fredrik Edlund is a yoga and meditation teacher based in Stockholm.', 'edlund1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fredrik@innerworks.io', 'Z2sxWmRJVDFjNVNvK3d1QjJkbm4rZz09', 'S', 'S', '2021-01-18'),
(13, 2, 'Kim', 'Gajraj', NULL, 'Kim is a holistic voice therapist based in Stockholm.', 'gajraj.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kim@kimgajraj.com', 'RzJySUZEUWFod3M2UCtuc1ZoZkxMQT09', 'S', 'S', '2021-04-22'),
(18, 2, 'Valeria', 'Payton', NULL, 'Valeria Payton is the founder of Tulum International School and the co-founder of Ephata.', 'valeria_profile_square2.jpg', NULL, 'MTA2OTkzMQ==', NULL, NULL, NULL, NULL, NULL, 'valeria@ephata.me', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', '2021-01-29'),
(29, 2, 'Eduardo Raymundo', 'Villagran', 'Personal Phrase Hello', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'eduardo.raymundo8787@gmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', 'S', '2021-03-30'),
(32, 2, 'Anja', 'Loizaga-Velder, PhD', NULL, 'Anja Katharina Loizaga-Velder is a German-Mexican clinical psychologist and psychotherapist, who has been investigating the therapeutic potential of the ritual use of psychedelic plants for over 25 years, in collaboration with indigenous healers. She earned a PhD degree in Medical Psychology from Heidelberg University in Germany with a doctoral dissertation on: The therapeutic uses of ayahuasca in addiction treatment. She is a founding member and director of research and psychotherapy of the Nierika Institute for Intercultural Medicine in Mexico and is an adjunct professor and researcher at the National Autonomous University of Mexico, where she researches the therapeutic potential of psychedelics in intercultural therapeutic settings. Additionally, she works as a psychotherapist with humanistic and transpersonal orientation in private practice.', 'anja_profilepng.png', 'anja_cover2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'nierika@gmx.net', 'c0JJbHcyaHllazMwdFNsQmxBVXdCdz09', 'S', 'S', '2021-03-04'),
(33, 2, 'Armando', 'Loizaga', NULL, 'Armando Loizaga is the President of Nierika AC, a non-profit NGO based in Mexico. He has dedicated over 20 years to the study of addictions and sacred plants. The Peyote Conservation Project works hand in hand with the Wixarika and lays out a strategy to reconsider peyote as a cultural and spiritual patrimony of indigenous peoples that calls for a sustainable development approach to sacred plants and tending to the natural environments where they grow. His work currently centers on advancing clinical research protocols that employ sacred plants and participating closely in traditional uses and psychedelic drug policy discussions with different levels of Mexican government and international agencies.', 'armando-loizaga_profile.jpg', 'armando_coverpng.png', NULL, NULL, NULL, NULL, NULL, NULL, 'armando@centronierika.net', 'TDBXTGNvYWRxNld4TFNrYjVDL2h4Zz09', 'S', 'S', '2021-03-04'),
(34, 2, 'Hannah', 'Rapp', NULL, 'My name is Hannah Rapp. I am a Licensed Mental Health Counselor (LMHC) and Creative Healing Arts Therapist with a passion for utilizing creativity and nature to embody our wildest authentic expressions and healing. My path and life\'s work emerged from a deep exploration of my emotional body, wild adventures, free-form movement and expansive transformation. It is my greatest joy and calling to be witness to the human experience.\n \nAs a therapist I help individuals to remember their purpose, connect to and trust their intuition, listen to the wisdom of their body, identify and understand their feelings, and live authentically. Through creativity, ceremony, movement and mindfulness I guide individuals to express themselves fully, to create a loving relationship to themselves and the Earth, and to nurture their inner child.   ', 'hannah_profile2.jpg', 'hannah_cover.webp', NULL, 'https://www.facebook.com/hannah.rapp.19', NULL, NULL, NULL, 'https://www.playfulpassages.com/', 'hannah@centronierika.net', 'a1FZWWhiZ05jVEczb2xEK05TNlJEZz09', 'S', 'S', '2021-03-04'),
(35, 2, 'Valeria', 'Payton', NULL, 'Valeria is the founder of Ephata and a passionate educator of several years ', 'valeria_profile_square2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'valeriapayton@gmail.com', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', '2021-03-13'),
(36, 2, 'Sanja Guide', 'Exanimo', NULL, 'jksdjkfsdjk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 's@exanimo.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', 'S', '2021-03-17'),
(37, 2, 'Roshi', 'Derakshan', NULL, 'I am a licenced Psychotherapist and Life Coach, with 10+ years of experience in treating disorders such as depression, burnout, anxiety, life crises and relational difficulties. \n\n \n\nI\'m not your \"typical therapist\". I\'m very honest in my assessments and goal oriented to get my clients to where they want to be. Unfortunately there are no shortcuts when doing self- work. Personal develoment and growth is hard work and there are no “cutting corners”, which is why I work online, with clients that are genuinely motivated and want to put in the work to transform their lives.  ', 'roshipng.png', 'roshi_coverpng.png', NULL, 'https://www.facebook.com/therapywithroshi', NULL, '@therapywithroshi', NULL, 'https://www.roshiderakshan.com/', 'roshi.derakhshan@gmail.com', 'MDR6aVV0SSswOGdXOEFLK2hJQ0V5dz09', 'S', 'S', '2021-04-23'),
(39, 2, 'Amber', 'Coffman', NULL, 'Amber is a growth strategist and the CEO of Orogamis, a team of strategists, marketers, engineers, and designers working together toward goals of growth. Orogamis fosters and embracse growth in all of its forms: professional, personal, and on behalf of their partners. Growth is the central theme of Orogamis, the mission, commitments, and company culture.', 'amber2.jpg', 'amber_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/amber-lee-coffman-81b43725/', 'amber@shala.us', 'YVN1WjhoYllpNGY4cEpnWkw4eGpCZz09', 'S', 'S', '2021-03-20'),
(40, 2, 'Ivan', 'Martin Maseda', NULL, 'Ivan is the Chief Technology Officer at Binfluencer and Professor in Machine Learning at IE Business School in Madrid, Spain. The use of technology to reduce human intervention in repetitive tasks should lead to an environment where brains could dedicate their time to more fulfilling endeavors. His current goal is to become an excellent bridge between business needs and the technical information challenges. Translating business needs into technical requirements is only half of the work, he will make sure the technical results obtained are completely understood and communicated to all team members and shareholders. ', 'ivan2.jpg', 'ivan_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/ivanmartinmaseda/', 'ivan@shala.us', 'UnRmYUJsZVJOZis1UDBZMHFLSHl3dz09', 'S', 'S', '2021-03-20'),
(41, 2, 'Vincent', 'Guntrum', NULL, 'I am a content and video marketing coach for trainers, coaches and healers. In the last few years I have struggled - mostly impatiently - off the beaten path through the jungle of self-discovery. Just to learn that unbelievable paths suddenly open up when I manage to relax and connect with my deeper resource. Life has something special in store for everyone. I have the honor to guide you to your resource, to support you to take the decisive steps and to show you to the world.', 'vincent2png.png', 'vincent2_coverpng.png', NULL, NULL, NULL, NULL, NULL, 'https://www.wild-games.net/', 'vincent@shala.us', 'YzRIeEEvM3JYbm9BWVdSQndhcG80UT09', 'S', 'S', '2021-03-20'),
(42, 2, 'Benjamin', 'Cuenod', NULL, 'All-rounder creative at heart, I offer passionate creative thinking, strategic planning, and analytical problem-solving. I believe in the brand story as a seed to systemic change within corporations, setting brands towards a sustainable and equitable framework, establishing resilience, and provoking evolution.\n\nServices:\nStrategy\nBranding\nDesign\nCreative Technology ', '02-benjamin-cuénod.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/benjamin-cu%C3%A9nod-b47b401/', 'ben@shala.us', 'OUpidDUwQ3dyZkZPZWRSWnhualFxdz09', 'S', 'S', '2021-03-22'),
(43, 2, 'Dmitrij', 'Achelrod', NULL, ' ', 'dimi.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dimi@shala.us', 'bXh6Z0VRNHRLekhLVFRmU3RjY0x1dz09', 'S', 'S', '2021-03-24'),
(44, 2, 'Roman', 'Fernandez', 'Innovation Coach', 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'profile2.jpg', 'tlm120720kg-112.jpg', NULL, 'https://www.facebook.com/romanspade', NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'www.romanf.com', 'roman@alumni.ie.edu', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'S', 'S', '2021-04-19'),
(45, 2, 'Levin', 'Dihard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'levin@dihard.com', 'cmRtM2I2aXFSeHRnbW1IN2pHMUd6QT09', 'S', 'S', '2021-04-07'),
(46, 2, 'Ryan', 'Monahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'roman@eleutheria.world', 'RTQ1SFVEWm1HVGFRNm1UMW1IRTRRZz09', 'S', 'S', '2021-04-13'),
(47, 2, 'Shalaland', 'Dhalaland', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shalaland@shalaland.com', 'OXE1R1JXQ2wzVlhydFQ0Z3JlTndYUT09', 'S', 'S', '2021-04-13'),
(48, 2, 'New', 'Guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'romedetroit@outlook.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', '2021-04-14'),
(49, 2, 'Gabo', 'Test guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gabofc+testguide@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', 'N', '2021-04-14'),
(50, 2, 'Ismelda', 'Lopez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ismelda@lopez.net', 'c1NqVVpFMEdnL0lQalhVanhoSUl1dz09', 'S', 'N', '2021-04-15'),
(51, 2, 'Sanna', 'Norrvidd', NULL, 'Sanna teach how to shift the focus from an exhausting fight with the body to a loving an empathic teamwork. Her methodology is transformational coaching that includes inner child work, meditation, breath work and somatic tools that creates more self love and safety from within.\r\n\r\nShe is working as coach for both individuals and groups to support them in developing body awareness and the ability to stay and be with the bodily sensations in this moment, regardless how challenging they may be. This work is a foundation in getting your sense of power and aliveness back and stay clear and resilient in the face of challenging emotions like fear and anger that often subconscious control our life and keeps us from getting what we truly desire.', '1d5d4c50-c910-4b00-9385-9c27e5ee6cae.jpeg', '62f8ba19-6d3f-4875-bc88-65d7d0b1b7cc.jpeg', NULL, 'https://m.facebook.com/sannanorrvidd/', NULL, '@sannanorrvidd', NULL, 'www.sannanorrvidd.com', 'sanna@sannanorrvidd.com', 'YTlsQmhkN01oV1Z0RnpFd2RCOCtrdz09', 'S', 'S', '2021-04-23'),
(52, 2, 'Amit', 'Paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hello@amitpaul.com', 'ekNMbXRNQk5RbHBvZG9ZMldsT1NvdE1kVVRBWllIenUxWElwYnI4bUFtbz0=', 'S', 'S', '2021-04-23'),
(53, 2, 'Christian ', 'Haag ', 'Don\'t forget to breath', NULL, '24252b5d-4d4d-4863-816c-d43a8634f510.jpeg', NULL, NULL, 'https://www.facebook.com/crillehaag', NULL, 'Instagram.com/crillehaag', NULL, 'Www.cowbellproductions.se', 'crillehaag@gmail.com', 'UTVqcms0RDNoRkE5TVlWZURXSHJEUT09', 'S', 'S', '2021-04-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachesclients`
--

CREATE TABLE `coachesclients` (
  `coachClientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachesclients`
--

INSERT INTO `coachesclients` (`coachClientId`, `coachId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(15, 2, 3, 'R', NULL),
(30, 3, 3, 'N', NULL),
(33, 5, 3, 'N', NULL),
(34, 6, 3, 'N', NULL),
(35, 7, 3, 'R', 'Not in onboarding meeting'),
(36, 8, 3, 'R', 'Not in onboarding meeting'),
(38, 10, 3, 'N', NULL),
(39, 11, 3, 'R', 'Not in meeting'),
(40, 12, 3, 'R', 'Not in onboarding meeting'),
(41, 13, 3, 'N', NULL),
(42, 14, 3, 'N', NULL),
(43, 15, 3, 'N', NULL),
(46, 16, 3, 'N', NULL),
(48, 17, 3, 'N', NULL),
(50, 18, 4, 'N', NULL),
(52, 19, 4, 'N', NULL),
(58, 9, 3, 'N', NULL),
(59, 20, 3, 'R', 'You are a bad teacher'),
(60, 21, 3, 'S', NULL),
(61, 22, 1, 'S', NULL),
(62, 23, 1, 'S', NULL),
(63, 24, 1, 'N', NULL),
(64, 25, 1, 'R', 'porue si porque quise y porque puedo'),
(65, 26, 1, 'N', NULL),
(66, 27, 1, 'R', 'asfasf'),
(67, 28, 1, 'S', 'test reject'),
(68, 29, 1, 'N', 'porque puedo'),
(69, 3, 4, 'N', NULL),
(70, 30, 3, 'N', 'Because we dont like you'),
(71, 31, 3, 'N', 'Your are not a true coach'),
(72, 1, 1, 'S', NULL),
(73, 32, 7, 'N', NULL),
(74, 33, 7, 'N', NULL),
(75, 34, 7, 'N', NULL),
(76, 35, 2, 'N', NULL),
(77, 18, 2, 'S', NULL),
(112, 48, 8, 'N', NULL),
(114, 50, 3, 'S', NULL),
(111, 47, 8, 'S', NULL),
(108, 44, 3, 'N', NULL),
(107, 43, 10, 'S', NULL),
(106, 42, 9, 'N', NULL),
(105, 29, 8, 'N', NULL),
(104, 41, 9, 'N', NULL),
(103, 40, 9, 'N', NULL),
(102, 39, 9, 'N', NULL),
(101, 38, 9, 'N', NULL),
(100, 37, 3, 'N', NULL),
(99, 37, 1, 'S', NULL),
(113, 49, 8, 'S', NULL),
(115, 51, 3, 'N', NULL),
(116, 52, 3, 'S', NULL),
(117, 53, 3, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachserie`
--

CREATE TABLE `coachserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorcatalog`
--

CREATE TABLE `colorcatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorcatalog`
--

INSERT INTO `colorcatalog` (`catalogId`, `nombre`) VALUES
(1, 'Main Color'),
(2, 'Secondary Color'),
(3, 'Text Color'),
(4, 'Text Hover Color');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `colorId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorsclient`
--

CREATE TABLE `colorsclient` (
  `colorClientId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorsclient`
--

INSERT INTO `colorsclient` (`colorClientId`, `clientId`, `catalogId`, `color`) VALUES
(1, 3, 1, '#e3913a'),
(2, 3, 2, '#815e39'),
(3, 3, 3, '#918a83'),
(4, 3, 4, '#e8ddd2'),
(5, 8, 1, '#fb2449');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

CREATE TABLE `diccionario` (
  `iddiccionario` int(11) NOT NULL,
  `campo` varchar(200) NOT NULL,
  `etiqueta` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diccionario`
--

INSERT INTO `diccionario` (`iddiccionario`, `campo`, `etiqueta`) VALUES
(8, 'tipousuario', 'User\'s Type'),
(9, 'pass', 'Password'),
(14, 'statuspago', 'Payment Status'),
(28, 'usuario', 'User'),
(27, 'tipoadmin', 'Admin Type'),
(23, 'category', 'Categories'),
(26, 'nombre', 'Name'),
(29, 'accessadmin', 'Admin Access'),
(30, 'logotipo', 'Logo'),
(31, 'icono', 'Icon'),
(32, 'biografia', 'Biography'),
(33, 'foto', 'Photo'),
(34, 'telefono', 'Phone'),
(35, 'colorcatalog', 'Color\'s Catalog'),
(36, 'servicescatalog', 'Service\'s Catalog'),
(37, 'colorsclient', 'Clients Colors'),
(38, 'descripcion', 'Description'),
(39, 'imagen', 'Image'),
(40, 'titulo', 'Title'),
(41, 'apellido', 'Last Name'),
(42, 'fecha', 'Date'),
(43, 'users', 'Members'),
(44, 'bannerhome', 'Home Banners'),
(45, 'slidehome', 'Home Slider'),
(46, 'bannersomos', 'About Content'),
(47, 'enviar', 'Send'),
(48, 'manda', 'Send'),
(49, 'tipollamada', 'Session Access'),
(50, 'postcategory', 'Category'),
(51, 'posicion', 'Position'),
(52, 'imagenMovil', 'Mobile image'),
(53, 'boton', 'Button'),
(54, 'botonMovil', 'Button Mobile'),
(55, 'botonMobile', 'Button Mobile'),
(56, 'statusaccount', 'Status Account'),
(57, 'coach', 'Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email`
--

CREATE TABLE `email` (
  `emailId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `typeId` int(11) NOT NULL COMMENT 'opciones=typemail',
  `subject` varchar(200) NOT NULL,
  `generalText` varchar(20000) NOT NULL COMMENT 'html'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE `gallery` (
  `imageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `title` varchar(200) NOT NULL,
  `altText` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL COMMENT 'liga',
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`imageId`, `clientId`, `image`, `title`, `altText`, `link`, `position`) VALUES
(1, 8, '42-17452823.jpg', 'Image 1', 'Image 1', NULL, 2),
(2, 8, '42-17454853.jpg', 'Image 2', 'Image 2', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generals`
--

CREATE TABLE `generals` (
  `generalId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `hideCoachContent` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `homeMenuText` varchar(200) NOT NULL,
  `exploreMenuText` varchar(200) NOT NULL,
  `coachesMenuText` varchar(200) NOT NULL,
  `aboutMenuText` varchar(200) NOT NULL,
  `coachText` varchar(200) NOT NULL,
  `memberText` varchar(200) NOT NULL,
  `liveMenuText` varchar(200) NOT NULL,
  `liveSessionText` varchar(200) NOT NULL,
  `processingText` varchar(200) NOT NULL,
  `signUpText` varchar(200) NOT NULL,
  `headerTitle` varchar(200) NOT NULL,
  `subtitleHeader` varchar(500) NOT NULL,
  `headerButtonShow` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `headerButtonFontSize` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `headerButtonText` varchar(200) DEFAULT NULL,
  `headerButtonUrl` varchar(200) DEFAULT NULL COMMENT 'liga',
  `headerButtonTextColor` varchar(10) NOT NULL DEFAULT '#000000' COMMENT 'color',
  `headerButtonBackgroundColor` varchar(10) NOT NULL DEFAULT '#ffffff' COMMENT 'color',
  `headerButtonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `videoHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `videoMobileHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titleCategories` varchar(200) NOT NULL,
  `subtitleCategories` varchar(500) NOT NULL,
  `titleCoaches` varchar(200) NOT NULL,
  `subtitleCoaches` varchar(500) NOT NULL,
  `titleClasses` varchar(200) NOT NULL,
  `subtitleClasses` varchar(500) NOT NULL,
  `trialDaysText` varchar(200) NOT NULL,
  `trialDescriptionText` varchar(500) NOT NULL,
  `blogText` varchar(200) NOT NULL DEFAULT 'Blog',
  `signUpCoachText` varchar(200) NOT NULL DEFAULT 'Become a Guide'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `generals`
--

INSERT INTO `generals` (`generalId`, `clientId`, `hideCoachContent`, `homeMenuText`, `exploreMenuText`, `coachesMenuText`, `aboutMenuText`, `coachText`, `memberText`, `liveMenuText`, `liveSessionText`, `processingText`, `signUpText`, `headerTitle`, `subtitleHeader`, `headerButtonShow`, `headerButtonFontSize`, `headerButtonText`, `headerButtonUrl`, `headerButtonTextColor`, `headerButtonBackgroundColor`, `headerButtonRounded`, `videoHeader`, `videoMobileHeader`, `titleCategories`, `subtitleCategories`, `titleCoaches`, `subtitleCoaches`, `titleClasses`, `subtitleClasses`, `trialDaysText`, `trialDescriptionText`, `blogText`, `signUpCoachText`) VALUES
(1, 3, 'N', 'Home', 'Journeys', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, '/explore', '#000000', '#ffffff', 'N', 'gradientwithtext_4096x1728.mp4', 'mobile_text_new.mp4', 'UPGRADE YOUR HUMAN SOFTWARE', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'START WITH FREE LIMITED ACCESS', 'Get started with limited access to our breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(2, 4, 'N', 'Home', 'Classes', 'Teachers', 'About', 'Teacher', 'Student', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Faith. Wisdom. Goodness', 'Prepare our children for the future with passion based learning', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'header-ephata.mp4', NULL, 'FALL IN LOVE WITH LEARNING', 'Find the live classes, educational videos and exercises that nurture children beyond the curriculums of regular school', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'CLASSES', 'FIND A CLASS TO GET STARTED', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited purpose approach, passion based learning, self efficacy, academics, environmental awareness, social responsibility and spiritual balance classes', 'Blog', 'Become a Guide'),
(3, 7, 'N', 'Home', 'Programs', 'Facilitators', 'Location', 'Facilitators', 'Guests', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'HEAL - CONNECT - GROW', 'Holistic eco-center for retreats and programs in personal growth, spirituality, healing arts and therapeutic sciences', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', '', NULL, 'Our programs', 'Nierika offers therapeutic programs that integrate elements of traditional indigenous medicine and modern psychotherapy facilitated by multidisciplinary professionals\n', 'Meet our facilitators', 'Heal, connect and grow through programs facilitated by our world class therapists, instructors and mentors both in-person at our eco-center and online.', 'PROGRAMS', 'FIND A PROGRAM THAT WORKS FOR YOU', 'Sign-up for our programs', 'Tell us about yourself and what brings you to Nierika. We respond to everyone with an open heart.', 'Blog', 'Become a Guide'),
(4, 8, 'N', 'Home', 'Retreats', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'FIND PROFOUND', 'Exponential transformation by combining mind, body and spirit', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', NULL, 'WORK ON YOUR INNER self', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(5, 9, 'N', 'Home', 'Tutorials', 'Experts', 'About', 'Expert', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', '', '', 'LEARN OUR SECRETS', 'Everything we applied to build and grow Shala is here for you to use – Learn how to create strong content, prepare for camera, build your marketing strategy, find talent, design your brand, scale users and a lot more!', 'MEET OUR EXPERTS', 'Everyone who advised or contributed to Shala are now part of our expert team on our platform. They are here to help you build your platform, create winning content, scale your users and make your vision a reality!', 'TUTORIALS', 'FIND A TUTORIAL TO HELP REALIZE THE FULL POTENTIAL OF YOUR PLATFORM', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited access to our courses and building out your test platform. Cancel anytime.', 'Blog', 'Become a Guide'),
(6, 10, 'S', 'Home', 'Programs', 'Team', 'About Us', 'Team', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Explore. Realize. Transform.', 'The only journey is the one within', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', '', 'START YOUR JOURNEY WITH US', 'We enable leaders to safely embark on probably the most challenging and yet powerful journey of their lives\n', 'MEET OUR TEAM', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide'),
(7, 11, 'N', 'Home', 'Classes', 'Guides', 'About Us', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Align. Heal. Act.', 'Upgrade your human software to unlock your full potential and find your flow', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', 'video-header.mp4', 'video-header.mp4', 'WORK ON YOUR INNER SELF', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home`
--

CREATE TABLE `home` (
  `homeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `home`
--

INSERT INTO `home` (`homeId`, `nombre`) VALUES
(1, 'Header'),
(2, 'Categories'),
(3, 'Coaches'),
(4, 'Banners'),
(5, 'TrialText'),
(6, 'LiveSessions');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homeplatform`
--

CREATE TABLE `homeplatform` (
  `homePlatformId` int(11) NOT NULL,
  `homeId` int(11) NOT NULL COMMENT 'opciones=home',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `homeplatform`
--

INSERT INTO `homeplatform` (`homePlatformId`, `homeId`, `clientId`) VALUES
(117, 3, 3),
(6, 1, 9),
(7, 2, 9),
(8, 3, 9),
(9, 4, 9),
(10, 5, 9),
(90, 6, 8),
(89, 5, 8),
(88, 4, 8),
(87, 3, 8),
(86, 2, 8),
(16, 1, 7),
(17, 2, 7),
(18, 3, 7),
(19, 4, 7),
(20, 5, 7),
(21, 1, 4),
(22, 2, 4),
(23, 3, 4),
(24, 4, 4),
(25, 5, 4),
(57, 6, 10),
(56, 5, 10),
(55, 4, 10),
(54, 3, 10),
(53, 2, 10),
(116, 1, 3),
(41, 6, 4),
(42, 6, 7),
(85, 1, 8),
(44, 6, 9),
(52, 1, 10),
(91, 1, 11),
(92, 2, 11),
(93, 3, 11),
(94, 4, 11),
(95, 5, 11),
(96, 6, 11),
(97, 2, 12),
(98, 3, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `levelId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`levelId`, `clientId`, `nombre`, `posicion`) VALUES
(1, 3, 'Easy', 1),
(2, 8, 'Easy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pages`
--

INSERT INTO `pages` (`pageId`, `nombre`) VALUES
(1, 'Classes'),
(2, 'About Us'),
(3, 'Live Session'),
(4, 'Coaches'),
(5, 'Gallery');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passcambio`
--

CREATE TABLE `passcambio` (
  `passId` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `passcambio`
--

INSERT INTO `passcambio` (`passId`, `tipo`, `id`, `token`) VALUES
(3, 1, 5, 'T1lyVlFod3ladlhzTWpHaGVYK2k4RlN5Vzg2ZVp0ZHRzcytpd05qMmlMND0='),
(4, 1, 6, 'cURZQSsxYU01elA0T2lMVnJPZWJDS0NFUW1vc2ljSWFuNkZpcldhOUIxZz0='),
(27, 1, 49, 'VDhtdXpsSkpDeFpjY0lCMGNEOG9yTU5ETjBKVHYyZFJxcERMcEJDdFk3OD0='),
(11, 2, 25, 'RkR4dUQ2WDgvWU51eG92eFh3Zk14dmx4QmFaaC9RQVVXbGduT3ZGQ3kzcz0='),
(12, 2, 25, 'bC82dDVvTUMyaWEvTUExSzNmTi9aZGcrNkIyekt2by9VNnNxRDZvZGFtST0='),
(13, 2, 26, 'dG1xcmFIcUtHOXI1STlVRVpLZWcvdXNMdi9wM2xhUEZNeDdZZGJ0eTduRT0='),
(19, 1, 3, 'czkzeVRWUjZrSUtsZkl3WDZ2dVg3WHRzaGpWUVNCd0tFQW5HZWsxWkxaRT0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platforms`
--

CREATE TABLE `platforms` (
  `platformId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `clientId` int(11) DEFAULT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platforms`
--

INSERT INTO `platforms` (`platformId`, `nombre`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `clientId`) VALUES
(1, 'Innerworks', 'innerworks_logo_5-innerworks.png', 'innerworks_logo_black-innerworks.png', 'hero-new-light-innerworks.otf', NULL, 'innerworks', 'roman@romanf.com', 3),
(2, 'Ephata', 'ephata.png', 'ephata.png', 'roboto-regular.ttf', NULL, 'ephata', 'elopez@junkyard.mx', 4),
(3, 'Nierika', 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', 'roboto-regular-nierika.ttf', NULL, 'nierika', 'universidad@nierika.com', 7),
(4, 'Test', 'zenrise_logo_01_black_background-test.png', 'zenrise_logo_01_black_background-test.png', 'hero-new-light-test.otf', NULL, 'test', 'elopez@junkyard.mx', 8),
(5, 'Shala', 'logo-shala.png', 'logo-shala.png', 'roboto-regular-shala.ttf', NULL, 'shala', 'rome7f@gmail.com', 9),
(6, 'Eleutheria', 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', 'roboto-regular-eleutheria.ttf', NULL, 'Eleutheria', 'rome7f@gmail.com', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platformservices`
--

CREATE TABLE `platformservices` (
  `psId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=servicescatalog'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platformservices`
--

INSERT INTO `platformservices` (`psId`, `platformId`, `catalogId`) VALUES
(80, 1, 5),
(79, 1, 4),
(78, 1, 3),
(77, 1, 2),
(76, 1, 1),
(35, 2, 5),
(34, 2, 4),
(33, 2, 3),
(32, 2, 2),
(31, 2, 1),
(46, 3, 1),
(47, 3, 2),
(48, 3, 3),
(49, 3, 4),
(50, 3, 5),
(105, 4, 5),
(104, 4, 4),
(103, 4, 3),
(102, 4, 2),
(101, 4, 1),
(100, 5, 5),
(99, 5, 4),
(98, 5, 3),
(97, 5, 2),
(96, 5, 1),
(106, 6, 1),
(107, 6, 2),
(108, 6, 3),
(109, 6, 4),
(110, 6, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postcategory`
--

CREATE TABLE `postcategory` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `postcategory`
--

INSERT INTO `postcategory` (`categoryId`, `clientId`, `name`) VALUES
(1, 3, 'Category One'),
(2, 3, 'Category Two'),
(3, 8, 'Category One'),
(4, 8, 'Category Two');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `postId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `content` text NOT NULL COMMENT 'html',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`postId`, `clientId`, `categoryId`, `title`, `image`, `video`, `content`, `fecha`) VALUES
(1, 3, 1, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(2, 3, 1, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T FORCE IT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">KEEP IT SHORT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">TRY SOME BREATHWORK</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T GET HUNG UP ON FORM</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATE ON THE MOVE</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATION ISN’T PERFECT!</strong></h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-03-15'),
(3, 3, 2, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">7 a.m.</strong> <br style=\"overflow-wrap: break-word;\">Wake up. This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-03-16'),
(4, 8, 3, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(5, 8, 21, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T FORCE IT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">KEEP IT SHORT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">TRY SOME BREATHWORK</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T GET HUNG UP ON FORM</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATE ON THE MOVE</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATION ISN’T PERFECT!</h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-04-08'),
(6, 8, 21, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', 'MTEwNzI1MQ==', '<p>Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p><span style=\"font-weight: bold;\">7 a.m. Wake up.</span></p><p>This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-04-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionnaire`
--

CREATE TABLE `questionnaire` (
  `questionId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `question` varchar(1000) NOT NULL COMMENT 'nombre',
  `answerOne` varchar(200) NOT NULL,
  `answerTwo` varchar(200) NOT NULL,
  `answerThree` varchar(200) DEFAULT NULL,
  `answerFour` varchar(200) DEFAULT NULL,
  `answerFive` varchar(200) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `questionnaire`
--

INSERT INTO `questionnaire` (`questionId`, `clientId`, `question`, `answerOne`, `answerTwo`, `answerThree`, `answerFour`, `answerFive`, `position`) VALUES
(1, 3, 'How did you learn about Innerworks?', 'Invited by a Guide/Coach', 'Online search', 'Through company HR', 'Referred by a friend', 'Other', 1),
(6, 4, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(7, 7, 'What is your interest in visiting Nierika?', 'Lodging', 'Therapy', 'Volunteer', 'Retreat', 'Event space', 1),
(8, 7, 'How did you find out about us?', 'Referral', 'Webpage', 'Web search', 'Other', NULL, 2),
(9, 8, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(10, 8, 'Question Two', 'One', 'Two', 'Three', 'Four', 'Five', 2),
(11, 8, 'Question Three', 'One', 'Two', 'Three', 'Four', 'Five', 3),
(12, 8, 'Question Four', 'One', 'Two', 'Three', 'Four', 'Five', 4),
(13, 8, 'Question Five', 'One', 'Two', 'Three', 'Four', 'Five', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serierequest`
--

CREATE TABLE `serierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`serieId`, `clientId`, `coachId`, `nombre`, `description`, `photo`, `video`, `accessId`) VALUES
(14, 3, 1, 'The Loop', 'The Loop is a course in mastering and manifesting change and transformation in your life. The course entails a total of 10 episodes, out of which 4 are optional as live sessions together with Nils.', 'yinyang.jpeg', 'MTEwNzIwNA==', 3),
(16, 4, 18, 'Hero\'s Journey', NULL, NULL, NULL, 3),
(17, 4, 18, 'Ephata Summary', NULL, NULL, NULL, 3),
(18, 3, 6, 'Meditations', 'This series will help you get started with meditation through my short guided exercises.', 'top-5-scientific-findings-on-meditationmindfulness.jpeg', 'MTA3NDU2MA==', 1),
(22, 4, 3, 'Ephata Series', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '42-15833533.jpg', 'MTA4NTMxMg==', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seriescategories`
--

CREATE TABLE `seriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seriescategories`
--

INSERT INTO `seriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(39, 14, 1),
(17, 19, 2),
(13, 20, 3),
(18, 21, 2),
(23, 13, 1),
(19, 21, 4),
(24, 22, 7),
(38, 18, 2),
(29, 25, 2),
(30, 23, 4),
(31, 26, 13),
(40, 14, 2),
(41, 14, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicescatalog`
--

CREATE TABLE `servicescatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicescatalog`
--

INSERT INTO `servicescatalog` (`catalogId`, `nombre`) VALUES
(1, 'Schedulling Calls'),
(2, 'Pre-Recorded Videos'),
(3, 'Live Video Conferences'),
(4, 'Interactive Exercises & Tests'),
(5, 'Reporting');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shala`
--

CREATE TABLE `shala` (
  `shalaId` int(11) NOT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `spotlightrApiKey` varchar(200) DEFAULT NULL,
  `stripePublicKey` varchar(200) DEFAULT NULL,
  `stripePrivateKey` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shala`
--

INSERT INTO `shala` (`shalaId`, `logotipo`, `icono`, `spotlightrApiKey`, `stripePublicKey`, `stripePrivateKey`) VALUES
(1, '', '', 'c53bcd228d6d73879a567d77fa35c5368d390ecb', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slidehome`
--

CREATE TABLE `slidehome` (
  `slideId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `imagen` varchar(200) NOT NULL COMMENT 'archivo',
  `imagenMovil` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `altText` varchar(200) DEFAULT NULL,
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slidehome`
--

INSERT INTO `slidehome` (`slideId`, `clientId`, `imagen`, `imagenMovil`, `altText`, `posicion`) VALUES
(1, 7, 'slide1.jpg', 'slide1movil.jpg', NULL, 1),
(2, 7, 'slide2.jpg', 'slide2movil.jpg', NULL, 2),
(3, 7, 'slide3.jpg', 'slide3movil.jpg', NULL, 3),
(4, 7, 'slide4.jpg', 'slide4movil.jpg', NULL, 4),
(5, 7, 'slide5.jpg', 'slide5movil.jpg', NULL, 5),
(6, 9, 'slider1png.png', NULL, 'All-in-one website builder for secured platforms with LIVE content and more …', 1),
(11, 9, 'slider6png.png', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statusaccount`
--

CREATE TABLE `statusaccount` (
  `statusId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `statusaccount`
--

INSERT INTO `statusaccount` (`statusId`, `nombre`) VALUES
(1, 'Inactive'),
(2, 'Active');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoadmin`
--

CREATE TABLE `tipoadmin` (
  `tipoId` int(11) NOT NULL COMMENT ' multiple=accessadmin,accessuseradmin',
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoadmin`
--

INSERT INTO `tipoadmin` (`tipoId`, `nombre`) VALUES
(2, 'Global Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipollamada`
--

CREATE TABLE `tipollamada` (
  `tipoId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipollamada`
--

INSERT INTO `tipollamada` (`tipoId`, `nombre`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered Users');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typemail`
--

CREATE TABLE `typemail` (
  `typeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typemail`
--

INSERT INTO `typemail` (`typeId`, `nombre`) VALUES
(1, 'Password Recovery'),
(2, 'Contact Web'),
(3, 'Rejected Coach'),
(4, 'Approved Coach'),
(5, 'Registered Coach'),
(6, 'Requested Series Access'),
(7, 'Requested Coach'),
(8, 'Approved Member'),
(9, 'Rejected Member'),
(10, 'Approval coach request '),
(11, 'Approval member request'),
(12, 'Email Confirmation');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userarticleserie`
--

CREATE TABLE `userarticleserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercall`
--

CREATE TABLE `usercall` (
  `userCallId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercall`
--

INSERT INTO `usercall` (`userCallId`, `callId`, `userId`) VALUES
(1, 2, 3),
(2, 3, 3),
(3, 4, 3),
(4, 6, 3),
(5, 7, 3),
(6, 9, 4),
(7, 10, 4),
(8, 11, 4),
(9, 14, 7),
(10, 15, 3),
(11, 16, 3),
(12, 19, 4),
(13, 20, 4),
(14, 22, 11),
(15, 36, 15),
(16, 47, 3),
(17, 49, 3),
(18, 70, 24),
(19, 76, 4),
(20, 43, 3),
(21, 79, 34),
(22, 80, 3),
(23, 80, 4),
(24, 80, 26),
(25, 80, 34),
(26, 74, 45),
(28, 74, 51);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userclient`
--

CREATE TABLE `userclient` (
  `userClientId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userclient`
--

INSERT INTO `userclient` (`userClientId`, `userId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(2, 1, 3, 'N', NULL),
(9, 3, 3, 'N', NULL),
(8, 2, 3, 'N', NULL),
(10, 4, 3, 'N', NULL),
(11, 5, 3, 'N', NULL),
(12, 6, 3, 'N', NULL),
(13, 3, 3, 'N', NULL),
(14, 3, 3, 'N', NULL),
(15, 7, 3, 'N', NULL),
(16, 8, 3, 'N', NULL),
(17, 4, 3, 'N', NULL),
(18, 9, 3, 'R', NULL),
(19, 10, 3, 'N', NULL),
(20, 11, 3, 'N', NULL),
(21, 12, 3, 'N', NULL),
(22, 12, 3, 'N', NULL),
(23, 13, 3, 'N', NULL),
(24, 13, 3, 'N', NULL),
(25, 14, 3, 'N', NULL),
(26, 15, 3, 'N', NULL),
(27, 16, 3, 'N', NULL),
(28, 17, 4, 'N', NULL),
(29, 18, 4, 'N', NULL),
(30, 19, 3, 'N', NULL),
(31, 3, 3, 'N', NULL),
(32, 11, 1, 'N', NULL),
(33, 20, 1, 'N', NULL),
(34, 21, 3, 'N', NULL),
(35, 22, 7, 'N', NULL),
(36, 23, 7, 'N', NULL),
(66, 4, 8, 'N', NULL),
(65, 3, 8, 'N', NULL),
(64, 25, 3, 'N', NULL),
(63, 10, 8, 'N', NULL),
(67, 26, 3, 'N', 'test'),
(68, 27, 8, 'N', NULL),
(69, 28, 8, 'N', 'ascasc'),
(70, 30, 3, 'S', NULL),
(71, 31, 8, 'N', NULL),
(72, 32, 8, 'S', NULL),
(73, 33, 8, 'S', NULL),
(74, 34, 3, 'N', NULL),
(75, 35, 3, 'N', NULL),
(76, 36, 3, 'S', NULL),
(77, 37, 3, 'N', NULL),
(78, 38, 3, 'S', NULL),
(79, 39, 3, 'S', NULL),
(80, 40, 3, 'N', NULL),
(81, 41, 3, 'N', NULL),
(82, 42, 3, 'N', NULL),
(83, 43, 3, 'N', NULL),
(84, 44, 3, 'S', NULL),
(85, 45, 3, 'N', NULL),
(86, 46, 3, 'N', NULL),
(87, 47, 3, 'N', NULL),
(88, 48, 3, 'S', NULL),
(89, 49, 3, 'S', NULL),
(90, 50, 3, 'N', NULL),
(91, 51, 3, 'N', NULL),
(92, 52, 3, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`userId`, `statusId`, `nombre`, `apellido`, `foto`, `email`, `pass`, `emailConfirmed`, `fecha`) VALUES
(3, 2, 'Eduardo ', 'Lopez', 'teaser-uhmay.jpg', 'elopez@junkyard.mx', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-05'),
(4, 2, 'Gabo Student', 'Student', NULL, 'gabofc@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-17'),
(5, 2, 'Gabo', 'T1', NULL, 'gabofc+t1@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-05'),
(6, 2, 'Gabo ', 'T2', NULL, 'gabofc+t2@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-05'),
(7, 2, 'Ismelda', 'Coco', NULL, 'coco@romanf.com', 'WDgyUmpZbjQwUVlLSVlkRzBMQkZ3dz09', 'S', '2021-01-08'),
(8, 2, 'John', 'Doe', NULL, 'johndoe@test.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'S', '2021-01-12'),
(9, 1, 'Thalia', 'Wilson', NULL, 'thalia-deactivated-2021-05-03@gmail.com', 'SG5POGY5amJ2QU1INVFzK2tHWWVOdz09', 'S', '2021-01-18'),
(30, 1, 'Eduardo Raymundo', 'Villagran', NULL, 'eduardo_raymundo-deactivated-2021-05-03@hotmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-13'),
(11, 1, 'David', 'Lindberg', NULL, 'david-deactivated-2021-05-03@innerworks.io', 'ckRpNTNmbkRmL243TEM4eGFON1MyQT09', 'S', '2021-03-01'),
(12, 1, 'bb2', 'bb2', NULL, 'test-deactivated-2021-05-03@me.com', 'cXZLWUs2a3V5aDF0aE9PT2xUUDQzdz09', 'S', '2021-01-19'),
(13, 1, 'Iván', 'Martin', '4880-327.jpg', 'martin.maseda-deactivated-2021-05-03@gmail.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(14, 1, 'Ivan test', 'Test', NULL, 'ivan-deactivated-2021-05-03@test.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(15, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-25'),
(16, 2, 'Nils', 'von Heijne 2', NULL, 'nils@worldofwisdom.io', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-26'),
(17, 2, 'EDUARDO', 'LOPEZ', NULL, 'prueba@estudiante.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-28'),
(18, 2, 'Roman', 'Fernandez', NULL, 'roman@ephata.me', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-01-28'),
(19, 1, 'Gabo', 'FC', NULL, 'gabofc+t100-deactivated-2021-05-03@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-02-09'),
(20, 2, 'Johnathan', 'Meade', NULL, 'john@innerworks.io', 'YW9WODhydnBMTThSVUhINyszWkkvZz09', 'S', '2021-03-03'),
(22, 2, 'Roman', 'Fernandez', NULL, 'rome@rome.net', 'ZUYyL2VzcmZMT1RLU01OTzhGOUtZUT09', 'S', '2021-03-04'),
(23, 2, 'john ', 'doe', NULL, 'babaji@juice.net', 'dWMwTEVJL1UzVnozUU5aM1hZWnBJQT09', 'S', '2021-03-04'),
(24, 2, 'StudentSanja', 'Menicanin', NULL, 'hello@sanjamenicanin.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', '2021-03-17'),
(25, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@email.com', 'bkpYdm05ZUJRaUJ5NkZ6bUVXbjdvUT09', 'S', '2021-03-26'),
(26, 1, 'Test', 'User', NULL, 'test-deactivated-2021-05-03@aprobado.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-09'),
(27, 2, 'Gabriel', 'Test Estudiante', NULL, 'gabofc+student_test1@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-04-11'),
(28, 2, 'Eduardo Raymundo', 'Villagran', NULL, 'lourdes.serrano.tun@gmail.com', 'Um0wYlVaNEZtRkd4OGFWWGQ2Mk1OUT09', 'S', '2021-04-12'),
(31, 2, 'test gabo ', 'gabo test', NULL, 'gabofc+testvalidation@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-04-14'),
(32, 2, 'Gabo 2', 'Gabo 2', NULL, 'web@neotegra.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'N', '2021-04-14'),
(33, 2, 'dasda', 'asdas', NULL, 'asdasda@asdadds.com', 'clJmM2xTUTcyVVNJTFlseEEwWm95U2lSdENyN1FOVmpVUG4wbFZ4SGhkcz0=', 'N', '2021-04-14'),
(34, 1, 'Test', 'User', NULL, 'rinz7-deactivated-2021-05-03@hotmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-14'),
(35, 1, 'Stephanie', 'Wilson', NULL, 'rome7f-deactivated-2021-05-03@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-30'),
(36, 2, 'eris', 'diel', NULL, 'eris@diel.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'N', '2021-05-01'),
(37, 2, 'Gunilla', 'Rubin', NULL, 'gunilla.rubin@sweco.se', 'V3BTdHNNUFlpcXpsV3ZEOHhZeWNnZz09', 'S', '2021-05-02'),
(38, 2, 'Anja', 'Thernström', NULL, 'anja.thernstrom@sweco.se', 'eWhuVGxIU1U5S2F0bUZ4RTV0cUFydz09', 'N', '2021-05-02'),
(39, 2, 'Cecilia ', 'Engvall', NULL, 'cissiande@hotmail.com', 'aStnelp2TFN5Y2owclBUS2paanFsUT09', 'N', '2021-05-02'),
(40, 2, 'Malin', 'Magnusson', NULL, 'malin.k.magnusson@gmail.com', 'VU55U3BSTmYxc1V1WFhnQjdWOERadz09', 'S', '2021-05-02'),
(41, 2, 'Dagmara', 'Fajkis-Lau', NULL, 'dagmara.fajkis@gmail.com', 'OXBFbGFrRVA5UE4wZUdLMWZ6ZDdjUT09', 'S', '2021-05-02'),
(42, 2, 'Moa', 'Andreasson Gistedt', NULL, 'moa.gistedt@gmail.com', 'NE5aTFUzV2xTSGxEczcvOHZtZkNSdz09', 'S', '2021-05-02'),
(43, 2, 'Theres', 'Kvarnström', NULL, 'theres.kvarnstrom@sweco.se', 'SEhreDc5V3dDWFJUMTR3YkZoZnpPUT09', 'S', '2021-05-03'),
(44, 2, 'Mari', 'Wennlund', NULL, 'mari.wennlund@sweco.se', 'S2pNeE9yU1RocmFHbzAwT2hXLzFMUT09', 'N', '2021-05-03'),
(45, 2, 'Peter', 'Westerberg', NULL, 'peterwesterberg2@gmail.com', 'VWViNklTZ0RBby9FOGJmTzlhWGhXdz09', 'S', '2021-05-03'),
(46, 2, 'Isa', 'Brisby', NULL, 'isa.brisby@sweco.se', 'djk3dXp4K3ozeW0xc3Mwd0hrTkhiQT09', 'S', '2021-05-03'),
(47, 2, 'Helena', 'Törmä', NULL, 'haetii@gmail.com', 'ZTkzbWVpS21SVW1WL2VEeTN5Q0R0ZkZONlA1Mm9JZjNvbXdjTk1SV3NDTT0=', 'S', '2021-05-04'),
(48, 2, 'Marie', 'Alke', NULL, 'marie.alke@hotmail.com', 'M1ZId3VVZUo3STFrcUVTQkRtUnljZz09', 'N', '2021-05-04'),
(49, 2, 'Caroline', 'Hansson', NULL, 'hansson_120@hotmail.com', 'cm1GeU9VTDZKbFB2QWdFbVcweXpKQT09', 'N', '2021-05-05'),
(50, 2, 'Niklas', 'Gripenstam', NULL, 'niklas.gripenstam@hotmail.com', 'L1NsY1dyWDdIcFBBeHhjWDRnamE5dz09', 'S', '2021-05-05'),
(51, 2, 'Helia', 'Tehranchi', NULL, 'helia.tehranchi@sweco.se', 'MU1XVkU3QVhvSnlZa0Jlc2JlRVIzUT09', 'S', '2021-05-05'),
(52, 2, 'Sofia ', 'Gärde', NULL, 'sofia.m.garde@gmail.com', 'NWFyY1hWdTFXQnVPQUJ6eW9wdTJhdz09', 'S', '2021-05-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userserie`
--

CREATE TABLE `userserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userserie`
--

INSERT INTO `userserie` (`userSerieId`, `userId`, `serieId`) VALUES
(1, 3, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocategories`
--

CREATE TABLE `videocategories` (
  `vcId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videocategories`
--

INSERT INTO `videocategories` (`vcId`, `videoId`, `categoryId`) VALUES
(3, 1, 1),
(5, 3, 1),
(6, 8, 1),
(8, 9, 1),
(9, 10, 1),
(10, 14, 1),
(11, 31, 2),
(12, 32, 2),
(13, 33, 3),
(14, 34, 2),
(15, 35, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `videoId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`videoId`, `clientId`, `coachId`, `serieId`, `video`, `titulo`, `imagen`, `description`, `orden`) VALUES
(31, 3, 6, 18, 'MTA3NDU2NA==', 'Life as a gift or a struggle', 'sanna_glad.jpg', 'Are you the doer or is life really happening without any effort? Try this meditation and see for your self. ', 0),
(20, 3, 1, 14, 'MTA3MTU0NQ==', '1: Understanding the change you\'re going through', 'skärmavbild-2021-02-02-kl-163034.png', 'Introduction to The Loop as a core pattern of how change plays out in life, how to master the change process and manifest the change you need.', 1),
(15, 4, 18, 16, 'MTA2ODg2Mg==', 'Intro to Hero\'s Journey', 'heros-journey-thumbnail.jpg', 'Hero\'s journey is a workshop that takes children on a journey to understanding fear and building courage. It utilizes basic language and comprehension skills and builds on lessons learned at school with fun exercises.', 0),
(18, 4, 18, 17, 'MTA3MDk3Nw==', 'Meet Valeria', 'valeria_profile_square2.jpg', 'Meet Valeria, the co-founder of Ephata and get to know her story that led to building schools and complementary educational curriculums for children.', 0),
(16, 4, 18, 17, 'MTA2ODg5OA==', '7 Pillars of Ephata', 'ephata-7-pillarspng.png', 'This short video explains the 7 pillars of Ephata and how they come together to guide children towards a path of faith, wisdom and goodness.', 0),
(17, 4, 18, 17, 'MTA2ODkzOA==', 'What is Ephata?', 'tlm121720kg-19.jpg', 'We learn about the story of Ephata and how it can nurture children in many ways beyond home and school.', 0),
(21, 3, 1, 14, 'MTA3MTU0OA==', '2: Find your starting point', 'skärmavbild-2021-02-02-kl-164639.png', 'Explore who you are in this moment of your life, and find the starting point of your current transformation loop.\n\nHave pen and paper ready!', 2),
(22, 3, 1, 14, 'MTA3MTU2MQ==', '3: Find your shadow', 'skärmavbild-2021-02-02-kl-165002.png', 'Explore your shadow by mapping your psychological pain, anger, anxiety and other challenging emotions in your life.', 3),
(23, 3, 1, 14, 'MTA3MTU2NQ==', '4: Embrace your shadow', 'skärmavbild-2021-02-02-kl-165152.png', 'Instructions for shadow breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 4),
(24, 3, 1, 14, 'MTA3MTU3Mw==', '5: Find your light', 'skärmavbild-2021-02-02-kl-165320.png', 'Connect with your inner light and map your pride, passion and curiosity.', 5),
(25, 3, 1, 14, 'MTA3MTU4Mw==', '6: Embrace your light', 'skärmavbild-2021-02-02-kl-165432.png', 'Visualisation meditation to connect with your path of light', 6),
(26, 3, 1, 14, 'MTA3MTU4Nw==', '7: Find your challenge', 'skärmavbild-2021-02-02-kl-165529.png', 'Explore what is holding you back from changing, and map your fears and energy drainers', 7),
(27, 3, 1, 14, 'MTA3MTU5Mg==', '8: Embrace your challenge', 'skärmavbild-2021-02-02-kl-165635.png', 'Instructions for Hero\'s breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 8),
(28, 3, 1, 14, 'MTA3MTU5NA==', '9: Manifest the change you need', 'skärmavbild-2021-02-02-kl-165748.png', 'Step into manifestation and transform your life. Link to your agreement: https://tiny.cc/myagreement', 9),
(29, 3, 1, 14, 'MTA3MTU5NQ==', '10: Integration and follow up', 'skärmavbild-2021-02-02-kl-165846.png', 'Closing integration session, also offered as an optional live experience', 10),
(30, 3, 1, 14, 'MTA3MTY1NA==', 'BONUS: 8D sound healing', 'skärmavbild-2021-02-02-kl-170013.png', 'As a little bonus to celebrate your completion of The Loop, we\'re giving you a sound healing session in 8D. Bring your headphones!', 11);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  ADD PRIMARY KEY (`accessUserId`);

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answerId`);

--
-- Indices de la tabla `appearance`
--
ALTER TABLE `appearance`
  ADD PRIMARY KEY (`appearanceId`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`);

--
-- Indices de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`callId`);

--
-- Indices de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  ADD PRIMARY KEY (`clientPageId`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientId`);

--
-- Indices de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  ADD PRIMARY KEY (`coachCallId`);

--
-- Indices de la tabla `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`coachId`);

--
-- Indices de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  ADD PRIMARY KEY (`coachClientId`);

--
-- Indices de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`colorId`);

--
-- Indices de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  ADD PRIMARY KEY (`colorClientId`);

--
-- Indices de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  ADD PRIMARY KEY (`iddiccionario`);

--
-- Indices de la tabla `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`emailId`);

--
-- Indices de la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`imageId`);

--
-- Indices de la tabla `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`generalId`);

--
-- Indices de la tabla `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`homeId`);

--
-- Indices de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  ADD PRIMARY KEY (`homePlatformId`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`levelId`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`);

--
-- Indices de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  ADD PRIMARY KEY (`passId`);

--
-- Indices de la tabla `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`platformId`);

--
-- Indices de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  ADD PRIMARY KEY (`psId`);

--
-- Indices de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`postId`);

--
-- Indices de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`questionId`);

--
-- Indices de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `shala`
--
ALTER TABLE `shala`
  ADD PRIMARY KEY (`shalaId`);

--
-- Indices de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  ADD PRIMARY KEY (`slideId`);

--
-- Indices de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  ADD PRIMARY KEY (`statusId`);

--
-- Indices de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `typemail`
--
ALTER TABLE `typemail`
  ADD PRIMARY KEY (`typeId`);

--
-- Indices de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `usercall`
--
ALTER TABLE `usercall`
  ADD PRIMARY KEY (`userCallId`);

--
-- Indices de la tabla `userclient`
--
ALTER TABLE `userclient`
  ADD PRIMARY KEY (`userClientId`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indices de la tabla `userserie`
--
ALTER TABLE `userserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  ADD PRIMARY KEY (`vcId`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`videoId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access`
--
ALTER TABLE `access`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  MODIFY `accessUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `answerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT de la tabla `appearance`
--
ALTER TABLE `appearance`
  MODIFY `appearanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `calls`
--
ALTER TABLE `calls`
  MODIFY `callId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;', AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  MODIFY `clientPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  MODIFY `coachCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `coaches`
--
ALTER TABLE `coaches`
  MODIFY `coachId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  MODIFY `coachClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `colorId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  MODIFY `colorClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  MODIFY `iddiccionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `email`
--
ALTER TABLE `email`
  MODIFY `emailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gallery`
--
ALTER TABLE `gallery`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `generals`
--
ALTER TABLE `generals`
  MODIFY `generalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `home`
--
ALTER TABLE `home`
  MODIFY `homeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  MODIFY `homePlatformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `levelId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  MODIFY `passId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `platforms`
--
ALTER TABLE `platforms`
  MODIFY `platformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  MODIFY `psId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `postId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `questionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `series`
--
ALTER TABLE `series`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;', AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `shala`
--
ALTER TABLE `shala`
  MODIFY `shalaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  MODIFY `slideId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT COMMENT ' multiple=accessadmin,accessuseradmin', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `typemail`
--
ALTER TABLE `typemail`
  MODIFY `typeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usercall`
--
ALTER TABLE `usercall`
  MODIFY `userCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `userclient`
--
ALTER TABLE `userclient`
  MODIFY `userClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `userserie`
--
ALTER TABLE `userserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  MODIFY `vcId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `videoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
