-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 22-02-2021 a las 11:00:55
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shala`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access`
--

DROP TABLE IF EXISTS `access`;
CREATE TABLE IF NOT EXISTS `access` (
  `accessId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`accessId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `access`
--

INSERT INTO `access` (`accessId`, `name`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered users');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessadmin`
--

DROP TABLE IF EXISTS `accessadmin`;
CREATE TABLE IF NOT EXISTS `accessadmin` (
  `accessId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`accessId`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessadmin`
--

INSERT INTO `accessadmin` (`accessId`, `nombre`) VALUES
(7, 'generalSetup'),
(8, 'platformsManage'),
(9, 'webContent'),
(10, 'userManage'),
(11, 'platformUsers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessuseradmin`
--

DROP TABLE IF EXISTS `accessuseradmin`;
CREATE TABLE IF NOT EXISTS `accessuseradmin` (
  `accessUserId` int(11) NOT NULL AUTO_INCREMENT,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `accessId` int(11) NOT NULL COMMENT 'opciones=accessadmin',
  PRIMARY KEY (`accessUserId`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessuseradmin`
--

INSERT INTO `accessuseradmin` (`accessUserId`, `tipoId`, `accessId`) VALUES
(11, 2, 7),
(12, 2, 8),
(13, 2, 9),
(14, 2, 11),
(15, 2, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `usuario` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  PRIMARY KEY (`adminId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`adminId`, `tipoId`, `nombre`, `usuario`, `pass`) VALUES
(1, 2, 'Administrator', 'admin', 'MGRzOGtMVXcxRDJHR3piQ0NyMVdJVEtoWGE5RWVrL3p6blFMdEtyQTErST0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `answerId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `questionId` int(11) NOT NULL COMMENT 'opciones=questionnaire',
  `answer` varchar(200) NOT NULL,
  PRIMARY KEY (`answerId`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`answerId`, `clientId`, `userId`, `questionId`, `answer`) VALUES
(36, 3, 3, 5, 'Three'),
(35, 3, 3, 4, 'Two'),
(34, 3, 3, 3, 'One'),
(33, 3, 3, 2, 'Two'),
(12, 3, 4, 1, 'One'),
(13, 3, 4, 2, 'Two'),
(14, 3, 4, 3, 'Three'),
(15, 3, 4, 4, 'Four'),
(16, 3, 4, 5, 'Five'),
(32, 3, 3, 1, 'Four'),
(22, 3, 5, 1, 'One'),
(23, 3, 5, 2, 'Two'),
(24, 3, 5, 3, 'Three'),
(25, 3, 5, 4, 'Four'),
(26, 3, 5, 5, 'Five'),
(27, 3, 6, 1, 'One'),
(28, 3, 6, 2, 'Two'),
(29, 3, 6, 3, 'Three'),
(30, 3, 6, 4, 'Four'),
(31, 3, 6, 5, 'Five'),
(37, 3, 7, 1, 'One'),
(38, 3, 7, 2, 'Two'),
(39, 3, 7, 3, 'Two'),
(40, 3, 7, 4, 'Four'),
(41, 3, 7, 5, 'Two'),
(42, 3, 8, 1, 'One'),
(43, 3, 8, 2, 'Two'),
(44, 3, 8, 3, 'Three'),
(45, 3, 8, 4, 'Four'),
(46, 3, 8, 5, 'Two'),
(47, 3, 9, 1, 'One'),
(48, 3, 9, 2, 'Three'),
(49, 3, 9, 3, 'Two'),
(50, 3, 9, 4, 'Two'),
(51, 3, 9, 5, 'One'),
(52, 3, 10, 1, 'One'),
(53, 3, 10, 2, 'Two'),
(54, 3, 10, 3, 'Three'),
(55, 3, 10, 4, 'Four'),
(56, 3, 10, 5, 'Five'),
(57, 3, 11, 1, 'Two'),
(58, 3, 11, 2, 'Four'),
(59, 3, 11, 3, 'Three'),
(60, 3, 11, 4, 'Two'),
(61, 3, 11, 5, 'Three'),
(62, 3, 12, 1, 'Four'),
(63, 3, 12, 2, 'Two'),
(64, 3, 12, 3, 'Three'),
(65, 3, 12, 4, 'Three'),
(66, 3, 12, 5, 'Three'),
(67, 3, 13, 1, 'One'),
(68, 3, 13, 2, 'Two'),
(69, 3, 13, 3, 'Three'),
(70, 3, 13, 4, 'Four'),
(71, 3, 13, 5, 'Five'),
(72, 3, 15, 1, 'Two'),
(73, 3, 15, 2, 'One'),
(74, 3, 15, 3, 'Three'),
(75, 3, 15, 4, 'Two'),
(76, 3, 15, 5, 'Four'),
(77, 3, 16, 1, 'Three'),
(78, 3, 16, 2, 'Two'),
(79, 3, 16, 3, 'Two'),
(80, 3, 16, 4, 'Two'),
(81, 3, 16, 5, 'Three'),
(82, 4, 17, 6, 'One'),
(83, 4, 18, 6, 'Two'),
(84, 3, 19, 1, 'Four'),
(85, 3, 19, 2, 'Four'),
(86, 3, 19, 3, 'Four'),
(87, 3, 19, 4, 'Five'),
(88, 3, 19, 5, 'One');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls`
--

DROP TABLE IF EXISTS `calls`;
CREATE TABLE IF NOT EXISTS `calls` (
  `callId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercall;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `image` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(2000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL COMMENT 'liga',
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipollamada',
  `fecha` datetime NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '60',
  PRIMARY KEY (`callId`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls`
--

INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `image`, `title`, `description`, `url`, `tipoId`, `fecha`, `duration`) VALUES
(2, 3, 1, 1, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/92589029676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXdGOUh2VXRRbFpkTklxNm9TZ0xYSzFOSkdkWWFNSWJVdXZoaElGRERiZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhYThpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE3OTMsImlhdCI6MTYwOTM0NDU5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DJJ4yaMyKdFUbHeKB-e-eENfqwIzbdGMNrnoYFBKqB4', 2, '2020-12-30 13:14:20', 60),
(4, 3, 1, 1, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/95415883734?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiU3R2U3FZRFRGN0h5a2w3YmJqU09RNFFsWDdLaXlnX0hhWmxCU05wMzVBQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhdXBSQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE4NzQsImlhdCI6MTYwOTM0NDY3NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cYvkrWMZfJ8CPJd5VbtBNsIESW2SDtpMC5k0kYDFXUg', 2, '2020-12-30 11:11:10', 60),
(5, 3, 1, 1, NULL, 'Call Test Set', NULL, 'https://zoom.us/s/97839866400?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiekZqeV85X0JoclpzWldDRmV0b0tJRTBpYjQwZV83akJFZ1htMV9QZE1PTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBiWXdpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTIwNDYsImlhdCI6MTYwOTM0NDg0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.568iWC_NSRtrEk35lAKeKCGKDq8k0cTDA1-QG-Q7gb4', 1, '2020-12-30 11:14:02', 60),
(6, 3, 1, 1, NULL, 'Yoga class', NULL, 'https://zoom.us/s/93979434891?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidmVzRmM0UlctVGxnN3d3WUJrMUluZU4tcmdaY3NHR2F2Q0Y3X3ZEVWNYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwRGZ2QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU2MjksImlhdCI6MTYwOTM0ODQyOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.kls1b_sCbUZYBv6KyLw7rBGo2rS_yEy2R4gMK0S-NSs', 2, '2020-12-30 12:14:00', 60),
(7, 3, 1, 1, NULL, 'Test Global', NULL, 'https://zoom.us/s/93734417213?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNU8zMTNuc0paV0J4OWdpNVlDNEQwNGpqLWtmU1Bub2prbWN4SEY3VnhnZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwWEZ3QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU3MTAsImlhdCI6MTYwOTM0ODUxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RftmAUITz5rnehnjCRPM-7Ytl4bBPd4tF4PD-zBuLVE', 1, '2020-12-31 12:14:54', 60),
(8, 3, 1, 1, NULL, 'Test Global test', NULL, 'https://zoom.us/s/95152785050?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNFpLZV83NzcyR3pmMlo4anc4MVMwSk1TbGFqc0pGaUJhZUl0S3R3SDlTay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBzN19YQUJKMUFBQUEiLCJleHAiOjE2MDkzNTY2NDcsImlhdCI6MTYwOTM0OTQ0NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.QtFFjV6iADF3HXvQa-gae3Tq0TjDGMKhjuydjwxnb0I', 1, '2020-12-31 00:00:00', 60),
(9, 3, 1, 1, NULL, 'Private Yoga Class', NULL, 'https://zoom.us/s/93360745123?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiY203RmVCQ3NOaUlsSkcyTDJJRnRsbWJCZHh0dWtiLWlSb2pyMm1fblZacy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA3MF85QUJKMUFBQUEiLCJleHAiOjE2MDkzNjA1NTEsImlhdCI6MTYwOTM1MzM1MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ufxODqV6_RdyIGJsY5jKA1ZnUpbyXqO8IxbXgjKNIY0', 2, '2020-12-30 13:35:00', 60),
(10, 3, 1, 1, NULL, 'Private Class for Tomorrow', NULL, 'https://zoom.us/s/96485903856?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibDBWWTFCZ0pRbDBSeHpfbF95X0JuZk1xVXFKVURmcFMwbzBXYUF3QWJKdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA4T3lBQUJKMUFBQUEiLCJleHAiOjE2MDkzNjA2NTYsImlhdCI6MTYwOTM1MzQ1NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.z4jNyIbpTLfzprVVKitK1hho8qDacy58IAW09luV5Is', 2, '2020-12-31 11:30:00', 60),
(12, 3, 1, 1, NULL, 'Yoga Beginners', NULL, 'https://zoom.us/s/93845368332?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVllGMGQ4eVFRcEgyLU93eEh2eDJDVTBwblhiZEw2Si0wbGV5cklBZDhWMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTAtX3FYQUJKMUFBQUEiLCJleHAiOjE2MDkzNjEzODEsImlhdCI6MTYwOTM1NDE4MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.os9YSdyOiUyXEoBJdvPc9AdwpaSVG0_gx9M7wKs0xKo', 1, '2020-12-30 13:55:00', 60),
(13, 3, 3, 1, NULL, 'Workshop', NULL, 'https://zoom.us/s/97563617492?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoia05rSlJ0Q0sxdi1aeDFfUVZESi1oeXRZZ1VNT3h1NVlmTGdZV0xVbDJUUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpWai1WQUJKMUFBQUEiLCJleHAiOjE2MTAxMzkwNDksImlhdCI6MTYxMDEzMTg0OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ532tN12H1MdALQ1uXbewIsy4j6brhBMqBPP-Umyi0', 1, '2021-01-08 14:00:00', 60),
(14, 3, 3, 1, NULL, 'Intro', NULL, 'https://zoom.us/s/93574307471?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoieDVVVDR4a2RuTG1IdXZCWkIwOFl4bXVXSjE4TkRoY0JNeU9kblQ4NE5KMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpYZkIzQUJKMUFBQUEiLCJleHAiOjE2MTAxMzk1NTMsImlhdCI6MTYxMDEzMjM1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.dHC_tOib4tV0EcxRlY66jcLQF_0WAn_y49MQZ1Petj0', 2, '2021-01-08 14:00:00', 60),
(15, 3, 1, 0, NULL, 'Testing call', NULL, 'https://zoom.us/s/92511603346?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibHdzZlAzMjh5NDBKemRITUUzaWFkWVN0WlJlYzhkUEE4MnctcHlRbVdSYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNRdGFMQUJKMUFBQUEiLCJleHAiOjE2MTA0NzMzMjEsImlhdCI6MTYxMDQ2NjEyMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JHA5cmEiCQF8nVFnk9f04nsUS5brj0u_mx4ZXcf-iKY', 2, '2021-01-15 11:00:00', 60),
(44, 3, 9, 4, NULL, 'Sound Healing ', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', 'https://zoom.us/s/99428845676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWHVRalVCLW9tMFBWdWtkRDAwMEdQdlVoVFh3UnJleU82T0JBcGVnZEVBby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZG45aTJwQUJKMUFBQUEiLCJleHAiOjE2MTIzNjQxMjIsImlhdCI6MTYxMjM1NjkyMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ9Tqr4sVWM1KG825lRAPnj8UONvXoA_VdJXeK2TVEQ', 1, '2021-02-04 08:00:00', 45),
(17, 3, 1, 1, NULL, 'Public Call For Now', 'Testing call', 'https://zoom.us/s/97018231393?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYUdEZDctcnljOE1uZGxPVThRT1NwbUJtMEFwelBTZU96cG1vaFdRX2xQUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNteHFqQUJKMUFBQUEiLCJleHAiOjE2MTA0NzkxMDUsImlhdCI6MTYxMDQ3MTkwNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.47s_8lJyH8LOPfJcnsw8ldKiadMXzRbSIxILNftX5kI', 1, '2021-01-12 12:15:00', 15),
(18, 3, 15, 1, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/91585109126?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0lMWVRQU2hMZlhlQ1Y4bnB0OVhjUG5rdFRDc2Vtb2pyd2JXbHVOSTBvNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY1ZfVnhoQUJKMUFBQUEiLCJleHAiOjE2MTA5ODg4NjEsImlhdCI6MTYxMDk4MTY2MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.eRfEN9p6VbtOaErGGtHzlCwNcV-nI0KJMdIvvT6R_jQ', 1, '2021-01-18 10:00:00', 30),
(19, 3, 3, 1, NULL, 'Intro class', 'Desc', 'https://zoom.us/s/99792834871?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicmxIM2lOYXdyeDU0SVRINkJTazhVdGFYYXVFRGlra002LVQ2Zk1aWGdFay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JHd3FJQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ2OTMsImlhdCI6MTYxMTA2NzQ5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.O5FoUKEjo9qXIoFIN8lRdqzz26HyZmQc8KmLoUnpOEc', 2, '2021-01-20 12:30:00', 30),
(20, 3, 3, 0, NULL, 'Consultation', NULL, 'https://zoom.us/s/96228600796?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZmcyd3lwZDFHZWdia2VuSWRJUU5mN1p5Q1ZVOFVRcVVCWHREeFB0RGVBUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JITjJEQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ4MTIsImlhdCI6MTYxMTA2NzYxMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ILj3fwYSfXzyLqXDtQzjYudTJCTiOsQ98IZE0ACVYfs', 2, '2021-01-20 18:00:00', 60),
(21, 3, 3, 1, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/93007611295?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXV2c2REMk5pS1Q3d1VtNlR5OWxwSllaMlFmaXFUbVphaFlMS1NHMk02OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JIa3RVQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ5MDYsImlhdCI6MTYxMTA2NzcwNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.LNGXXRB3OKMHPaK4I466gKp0uTzb4bE9x2uQkM3vHEY', 1, '2021-01-19 10:00:00', 30),
(23, 3, 3, 2, NULL, 'Public Session', 'Test', 'https://zoom.us/s/94332087831?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicUVCV09yNGRYcWNaQmdnOFF1ai12dUpRcHBsZzBNR19kNWRJcWtsTXgxYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY24wYmVoQUJKMUFBQUEiLCJleHAiOjE2MTEyODc5OTEsImlhdCI6MTYxMTI4MDc5MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DoeF-sRkk2ss-Kj4VVr-eYrmwIAv0z9tKOH0122ynNo', 1, '2021-01-21 19:00:00', 60),
(30, 3, 3, 1, 'roman-videopng.png', 'Breathwork for Innovation ', 'Test', 'https://zoom.us/s/99732096017?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWW9wRlBzYzNucnBLQlVUeU11V21IZzJiWEtidXFSWE81TFZIeVJyUmtYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJYZU8zQUJKMUFBQUEiLCJleHAiOjE2MTE1MzIwNTgsImlhdCI6MTYxMTUyNDg1OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PSGjJ5rhfBE-rHLjacizH7OuV3S_dk2f3t5LS9w8OzI', 1, '2021-01-24 17:00:00', 30),
(26, 3, 3, 1, 'roman-videopng.png', 'Breathwork for Memory and Focus', 'Join this 20 minute session to transcend your state of mind and achieve high levels of memory and focus', 'https://zoom.us/s/97025013577?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZVZuNVBMV0lkamJyakZIejFaSmhNQ2VTVmMyV1REd3hhN2ROa2hpb29OVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJFdHlBQUJKMUFBQUEiLCJleHAiOjE2MTE1MjcxNDEsImlhdCI6MTYxMTUxOTk0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gFeRyHl4YGbz6Hb1ozDJCnK5ek0Mhgeax_MEhKmcLeY', 1, '2021-01-24 15:30:00', 30),
(35, 3, 3, 4, 'iw_demopng.png', 'Innerworks Platform Demo', 'Exclusive presentation for the coach team to demonstrate features in the first version of the Innerworks platfrom.', 'https://zoom.us/s/93689176598?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoid1E2RzJUSVprZ1RHMDJ5S0U4RFREVDVHQ2RITmRUZmlJS0FsamtyaGpSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzV0bXNsQUJKMUFBQUEiLCJleHAiOjE2MTE1ODgxOTIsImlhdCI6MTYxMTU4MDk5MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ZDoClv4qr5KvX0BbZYE1FH1v28WiNy-NvcnUFhhqh3I', 1, '2021-01-30 06:30:00', 60),
(36, 3, 2, 0, NULL, 'Consultation', NULL, NULL, 2, '2021-01-26 13:30:00', 60),
(37, 3, 3, 1, NULL, 'User Onboarding', 'Onboarding session for coaches and students to join the Innerworks program', 'https://zoom.us/s/92731884092?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWmUyOEZnMDhMM1h3ME9UREl2NGJTS0dyd293UzdKX2ZkdzlMSHVKNlNEdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzU1OFhLQUJKMUFBQUEiLCJleHAiOjE2MTE1OTE0MjYsImlhdCI6MTYxMTU4NDIyNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.8VsbQ9B8pCwq-pspewAzemABM4PmqXyvpo_Gw7bAFlM', 1, '2021-01-31 18:00:00', 60),
(38, 4, 18, 5, 'highly_engaging_content.jpg', 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/97092458414?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSjAzdWJCS2xiTHJ4V0lIRWVVVmRvZnpUdGd0UW1rbGN5eTJRZ093aEhoZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtYX1h5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3MzgsImlhdCI6MTYxMTg2MDUzOCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JG3ngE2Klb6yIcbs8_IxTDyEyQOxMOQ-8HpiE0_zEKA', 1, '2021-01-28 12:00:00', 30),
(39, 4, 18, 5, 'highly_engaging_content.jpg', 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/96476527234?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiLV9EYXZNRlVLVHoxS01nZ1R4cnlRVG9Cc3JLZDV1S0RyQmMteXRNS0RaZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtZQUU5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3NDEsImlhdCI6MTYxMTg2MDU0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.94iOjlG0MAyYcWtwH08GC0YdOPepOswWGCCHosHe5ZU', 1, '2021-01-28 12:00:00', 30),
(40, 4, 19, 5, NULL, 'First Session', 'This is a test for sessions', 'https://zoom.us/s/94600546115?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiam5uWkMxaVFKY2pxdnVNeHF0NENKNjUyNmZkYk5XOVRxM3VXc2U1a2RIcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtqUEtMQUJKMUFBQUEiLCJleHAiOjE2MTE4NzA2ODcsImlhdCI6MTYxMTg2MzQ4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.I1rwWT1Zg9Qk0cGkhWNFPZGZ-zJ6bLdQCMTWlTInrAI', 1, '2021-01-29 02:00:00', 60),
(41, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/99729884957?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDVoZHFGSjFPNGdMMlFTVm93MERqdXVVbEp5MDA4Nm9YS210WTVnVEVFMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMEw3QUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Nvzqu35EA6WRjqnRIGFM4Ffl7jwr7kv0Tnj0hRj8_Nc', 1, '2021-02-16 02:00:00', 60),
(42, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/95030990013?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0FjRXFkc0hjR3NHWENnUmRnYnRfYUJtekIyOW5GS1Z6bExvUXpZckV4by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMFBWQUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.zWmvF41PErMh0aqgz9YFPyXdnziSVd_8xXrxpGNSRAw', 1, '2021-02-16 02:00:00', 60),
(43, 3, 1, 1, 'skärmavbild-2021-02-02-kl-165152.png', 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/92409182049?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiS1ZiLUVCQy1QeVk2Q3h5R0dMRGZrUDNpSUYwYW1xd0s0THN3ODZkY2tTZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMnAzQUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNjUsImlhdCI6MTYxMjI4Mzg2NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.pbVg8DWkAoVlJTAICXDt2GbTd5-b310phl4zDxuIBrY', 1, '2021-02-16 02:00:00', 60),
(45, 3, 3, 2, 'img_3153jpg.jpg', 'Demo Schedule', 'Bla bla bla', 'https://zoom.us/s/98430952016?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQlRGbTM5WmdoeTZVR2lkVFNfeXRIQXZXbkt5eDlpNV9aRGNaNUJFZklXby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4SmRrQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3NDMsImlhdCI6MTYxMjg5MDU0MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.4R08l4-g8dQCsjqsc4QDfoQi63iulelNcxiAWhbpa0U', 1, '2021-02-09 00:30:00', 30),
(46, 3, 3, 1, 'img_3154jpg.jpg', 'Demo test', 'bla bla', 'https://zoom.us/s/98236149012?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOHNIZFZXSkV4amFTRVpiTFZ5NWlOZ3AxVEZDajJ6SWY3czMtTFllcW5Ldy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4VzFoQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3OTgsImlhdCI6MTYxMjg5MDU5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.HAzgvFAeHKDcxmjJdpKHicWEqq3ZqtbXL0RYkl4O7sA', 1, '2021-02-09 12:30:00', 30),
(47, 3, 3, 0, NULL, 'Yoga Intro 1', NULL, NULL, 2, '2021-02-10 06:00:00', 60),
(48, 3, 3, 1, 'dy001484.jpg', 'Feb 16 Session', 'Testing session', 'https://zoom.us/s/93142513286?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT0ItQzc0cU14bDdsMkhmS0o2aGx5OWtKU0JNR3VlR0FCOE92LWFOZDhGay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZXIySmJOQUJKMUFBQUEiLCJleHAiOjE2MTM1MDMwMzQsImlhdCI6MTYxMzQ5NTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gLGFEYvoiOyKXeUYJCVVulGabFDFjkfwnrxPqxKv-ew', 1, '2021-02-16 12:30:00', 30),
(49, 3, 3, 2, 'img_3153jpg.jpg', 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96232917912?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVB4S0lUM3hrdWhvYThSVkZ5Z05oMklLclFOd3ZHaG9vakJwUHZoSnc3SS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDcFY2QUJKMUFBQUEiLCJleHAiOjE2MTM4NDE4NTUsImlhdCI6MTYxMzgzNDY1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.xQhfPLbBk6OGkmS1mYN8SmUXOVeJX8k4ceqOPXyIbTs', 2, '2021-02-24 02:00:00', 30),
(50, 3, 3, 2, 'img_3154jpg.jpg', 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96365592232?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNE9sMUZTWnBicWZBWWl4VDM1VUl1SkZ6S0p0VHptZVZZaEowWVdFLTU5NC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDLXZWQUJKMUFBQUEiLCJleHAiOjE2MTM4NDE5NDIsImlhdCI6MTYxMzgzNDc0MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cvc7_GEYd53DEePa7SvyqeBlaiw7aZNcfkK8JdKiiqU', 1, '2021-02-23 14:30:00', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `descripcion` varchar(1000) NOT NULL,
  `imagen` varchar(200) NOT NULL COMMENT 'archivo',
  PRIMARY KEY (`categoryId`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`categoryId`, `clientId`, `nombre`, `descripcion`, `imagen`) VALUES
(1, 3, 'Breathwork', 'Learn breathwork exercises that calm your mind, balance your nervous system and move you into a state of creativity and productivity', 'category-breathwork.jpg'),
(2, 3, 'Meditation', 'Practice meditation exercises that cultivate a state of equanimity and openness, bringing you in alignment with others and closer to your purpose', 'category-meditation.jpg'),
(3, 3, 'Psychotherapy', 'Consult with us and connect to your true authentic self through personal healing, focused and results oriented psychotherapy. ', 'category-psychotherapy.jpg'),
(4, 3, 'Connection', 'Discover the tools that help you relate, empathize and build trust in relationships so that share and co-create boldly and freely', 'category-connection.jpg'),
(5, 4, 'Purpose Approach', 'Children discover their individuality and explore what they want for themselves and the world through this series of classes and workshops.', 'challenge-mission-and-achievement-a-little-girl-is-v9dvn29.jpg'),
(6, 4, 'Passion based learning', 'We work with children to identify their interests and curate personalized exercises that lead them to seek learning out of passion. ', 'reading-is-her-big-passion-smp242b.jpg'),
(7, 4, 'Self-Efficacy', 'Self-efficacy classes develop the self-management skills that enable children to become resourceful, persistent and confident.', 'father-with-small-children-working-outdoors-in-gar-kkd5msl.jpg'),
(8, 4, 'Academics', 'Our academics classes explores art and culture and connects them with science, language, math and social studies taught at school.', 'lovely-little-girl-is-dancing-in-headphones-tqsuaje.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `zoomPublic` varchar(200) DEFAULT NULL,
  `zoomSecret` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`clientId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`clientId`, `platformId`, `nombre`, `logotipo`, `icono`, `url`, `email`, `pass`, `zoomPublic`, `zoomSecret`) VALUES
(1, 1, 'Shopify', 'logo-inner.png', 'logo-inner.png', 'client', 'client@test.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(2, 2, 'EphataClient', 'ephata.png', 'ephata.png', 'clients', 'client@ephata.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(3, 1, 'Innerworks University', 'logo-inner.png', 'logo-inner.png', 'innerworks', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(4, 2, 'Ephata University', 'ephata.png', 'ephata.png', 'ephata', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coaches`
--

DROP TABLE IF EXISTS `coaches`;
CREATE TABLE IF NOT EXISTS `coaches` (
  `coachId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `biografia` varchar(1000) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `header` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `facebook` varchar(200) DEFAULT NULL COMMENT 'liga',
  `twitter` varchar(200) DEFAULT NULL COMMENT 'liga',
  `web` varchar(200) DEFAULT NULL COMMENT 'liga',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `fecha` date NOT NULL COMMENT 'hoy',
  PRIMARY KEY (`coachId`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coaches`
--

INSERT INTO `coaches` (`coachId`, `nombre`, `apellido`, `biografia`, `foto`, `header`, `video`, `facebook`, `twitter`, `web`, `email`, `pass`, `fecha`) VALUES
(1, 'Nils', 'von Heijne', 'Nils is a holistic coach, advisor and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker and sound healer.\n\nMore info at nilsvonheijne.com', 'nilsvonheijne9.jpeg', NULL, 'MTA3MTY3NA==', NULL, NULL, NULL, 'nils@innerworks.io', 'OUFYRzJQT081NDBPQzBtYWtEOUlpUT09', '2021-02-02'),
(2, 'Roshi', 'Derakshan', 'Licensed Psychotherapist and dedicated to helping people connect to their true authentic self through personal healing, focused and results oriented psychotherapy. Licenced Life coach and work as a ”hybrid” where you get the best of a therapist and coach to break free from what’s holding you back to push towards where you want to be.', 'roshi-2png.png', NULL, NULL, NULL, NULL, NULL, 'roshi@innerworks.io', 'YTZ2cHZleWVOM1FFWXF6RzJ6OHNOajBtTUpxWDhHSUxDNGlkVVRrZExhbz0=', '2021-01-24'),
(5, 'Jessika', 'Klingspor', 'Jessika is Swedish entrepreneur and business owner living in Barcelona since 2001. She is currently collaborating with the Barcelona City Council building new alliances between The Nordic\nCountries and Barcelona within the sector of innovation. She is co-creator within the Innerworks Community with aim to bridge the gap between the corporate world and the spiritual world. Planning to launch Innerworks Barcelona and World of Wisdom Barcelona 2021.', 'jessklingspor1.jpg', NULL, NULL, NULL, NULL, NULL, 'jessika@innerworks.io', 'd3RFV2VGd2FjNWgzbHdyT09JMTZNdz09', '2021-01-18'),
(3, 'Roman', 'Fernandez', 'Roman is a leadership coach, certified breathwork instructor and crossfit trainer with a background in management consulting, innovation and change management.', 'romanf-2png.png', 'teaser-uhmay.jpg', 'MTA1OTIwOQ==', NULL, NULL, NULL, 'roman@alumni.ie.edu', 'L3F2aDRXVks3aXhDNzl3d0NXRkkzQT09', '2021-02-09'),
(4, 'Nils', 'von Heijne', 'Nils is a holistic coach, advisor and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker and sound healer.', 'nilspng.png', NULL, NULL, NULL, NULL, NULL, 'nils@wishful.se', 'UnZQaGdVYUdqdVBDTlYxS0JhUVhiUT09', '2021-01-17'),
(6, 'Mattis', 'Norrvidd', 'Mattis is a heartdriven environmental engineer who supports individuals, groups and companies to thrive through courses in self leadership, inspirational talks, meditations and coaching. He uses curiosity and clearity of mind to develop inner and outer connection.', 'mattis.png', NULL, NULL, NULL, NULL, NULL, 'mattis@innerworks.io', 'N0F6aHpjK3N2cUVTWCs2d3U5ZkhnZz09', '2021-02-05'),
(7, 'Therese', 'Lyander', 'Therese is a transformational life coach, space holder and emotional guide. In her toolbox you will find\ndetox, fasting, breathwork, trauma therapy, meditation, creative feminine leadership and inspiration', 'teresa.jpg', NULL, NULL, NULL, NULL, NULL, 'therese@innerworks.io', 'elV5VzRzYnYreGVmQXo0OGJROFlIdz09', '2021-01-18'),
(8, 'Johan', 'Reunanen', 'Johan is a coach, business advisor and changemaker. Besides being a partner at Scandinavian managament consultancy, Cordial, focusing on sustainable transformation, Johan shares his message of sobriety and loving leadership through talks, podcasts and writing', 'johan-r-1.jpeg', NULL, NULL, NULL, NULL, NULL, 'johan@innerworks.io', 'WGJ6aiswWW5IMmhSR1o0Rzh2aTFCUT09', '2021-01-18'),
(9, 'Faviana', 'Vangelius', 'Within the InnerWorks community Faviana does Sound Healing and Reiki. Beyond being one of InnerWorks co-founders, she also does Community Building and Explorer in Chief (Experience Manager).', 'faviana.jpg', NULL, NULL, NULL, NULL, NULL, 'faviana@innerworks.io', 'YlEzN0pxRjlVa01WdDRBSFVGT2VQdz09', '2021-02-03'),
(10, 'Daniel', 'Mueller-Gonzalez', 'Daniel is a Wim Hof Method instructor, breath guide and human evolution coach based in Stockholm.', 'daniel.jpg', NULL, NULL, NULL, NULL, NULL, 'daniel@innerworks.io', 'aTBScUo3ZzB5YTczVEhTVGpkNGM1UT09', '2021-02-02'),
(11, 'Alexander', 'Holmberg', 'Alexander is a personal coach in Stockholm.', 'holmberg.jpeg', NULL, NULL, NULL, NULL, NULL, 'alexander@innerworks.io', 'S296aXBoNk9uNVR6NjlKZTVMWm1iQT09', '2021-01-18'),
(12, 'Fredrik', 'Edlund', 'Fredrik Edlund is a yoga and meditation teacher based in Stockholm.', 'edlund1.jpg', NULL, NULL, NULL, NULL, NULL, 'fredrik@innerworks.io', 'Z2sxWmRJVDFjNVNvK3d1QjJkbm4rZz09', '2021-01-18'),
(13, 'Kim', 'Gajraj', 'Kim is a holistic voice therapist based in Stockholm.', 'gajraj.jpg', NULL, NULL, NULL, NULL, NULL, 'kim@innerworks.io', 'RzJySUZEUWFod3M2UCtuc1ZoZkxMQT09', '2021-01-18'),
(18, 'Valeria', 'Payton', 'Valeria Payton is the founder of Tulum International School and the co-founder of Ephata.', 'valeria_profile_square2.jpg', NULL, 'MTA2OTkzMQ==', NULL, NULL, NULL, 'valeria@ephata.me', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', '2021-01-29'),
(20, 'Jan Claude', 'Van Dam', 'Hey its me!!', NULL, 'img_3155jpg.jpg', NULL, NULL, NULL, NULL, 'gabofc+vandam@gmail.com', 'TTE1V1ZUWW9oZUJZdW1CZkVDMVpUdz09', '2021-02-09'),
(21, 'Test ', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gabofc+test@gmail.com', 'b290SkVDeW1zWkI0dDBLLytnRHcrQT09', '2021-02-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachesclients`
--

DROP TABLE IF EXISTS `coachesclients`;
CREATE TABLE IF NOT EXISTS `coachesclients` (
  `coachClientId` int(11) NOT NULL AUTO_INCREMENT,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`coachClientId`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachesclients`
--

INSERT INTO `coachesclients` (`coachClientId`, `coachId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(15, 2, 3, 'N', NULL),
(30, 3, 3, 'N', NULL),
(33, 5, 3, 'N', NULL),
(34, 6, 3, 'N', NULL),
(35, 7, 3, 'N', NULL),
(36, 8, 3, 'N', NULL),
(38, 10, 3, 'N', NULL),
(39, 11, 3, 'N', NULL),
(40, 12, 3, 'N', NULL),
(41, 13, 3, 'N', NULL),
(42, 14, 3, 'N', NULL),
(43, 15, 3, 'N', NULL),
(46, 16, 3, 'N', NULL),
(48, 17, 3, 'N', NULL),
(50, 18, 4, 'N', NULL),
(52, 19, 4, 'N', NULL),
(58, 9, 3, 'N', NULL),
(59, 20, 3, 'R', 'You are a bad teacher'),
(60, 21, 3, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorcatalog`
--

DROP TABLE IF EXISTS `colorcatalog`;
CREATE TABLE IF NOT EXISTS `colorcatalog` (
  `catalogId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`catalogId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorcatalog`
--

INSERT INTO `colorcatalog` (`catalogId`, `nombre`) VALUES
(1, 'Main Color'),
(2, 'Secondary Color'),
(3, 'Text Color'),
(4, 'Text Hover Color');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

DROP TABLE IF EXISTS `colores`;
CREATE TABLE IF NOT EXISTS `colores` (
  `colorId` int(11) NOT NULL AUTO_INCREMENT,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color',
  PRIMARY KEY (`colorId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorsclient`
--

DROP TABLE IF EXISTS `colorsclient`;
CREATE TABLE IF NOT EXISTS `colorsclient` (
  `colorClientId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color',
  PRIMARY KEY (`colorClientId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorsclient`
--

INSERT INTO `colorsclient` (`colorClientId`, `clientId`, `catalogId`, `color`) VALUES
(1, 3, 1, '#e3913a'),
(2, 3, 2, '#815e39'),
(3, 3, 3, '#918a83'),
(4, 3, 4, '#e8ddd2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

DROP TABLE IF EXISTS `diccionario`;
CREATE TABLE IF NOT EXISTS `diccionario` (
  `iddiccionario` int(11) NOT NULL AUTO_INCREMENT,
  `campo` varchar(200) NOT NULL,
  `etiqueta` varchar(200) NOT NULL,
  PRIMARY KEY (`iddiccionario`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diccionario`
--

INSERT INTO `diccionario` (`iddiccionario`, `campo`, `etiqueta`) VALUES
(8, 'tipousuario', 'User\'s Type'),
(9, 'pass', 'Password'),
(14, 'statuspago', 'Payment Status'),
(28, 'usuario', 'User'),
(27, 'tipoadmin', 'Admin Type'),
(23, 'category', 'Categories'),
(26, 'nombre', 'Name'),
(29, 'accessadmin', 'Admin Access'),
(30, 'logotipo', 'Logo'),
(31, 'icono', 'Icon'),
(32, 'biografia', 'Biography'),
(33, 'foto', 'Photo'),
(34, 'telefono', 'Phone'),
(35, 'colorcatalog', 'Color\'s Catalog'),
(36, 'servicescatalog', 'Service\'s Catalog'),
(37, 'colorsclient', 'Clients Colors'),
(38, 'descripcion', 'Description'),
(39, 'imagen', 'Image'),
(40, 'titulo', 'Title'),
(41, 'apellido', 'Last Name'),
(42, 'fecha', 'Date'),
(43, 'users', 'Members');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

DROP TABLE IF EXISTS `levels`;
CREATE TABLE IF NOT EXISTS `levels` (
  `levelId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `posicion` int(11) NOT NULL COMMENT 'posicion',
  PRIMARY KEY (`levelId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`levelId`, `clientId`, `nombre`, `posicion`) VALUES
(1, 3, 'Easy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passcambio`
--

DROP TABLE IF EXISTS `passcambio`;
CREATE TABLE IF NOT EXISTS `passcambio` (
  `passId` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  PRIMARY KEY (`passId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `passcambio`
--

INSERT INTO `passcambio` (`passId`, `tipo`, `id`, `token`) VALUES
(3, 1, 5, 'T1lyVlFod3ladlhzTWpHaGVYK2k4RlN5Vzg2ZVp0ZHRzcytpd05qMmlMND0='),
(4, 1, 6, 'cURZQSsxYU01elA0T2lMVnJPZWJDS0NFUW1vc2ljSWFuNkZpcldhOUIxZz0='),
(5, 1, 3, 'UWhwVzFmUWZFSVljcGRjTFVyemY0WW5NYVZPS2M0VWMvYStqc3Zwa3Q1OD0='),
(6, 1, 3, 'Q1VFejlicFhpdStna1lzeURXT09TQ1NZMEdLaC9JZDFWSXJlSlBISFQvRT0='),
(7, 1, 3, 'ZUR3ditYZFNuQnF6cVFuNTQ3bnQxM09Uc2cveGFuY3FZeWFNb3VnM3ZLbz0='),
(8, 1, 3, 'OUpIWE8vOWlPY3ZiN0ZxNCtEelZDMDRlbmI4YjhmTUgvcEYybGw5Vmk2QT0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platforms`
--

DROP TABLE IF EXISTS `platforms`;
CREATE TABLE IF NOT EXISTS `platforms` (
  `platformId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=servicescatalog,platformservices;',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `clientId` int(11) DEFAULT NULL COMMENT 'opciones=clients',
  PRIMARY KEY (`platformId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platforms`
--

INSERT INTO `platforms` (`platformId`, `nombre`, `logotipo`, `icono`, `fontFile`, `url`, `email`, `clientId`) VALUES
(1, 'Innerworks', 'logo-inner.png', 'logo-inner.png', 'roboto-regular.ttf', 'innerworks', 'elopez@junkyard.mx', 3),
(2, 'Ephata', 'ephata.png', 'ephata.png', 'roboto-regular.ttf', 'ephata', 'elopez@junkyard.mx', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platformservices`
--

DROP TABLE IF EXISTS `platformservices`;
CREATE TABLE IF NOT EXISTS `platformservices` (
  `psId` int(11) NOT NULL AUTO_INCREMENT,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=servicescatalog',
  PRIMARY KEY (`psId`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platformservices`
--

INSERT INTO `platformservices` (`psId`, `platformId`, `catalogId`) VALUES
(45, 1, 5),
(44, 1, 4),
(43, 1, 3),
(42, 1, 2),
(41, 1, 1),
(35, 2, 5),
(34, 2, 4),
(33, 2, 3),
(32, 2, 2),
(31, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
CREATE TABLE IF NOT EXISTS `questionnaire` (
  `questionId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `question` varchar(1000) NOT NULL COMMENT 'nombre',
  `answerOne` varchar(200) NOT NULL,
  `answerTwo` varchar(200) NOT NULL,
  `answerThree` varchar(200) DEFAULT NULL,
  `answerFour` varchar(200) DEFAULT NULL,
  `answerFive` varchar(200) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT 'posicion',
  PRIMARY KEY (`questionId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `questionnaire`
--

INSERT INTO `questionnaire` (`questionId`, `clientId`, `question`, `answerOne`, `answerTwo`, `answerThree`, `answerFour`, `answerFive`, `position`) VALUES
(1, 3, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(2, 3, 'Question Two', 'One', 'Two', 'Three', 'Four', 'Five', 2),
(3, 3, 'Question Three', 'One', 'Two', 'Three', 'Four', 'Five', 3),
(4, 3, 'Question Four', 'One', 'Two', 'Three', 'Four', 'Five', 4),
(5, 3, 'Question Five', 'One', 'Two', 'Three', 'Four', 'Five', 5),
(6, 4, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

DROP TABLE IF EXISTS `series`;
CREATE TABLE IF NOT EXISTS `series` (
  `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userserie;',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  PRIMARY KEY (`serieId`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`serieId`, `coachId`, `accessId`, `nombre`, `description`, `photo`, `video`) VALUES
(1, 2, 3, 'Test', NULL, NULL, NULL),
(5, 2, 3, 'Other', NULL, NULL, NULL),
(6, 1, 3, 'My Serie', NULL, NULL, NULL),
(7, 1, 3, 'Yoga Starters', NULL, NULL, NULL),
(8, 1, 3, 'Starter Video Serie', NULL, NULL, NULL),
(13, 3, 3, 'Breathwork for Innovation', NULL, NULL, NULL),
(14, 1, 3, 'The Loop - Manifest the Change You Need', NULL, NULL, NULL),
(15, 15, 3, 'Audio Visual', NULL, NULL, NULL),
(16, 18, 3, 'Hero\'s Journey', NULL, NULL, NULL),
(17, 18, 3, 'Ephata Summary', NULL, NULL, NULL),
(18, 6, 1, 'Meditations', NULL, NULL, 'MTA3NDU2MA=='),
(19, 3, 1, 'Test Series Public', 'Bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla', '42-16669945.jpg', 'MTA3NTM5Mg=='),
(20, 3, 2, 'Test Serie Private', 'Private serie', 'logo-nye.jpg', 'MTA3NTM5NQ=='),
(21, 3, 3, 'Private Registered U', 'Registered U', 'logo-nye.jpg', 'MTA3NTM5Nw==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicescatalog`
--

DROP TABLE IF EXISTS `servicescatalog`;
CREATE TABLE IF NOT EXISTS `servicescatalog` (
  `catalogId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`catalogId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicescatalog`
--

INSERT INTO `servicescatalog` (`catalogId`, `nombre`) VALUES
(1, 'Schedulling Calls'),
(2, 'Pre-Recorded Videos'),
(3, 'Live Video Conferences'),
(4, 'Interactive Exercises & Tests'),
(5, 'Reporting');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shala`
--

DROP TABLE IF EXISTS `shala`;
CREATE TABLE IF NOT EXISTS `shala` (
  `shalaId` int(11) NOT NULL AUTO_INCREMENT,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `spotlightrApiKey` varchar(200) DEFAULT NULL,
  `stripePublicKey` varchar(200) DEFAULT NULL,
  `stripePrivateKey` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`shalaId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shala`
--

INSERT INTO `shala` (`shalaId`, `logotipo`, `icono`, `spotlightrApiKey`, `stripePublicKey`, `stripePrivateKey`) VALUES
(1, '', '', 'c53bcd228d6d73879a567d77fa35c5368d390ecb', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoadmin`
--

DROP TABLE IF EXISTS `tipoadmin`;
CREATE TABLE IF NOT EXISTS `tipoadmin` (
  `tipoId` int(11) NOT NULL AUTO_INCREMENT COMMENT ' multiple=accessadmin,accessuseradmin',
  `nombre` varchar(100) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`tipoId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoadmin`
--

INSERT INTO `tipoadmin` (`tipoId`, `nombre`) VALUES
(2, 'Global Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipollamada`
--

DROP TABLE IF EXISTS `tipollamada`;
CREATE TABLE IF NOT EXISTS `tipollamada` (
  `tipoId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  PRIMARY KEY (`tipoId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipollamada`
--

INSERT INTO `tipollamada` (`tipoId`, `nombre`) VALUES
(1, 'Public'),
(2, 'Private');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercall`
--

DROP TABLE IF EXISTS `usercall`;
CREATE TABLE IF NOT EXISTS `usercall` (
  `userCallId` int(11) NOT NULL AUTO_INCREMENT,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  PRIMARY KEY (`userCallId`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercall`
--

INSERT INTO `usercall` (`userCallId`, `callId`, `userId`) VALUES
(1, 2, 3),
(2, 3, 3),
(3, 4, 3),
(4, 6, 3),
(5, 7, 3),
(6, 9, 4),
(7, 10, 4),
(8, 11, 4),
(9, 14, 7),
(10, 15, 3),
(11, 16, 3),
(12, 19, 4),
(13, 20, 4),
(14, 22, 11),
(15, 36, 15),
(16, 47, 3),
(17, 49, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userclient`
--

DROP TABLE IF EXISTS `userclient`;
CREATE TABLE IF NOT EXISTS `userclient` (
  `userClientId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  PRIMARY KEY (`userClientId`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userclient`
--

INSERT INTO `userclient` (`userClientId`, `userId`, `clientId`) VALUES
(1, 1, 3),
(2, 1, 3),
(9, 3, 3),
(8, 2, 3),
(10, 4, 3),
(11, 5, 3),
(12, 6, 3),
(13, 3, 3),
(14, 3, 3),
(15, 7, 3),
(16, 8, 3),
(17, 4, 3),
(18, 9, 3),
(19, 10, 3),
(20, 11, 3),
(21, 12, 3),
(22, 12, 3),
(23, 13, 3),
(24, 13, 3),
(25, 14, 3),
(26, 15, 3),
(27, 16, 3),
(28, 17, 4),
(29, 18, 4),
(30, 19, 3),
(31, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `fecha` date NOT NULL COMMENT 'hoy',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`userId`, `nombre`, `apellido`, `foto`, `email`, `pass`, `fecha`) VALUES
(3, 'Eduardo ', 'Lopez', 'teaser-uhmay.jpg', 'elopez@junkyard.mx', 'L3F2aDRXVks3aXhDNzl3d0NXRkkzQT09', '2021-01-05'),
(4, 'Gabo Student', 'Student', NULL, 'gabofc@gmail.com', 'RVVVaWFmVGwyN2liYW5GN3JzZDRwZz09', '2021-01-17'),
(5, 'Gabo', 'T1', NULL, 'gabofc+t1@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', '2021-01-05'),
(6, 'Gabo ', 'T2', NULL, 'gabofc+t2@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', '2021-01-05'),
(7, 'Ismelda', 'Coco', NULL, 'coco@romanf.com', 'WDgyUmpZbjQwUVlLSVlkRzBMQkZ3dz09', '2021-01-08'),
(8, 'John', 'Doe', NULL, 'johndoe@test.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', '2021-01-12'),
(9, 'Thalia', 'Wilson', NULL, 'thalia@gmail.com', 'SG5POGY5amJ2QU1INVFzK2tHWWVOdz09', '2021-01-18'),
(10, 'Eduardo Raymundo', 'Villagran', NULL, 'eduardo_raymundo@hotmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', '2021-01-19'),
(11, 'David', 'Lindberg', NULL, 'david@innerworks.io', 'Y1RESjk0OXpYVE9zbVFmTURZSVo0UT09', '2021-01-19'),
(12, 'bb2', 'bb2', NULL, 'test@me.com', 'cXZLWUs2a3V5aDF0aE9PT2xUUDQzdz09', '2021-01-19'),
(13, 'Iván', 'Martin', '4880-327.jpg', 'martin.maseda@gmail.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', '2021-01-22'),
(14, 'Ivan test', 'Test', NULL, 'ivan@test.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', '2021-01-22'),
(15, 'John', 'Doe', NULL, 'johndoe@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', '2021-01-25'),
(16, 'Nils', 'von Heijne 2', NULL, 'nils@worldofwisdom.io', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', '2021-01-26'),
(17, 'EDUARDO', 'LOPEZ', NULL, 'prueba@estudiante.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', '2021-01-28'),
(18, 'Roman', 'Fernandez', NULL, 'roman@ephata.me', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', '2021-01-28'),
(19, 'Gabo', 'FC', NULL, 'gabofc+t100@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', '2021-02-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userserie`
--

DROP TABLE IF EXISTS `userserie`;
CREATE TABLE IF NOT EXISTS `userserie` (
  `userSerieId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  PRIMARY KEY (`userSerieId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userserie`
--

INSERT INTO `userserie` (`userSerieId`, `userId`, `serieId`) VALUES
(1, 3, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocategories`
--

DROP TABLE IF EXISTS `videocategories`;
CREATE TABLE IF NOT EXISTS `videocategories` (
  `vcId` int(11) NOT NULL AUTO_INCREMENT,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  PRIMARY KEY (`vcId`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videocategories`
--

INSERT INTO `videocategories` (`vcId`, `videoId`, `categoryId`) VALUES
(3, 1, 1),
(5, 3, 1),
(6, 8, 1),
(8, 9, 1),
(9, 10, 1),
(10, 14, 1),
(11, 31, 2),
(12, 32, 2),
(13, 33, 3),
(14, 34, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `videoId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=categories,videocategories',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`videoId`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`videoId`, `clientId`, `coachId`, `serieId`, `video`, `titulo`, `imagen`, `description`) VALUES
(31, 3, 6, 18, 'MTA3NDU2NA==', 'Life as a gift or a struggle', 'sanna_glad.jpg', 'Are you the doer or is life really happening without any effort? Try this meditation and see for your self. '),
(32, 3, 3, 19, 'MTA3NTM5Mw==', 'Video 1', 'teaser-uhmay.jpg', 'video 1'),
(33, 3, 3, 20, 'MTA3NTM5Ng==', 'Video 1', 'logo-nye.jpg', 'VIdeo 1'),
(34, 3, 3, 21, 'MTA3NTM5OA==', 'Video 1', 'logo-nye.jpg', 'Video 1'),
(20, 3, 1, 14, 'MTA3MTU0NQ==', '1: Understanding the change you\'re going through', 'skärmavbild-2021-02-02-kl-163034.png', 'Introduction to The Loop as a core pattern of how change plays out in life, how to master the change process and manifest the change you need.'),
(15, 4, 18, 16, 'MTA2ODg2Mg==', 'Intro to Hero\'s Journey', 'heros-journey-thumbnail.jpg', 'Hero\'s journey is a workshop that takes children on a journey to understanding fear and building courage. It utilizes basic language and comprehension skills and builds on lessons learned at school with fun exercises.'),
(18, 4, 18, 17, 'MTA3MDk3Nw==', 'Meet Valeria', 'valeria_profile_square2.jpg', 'Meet Valeria, the co-founder of Ephata and get to know her story that led to building schools and complementary educational curriculums for children.'),
(16, 4, 18, 17, 'MTA2ODg5OA==', '7 Pillars of Ephata', 'ephata-7-pillarspng.png', 'This short video explains the 7 pillars of Ephata and how they come together to guide children towards a path of faith, wisdom and goodness.'),
(17, 4, 18, 17, 'MTA2ODkzOA==', 'What is Ephata?', 'tlm121720kg-19.jpg', 'We learn about the story of Ephata and how it can nurture children in many ways beyond home and school.'),
(21, 3, 1, 14, 'MTA3MTU0OA==', '2: Find your starting point', 'skärmavbild-2021-02-02-kl-164639.png', 'Explore who you are in this moment of your life, and find the starting point of your current transformation loop.\n\nHave pen and paper ready!'),
(22, 3, 1, 14, 'MTA3MTU2MQ==', '3: Find your shadow', 'skärmavbild-2021-02-02-kl-165002.png', 'Explore your shadow by mapping your psychological pain, anger, anxiety and other challenging emotions in your life.'),
(23, 3, 1, 14, 'MTA3MTU2NQ==', '4: Embrace your shadow', 'skärmavbild-2021-02-02-kl-165152.png', 'Instructions for shadow breathwork. This session is also offered as an optional live experience. \nPlease read the safety guidelines first: http://tiny.cc/breathesafely'),
(24, 3, 1, 14, 'MTA3MTU3Mw==', '5: Find your light', 'skärmavbild-2021-02-02-kl-165320.png', 'Connect with your inner light and map your pride, passion and curiosity.'),
(25, 3, 1, 14, 'MTA3MTU4Mw==', '6: Embrace your light', 'skärmavbild-2021-02-02-kl-165432.png', 'Visualisation meditation to connect with your path of light'),
(26, 3, 1, 14, 'MTA3MTU4Nw==', '7: Find your challenge', 'skärmavbild-2021-02-02-kl-165529.png', 'Explore what is holding you back from changing, and map your fears and energy drainers'),
(27, 3, 1, 14, 'MTA3MTU5Mg==', '8: Embrace your challenge', 'skärmavbild-2021-02-02-kl-165635.png', 'Instructions for Hero\'s breathwork. This session is also offered as an optional live experience. Please read the safety guidelines first: http://tiny.cc/breathesafely'),
(28, 3, 1, 14, 'MTA3MTU5NA==', '9: Manifest the change you need', 'skärmavbild-2021-02-02-kl-165748.png', 'Step into manifestation and transform your life. Link to your agreement: https://tiny.cc/myagreement'),
(29, 3, 1, 14, 'MTA3MTU5NQ==', '10: Integration and follow up', 'skärmavbild-2021-02-02-kl-165846.png', 'Closing integration session, also offered as an optional live experience'),
(30, 3, 1, 14, 'MTA3MTY1NA==', 'BONUS: 8D sound healing', 'skärmavbild-2021-02-02-kl-170013.png', 'As a little bonus to celebrate your completion of The Loop, we\'re giving you a sound healing session in 8D. Bring your headphones!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
