CREATE TABLE IF NOT EXISTS `articlecircle` (
  `articleCircleId` int NOT NULL AUTO_INCREMENT,
  `articleId` int NOT NULL COMMENT 'opciones=articles',
  `circleId` int NOT NULL COMMENT 'opciones=circles',
  PRIMARY KEY (`articleCircleId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `userarticle` (
  `userArticleId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `articleId` int NOT NULL COMMENT 'opciones=articles',
  PRIMARY KEY (`userArticleId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `coacharticle` (
  `coachArticleId` int NOT NULL AUTO_INCREMENT,
  `coachId` int NOT NULL COMMENT 'opciones=coaches',
  `articleId` int NOT NULL COMMENT 'opciones=articles',
  PRIMARY KEY (`coachArticleId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `articlerequest` (
  `requestArticleId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `articleId` int NOT NULL COMMENT 'opciones=articles',
  PRIMARY KEY (`requestArticleId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `articles` ADD `accessId` INT NOT NULL DEFAULT '3' COMMENT 'opciones=access' AFTER `description`;

ALTER TABLE `articles` CHANGE `serieId` `serieId` INT(11) NULL COMMENT 'opciones=series';

ALTER TABLE `articles` ADD `pdf` VARCHAR(200) NULL COMMENT 'archivo' AFTER `description`;

ALTER TABLE `articles` CHANGE `articleId` `articleId` INT NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticle;multiple=coaches,coacharticle;multiple=circles,articlecircle;';

ALTER TABLE `coaches` ADD `zoomAuth` VARCHAR(200) NULL AFTER `emailConfirmed`, ADD `zoomToken` VARCHAR(2000) NULL AFTER `zoomAuth`;

ALTER TABLE `coaches` ADD `zoomRefresh` VARCHAR(2000) NULL AFTER `zoomToken`;

CREATE TABLE IF NOT EXISTS `audios` (
  `audioId` int NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,useraudio;multiple=coaches,coachaudio;multiple=categories,audiocategories;multiple=circles,audiocircle;',
  `clientId` int NOT NULL COMMENT 'opciones=clients',
  `coachId` int NOT NULL COMMENT 'opciones=coaches',
  `audio` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `accessId` int NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  PRIMARY KEY (`audioId`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `coachaudio` (
  `coachAudioId` int NOT NULL AUTO_INCREMENT,
  `coachId` int NOT NULL COMMENT 'opciones=coaches',
  `audioId` int NOT NULL COMMENT 'opciones=audios',
  PRIMARY KEY (`coachAudioId`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `audiorequest` (
  `requestAudioId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `audioId` int NOT NULL COMMENT 'opciones=audios',
  PRIMARY KEY (`requestAudioId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `audiocircle` (
  `audioCircleId` int NOT NULL AUTO_INCREMENT,
  `audioId` int NOT NULL COMMENT 'opciones=audios',
  `circleId` int NOT NULL COMMENT 'opciones=circles',
  PRIMARY KEY (`audioCircleId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `audiocategories` (
  `acId` int NOT NULL AUTO_INCREMENT,
  `audioId` int NOT NULL COMMENT 'opciones=audios',
  `categoryId` int NOT NULL COMMENT 'opciones=categories',
  PRIMARY KEY (`acId`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `useraudio` (
  `userAudioId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `audioId` int NOT NULL COMMENT 'opciones=audios',
  PRIMARY KEY (`userAudioId`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `callcategories` (
  `ccId` int NOT NULL AUTO_INCREMENT,
  `callId` int NOT NULL COMMENT 'opciones=calls',
  `categoryId` int NOT NULL COMMENT 'opciones=categories',
  PRIMARY KEY (`ccId`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

ALTER TABLE `calls` CHANGE `callId` `callId` INT NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;multiple=circles,callscircle;multiple=categories,callcategories;';

ALTER TABLE `calls` ADD `allMembers` CHAR(1) NULL COMMENT 'activar' AFTER `categoryId`, ADD `allCoaches` CHAR(1) NULL COMMENT 'activar' AFTER `allMembers`;

ALTER TABLE `calls` ADD `zoomId` VARCHAR(200) NULL AFTER `image`;

CREATE TABLE IF NOT EXISTS `excluded` (
  `excludedId` int NOT NULL AUTO_INCREMENT,
  `callId` int NOT NULL COMMENT 'opciones=calls',
  `userId` int NOT NULL COMMENT 'opciones=users',
  PRIMARY KEY (`excludedId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `circles` ADD `parentId` INT NOT NULL DEFAULT '0' AFTER `coachId`, ADD `public` CHAR(1) NOT NULL DEFAULT 'S' COMMENT 'activar' AFTER `parentId`;
