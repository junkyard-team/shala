CREATE TABLE IF NOT EXISTS `videocircle` (
  `videoCircleId` int NOT NULL AUTO_INCREMENT,
  `videoId` int NOT NULL COMMENT 'opciones=videos',
  `circleId` int NOT NULL COMMENT 'opciones=circles',
  PRIMARY KEY (`videoCircleId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `uservideo` (
  `userVideoId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `videoId` int NOT NULL COMMENT 'opciones=videos',
  PRIMARY KEY (`userVideoId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `coachvideo` (
  `coachVideoId` int NOT NULL AUTO_INCREMENT,
  `coachId` int NOT NULL COMMENT 'opciones=coaches',
  `videoId` int NOT NULL COMMENT 'opciones=videos',
  PRIMARY KEY (`coachVideoId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `videorequest` (
  `requestVideoId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL COMMENT 'opciones=users',
  `videoId` int NOT NULL COMMENT 'opciones=videos',
  PRIMARY KEY (`requestVideoId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `videos` ADD `accessId` INT NOT NULL DEFAULT '3' COMMENT 'opciones=access' AFTER `description`;

ALTER TABLE `videos` CHANGE `serieId` `serieId` INT(11) NULL COMMENT 'opciones=series';