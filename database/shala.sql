-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-01-2022 a las 10:56:19
-- Versión del servidor: 5.6.51-cll-lve
-- Versión de PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shala`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access`
--

CREATE TABLE `access` (
  `accessId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `access`
--

INSERT INTO `access` (`accessId`, `name`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered users'),
(4, 'Circles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessadmin`
--

CREATE TABLE `accessadmin` (
  `accessId` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessadmin`
--

INSERT INTO `accessadmin` (`accessId`, `nombre`) VALUES
(7, 'generalSetup'),
(8, 'platformsManage'),
(9, 'webContent'),
(10, 'userManage'),
(11, 'platformUsers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessuseradmin`
--

CREATE TABLE `accessuseradmin` (
  `accessUserId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `accessId` int(11) NOT NULL COMMENT 'opciones=accessadmin'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessuseradmin`
--

INSERT INTO `accessuseradmin` (`accessUserId`, `tipoId`, `accessId`) VALUES
(11, 2, 7),
(12, 2, 8),
(13, 2, 9),
(14, 2, 11),
(15, 2, 10),
(16, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `usuario` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL COMMENT 'pass'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`adminId`, `tipoId`, `nombre`, `usuario`, `pass`) VALUES
(1, 2, 'Administrator', 'admin', 'MGRzOGtMVXcxRDJHR3piQ0NyMVdJVEtoWGE5RWVrL3p6blFMdEtyQTErST0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `answerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `questionId` int(11) NOT NULL COMMENT 'opciones=questionnaire',
  `answer` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`answerId`, `clientId`, `userId`, `questionId`, `answer`) VALUES
(36, 3, 3, 5, 'Three'),
(35, 3, 3, 4, 'Two'),
(34, 3, 3, 3, 'One'),
(33, 3, 3, 2, 'Two'),
(12, 3, 4, 1, 'One'),
(13, 3, 4, 2, 'Two'),
(14, 3, 4, 3, 'Three'),
(15, 3, 4, 4, 'Four'),
(16, 3, 4, 5, 'Five'),
(32, 3, 3, 1, 'Four'),
(22, 3, 5, 1, 'One'),
(23, 3, 5, 2, 'Two'),
(24, 3, 5, 3, 'Three'),
(25, 3, 5, 4, 'Four'),
(26, 3, 5, 5, 'Five'),
(27, 3, 6, 1, 'One'),
(28, 3, 6, 2, 'Two'),
(29, 3, 6, 3, 'Three'),
(30, 3, 6, 4, 'Four'),
(31, 3, 6, 5, 'Five'),
(37, 3, 7, 1, 'One'),
(38, 3, 7, 2, 'Two'),
(39, 3, 7, 3, 'Two'),
(40, 3, 7, 4, 'Four'),
(41, 3, 7, 5, 'Two'),
(42, 3, 8, 1, 'One'),
(43, 3, 8, 2, 'Two'),
(44, 3, 8, 3, 'Three'),
(45, 3, 8, 4, 'Four'),
(46, 3, 8, 5, 'Two'),
(47, 3, 9, 1, 'One'),
(48, 3, 9, 2, 'Three'),
(49, 3, 9, 3, 'Two'),
(50, 3, 9, 4, 'Two'),
(51, 3, 9, 5, 'One'),
(52, 3, 10, 1, 'One'),
(53, 3, 10, 2, 'Two'),
(54, 3, 10, 3, 'Three'),
(55, 3, 10, 4, 'Four'),
(56, 3, 10, 5, 'Five'),
(57, 3, 11, 1, 'Two'),
(58, 3, 11, 2, 'Four'),
(59, 3, 11, 3, 'Three'),
(60, 3, 11, 4, 'Two'),
(61, 3, 11, 5, 'Three'),
(62, 3, 12, 1, 'Four'),
(63, 3, 12, 2, 'Two'),
(64, 3, 12, 3, 'Three'),
(65, 3, 12, 4, 'Three'),
(66, 3, 12, 5, 'Three'),
(67, 3, 13, 1, 'One'),
(68, 3, 13, 2, 'Two'),
(69, 3, 13, 3, 'Three'),
(70, 3, 13, 4, 'Four'),
(71, 3, 13, 5, 'Five'),
(72, 3, 15, 1, 'Two'),
(73, 3, 15, 2, 'One'),
(74, 3, 15, 3, 'Three'),
(75, 3, 15, 4, 'Two'),
(76, 3, 15, 5, 'Four'),
(77, 3, 16, 1, 'Three'),
(78, 3, 16, 2, 'Two'),
(79, 3, 16, 3, 'Two'),
(80, 3, 16, 4, 'Two'),
(81, 3, 16, 5, 'Three'),
(82, 4, 17, 6, 'One'),
(83, 4, 18, 6, 'Two'),
(84, 3, 19, 1, 'Four'),
(85, 3, 19, 2, 'Four'),
(86, 3, 19, 3, 'Four'),
(87, 3, 19, 4, 'Five'),
(88, 3, 19, 5, 'One'),
(89, 7, 22, 7, 'Lodging'),
(90, 7, 22, 8, 'Referral'),
(91, 7, 23, 7, 'Retreat'),
(92, 7, 23, 8, 'Referral'),
(93, 8, 24, 9, 'Two'),
(94, 8, 24, 10, 'Four'),
(95, 8, 24, 11, 'One'),
(96, 8, 24, 12, 'Four'),
(97, 8, 24, 13, 'Five'),
(98, 3, 26, 1, 'Three'),
(99, 3, 26, 2, 'One'),
(100, 3, 26, 3, 'Two'),
(101, 3, 26, 4, 'Four'),
(102, 3, 26, 5, 'Five'),
(103, 8, 27, 9, 'Two'),
(104, 8, 27, 10, 'Three'),
(105, 8, 27, 11, 'Three'),
(106, 8, 27, 12, 'Four'),
(107, 8, 27, 13, 'Five'),
(108, 8, 28, 9, 'One'),
(109, 8, 28, 10, 'Three'),
(110, 8, 28, 11, 'Two'),
(111, 8, 28, 12, 'Four'),
(112, 8, 28, 13, 'Five'),
(116, 3, 30, 1, 'Two'),
(117, 3, 30, 2, 'One'),
(118, 3, 30, 3, 'Three'),
(119, 3, 30, 4, 'Five'),
(120, 3, 30, 5, 'Four'),
(121, 8, 31, 9, 'Two'),
(122, 8, 31, 10, 'Three'),
(123, 8, 31, 11, 'Three'),
(124, 8, 31, 12, 'Five'),
(125, 8, 31, 13, 'Two'),
(126, 8, 32, 9, 'Three'),
(127, 8, 32, 10, 'Five'),
(128, 8, 32, 11, 'One'),
(129, 8, 32, 12, 'Two'),
(130, 8, 32, 13, 'Three'),
(131, 8, 33, 9, 'Two'),
(132, 8, 33, 10, 'Two'),
(133, 8, 33, 11, 'Two'),
(134, 8, 33, 12, 'Three'),
(135, 8, 33, 13, 'Three'),
(136, 3, 34, 1, 'One'),
(137, 3, 34, 2, 'One'),
(138, 3, 34, 3, 'Two'),
(139, 3, 34, 4, 'Two'),
(140, 3, 34, 5, 'Two'),
(141, 3, 35, 1, 'Two'),
(142, 3, 35, 2, 'One'),
(143, 3, 35, 3, 'Two'),
(144, 3, 35, 4, 'Two'),
(145, 3, 36, 1, 'Online search'),
(146, 3, 37, 1, 'Invited by a Guide/Coach'),
(147, 3, 38, 1, 'Invited by a Guide/Coach'),
(148, 3, 39, 1, 'Invited by a Guide/Coach'),
(149, 3, 40, 1, 'Invited by a Guide/Coach'),
(150, 3, 41, 1, 'Invited by a Guide/Coach'),
(151, 3, 42, 1, 'Invited by a Guide/Coach'),
(152, 3, 43, 1, 'Invited by a Guide/Coach'),
(153, 3, 45, 1, 'Invited by a Guide/Coach'),
(154, 3, 46, 1, 'Invited by a Guide/Coach'),
(155, 3, 47, 1, 'Invited by a Guide/Coach'),
(156, 3, 48, 1, 'Invited by a Guide/Coach'),
(157, 3, 49, 1, 'Invited by a Guide/Coach'),
(158, 3, 50, 1, 'Invited by a Guide/Coach'),
(159, 3, 51, 1, 'Invited by a Guide/Coach'),
(160, 3, 52, 1, 'Invited by a Guide/Coach'),
(161, 3, 53, 1, 'Invited by a Guide/Coach'),
(162, 3, 54, 1, 'Invited by a Guide/Coach'),
(163, 3, 55, 1, 'Invited by a Guide/Coach'),
(164, 3, 56, 1, 'Invited by a Guide/Coach'),
(165, 3, 57, 1, 'Invited by a Guide/Coach'),
(166, 3, 58, 1, 'Invited by a Guide/Coach'),
(167, 3, 60, 1, 'Referred by a friend'),
(168, 3, 62, 1, 'Referred by a friend'),
(169, 3, 63, 1, 'Referred by a friend'),
(170, 3, 64, 1, 'Invited by a Guide/Coach'),
(171, 3, 67, 1, 'Online search');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appearance`
--

CREATE TABLE `appearance` (
  `appearanceId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `h1` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h1Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `p` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `pMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `boton` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `botonMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `buttonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `buttonColor` varchar(10) DEFAULT NULL COMMENT 'color',
  `buttonColorText` varchar(10) DEFAULT NULL COMMENT 'color',
  `menuLink` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `menuLinkMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `appearance`
--

INSERT INTO `appearance` (`appearanceId`, `clientId`, `h1`, `h1Mobile`, `h2`, `h2Mobile`, `h3`, `h3Mobile`, `h4`, `h4Mobile`, `h5`, `h5Mobile`, `p`, `pMobile`, `boton`, `botonMobile`, `buttonRounded`, `buttonColor`, `buttonColorText`, `menuLink`, `menuLinkMobile`) VALUES
(3, 10, '28px', NULL, '24px', NULL, '22px', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24px', NULL, 'N', NULL, NULL, NULL, NULL),
(4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16px', NULL, 'S', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articlecircle`
--

CREATE TABLE `articlecircle` (
  `articleCircleId` int(11) NOT NULL,
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articlecircle`
--

INSERT INTO `articlecircle` (`articleCircleId`, `articleId`, `circleId`) VALUES
(6, 5, 38),
(7, 6, 38),
(12, 7, 37),
(14, 8, 39),
(19, 9, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articlerequest`
--

CREATE TABLE `articlerequest` (
  `requestArticleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `articleId` int(11) NOT NULL COMMENT 'multiple=users,userarticle;multiple=coaches,coacharticle;multiple=circles,articlecircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) DEFAULT NULL COMMENT 'opciones=series',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `pdf` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `orden` int(11) DEFAULT NULL,
  `circleCoachLock` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Give permission to Circle Owner'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`articleId`, `clientId`, `coachId`, `serieId`, `titulo`, `imagen`, `description`, `pdf`, `accessId`, `orden`, `circleCoachLock`) VALUES
(5, 3, 55, NULL, 'Action Learning Guide', 'social-skills-development-2021-09-24-03-32-38-utc1635521683.jpg', 'Action Learning is appropriate when you have to deal with ill-defined complex issues. It\nwould not just be enough to get an expert or a plumber in to do the job. Someone needs to\nmake hard decisions where no one can tell if there is a good solution at all, or there are\nmany, too many options.\nIt is essential that at least one person in the room deeply cares about the issue at hand and\ncan do at least something about it. Action learning deals with real practical issues, not just ‘theory’.', 'action-learning-guide1635212325.pdf', 2, NULL, 1),
(6, 3, 55, NULL, 'The Coaching Template', 'female-coach-showing-project-management-studies-ov-pqzw4981635212431.jpg', 'A beginner\'s guide to Fraendi\'s Action Learning Coaching Template', 'the-coaching-template1635212431.pdf', 2, NULL, 1),
(8, 3, 35, NULL, 'Introduction to Ephata', 'ephata-intro-doc1635558965.png', 'A digital program offering supplementary courses, mentorship and workshops for parents and children', '2021-02-05-ephata_introduction_v011635559026.pdf', 2, NULL, 1),
(9, 3, 1, NULL, 'test1', '1637825680.jpg', 'sdcsdfdfvd', '1638427316.pdf', 3, NULL, 1),
(7, 3, 55, NULL, 'Overview: Assessing developmental text for cognitive and social emotional development', 'laske-overview51635524416.png', 'The program is structured in 10 2-hour sessions, with 2 sessions per week:\n\n2, 4, 16, 18, 30 November / 2, 7, 9, 14, 16 December\nTime: 20:00 - 22:00 CET', '2021-10-29-fraendi-laske-program-overview1635521558.pdf', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseriecircle`
--

CREATE TABLE `articleseriecircle` (
  `serieArticlesCircleId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleserierequest`
--

CREATE TABLE `articleserierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseries`
--

CREATE TABLE `articleseries` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;multiple=circles,articleseriecircle;',
  `clientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseries`
--

INSERT INTO `articleseries` (`serieId`, `clientId`, `coachId`, `accessId`, `nombre`, `description`, `photo`) VALUES
(1, 3, 1, 2, 'Design Your Future Now', 'A journey into consciously designing your future and manifesting it into your lived reality', 'aditya-saxena-_mixhvl_wza-unsplash-1.jpg'),
(2, 3, 9, 3, 'Sound Healing', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', NULL),
(3, 3, 55, 2, 'Action Learning', 'Fraendi pilot program', 'fraendi_logo_ohne_rand-removebg1630508247.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseriescategories`
--

CREATE TABLE `articleseriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseriescategories`
--

INSERT INTO `articleseriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(45, 1, 4),
(44, 1, 2),
(41, 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiocategories`
--

CREATE TABLE `audiocategories` (
  `acId` int(11) NOT NULL,
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audiocategories`
--

INSERT INTO `audiocategories` (`acId`, `audioId`, `categoryId`) VALUES
(59, 62, 2),
(60, 63, 2),
(61, 64, 2),
(62, 65, 2),
(63, 66, 2),
(64, 67, 2),
(65, 68, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiocircle`
--

CREATE TABLE `audiocircle` (
  `audioCircleId` int(11) NOT NULL,
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audiocircle`
--

INSERT INTO `audiocircle` (`audioCircleId`, `audioId`, `circleId`) VALUES
(4, 67, 33),
(5, 68, 33),
(8, 72, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiorequest`
--

CREATE TABLE `audiorequest` (
  `requestAudioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audios`
--

CREATE TABLE `audios` (
  `audioId` int(11) NOT NULL COMMENT 'multiple=users,useraudio;multiple=coaches,coachaudio;multiple=categories,audiocategories;multiple=circles,audiocircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `audio` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `circleCoachLock` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Give permission to Circle Owner'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audios`
--

INSERT INTO `audios` (`audioId`, `clientId`, `coachId`, `audio`, `titulo`, `imagen`, `description`, `accessId`, `circleCoachLock`) VALUES
(69, 3, 1, '1637829858.mp4', 'Admin', '1637829864.jpg', 'cvfdvgdfgfdhbgfh', 3, 1),
(72, 3, 6, '1638521942.m4a', 'Test Audio', '1638521931.jpg', 'Test', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannerhome`
--

CREATE TABLE `bannerhome` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `buttonText` varchar(200) NOT NULL,
  `buttonUrl` varchar(200) NOT NULL COMMENT 'liga'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannerhome`
--

INSERT INTO `bannerhome` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`, `buttonText`, `buttonUrl`) VALUES
(8, 7, 'RENT SPACE', 'Rent our facilities for retreats, workshops and special events. Nierika is situated in a peaceful and quiet river valley where the song of birds and the whisper of the wind inspire tranquility and introspection. Our ecological facilities and permaculture gardens help you feel immersed in nature and community.', 'pagoda-bed-room-pic.jpg', NULL, 'Learn more', '/about'),
(3, 3, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(4, 3, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore'),
(5, 4, 'FOR EVERY SPACE, AT ANY PACE\r\n', 'From the comfort of your living room to outside spaces, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you.\r\n', 'for-every-ephata.jpg', NULL, 'Explore classes', '/explore'),
(6, 4, 'HIGHLY ENGAGING CONTENT\r\n', 'We understand the needs of children and designed our programs accordingly in byte size videos, personalized live sessions, engaging group workshops and fun exercises.\r\n', 'highly-engaging-ephata.jpg', NULL, 'Explore classes', '/explore'),
(12, 7, 'Retreats', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'img_6822-will-need-cropping2.jpg', NULL, 'Learn more', '/explore'),
(13, 8, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(14, 8, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannersomos`
--

CREATE TABLE `bannersomos` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(10000) NOT NULL COMMENT 'html',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannersomos`
--

INSERT INTO `bannersomos` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`) VALUES
(7, 7, ' ', 'At Nierika, we provide a natural path to recovery and healing that helps you grow from your current challenge into the new phase in life.\n\nWe create an environment for deep self-exploration that allows new insights to emerge from within.\n\nOur client-centered approach has demonstrated effectiveness in conditions such as chronic depression, substance abuse, eating disorders, anxiety, loss and grief issues, PTSD, spiritual emergency and other mental, emotional and behavioral challenges.', 'pond.jpg', NULL),
(8, 7, 'Our History', 'Over the last decade, Centro Nierika has hosted an impressive range of events.  We provide a safe, beautiful and ecological space for holistic retreats, workshops, therapies, ceremonies, family gatherings, staff retreats and other special events.\n\nIndigenous healers from North & South America, internationally recognized psychologists, educators and persons seeking alternative paths to healing find Nierika the perfect setting for deep and transformative work.', 'centre-int-view-through-mirror-1-copy.jpg', NULL),
(9, 7, 'The Region', 'Our sanctuary is nested in a lush subtropical valley, at about 5,577′ (1,700 m) elevation in the Sierra Mountains.  The climate is moderate with mild humidity, pleasantly warm day temperatures that cool down at night. Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally.', 'img_7398.jpg', NULL),
(10, 7, 'Getting here', 'Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally. You can also take Uber, Didi or local taxi from the airport or the city to arrive here. Fares range from 700 to1500 pesos for a one way trip depending on demand. We recommend avoiding night time travel due to poor visibility on the mountainous roads.', 'mappng.png', NULL),
(11, 7, 'The Facilities', 'The center provides  individual and/or shared comfortable accommodations, architectural spaces, hot solar showers, wireless internet, healthy and delicious meals prepared with organic produce, use of an art studio and yoga/meditation facilities as well as many acres of organic orchards and gardens. You will experience the serenity of the surrounding nature reserve setting, high standards of cleanliness, comfort and a unique style.', 'dinning-room-main-pic-6.jpg', NULL),
(12, 7, 'ACCOMMODATION', 'We can lodge up to 40 people in various types of spaces. Our dormitories are a thermal adobe building with 2 sections (Men & Women) with a capacity of 8 beds each. We also have individual or shared rooms (2 beds), either with personal or shared bathroom. Our suites are queen size, private bathroom, some with and extra individual bed. We also have two bedroom (one queen and two singles) apartment with bathroom, with small living room and kitchen.', 'pool-suite-pic-4-new.jpg', NULL),
(13, 7, 'Food and Water', 'We provide a mostly organic menu with lots of fresh farm-to-table ingredients. Our cooks are masterful in preparing family-style Mexican food, that is healthy, flavorful and vegetarian-friendly. Meals are included in the cost of accommodation.\n\nNierika’s permaculture gardens are thriving with avocado, citrus, and coffee orchards, fresh greens and other vegetables as well as medicinal herbs. All our water comes straight from Mountain springs and goes through a chemical-free filtration process.', 'alfresco-shot-1.jpg', NULL),
(14, 7, 'Workspaces', 'Center Space with terrace – An architecturally designed roofted terrace with sliding doors connecting to the altar and music room.\n\nThe Rooftop – Terrace with amazing valley view, suitable for group yoga and meditation.\n\nThe Cave of Visions – Inspire that a rock we could not move, we created a man-made cave with rock paintings that transport you to a mystical and magical space. It also has a fire place.', 'home_n_0007_zen_6897-edit-edit-scaled.jpg', NULL),
(15, 7, 'Teocali', 'A sacred geometry designed circular prayer & ceremonial room, with the possibility of having a fire inside, where countless ceremonies of different traditions have been held.', 'tucali-wide-shot.jpg', NULL),
(16, 7, 'Our Gardens', 'Immerse yourself in the beautiful natural landscapes filled with exotic flowers and ancient trees, suited for a process of profound inner growth and contemplation. Enjoy practicing organic gardening, seeding edible plants and farming in our lovely gardens and orchards, allowing life to grow in the natural path. Our gardens also provide a wonderful opportunity for giving back to Mother Earth by learning how to grow your own food while you help us harvest for our kitchen or sow new seeds.', 'gardens.jpg', NULL),
(17, 7, 'Temictla ', 'The Nierika project extends over 30 acres and is comprised by two sections divided by a river. Temictla, in nahuatl, means the place which is beyond dreams. It holds a reconstructed pyramid and an amphitheater field extends into the mountains where diverse public events have been held, such as Council of Visions, musical concerts and sundance, children camp, ideal for bigger groups and festivals. Temictla is excellent for morning walks up the mountain, and for outdoor group activities.', 'img_7398.jpg', NULL),
(18, 7, 'Nearby Nierika', 'Discover Historical Towns, Sacred Sites & Mythical Surroundings. Nestled in the Chichinautzin natural reserve 2 hours south of Mexico city, we are located in a lush subtropical watershed rich valley with springs and waterfalls. Ancient myths and archetypes live on in nearby historic world-heritage sites, temples and spiritual pilgrimage routes.', 'surroundings.jpg', NULL),
(20, 8, 'Section 1', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', '42-15216471.jpg', NULL),
(21, 8, 'Section 2', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', 'azulik_web_cover.jpg', 'MTEwNzI1NA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `callcategories`
--

CREATE TABLE `callcategories` (
  `ccId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `callcategories`
--

INSERT INTO `callcategories` (`ccId`, `callId`, `categoryId`) VALUES
(62, 139, 2),
(63, 140, 1),
(64, 141, 2),
(65, 142, 2),
(69, 143, 3),
(68, 143, 2),
(73, 144, 2),
(77, 145, 1),
(78, 146, 2),
(80, 148, 4),
(82, 149, 4),
(83, 150, 4),
(93, 138, 4),
(85, 151, 4),
(87, 153, 4),
(88, 155, 4),
(89, 158, 4),
(92, 137, 4),
(94, 147, 4),
(95, 164, 4),
(96, 165, 4),
(97, 166, 4),
(98, 167, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls`
--

CREATE TABLE `calls` (
  `callId` int(11) NOT NULL COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;multiple=circles,callscircle;multiple=categories,callcategories;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `allMembers` char(1) DEFAULT NULL COMMENT 'activar',
  `allCoaches` char(1) DEFAULT NULL COMMENT 'activar',
  `image` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `zoomId` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(2000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL COMMENT 'liga',
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipollamada',
  `fecha` datetime NOT NULL,
  `timezone` varchar(200) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '60'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls`
--

INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `zoomId`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(13, 3, 3, 1, NULL, NULL, NULL, NULL, 'Workshop', NULL, 'https://zoom.us/s/97563617492?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoia05rSlJ0Q0sxdi1aeDFfUVZESi1oeXRZZ1VNT3h1NVlmTGdZV0xVbDJUUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpWai1WQUJKMUFBQUEiLCJleHAiOjE2MTAxMzkwNDksImlhdCI6MTYxMDEzMTg0OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ532tN12H1MdALQ1uXbewIsy4j6brhBMqBPP-Umyi0', 1, '2021-01-08 14:00:00', NULL, 60),
(14, 3, 3, 1, NULL, NULL, NULL, NULL, 'Intro', NULL, 'https://zoom.us/s/93574307471?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoieDVVVDR4a2RuTG1IdXZCWkIwOFl4bXVXSjE4TkRoY0JNeU9kblQ4NE5KMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpYZkIzQUJKMUFBQUEiLCJleHAiOjE2MTAxMzk1NTMsImlhdCI6MTYxMDEzMjM1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.dHC_tOib4tV0EcxRlY66jcLQF_0WAn_y49MQZ1Petj0', 2, '2021-01-08 14:00:00', NULL, 60),
(44, 3, 9, 4, NULL, NULL, NULL, NULL, 'Sound Healing ', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', 'https://zoom.us/s/99428845676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWHVRalVCLW9tMFBWdWtkRDAwMEdQdlVoVFh3UnJleU82T0JBcGVnZEVBby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZG45aTJwQUJKMUFBQUEiLCJleHAiOjE2MTIzNjQxMjIsImlhdCI6MTYxMjM1NjkyMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ9Tqr4sVWM1KG825lRAPnj8UONvXoA_VdJXeK2TVEQ', 1, '2021-02-04 08:00:00', NULL, 45),
(18, 3, 15, 1, NULL, NULL, NULL, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/91585109126?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0lMWVRQU2hMZlhlQ1Y4bnB0OVhjUG5rdFRDc2Vtb2pyd2JXbHVOSTBvNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY1ZfVnhoQUJKMUFBQUEiLCJleHAiOjE2MTA5ODg4NjEsImlhdCI6MTYxMDk4MTY2MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.eRfEN9p6VbtOaErGGtHzlCwNcV-nI0KJMdIvvT6R_jQ', 1, '2021-01-18 10:00:00', NULL, 30),
(19, 3, 3, 1, NULL, NULL, NULL, NULL, 'Intro class', 'Desc', 'https://zoom.us/s/99792834871?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicmxIM2lOYXdyeDU0SVRINkJTazhVdGFYYXVFRGlra002LVQ2Zk1aWGdFay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JHd3FJQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ2OTMsImlhdCI6MTYxMTA2NzQ5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.O5FoUKEjo9qXIoFIN8lRdqzz26HyZmQc8KmLoUnpOEc', 2, '2021-01-20 12:30:00', NULL, 30),
(20, 3, 3, 0, NULL, NULL, NULL, NULL, 'Consultation', NULL, 'https://zoom.us/s/96228600796?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZmcyd3lwZDFHZWdia2VuSWRJUU5mN1p5Q1ZVOFVRcVVCWHREeFB0RGVBUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JITjJEQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ4MTIsImlhdCI6MTYxMTA2NzYxMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ILj3fwYSfXzyLqXDtQzjYudTJCTiOsQ98IZE0ACVYfs', 2, '2021-01-20 18:00:00', NULL, 60),
(21, 3, 3, 1, NULL, NULL, NULL, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/93007611295?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXV2c2REMk5pS1Q3d1VtNlR5OWxwSllaMlFmaXFUbVphaFlMS1NHMk02OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JIa3RVQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ5MDYsImlhdCI6MTYxMTA2NzcwNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.LNGXXRB3OKMHPaK4I466gKp0uTzb4bE9x2uQkM3vHEY', 1, '2021-01-19 10:00:00', NULL, 30),
(23, 3, 3, 2, NULL, NULL, NULL, NULL, 'Public Session', 'Test', 'https://zoom.us/s/94332087831?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicUVCV09yNGRYcWNaQmdnOFF1ai12dUpRcHBsZzBNR19kNWRJcWtsTXgxYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY24wYmVoQUJKMUFBQUEiLCJleHAiOjE2MTEyODc5OTEsImlhdCI6MTYxMTI4MDc5MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DoeF-sRkk2ss-Kj4VVr-eYrmwIAv0z9tKOH0122ynNo', 1, '2021-01-21 19:00:00', NULL, 60),
(30, 3, 3, 1, NULL, NULL, 'roman-videopng.png', NULL, 'Breathwork for Innovation ', 'Test', 'https://zoom.us/s/99732096017?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWW9wRlBzYzNucnBLQlVUeU11V21IZzJiWEtidXFSWE81TFZIeVJyUmtYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJYZU8zQUJKMUFBQUEiLCJleHAiOjE2MTE1MzIwNTgsImlhdCI6MTYxMTUyNDg1OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PSGjJ5rhfBE-rHLjacizH7OuV3S_dk2f3t5LS9w8OzI', 1, '2021-01-24 17:00:00', NULL, 30),
(26, 3, 3, 1, NULL, NULL, 'roman-videopng.png', NULL, 'Breathwork for Memory and Focus', 'Join this 20 minute session to transcend your state of mind and achieve high levels of memory and focus', 'https://zoom.us/s/97025013577?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZVZuNVBMV0lkamJyakZIejFaSmhNQ2VTVmMyV1REd3hhN2ROa2hpb29OVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJFdHlBQUJKMUFBQUEiLCJleHAiOjE2MTE1MjcxNDEsImlhdCI6MTYxMTUxOTk0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gFeRyHl4YGbz6Hb1ozDJCnK5ek0Mhgeax_MEhKmcLeY', 1, '2021-01-24 15:30:00', NULL, 30),
(35, 3, 3, 4, NULL, NULL, 'iw_demopng.png', NULL, 'Innerworks Platform Demo', 'Exclusive presentation for the coach team to demonstrate features in the first version of the Innerworks platfrom.', 'https://zoom.us/s/93689176598?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoid1E2RzJUSVprZ1RHMDJ5S0U4RFREVDVHQ2RITmRUZmlJS0FsamtyaGpSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzV0bXNsQUJKMUFBQUEiLCJleHAiOjE2MTE1ODgxOTIsImlhdCI6MTYxMTU4MDk5MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ZDoClv4qr5KvX0BbZYE1FH1v28WiNy-NvcnUFhhqh3I', 1, '2021-01-30 06:30:00', NULL, 60),
(36, 3, 2, 0, NULL, NULL, NULL, NULL, 'Consultation', NULL, NULL, 2, '2021-01-26 13:30:00', NULL, 60),
(37, 3, 3, 1, NULL, NULL, NULL, NULL, 'User Onboarding', 'Onboarding session for coaches and students to join the Innerworks program', 'https://zoom.us/s/92731884092?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWmUyOEZnMDhMM1h3ME9UREl2NGJTS0dyd293UzdKX2ZkdzlMSHVKNlNEdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzU1OFhLQUJKMUFBQUEiLCJleHAiOjE2MTE1OTE0MjYsImlhdCI6MTYxMTU4NDIyNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.8VsbQ9B8pCwq-pspewAzemABM4PmqXyvpo_Gw7bAFlM', 1, '2021-01-31 18:00:00', NULL, 60),
(38, 4, 18, 5, NULL, NULL, 'highly_engaging_content.jpg', NULL, 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/97092458414?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSjAzdWJCS2xiTHJ4V0lIRWVVVmRvZnpUdGd0UW1rbGN5eTJRZ093aEhoZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtYX1h5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3MzgsImlhdCI6MTYxMTg2MDUzOCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JG3ngE2Klb6yIcbs8_IxTDyEyQOxMOQ-8HpiE0_zEKA', 1, '2021-01-28 12:00:00', NULL, 30),
(39, 4, 18, 5, NULL, NULL, 'highly_engaging_content.jpg', NULL, 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/96476527234?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiLV9EYXZNRlVLVHoxS01nZ1R4cnlRVG9Cc3JLZDV1S0RyQmMteXRNS0RaZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtZQUU5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3NDEsImlhdCI6MTYxMTg2MDU0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.94iOjlG0MAyYcWtwH08GC0YdOPepOswWGCCHosHe5ZU', 1, '2021-01-28 12:00:00', NULL, 30),
(40, 4, 19, 5, NULL, NULL, NULL, NULL, 'First Session', 'This is a test for sessions', 'https://zoom.us/s/94600546115?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiam5uWkMxaVFKY2pxdnVNeHF0NENKNjUyNmZkYk5XOVRxM3VXc2U1a2RIcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtqUEtMQUJKMUFBQUEiLCJleHAiOjE2MTE4NzA2ODcsImlhdCI6MTYxMTg2MzQ4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.I1rwWT1Zg9Qk0cGkhWNFPZGZ-zJ6bLdQCMTWlTInrAI', 1, '2021-01-29 02:00:00', NULL, 60),
(41, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/99729884957?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDVoZHFGSjFPNGdMMlFTVm93MERqdXVVbEp5MDA4Nm9YS210WTVnVEVFMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMEw3QUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Nvzqu35EA6WRjqnRIGFM4Ffl7jwr7kv0Tnj0hRj8_Nc', 1, '2021-02-16 02:00:00', NULL, 60),
(42, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/95030990013?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0FjRXFkc0hjR3NHWENnUmRnYnRfYUJtekIyOW5GS1Z6bExvUXpZckV4by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMFBWQUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.zWmvF41PErMh0aqgz9YFPyXdnziSVd_8xXrxpGNSRAw', 1, '2021-02-16 02:00:00', NULL, 60),
(43, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/96112508917?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXpSS0NMVTVad2t5RmRZdlZPRTJCS054d2x5YVFuOWNxSVJxY19XRlNsTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalNJUmtMQUJKMUFBQUEiLCJleHAiOjE2MTg0NDAyODcsImlhdCI6MTYxODQzMzA4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.24uc757ZB0XUsrlPTBjpeu96VJwaIOS97w0uTHTgHUc', 2, '2021-04-16 12:00:00', 'America/Adak', 60),
(45, 3, 3, 2, NULL, NULL, 'img_3153jpg.jpg', NULL, 'Demo Schedule', 'Bla bla bla', 'https://zoom.us/s/98430952016?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQlRGbTM5WmdoeTZVR2lkVFNfeXRIQXZXbkt5eDlpNV9aRGNaNUJFZklXby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4SmRrQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3NDMsImlhdCI6MTYxMjg5MDU0MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.4R08l4-g8dQCsjqsc4QDfoQi63iulelNcxiAWhbpa0U', 1, '2021-02-09 00:30:00', NULL, 30),
(46, 3, 3, 1, NULL, NULL, 'img_3154jpg.jpg', NULL, 'Demo test', 'bla bla', 'https://zoom.us/s/98236149012?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOHNIZFZXSkV4amFTRVpiTFZ5NWlOZ3AxVEZDajJ6SWY3czMtTFllcW5Ldy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4VzFoQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3OTgsImlhdCI6MTYxMjg5MDU5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.HAzgvFAeHKDcxmjJdpKHicWEqq3ZqtbXL0RYkl4O7sA', 1, '2021-02-09 12:30:00', NULL, 30),
(47, 3, 3, 0, NULL, NULL, NULL, NULL, 'Yoga Intro 1', NULL, NULL, 2, '2021-02-10 06:00:00', NULL, 60),
(48, 3, 3, 1, NULL, NULL, 'dy001484.jpg', NULL, 'Feb 16 Session', 'Testing session', 'https://zoom.us/s/93142513286?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT0ItQzc0cU14bDdsMkhmS0o2aGx5OWtKU0JNR3VlR0FCOE92LWFOZDhGay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZXIySmJOQUJKMUFBQUEiLCJleHAiOjE2MTM1MDMwMzQsImlhdCI6MTYxMzQ5NTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gLGFEYvoiOyKXeUYJCVVulGabFDFjkfwnrxPqxKv-ew', 1, '2021-02-16 12:30:00', NULL, 30),
(49, 3, 3, 2, NULL, NULL, 'img_3153jpg.jpg', NULL, 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96232917912?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVB4S0lUM3hrdWhvYThSVkZ5Z05oMklLclFOd3ZHaG9vakJwUHZoSnc3SS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDcFY2QUJKMUFBQUEiLCJleHAiOjE2MTM4NDE4NTUsImlhdCI6MTYxMzgzNDY1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.xQhfPLbBk6OGkmS1mYN8SmUXOVeJX8k4ceqOPXyIbTs', 2, '2021-02-24 02:00:00', NULL, 30),
(50, 3, 3, 2, NULL, NULL, 'img_3154jpg.jpg', NULL, 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96365592232?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNE9sMUZTWnBicWZBWWl4VDM1VUl1SkZ6S0p0VHptZVZZaEowWVdFLTU5NC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDLXZWQUJKMUFBQUEiLCJleHAiOjE2MTM4NDE5NDIsImlhdCI6MTYxMzgzNDc0MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cvc7_GEYd53DEePa7SvyqeBlaiw7aZNcfkK8JdKiiqU', 1, '2021-02-23 14:30:00', NULL, 30),
(51, 3, 3, 2, NULL, NULL, NULL, NULL, 'February 23', 'This is a description test', 'https://zoom.us/s/98979398236?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNTZlTURiRWlWVXV6ZlJCU2V4bFhNeWlwaGt2M0VtbGdDblRRUXI2bWVCSS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFVQmZIQUJKMUFBQUEiLCJleHAiOjE2MTQxMTQ4NDUsImlhdCI6MTYxNDEwNzY0NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gdUVNvT5aQwVH7isWmRK9dF5fYPcVPjvQhw1SYv9I1w', 1, '2021-02-23 13:30:00', NULL, 60),
(52, 3, 30, 3, NULL, NULL, 'img_0007jpg.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(53, 3, 30, 3, NULL, NULL, 'img_0007jpg.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(54, 3, 30, 3, NULL, NULL, '325692_3417.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', 'https://zoom.us/s/92701510519?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiX2Z3QUJIYTAxR2JfbG5DZnBSbTBwOXdmNzl5Q3V5ZVdkWWZsdm5DbmYyQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiaE56QUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4MTAsImlhdCI6MTYxNDEwOTYxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.m2DU2aY3PevRhw1Vy6zPfhvZL7lUEBL9qLBK-YnesnM', 1, '2021-02-24 14:00:00', NULL, 60),
(55, 3, 3, 4, NULL, NULL, 'cb067372.jpg', NULL, 'Feb 23 Session', 'Feb 23 Session', 'https://zoom.us/s/99410747909?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiN0hCM0stWWlZc1pqZXd5cWYwOGhQYkM5SGszS3Q1MTFJX3FhQ3hONjEwZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiMmhkQUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4OTgsImlhdCI6MTYxNDEwOTY5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Lp62CuODwpHnVwJjv22YsuVKjz3wXicgtkRnEzx3AwE', 1, '2021-02-24 17:00:00', NULL, 60),
(56, 3, 3, 4, NULL, NULL, 'cool20080526_001.jpg', NULL, 'Test Feb 26', 'Test', 'https://zoom.us/s/91456256719?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYkZXeHQyOWZzTDVJaUZMVHBETnVHTUNia09Idkx3Q1dxcGQtVkNuYUJTcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZmY3b09wQUJKMUFBQUEiLCJleHAiOjE2MTQzNzY4ODYsImlhdCI6MTYxNDM2OTY4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NDSuy6MNRxRWSpkPVjh1JMUaLWeqEggBNvK4tVKpAb0', 1, '2021-02-27 18:00:00', NULL, 60),
(57, 3, 3, 1, NULL, NULL, 'huge.jpg', NULL, 'Big Image', 'test', 'https://zoom.us/s/92286786524?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiV0F4R19YX0RPemQyVW5Sa3RwSnB4Nk9zTWlLVmk0RG5kdGdJSjFSZWJwdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnQzV0hCQUJKMUFBQUEiLCJleHAiOjE2MTQ2MTA2NDQsImlhdCI6MTYxNDYwMzQ0NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Ause1FBZXJe47EMpqIyEWOa7dYDH8_NhqFpafxWn19g', 1, '2021-03-03 01:00:00', NULL, 60),
(58, 3, 3, 1, NULL, NULL, 'tlm120720kg-138-small.jpg', NULL, 'Breath of Fire', 'To begin, take a comfortable seated position. \n\nClose your eyes, chin parallel to the floor, face and shoulders relaxed.  \n\nThis exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. \n\nAfter the sharp exhale, relax and allow the breath to enter all on its own. \n\nFocus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/95107350138?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQk1iaWcyYVZjUzZaWWJGZXhveV92R1RodEhWWFdTLUoweWZ0TTl4RTF0TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnp5ejhmQUJKMUFBQUEiLCJleHAiOjE2MTQ3MTAxMTksImlhdCI6MTYxNDcwMjkxOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.l8HImoZ9lOkcXySCtxwu4H5u-JJCy3sDlXUVgCP592g', 0, '2021-03-31 13:00:00', NULL, 30),
(59, 3, 3, 2, NULL, NULL, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', NULL, 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome. ', 'https://zoom.us/s/94619340642?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDQySDE1VzcyRmYtQnVJNmdxQ1hMMzd0eGtOaE9MMDE2aE9GaThjY0pBZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjBFNTA5QUJKMUFBQUEiLCJleHAiOjE2MTQ3MTQ4NjIsImlhdCI6MTYxNDcwNzY2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.hcRxbXkGzi0LWM5QZKzzCzHFeTRXorXhkVE1FXeCkrQ', 0, '2021-03-29 12:00:00', NULL, 30),
(60, 3, 3, 1, NULL, NULL, 'tlm120720kg-138-small.jpg', NULL, 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome.', 'https://zoom.us/s/94210581725?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUVgwS1R3aGxVbHBqbnBObXdDdUU3RkFYTmZJczFIeGNrZzVYWGdZZkZvMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdDa05aQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzE2ODksImlhdCI6MTYxNDgyNDQ4OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.lXTQZERJ97gpIypW5dRYSKJQyc7HMU8dHIENt8W_lTc', 1, '2021-03-08 09:00:00', NULL, 30),
(61, 3, 3, 1, NULL, NULL, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', NULL, 'Breath of Fire', 'To begin, take a comfortable seated position. Close your eyes, chin parallel to the floor, face and shoulders relaxed. This exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. After the sharp exhale, relax and allow the breath to enter all on its own. Focus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/93208341462?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibWdHVVhZd1llbEZRaHBqa2ZxNl9FYW9TRWpNbzlkSFFUR0dTTWF3SlRWZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdFSXZhQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzIxMDEsImlhdCI6MTYxNDgyNDkwMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NpC8J3khl4pZrV-UIhZ5B4rmUA_N4uKOfRlWeTVRB94', 1, '2021-03-15 09:00:00', NULL, 30),
(62, 7, 32, 10, NULL, NULL, 'cueva-venados-1-small.jpg', NULL, 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99083961397?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOUdtUmNWY1lPbWRzZElvMDVwNzE4ZTVWdEROUVJkZVNEMUZrc1E2U0pXdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl83ZzhPQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM3MjcsImlhdCI6MTYxNDkwNjUyNywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OhcMwfpoWJKTAXwbEv0oj0MAtuyIID4-LZtEkcJ0AVo', 1, '2021-03-06 18:00:00', NULL, 30),
(63, 7, 32, 10, NULL, NULL, 'entrada-fmm-003_temazcal.jpg', NULL, 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99753362893?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidHpiNjZqYW9BcW40b2htck9KelB1ZmRtODhQckdEek9nSUlPaW1yNmdYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl84R1lvQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM4ODAsImlhdCI6MTYxNDkwNjY4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.tR2XCG06_woHLOsKAKz-aorU6eBSXTE2zWm9rKZpfH8', 1, '2021-03-09 18:00:00', NULL, 30),
(64, 7, 33, 9, NULL, NULL, 'img_6822-will-need-cropping.jpg', NULL, 'Sound Healing', 'Sound healing works on vibration. Everything is a vibration and you tune your body like you tune an instrument. Different instruments are set to certain frequencies. Sound healing allows your body to heal itself by slowing down your brain waves, which affect every cell in your body, shifting them from diseased to being in ease. It’s just aligning it with whatever you need.', 'https://zoom.us/s/95606205299?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNzNkSE15YWhIU3JMZnA2RWI5UDZKdjRneWIzbmNWQ19sZlJPTWQ5ZGM2RS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl85UTY1QUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQxODYsImlhdCI6MTYxNDkwNjk4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.-h0YU96soev1JT63E6u-dYExTbWbHRG_u5a0UHoG0fc', 1, '2021-03-08 19:00:00', NULL, 30),
(65, 7, 33, 10, NULL, NULL, 'gws-temazcalero-or-mexican-shaman.jpg', NULL, 'Temezcal / Sweatlodge', 'A temazcal is a traditional Mexican steam bath that is in many ways similar to the Native American sweat lodge. Besides promoting physical well-being and healing, the temazcal is also a ritual and spiritual practice in which traditional healing methods are used to encourage reflection and introspection. While the body rids itself of toxins through sweating, the spirit is renewed through ritual. The temazcal is thought to represent the womb and people coming out of the bath are, in a symbolic sense, re-born.', 'https://zoom.us/s/91081953418?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUUd6RVB2b0hTb0dCY09NVngxZ3hrbF9DalZNclBGNlNtVnNhaTlfMEl1by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl8tV1JzQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQ0NzAsImlhdCI6MTYxNDkwNzI3MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.d5RPnKjj3A8FyixJSHewQL2ymjqo2vLwIykH7AoiIU4', 1, '2021-04-13 16:44:06', NULL, 30),
(68, 7, 34, 9, NULL, NULL, 'arttherapy3png.png', NULL, 'Art Therapy', 'Art therapy is founded on the belief that self-expression through artistic creation has therapeutic value for those who are healing or seeking deeper understanding of themselves and their personalities. According to the American Art Therapy Association, art therapists are trained to understand the roles that color, texture, and various art media can play in the therapeutic process and how these tools can help reveal one’s thoughts, feelings, and psychological disposition. Art therapy integrates psychotherapy and some form of visual arts as a specific, stand-alone form of therapy, but it is also used in combination with other types of therapy.', 'https://zoom.us/s/94586952459?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTFp6anZWNGFPYk5FX2VBYjVxSzZoejVqNVhEanp1Q2JNcmNYZjJkcUZHZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZ0FEV2lfQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTU3ODIsImlhdCI6MTYxNDkwODU4MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.efQkT-LwkZVszGw0t9TDOQQkJJID7ecKM_JXXRvMu1E', 1, '2021-03-06 15:00:00', NULL, 30),
(72, 8, 29, 21, NULL, NULL, 'ih212972.jpg', NULL, 'Live session', 'bla blabla', 'https://zoom.us/s/93474495279?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSzJaMHJJaG9xdXY3cTJSV0Q1cnRpbkpEZEpRR091S0JBb2dEOUlyU1ZpZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaURfZzYzQUJKMUFBQUEiLCJleHAiOjE2MTcxMjkzNjgsImlhdCI6MTYxNzEyMjE2OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.5kp9Ns_qAPHp9ehqKFotD5YE8dfPsOdlZfPKhVKMAdg', 1, '2021-04-01 09:00:00', NULL, 30),
(73, 3, 44, 4, NULL, NULL, 'student-using.jpg', NULL, 'Onboarding of Guides', 'We will introduce the latest version of the digital platform to our guide community and outline what we are building ahead so you can get started with creating and sharing content.', 'https://zoom.us/s/91932063976?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYnFfM3NPYVV0cFdCNFZNNDNqZnRRWXo2MUY4LTVtWVVwZmhnUDBXM0YzVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajdraTJMQUJKMUFBQUEiLCJleHAiOjE2MTkxMzU1NjQsImlhdCI6MTYxOTEyODM2NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Vm-La3a8rnHO-tGPIDDH9q4M_rZwkHWcnjcZoGEuP5o', 1, '2021-04-23 14:30:00', 'Europe/Stockholm', 90),
(71, 8, 29, 21, NULL, NULL, 'video-final.png', NULL, 'Updated call', 'This is a call test after edited', 'https://zoom.us/s/92279692885?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXF0MnpYc0hUTTM0cUpBSjYyNVBPTUxMZjRmQ252MVMxUU5GbHZzLUpTMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaGY0XzJPQUJKMUFBQUEiLCJleHAiOjE2MTY1MjM2ODAsImlhdCI6MTYxNjUxNjQ4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.yCJ1TLQDrdZfVokvp5uSsxSpEoK-YeKfCGY1cKJ_GE0', 1, '2021-03-24 08:30:00', NULL, 90),
(95, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1623224042.jpg', NULL, 'Meditation Sweco', 'Meditation time :)', 'https://zoom.us/s/94450722881?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJwV1RxaXd1b0c1U3BNTUpyTHJiUFN2SnlQYks1OHlWS2pBMTY5RkFMczBnLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhudnNVU2lBQkoxQUFBQSIsImV4cCI6MTYyMzIzMTI0MiwiaWF0IjoxNjIzMjI0MDQyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.UOcnzCIj4vez2vycZrLzAwmHkbRJ9BZhozAurPtUhso', 1, '2021-06-09 11:30:00', 'Europe/Stockholm', 30),
(75, 3, 37, 4, NULL, NULL, 'vials-of-samples-with-positive-results-from-patien-r2rhlfz.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/94486212662?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVlHNVRIUTh2NGNwZl95X1RWYmxLV3JYbnV2R2dkdmhXSFZkeXlpREFtby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1MdTE1QUJKMUFBQUEiLCJleHAiOjE2MTgzNDA1MzAsImlhdCI6MTYxODMzMzMzMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.WEHLdgTEx_LfOQ6PJYwVEWdee5nzr9JvyZVjR2I2oAY', 3, '2021-04-14 09:00:00', NULL, 30),
(76, 3, 37, 4, NULL, NULL, 'video-conference-dd9bdje.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/98664949383?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUU5iR3R2T0hoNEU0eFVKU2g3VXc1Y3A4b0k0elFtQ05nck4zWnJEak5kYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1QX0c0QUJKMUFBQUEiLCJleHAiOjE2MTgzNDE2NDYsImlhdCI6MTYxODMzNDQ0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PivqL82KElgtp7yC51y5E_J-hPeEOnnfwbSNqzIpub4', 2, '2021-04-14 09:00:00', NULL, 30),
(77, 8, 29, 21, NULL, NULL, '42-16624306.jpg', NULL, 'Session test 1', 'Session test 1 Session test 1 Session test 1', 'https://zoom.us/s/91903233225?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSXRDQk5XT1hKbXBNeEdnd3BkOU9LYTVVTEF0NW1rbzN6VlYyOG84VEE2Zy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1YTWFHQUJKMUFBQUEiLCJleHAiOjE2MTgzNDM1MzUsImlhdCI6MTYxODMzNjMzNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.pAXJygXfjQ3G2jBoRpCZ1HFPlIctaaV7Go4jXnGkLts', 3, '2021-04-14 09:30:00', NULL, 30),
(78, 3, 37, 4, NULL, NULL, 'video-conference-dd9bdje.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/99889707394?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT1cya1JmV25pUEpxX2ExT205d3VOZGpwUzR2Wno5SF9ZM0Z3eGZSdEVicy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalI1ck5TQUJKMUFBQUEiLCJleHAiOjE2MTg0MzY0NjAsImlhdCI6MTYxODQyOTI2MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OV8pih55NLxVea4sZ34sbeOc7d3OEi66eCTbaCw58j4', 3, '2021-04-16 08:00:00', 'America/Adak', 30),
(81, 3, 37, 1, NULL, NULL, NULL, NULL, 'Test', 'TBD', 'https://zoom.us/s/92351401707?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTVVXd0VHNDBMRzFNTGktZmRFeUtkUl9Hd2VETl93Zjh5NXFzODV1TWNHcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajNUWjJRQUJKMUFBQUEiLCJleHAiOjE2MTkwNjM5NjIsImlhdCI6MTYxOTA1Njc2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RLfL6gR4U6-Qx3AOH_yb4WY9ke4vrcfU-NUNdbzfmhk', 2, '2021-04-22 07:00:00', 'Europe/Stockholm', 30),
(83, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx.jpg', NULL, 'Weekly Meditation', 'This is a weekly series for all levels to explore themselves and move into stillness for body and mind through different styles of meditation. Welcome!  ', 'https://zoom.us/s/94539032093?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiMWtmSzhnd2x4LUYyZWZWTkxZMmNNalkxbGJzM0VfTm41ZGFNTTY2RU02dy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbGNHbW1kQUJKMUFBQUEiLCJleHAiOjE2MjA3NTUxMDUsImlhdCI6MTYyMDc0NzkwNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.-08Vf6J969j3bPfpIsDm-iazmKeZBFDt9fx5lWvrPhE', 1, '2021-05-12 11:30:00', 'Europe/Stockholm', 30),
(90, 3, 6, 2, NULL, NULL, 'papaioannou-kostas-tysecum5hja-unsplash1622016434.jpg', NULL, 'Meditation Sweco', 'Meditation for anyone.', 'https://zoom.us/s/91046077015?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUHd1RGo4anNZalk5MUx4VVVzOWJ1M05UVFh1Vnp5VHZLQ0hxazh6WW41QS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbW50cVB0QUJKMUFBQUEiLCJleHAiOjE2MjIwMjM2MzUsImlhdCI6MTYyMjAxNjQzNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.q0KFpK0jPwlOM_wLEjE0QsPAupQBv0Sd5gdqtAkHHGA', 1, '2021-05-26 11:30:00', 'Europe/Stockholm', 30),
(89, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1621412313.jpg', NULL, 'Meditation Sweco', 'Weekly meditation. Not earlier experience required.', 'https://zoom.us/s/97736079098?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicFpqX2hSOGtfYmhSbXp4SEVQZXFUSEhneVZhOVQ0Z3hidlBQbEdwVm94by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbUR0SHFFQUJKMUFBQUEiLCJleHAiOjE2MjE0MTk1MTMsImlhdCI6MTYyMTQxMjMxMywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ABm86K811RgSMY-MC8_nwV4ezTGs2Vb-kNJFGyyH9Cc', 1, '2021-05-19 11:30:00', 'Europe/Stockholm', 30),
(88, 3, 6, 2, NULL, NULL, NULL, NULL, 'Meditation Sweco', 'Borde funka nu. Heja oss!', 'https://zoom.us/s/94637281366?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZlJYcWlMZGw3ZnBkVjJIaGJTc2ZJeE5Cbmd1SnVka2dQUEZMS3VCcW14TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbGY2ZVI5QUJKMUFBQUEiLCJleHAiOjE2MjA4MTkwMzQsImlhdCI6MTYyMDgxMTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9._Fi3K4k-XLYX5CQPraDbpXDlFaYbW34EGNybdxwwFQc', 1, '2021-05-12 11:30:00', 'Europe/Stockholm', 30),
(92, 3, 6, 2, NULL, NULL, NULL, NULL, 'Meditation Sweco', 'Meditation', 'https://zoom.us/s/98261832057?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJra0cwdHhKSjJGVjdkT2lLM3BaR1FOV2d3eFBMTjlCdmZEejgxMXlTM3JVLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhuTUQ3UThBQkoxQUFBQSIsImV4cCI6MTYyMjYzMzQ1MSwiaWF0IjoxNjIyNjI2MjUxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.a9C_rb5v1zlc-NwsgcoF4ImEznnw_xWmJ6n3qCM8cPE', 1, '2021-06-02 11:30:00', 'Europe/Stockholm', 30),
(97, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1623835629.jpg', NULL, 'Meditation', 'Meditation för alla :)', 'https://zoom.us/s/92538477316?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJ0STlxZ1Bld0dueFNqSXB1WDlZR09DNVNHSTJmdkpCVlVvSi1fc09NbU9VLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhvVUpWZ2xBQkoxQUFBQSIsImV4cCI6MTYyMzg0MjgyOSwiaWF0IjoxNjIzODM1NjI5LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.eaG2mc9UnHe7MmoM6WCOcxalY4wAYtdluvEUuYi9eCY', 1, '2021-06-16 11:30:00', 'Europe/Stockholm', 30),
(100, 3, 6, 2, NULL, NULL, '759fd534-f056-4148-9249-3bba5bddee3f1624440137.jpeg', NULL, 'Meditation', 'För en stunds vila för sinne och kropp', 'https://zoom.us/s/94580801471?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJCeEcxcjBabDVYMXZ1bEx6RE5KUVZGczU2WjRVd1E5bmtlMGhIS2h4SkJnLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhvNExXclNBQkoxQUFBQSIsImV4cCI6MTYyNDQ0NzMzOCwiaWF0IjoxNjI0NDQwMTM4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.8rq3YDlnyVgRPmu-mhAGgvGBoOWEdaANYyNuw4TDnXU', 1, '2021-06-23 11:30:00', 'Europe/Stockholm', 30);
INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `zoomId`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(101, 9, 38, 18, NULL, NULL, 'shala-new-logo1628974645.png', NULL, 'Platform Introduction', 'This is an introductory session for new platform members', 'https://us06web.zoom.us/s/83291805997?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWlZMc3RFVFNTUzRtTmI0SHVGZzNXWlpxRkhEVUVMX0pSdkRpaGF3bEI4WS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdSblNESVFBU2RRQUFBQSIsImV4cCI6MTYyODk4MTg0NiwiaWF0IjoxNjI4OTc0NjQ2LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.1skClPOZWuEmfK13P62THcixLlyEVbBDFbnEBKFeATE', 1, '2021-08-15 16:00:00', 'Europe/Stockholm', 30),
(107, 9, 38, 17, NULL, NULL, 'stress1630462937.jpg', NULL, 'Intro to Platform', 'Test', 'https://us06web.zoom.us/s/82605480824?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiZ1FSVURSR09XdUdSNmc2Z3pRdjU2bEpwSGlZcmFqLXNpa2RfQWF0YWVSOC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdueW9NRkFBU2RRQUFBQSIsImV4cCI6MTYzMDQ3MDEzOCwiaWF0IjoxNjMwNDYyOTM4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.CAvnFQ-x-1MH9PqyZvztMoOM3O_fexBbMZJBNEwG1RY', 1, '2006-00-00 00:00:00', 'Europe/Stockholm', 30),
(108, 9, 38, 17, NULL, NULL, 'stress1630463014.jpg', NULL, 'Onboarding of Guides', 'This is a test', 'https://us06web.zoom.us/s/89327392713?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoielJQQUlmU09idzhlV19EbXMyVm9jenpZcGZnOGRLLTJhWW1qVlc0TktmNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdueXM1S3dBU2RRQUFBQSIsImV4cCI6MTYzMDQ3MDIxNSwiaWF0IjoxNjMwNDYzMDE1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.BmeqNX6RPW5EaAntLjs4MiGvtml-B3zjKlsnvuVCkmU', 1, '2021-09-01 06:00:00', 'Europe/Stockholm', 30),
(109, 9, 38, 17, NULL, NULL, 'stress1630955948.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/82331269788?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoicHBNSW5KN05LNF9wcGVFVl9KOWxJYkl0aGh3X0xXd2VMTzl3c0R2UE1pVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2SXpMdndBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzE0OCwiaWF0IjoxNjMwOTU1OTQ4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ncHW-fIUJWTLfq3wTAc1hcfdhdUsYbmAMeWVx96zNHE', 2, '2022-00-00 00:00:00', 'Europe/Stockholm', 30),
(110, 9, 38, 17, NULL, NULL, 'stress1630956001.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/84331962860?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWTFFdG5QOGhCRi1DbURieWd0MHFSeVdKdm1jbkNCRkNReUF2cFdjTDMxRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2STJja2dBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzIwMiwiaWF0IjoxNjMwOTU2MDAyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.w_ausJpNhdWN8VcJiQG_8dJoeOnATdqAvIemuCffUIw', 2, '2022-00-00 00:00:00', 'Europe/Stockholm', 30),
(111, 9, 38, 17, NULL, NULL, 'stress1630956072.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/89591381958?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiQ3lxVWkxaHVYdV92QWNpYXVTdmtPZjF2aE02Xzd0Snd5eFk4ZnZ5NGxORS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2STZ4UVFBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzI3MywiaWF0IjoxNjMwOTU2MDczLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.h1yXEnMOiRx2IvxFXNfzgZIhJ7Jt9tRdsOs4eehkZqk', 1, '2021-09-06 22:00:00', 'Europe/Stockholm', 30),
(112, 9, 38, 17, NULL, NULL, 'stress1630959467.jpg', NULL, 'Session with Evolvia', 'Test', 'https://us06web.zoom.us/s/87254955165?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiM3VnbGVZUGJQd3FTamk5QVE2N21uTEZMRHQ3VGpQcWE2d0hxaHF4RmRmQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TUo4NndBU2RRQUFBQSIsImV4cCI6MTYzMDk2NjY2NywiaWF0IjoxNjMwOTU5NDY3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.UQgw-iqmWr0n4afJNAVQtli73CR9EfzdLoITk0qqf1k', 1, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(113, 9, 38, 17, NULL, NULL, 'stress1630959542.jpg', NULL, 'Evolvia Test', 'Test', 'https://us06web.zoom.us/s/88643371788?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiR3hOQzdUV2ZNOHBMSjBGUlFrTWZ4d1l2YUNRUGRFVE8tUWpKNFRwZDhVRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TU9pTGdBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njc0MiwiaWF0IjoxNjMwOTU5NTQyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.xQofqh08G70TVK0BUiRV8C4miGiguCVzDaJMKoCVTVc', 2, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(114, 9, 38, 17, NULL, NULL, 'stress1630959684.jpg', NULL, 'Art Therapy', 'test', 'https://us06web.zoom.us/s/83744067165?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiSktFRF9YcW1GME55cGlrVjMwaFJVdVZsQ25oblBPbHdFaWpaLWVESnlfRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TVhPRHdBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njg4NSwiaWF0IjoxNjMwOTU5Njg1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.oFsUmtyuXRPLT9KvNkY8N0gyikweBY9i_nArPMp6mQA', 2, '0000-00-00 00:00:00', 'Europe/Stockholm', 30),
(115, 9, 38, 17, NULL, NULL, 'stress1630959740.jpg', NULL, 'Onboarding of Guides', 'Test', 'https://us06web.zoom.us/s/88590392491?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoibUx5bzRJMkZKQTFHV09PeDRBQmloWmVJMmtFZy1wRXdoOVhISDlfVF9lWS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TWFuU3dBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njk0MCwiaWF0IjoxNjMwOTU5NzQwLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.sTjym8cdMhjo3xNG2Fyxo0hkb07bxTkfhNgfMa94siE', 2, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(119, 9, 38, 17, NULL, NULL, 'blogger-conducting-online-yoga-class-y27lxr61631192790.jpg', NULL, 'Test Session', 'Test', 'https://us06web.zoom.us/s/85352150503?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoid0RVdmFBSkl3ZHl4azNvTWhNZGYzSU1KTndKY3dsVl9nSE1CUmQzWWk1TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd5cXE0SmdBU2RRQUFBQSIsImV4cCI6MTYzMTE5OTk5MSwiaWF0IjoxNjMxMTkyNzkxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.TA4o4pWtIMLImcqzMl1BqMukLLU4Cm3fVTU290SJlt0', 2, '2021-09-09 15:30:00', 'Europe/Stockholm', 30),
(120, 9, 38, 17, NULL, NULL, 'meditation-fs9suqr1631193026.jpg', NULL, 'Another Test', 'Test', 'https://us06web.zoom.us/s/81263843596?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiemhRc3BySm5JYTFEWjdLVXBYU0pUbWlzeEs2RDZZTWhrRERpSElKSzRSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2SVZuUkFBU2RRQUFBQSIsImV4cCI6MTYzMTcwMDg2MiwiaWF0IjoxNjMxNjkzNjYyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.Qfrd0TNtLQxmdmZa12HjtRAX5C791gQFbHTVwnOe-Bc', 2, '2021-09-15 13:00:00', 'Asia/Kolkata', 30),
(121, 9, 38, 17, NULL, NULL, 'category-connection1631449372.jpg', NULL, 'Art Therapy', 'Test', 'https://us06web.zoom.us/s/82751602454?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiTThyWnRQN0NKTkU2TC1JMmNRaTNzRXRDQUwxSlpuekxUQTZTLVN3VXpGdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjcyZlhaUkFBU2RRQUFBQSIsImV4cCI6MTYzMTQ1NjU3MiwiaWF0IjoxNjMxNDQ5MzcyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0._o_RM_JEHoRI9PFaFQ3QchV0ShGlxosCKwJRsnZJgbM', 2, '2021-09-12 14:30:00', 'Europe/Stockholm', 30),
(122, 9, 38, 17, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1631583089.jpg', NULL, 'Test 09/14', 'Test', 'https://us06web.zoom.us/s/84490839034?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiNkVFcERsN0x5NDdjREZTMklvMUxnMmNPaG02OGt3Y2xGOHpwSDk3NWs4cy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc0ZTQyVmdBU2RRQUFBQSIsImV4cCI6MTYzMTU5MDI5MCwiaWF0IjoxNjMxNTgzMDkwLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.zVPvmDKOl3fqQW3LkYgyHs99M8k1kQUsbKo35HXTGd0', 1, '2021-09-14 06:00:00', 'Europe/Stockholm', 30),
(123, 9, 38, 17, NULL, NULL, 'meditation-fs9suqr1631622770.jpg', NULL, 'Test 09/14 Part 2', 'Test', 'https://us06web.zoom.us/s/89053567217?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWXFfZUVCbXhfV19BaVhPdU9YZHphZ0k2dEQxelIxbVR5a1ZqZUpxb0FzQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RXhhSWdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDAxNCwiaWF0IjoxNjMxNjIyODE0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ZAUdT6K9oVeNM5EBXDZqywo_athUlgT-BcNOXVFRgIU', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(124, 9, 38, 17, NULL, NULL, NULL, NULL, 'Art Therapy', 'Test', 'https://us06web.zoom.us/s/85642676650?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoibnBRNWZudzZZWUVoZTVlc1ZSWmdpRFhKUWxneTlHWEtFSDVISWJWMTY3OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RTBqRXdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDA2NSwiaWF0IjoxNjMxNjIyODY1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.VvrJH-h3jKrLP7aL8qWbp4YHjOi7vrWuqIvBmsqyURY', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(125, 9, 38, 17, NULL, NULL, NULL, NULL, 'Test 09/14 Part 3', 'Test', 'https://us06web.zoom.us/s/83342086340?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiX3ZEVXIycm1jaE1BMWJGQXlQNklGdTFKeFJ1OWNYUEFoZGRMcGtiUzJBQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RTRsWmdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDEzMSwiaWF0IjoxNjMxNjIyOTMxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0._CGp8XBRI0pyDlOLsOn3PRLqu6ijMLBJODNk6i84MoI', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(126, 9, 38, 18, NULL, NULL, '41nn6rihmzl_ac_ul260_sr200260_1631683239.jpg', NULL, 'test1', 'test1', 'https://us06web.zoom.us/s/81374592146?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiclhvY1lYQ0FTSS1LdW9LemNXR29WemthUVZXQTViN015OHFoSlZGV2hzcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2RlA4U0FBU2RRQUFBQSIsImV4cCI6MTYzMTY5NzYyMywiaWF0IjoxNjMxNjkwNDIzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.fvRE1E89qxjzNI3cSLcnO3QhdeBiCjGWJhqaszAEP-s', 1, '2021-09-15 13:00:00', 'Asia/Kolkata', 60),
(127, 9, 38, 17, NULL, NULL, 'r1631694226.jpg', NULL, 'test2', 'test2', 'https://us06web.zoom.us/s/81438935969?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgxNDM4OTM1OTY5Iiwic3RrIjoiekFyWk15S01oRVF4d3ozZlpYUnBtT3o2TW92ekVDc2FGZ0NLZklrOTd1RS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhFUWVhMVFBU2RRQUFBQSIsImV4cCI6MTYzMjM4MDQ4MywiaWF0IjoxNjMyMzczMjgzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.bPmG_L_l5eXM9BtWmUhhVtQHU1e8ZAR0lXywuwA32aA', 2, '2021-09-23 07:00:00', 'Europe/Stockholm', 30),
(128, 9, 38, 17, NULL, NULL, 'oip1631698766.jpg', NULL, 'test3', 'test3', 'https://us06web.zoom.us/s/83654539252?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiLV9HLVp3ZDc1d0JwX2ppVUNWUUQ5WVJWZTM2b1phVHNvSlZydGY2R1VGNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2Tk5PTndBU2RRQUFBQSIsImV4cCI6MTYzMTcwNTk2NywiaWF0IjoxNjMxNjk4NzY3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.c3etGSl7QBwxyO2_T2Yo4cW8gQANZiSXVTKR51kGev0', 1, '2021-09-15 16:30:00', 'Asia/Kolkata', 30),
(130, 9, 38, 17, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1631710214.jpg', NULL, 'Test 09/15', 'This is a test session. Our highest online bonus points offer ever. Earn 50,000 bonus points worth up to $1,250 in Amtrak travel. Must apply here for this offer. Offers vary elsewhere.Our highest online bonus points offer ever. Earn 50,000 bonus points worth up to $1,250 in Amtrak travel. Must apply here for this offer. Offers vary elsewhere.', 'https://us06web.zoom.us/s/86048914099?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiQXU2Sk1POXUyZGNQM2xBdndrWGs2TGtJbEZidTZuSE5VcG1TamRHT3Z2Zy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3bVBTSVFBU2RRQUFBQSIsImV4cCI6MTYzMTc5OTMyNCwiaWF0IjoxNjMxNzkyMTI0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.45Ll99rrZU9rejTayxN9Z1uynp2aET4yGhIJVm71EKw', 2, '2021-09-16 15:00:00', 'Europe/Stockholm', 30),
(131, 9, 38, 17, NULL, NULL, 'online-photography-school-nt5kr6f1631794257.jpg', NULL, 'Test 09/16', 'Test', 'https://us06web.zoom.us/s/87194214754?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiMGNtRV9MWkxiOHV3N1RfMmFCZWRUdVFRT2JtbFU1RjFhRUtwVmxGRl9Uay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3b1JnbmdBU2RRQUFBQSIsImV4cCI6MTYzMTgwMTQ1OCwiaWF0IjoxNjMxNzk0MjU4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.hxP9GwaBN2l7Iej6KN3gwl0VtsIoE6IJAVYDPwr2I_c', 2, '2021-09-16 14:00:00', 'Europe/Stockholm', 30),
(132, 9, 38, 17, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1631795556.jpg', NULL, 'Test 09/16 Part 2', 'Test', 'https://us06web.zoom.us/s/87617802899?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiSTNKcXBqdHpuVkQ1MzRfeFF2ZUZfSXR2ZEM1RUtiRkNkS3ptdVd5NVlXZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3cGd3bEFBU2RRQUFBQSIsImV4cCI6MTYzMTgwMjc1NiwiaWF0IjoxNjMxNzk1NTU2LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ZWP4FjnQ79-UXaZf3HNMqM2fIGnYec-6ETcghnB5Itg', 2, '2021-09-16 15:00:00', 'Europe/Stockholm', 30),
(133, 9, 38, 17, NULL, NULL, 'profile-portrait-of-young-african-online-shop-mana-c2ucmh61631880187.jpg', NULL, 'Test 09/17 ', 'Test', 'https://us06web.zoom.us/s/85680690304?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiOFp1NzRvRExWRDdsU2x4d1lWa0ZIc2Q5WnZ6NkNqc2NFMXVNZHJvWGxBcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc4Nk9RQlFBU2RRQUFBQSIsImV4cCI6MTYzMTg4NzM4NywiaWF0IjoxNjMxODgwMTg3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.MKZrjCYmULwb7KycerS9zAP6gLRMbttmojo6ronRmLU', 2, '2021-09-17 14:00:00', 'Europe/Stockholm', 30),
(137, 3, 1, 0, 'N', 'N', 'reclam-your-flow-sept-81635554629.png', '82678468789', 'Reclaim your flow: Live Session 1', 'The live sessions mix skill building and sharings. They are also designed so that you can experience what the tools and the meditations are inviting you into.', 'https://us06web.zoom.us/s/82678468789?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgyNjc4NDY4Nzg5Iiwic3RrIjoibjhBZGU4UzQwYjVlQjNMWlJIRzdMZGk0YUZSajIxdE5qaENLNWsyQ3pJWS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjh6clg3WkFBU2RRQUFBQSIsImV4cCI6MTYzNTU2MjgwNSwiaWF0IjoxNjM1NTU1NjA1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.VylVd87GvF_CXKzW_WnTaqV7MeROqF8K6rT1daIDh3Y', 2, '2021-09-08 13:05:00', 'Europe/Stockholm', 90),
(138, 3, 1, 0, 'N', 'N', 'iw-back1635555745.png', '86723314118', 'Reclaim your flow: Live Session 2', 'The live sessions mix skill building and sharings. They are also designed so that you can experience what the tools and the meditations are inviting you into.', 'https://us06web.zoom.us/s/86723314118?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg2NzIzMzE0MTE4Iiwic3RrIjoiUUFfNXRrUnpfVWtzVHRYVmVrTFk5aUp4MFQ3WUF6cW9wY2c0d1BYazBQZy5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjh6cmdneFFBU2RRQUFBQSIsImV4cCI6MTYzNTU2Mjk0NSwiaWF0IjoxNjM1NTU1NzQ1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.dCjnXoRE9nZcunHja39uvN7SLRV7kCkCKsgoboHLvTQ', 2, '2021-09-22 13:05:00', 'Europe/Stockholm', 90),
(163, 3, 1, 0, 'N', 'N', 'iw-back1635556311.png', '84005861705', 'Reclaim your flow: Live Session 3', 'The live sessions mix skill building and sharings. They are also designed so that you can experience what the tools and the meditations are inviting you into.', 'https://us06web.zoom.us/s/84005861705?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0MDA1ODYxNzA1Iiwic3RrIjoiWk1DSU01cmhQVWtEbnNjWURDdjd5alNtZldHZjgyY0x2c1l6TlJueGd2WS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjh6c0REZXdBU2RRQUFBQSIsImV4cCI6MTYzNTU2MzUxMSwiaWF0IjoxNjM1NTU2MzExLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.av2pl_CmKzQ7lMQ8MYzTqLeezl__VbSrbtr3C4rEdNU', 2, '2021-10-06 13:05:00', 'Europe/Stockholm', 90),
(153, 3, 55, 0, 'N', 'N', 'laske-class-11635525446.png', '89932379068', 'Class 1', 'This is class 1 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/89932379068?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg5OTMyMzc5MDY4Iiwic3RrIjoia0Y5bDFHTjU3dFhLTEJvODlhdnA2YjJZRVZ3bWxWSlJnUWQ5U3hwcDlIay5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNenJJWjRBRW5VQUFBQSIsImV4cCI6MTYzNTUzMjczNCwiaWF0IjoxNjM1NTI1NTM0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.s9c6r1ADJlD5iRpAa8dzCuP2NZuocaHClGugD2bNk_0', 2, '2021-11-02 20:00:00', 'Europe/Stockholm', 120),
(167, 3, 1, 0, 'N', 'N', 'meditation-fs9suqr1636492100.jpg', '82778324728', 'Test Session', 'Test', 'https://us06web.zoom.us/s/82778324728?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgyNzc4MzI0NzI4Iiwic3RrIjoick9Ya2V4aVJWLWZkVVRXLWNlWEJQNFJhTkxnMl84TXdDSjN3elBMekRLWS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjlCb2ZIRkFBU2RRQUFBQSIsImV4cCI6MTYzNjQ5OTMwMSwiaWF0IjoxNjM2NDkyMTAxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.FZ0Xqt3JLXt8ibjgWhaNFK0cvstEZ0AYv7KNJdG9ghk', 2, '2021-11-09 15:30:00', 'Europe/Stockholm', 30),
(154, 3, 55, 0, 'N', 'N', 'laske-class-21635526570.png', '84294039346', 'Class 2', 'This is class 2 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/84294039346?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0Mjk0MDM5MzQ2Iiwic3RrIjoiOU1MRzN1eUpsdjlkNkQ5VlhyeHBwUVh3TC05RVhHWEpSR3dNYTJQME5lYy5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNejdXYkVBRW5VQUFBQSIsImV4cCI6MTYzNTUzMzc5NywiaWF0IjoxNjM1NTI2NTk3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.9DAbS5Qk38qxBbNI_xTfw2S9gvM2MLiPSF_8hVVWXoo', 2, '2021-11-08 20:00:00', 'Europe/Stockholm', 120),
(155, 3, 55, 0, 'N', 'N', 'laske-class-31635526767.png', '81935897183', 'Class 3', 'This is class 3 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/81935897183?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgxOTM1ODk3MTgzIiwic3RrIjoiUndYcDZPRS1qWHY5STJKdUNlNDdOMGg5c2VpbVd5U2tKMU1oQzRETkR3MC5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNejk5eEFBRW5VQUFBQSIsImV4cCI6MTYzNTUzMzk2OCwiaWF0IjoxNjM1NTI2NzY4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.I_9jxIXbQ_06nRuh91z6Raac-3Ttn3K7zYr9qfFDG5U', 2, '2021-11-16 20:00:00', 'Europe/Stockholm', 30),
(156, 3, 55, 0, 'N', 'N', 'laske-class-41635526797.png', '83672216904', 'Class 4', 'This is class 4 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/83672216904?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgzNjcyMjE2OTA0Iiwic3RrIjoiZm8xRkNqS2V5NFVUSUFyMUJmbW95NUwwbGwtMjhlQUVsdVZHUnA1R0c0by5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMEEyYjhBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDE1NywiaWF0IjoxNjM1NTI2OTU3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.X9o_HmR6IxLxVVO3KlrTYzcMv6NnPrhhHlSe9XtINoo', 2, '2021-11-18 20:00:00', 'Europe/Stockholm', 120),
(157, 3, 55, 0, 'N', 'N', 'laske-class-51635526947.png', '86048104363', 'Class 5', 'This is class 5 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/86048104363?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg2MDQ4MTA0MzYzIiwic3RrIjoiTko1S0JYR1k5RWdBTzdETC1kSVBjekJ3WnhkYVJ2enp1cFI5enpmQ0xSdy5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMEFzOGNBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDE0NywiaWF0IjoxNjM1NTI2OTQ3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.XIDiRxjsGDydnlVN9ySl3Jp9I0bpSQGMZhrfo03r_5k', 2, '2021-11-30 20:00:00', 'Europe/Stockholm', 120),
(158, 3, 55, 0, 'N', 'N', 'laske-class-61635527052.png', '89647803179', 'Class 6', 'This is class 6 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/89647803179?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg5NjQ3ODAzMTc5Iiwic3RrIjoiaUhIRnFMU3ctT3ZFbkY0QmcxWHF2NVpkZi1aUFN3X3J1WG9rSkU1cDI0TS5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMENVQk1BRW5VQUFBQSIsImV4cCI6MTYzNTUzNDI1MywiaWF0IjoxNjM1NTI3MDUzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.pHHslpLAzuzK6p4xqmoPVDI2WypFVY5mUPzCvPANqTc', 2, '2021-12-02 20:00:00', 'Europe/Stockholm', 120),
(159, 3, 55, 0, 'N', 'N', 'laske-class-71635527093.png', '85401808992', 'Class 7', 'This is class 7 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/85401808992?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg1NDAxODA4OTkyIiwic3RrIjoiUWtIQUtfZ09RR19ERklraUVGaHMtWmRXbTljS1BNTUlIU3VieFlRcWtQZy5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMEM3ZVVBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDI5MywiaWF0IjoxNjM1NTI3MDkzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.qsRp5g8qiDfeSnhqsZhWG2GTOtgFK6HAw-Bhu1HuNrQ', 2, '2021-12-07 20:00:00', 'Europe/Stockholm', 120),
(160, 3, 55, 0, 'N', 'N', 'laske-class-81635527122.png', '82166042166', 'Class 8', 'This is class 8 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/82166042166?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgyMTY2MDQyMTY2Iiwic3RrIjoidmZmTHphOUlqWnJHaENhc3ZOd3ExdTYyNUVkZFgxNGRkUFFxeENjMzJxay5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMERZQ2NBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDMyMiwiaWF0IjoxNjM1NTI3MTIyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.DQ3KQ2LZONtLAx5AjjzsuYiTxugmfI4u80KF842owVE', 2, '2021-12-09 20:00:00', 'Europe/Stockholm', 120),
(161, 3, 55, 0, 'N', 'N', 'laske-class-91635527165.png', '87355894274', 'Class 9', 'This is class 9 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/87355894274?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg3MzU1ODk0Mjc0Iiwic3RrIjoiRU82VjRZbWxZOVlnYldhbTU1ZGczaUh2NU4tckh4eGtvb2hOY1l2emVTTS5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMEVCMElBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDM2NSwiaWF0IjoxNjM1NTI3MTY1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.43Wn7n8WyYFO-dxa4HTii54dO1Ko2tw0HVbHphoLu_M', 2, '2021-12-14 20:00:00', 'Europe/Stockholm', 120),
(162, 3, 55, 0, 'N', 'N', 'laske-class-101635527195.png', '84201772220', 'Class 10', 'This is class 10 in our program for assessing development test for cognitive and social emotional development. The session will open 5 minutes before start time of the class. If you have any technical issues, please email Roman at roman@innerworks.io for support, who will be on standby during the class to ensure all sessions run as scheduled.', 'https://us06web.zoom.us/s/84201772220?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IlhzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0MjAxNzcyMjIwIiwic3RrIjoiRHhmZTZIVDRpbmlkMkFucXI2LVBQbHZXVXRnWEtXZkMzR1VCd3l1ZVliNC5CZ1lnZURKcEx6bDVXbk5uVW1JeFdIZDRaVTlRVG0xTE0zVllNQzlwVmpGVWR6WkFNRGxtT0dVM09UZ3hZbU0zWVRkaE9UZzJabVl5TURJMFlUWmtaVFEzTVRFNU5URXpZbUl4TVRZNFpqSXhPRFF6TldVNVpqTm1OVGs1TW1Fd01EVTJaUUFNY21Wc2NIaFhMMVZEWjBFOUFBUjFjekEyQUFBQmZNMEVmTUVBRW5VQUFBQSIsImV4cCI6MTYzNTUzNDM5NSwiaWF0IjoxNjM1NTI3MTk1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.WQWuhwA4RgxyH7-xs8P9NfNbyOHMOEd1FpEIjjmHJlE', 2, '2021-12-16 20:00:00', 'Europe/Stockholm', 120);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `callscircle`
--

CREATE TABLE `callscircle` (
  `callCircleId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `callscircle`
--

INSERT INTO `callscircle` (`callCircleId`, `callId`, `circleId`) VALUES
(4, 140, 28),
(7, 144, 28),
(8, 146, 28),
(35, 147, 36),
(11, 148, 37),
(13, 149, 37),
(14, 150, 38),
(34, 138, 36),
(16, 151, 33),
(17, 152, 38),
(19, 153, 37),
(20, 154, 37),
(21, 155, 37),
(24, 156, 37),
(23, 157, 37),
(25, 158, 37),
(26, 159, 37),
(27, 160, 37),
(28, 161, 37),
(29, 162, 37),
(33, 137, 36),
(36, 163, 36),
(37, 164, 37),
(38, 165, 37),
(39, 166, 37),
(40, 167, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfil`
--

CREATE TABLE `cambioperfil` (
  `cambioId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfilmiembro`
--

CREATE TABLE `cambioperfilmiembro` (
  `cambioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `descripcion` varchar(1000) NOT NULL,
  `imagen` varchar(200) NOT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`categoryId`, `clientId`, `nombre`, `descripcion`, `imagen`) VALUES
(1, 3, 'Breathwork', 'Learn breathwork exercises that calm your mind, balance your nervous system and move you into a state of creativity and productivity', 'category-breathwork.jpg'),
(2, 3, 'Meditation', 'Practice meditation exercises that cultivate a state of equanimity and openness, bringing you in alignment with others and closer to your purpose', 'category-meditation.jpg'),
(3, 3, 'Psychotherapy', 'Consult with us and connect to your true authentic self through personal healing, focused and results oriented psychotherapy. ', 'category-psychotherapy.jpg'),
(4, 3, 'Connection', 'Discover the tools that help you relate, empathize and build trust in relationships so that share and co-create boldly and freely', 'category-connection.jpg'),
(5, 4, 'Purpose Approach', 'Children discover their individuality and explore what they want for themselves and the world through this series of classes and workshops.', 'challenge-mission-and-achievement-a-little-girl-is-v9dvn29.jpg'),
(6, 4, 'Passion based learning', 'We work with children to identify their interests and curate personalized exercises that lead them to seek learning out of passion. ', 'reading-is-her-big-passion-smp242b.jpg'),
(7, 4, 'Self-Efficacy', 'Self-efficacy classes develop the self-management skills that enable children to become resourceful, persistent and confident.', 'father-with-small-children-working-outdoors-in-gar-kkd5msl.jpg'),
(8, 4, 'Academics', 'Our academics classes explores art and culture and connects them with science, language, math and social studies taught at school.', 'lovely-little-girl-is-dancing-in-headphones-tqsuaje.jpg'),
(9, 7, 'Integral therapy', 'Gestalt, Integral and Transpersonal Psychotherapy, Naturopathic medicine, Therapeutic Massages, Horticultural therapy, Drug Addiction Treatment & Harm Reduction', 'cueva-venados-1-small.jpg'),
(11, 7, 'Specialized Workshops', 'Yoga, Meditation, Breathwork, Dreamwork, Family Constellation, Permaculture & Farming, Team Building and Art Therapy', 'yoga-terrace-space-2-small.jpg'),
(10, 7, 'Indigenous medicine', 'Traditional medicinal plants, Medicinal Herbs, Temazcal / Sweatlodge, Healing ceremonies, Medicine Wheel, Diets and Detox', 'entrada-fmm-003_temazcal.jpg'),
(12, 7, 'Consultations', 'Individual psychotherapy sessions for depression, anxiety, addiction, eating disorders, PTSD, existential crisis, difficult grief issues and life transitions', 'nierika-int-_img_5866.jpg'),
(20, 9, 'Business Development', 'Develop a business plan, find the right talent, build a marketing funnel, scale users, monetize your service, reach beyond digital and realize your vision!', 'tasks-for-business-development-8v2bzghjpg.jpg'),
(19, 9, 'Brand and Design', 'Build brand strategy and design language, connect them with how you think, speak, act and appear, and merge online with offline experiences ', 'hand-holding-notebook-with-drew-brand-logo-creativ-p8htw8w.jpg'),
(18, 9, 'Content Creation', 'Write compelling content, shoot your first video, speak confidently in front of the camera, create explainer videos and manage your audience on Zoom!', 'talk-show-at-online-radio-station-9fmcxz4.jpg'),
(17, 9, 'Platform Setup', 'Set up your platform, customize your content, administer your users, start LIVE programming and publish your website with your own domain name!', 'software-engineers-working-on-project-and-programm-xzp2tp7.jpg'),
(21, 8, 'Test', 'This a category test', 'video-final.gif');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlecategories`
--

CREATE TABLE `circlecategories` (
  `ccId` int(11) NOT NULL,
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circlecategories`
--

INSERT INTO `circlecategories` (`ccId`, `circleId`, `categoryId`) VALUES
(46, 27, 2),
(69, 28, 1),
(47, 29, 4),
(94, 30, 2),
(155, 31, 4),
(125, 32, 4),
(124, 32, 3),
(123, 32, 2),
(122, 32, 1),
(140, 33, 4),
(154, 31, 3),
(153, 31, 2),
(152, 31, 1),
(100, 34, 4),
(113, 35, 4),
(114, 36, 4),
(143, 37, 4),
(137, 38, 4),
(158, 39, 4),
(159, 40, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlecoachrequest`
--

CREATE TABLE `circlecoachrequest` (
  `requestCircleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlerequest`
--

CREATE TABLE `circlerequest` (
  `requestCircleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circlerequest`
--

INSERT INTO `circlerequest` (`requestCircleId`, `userId`, `circleId`) VALUES
(17, 73, 37),
(18, 72, 38),
(21, 9, 33),
(22, 84, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circles`
--

CREATE TABLE `circles` (
  `circleId` int(11) NOT NULL COMMENT 'multiple=users,usercircle;multiple=coaches,coachcircle;multiple=categories,circlecategories;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `public` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circles`
--

INSERT INTO `circles` (`circleId`, `clientId`, `coachId`, `parentId`, `public`, `nombre`, `description`, `photo`, `video`) VALUES
(30, 3, 6, 31, 'S', 'Experiencing You', 'This circle is for you who want to keep meditation as an essential part of your life to use as a tool to come back to a relaxed body, clear mind and a loving relationship to life. \n\nIt is a place to get or give support in making the meditation practice happen, share and inspire each other and also to give feedback to the online meditations so I get a feel for what is happening in you.', 'experiencing-you-thumbnail1635005625.jpg', NULL),
(31, 3, 1, 0, 'S', 'Innerworks Academy', 'We’re a community of changemakers and coaches\nfrom around the world, facilitating customised\nprograms for individual thriving, team effectiveness and organisational resilience. Our passion is supporting thriving in the face of increasing complexity, uncertainty and stress. Innerworks offers a holistic approach for facilitating individual thriving, team effectiveness and organisational resilience. ', 'iw-thumbnail21635006840.png', NULL),
(38, 3, 55, 33, 'S', 'Action Learning', 'Action Learning is appropriate when you have to deal with ill-defined complex issues. It\nwould not just be enough to get an expert or a plumber in to do the job. Someone needs to\nmake hard decisions where no one can tell if there is a good solution at all, or there are\nmany, too many options.\nIt is essential that at least one person in the room deeply cares about the issue at hand and\ncan do at least something about it. Action learning deals with real practical issues, not just ‘theory’.', 'hand-holding-notebook-with-drew-brand-logo-creativ-p8htw8w1635302138.jpg', NULL),
(33, 3, 55, 0, 'S', 'Fraendi', 'Frændi is a group of consulting professionals, thinkers, educators, doers, and optimists who are committed to the practice of unlocking potential—in people, teams, systems, and organizations.\n\nOur goal is to instill confidence in individuals, leaders, governments, and organizations and together help create a roadmap to a prosperous, planet-sustaining future.\n\nIn a world that faces unprecedented challenges from social justice to environmental degradation, Frændi is working with leaders who are moving their organizations forward with clarity, confidence, empathy, agility, and resilience.', 'fraendi-logo21635210562.png', NULL),
(34, 3, 1, 31, 'S', 'The Loop', 'The Loop introduces you to the core pattern of how change plays out in life, how to master the change process and manifest the change you need. Explore who you are in this moment of your life, and find the starting point of your current transformation loop and explore further in this series of 11 videos', 'loop-thumbnail1635006687.png', NULL),
(36, 3, 1, 35, 'S', 'Reclaim your flow', 'The program consist of 3, 2 week modules: Self-understanding, Self-grounding and Self-leadership. \n\nEach model will consist of 3 different types of sessions: \n- Meditation: Guided meditations that will begin building your capacity to notice what that is already happening in you all the time. Research has shown that meditation increases wellbeing and creativity if done regularly.\n- Tools: A short lesson on something that will build your capacity or an exercise for you to engage in. When you engage in these you will improve your skills or clarity so that you can are better equipped to deal with for instance stress or work relationships.\n- Live sessions: The live sessions are live online via zoom. They will mix skill building and sharings. They are also designed so that you can experience what the tools and the meditations are inviting you into.\n\nThe program will run for 6 weeks from the 30th of Aug.  Live sessions will be every other Wednesday. Please do what you can to be there. Meditations and Tools will be made available to you to engage with when it suits your schedule.', 'reclaim-your-flow-thumbnail1635008296.jpg', NULL),
(35, 3, 1, 31, 'S', 'Sircle Collection', 'This is an user circle for Sircle Collection - A collection of creative hospitality brands that are located in Europe\'s most inspiring neighborhoods.', 'sircle-thumbnail1635006977.jpg', NULL),
(37, 3, 55, 33, 'N', 'Assessing developmental text for cognitive and social emotional development', 'The ability to look beyond the text, beyond the words to identify the underlying thinking and meaning-making is the focus of this training. Participants learn to apply Otto Laske’s developmental theory by working with the text produced through a developmental interview and learn how the results are translated into feedback conversations and reports. ', 'hiking-blog1635210897.jpeg', NULL),
(39, 3, 35, 0, 'S', 'Ephata', 'EPHATA is a program offering supplementary courses, mentorship and workshops for parents and children. \nIt is a movement, philosophy, life flow, to be emotionally, spiritually and socially equipped to process challenges in adequate, pertinent, cognitive, congruent, aware ways to successfully navigate the present while investing for tomorrow\n', 'ephata-circle-pic-21635558720.jpg', NULL),
(40, 3, 35, 39, 'S', 'Ephata Workshops', 'This is a series of workshops for 6 to 16 year olds to put the 7 pillars of Ephata to practice. To enroll for the workshops and find out more, email Valeria at valeriapayton@gmail.com', 'environmental-awareness1635560708.jpg', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circletodos`
--

CREATE TABLE `circletodos` (
  `id` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT ' opciones=coaches',
  `circleId` int(11) NOT NULL COMMENT ' opciones=circles',
  `videoId` int(11) DEFAULT NULL COMMENT 'opciones=videos',
  `audioId` int(11) DEFAULT NULL COMMENT 'opciones=audios',
  `articleId` int(11) DEFAULT NULL COMMENT 'opciones=articles',
  `pluginContentId` int(11) DEFAULT NULL COMMENT 'opciones=plugincontent',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `circletodos`
--

INSERT INTO `circletodos` (`id`, `clientId`, `coachId`, `circleId`, `videoId`, `audioId`, `articleId`, `pluginContentId`, `status`, `datetime`) VALUES
(16, 3, 1, 31, 59, NULL, NULL, NULL, 0, '2021-11-18 05:00:00'),
(18, 3, 1, 31, 65, NULL, NULL, NULL, 0, '2021-11-29 01:01:00'),
(19, 3, 1, 36, 52, NULL, NULL, NULL, 0, '2021-11-25 09:00:00'),
(21, 3, 1, 31, 66, NULL, NULL, NULL, 0, '2021-11-25 09:00:00'),
(22, 3, 1, 36, 60, NULL, NULL, NULL, 0, '2021-12-16 17:50:00'),
(23, 3, 1, 36, NULL, NULL, NULL, 12, 0, '2022-01-12 17:56:47'),
(24, 3, 55, 38, NULL, NULL, NULL, 13, 0, '2022-01-14 14:19:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientpages`
--

CREATE TABLE `clientpages` (
  `clientPageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pageId` int(11) NOT NULL COMMENT 'opciones=pages'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientpages`
--

INSERT INTO `clientpages` (`clientPageId`, `clientId`, `pageId`) VALUES
(104, 9, 5),
(99, 3, 2),
(9, 1, 1),
(10, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 2, 1),
(14, 2, 2),
(15, 2, 3),
(16, 2, 4),
(17, 4, 1),
(18, 4, 2),
(19, 4, 3),
(20, 4, 4),
(21, 7, 1),
(22, 7, 2),
(23, 7, 3),
(24, 7, 4),
(62, 8, 5),
(61, 8, 4),
(60, 8, 3),
(59, 8, 2),
(103, 9, 4),
(102, 9, 3),
(101, 9, 2),
(100, 9, 1),
(40, 10, 4),
(39, 10, 3),
(38, 10, 2),
(37, 10, 1),
(58, 8, 1),
(63, 11, 1),
(64, 11, 2),
(65, 11, 3),
(66, 11, 4),
(67, 12, 2),
(68, 12, 3),
(69, 12, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `clientId` int(11) NOT NULL COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;',
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `accessPassword` varchar(200) DEFAULT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `memberQuestionnaire` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `zoomPublic` varchar(200) DEFAULT NULL,
  `zoomSecret` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`clientId`, `platformId`, `nombre`, `accessPassword`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `pass`, `memberQuestionnaire`, `zoomPublic`, `zoomSecret`) VALUES
(1, 1, 'Shopify', NULL, 'logo-inner.png', 'logo-inner.png', NULL, NULL, 'archive', 'client@test.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(2, 2, 'EphataClient', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'clients', 'client@ephata.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(3, 1, 'Innerworks', NULL, 'innerworks_logo_5-innerworks-university.png', 'innerworks_logo_black-innerworks-university.png', NULL, NULL, 'innerworks', 'roman@romanf.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'N', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(4, 2, 'Ephata University', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'ephata', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(7, 3, 'Nierika', NULL, 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', NULL, NULL, 'nierika', 'universidad@nierika.com', 'dmlHaXNmUTN6bVZreEpHMURQQkxCYnhRUEJNTjEyRG51Z3NybEtPaC81Yz0=', 'S', NULL, NULL),
(8, 4, 'Test University', 'testpassword', 'logo-azulik-black.png', 'zenrise_logo_03-test-university.png', NULL, NULL, 'test', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(9, 5, 'Shala', NULL, 'shala-new-logo-shala.png', 'shala-new-icon-shala.png', NULL, NULL, 'shala', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'N', NULL, NULL),
(10, 6, 'Eleutheria', NULL, 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', NULL, 'lexendmega-regular.ttf', 'eleutheria', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coacharticle`
--

CREATE TABLE `coacharticle` (
  `coachArticleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coacharticle`
--

INSERT INTO `coacharticle` (`coachArticleId`, `coachId`, `articleId`) VALUES
(2, 35, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coacharticleserie`
--

CREATE TABLE `coacharticleserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coacharticleserie`
--

INSERT INTO `coacharticleserie` (`coachSerieId`, `coachId`, `serieId`) VALUES
(3, 44, 3),
(9, 52, 1),
(8, 44, 1),
(7, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachaudio`
--

CREATE TABLE `coachaudio` (
  `coachAudioId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachcall`
--

CREATE TABLE `coachcall` (
  `coachCallId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `callId` int(11) NOT NULL COMMENT 'opciones=calls'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachcall`
--

INSERT INTO `coachcall` (`coachCallId`, `coachId`, `callId`) VALUES
(6, 44, 86),
(5, 6, 85),
(4, 44, 81),
(8, 6, 83),
(9, 6, 91),
(10, 6, 94),
(12, 1, 106),
(111, 38, 110),
(16, 38, 119),
(19, 38, 120),
(109, 44, 134),
(108, 1, 134),
(27, 44, 128),
(141, 6, 139),
(117, 52, 135),
(110, 52, 134),
(116, 44, 135),
(115, 1, 135),
(121, 44, 136),
(120, 1, 136),
(122, 52, 136),
(173, 52, 137),
(172, 44, 137),
(171, 6, 137),
(170, 1, 137),
(157, 55, 153),
(174, 1, 138),
(153, 55, 149),
(152, 1, 143),
(175, 6, 138),
(176, 1, 147),
(177, 6, 147),
(178, 1, 163),
(179, 6, 163),
(158, 55, 154),
(159, 55, 155),
(160, 55, 156),
(161, 55, 157),
(162, 55, 158),
(163, 55, 159),
(164, 55, 160),
(165, 55, 161),
(166, 55, 162),
(167, 55, 164),
(180, 55, 166);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachcircle`
--

CREATE TABLE `coachcircle` (
  `coachCircleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachcircle`
--

INSERT INTO `coachcircle` (`coachCircleId`, `coachId`, `circleId`) VALUES
(44, 44, 30),
(105, 35, 40),
(104, 35, 39),
(100, 6, 31),
(99, 1, 31),
(76, 6, 32),
(43, 6, 30),
(75, 1, 32),
(89, 55, 33),
(69, 6, 35),
(68, 1, 35),
(72, 6, 36),
(71, 1, 36),
(85, 55, 38),
(92, 55, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coaches`
--

CREATE TABLE `coaches` (
  `coachId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `phrase` varchar(1000) DEFAULT NULL,
  `biografia` varchar(1000) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `header` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `facebook` varchar(200) DEFAULT NULL COMMENT 'liga',
  `twitter` varchar(200) DEFAULT NULL COMMENT 'liga',
  `instagram` varchar(200) DEFAULT NULL COMMENT 'liga',
  `linkedin` varchar(200) DEFAULT NULL COMMENT 'liga',
  `web` varchar(200) DEFAULT NULL COMMENT 'liga',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `showHome` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `zoomAuth` varchar(200) DEFAULT NULL,
  `zoomToken` varchar(2000) DEFAULT NULL,
  `zoomRefresh` varchar(2000) DEFAULT NULL,
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coaches`
--

INSERT INTO `coaches` (`coachId`, `statusId`, `nombre`, `apellido`, `phrase`, `biografia`, `foto`, `header`, `video`, `facebook`, `twitter`, `instagram`, `linkedin`, `web`, `email`, `pass`, `showHome`, `emailConfirmed`, `zoomAuth`, `zoomToken`, `zoomRefresh`, `fecha`) VALUES
(1, 2, 'Nils', 'von Heijne', 'Holistic coach', 'Nils is a holistic coach, advisor, shaman and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker, energy worker and sound healer. More info can be found at nilsvonheijne.com.', 'dsc0864116202183561951631050233.jpeg', 'frank-mckenna-od9eozfsoh0-unsplash1630072029.jpg', 'MTA3MTY3NA==', 'https://www.facebook.com/nilsvonheijne', 'https://twitter.com/nilsvonheijne', 'https://www.instagram.com/nilsvonheijne/', 'https://www.linkedin.com/in/nilsvonheijne/', 'https://nilsvonheijne.com', 'nils@innerworks.io', 'TnlwYUZVV01lNEUxTjRCNDNZT3dHQT09', 'S', 'S', NULL, NULL, NULL, '2021-09-07'),
(2, 1, 'Roshi', 'Derakshan', NULL, 'Licensed Psychotherapist and dedicated to helping people connect to their true authentic self through personal healing, focused and results oriented psychotherapy. Licenced Life coach and work as a ”hybrid” where you get the best of a therapist and coach to break free from what’s holding you back to push towards where you want to be.', 'roshi-2png.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rome7f-deactivated-2021-04-22@gmail.com', 'VjJxelJkZC9VSUIwK0kyTHo4QUZyUT09', 'N', 'S', NULL, NULL, NULL, '2021-03-23'),
(5, 2, 'Jessika', 'Klingspor', NULL, 'Jessika is Swedish entrepreneur and business owner living in Barcelona since 2001. She is currently collaborating with the Barcelona City Council building new alliances between The Nordic\r\nCountries and Barcelona within the sector of innovation. She is co-creator within the Innerworks Community with aim to bridge the gap between the corporate world and the spiritual world. Planning to launch Innerworks Barcelona and World of Wisdom Barcelona 2021.', 'jessklingspor1.jpg', NULL, NULL, 'null', 'null', 'null', 'null', 'null', 'Jessika.klingspor@nordicsinbarcelona.com', 'WWY5STRKWUNnc0t1L1kvWjF0NG1QZz09', 'S', 'S', NULL, NULL, NULL, '2021-04-22'),
(38, 2, 'Roman', 'Fernandez', NULL, 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'linkedin.jpg', 'tlm120720kg-112-small.jpg', 'MTE1Njg0NA==', 'https://www.facebook.com/romanspade/', NULL, NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'roman@romanf.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, '2021-03-20'),
(4, 2, 'Nils', 'von Heijne', NULL, 'Nils is a holistic coach, advisor and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker and sound healer.', 'nilspng.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nils@wishful.se', 'UnZQaGdVYUdqdVBDTlYxS0JhUVhiUT09', 'S', 'S', NULL, NULL, NULL, '2021-01-17'),
(6, 2, 'Mattis', 'Norrvidd', 'Meditation coach', 'Mattis is a heartdriven environmental engineer who supports individuals, groups and companies to thrive through courses in self leadership, inspirational talks, meditations and coaching. He uses curiosity and clearity of mind to develop inner and outer connection.', 'mattis.png', 'giuseppe-peletti-mgg1xks9tjo-unsplash1630575886.jpg', NULL, 'https://www.facebook.com/mattis.hansson.1', NULL, NULL, NULL, 'https://insighttimer.com/mattis.norrvidd', 'mattis@comecloser.nu', 'cG5oOTNKb1ZZOGlHbnN2d3ZQUXJSQT09', 'S', 'S', NULL, NULL, NULL, '2021-09-02'),
(7, 2, 'Therese', 'Lyander', NULL, 'Therese is a transformational life coach, space holder and emotional guide. In her toolbox you will find\ndetox, fasting, breathwork, trauma therapy, meditation, creative feminine leadership and inspiration', 'teresa.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'therese@innerworks.io', 'elV5VzRzYnYreGVmQXo0OGJROFlIdz09', 'S', 'S', NULL, NULL, NULL, '2021-01-18'),
(8, 2, 'Johan', 'Reunanen', NULL, 'Johan is a coach, business advisor and changemaker. Besides being a partner at Scandinavian managament consultancy, Cordial, focusing on sustainable transformation, Johan shares his message of sobriety and loving leadership through talks, podcasts and writing', 'johan-r-1.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'johan@innerworks.io', 'WGJ6aiswWW5IMmhSR1o0Rzh2aTFCUT09', 'S', 'S', NULL, NULL, NULL, '2021-01-18'),
(9, 2, 'Faviana', 'Vangelius', NULL, 'Within the InnerWorks community Faviana does Sound Healing and Reiki. Beyond being one of InnerWorks co-founders, she also does Community Building and Explorer in Chief (Experience Manager).', 'faviana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'faviana@innerworks.io', 'WHc4UGNrckIyZTFrbWlJMFdsdFlTUT09', 'S', 'S', NULL, NULL, NULL, '2021-04-23'),
(10, 2, 'Daniel', 'Mueller-Gonzalez', NULL, 'Daniel is a Wim Hof Method instructor, breath guide and human evolution coach based in Stockholm.', 'daniel.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hello@danielmueller.se', 'QUtQTU56RGltQUZ4WWhnS0FUbExzUT09', 'S', 'S', NULL, NULL, NULL, '2021-04-22'),
(11, 2, 'Alexander', 'Holmberg', NULL, 'Alexander is a personal coach in Stockholm.', 'holmberg.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alexander@innerworks.io', 'S296aXBoNk9uNVR6NjlKZTVMWm1iQT09', 'S', 'S', NULL, NULL, NULL, '2021-01-18'),
(12, 2, 'Fredrik', 'Edlund', NULL, 'Fredrik Edlund is a yoga and meditation teacher based in Stockholm.', 'edlund1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fredrik@innerworks.io', 'Z2sxWmRJVDFjNVNvK3d1QjJkbm4rZz09', 'S', 'S', NULL, NULL, NULL, '2021-01-18'),
(13, 2, 'Kim', 'Gajraj', NULL, 'Kim is a holistic voice therapist based in Stockholm.', 'gajraj.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kim@kimgajraj.com', 'RzJySUZEUWFod3M2UCtuc1ZoZkxMQT09', 'S', 'S', NULL, NULL, NULL, '2021-04-22'),
(18, 2, 'Valeria', 'Payton', NULL, 'Valeria Payton is the founder of Tulum International School and the co-founder of Ephata.', 'valeria_profile_square2.jpg', NULL, 'MTA2OTkzMQ==', NULL, NULL, NULL, NULL, NULL, 'valeria@ephata.me', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', NULL, NULL, NULL, '2021-01-29'),
(29, 2, 'Eduardo Raymundo', 'Villagran', 'Personal Phrase Hello', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'eduardo.raymundo8787@gmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', 'S', NULL, NULL, NULL, '2021-03-30'),
(32, 2, 'Anja', 'Loizaga-Velder, PhD', NULL, 'Anja Katharina Loizaga-Velder is a German-Mexican clinical psychologist and psychotherapist, who has been investigating the therapeutic potential of the ritual use of psychedelic plants for over 25 years, in collaboration with indigenous healers. She earned a PhD degree in Medical Psychology from Heidelberg University in Germany with a doctoral dissertation on: The therapeutic uses of ayahuasca in addiction treatment. She is a founding member and director of research and psychotherapy of the Nierika Institute for Intercultural Medicine in Mexico and is an adjunct professor and researcher at the National Autonomous University of Mexico, where she researches the therapeutic potential of psychedelics in intercultural therapeutic settings. Additionally, she works as a psychotherapist with humanistic and transpersonal orientation in private practice.', 'anja_profilepng.png', 'anja_cover2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'nierika@gmx.net', 'c0JJbHcyaHllazMwdFNsQmxBVXdCdz09', 'S', 'S', NULL, NULL, NULL, '2021-03-04'),
(33, 2, 'Armando', 'Loizaga', NULL, 'Armando Loizaga is the President of Nierika AC, a non-profit NGO based in Mexico. He has dedicated over 20 years to the study of addictions and sacred plants. The Peyote Conservation Project works hand in hand with the Wixarika and lays out a strategy to reconsider peyote as a cultural and spiritual patrimony of indigenous peoples that calls for a sustainable development approach to sacred plants and tending to the natural environments where they grow. His work currently centers on advancing clinical research protocols that employ sacred plants and participating closely in traditional uses and psychedelic drug policy discussions with different levels of Mexican government and international agencies.', 'armando-loizaga_profile.jpg', 'armando_coverpng.png', NULL, NULL, NULL, NULL, NULL, NULL, 'armando@centronierika.net', 'TDBXTGNvYWRxNld4TFNrYjVDL2h4Zz09', 'S', 'S', NULL, NULL, NULL, '2021-03-04'),
(34, 2, 'Hannah', 'Rapp', NULL, 'My name is Hannah Rapp. I am a Licensed Mental Health Counselor (LMHC) and Creative Healing Arts Therapist with a passion for utilizing creativity and nature to embody our wildest authentic expressions and healing. My path and life\'s work emerged from a deep exploration of my emotional body, wild adventures, free-form movement and expansive transformation. It is my greatest joy and calling to be witness to the human experience.\n \nAs a therapist I help individuals to remember their purpose, connect to and trust their intuition, listen to the wisdom of their body, identify and understand their feelings, and live authentically. Through creativity, ceremony, movement and mindfulness I guide individuals to express themselves fully, to create a loving relationship to themselves and the Earth, and to nurture their inner child.   ', 'hannah_profile2.jpg', 'hannah_cover.webp', NULL, 'https://www.facebook.com/hannah.rapp.19', NULL, NULL, NULL, 'https://www.playfulpassages.com/', 'hannah@centronierika.net', 'a1FZWWhiZ05jVEczb2xEK05TNlJEZz09', 'S', 'S', NULL, NULL, NULL, '2021-03-04'),
(35, 2, 'Valeria', 'Payton', 'Child Learning Expert', 'Valeria is the founder of Ephata and a passionate educator of several years. ', 'valeria_profile_square2.jpg', 'ephata-circle-pic1635558445.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'valeriapayton@gmail.com', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', NULL, NULL, NULL, '2021-10-29'),
(36, 2, 'Sanja Guide', 'Exanimo', NULL, 'jksdjkfsdjk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 's@exanimo.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', 'S', NULL, NULL, NULL, '2021-03-17'),
(37, 2, 'Roshi', 'Derakshan', NULL, 'I am a licenced Psychotherapist and Life Coach, with 10+ years of experience in treating disorders such as depression, burnout, anxiety, life crises and relational difficulties. \n\n \n\nI\'m not your \"typical therapist\". I\'m very honest in my assessments and goal oriented to get my clients to where they want to be. Unfortunately there are no shortcuts when doing self- work. Personal develoment and growth is hard work and there are no “cutting corners”, which is why I work online, with clients that are genuinely motivated and want to put in the work to transform their lives.  ', 'roshipng.png', 'roshi_coverpng.png', NULL, 'https://www.facebook.com/therapywithroshi', NULL, '@therapywithroshi', NULL, 'https://www.roshiderakshan.com/', 'roshi.derakhshan@gmail.com', 'MDR6aVV0SSswOGdXOEFLK2hJQ0V5dz09', 'S', 'S', NULL, NULL, NULL, '2021-04-23'),
(39, 2, 'Amber', 'Coffman', NULL, 'Amber is a growth strategist and the CEO of Orogamis, a team of strategists, marketers, engineers, and designers working together toward goals of growth. Orogamis fosters and embracse growth in all of its forms: professional, personal, and on behalf of their partners. Growth is the central theme of Orogamis, the mission, commitments, and company culture.', 'amber2.jpg', 'amber_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/amber-lee-coffman-81b43725/', 'amber@shala.us', 'YVN1WjhoYllpNGY4cEpnWkw4eGpCZz09', 'S', 'S', NULL, NULL, NULL, '2021-03-20'),
(40, 2, 'Ivan', 'Martin Maseda', NULL, 'Ivan is the Chief Technology Officer at Binfluencer and Professor in Machine Learning at IE Business School in Madrid, Spain. The use of technology to reduce human intervention in repetitive tasks should lead to an environment where brains could dedicate their time to more fulfilling endeavors. His current goal is to become an excellent bridge between business needs and the technical information challenges. Translating business needs into technical requirements is only half of the work, he will make sure the technical results obtained are completely understood and communicated to all team members and shareholders. ', 'ivan2.jpg', 'ivan_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/ivanmartinmaseda/', 'ivan@shala.us', 'UnRmYUJsZVJOZis1UDBZMHFLSHl3dz09', 'S', 'S', NULL, NULL, NULL, '2021-03-20'),
(41, 2, 'Vincent', 'Guntrum', NULL, 'I am a content and video marketing coach for trainers, coaches and healers. In the last few years I have struggled - mostly impatiently - off the beaten path through the jungle of self-discovery. Just to learn that unbelievable paths suddenly open up when I manage to relax and connect with my deeper resource. Life has something special in store for everyone. I have the honor to guide you to your resource, to support you to take the decisive steps and to show you to the world.', 'vincent2png.png', 'vincent2_coverpng.png', NULL, NULL, NULL, NULL, NULL, 'https://www.wild-games.net/', 'vincent@shala.us', 'YzRIeEEvM3JYbm9BWVdSQndhcG80UT09', 'S', 'S', NULL, NULL, NULL, '2021-03-20'),
(42, 2, 'Benjamin', 'Cuenod', NULL, 'All-rounder creative at heart, I offer passionate creative thinking, strategic planning, and analytical problem-solving. I believe in the brand story as a seed to systemic change within corporations, setting brands towards a sustainable and equitable framework, establishing resilience, and provoking evolution.\n\nServices:\nStrategy\nBranding\nDesign\nCreative Technology ', '02-benjamin-cuénod.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/benjamin-cu%C3%A9nod-b47b401/', 'ben@shala.us', 'OUpidDUwQ3dyZkZPZWRSWnhualFxdz09', 'S', 'S', NULL, NULL, NULL, '2021-03-22'),
(43, 2, 'Dmitrij', 'Achelrod', NULL, ' ', 'dimi.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dimi@shala.us', 'bXh6Z0VRNHRLekhLVFRmU3RjY0x1dz09', 'S', 'S', NULL, NULL, NULL, '2021-03-24'),
(44, 2, 'Roman', 'Fernandez', 'Innovation Coach', 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'profile2.jpg', 'tlm120720kg-112.jpg', NULL, 'https://www.facebook.com/romanspade', NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'www.romanf.com', 'roman@alumni.ie.edu', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, '2021-08-26'),
(45, 2, 'Levin', 'Dihard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'levin@dihard.com', 'cmRtM2I2aXFSeHRnbW1IN2pHMUd6QT09', 'S', 'S', NULL, NULL, NULL, '2021-04-07'),
(46, 2, 'Ryan', 'Monahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'roman@eleutheria.world', 'RTQ1SFVEWm1HVGFRNm1UMW1IRTRRZz09', 'S', 'S', NULL, NULL, NULL, '2021-04-13'),
(47, 2, 'Shalaland', 'Dhalaland', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shalaland@shalaland.com', 'OXE1R1JXQ2wzVlhydFQ0Z3JlTndYUT09', 'S', 'S', NULL, NULL, NULL, '2021-04-13'),
(48, 2, 'New', 'Guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'romedetroit@outlook.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, '2021-04-14'),
(49, 2, 'Gabo', 'Test guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gabofc+testguide@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', 'N', NULL, NULL, NULL, '2021-04-14'),
(50, 2, 'Ismelda', 'Lopez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ismelda@lopez.net', 'c1NqVVpFMEdnL0lQalhVanhoSUl1dz09', 'S', 'N', NULL, NULL, NULL, '2021-04-15'),
(54, 2, 'Hanna', 'Kuznetsova', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hanna.kuznetsova@gmx.de', 'aGZSdVBzWmJwMGIwT2wzN3dBUXVWUT09', 'N', 'S', NULL, NULL, NULL, '2021-08-29'),
(51, 2, 'Sanna', 'Norrvidd', NULL, 'Sanna teach how to shift the focus from an exhausting fight with the body to a loving an empathic teamwork. Her methodology is transformational coaching that includes inner child work, meditation, breath work and somatic tools that creates more self love and safety from within.\r\n\r\nShe is working as coach for both individuals and groups to support them in developing body awareness and the ability to stay and be with the bodily sensations in this moment, regardless how challenging they may be. This work is a foundation in getting your sense of power and aliveness back and stay clear and resilient in the face of challenging emotions like fear and anger that often subconscious control our life and keeps us from getting what we truly desire.', '1d5d4c50-c910-4b00-9385-9c27e5ee6cae.jpeg', '62f8ba19-6d3f-4875-bc88-65d7d0b1b7cc.jpeg', NULL, 'https://m.facebook.com/sannanorrvidd/', NULL, '@sannanorrvidd', NULL, 'www.sannanorrvidd.com', 'sanna@sannanorrvidd.com', 'YTlsQmhkN01oV1Z0RnpFd2RCOCtrdz09', 'S', 'S', NULL, NULL, NULL, '2021-04-23'),
(52, 2, 'Amit', 'Paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'amitpaul.com', 'hello@amitpaul.com', 'ekNMbXRNQk5RbHBvZG9ZMldsT1NvdE1kVVRBWllIenUxWElwYnI4bUFtbz0=', 'N', 'S', NULL, NULL, NULL, '2021-08-29'),
(53, 2, 'Christian ', 'Haag ', 'Don\'t forget to breath', NULL, '24252b5d-4d4d-4863-816c-d43a8634f510.jpeg', NULL, NULL, 'https://www.facebook.com/crillehaag', NULL, 'Instagram.com/crillehaag', NULL, 'Www.cowbellproductions.se', 'crillehaag@gmail.com', 'UTVqcms0RDNoRkE5TVlWZURXSHJEUT09', 'S', 'S', NULL, NULL, NULL, '2021-04-27'),
(55, 2, 'Rainer', 'von Leoprechting', 'Cognitive Leadership', 'Recruitment and talent: \nWhen reengineering the recruitment process, I found a way to rapidly and intuitively connect people or people to jobs simply and fast. \n\nThe basic concept: You share some of your lived or desired successes and we match them with the success desired from your future employer. Easy, isn\'t it? Just connect with me for finding out more.\n\nDevelopmental profiles:\nBased on O. Laske Frændi and I offer precise observations to individuals and teams on where they are in their journey to practical wisdom at work. I also offer trainings on the matter.\n\nEffective meetings: We regularly lead interactive seminars in the Art of Hosting to support a self-authoring culture in organisations, maybe in yours.\n\nSpecialties: Leading in an engaging way; Participatory Meetings and Conventions; Strategy development and implementation; small and large scale organisational change and development; career coaching; leadership development; coaching; Open Space; world café; a living organisati', 'rainer1635210338.jpg', 'fraendi-coverpng1630507531.png', NULL, NULL, NULL, NULL, 'https://at.linkedin.com/in/rainerleo', 'https://fraendi.org/', 'jeroen@fraendi.org', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', '46TdhqThe8_XsDEwmfSRXut0Q0P8y8SrA', 'eyJhbGciOiJIUzUxMiIsInYiOiIyLjAiLCJraWQiOiJmMzQ4NmQwMy1lOTJlLTQxNmQtODRmMy1kNjgzNTBiYzk2ZGQifQ.eyJ2ZXIiOjcsImF1aWQiOiJhOWI1ZjVhMTViZjkyYTY0MzQxNjgzYzI2OTY2Njk3YSIsImNvZGUiOiI0NlRkaHFUaGU4X1hzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ6bTpjaWQ6Z28zV3MwSElRYTZBdVkxUDR3SVpEZyIsImdubyI6MCwidHlwZSI6MCwidGlkIjo0LCJhdWQiOiJodHRwczovL29hdXRoLnpvb20udXMiLCJ1aWQiOiJYc0RFd21mU1JYdXQwUTBQOHk4U3JBIiwibmJmIjoxNjM1Nzk0ODUzLCJleHAiOjE2MzU3OTg0NTMsImlhdCI6MTYzNTc5NDg1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImp0aSI6IjRhYzg5NTZiLTk1ODgtNDU3Ni04NjYzLTdkMDBiOGZmMTk4YSJ9.30uA5RxjYjYuxLs36zAWlPVdCNJpJ7XQ0TNGPEbNYA7qX4kOFeS96XxJI-5aTTST2I3B1-DmnbDswKLe8Vy09g', 'eyJhbGciOiJIUzUxMiIsInYiOiIyLjAiLCJraWQiOiJhNzdkNGQzYi1jNDg0LTQwODktYWFmYi00NjdmYWQwYzUwM2MifQ.eyJ2ZXIiOjcsImF1aWQiOiJhOWI1ZjVhMTViZjkyYTY0MzQxNjgzYzI2OTY2Njk3YSIsImNvZGUiOiI0NlRkaHFUaGU4X1hzREV3bWZTUlh1dDBRMFA4eThTckEiLCJpc3MiOiJ6bTpjaWQ6Z28zV3MwSElRYTZBdVkxUDR3SVpEZyIsImdubyI6MCwidHlwZSI6MSwidGlkIjo0LCJhdWQiOiJodHRwczovL29hdXRoLnpvb20udXMiLCJ1aWQiOiJYc0RFd21mU1JYdXQwUTBQOHk4U3JBIiwibmJmIjoxNjM1Nzk0ODUzLCJleHAiOjIxMDg4MzQ4NTMsImlhdCI6MTYzNTc5NDg1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImp0aSI6IjcwNjA2N2U0LWU2NTgtNGFjNi1iZTJhLWJhODFlOGIzYzgzOCJ9.6-W7m9DLI4YAencXfkf256dW7fnxhglHeXlS6_WQDtAOje-EMpvWbtGi2tncb9siLeYMGEl4o8k63miSlZ3TWw', '2021-10-27'),
(56, 2, 'Fraendi', 'Team', NULL, 'Fraendi is an international network of consultants and leading practitioners in Leadership Development, Consulting, Coaching and Organisational Development. We bring together a wealth of expertise and experience from a wide range of organizational and societal perspectives into one single vision: \nWE PARTNER WITH LEADERS ACROSS ORGANIZATIONS TO DEVELOP CAPACITIES FOR COMPLEXITY AND TRANSFORMATION\n\nWe are living in disruptive times. We face multiple crisis and wicked problems. Especially people in leadership positions need to be able to meet these challenges. Fraendi is specialising in methods and approaches that senior leaders need for learning and transformation individually for their teams across the organisation as part of multi-stakeholder or whole sector learning', 'fraendi_logo_ohne_rand-removebg1630506796.png', 'fraendi-coverpng1630506796.png', NULL, NULL, NULL, NULL, NULL, 'https://fraendi.org/', 'fraendi@innerworks.io', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', 'N', NULL, NULL, NULL, '2021-09-01'),
(57, 2, 'Peter', 'Wallberg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pwallberg@gmail.com', 'YVZycnVvRFN6QkJJcXdJVmVuak1TZz09', 'S', 'S', NULL, NULL, NULL, '2021-09-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachesclients`
--

CREATE TABLE `coachesclients` (
  `coachClientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachesclients`
--

INSERT INTO `coachesclients` (`coachClientId`, `coachId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(15, 2, 3, 'R', NULL),
(30, 3, 3, 'N', NULL),
(33, 5, 3, 'S', NULL),
(34, 6, 3, 'N', NULL),
(35, 7, 3, 'R', 'Not in onboarding meeting'),
(36, 8, 3, 'S', 'Not in onboarding meeting'),
(38, 10, 3, 'S', NULL),
(39, 11, 3, 'R', 'Not in meeting'),
(40, 12, 3, 'R', 'Not in onboarding meeting'),
(41, 13, 3, 'S', NULL),
(42, 14, 3, 'N', NULL),
(43, 15, 3, 'N', NULL),
(46, 16, 3, 'N', NULL),
(48, 17, 3, 'N', NULL),
(50, 18, 4, 'N', NULL),
(52, 19, 4, 'N', NULL),
(58, 9, 3, 'S', NULL),
(59, 20, 3, 'R', 'You are a bad teacher'),
(60, 21, 3, 'S', NULL),
(61, 22, 1, 'S', NULL),
(62, 23, 1, 'S', NULL),
(63, 24, 1, 'N', NULL),
(64, 25, 1, 'R', 'porue si porque quise y porque puedo'),
(65, 26, 1, 'N', NULL),
(66, 27, 1, 'R', 'asfasf'),
(67, 28, 1, 'S', 'test reject'),
(68, 29, 1, 'N', 'porque puedo'),
(69, 3, 4, 'N', NULL),
(70, 30, 3, 'N', 'Because we dont like you'),
(71, 31, 3, 'N', 'Your are not a true coach'),
(72, 1, 1, 'S', NULL),
(73, 32, 7, 'N', NULL),
(74, 33, 7, 'N', NULL),
(75, 34, 7, 'N', NULL),
(76, 35, 2, 'N', NULL),
(77, 18, 2, 'S', NULL),
(112, 48, 8, 'N', NULL),
(114, 50, 3, 'S', NULL),
(111, 47, 8, 'S', NULL),
(108, 44, 3, 'N', NULL),
(107, 43, 10, 'S', NULL),
(106, 42, 9, 'N', NULL),
(105, 29, 8, 'N', NULL),
(104, 41, 9, 'N', NULL),
(103, 40, 9, 'N', NULL),
(102, 39, 9, 'N', NULL),
(101, 38, 9, 'N', NULL),
(100, 37, 3, 'S', NULL),
(99, 37, 1, 'S', NULL),
(113, 49, 8, 'S', NULL),
(115, 51, 3, 'S', NULL),
(116, 52, 3, 'S', NULL),
(117, 53, 3, 'S', NULL),
(118, 54, 3, 'S', NULL),
(119, 44, 9, 'S', NULL),
(120, 55, 3, 'N', NULL),
(121, 56, 3, 'S', NULL),
(122, 57, 3, 'S', NULL),
(123, 44, 0, 'S', NULL),
(124, 1, 0, 'S', NULL),
(125, 35, 3, 'N', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachserie`
--

CREATE TABLE `coachserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachserie`
--

INSERT INTO `coachserie` (`coachSerieId`, `coachId`, `serieId`) VALUES
(59, 44, 31),
(4, 52, 32),
(7, 44, 14),
(58, 6, 31),
(69, 44, 29),
(68, 6, 29),
(67, 5, 29),
(66, 1, 29),
(64, 44, 30),
(63, 6, 30),
(62, 5, 30),
(61, 1, 30),
(57, 5, 31),
(56, 1, 31),
(51, 55, 33),
(50, 44, 33),
(60, 52, 31),
(65, 52, 30),
(70, 52, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachvideo`
--

CREATE TABLE `coachvideo` (
  `coachVideoId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachvideo`
--

INSERT INTO `coachvideo` (`coachVideoId`, `coachId`, `videoId`) VALUES
(305, 52, 57),
(304, 44, 57),
(303, 9, 57),
(302, 6, 57),
(301, 5, 57),
(311, 52, 54),
(310, 44, 54),
(309, 9, 54),
(308, 6, 54),
(307, 5, 54),
(379, 35, 72),
(376, 55, 56),
(317, 52, 55),
(316, 44, 55),
(315, 9, 55),
(314, 6, 55),
(313, 5, 55),
(377, 35, 71),
(373, 55, 58),
(323, 52, 59),
(322, 44, 59),
(321, 9, 59),
(320, 6, 59),
(319, 5, 59),
(329, 52, 61),
(328, 44, 61),
(327, 9, 61),
(326, 6, 61),
(325, 5, 61),
(278, 44, 20),
(279, 44, 21),
(280, 44, 22),
(281, 44, 23),
(282, 44, 24),
(283, 44, 25),
(284, 44, 26),
(285, 44, 27),
(286, 44, 28),
(287, 44, 29),
(288, 44, 30),
(299, 52, 52),
(298, 44, 52),
(297, 9, 52),
(296, 6, 52),
(295, 5, 52),
(300, 1, 57),
(306, 1, 54),
(312, 1, 55),
(318, 1, 59),
(324, 1, 61),
(335, 52, 63),
(334, 44, 63),
(333, 9, 63),
(332, 6, 63),
(331, 5, 63),
(330, 1, 63),
(349, 52, 60),
(348, 44, 60),
(347, 9, 60),
(346, 6, 60),
(345, 5, 60),
(344, 1, 60),
(355, 52, 62),
(354, 44, 62),
(353, 9, 62),
(352, 6, 62),
(351, 5, 62),
(350, 1, 62),
(361, 52, 64),
(360, 44, 64),
(359, 9, 64),
(358, 6, 64),
(357, 5, 64),
(356, 1, 64),
(294, 1, 52),
(277, 44, 31),
(293, 52, 65),
(292, 44, 65),
(291, 9, 65),
(290, 5, 65),
(289, 1, 65),
(336, 1, 66),
(342, 52, 67),
(343, 1, 68),
(367, 52, 69),
(341, 44, 67),
(340, 9, 67),
(339, 6, 67),
(338, 5, 67),
(337, 1, 67),
(366, 44, 69),
(365, 9, 69),
(364, 6, 69),
(363, 5, 69),
(362, 1, 69),
(272, 1, 70),
(380, 35, 73),
(381, 55, 74);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorcatalog`
--

CREATE TABLE `colorcatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorcatalog`
--

INSERT INTO `colorcatalog` (`catalogId`, `nombre`) VALUES
(1, 'Main Color'),
(2, 'Secondary Color'),
(3, 'Text Color'),
(4, 'Text Hover Color');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `colorId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorsclient`
--

CREATE TABLE `colorsclient` (
  `colorClientId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorsclient`
--

INSERT INTO `colorsclient` (`colorClientId`, `clientId`, `catalogId`, `color`) VALUES
(1, 3, 1, '#e3913a'),
(2, 3, 2, '#815e39'),
(3, 3, 3, '#918a83'),
(4, 3, 4, '#e8ddd2'),
(5, 8, 1, '#fb2449');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

CREATE TABLE `diccionario` (
  `iddiccionario` int(11) NOT NULL,
  `campo` varchar(200) NOT NULL,
  `etiqueta` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diccionario`
--

INSERT INTO `diccionario` (`iddiccionario`, `campo`, `etiqueta`) VALUES
(8, 'tipousuario', 'User\'s Type'),
(9, 'pass', 'Password'),
(14, 'statuspago', 'Payment Status'),
(28, 'usuario', 'User'),
(27, 'tipoadmin', 'Admin Type'),
(23, 'category', 'Categories'),
(26, 'nombre', 'Name'),
(29, 'accessadmin', 'Admin Access'),
(30, 'logotipo', 'Logo'),
(31, 'icono', 'Icon'),
(32, 'biografia', 'Biography'),
(33, 'foto', 'Photo'),
(34, 'telefono', 'Phone'),
(35, 'colorcatalog', 'Color\'s Catalog'),
(36, 'servicescatalog', 'Service\'s Catalog'),
(37, 'colorsclient', 'Clients Colors'),
(38, 'descripcion', 'Description'),
(39, 'imagen', 'Image'),
(40, 'titulo', 'Title'),
(41, 'apellido', 'Last Name'),
(42, 'fecha', 'Date'),
(43, 'users', 'Members'),
(44, 'bannerhome', 'Home Banners'),
(45, 'slidehome', 'Home Slider'),
(46, 'bannersomos', 'About Content'),
(47, 'enviar', 'Send'),
(48, 'manda', 'Send'),
(49, 'tipollamada', 'Session Access'),
(50, 'postcategory', 'Category'),
(51, 'posicion', 'Position'),
(52, 'imagenMovil', 'Mobile image'),
(53, 'boton', 'Button'),
(54, 'botonMovil', 'Button Mobile'),
(55, 'botonMobile', 'Button Mobile'),
(56, 'statusaccount', 'Status Account'),
(57, 'coach', 'Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email`
--

CREATE TABLE `email` (
  `emailId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `typeId` int(11) NOT NULL COMMENT 'opciones=typemail',
  `subject` varchar(200) NOT NULL,
  `generalText` varchar(20000) NOT NULL COMMENT 'html'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventrecording`
--

CREATE TABLE `eventrecording` (
  `eventRecordingId` int(11) NOT NULL,
  `eventId` int(11) NOT NULL COMMENT 'opciones=events',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `eventId` int(11) NOT NULL COMMENT 'multiple=circles,eventscircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `categoryId` int(11) DEFAULT NULL COMMENT 'opciones=categories',
  `allMembers` char(1) DEFAULT NULL,
  `allCoaches` char(1) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL COMMENT 'file',
  `type` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL COMMENT 'name',
  `description` varchar(2000) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL COMMENT 'address',
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipollamada',
  `date` datetime NOT NULL,
  `timezone` varchar(200) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '60',
  `orden` int(11) DEFAULT NULL,
  `googleCalendarEventId` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`eventId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `type`, `title`, `description`, `location`, `tipoId`, `date`, `timezone`, `duration`, `orden`, `googleCalendarEventId`) VALUES
(1, 3, 1, NULL, NULL, NULL, '1638991749.jpg', '1', 'Test session 1', 'Test', 'Innerworks Academy Main Hall', 2, '2021-12-08 22:00:00', 'Europe/Stockholm', 60, NULL, NULL),
(4, 3, 1, NULL, NULL, NULL, '1639076308.jpg', '1', 'Test Session 1', 'This is a test', 'Innerworks HQ Main Hall', 2, '2021-12-10 18:56:00', 'Europe/Stockholm', 60, NULL, NULL),
(5, 3, 1, NULL, NULL, NULL, '1639423925.jpg', '1', 'Test Session 12/13', 'Test', 'Innerworks HQ', 2, '2021-12-13 21:00:00', 'Europe/Stockholm', 60, NULL, NULL),
(6, 3, 1, NULL, NULL, NULL, '1639430975.jpg', '1', 'Test Session 1', 'Test', 'Innerworks HQ', 2, '2021-12-14 23:00:00', 'Europe/Berlin', 60, NULL, NULL),
(7, 3, 1, NULL, NULL, NULL, '1639500262.jpg', '1', 'Test 12/15', 'Test', 'InnerworksHQ', 2, '2021-12-15 16:00:00', 'Europe/Stockholm', 60, NULL, NULL),
(28, 3, 35, NULL, NULL, NULL, '1640250599.jpg', '1', 'Ephata Workshop Test', 'This is a series of workshops for 6 to 16 year olds to put the 7 pillars of Ephata to practice. To enroll for the workshops and find out more, email Valeria at valeriapayton@gmail.com', 'https://zoom.romanf.com', 2, '2021-12-24 15:00:00', 'Europe/Berlin', 60, NULL, NULL),
(30, 3, 35, NULL, NULL, NULL, '1640291163.jpg', '1', 'Workshop Test 2', 'Test', 'Test location', 2, '2021-12-24 15:00:00', 'Europe/Berlin', 60, NULL, NULL),
(31, 3, 1, NULL, NULL, NULL, '1640610677.jpg', '1', 'testing23', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mi in mauris posuere, at auctor ipsum volutpat. Nullam hendrerit sed nulla tincidunt facilisis. Sed interdum, ex nec sodales consequat, tortor dolor egestas leo, vitae volutpat odio nibh et enim. Nunc rutrum odio lorem, at vehicula leo facilisis sed. Ut a nisl nisi. Nulla non nunc nisi. Sed sagittis consequat quam. Pellentesque fermentum ornare enim, quis feugiat mi laoreet at. Sed eget lacus nec libero condimentum pretium nec id mauris. Nulla quis tincidunt metus. Pellentesque quis cursus nunc. Nulla facilisi. Vestibulum et scelerisque ante. Nunc sodales ut mi et pulvinar.', 'testing1', 2, '2022-01-05 07:14:00', 'Europe/Berlin', 60, NULL, 'fkr3dsl16bobhchlalqava3ndc'),
(32, 3, 35, NULL, NULL, NULL, '1640644934.jpg', '1', 'Test Session CM 12/27', 'Shala’s vision is to become a leading digital marketplace for individuals and companies to sell and operate services\nTo realize this ambitious vision, Shala is guided by 3 strategic principles, derived from an original paper by its founder Roman in August 2020: Stewardship over Ownership, Collaboration over Competition and Human over Digital', 'Shala HQ', 2, '2021-12-28 09:00:00', 'Europe/Berlin', 60, NULL, NULL),
(33, 3, 35, NULL, NULL, NULL, '1640644934.jpg', '1', 'Test Session CM 12/27 Ver 2', 'Shala’s vision is to become a leading digital marketplace for individuals and companies to sell and operate services\nTo realize this ambitious vision, Shala is guided by 3 strategic principles, derived from an original paper by its founder Roman in August 2020: Stewardship over Ownership, Collaboration over Competition and Human over Digital', 'Shala HQ', 2, '2021-12-29 10:00:00', 'Europe/Berlin', 60, NULL, NULL),
(34, 3, 35, NULL, NULL, NULL, '1640645379.jpg', '1', 'Test Session 3', 'Shala’s vision is to become a leading digital marketplace for individuals and companies to sell and operate services To realize this ambitious vision, Shala is guided by 3 strategic principles, derived from an original paper by its founder Roman in August 2020: Stewardship over Ownership, Collaboration over Competition and Human over Digital', 'Shala HQ', 2, '2021-12-30 18:48:00', 'Europe/Berlin', 60, NULL, NULL),
(35, 3, 35, NULL, NULL, NULL, '1640731232.jpg', '1', 'Calendar invite test', 'Test', 'Shala', 2, '2021-12-31 22:39:00', 'Europe/Berlin', 60, NULL, NULL),
(36, 3, 1, NULL, NULL, NULL, '1640772539.jpg', '1', 'Vipin Testing', 'Vipin Testing Vipin Testing Vipin Testing Vipin Testing Vipin Testing Vipin Testing Vipin Testing', 'https://innerworks.consciousmission.com', 2, '2022-01-02 06:33:03', 'America/Indiana/Vevay', 60, NULL, NULL),
(37, 3, 1, NULL, NULL, NULL, '1640870209.jpg', '1', 'My First testing', 'My First testing', 'My First testing', 2, '2022-01-05 03:46:00', 'Europe/Berlin', 60, NULL, 'p59f1aemei9fj8subnc6r3ovpc'),
(38, 3, 1, NULL, NULL, NULL, '1640871344.jpg', '1', 'Vipin Final Testing', 'Vipin Final Testing Vipin Final Testing Vipin Final Testing Vipin Final Testing', 'https://innerworks.consciousmission.com/', 2, '2022-01-03 15:35:00', 'Etc/GMT-9', 60, NULL, NULL),
(39, 3, 1, NULL, NULL, NULL, '1640871470.jpg', '1', 'Vipin Final Testing', 'Vipin Final Testing Vipin Final Testing Vipin Final Testing', 'https://innerworks.consciousmission.com/', 2, '2022-01-02 01:36:00', 'Etc/Zulu', 60, NULL, NULL),
(40, 3, 1, NULL, NULL, NULL, '1640944183.jpg', '1', 'Vipin Final Testing', 'Vipin Final Testing Vipin Final Testing Vipin Final Testing', 'https://innerworks.consciousmission.com/', 2, '2022-01-01 20:06:00', 'Etc/Zulu', 60, NULL, NULL),
(41, 3, 1, NULL, NULL, NULL, '1640933567.jpg', '1', 'First testing', 'First testing', 'First testing', 2, '2022-01-03 01:53:00', 'Europe/Berlin', 60, NULL, NULL),
(42, 3, 6, NULL, NULL, NULL, '1641245690.jpg', '1', 'Test Meeting', 'I and my son would sit and stare at the stars in the night sky with this playing it\'s something we had, sadly his battle with metal illness made him take his life 2 weeks ago. This music lets me be with him for a short while gazing at the stars and losing myself to the beauty of everything and the fact we are such a small spec in time, I hope he hears this and knows he\'s missed and that somewhere in those stars he\'s happy', 'Shala HQ', 2, '2022-01-05 11:30:00', 'Europe/Berlin', 60, NULL, NULL),
(43, 3, 35, NULL, NULL, NULL, '1641245847.jpg', '1', 'Test Event', 'I and my son would sit and stare at the stars in the night sky with this playing it\'s something we had, sadly his battle with metal illness made him take his life 2 weeks ago. This music lets me be with him for a short while gazing at the stars and losing myself to the beauty of everything and the fact we are such a small spec in time, I hope he hears this and knows he\'s missed and that somewhere in those stars he\'s happy', 'Shala HQ', 2, '2022-01-11 10:01:00', 'Europe/Berlin', 60, NULL, NULL),
(44, 3, 1, NULL, NULL, NULL, '1641879652.jpg', '1', 'test234', 'test2', 'test2', 2, '2022-01-13 05:10:09', 'Europe/Berlin', 60, NULL, 'al706so57fu2162l2apk6t957g'),
(45, 3, 1, NULL, NULL, NULL, '1641879652.jpg', '1', 'testing22', 'test2', 'test2', 2, '2022-01-13 00:40:09', 'Europe/Berlin', 60, NULL, 'svvnskbsjbgag68tp1u6nivu0g'),
(46, 3, 1, NULL, NULL, NULL, '1641881048.jpg', '1', 'new one test', 'new one test', 'new one test', 2, '2022-01-12 11:30:57', 'Europe/Berlin', 60, NULL, 'q2h0024l1ogdsf3v4arodi92v4'),
(47, 3, 6, NULL, NULL, NULL, '1641936886.jpg', '1', 'Test event', 'Test', 'Innerworks HQ', 2, '2022-01-08 20:29:00', 'Europe/Berlin', 60, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventscircle`
--

CREATE TABLE `eventscircle` (
  `eventCircleId` int(11) NOT NULL,
  `eventId` int(11) NOT NULL COMMENT 'opciones=events',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `eventscircle`
--

INSERT INTO `eventscircle` (`eventCircleId`, `eventId`, `circleId`) VALUES
(1, 1, 36),
(4, 4, 36),
(5, 5, 36),
(6, 6, 36),
(7, 7, 36),
(29, 28, 40),
(31, 30, 40),
(38, 32, 40),
(39, 33, 40),
(60, 34, 40),
(61, 35, 39),
(79, 38, 31),
(109, 39, 31),
(117, 40, 31),
(134, 36, 31),
(137, 41, 36),
(139, 37, 35),
(140, 31, 31),
(141, 42, 30),
(142, 43, 40),
(153, 44, 35),
(154, 45, 35),
(155, 46, 35),
(158, 47, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventsunregister`
--

CREATE TABLE `eventsunregister` (
  `eventUnregisterId` int(11) NOT NULL,
  `eventId` int(11) NOT NULL COMMENT 'opciones=events',
  `userId` int(11) NOT NULL COMMENT 'opciones=users	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `excluded`
--

CREATE TABLE `excluded` (
  `excludedId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `excluded`
--

INSERT INTO `excluded` (`excludedId`, `callId`, `userId`) VALUES
(3, 147, 73),
(4, 147, 73),
(5, 147, 73);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE `gallery` (
  `imageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `title` varchar(200) NOT NULL,
  `altText` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL COMMENT 'liga',
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`imageId`, `clientId`, `image`, `title`, `altText`, `link`, `position`) VALUES
(1, 8, '42-17452823.jpg', 'Image 1', 'Image 1', NULL, 2),
(2, 8, '42-17454853.jpg', 'Image 2', 'Image 2', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generals`
--

CREATE TABLE `generals` (
  `generalId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `hideCoachContent` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `homeMenuText` varchar(200) NOT NULL,
  `exploreMenuText` varchar(200) NOT NULL,
  `coachesMenuText` varchar(200) NOT NULL,
  `aboutMenuText` varchar(200) NOT NULL,
  `coachText` varchar(200) NOT NULL,
  `memberText` varchar(200) NOT NULL,
  `liveMenuText` varchar(200) NOT NULL,
  `liveSessionText` varchar(200) NOT NULL,
  `processingText` varchar(200) NOT NULL,
  `signUpText` varchar(200) NOT NULL,
  `headerTitle` varchar(200) NOT NULL,
  `subtitleHeader` varchar(500) NOT NULL,
  `headerButtonShow` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `headerButtonFontSize` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `headerButtonText` varchar(200) DEFAULT NULL,
  `headerButtonUrl` varchar(200) DEFAULT NULL COMMENT 'liga',
  `headerButtonTextColor` varchar(10) NOT NULL DEFAULT '#000000' COMMENT 'color',
  `headerButtonBackgroundColor` varchar(10) NOT NULL DEFAULT '#ffffff' COMMENT 'color',
  `headerButtonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `videoHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `videoMobileHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titleCategories` varchar(200) NOT NULL,
  `subtitleCategories` varchar(500) NOT NULL,
  `titleCoaches` varchar(200) NOT NULL,
  `subtitleCoaches` varchar(500) NOT NULL,
  `titleClasses` varchar(200) NOT NULL,
  `subtitleClasses` varchar(500) NOT NULL,
  `trialDaysText` varchar(200) NOT NULL,
  `trialDescriptionText` varchar(500) NOT NULL,
  `blogText` varchar(200) NOT NULL DEFAULT 'Blog',
  `signUpCoachText` varchar(200) NOT NULL DEFAULT 'Become a Guide'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `generals`
--

INSERT INTO `generals` (`generalId`, `clientId`, `hideCoachContent`, `homeMenuText`, `exploreMenuText`, `coachesMenuText`, `aboutMenuText`, `coachText`, `memberText`, `liveMenuText`, `liveSessionText`, `processingText`, `signUpText`, `headerTitle`, `subtitleHeader`, `headerButtonShow`, `headerButtonFontSize`, `headerButtonText`, `headerButtonUrl`, `headerButtonTextColor`, `headerButtonBackgroundColor`, `headerButtonRounded`, `videoHeader`, `videoMobileHeader`, `titleCategories`, `subtitleCategories`, `titleCoaches`, `subtitleCoaches`, `titleClasses`, `subtitleClasses`, `trialDaysText`, `trialDescriptionText`, `blogText`, `signUpCoachText`) VALUES
(1, 3, 'N', 'Home', 'Journeys', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, '/dashboard/login', '#000000', '#ffffff', 'N', 'gradientwithtext_4096x1728.mp4', 'mobile_text_new.mp4', 'UPGRADE YOUR HUMAN SOFTWARE', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'START WITH FREE LIMITED ACCESS', 'Get started with limited access to our breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(2, 4, 'N', 'Home', 'Classes', 'Teachers', 'About', 'Teacher', 'Student', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Faith. Wisdom. Goodness', 'Prepare our children for the future with passion based learning', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'header-ephata.mp4', NULL, 'FALL IN LOVE WITH LEARNING', 'Find the live classes, educational videos and exercises that nurture children beyond the curriculums of regular school', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'CLASSES', 'FIND A CLASS TO GET STARTED', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited purpose approach, passion based learning, self efficacy, academics, environmental awareness, social responsibility and spiritual balance classes', 'Blog', 'Become a Guide'),
(3, 7, 'N', 'Home', 'Programs', 'Facilitators', 'Location', 'Facilitators', 'Guests', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'HEAL - CONNECT - GROW', 'Holistic eco-center for retreats and programs in personal growth, spirituality, healing arts and therapeutic sciences', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', '', NULL, 'Our programs', 'Nierika offers therapeutic programs that integrate elements of traditional indigenous medicine and modern psychotherapy facilitated by multidisciplinary professionals\n', 'Meet our facilitators', 'Heal, connect and grow through programs facilitated by our world class therapists, instructors and mentors both in-person at our eco-center and online.', 'PROGRAMS', 'FIND A PROGRAM THAT WORKS FOR YOU', 'Sign-up for our programs', 'Tell us about yourself and what brings you to Nierika. We respond to everyone with an open heart.', 'Blog', 'Become a Guide'),
(4, 8, 'N', 'Home', 'Retreats', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'FIND PROFOUND', 'Exponential transformation by combining mind, body and spirit', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', NULL, 'WORK ON YOUR INNER self', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(5, 9, 'N', 'Home', 'Tutorials', 'Experts', 'About', 'Expert', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', '', '', 'LEARN OUR SECRETS', 'Everything we applied to build and grow Shala is here for you to use – Learn how to create strong content, prepare for camera, build your marketing strategy, find talent, design your brand, scale users and a lot more!', 'MEET OUR EXPERTS', 'Everyone who advised or contributed to Shala are now part of our expert team on our platform. They are here to help you build your platform, create winning content, scale your users and make your vision a reality!', 'TUTORIALS', 'FIND A TUTORIAL TO HELP REALIZE THE FULL POTENTIAL OF YOUR PLATFORM', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited access to our courses and building out your test platform. Cancel anytime.', 'Blog', 'Become a Guide'),
(6, 10, 'S', 'Home', 'Programs', 'Team', 'About Us', 'Team', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Explore. Realize. Transform.', 'The only journey is the one within', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', '', 'START YOUR JOURNEY WITH US', 'We enable leaders to safely embark on probably the most challenging and yet powerful journey of their lives\n', 'MEET OUR TEAM', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide'),
(7, 11, 'N', 'Home', 'Classes', 'Guides', 'About Us', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Align. Heal. Act.', 'Upgrade your human software to unlock your full potential and find your flow', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', 'video-header.mp4', 'video-header.mp4', 'WORK ON YOUR INNER SELF', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home`
--

CREATE TABLE `home` (
  `homeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `home`
--

INSERT INTO `home` (`homeId`, `nombre`) VALUES
(1, 'Header'),
(2, 'Categories'),
(3, 'Coaches'),
(4, 'Banners'),
(5, 'TrialText'),
(6, 'LiveSessions');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homeplatform`
--

CREATE TABLE `homeplatform` (
  `homePlatformId` int(11) NOT NULL,
  `homeId` int(11) NOT NULL COMMENT 'opciones=home',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `homeplatform`
--

INSERT INTO `homeplatform` (`homePlatformId`, `homeId`, `clientId`) VALUES
(137, 6, 9),
(136, 5, 9),
(135, 4, 9),
(134, 3, 9),
(133, 2, 9),
(90, 6, 8),
(89, 5, 8),
(88, 4, 8),
(87, 3, 8),
(86, 2, 8),
(16, 1, 7),
(17, 2, 7),
(18, 3, 7),
(19, 4, 7),
(20, 5, 7),
(21, 1, 4),
(22, 2, 4),
(23, 3, 4),
(24, 4, 4),
(25, 5, 4),
(57, 6, 10),
(56, 5, 10),
(55, 4, 10),
(54, 3, 10),
(53, 2, 10),
(131, 1, 3),
(41, 6, 4),
(42, 6, 7),
(85, 1, 8),
(132, 1, 9),
(52, 1, 10),
(91, 1, 11),
(92, 2, 11),
(93, 3, 11),
(94, 4, 11),
(95, 5, 11),
(96, 6, 11),
(97, 2, 12),
(98, 3, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `levelId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`levelId`, `clientId`, `nombre`, `posicion`) VALUES
(1, 3, 'Easy', 1),
(2, 8, 'Easy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pages`
--

INSERT INTO `pages` (`pageId`, `nombre`) VALUES
(1, 'Classes'),
(2, 'About Us'),
(3, 'Live Session'),
(4, 'Coaches'),
(5, 'Gallery');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passcambio`
--

CREATE TABLE `passcambio` (
  `passId` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `passcambio`
--

INSERT INTO `passcambio` (`passId`, `tipo`, `id`, `token`) VALUES
(3, 1, 5, 'T1lyVlFod3ladlhzTWpHaGVYK2k4RlN5Vzg2ZVp0ZHRzcytpd05qMmlMND0='),
(4, 1, 6, 'cURZQSsxYU01elA0T2lMVnJPZWJDS0NFUW1vc2ljSWFuNkZpcldhOUIxZz0='),
(35, 1, 85, 'NjBKVC9PZHc1Z25rK292Z08zcmMwM0dDbFE3SjUxU2Yvcjk3ZURmTU1OMD0='),
(33, 1, 68, 'NUpWb3g1QkE2eTZHbUVFdkRVaWp3SHhWdUhiRXR4d29hczFKZkZhU1hNND0='),
(29, 1, 52, 'TnlCV2lhcGlhR25oMnlLNWQxUTRmUTNoQ2trc2VWTnpKd0NYenRHUDFLTT0='),
(27, 1, 49, 'VDhtdXpsSkpDeFpjY0lCMGNEOG9yTU5ETjBKVHYyZFJxcERMcEJDdFk3OD0='),
(11, 2, 25, 'RkR4dUQ2WDgvWU51eG92eFh3Zk14dmx4QmFaaC9RQVVXbGduT3ZGQ3kzcz0='),
(12, 2, 25, 'bC82dDVvTUMyaWEvTUExSzNmTi9aZGcrNkIyekt2by9VNnNxRDZvZGFtST0='),
(13, 2, 26, 'dG1xcmFIcUtHOXI1STlVRVpLZWcvdXNMdi9wM2xhUEZNeDdZZGJ0eTduRT0='),
(19, 1, 3, 'czkzeVRWUjZrSUtsZkl3WDZ2dVg3WHRzaGpWUVNCd0tFQW5HZWsxWkxaRT0='),
(36, 1, 85, 'bjZ5d1hNbmtUc1B2ZkxPVW5iTmNTZnR4bjFDTTcxN1M0QUNaeEJ4enFEdz0='),
(37, 1, 85, 'cThRN3ZpK3p6R0NiSlAwTFRTZm44STFBWTdTRDdnek9nVzJHRUdlSllUMD0='),
(38, 1, 85, 'V0ZDaDM4aWlTL1dJUUpqT0dicE4rdlZUS3BwOHdSMnd1ZVI3S3JXdVVNRT0='),
(39, 1, 85, 'djBvZHJwc0JIYzRCbkJId0w2R1JuQ3BhSHh1Um1Hc0FWRmkyWGtha2lvcz0='),
(40, 1, 85, 'emtLWVdEcVdOc0xMQm1Ob3g5VVZDWFhaZkNkU20wcUh2SmFKMFVTQVloQT0='),
(41, 3, 10, 'ZmdIMXp0eTBOZVJHcGM4VVhXb2FjclFOTWRadG9iSGwyMnRSbUY0UFUzZz0='),
(42, 2, 38, 'QUozYXJJL1lSRFRsZENRR0Rkd3FibktyV0xXUWJLSFRIMVdvYU5uLzZKZz0='),
(43, 1, 70, 'cWN4a1p3dHZDZkc4SkhxeWoyamt0WDRqcVhyQjF4M2VITVRXd0gxci9Hbz0='),
(50, 1, 54, 'bTVZSTJFWEVMT3RmV0o0dXliUzR1ZmNtMzR1dkdSWGFpTDNmNWM4d2lxOD0='),
(49, 1, 54, 'T0NHcjZrVW5OUENTRVJiczVGbHZqSDR1QktGRHZ1RWRKN3AwRzFzRGVyRT0='),
(48, 1, 54, 'WkJqdS90YmcvNzhhRFFKR0trT01VTFJibHVmRHozSGdWL1RYb01NK0xlQT0='),
(51, 1, 54, 'b3JJSnlCZVZPU3ZoYWNMWXBqa1lkZzFxWHI3QVRadTJxN0EycEdwYmV6UT0='),
(52, 1, 54, 'MW5ScE13VGpja2NkQ09hbnNwaWZ0WHUrN2Fid2o1dXpvMDJQbHBwY1RuWT0='),
(53, 1, 54, 'cHV0aGtORldrWnZnYUE5cXBGWTEvZ3c1QnZycmRsU0dYZGdVd2J0dTgzbz0='),
(54, 1, 54, 'Ly81L0pHZE1xUHhWNExPOGwrTi8rcDdFYXJHWEhjdkh0TW5tRWFKWEZFMD0='),
(55, 1, 54, 'TXhveTlQTFNPbitxdmIwZzZvY3RxdkhWVWdldytLWXB1ZkNKTWdDcUQyST0='),
(56, 1, 54, 'RE9NcE9ydFUrWnNNOTNNSm1tQnBuSkFQOFUxSGt4Y0ZtclJkNUM0THQwTT0='),
(57, 1, 54, 'bVlsL0M4TUpnclpiTzk3aVRoanFqSTh0SGdWTjExcHFzQzVKNlNVQngrdz0='),
(58, 1, 54, 'cCs0eHRGTGUxNU9HajVpbGx5VTlIeG5Xb1RLUUltMkFRYU14MXVJSmt0bz0='),
(59, 1, 54, 'STN3NWxWZmFndVZnNGxYaU5saTNkTUc5TS9EcVJjWFQwU2ZseCs1OGpLTT0='),
(60, 1, 54, 'Z0l2Qi9LQWEwUFRGRTZlRVJhOVdiWGJDd09BYTdxNnJPMzJUUFdwWUNjWT0='),
(61, 1, 54, 'MXphLzVJZ25idVhrRXZKL0Q0ejlwMTI1MzBodTROWmZ5bDRrRWl1OEVYST0='),
(62, 1, 54, 'QllBUVZXMGdLTUhicWlCUGJtNjhJSVBQTTJaNGE5VTd0SVE4SXBrY3dtaz0='),
(63, 1, 92, 'RXp6STh5ZzA5eE5uclFRT1RpYU1kUGtoWEFaNzh5NmlKS05CYlZ3MDNaZz0='),
(64, 1, 92, 'b3ByVHFraVNUWk1RYk1Cd1AvNklIeUhtUjBiVzQwcTF5SHBVL09oeTJ4cz0='),
(65, 1, 92, 'dDhlNms3WDRLVFNRYmR1NWEwbzJHZ3hmaHNOWjZ0Z3NkMUZ1cWZkUUs3VT0='),
(83, 1, 76, 'b3pSMUtOaFlmNjhUYUthS2YraUtuTlI0STd3UW5EZXJTUTlnZ1FIWkJBdz0='),
(82, 1, 76, 'bmcwZDgrcWlpS0pjcFVFQ1RVcjl0T09KYjJ6T2xTTGliQ2FFdE14WW1hdz0='),
(81, 1, 76, 'aFpYMVVMZXpBQzY2QStibEEvbDRaRnBwaUZxYWRsZVU5N1ZNcEZLak03ST0='),
(80, 1, 76, 'MFAvQVZ2NzRhYlRocWhOTjM5YkYzL1hiaUJZWkc1VEJNc1p6MjlCMDdpND0='),
(79, 1, 76, 'ODlKbzlYd01JNDlCYlgvc2JYbDd6UmpSS0dYM05YK05DWFRidnJ5c0VtTT0='),
(78, 1, 76, 'Q3JqUmV5MGlwYUxZQWMxYmhGek9pdXczNjR0anQxZnowT2NhSnA1bVB2az0='),
(77, 1, 76, 'dWxQL3hzZ25Wdk8wc2JuV2tuakhqWFlHdkNjS3paSmxNcUZVQ01UcTEvQT0='),
(76, 1, 76, 'NTlqUTJTTU1Cd2xNaVRiSjg3Q1ZLeUl0UzlpUFYxUXdiRVMwc2lyNzdIZz0='),
(75, 1, 76, 'b3BFVk8xR3ZyVzljamw1bU1hUGRtRUFSUm54R1I5ZzBFTjFmNXV5ZUJHMD0='),
(84, 1, 76, 'YUs5Wkl5dFlMc0hjUVdxYkRGU01vR1Iza29EOHh1SDFmMjFlQ3FWdFk1UT0='),
(85, 1, 76, 'UDRVdXlOQ2FuUG1oU0JUYjlTU1BkSUgxWkJEME42NzhmTkkwN2ZlaEF2az0='),
(86, 1, 76, 'ay82Wm1KUUJmRDdqenduSHV5OGFuUE1TN2N6eHZ0YUdLaTJ3Q1N2bytZND0='),
(87, 1, 76, 'Ykx0K1ZvaEo4QXIyYUFkam9FRWczYjJ2TWVMaW9uanlWWm9CZ3pocWs5UT0='),
(88, 1, 76, 'Y1IrTklMYWpINTR3L2cvYU5DaE1pcmNoNFQ5MlVBRFZlUkpOS1JFTWgvQT0='),
(89, 1, 76, 'T1NzZnlwZXV5SEhuMlVYcWIrdG5VdXlEWWtqZjFOTE9KVVZXWFUyZW1IND0='),
(90, 1, 76, 'eVJQc1R2OEFUYmdSL0hwWk1ndmcrQmpUZ1FzM25peUNWUEdjUENUcEJEaz0='),
(91, 1, 76, 'NDN3SDFia1NnTjBlS25kSGdaV0dtOXBjTEtNTnFLdmlEeGl0UXN0ajYvbz0='),
(92, 1, 76, 'alhWZUJaYUZ0VHF2emU1bTd5LytvdTVHNVlnZm94TjQyUU1naDhJTmw0OD0='),
(93, 1, 76, 'cFRPVmN6L3BxRkhiK1Rsb0RWTmNJRXFaLy94NFJLQVBJaVlMRjVYTVIzaz0='),
(94, 1, 76, 'Rk5obXBnZ2xydlhLcHIxN1oyMnVPcEtQVTlQTGcrWHZpczNoNTc0UHdPMD0='),
(95, 2, 44, 'Q1lTZnhrLzJ4WHhWTEd6MjZvOG5vaWpWUWJZYVp5bzArb3NXaTRhTjdvOD0='),
(96, 2, 44, 'WXhJSzZFWUIrOUhHY0NJWndoMjZRMjhNaW9RRmNRS3pPTGhqV0Y1ZTZLQT0='),
(97, 2, 44, 'SW1ka093SUpjQWMzZk81MXNUWmIvVnVRWkRjOHNtMXZLeGRMTFd3amw5VT0='),
(98, 2, 44, 'TUV1YXpMZFJWRVZaWEFlTWZ3RW55YzhpWGl1bEx1eW9EWUJ2S3pzSU1sbz0='),
(99, 2, 44, 'czUwbTZGOGdkcVM2UUhKWjdzR2FBRCs0OEtRekx3eExFT1R4a0hCYUlFdz0='),
(100, 2, 44, 'THZnaXY1MFhBQjFqTkx4UnJCTlI3em5aSThLZEg2YWJFWGJFb1pXSk55MD0='),
(101, 2, 44, 'WWZUd0dFbXptR3lhYXViK2dkUlBBLzVSSURNTzZ0d2tCMHFPZjBaeVlJMD0='),
(102, 1, 68, 'ZDRjdkZDSmtDeUxKQ25HL0FsWEkwamwrMTdBbkEvTW12Y0FSVTllRUJ6MD0='),
(103, 1, 68, 'UTJTNkdBZ0FmN2Y5bnNqdytSdkMrbHNZdWcxd3dlUHRKRW9MOHRDMDdnYz0='),
(104, 1, 68, 'SGRmUEVSNXoySTUwZ2E3OHorb29Va2N2QitzdDZSNGJzcFlYOGRaaG9aND0='),
(105, 1, 68, 'Z3h4VWlObWxnWXdBNnlpL2lhUGhsZURWTmhBamJhcUp6aTd5SXRnWjVmbz0='),
(106, 1, 68, 'bTRaMS8zd0IvSmxYY3Q3S2ZlYUVmQWJUSEJJVzI3ZEtUbnBhR0tqSEFPQT0='),
(107, 1, 68, 'clNaT3Y0WHVXcGpKM0d2ckgrMm1XZEhEekw1MG9iU2RmS0kycUNSYjZIQT0='),
(108, 1, 68, 'UCs4TGlQWXFaMzFUMHpKeEdyQXg3Tnlwa2JDbmJjZWlWNUxLb2lXM3ZpND0='),
(109, 1, 68, 'TFZkcTIvTFRBYVZUdFp2U1Vvc1h1dm1oMUpCS3NQaEtYWnN6R3VuaGRTbz0='),
(110, 1, 68, 'enV5WmZvbmNhYzlqVGNGU3BXTjZrRGNpekFqQmNmcS9ENmp1VUFvbFpqWT0='),
(111, 1, 68, 'ZEpyUStpWnk5a2tJVDVEVEdXRTAxVHhua3ZrZ2VVQ05QbnRwTnI0c1hmVT0='),
(112, 1, 68, 'eDVHUGZ0djllZy9RZDduUXVOeVdWRzUrN0lLYVdoajYvVEwxMENEYi9yaz0='),
(113, 1, 68, 'Rk1DcWVOQittM29aaWtuZVVuamt2SWt2cHVldGFNUElnUVlIdzE5bzhFYz0='),
(114, 1, 68, 'Rk02OWN3UjFBeUtQK1J3Q3J3YUdvN3lxdStlWm5oL3VqRjE2SklGMnlJQT0='),
(115, 2, 55, 'Rzl2amRwZGVmbHRYT0VjMTRWOTlweDhIbENqN3BWU0R3NytZUEhMKzZaYz0='),
(116, 1, 72, 'WHduYVRTTVRyVUJUbUJGcFlsYUF1SE4vcUVsN1pMeHBjWVU4YXBQSGlocz0='),
(117, 1, 6, 'Vm4xUmRQYjFxdGpZazFDekZBQW1pWVlObFdYQ096KzlwcGJmS2xCY2ZFcz0='),
(118, 1, 6, 'ZmRZU2Vhc2IrRjJ3cjBlMkVTMFoyenowbi9wc3dGb0lWY0RJQ0ttMmd5cz0='),
(119, 1, 6, 'L2VteWpLc3Nqc0hnRTNEdzdWZk0zSVlCOEFUbzhWWXp3SUs1WHZhSXloMD0='),
(120, 1, 7, 'aFlRcjlFSVJzWExNVTJ0N3Y4WUVSaDF3Y1dTRWhJQ3U3OFR1MUh1UGtOWT0='),
(121, 1, 8, 'a3h6bGtETXNONXFiYktvN2NSKzdPR3JMS3hreXVTTjZxZElPZml2cDlhUT0='),
(122, 1, 9, 'dWFxVkl1RzJvNlk0YWVqOXAyZ1cxY1hZcFB4N1lQcVpzU1Y3Yk0zYitMZz0='),
(123, 1, 11, 'cmt6Uk9UczJ0a1FRdk80YWR0U25xbVo1R2lWUThZZHhQY01mSTBZcmgwUT0='),
(124, 1, 68, 'Q0h1aHNFdDBpNUhqVGwwTDZiNktNd3pMQUFoVDRFQU1lQVhDbExIRHFsaz0='),
(125, 1, 68, 'K1NkNW1BUlBYY1RDT25uYjJlbHkzOHovNVZ6Y0E0OXI4VzJhQzBUeXhvbz0='),
(126, 1, 68, 'MW5OZkhvbnlkaVZUVjVjaS9weS8yN0hRSEovK0lpOEpwTVdJMWUybUV4ST0='),
(127, 1, 68, 'bzdqUW1OZHcrN3BqWC9NRUVzQjVqZUMrUU9xcHhBZlh0N2VZVlc2MTJsWT0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platforms`
--

CREATE TABLE `platforms` (
  `platformId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `clientId` int(11) DEFAULT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platforms`
--

INSERT INTO `platforms` (`platformId`, `nombre`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `clientId`) VALUES
(1, 'Innerworks', 'innerworks_logo_5-innerworks.png', 'innerworks_logo_black-innerworks.png', 'hero-new-light-innerworks.otf', NULL, 'innerworks', 'roman@romanf.com', 3),
(2, 'Ephata', 'ephata.png', 'ephata.png', 'roboto-regular.ttf', NULL, 'ephata', 'elopez@junkyard.mx', 4),
(3, 'Nierika', 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', 'roboto-regular-nierika.ttf', NULL, 'nierika', 'universidad@nierika.com', 7),
(4, 'Test', 'zenrise_logo_01_black_background-test.png', 'zenrise_logo_01_black_background-test.png', 'hero-new-light-test.otf', NULL, 'test', 'elopez@junkyard.mx', 8),
(5, 'Shala', 'logo-shala.png', 'logo-shala.png', 'roboto-regular-shala.ttf', NULL, 'shala', 'rome7f@gmail.com', 9),
(6, 'Eleutheria', 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', 'roboto-regular-eleutheria.ttf', NULL, 'Eleutheria', 'rome7f@gmail.com', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platformservices`
--

CREATE TABLE `platformservices` (
  `psId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=servicescatalog'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platformservices`
--

INSERT INTO `platformservices` (`psId`, `platformId`, `catalogId`) VALUES
(80, 1, 5),
(79, 1, 4),
(78, 1, 3),
(77, 1, 2),
(76, 1, 1),
(35, 2, 5),
(34, 2, 4),
(33, 2, 3),
(32, 2, 2),
(31, 2, 1),
(46, 3, 1),
(47, 3, 2),
(48, 3, 3),
(49, 3, 4),
(50, 3, 5),
(105, 4, 5),
(104, 4, 4),
(103, 4, 3),
(102, 4, 2),
(101, 4, 1),
(100, 5, 5),
(99, 5, 4),
(98, 5, 3),
(97, 5, 2),
(96, 5, 1),
(106, 6, 1),
(107, 6, 2),
(108, 6, 3),
(109, 6, 4),
(110, 6, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plugincontent`
--

CREATE TABLE `plugincontent` (
  `pluginContentId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `titulo` varchar(200) NOT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `description` varchar(1000) NOT NULL,
  `pluginLink` varchar(200) DEFAULT NULL,
  `accessId` int(11) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `circleCoachLock` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `plugincontent`
--

INSERT INTO `plugincontent` (`pluginContentId`, `clientId`, `coachId`, `titulo`, `imagen`, `description`, `pluginLink`, `accessId`, `orden`, `circleCoachLock`) VALUES
(4, 3, 6, 'StoryMatcher Test', 'experiencing-you-thumbnail1635005625.jpg', 'This is a test', NULL, 2, NULL, 1),
(9, 3, 1, 'test1', 'sircle-thumbnail1635006977.jpg', 'test1', NULL, 2, NULL, 1),
(10, 3, 35, 'StoryMatcher test', 'ephata-circle-pic-21635558720.jpg', 'Test', NULL, 2, NULL, 1),
(11, 3, 35, 'Test SM', 'environmental-awareness1635560708.jpg', 'Test', NULL, 2, NULL, 1),
(12, 3, 1, 'testing 2', 'sircle-thumbnail1635006977.jpg', 'testing 2', NULL, 2, NULL, 1),
(13, 3, 55, 'StoryMatcher test', 'fraendi-logo21635210562.png', 'Test', NULL, 2, NULL, 1),
(14, 3, 35, 'StoryMatcher Test', 'environmental-awareness1635560708.jpg', 'Test', NULL, 2, NULL, 1),
(15, 3, 1, 'StoryMatcher test', 'reclaim-your-flow-thumbnail1635008296.jpg', 'Test', NULL, 2, NULL, 1),
(16, 3, 55, 'Leadership Profile Assessment', '1642091082.jpg', 'This assessment of leadership profile uses Fraendi’s StoryMatcher system. Each participant selects and rates 3 to 6 stories to complete the assessment.', 'http://3.135.29.166/StoryMatcher/?user=1', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plugincontentcircle`
--

CREATE TABLE `plugincontentcircle` (
  `pluginContentCircleId` int(11) NOT NULL,
  `pluginContentId` int(11) NOT NULL COMMENT 'opciones=plugincontent',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `plugincontentcircle`
--

INSERT INTO `plugincontentcircle` (`pluginContentCircleId`, `pluginContentId`, `circleId`) VALUES
(4, 4, 30),
(11, 9, 35),
(13, 10, 39),
(14, 11, 40),
(15, 12, 35),
(16, 13, 33),
(17, 14, 40),
(18, 15, 36),
(19, 16, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postcategory`
--

CREATE TABLE `postcategory` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `postcategory`
--

INSERT INTO `postcategory` (`categoryId`, `clientId`, `name`) VALUES
(1, 3, 'Category One'),
(2, 3, 'Category Two'),
(3, 8, 'Category One'),
(4, 8, 'Category Two');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `postId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `content` text NOT NULL COMMENT 'html',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`postId`, `clientId`, `categoryId`, `title`, `image`, `video`, `content`, `fecha`) VALUES
(1, 3, 1, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(2, 3, 1, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T FORCE IT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">KEEP IT SHORT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">TRY SOME BREATHWORK</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T GET HUNG UP ON FORM</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATE ON THE MOVE</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATION ISN’T PERFECT!</strong></h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-03-15'),
(3, 3, 2, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">7 a.m.</strong> <br style=\"overflow-wrap: break-word;\">Wake up. This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-03-16'),
(4, 8, 3, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(5, 8, 21, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T FORCE IT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">KEEP IT SHORT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">TRY SOME BREATHWORK</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T GET HUNG UP ON FORM</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATE ON THE MOVE</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATION ISN’T PERFECT!</h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-04-08'),
(6, 8, 21, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', 'MTEwNzI1MQ==', '<p>Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p><span style=\"font-weight: bold;\">7 a.m. Wake up.</span></p><p>This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-04-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionnaire`
--

CREATE TABLE `questionnaire` (
  `questionId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `question` varchar(1000) NOT NULL COMMENT 'nombre',
  `answerOne` varchar(200) NOT NULL,
  `answerTwo` varchar(200) NOT NULL,
  `answerThree` varchar(200) DEFAULT NULL,
  `answerFour` varchar(200) DEFAULT NULL,
  `answerFive` varchar(200) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `questionnaire`
--

INSERT INTO `questionnaire` (`questionId`, `clientId`, `question`, `answerOne`, `answerTwo`, `answerThree`, `answerFour`, `answerFive`, `position`) VALUES
(1, 3, 'How did you learn about Innerworks?', 'Invited by a Guide/Coach', 'Online search', 'Through company HR', 'Referred by a friend', 'Other', 1),
(6, 4, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(7, 7, 'What is your interest in visiting Nierika?', 'Lodging', 'Therapy', 'Volunteer', 'Retreat', 'Event space', 1),
(8, 7, 'How did you find out about us?', 'Referral', 'Webpage', 'Web search', 'Other', NULL, 2),
(9, 8, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(10, 8, 'Question Two', 'One', 'Two', 'Three', 'Four', 'Five', 2),
(11, 8, 'Question Three', 'One', 'Two', 'Three', 'Four', 'Five', 3),
(12, 8, 'Question Four', 'One', 'Two', 'Three', 'Four', 'Five', 4),
(13, 8, 'Question Five', 'One', 'Two', 'Three', 'Four', 'Five', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seriecircle`
--

CREATE TABLE `seriecircle` (
  `serieCircleId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serierequest`
--

CREATE TABLE `serierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;multiple=circles,seriecircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`serieId`, `clientId`, `coachId`, `nombre`, `description`, `photo`, `video`, `accessId`) VALUES
(14, 3, 1, 'The Loop', 'The Loop is a course in mastering and manifesting change and transformation in your life. The course entails a total of 10 episodes, out of which 4 are optional as live sessions together with Nils.', 'yinyang.jpeg', 'MTEwNzIwNA==', 2),
(16, 4, 18, 'Hero\'s Journey', NULL, NULL, NULL, 3),
(17, 4, 18, 'Ephata Summary', NULL, NULL, NULL, 3),
(18, 3, 6, 'Meditations', 'This series will help you get started with meditation through my short guided exercises.', 'top-5-scientific-findings-on-meditationmindfulness.jpeg', 'MTA3NDU2MA==', 2),
(22, 4, 3, 'Ephata Series', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '42-15833533.jpg', 'MTA4NTMxMg==', 1),
(31, 3, 1, 'Sircle Collection', 'Welcome!', 'skärmavbild-2021-08-30-kl-0001201630274515.png', NULL, 2),
(32, 3, 0, 'Test Video', 'Test', 'stress1630332544.jpg', 'MTE1NjQwMQ==', 2),
(29, 3, 1, 'Guided Meditations', 'Guided meditations for self-understanding, self-grounding and self-leadership.', 'isabell-winter-lzyzedj8fbo-unsplash1630071361.jpg', NULL, 2),
(30, 3, 1, 'Flow, Growth & Direction (FGD)', 'FGD is a series of hands-on tools, strategies and exercises for self-understanding, self-grounding and self-leadership.', 'artem-kovalev-fk3xucftavk-unsplash1630071431.jpg', NULL, 2),
(33, 3, 55, 'Action Learning', 'Fraendi Pilot Program', 'fraendi_logo_ohne_rand-removebg1630508856.png', NULL, 2),
(34, 3, 6, 'Sircle Collection', 'Meditations hosted by Mattis', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seriescategories`
--

CREATE TABLE `seriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seriescategories`
--

INSERT INTO `seriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(69, 14, 4),
(17, 19, 2),
(13, 20, 3),
(18, 21, 2),
(23, 13, 1),
(19, 21, 4),
(24, 22, 7),
(96, 31, 4),
(29, 25, 2),
(30, 23, 4),
(31, 26, 13),
(68, 14, 2),
(67, 14, 1),
(42, 28, 4),
(99, 29, 2),
(98, 30, 4),
(97, 30, 2),
(63, 32, 2),
(91, 33, 4),
(94, 18, 2),
(95, 34, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicescatalog`
--

CREATE TABLE `servicescatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicescatalog`
--

INSERT INTO `servicescatalog` (`catalogId`, `nombre`) VALUES
(1, 'Schedulling Calls'),
(2, 'Pre-Recorded Videos'),
(3, 'Live Video Conferences'),
(4, 'Interactive Exercises & Tests'),
(5, 'Reporting');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessionrecording`
--

CREATE TABLE `sessionrecording` (
  `sessionRecordingId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sessionrecording`
--

INSERT INTO `sessionrecording` (`sessionRecordingId`, `callId`, `videoId`) VALUES
(13, 137, 65),
(14, 147, 63);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shala`
--

CREATE TABLE `shala` (
  `shalaId` int(11) NOT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `spotlightrApiKey` varchar(200) DEFAULT NULL,
  `stripePublicKey` varchar(200) DEFAULT NULL,
  `stripePrivateKey` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shala`
--

INSERT INTO `shala` (`shalaId`, `logotipo`, `icono`, `spotlightrApiKey`, `stripePublicKey`, `stripePrivateKey`) VALUES
(1, '', '', 'c53bcd228d6d73879a567d77fa35c5368d390ecb', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slidehome`
--

CREATE TABLE `slidehome` (
  `slideId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `imagen` varchar(200) NOT NULL COMMENT 'archivo',
  `imagenMovil` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `altText` varchar(200) DEFAULT NULL,
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slidehome`
--

INSERT INTO `slidehome` (`slideId`, `clientId`, `imagen`, `imagenMovil`, `altText`, `posicion`) VALUES
(1, 7, 'slide1.jpg', 'slide1movil.jpg', NULL, 1),
(2, 7, 'slide2.jpg', 'slide2movil.jpg', NULL, 2),
(3, 7, 'slide3.jpg', 'slide3movil.jpg', NULL, 3),
(4, 7, 'slide4.jpg', 'slide4movil.jpg', NULL, 4),
(5, 7, 'slide5.jpg', 'slide5movil.jpg', NULL, 5),
(6, 9, 'slider1png.png', NULL, 'All-in-one website builder for secured platforms with LIVE content and more …', 1),
(11, 9, 'slider6png.png', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statusaccount`
--

CREATE TABLE `statusaccount` (
  `statusId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `statusaccount`
--

INSERT INTO `statusaccount` (`statusId`, `nombre`) VALUES
(1, 'Inactive'),
(2, 'Active');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoadmin`
--

CREATE TABLE `tipoadmin` (
  `tipoId` int(11) NOT NULL COMMENT ' multiple=accessadmin,accessuseradmin',
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoadmin`
--

INSERT INTO `tipoadmin` (`tipoId`, `nombre`) VALUES
(2, 'Global Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipollamada`
--

CREATE TABLE `tipollamada` (
  `tipoId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipollamada`
--

INSERT INTO `tipollamada` (`tipoId`, `nombre`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered Users'),
(4, 'Circles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `todos`
--

CREATE TABLE `todos` (
  `todoId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) DEFAULT NULL COMMENT 'opciones=users',
  `coachId` int(11) DEFAULT NULL COMMENT 'opciones=coaches',
  `videoId` int(11) DEFAULT NULL COMMENT 'opciones=videos',
  `audioId` int(11) DEFAULT NULL COMMENT 'opciones=audios',
  `articleId` int(11) DEFAULT NULL COMMENT 'opciones=articles',
  `pluginContentId` int(11) DEFAULT NULL COMMENT 'opciones=plugincontent',
  `markStatus` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'unmark=0,marked=1',
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `todos`
--

INSERT INTO `todos` (`todoId`, `clientId`, `userId`, `coachId`, `videoId`, `audioId`, `articleId`, `pluginContentId`, `markStatus`, `datetime`) VALUES
(55, 3, 72, NULL, 74, NULL, NULL, NULL, 0, '2021-11-02 09:54:58'),
(61, 3, 72, NULL, NULL, NULL, 7, NULL, 0, '2021-11-02 17:03:17'),
(62, 3, 84, NULL, NULL, NULL, 7, NULL, 1, '2021-11-16 09:05:01'),
(63, 3, 84, NULL, 72, NULL, NULL, NULL, 1, '2021-11-03 09:38:15'),
(84, 3, 84, NULL, 25, NULL, NULL, NULL, 1, '2021-11-17 10:00:00'),
(96, 3, 84, NULL, 21, NULL, NULL, NULL, 1, '2021-11-18 09:00:00'),
(97, 3, 84, NULL, 23, NULL, NULL, NULL, 0, '2021-11-19 09:00:00'),
(99, 3, NULL, 1, 65, NULL, NULL, NULL, 1, '2021-11-19 09:00:00'),
(100, 3, 84, NULL, 20, NULL, NULL, NULL, 1, '2021-11-20 09:00:00'),
(101, 3, 84, NULL, NULL, NULL, 8, NULL, 0, '2021-11-20 09:00:00'),
(102, 3, NULL, 1, 54, NULL, NULL, NULL, 0, '2021-11-24 09:00:00'),
(103, 3, NULL, 55, NULL, NULL, 6, NULL, 0, '2021-12-15 09:00:00'),
(104, 3, NULL, 1, 57, NULL, NULL, NULL, 0, '2021-11-23 10:00:00'),
(105, 3, NULL, 1, NULL, 70, NULL, NULL, 0, '2021-11-30 09:00:00'),
(106, 3, NULL, 1, NULL, NULL, 9, NULL, 0, '2021-12-15 06:00:00'),
(107, 3, 84, NULL, 30, NULL, NULL, NULL, 0, '2021-12-15 15:39:14'),
(108, 3, 84, NULL, 30, NULL, NULL, NULL, 0, '2021-12-15 15:39:14'),
(109, 3, 84, NULL, NULL, NULL, 5, NULL, 0, '2021-12-15 15:49:16'),
(110, 3, NULL, 1, 61, NULL, NULL, NULL, 0, '2021-12-17 10:46:00'),
(111, 3, NULL, 1, NULL, 69, NULL, NULL, 0, '2021-12-27 15:32:33'),
(112, 3, NULL, 1, NULL, NULL, NULL, 9, 1, '2022-01-12 17:55:55'),
(113, 3, NULL, 35, NULL, NULL, NULL, 10, 0, '2022-01-17 13:03:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typemail`
--

CREATE TABLE `typemail` (
  `typeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typemail`
--

INSERT INTO `typemail` (`typeId`, `nombre`) VALUES
(1, 'Password Recovery'),
(2, 'Contact Web'),
(3, 'Rejected Coach'),
(4, 'Approved Coach'),
(5, 'Registered Coach'),
(6, 'Requested Series Access'),
(7, 'Requested Coach'),
(8, 'Approved Member'),
(9, 'Rejected Member'),
(10, 'Approval coach request '),
(11, 'Approval member request'),
(12, 'Email Confirmation');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userarticle`
--

CREATE TABLE `userarticle` (
  `userArticleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userarticleserie`
--

CREATE TABLE `userarticleserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userarticleserie`
--

INSERT INTO `userarticleserie` (`userSerieId`, `userId`, `serieId`) VALUES
(2, 73, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `useraudio`
--

CREATE TABLE `useraudio` (
  `userAudioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `useraudio`
--

INSERT INTO `useraudio` (`userAudioId`, `userId`, `audioId`) VALUES
(27, 72, 62),
(28, 72, 63),
(29, 72, 64),
(30, 72, 65),
(31, 72, 66);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercall`
--

CREATE TABLE `usercall` (
  `userCallId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercall`
--

INSERT INTO `usercall` (`userCallId`, `callId`, `userId`) VALUES
(1, 2, 3),
(2, 3, 3),
(3, 4, 3),
(4, 6, 3),
(5, 7, 3),
(6, 9, 4),
(7, 10, 4),
(8, 11, 4),
(9, 14, 7),
(10, 15, 3),
(11, 16, 3),
(12, 19, 4),
(13, 20, 4),
(14, 22, 11),
(15, 36, 15),
(16, 47, 3),
(17, 49, 3),
(18, 70, 24),
(19, 76, 4),
(20, 43, 3),
(21, 79, 34),
(22, 80, 3),
(23, 80, 4),
(24, 80, 26),
(25, 80, 34),
(30, 89, 40),
(29, 83, 40),
(31, 90, 54),
(32, 109, 70),
(55, 110, 70),
(34, 114, 70),
(35, 115, 70),
(38, 116, 70),
(39, 117, 70),
(40, 121, 70),
(42, 123, 70),
(43, 125, 70),
(54, 132, 70),
(53, 131, 70),
(56, 133, 70),
(59, 134, 73),
(66, 129, 3),
(69, 139, 72),
(72, 144, 72),
(77, 145, 72),
(78, 146, 72);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercircle`
--

CREATE TABLE `usercircle` (
  `userCircleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercircle`
--

INSERT INTO `usercircle` (`userCircleId`, `userId`, `circleId`) VALUES
(92, 73, 28),
(91, 72, 28),
(360, 86, 33),
(152, 57, 30),
(151, 56, 30),
(150, 55, 30),
(149, 54, 30),
(148, 53, 30),
(147, 52, 30),
(146, 51, 30),
(145, 50, 30),
(144, 49, 30),
(143, 48, 30),
(142, 47, 30),
(141, 46, 30),
(140, 45, 30),
(139, 44, 30),
(138, 43, 30),
(137, 42, 30),
(136, 41, 30),
(135, 40, 30),
(134, 39, 30),
(90, 35, 28),
(133, 38, 30),
(89, 9, 28),
(276, 84, 32),
(275, 73, 32),
(274, 72, 32),
(273, 35, 32),
(272, 16, 32),
(271, 9, 32),
(359, 84, 33),
(358, 72, 33),
(357, 60, 33),
(356, 11, 33),
(355, 8, 33),
(342, 92, 38),
(354, 7, 33),
(169, 84, 34),
(168, 73, 34),
(341, 91, 38),
(477, 81, 31),
(476, 80, 31),
(340, 90, 38),
(228, 81, 35),
(227, 80, 35),
(226, 79, 35),
(225, 78, 35),
(224, 77, 35),
(223, 76, 35),
(222, 75, 35),
(221, 74, 35),
(220, 73, 35),
(339, 89, 38),
(244, 84, 36),
(243, 83, 36),
(242, 82, 36),
(241, 81, 36),
(240, 80, 36),
(239, 79, 36),
(238, 78, 36),
(237, 77, 36),
(236, 76, 36),
(235, 75, 36),
(234, 74, 36),
(233, 73, 36),
(388, 86, 37),
(229, 82, 35),
(230, 83, 35),
(231, 84, 35),
(361, 87, 33),
(353, 6, 33),
(338, 88, 38),
(337, 87, 38),
(336, 86, 38),
(335, 84, 38),
(362, 88, 33),
(333, 60, 38),
(363, 89, 33),
(364, 92, 33),
(387, 84, 37),
(386, 72, 37),
(385, 11, 37),
(384, 9, 37),
(383, 8, 37),
(382, 7, 37),
(381, 6, 37),
(475, 79, 31),
(474, 78, 31),
(473, 77, 31),
(472, 76, 31),
(471, 75, 31),
(470, 74, 31),
(469, 73, 31),
(468, 68, 31),
(467, 67, 31),
(466, 58, 31),
(465, 57, 31),
(464, 56, 31),
(463, 55, 31),
(462, 54, 31),
(461, 53, 31),
(460, 52, 31),
(459, 51, 31),
(458, 50, 31),
(457, 49, 31),
(456, 48, 31),
(455, 47, 31),
(454, 46, 31),
(453, 45, 31),
(452, 44, 31),
(451, 43, 31),
(450, 42, 31),
(449, 41, 31),
(448, 40, 31),
(447, 39, 31),
(446, 38, 31),
(445, 16, 31),
(478, 82, 31),
(479, 83, 31),
(480, 84, 31),
(483, 73, 39),
(484, 84, 39),
(485, 73, 40),
(486, 84, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userclient`
--

CREATE TABLE `userclient` (
  `userClientId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userclient`
--

INSERT INTO `userclient` (`userClientId`, `userId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(2, 1, 3, 'N', NULL),
(9, 3, 3, 'N', NULL),
(8, 2, 3, 'N', NULL),
(10, 4, 3, 'N', NULL),
(11, 5, 3, 'N', NULL),
(12, 6, 3, 'N', NULL),
(13, 3, 3, 'N', NULL),
(14, 3, 3, 'N', NULL),
(15, 7, 3, 'N', NULL),
(16, 8, 3, 'N', NULL),
(17, 4, 3, 'N', NULL),
(18, 9, 3, 'N', NULL),
(19, 10, 3, 'N', NULL),
(20, 11, 3, 'N', NULL),
(21, 12, 3, 'N', NULL),
(22, 12, 3, 'N', NULL),
(23, 13, 3, 'N', NULL),
(24, 13, 3, 'N', NULL),
(25, 14, 3, 'N', NULL),
(26, 15, 3, 'N', NULL),
(27, 16, 3, 'N', NULL),
(28, 17, 4, 'N', NULL),
(29, 18, 4, 'N', NULL),
(30, 19, 3, 'N', NULL),
(31, 3, 3, 'N', NULL),
(32, 11, 1, 'N', NULL),
(33, 20, 1, 'N', NULL),
(34, 21, 3, 'N', NULL),
(35, 22, 7, 'N', NULL),
(36, 23, 7, 'N', NULL),
(66, 4, 8, 'N', NULL),
(65, 3, 8, 'N', NULL),
(64, 25, 3, 'N', NULL),
(63, 10, 8, 'N', NULL),
(67, 26, 3, 'N', 'test'),
(68, 27, 8, 'N', NULL),
(69, 28, 8, 'N', 'ascasc'),
(70, 30, 3, 'S', NULL),
(71, 31, 8, 'N', NULL),
(72, 32, 8, 'S', NULL),
(73, 33, 8, 'S', NULL),
(74, 34, 3, 'N', NULL),
(75, 35, 3, 'N', NULL),
(76, 36, 3, 'S', NULL),
(77, 37, 3, 'N', NULL),
(78, 38, 3, 'S', NULL),
(79, 39, 3, 'S', NULL),
(80, 40, 3, 'N', NULL),
(81, 41, 3, 'N', NULL),
(82, 42, 3, 'N', NULL),
(83, 43, 3, 'N', NULL),
(84, 44, 3, 'S', NULL),
(85, 45, 3, 'N', NULL),
(86, 46, 3, 'N', NULL),
(87, 47, 3, 'N', NULL),
(88, 48, 3, 'S', NULL),
(89, 49, 3, 'S', NULL),
(90, 50, 3, 'N', NULL),
(91, 51, 3, 'N', NULL),
(92, 52, 3, 'N', NULL),
(93, 53, 3, 'S', NULL),
(94, 54, 3, 'N', NULL),
(95, 55, 3, 'N', NULL),
(96, 56, 3, 'N', NULL),
(97, 57, 3, 'N', NULL),
(98, 58, 3, 'N', NULL),
(99, 59, 9, 'S', NULL),
(100, 60, 3, 'N', NULL),
(101, 61, 9, 'S', NULL),
(102, 62, 3, 'S', NULL),
(103, 63, 3, 'S', NULL),
(104, 64, 3, 'S', NULL),
(105, 65, 9, 'S', NULL),
(106, 66, 9, 'S', NULL),
(107, 67, 3, 'N', NULL),
(108, 68, 9, 'S', NULL),
(109, 69, 9, 'S', NULL),
(110, 68, 3, 'N', NULL),
(111, 70, 9, 'N', NULL),
(112, 71, 9, 'S', NULL),
(113, 72, 3, 'N', NULL),
(114, 73, 3, 'N', NULL),
(115, 74, 3, 'N', NULL),
(116, 75, 3, 'N', NULL),
(117, 76, 3, 'N', NULL),
(118, 77, 3, 'N', NULL),
(119, 78, 3, 'N', NULL),
(120, 79, 3, 'N', NULL),
(121, 80, 3, 'N', NULL),
(122, 81, 3, 'N', NULL),
(123, 82, 3, 'N', NULL),
(124, 83, 3, 'N', NULL),
(125, 84, 3, 'N', NULL),
(126, 85, 3, 'S', NULL),
(127, 86, 3, 'S', NULL),
(128, 87, 3, 'N', NULL),
(129, 88, 3, 'S', NULL),
(130, 89, 3, 'S', NULL),
(131, 90, 3, 'N', NULL),
(132, 91, 3, 'S', NULL),
(133, 92, 3, 'N', NULL),
(134, 93, 9, 'S', NULL),
(135, 94, 9, 'S', NULL),
(136, 95, 9, 'S', NULL),
(137, 96, 9, 'S', NULL),
(138, 97, 9, 'S', NULL),
(139, 98, 9, 'S', NULL),
(140, 99, 9, 'S', NULL),
(141, 100, 3, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`userId`, `statusId`, `nombre`, `apellido`, `foto`, `email`, `pass`, `emailConfirmed`, `fecha`) VALUES
(3, 2, 'Eduardo ', 'Lopez', 'teaser-uhmay.jpg', 'elopez@junkyard.mx', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-05'),
(4, 2, 'Gabo Student', 'Student', NULL, 'gabofc@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-17'),
(5, 2, 'Test', 'Fraendi', 'fraendi-logo21635510701.png', 'test@fraendi.org', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-10-29'),
(6, 2, 'Tilman', 'Sauerwein', '1635512260.png', 'til@permakulturpark.de', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(7, 2, 'Maria', 'Deckeman', '1635516673.png', 'maria.deckeman@ki.se', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(8, 2, 'Estelle', 'Bessac', '1635516926.png', 'estelle.bessac@orange.fr', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(9, 2, 'Joanne', 'Taylor', '1635517216.png', 'joanne_taylor3941@hotmail.com', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(30, 1, 'Eduardo Raymundo', 'Villagran', NULL, 'eduardo_raymundo-deactivated-2021-05-03@hotmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-13'),
(11, 2, 'Ilka', 'Kass', '1635517597.png', 'kontakt@ilkakass.de', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(12, 1, 'bb2', 'bb2', NULL, 'test-deactivated-2021-05-03@me.com', 'cXZLWUs2a3V5aDF0aE9PT2xUUDQzdz09', 'S', '2021-01-19'),
(13, 1, 'Iván', 'Martin', '4880-327.jpg', 'martin.maseda-deactivated-2021-05-03@gmail.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(14, 1, 'Ivan test', 'Test', NULL, 'ivan-deactivated-2021-05-03@test.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(15, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-25'),
(16, 2, 'Nils', 'von Heijne 2', NULL, 'nils@worldofwisdom.io', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-26'),
(17, 2, 'EDUARDO', 'LOPEZ', NULL, 'prueba@estudiante.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-28'),
(18, 2, 'Roman', 'Fernandez', NULL, 'roman@ephata.me', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-01-28'),
(19, 1, 'Gabo', 'FC', NULL, 'gabofc+t100-deactivated-2021-05-03@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-02-09'),
(20, 2, 'Johnathan', 'Meade', NULL, 'john@innerworks.io', 'YW9WODhydnBMTThSVUhINyszWkkvZz09', 'S', '2021-03-03'),
(22, 2, 'Roman', 'Fernandez', NULL, 'rome@rome.net', 'ZUYyL2VzcmZMT1RLU01OTzhGOUtZUT09', 'S', '2021-03-04'),
(23, 2, 'john ', 'doe', NULL, 'babaji@juice.net', 'dWMwTEVJL1UzVnozUU5aM1hZWnBJQT09', 'S', '2021-03-04'),
(24, 2, 'StudentSanja', 'Menicanin', NULL, 'hello@sanjamenicanin.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', '2021-03-17'),
(25, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@email.com', 'bkpYdm05ZUJRaUJ5NkZ6bUVXbjdvUT09', 'S', '2021-03-26'),
(26, 1, 'Test', 'User', NULL, 'test-deactivated-2021-05-03@aprobado.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-09'),
(27, 2, 'Gabriel', 'Test Estudiante', NULL, 'gabofc+student_test1@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-04-11'),
(28, 2, 'Eduardo Raymundo', 'Villagran', NULL, 'lourdes.serrano.tun@gmail.com', 'Um0wYlVaNEZtRkd4OGFWWGQ2Mk1OUT09', 'S', '2021-04-12'),
(31, 2, 'test gabo ', 'gabo test', NULL, 'gabofc+testvalidation@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-04-14'),
(32, 2, 'Gabo 2', 'Gabo 2', NULL, 'web@neotegra.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'N', '2021-04-14'),
(33, 2, 'dasda', 'asdas', NULL, 'asdasda@asdadds.com', 'clJmM2xTUTcyVVNJTFlseEEwWm95U2lSdENyN1FOVmpVUG4wbFZ4SGhkcz0=', 'N', '2021-04-14'),
(34, 1, 'Test', 'User', NULL, 'rinz7-deactivated-2021-05-03@hotmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-14'),
(35, 1, 'Stephanie', 'Wilson', NULL, 'rome7f-deactivated-2021-05-03@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-30'),
(36, 2, 'eris', 'diel', NULL, 'eris@diel.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'N', '2021-05-01'),
(37, 2, 'Gunilla', 'Rubin', NULL, 'gunilla.rubin@sweco.se', 'NnRtUnVjZTZ2NC9lRXlWeEhqaXY5Zz09', 'S', '2021-08-10'),
(38, 2, 'Anja', 'Thernström', NULL, 'anja.thernstrom@sweco.se', 'eWhuVGxIU1U5S2F0bUZ4RTV0cUFydz09', 'N', '2021-05-02'),
(39, 2, 'Cecilia ', 'Engvall', NULL, 'cissiande@hotmail.com', 'aStnelp2TFN5Y2owclBUS2paanFsUT09', 'N', '2021-05-02'),
(40, 2, 'Malin', 'Magnusson', NULL, 'malin.k.magnusson@gmail.com', 'VU55U3BSTmYxc1V1WFhnQjdWOERadz09', 'S', '2021-05-02'),
(41, 2, 'Dagmara', 'Fajkis-Lau', NULL, 'dagmara.fajkis@gmail.com', 'OXBFbGFrRVA5UE4wZUdLMWZ6ZDdjUT09', 'S', '2021-05-02'),
(42, 2, 'Moa', 'Andreasson Gistedt', NULL, 'moa.gistedt@gmail.com', 'NE5aTFUzV2xTSGxEczcvOHZtZkNSdz09', 'S', '2021-05-02'),
(43, 2, 'Theres', 'Kvarnström', NULL, 'theres.kvarnstrom@sweco.se', 'SEhreDc5V3dDWFJUMTR3YkZoZnpPUT09', 'S', '2021-05-03'),
(44, 2, 'Mari', 'Wennlund', NULL, 'mari.wennlund@sweco.se', 'S2pNeE9yU1RocmFHbzAwT2hXLzFMUT09', 'N', '2021-05-03'),
(45, 2, 'Peter', 'Westerberg', NULL, 'peterwesterberg2@gmail.com', 'VWViNklTZ0RBby9FOGJmTzlhWGhXdz09', 'S', '2021-05-03'),
(46, 2, 'Isa', 'Brisby', NULL, 'isa.brisby@sweco.se', 'djk3dXp4K3ozeW0xc3Mwd0hrTkhiQT09', 'S', '2021-05-03'),
(47, 2, 'Helena', 'Törmä', NULL, 'haetii@gmail.com', 'ZTkzbWVpS21SVW1WL2VEeTN5Q0R0ZkZONlA1Mm9JZjNvbXdjTk1SV3NDTT0=', 'S', '2021-05-04'),
(48, 2, 'Marie', 'Alke', NULL, 'marie.alke@hotmail.com', 'M1ZId3VVZUo3STFrcUVTQkRtUnljZz09', 'N', '2021-05-04'),
(49, 2, 'Caroline', 'Hansson', NULL, 'hansson_120@hotmail.com', 'cm1GeU9VTDZKbFB2QWdFbVcweXpKQT09', 'N', '2021-05-05'),
(50, 2, 'Niklas', 'Gripenstam', NULL, 'niklas.gripenstam@hotmail.com', 'L1NsY1dyWDdIcFBBeHhjWDRnamE5dz09', 'S', '2021-05-05'),
(51, 2, 'Helia', 'Tehranchi', NULL, 'helia.tehranchi@sweco.se', 'MU1XVkU3QVhvSnlZa0Jlc2JlRVIzUT09', 'S', '2021-05-05'),
(52, 2, 'Sofia ', 'Gärde', NULL, 'sofia.m.garde@gmail.com', 'NWFyY1hWdTFXQnVPQUJ6eW9wdTJhdz09', 'S', '2021-05-05'),
(53, 2, 'Monika', 'Harvey', NULL, 'monika.harvey@hotmail.se', 'VUFEMzhSVlZJbEk4eXJBRXZWVVJBZz09', 'N', '2021-05-07'),
(54, 2, 'Monika', 'Harvey', NULL, 'monika.harvey@hotmail.com', 'T0tsSTEyT0JGT1lQY1RCclFtMjZhZz09', 'S', '2021-05-07'),
(55, 2, 'Maria', 'Strannelind', NULL, 'miastrannelind@hotmail.com', 'WmZMM0V6dG9IeDJla2pXN2tSWExXUT09', 'S', '2021-05-11'),
(56, 2, 'caroline', 'hansson', NULL, 'caroline.hansson@sweco.se', 'cm1GeU9VTDZKbFB2QWdFbVcweXpKQT09', 'S', '2021-05-12'),
(57, 2, 'Anna', 'Hanerud', NULL, 'anna.hanerud@gmail.com', 'NkdrVXdvbnR4N3JRZUhrQWtLY3JKZz09', 'S', '2021-05-19'),
(58, 2, 'Vincent', 'Guntrum', NULL, 'vincent.guntrum@posteo.de', 'dkppclBCUlFaNi9MSUNXZVV0a2lndz09', 'S', '2021-05-21'),
(59, 2, 'Switchable', 'compelling', NULL, 'Judah22@yahoo.com', 'VnBhbGhaUXJIcjU3WTVMVDh4Q3JTdz09', 'N', '2021-05-26'),
(60, 2, 'Rainer', 'von Leoprechting', 'rainer20201629630997.jpg', 'rainer@fraendi.org', 'Wkx2SWZyTnhzbklFU1luaFhlcmJQdz09', 'S', '2021-08-22'),
(61, 2, 'actuating', 'frame', NULL, 'konradinio@gmail.com', 'SWl1L092UUdQa0JJdWxLL05GVkFIUT09', 'N', '2021-06-04'),
(62, 2, 'Gabriela', 'Dolejsi', NULL, 'g.dolejsi@gmail.com', 'RE9CTWUwbWxUQ3B5THNEUm5kcUE3Zz09', 'N', '2021-06-16'),
(63, 2, 'Marit', 'Lissdaniels', NULL, 'marit.lissdaniels@gmail.com', 'WmUzRjZ2NzRJblVFYWNwQnFTbXpHUT09', 'N', '2021-06-16'),
(64, 2, 'Fredrik', 'Bränström', NULL, 'branstrom@gmail.com', 'SWgxOG5KR3YxckRtTjF3OEpKUnNKK3BBb1FuaEI1UkFSSi9Nb3RpaHdxcz0=', 'N', '2021-06-16'),
(65, 2, 'scale', 'Product', NULL, 'marius_stratulat@yahoo.com', 'VVF6K3FSQjVLQ3BnOUVRdEVEd0pZdz09', 'N', '2021-06-27'),
(66, 2, 'RAM', 'Mandatory', NULL, 'ute.dieter.jungclaus@t-online.de', 'RDBaY0FqKzBOSUxnSXd6bG9KclNDUT09', 'N', '2021-07-13'),
(67, 2, 'Vipin', 'Paul', NULL, 'vipin@braintechnosys.com', 'ZGxDcUdMMHE0NGJONE1LdG45Q203QT09', 'S', '2021-07-19'),
(68, 2, 'Rachana', 'Dhakern', '1630078059.jpg', 'rachana@braintechnosys.com', 'NDVKei9uaUkvU1M0cnRJVmJmWXo3UT09', 'S', '2021-07-27'),
(69, 2, 'SMS', 'deposit', NULL, 'jebbmanning@hotmail.com', 'eHNKVTZLNXVack1tYURBbm52c3crQT09', 'N', '2021-08-03'),
(70, 2, 'Evolvia', 'Test', NULL, 'roman@evolvia.mx', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-08-15'),
(71, 2, 'incubate', 'Frozen', NULL, 'barreraruperto13@gmail.com', 'blR0N0VoVklXblppYm5OQnJlSHFYQT09', 'N', '2021-08-18'),
(72, 2, 'Bernadette', 'Wesley', '1635511753.png', 'bernadette@fraendi.org', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', '2021-10-29'),
(73, 2, 'Roman', 'Fernandez', '1635528887.png', 'roman@innerworks.io', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-10-29'),
(74, 2, ' Myriel', 'Walter', NULL, 'Myriel.Walter@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(75, 2, 'Raquel', 'Corredoira', NULL, 'Raquel.Corredoira@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(76, 2, 'Adrien', 'Geenens', NULL, 'Adrien.Geenens@sircleclub.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(77, 2, 'Jana', 'Frank', NULL, 'Jana.Frank@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(78, 2, 'Manon', 'Majoor', NULL, 'Manon.Majoor@siradamhotel.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(79, 2, 'Emily', 'Orth', NULL, 'emily.orth@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(80, 2, 'Joe', 'Woods', NULL, 'J.Woods@siradamhotel.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(81, 2, 'Juliet', 'Piersma', NULL, 'juliet.piersma@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(82, 2, 'Lauren', 'Smit', NULL, 'Lauren.Smit@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(83, 2, 'Laura', 'Nolte', NULL, 'laura.nolte@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(84, 2, 'Amit', 'Paul', '1636492705.png', 'amitpaul@mac.com', 'c0tjaitXQ21pMXpFVExIekdaS0lhUT09', 'S', '2021-08-30'),
(85, 2, 'Servant', 'Conscious', NULL, 'servant@consciousmission.com', 'TG1nUVF6RDZCWk8wTXd6VFZNMFNZQT09', 'N', '2021-08-30'),
(86, 2, 'Steven', 'White', NULL, 'elenchikos@gmail.com', 'b2owQUtmbVg5RG95ZXR4MCt5OStndz09', 'N', '2021-08-31'),
(87, 2, 'Kaa', 'Faensen', NULL, 'kaa@fraendi.org', 'SHFuanJPdHl1blZwMkxpdUgrbmxiUT09', 'S', '2021-08-31'),
(88, 2, 'Sepp', 'Kirchengast', NULL, 'sepp@fraendi.org', 'VUtRUVdSS0ltbDIwMU96RFMxd2s4Zz09', 'N', '2021-08-31'),
(89, 2, 'Hendrik', 'Berberich', NULL, 'hendrik@fraendi.org', 'Zm5JWlM1eGVVSGw2TFdIdU4wYzBmdz09', 'N', '2021-08-31'),
(90, 2, 'Chris', 'Chapman', NULL, 'chrischapmaninireland@gmail.com', 'NDJ3cFh6V3Y5TTFERFo1R2ZHbkRZdz09', 'S', '2021-08-31'),
(91, 2, 'Bernhard', 'Possert', NULL, 'bernhard@possert.at', 'RlpEOFQ0cDcxYlVWaW1GL2pwdU9Jdz09', 'N', '2021-08-31'),
(92, 1, 'Jeroen', 'Vermeer', '1630673170.jpeg', 'jeroen-deactivated-2021-10-27@fraendi.org', 'dHdYb3dKUGRKSFI3dmV2UGthdFFCUT09', 'S', '2021-08-31'),
(93, 2, 'Buckinghamshire', 'Incredible', NULL, 'info@koucingopaslaugos.lt', 'S1ZRWFQzMTNUbFhva1NCYTA2S04yQT09', 'N', '2021-09-10'),
(94, 2, 'Nepal', 'Associate', NULL, 'waltandcori@gmail.com', 'Rkd5bjdxZlhqczhCZGcybmZiTnZ6dz09', 'N', '2021-09-11'),
(95, 2, 'Baby', 'Home Loan Account', NULL, 'scottieb@sympatico.ca', 'N1dEWHozVVlHNzRnNFRHNXo2eGJkUT09', 'N', '2021-09-15'),
(96, 2, 'Investment', 'Keyboard', NULL, 'stacyglover@live.ca', 'UTFZY25kRzFZNlBMNHVOUlgvWWs1Zz09', 'N', '2021-09-15'),
(98, 2, 'Rachana', 'Dhaker', NULL, 'errachana718@gmail.com', 'NDVKei9uaUkvU1M0cnRJVmJmWXo3UT09', 'N', '2021-09-16'),
(99, 2, 'Lead', 'Awesome', NULL, 'lynette.holliday@aol.com', 'NitMN3RrZWxqSUlHb0dRVGY2NUlXZz09', 'N', '2021-09-16'),
(100, 2, 'Adam', 'McKenty', NULL, 'adam@photosynthesis.ca', 'MzVMMDJPcFRXZkVaZk8xM0gzL2tZUT09', 'S', '2021-09-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userserie`
--

CREATE TABLE `userserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userserie`
--

INSERT INTO `userserie` (`userSerieId`, `userId`, `serieId`) VALUES
(1, 3, 20),
(54, 92, 33),
(7, 16, 32),
(53, 91, 33),
(52, 90, 33),
(51, 89, 33),
(50, 88, 33),
(49, 87, 33),
(48, 86, 33),
(47, 73, 33),
(46, 60, 33),
(58, 73, 31),
(57, 3, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uservideo`
--

CREATE TABLE `uservideo` (
  `userVideoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `uservideo`
--

INSERT INTO `uservideo` (`userVideoId`, `userId`, `videoId`) VALUES
(411, 83, 55),
(431, 83, 61),
(361, 3, 31),
(381, 83, 52),
(380, 82, 52),
(379, 81, 52),
(378, 80, 52),
(377, 79, 52),
(376, 78, 52),
(375, 77, 52),
(374, 76, 52),
(373, 75, 52),
(372, 74, 52),
(391, 83, 57),
(390, 82, 57),
(389, 81, 57),
(388, 80, 57),
(387, 79, 57),
(386, 78, 57),
(385, 77, 57),
(384, 76, 57),
(383, 75, 57),
(382, 74, 57),
(401, 83, 54),
(400, 82, 54),
(399, 81, 54),
(398, 80, 54),
(397, 79, 54),
(396, 78, 54),
(395, 77, 54),
(394, 76, 54),
(393, 75, 54),
(392, 74, 54),
(410, 82, 55),
(409, 81, 55),
(408, 80, 55),
(407, 79, 55),
(406, 78, 55),
(405, 77, 55),
(404, 76, 55),
(403, 75, 55),
(402, 74, 55),
(421, 83, 59),
(420, 82, 59),
(419, 81, 59),
(418, 80, 59),
(417, 79, 59),
(416, 78, 59),
(415, 77, 59),
(414, 76, 59),
(413, 75, 59),
(412, 74, 59),
(430, 82, 61),
(429, 81, 61),
(428, 80, 61),
(427, 79, 61),
(426, 78, 61),
(425, 77, 61),
(424, 76, 61),
(423, 75, 61),
(422, 74, 61),
(441, 83, 63),
(440, 82, 63),
(439, 81, 63),
(438, 80, 63),
(437, 79, 63),
(436, 78, 63),
(435, 77, 63),
(434, 76, 63),
(433, 75, 63),
(432, 74, 63),
(461, 83, 60),
(460, 82, 60),
(459, 81, 60),
(458, 80, 60),
(457, 79, 60),
(456, 78, 60),
(455, 77, 60),
(454, 76, 60),
(453, 75, 60),
(452, 74, 60),
(471, 83, 62),
(470, 82, 62),
(469, 81, 62),
(468, 80, 62),
(467, 79, 62),
(466, 78, 62),
(465, 77, 62),
(464, 76, 62),
(463, 75, 62),
(462, 74, 62),
(481, 83, 64),
(480, 82, 64),
(479, 81, 64),
(478, 80, 64),
(477, 79, 64),
(476, 78, 64),
(475, 77, 64),
(474, 76, 64),
(473, 75, 64),
(472, 74, 64),
(371, 83, 65),
(370, 82, 65),
(369, 81, 65),
(368, 80, 65),
(367, 79, 65),
(366, 78, 65),
(365, 77, 65),
(364, 76, 65),
(363, 75, 65),
(362, 74, 65),
(451, 83, 67),
(450, 82, 67),
(449, 81, 67),
(448, 80, 67),
(447, 79, 67),
(446, 78, 67),
(445, 77, 67),
(444, 76, 67),
(443, 75, 67),
(442, 74, 67),
(491, 83, 69),
(490, 82, 69),
(489, 81, 69),
(488, 80, 69),
(487, 79, 69),
(486, 78, 69),
(485, 77, 69),
(484, 76, 69),
(483, 75, 69),
(482, 74, 69);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocategories`
--

CREATE TABLE `videocategories` (
  `vcId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videocategories`
--

INSERT INTO `videocategories` (`vcId`, `videoId`, `categoryId`) VALUES
(197, 52, 2),
(198, 57, 2),
(209, 64, 2),
(199, 54, 2),
(219, 56, 4),
(200, 55, 2),
(216, 58, 4),
(203, 63, 2),
(201, 59, 2),
(207, 60, 2),
(202, 61, 2),
(162, 31, 2),
(165, 20, 4),
(164, 20, 2),
(163, 20, 1),
(168, 21, 4),
(167, 21, 2),
(166, 21, 1),
(171, 22, 4),
(170, 22, 2),
(169, 22, 1),
(174, 23, 4),
(173, 23, 2),
(172, 23, 1),
(177, 24, 4),
(176, 24, 2),
(175, 24, 1),
(180, 25, 4),
(179, 25, 2),
(178, 25, 1),
(183, 26, 4),
(182, 26, 2),
(181, 26, 1),
(186, 27, 4),
(185, 27, 2),
(184, 27, 1),
(189, 28, 4),
(188, 28, 2),
(187, 28, 1),
(192, 29, 4),
(191, 29, 2),
(190, 29, 1),
(195, 30, 4),
(194, 30, 2),
(193, 30, 1),
(208, 62, 2),
(196, 65, 2),
(204, 66, 2),
(205, 67, 2),
(206, 68, 2),
(210, 69, 2),
(159, 70, 4),
(220, 71, 4),
(222, 72, 4),
(223, 73, 4),
(224, 74, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocircle`
--

CREATE TABLE `videocircle` (
  `videoCircleId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videocircle`
--

INSERT INTO `videocircle` (`videoCircleId`, `videoId`, `circleId`) VALUES
(3, 70, 28),
(4, 31, 30),
(5, 20, 34),
(6, 21, 34),
(7, 22, 34),
(8, 23, 34),
(9, 24, 34),
(10, 25, 34),
(11, 26, 34),
(12, 27, 34),
(13, 28, 34),
(14, 29, 34),
(15, 30, 34),
(71, 65, 36),
(52, 52, 36),
(18, 57, 36),
(19, 54, 36),
(20, 55, 36),
(51, 59, 36),
(22, 61, 36),
(23, 63, 36),
(24, 66, 36),
(25, 67, 36),
(26, 68, 36),
(27, 60, 36),
(28, 62, 36),
(65, 64, 36),
(30, 69, 36),
(41, 71, 39),
(36, 58, 38),
(40, 56, 38),
(43, 72, 39),
(44, 73, 40),
(45, 74, 33),
(50, 76, 36),
(63, 78, 31),
(54, 77, 31),
(66, 79, 36),
(67, 80, 36),
(84, 89, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videorequest`
--

CREATE TABLE `videorequest` (
  `requestVideoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `videoId` int(11) NOT NULL COMMENT 'multiple=users,uservideo;multiple=coaches,coachvideo;multiple=categories,videocategories;multiple=circles,videocircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) DEFAULT NULL COMMENT 'opciones=coaches',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `orden` int(11) DEFAULT NULL,
  `circleCoachLock` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Give permission to Circle Owner',
  `coachName` varchar(255) DEFAULT NULL,
  `videoLink` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`videoId`, `clientId`, `coachId`, `video`, `titulo`, `imagen`, `description`, `accessId`, `orden`, `circleCoachLock`, `coachName`, `videoLink`) VALUES
(31, 3, 6, 'MTA3NDU2NA==', 'Life as a gift or a struggle', 'sanna_glad.jpg', 'Are you the doer or is life really happening without any effort? Try this meditation and see for your self. ', 2, 0, 1, NULL, NULL),
(20, 3, 1, 'MTA3MTU0NQ==', 'The Loop 1: Understanding the change you\'re going through', 'skärmavbild-2021-02-02-kl-163034.png', 'Introduction to The Loop as a core pattern of how change plays out in life, how to master the change process and manifest the change you need.', 2, 1, 1, NULL, NULL),
(15, 4, 18, 'MTA2ODg2Mg==', 'Intro to Hero\'s Journey', 'heros-journey-thumbnail.jpg', 'Hero\'s journey is a workshop that takes children on a journey to understanding fear and building courage. It utilizes basic language and comprehension skills and builds on lessons learned at school with fun exercises.', 3, 0, 1, NULL, NULL),
(18, 4, 18, 'MTA3MDk3Nw==', 'Meet Valeria', 'valeria_profile_square2.jpg', 'Meet Valeria, the co-founder of Ephata and get to know her story that led to building schools and complementary educational curriculums for children.', 3, 0, 1, NULL, NULL),
(16, 4, 18, 'MTA2ODg5OA==', '7 Pillars of Ephata', 'ephata-7-pillarspng.png', 'This short video explains the 7 pillars of Ephata and how they come together to guide children towards a path of faith, wisdom and goodness.', 3, 0, 1, NULL, NULL),
(17, 4, 18, 'MTA2ODkzOA==', 'What is Ephata?', 'tlm121720kg-19.jpg', 'We learn about the story of Ephata and how it can nurture children in many ways beyond home and school.', 3, 0, 1, NULL, NULL),
(21, 3, 1, 'MTA3MTU0OA==', 'The Loop 2: Find your starting point', 'skärmavbild-2021-02-02-kl-164639.png', 'Explore who you are in this moment of your life, and find the starting point of your current transformation loop.\n\nHave pen and paper ready!', 2, 2, 1, NULL, NULL),
(22, 3, 1, 'MTA3MTU2MQ==', 'The Loop 3: Find your shadow', 'skärmavbild-2021-02-02-kl-165002.png', 'Explore your shadow by mapping your psychological pain, anger, anxiety and other challenging emotions in your life.', 2, 3, 1, NULL, NULL),
(23, 3, 1, 'MTA3MTU2NQ==', 'The Loop 4: Embrace your shadow', 'skärmavbild-2021-02-02-kl-165152.png', 'Instructions for shadow breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 2, 4, 1, NULL, NULL),
(24, 3, 1, 'MTA3MTU3Mw==', 'The Loop 5: Find your light', 'skärmavbild-2021-02-02-kl-165320.png', 'Connect with your inner light and map your pride, passion and curiosity.', 2, 5, 1, NULL, NULL),
(25, 3, 1, 'MTA3MTU4Mw==', 'The Loop 6: Embrace your light', 'skärmavbild-2021-02-02-kl-165432.png', 'Visualisation meditation to connect with your path of light', 2, 6, 1, NULL, NULL),
(26, 3, 1, 'MTA3MTU4Nw==', 'The Loop 7: Find your challenge', 'skärmavbild-2021-02-02-kl-165529.png', 'Explore what is holding you back from changing, and map your fears and energy drainers', 2, 7, 1, NULL, NULL),
(27, 3, 1, 'MTA3MTU5Mg==', 'The Loop 8: Embrace your challenge', 'skärmavbild-2021-02-02-kl-165635.png', 'Instructions for Hero\'s breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 2, 8, 1, NULL, NULL),
(28, 3, 1, 'MTA3MTU5NA==', 'The Loop 9: Manifest the change you need', 'skärmavbild-2021-02-02-kl-165748.png', 'Step into manifestation and transform your life. Link to your agreement: https://tiny.cc/myagreement', 2, 9, 1, NULL, NULL),
(29, 3, 1, 'MTA3MTU5NQ==', 'The Loop 10: Integration and follow up', 'skärmavbild-2021-02-02-kl-165846.png', 'Closing integration session, also offered as an optional live experience', 2, 10, 1, NULL, NULL),
(30, 3, 1, 'MTA3MTY1NA==', 'BONUS: 8D sound healing', 'skärmavbild-2021-02-02-kl-170013.png', 'As a little bonus to celebrate your completion of The Loop, we\'re giving you a sound healing session in 8D. Bring your headphones!', 2, 11, 1, NULL, NULL),
(65, 3, 1, 'MTE2MjYzMw==', 'Meditation: Grounding Triad', 'reclam-your-flow-sept-81635554629.png', 'This meditation invites you to ground yourself, calm your central nervous system and silence your mind, by combining boxed breathing, sounding and focusing.', 2, NULL, 0, NULL, NULL),
(52, 3, 1, 'MTE1NTgyNQ==', 'Meditation: Body Scan', 'skärmavbild-2021-09-10-kl-1735431631484867.png', 'A short meditation to relax and connect with your body, while deepening your understanding of where you\'re holding opportunity to release tensions and step into your power. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(57, 3, 1, 'MTE1ODU5Nw==', 'Meditation: Grateful Past & Hopeful Future', 'skärmavbild-2021-08-27-kl-1441251631485267.png', 'A short meditation for exploring what we value in life and where we wish to go next. The intention is to connect with gratitude and give ourselves a sense of direction. Rooted in neuroplasticity and the science of gratitude and intention. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(54, 3, 1, 'MTE1NTgyOQ==', 'Tool: Anchoring Yourself', 'skärmavbild-2021-09-07-kl-2144491631485311.png', 'This session offers simple guidance to find your own anchor for dealing with challenging, stressful or uncomfortable situations in life. This practice is rooted in Somatic Experiencing and NLP coaching.', 2, NULL, 1, NULL, NULL),
(56, 3, 55, 'MTE1NzgyNw==', 'Reg Revans talks about Action Learning', 'reg-revans1635303160.png', 'Action Learning is appropriate when you have to deal with ill-defined complex issues. It\nwould not just be enough to get an expert or a plumber in to do the job. Someone needs to\nmake hard decisions where no one can tell if there is a good solution at all, or there are\nmany, too many options.\nIt is essential that at least one person in the room deeply cares about the issue at hand and\ncan do at least something about it. Action learning deals with real practical issues, not just ‘theory’.', 2, NULL, 1, NULL, NULL),
(55, 3, 1, 'MTE1NjIxMw==', 'Welcome to Reclaim Your Flow', 'skärmavbild-2021-09-07-kl-2135351631485358.png', 'This is a brief welcoming message from your guides Nils and Mattis.', 2, NULL, 1, NULL, NULL),
(58, 3, 55, 'MTE1ODY3OA==', 'Action Learning Training 1', 'hand-holding-notebook-with-drew-brand-logo-creativ-p8htw8w1635302321.jpg', 'Action Learning is appropriate when you have to deal with ill-defined complex issues. It\nwould not just be enough to get an expert or a plumber in to do the job. Someone needs to\nmake hard decisions where no one can tell if there is a good solution at all, or there are\nmany, too many options.\nIt is essential that at least one person in the room deeply cares about the issue at hand and\ncan do at least something about it. Action learning deals with real practical issues, not just ‘theory’.', 2, NULL, 1, NULL, NULL),
(59, 3, 1, 'MTE1OTMzNQ==', 'Tool: Embracing Your Inner Critic', 'skärmavbild-2021-09-07-kl-1311321631485227.png', 'Voicing your inner critic is a tool for acknowledging, embracing and exploring any judgmental inner narratives that might be limiting you. This practice is rooted in NLP coaching. Make sure you have pen and paper ready for taking notes.', 2, NULL, 1, NULL, NULL),
(60, 3, 6, 'MTE1OTM0Mg==', 'Meditation: Inner Child & Elder', 'skärmavbild-2021-09-07-kl-2140431631043937.png', 'A meditation inviting you to connect with your inner child and inner elder. This is a practice of deepening self-understanding and connecting with your underlying needs. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(61, 3, 1, 'MTE1OTQ3NA==', 'Live Session Recording: Sept 8, 2021', 'video-frames-iw0011631485422.jpeg', 'This is a recording of the Sircle Collection live session from Sept 8th 2021.', 2, NULL, 1, NULL, NULL),
(62, 3, 6, 'MTE1OTkwNQ==', 'Meditation: Inner Best Friend', 'skärmavbild-2021-09-10-kl-1735431631288450.png', 'A brief meditation inviting you to explore yourself through the concept of self-friendship.', 2, NULL, 1, NULL, NULL),
(63, 3, 1, 'MTE2MDE2OQ==', 'Tool: Reversing the Movement', 'skärmavbild-2021-09-13-kl-0024531631485714.png', 'This is a simple tool for grounding yourself in a challenging or stressful situation. This practice is commonly used by athletes and performers, and is grounded in NLP coaching.', 2, NULL, 1, NULL, NULL),
(64, 3, 6, 'MTE2MDE3MA==', 'Meditation: Grounding in Being', 'skärmavbild-2021-09-13-kl-0036091631486344.png', 'This guided meditation invites you to ground yourself simply by being. Feel free to take a few moments after the meditation to write down any thoughts, sensations or insights.', 2, NULL, 0, NULL, NULL),
(66, 3, 1, 'MTE2MjY2MA==', 'Meditation: Shedding Your Layers', 'skärmavbild-2021-09-16-kl-1533391631799944.png', 'A meditation inviting you to explore who you are without your layers of identity. Feel free to take a moment after the meditation to write down any reflections or insights.\n', 2, NULL, 1, NULL, NULL),
(67, 3, 1, 'MTE2MjY2NQ==', 'Tool: Self Inquiry', 'skärmavbild-2021-09-16-kl-1549131631800470.png', 'This is a short exercise in using self inquiry to explore any thought patterns and beliefs that might be limiting you. This practice is rooted in The Work by Byron Katie. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(68, 3, 1, 'MTE2MjcwMA==', 'Meditation: Purpose Walk', 'skärmavbild-2021-09-16-kl-1558331631800921.png', 'This meditation invites you to explore, and connect with, your personal path and purpose in this life. This is a practice rooted in NLP coaching. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(69, 3, 6, 'MTE2Mjg2MQ==', 'Meditation: Tiny Sensations', 'skärmavbild-2021-09-16-kl-1601341631802184.png', 'This grounding meditation invites you to notice the tiny sensations within you and around you. Feel free to take a few moments after the meditation to write down any reflections or insights.', 2, NULL, 1, NULL, NULL),
(70, 3, 44, 'MTE3NzgwOQ==', 'Onboarding Guides', 'diverse-teens-hands-together-concept-pbw4jmj1633900011.jpg', 'This is a video for onboarding guides', 2, NULL, 1, NULL, NULL),
(71, 3, 35, 'MTE4MzY2OQ==', '7 Pillars of Ephata', 'ephata-7-pillars1635559666.png', 'EPHATA is a movement, philosophy, life flow, to be emotionally, spiritually and socially equipped to process challenges in adequate, pertinent, cognitive, congruent, aware ways to successfully navigate the present while investing for tomorrow', 2, NULL, 1, NULL, NULL),
(72, 3, 35, 'MTE4MzY3MA==', 'What is Ephata?', 'ephata-circle-pic-21635560798.jpg', 'EPHATA is a movement, philosophy, life flow, to be emotionally, spiritually and socially equipped to process challenges in adequate, pertinent, cognitive, congruent, aware ways to successfully navigate the present while investing for tomorrow', 2, NULL, 1, NULL, NULL),
(73, 3, 35, 'MTE4MzY3MQ==', 'Introduction to Ephata Workshops', 'self-efficacy1635561111.jpg', 'Ephata workshops are for 6 to 16 year olds and are run online on a regular basis. To enroll for a workshop or find out more, email Valeria at valeriapayton@gmail.com', 2, NULL, 1, NULL, NULL),
(74, 3, 55, 'MTE4MzY3Mg==', 'Introduction to Fraendi', 'fraendi-intro1635561679.png', 'Frændi is a group of consulting professionals, thinkers, educators, doers, and optimists who are committed to the practice of unlocking potential—in people, teams, systems, and organizations.\n\nOur goal is to instill confidence in individuals, leaders, governments, and organizations and together help create a roadmap to a prosperous, planet-sustaining future.\n\nIn a world that faces unprecedented challenges from social justice to environmental degradation, Frændi is working with leaders who are moving their organizations forward with clarity, confidence, empathy, agility, and resilience.', 2, NULL, 1, NULL, NULL),
(75, 3, 1, 'MTE5MDQzOA==', 'Test video', '1637865575.jpg', 'Test', 3, NULL, 1, NULL, NULL),
(76, 3, 1, 'MTE5MDY4Nw==', 'Test video for Sircle', '1638225638.jpg', 'Test description', 2, NULL, 1, NULL, NULL),
(77, 3, 1, 'MTE5MjAyMg==', 'Test video for Reclaim your flow', '1638362998.jpg', 'This is a test', 2, NULL, 1, NULL, NULL),
(78, 3, 6, 'MTE5MzAwMw==', 'Test video with Mattis', '1638522129.jpg', 'Test', 2, NULL, 0, NULL, NULL),
(79, 3, 1, 'MTE5NTQ2Nw==', 'Test video 12/12', '1639342226.jpg', 'Test video', 2, NULL, 1, NULL, NULL),
(80, 3, 1, 'MTE5NTQ2Nw==', 'Test video 12/12', '1639342226.jpg', 'Test video', 2, NULL, 1, NULL, NULL),
(89, 3, 6, NULL, 'Come Closer', '1640000999.jpg', 'Come Closer game', 2, NULL, 1, 'Mattis Norrvidd', 'https://www.youtube.com/watch?v=G391Yt_TYhg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  ADD PRIMARY KEY (`accessUserId`);

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answerId`);

--
-- Indices de la tabla `appearance`
--
ALTER TABLE `appearance`
  ADD PRIMARY KEY (`appearanceId`);

--
-- Indices de la tabla `articlecircle`
--
ALTER TABLE `articlecircle`
  ADD PRIMARY KEY (`articleCircleId`);

--
-- Indices de la tabla `articlerequest`
--
ALTER TABLE `articlerequest`
  ADD PRIMARY KEY (`requestArticleId`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`);

--
-- Indices de la tabla `articleseriecircle`
--
ALTER TABLE `articleseriecircle`
  ADD PRIMARY KEY (`serieArticlesCircleId`);

--
-- Indices de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `audiocategories`
--
ALTER TABLE `audiocategories`
  ADD PRIMARY KEY (`acId`);

--
-- Indices de la tabla `audiocircle`
--
ALTER TABLE `audiocircle`
  ADD PRIMARY KEY (`audioCircleId`);

--
-- Indices de la tabla `audiorequest`
--
ALTER TABLE `audiorequest`
  ADD PRIMARY KEY (`requestAudioId`);

--
-- Indices de la tabla `audios`
--
ALTER TABLE `audios`
  ADD PRIMARY KEY (`audioId`);

--
-- Indices de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `callcategories`
--
ALTER TABLE `callcategories`
  ADD PRIMARY KEY (`ccId`);

--
-- Indices de la tabla `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`callId`);

--
-- Indices de la tabla `callscircle`
--
ALTER TABLE `callscircle`
  ADD PRIMARY KEY (`callCircleId`);

--
-- Indices de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `circlecategories`
--
ALTER TABLE `circlecategories`
  ADD PRIMARY KEY (`ccId`);

--
-- Indices de la tabla `circlecoachrequest`
--
ALTER TABLE `circlecoachrequest`
  ADD PRIMARY KEY (`requestCircleId`);

--
-- Indices de la tabla `circlerequest`
--
ALTER TABLE `circlerequest`
  ADD PRIMARY KEY (`requestCircleId`);

--
-- Indices de la tabla `circles`
--
ALTER TABLE `circles`
  ADD PRIMARY KEY (`circleId`);

--
-- Indices de la tabla `circletodos`
--
ALTER TABLE `circletodos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  ADD PRIMARY KEY (`clientPageId`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientId`);

--
-- Indices de la tabla `coacharticle`
--
ALTER TABLE `coacharticle`
  ADD PRIMARY KEY (`coachArticleId`);

--
-- Indices de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `coachaudio`
--
ALTER TABLE `coachaudio`
  ADD PRIMARY KEY (`coachAudioId`);

--
-- Indices de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  ADD PRIMARY KEY (`coachCallId`);

--
-- Indices de la tabla `coachcircle`
--
ALTER TABLE `coachcircle`
  ADD PRIMARY KEY (`coachCircleId`);

--
-- Indices de la tabla `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`coachId`);

--
-- Indices de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  ADD PRIMARY KEY (`coachClientId`);

--
-- Indices de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `coachvideo`
--
ALTER TABLE `coachvideo`
  ADD PRIMARY KEY (`coachVideoId`);

--
-- Indices de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`colorId`);

--
-- Indices de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  ADD PRIMARY KEY (`colorClientId`);

--
-- Indices de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  ADD PRIMARY KEY (`iddiccionario`);

--
-- Indices de la tabla `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`emailId`);

--
-- Indices de la tabla `eventrecording`
--
ALTER TABLE `eventrecording`
  ADD PRIMARY KEY (`eventRecordingId`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`eventId`);

--
-- Indices de la tabla `eventscircle`
--
ALTER TABLE `eventscircle`
  ADD PRIMARY KEY (`eventCircleId`);

--
-- Indices de la tabla `eventsunregister`
--
ALTER TABLE `eventsunregister`
  ADD PRIMARY KEY (`eventUnregisterId`);

--
-- Indices de la tabla `excluded`
--
ALTER TABLE `excluded`
  ADD PRIMARY KEY (`excludedId`);

--
-- Indices de la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`imageId`);

--
-- Indices de la tabla `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`generalId`);

--
-- Indices de la tabla `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`homeId`);

--
-- Indices de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  ADD PRIMARY KEY (`homePlatformId`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`levelId`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`);

--
-- Indices de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  ADD PRIMARY KEY (`passId`);

--
-- Indices de la tabla `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`platformId`);

--
-- Indices de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  ADD PRIMARY KEY (`psId`);

--
-- Indices de la tabla `plugincontent`
--
ALTER TABLE `plugincontent`
  ADD PRIMARY KEY (`pluginContentId`);

--
-- Indices de la tabla `plugincontentcircle`
--
ALTER TABLE `plugincontentcircle`
  ADD PRIMARY KEY (`pluginContentCircleId`);

--
-- Indices de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`postId`);

--
-- Indices de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`questionId`);

--
-- Indices de la tabla `seriecircle`
--
ALTER TABLE `seriecircle`
  ADD PRIMARY KEY (`serieCircleId`);

--
-- Indices de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `sessionrecording`
--
ALTER TABLE `sessionrecording`
  ADD PRIMARY KEY (`sessionRecordingId`);

--
-- Indices de la tabla `shala`
--
ALTER TABLE `shala`
  ADD PRIMARY KEY (`shalaId`);

--
-- Indices de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  ADD PRIMARY KEY (`slideId`);

--
-- Indices de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  ADD PRIMARY KEY (`statusId`);

--
-- Indices de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`todoId`);

--
-- Indices de la tabla `typemail`
--
ALTER TABLE `typemail`
  ADD PRIMARY KEY (`typeId`);

--
-- Indices de la tabla `userarticle`
--
ALTER TABLE `userarticle`
  ADD PRIMARY KEY (`userArticleId`);

--
-- Indices de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `useraudio`
--
ALTER TABLE `useraudio`
  ADD PRIMARY KEY (`userAudioId`);

--
-- Indices de la tabla `usercall`
--
ALTER TABLE `usercall`
  ADD PRIMARY KEY (`userCallId`);

--
-- Indices de la tabla `usercircle`
--
ALTER TABLE `usercircle`
  ADD PRIMARY KEY (`userCircleId`);

--
-- Indices de la tabla `userclient`
--
ALTER TABLE `userclient`
  ADD PRIMARY KEY (`userClientId`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indices de la tabla `userserie`
--
ALTER TABLE `userserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `uservideo`
--
ALTER TABLE `uservideo`
  ADD PRIMARY KEY (`userVideoId`);

--
-- Indices de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  ADD PRIMARY KEY (`vcId`);

--
-- Indices de la tabla `videocircle`
--
ALTER TABLE `videocircle`
  ADD PRIMARY KEY (`videoCircleId`);

--
-- Indices de la tabla `videorequest`
--
ALTER TABLE `videorequest`
  ADD PRIMARY KEY (`requestVideoId`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`videoId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access`
--
ALTER TABLE `access`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  MODIFY `accessUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `answerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT de la tabla `appearance`
--
ALTER TABLE `appearance`
  MODIFY `appearanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articlecircle`
--
ALTER TABLE `articlecircle`
  MODIFY `articleCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `articlerequest`
--
ALTER TABLE `articlerequest`
  MODIFY `requestArticleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticle;multiple=coaches,coacharticle;multiple=circles,articlecircle;', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `articleseriecircle`
--
ALTER TABLE `articleseriecircle`
  MODIFY `serieArticlesCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;multiple=circles,articleseriecircle;', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `audiocategories`
--
ALTER TABLE `audiocategories`
  MODIFY `acId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `audiocircle`
--
ALTER TABLE `audiocircle`
  MODIFY `audioCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `audiorequest`
--
ALTER TABLE `audiorequest`
  MODIFY `requestAudioId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `audios`
--
ALTER TABLE `audios`
  MODIFY `audioId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,useraudio;multiple=coaches,coachaudio;multiple=categories,audiocategories;multiple=circles,audiocircle;', AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `callcategories`
--
ALTER TABLE `callcategories`
  MODIFY `ccId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT de la tabla `calls`
--
ALTER TABLE `calls`
  MODIFY `callId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;multiple=circles,callscircle;multiple=categories,callcategories;', AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT de la tabla `callscircle`
--
ALTER TABLE `callscircle`
  MODIFY `callCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `circlecategories`
--
ALTER TABLE `circlecategories`
  MODIFY `ccId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT de la tabla `circlecoachrequest`
--
ALTER TABLE `circlecoachrequest`
  MODIFY `requestCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `circlerequest`
--
ALTER TABLE `circlerequest`
  MODIFY `requestCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `circles`
--
ALTER TABLE `circles`
  MODIFY `circleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercircle;multiple=coaches,coachcircle;multiple=categories,circlecategories;', AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `circletodos`
--
ALTER TABLE `circletodos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  MODIFY `clientPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `coacharticle`
--
ALTER TABLE `coacharticle`
  MODIFY `coachArticleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `coachaudio`
--
ALTER TABLE `coachaudio`
  MODIFY `coachAudioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  MODIFY `coachCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT de la tabla `coachcircle`
--
ALTER TABLE `coachcircle`
  MODIFY `coachCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `coaches`
--
ALTER TABLE `coaches`
  MODIFY `coachId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  MODIFY `coachClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla `coachvideo`
--
ALTER TABLE `coachvideo`
  MODIFY `coachVideoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=382;

--
-- AUTO_INCREMENT de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `colorId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  MODIFY `colorClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  MODIFY `iddiccionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `email`
--
ALTER TABLE `email`
  MODIFY `emailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `eventrecording`
--
ALTER TABLE `eventrecording`
  MODIFY `eventRecordingId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `eventId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=circles,eventscircle;', AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `eventscircle`
--
ALTER TABLE `eventscircle`
  MODIFY `eventCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT de la tabla `eventsunregister`
--
ALTER TABLE `eventsunregister`
  MODIFY `eventUnregisterId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `excluded`
--
ALTER TABLE `excluded`
  MODIFY `excludedId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `gallery`
--
ALTER TABLE `gallery`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `generals`
--
ALTER TABLE `generals`
  MODIFY `generalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `home`
--
ALTER TABLE `home`
  MODIFY `homeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  MODIFY `homePlatformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `levelId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  MODIFY `passId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT de la tabla `platforms`
--
ALTER TABLE `platforms`
  MODIFY `platformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  MODIFY `psId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT de la tabla `plugincontent`
--
ALTER TABLE `plugincontent`
  MODIFY `pluginContentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `plugincontentcircle`
--
ALTER TABLE `plugincontentcircle`
  MODIFY `pluginContentCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `postId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `questionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `seriecircle`
--
ALTER TABLE `seriecircle`
  MODIFY `serieCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `series`
--
ALTER TABLE `series`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;multiple=circles,seriecircle;', AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sessionrecording`
--
ALTER TABLE `sessionrecording`
  MODIFY `sessionRecordingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `shala`
--
ALTER TABLE `shala`
  MODIFY `shalaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  MODIFY `slideId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT COMMENT ' multiple=accessadmin,accessuseradmin', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `todos`
--
ALTER TABLE `todos`
  MODIFY `todoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT de la tabla `typemail`
--
ALTER TABLE `typemail`
  MODIFY `typeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `userarticle`
--
ALTER TABLE `userarticle`
  MODIFY `userArticleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `useraudio`
--
ALTER TABLE `useraudio`
  MODIFY `userAudioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `usercall`
--
ALTER TABLE `usercall`
  MODIFY `userCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT de la tabla `usercircle`
--
ALTER TABLE `usercircle`
  MODIFY `userCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=487;

--
-- AUTO_INCREMENT de la tabla `userclient`
--
ALTER TABLE `userclient`
  MODIFY `userClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `userserie`
--
ALTER TABLE `userserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `uservideo`
--
ALTER TABLE `uservideo`
  MODIFY `userVideoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=492;

--
-- AUTO_INCREMENT de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  MODIFY `vcId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT de la tabla `videocircle`
--
ALTER TABLE `videocircle`
  MODIFY `videoCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `videorequest`
--
ALTER TABLE `videorequest`
  MODIFY `requestVideoId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `videoId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,uservideo;multiple=coaches,coachvideo;multiple=categories,videocategories;multiple=circles,videocircle;', AUTO_INCREMENT=90;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
