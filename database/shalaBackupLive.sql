-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-11-2021 a las 09:38:43
-- Versión del servidor: 5.6.51-cll-lve
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shala_system`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access`
--

CREATE TABLE `access` (
  `accessId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `access`
--

INSERT INTO `access` (`accessId`, `name`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered users'),
(4, 'Circles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessadmin`
--

CREATE TABLE `accessadmin` (
  `accessId` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessadmin`
--

INSERT INTO `accessadmin` (`accessId`, `nombre`) VALUES
(7, 'generalSetup'),
(8, 'platformsManage'),
(9, 'webContent'),
(10, 'userManage'),
(11, 'platformUsers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accessuseradmin`
--

CREATE TABLE `accessuseradmin` (
  `accessUserId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `accessId` int(11) NOT NULL COMMENT 'opciones=accessadmin'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accessuseradmin`
--

INSERT INTO `accessuseradmin` (`accessUserId`, `tipoId`, `accessId`) VALUES
(11, 2, 7),
(12, 2, 8),
(13, 2, 9),
(14, 2, 11),
(15, 2, 10),
(16, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL,
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipoadmin',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `usuario` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL COMMENT 'pass'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`adminId`, `tipoId`, `nombre`, `usuario`, `pass`) VALUES
(1, 2, 'Administrator', 'admin', 'MGRzOGtMVXcxRDJHR3piQ0NyMVdJVEtoWGE5RWVrL3p6blFMdEtyQTErST0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `answerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `questionId` int(11) NOT NULL COMMENT 'opciones=questionnaire',
  `answer` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`answerId`, `clientId`, `userId`, `questionId`, `answer`) VALUES
(36, 3, 3, 5, 'Three'),
(35, 3, 3, 4, 'Two'),
(34, 3, 3, 3, 'One'),
(33, 3, 3, 2, 'Two'),
(12, 3, 4, 1, 'One'),
(13, 3, 4, 2, 'Two'),
(14, 3, 4, 3, 'Three'),
(15, 3, 4, 4, 'Four'),
(16, 3, 4, 5, 'Five'),
(32, 3, 3, 1, 'Four'),
(22, 3, 5, 1, 'One'),
(23, 3, 5, 2, 'Two'),
(24, 3, 5, 3, 'Three'),
(25, 3, 5, 4, 'Four'),
(26, 3, 5, 5, 'Five'),
(27, 3, 6, 1, 'One'),
(28, 3, 6, 2, 'Two'),
(29, 3, 6, 3, 'Three'),
(30, 3, 6, 4, 'Four'),
(31, 3, 6, 5, 'Five'),
(37, 3, 7, 1, 'One'),
(38, 3, 7, 2, 'Two'),
(39, 3, 7, 3, 'Two'),
(40, 3, 7, 4, 'Four'),
(41, 3, 7, 5, 'Two'),
(42, 3, 8, 1, 'One'),
(43, 3, 8, 2, 'Two'),
(44, 3, 8, 3, 'Three'),
(45, 3, 8, 4, 'Four'),
(46, 3, 8, 5, 'Two'),
(47, 3, 9, 1, 'One'),
(48, 3, 9, 2, 'Three'),
(49, 3, 9, 3, 'Two'),
(50, 3, 9, 4, 'Two'),
(51, 3, 9, 5, 'One'),
(52, 3, 10, 1, 'One'),
(53, 3, 10, 2, 'Two'),
(54, 3, 10, 3, 'Three'),
(55, 3, 10, 4, 'Four'),
(56, 3, 10, 5, 'Five'),
(57, 3, 11, 1, 'Two'),
(58, 3, 11, 2, 'Four'),
(59, 3, 11, 3, 'Three'),
(60, 3, 11, 4, 'Two'),
(61, 3, 11, 5, 'Three'),
(62, 3, 12, 1, 'Four'),
(63, 3, 12, 2, 'Two'),
(64, 3, 12, 3, 'Three'),
(65, 3, 12, 4, 'Three'),
(66, 3, 12, 5, 'Three'),
(67, 3, 13, 1, 'One'),
(68, 3, 13, 2, 'Two'),
(69, 3, 13, 3, 'Three'),
(70, 3, 13, 4, 'Four'),
(71, 3, 13, 5, 'Five'),
(72, 3, 15, 1, 'Two'),
(73, 3, 15, 2, 'One'),
(74, 3, 15, 3, 'Three'),
(75, 3, 15, 4, 'Two'),
(76, 3, 15, 5, 'Four'),
(77, 3, 16, 1, 'Three'),
(78, 3, 16, 2, 'Two'),
(79, 3, 16, 3, 'Two'),
(80, 3, 16, 4, 'Two'),
(81, 3, 16, 5, 'Three'),
(82, 4, 17, 6, 'One'),
(83, 4, 18, 6, 'Two'),
(84, 3, 19, 1, 'Four'),
(85, 3, 19, 2, 'Four'),
(86, 3, 19, 3, 'Four'),
(87, 3, 19, 4, 'Five'),
(88, 3, 19, 5, 'One'),
(89, 7, 22, 7, 'Lodging'),
(90, 7, 22, 8, 'Referral'),
(91, 7, 23, 7, 'Retreat'),
(92, 7, 23, 8, 'Referral'),
(93, 8, 24, 9, 'Two'),
(94, 8, 24, 10, 'Four'),
(95, 8, 24, 11, 'One'),
(96, 8, 24, 12, 'Four'),
(97, 8, 24, 13, 'Five'),
(98, 3, 26, 1, 'Three'),
(99, 3, 26, 2, 'One'),
(100, 3, 26, 3, 'Two'),
(101, 3, 26, 4, 'Four'),
(102, 3, 26, 5, 'Five'),
(103, 8, 27, 9, 'Two'),
(104, 8, 27, 10, 'Three'),
(105, 8, 27, 11, 'Three'),
(106, 8, 27, 12, 'Four'),
(107, 8, 27, 13, 'Five'),
(108, 8, 28, 9, 'One'),
(109, 8, 28, 10, 'Three'),
(110, 8, 28, 11, 'Two'),
(111, 8, 28, 12, 'Four'),
(112, 8, 28, 13, 'Five'),
(116, 3, 30, 1, 'Two'),
(117, 3, 30, 2, 'One'),
(118, 3, 30, 3, 'Three'),
(119, 3, 30, 4, 'Five'),
(120, 3, 30, 5, 'Four'),
(121, 8, 31, 9, 'Two'),
(122, 8, 31, 10, 'Three'),
(123, 8, 31, 11, 'Three'),
(124, 8, 31, 12, 'Five'),
(125, 8, 31, 13, 'Two'),
(126, 8, 32, 9, 'Three'),
(127, 8, 32, 10, 'Five'),
(128, 8, 32, 11, 'One'),
(129, 8, 32, 12, 'Two'),
(130, 8, 32, 13, 'Three'),
(131, 8, 33, 9, 'Two'),
(132, 8, 33, 10, 'Two'),
(133, 8, 33, 11, 'Two'),
(134, 8, 33, 12, 'Three'),
(135, 8, 33, 13, 'Three'),
(136, 3, 34, 1, 'One'),
(137, 3, 34, 2, 'One'),
(138, 3, 34, 3, 'Two'),
(139, 3, 34, 4, 'Two'),
(140, 3, 34, 5, 'Two'),
(141, 3, 35, 1, 'Two'),
(142, 3, 35, 2, 'One'),
(143, 3, 35, 3, 'Two'),
(144, 3, 35, 4, 'Two'),
(145, 3, 36, 1, 'Online search'),
(146, 3, 37, 1, 'Invited by a Guide/Coach'),
(147, 3, 38, 1, 'Invited by a Guide/Coach'),
(148, 3, 39, 1, 'Invited by a Guide/Coach'),
(149, 3, 40, 1, 'Invited by a Guide/Coach'),
(150, 3, 41, 1, 'Invited by a Guide/Coach'),
(151, 3, 42, 1, 'Invited by a Guide/Coach'),
(152, 3, 43, 1, 'Invited by a Guide/Coach'),
(153, 3, 45, 1, 'Invited by a Guide/Coach'),
(154, 3, 46, 1, 'Invited by a Guide/Coach'),
(155, 3, 47, 1, 'Invited by a Guide/Coach'),
(156, 3, 48, 1, 'Invited by a Guide/Coach'),
(157, 3, 49, 1, 'Invited by a Guide/Coach'),
(158, 3, 50, 1, 'Invited by a Guide/Coach'),
(159, 3, 51, 1, 'Invited by a Guide/Coach'),
(160, 3, 52, 1, 'Invited by a Guide/Coach'),
(161, 3, 53, 1, 'Invited by a Guide/Coach'),
(162, 3, 54, 1, 'Invited by a Guide/Coach'),
(163, 3, 55, 1, 'Invited by a Guide/Coach'),
(164, 3, 56, 1, 'Invited by a Guide/Coach'),
(165, 3, 57, 1, 'Invited by a Guide/Coach'),
(166, 3, 58, 1, 'Invited by a Guide/Coach'),
(167, 3, 60, 1, 'Referred by a friend'),
(168, 3, 62, 1, 'Referred by a friend'),
(169, 3, 63, 1, 'Referred by a friend'),
(170, 3, 64, 1, 'Invited by a Guide/Coach'),
(171, 3, 67, 1, 'Online search');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appearance`
--

CREATE TABLE `appearance` (
  `appearanceId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `h1` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h1Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h2Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h3Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h4Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `h5Mobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `p` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `pMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `boton` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `botonMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `buttonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `buttonColor` varchar(10) DEFAULT NULL COMMENT 'color',
  `buttonColorText` varchar(10) DEFAULT NULL COMMENT 'color',
  `menuLink` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `menuLinkMobile` varchar(10) DEFAULT NULL COMMENT 'fontSize'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `appearance`
--

INSERT INTO `appearance` (`appearanceId`, `clientId`, `h1`, `h1Mobile`, `h2`, `h2Mobile`, `h3`, `h3Mobile`, `h4`, `h4Mobile`, `h5`, `h5Mobile`, `p`, `pMobile`, `boton`, `botonMobile`, `buttonRounded`, `buttonColor`, `buttonColorText`, `menuLink`, `menuLinkMobile`) VALUES
(3, 10, '28px', NULL, '24px', NULL, '22px', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24px', NULL, 'N', NULL, NULL, NULL, NULL),
(4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16px', NULL, 'S', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articlecircle`
--

CREATE TABLE `articlecircle` (
  `articleCircleId` int(11) NOT NULL,
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articlerequest`
--

CREATE TABLE `articlerequest` (
  `requestArticleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `articleId` int(11) NOT NULL COMMENT 'multiple=users,userarticle;multiple=coaches,coacharticle;multiple=circles,articlecircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) DEFAULT NULL COMMENT 'opciones=series',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `pdf` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`articleId`, `clientId`, `coachId`, `serieId`, `titulo`, `imagen`, `description`, `pdf`, `accessId`, `orden`) VALUES
(2, 3, 55, 3, 'Action Learning Guide', 'fraendi_logo_ohne_rand-removebg1630507726.png', '<p><strong>Action Learning - what is it good for?<br /></strong><br />Action Learning is appropriate when you have to deal with ill-defined complex issues. It<br />would not just be enough to get an expert or a plumber in to do the job. Someone needs to<br />make hard decisions where no one can tell if there is a good solution at all, or there are<br />many, too many options.<br />It is essential that at least one person in the room deeply cares about the issue at hand and<br />can do at least something about it. Action learning deals with real practical issues, not just<br />&lsquo;theory&rsquo;.<br />How does it work?<br />A group of 5-7 people meets for an hour plus 10 minutes or a little longer. They sit in a<br />circle close to each other.<br />In the beginning, a few roles are defined:<br />The issue owner: takes care of the issue, takes decisions on which questions to focus on,<br />formulates three concrete action steps at the end of the session.<br />The coach: accompanies th', NULL, 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseriecircle`
--

CREATE TABLE `articleseriecircle` (
  `serieArticlesCircleId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleserierequest`
--

CREATE TABLE `articleserierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseries`
--

CREATE TABLE `articleseries` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;multiple=circles,articleseriecircle;',
  `clientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseries`
--

INSERT INTO `articleseries` (`serieId`, `clientId`, `coachId`, `accessId`, `nombre`, `description`, `photo`) VALUES
(1, 3, 1, 2, 'Design Your Future Now', 'A journey into consciously designing your future and manifesting it into your lived reality', 'aditya-saxena-_mixhvl_wza-unsplash-1.jpg'),
(2, 3, 9, 3, 'Sound Healing', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', NULL),
(3, 3, 55, 2, 'Action Learning', 'Fraendi pilot program', 'fraendi_logo_ohne_rand-removebg1630508247.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articleseriescategories`
--

CREATE TABLE `articleseriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articleseriescategories`
--

INSERT INTO `articleseriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(45, 1, 4),
(44, 1, 2),
(41, 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiocategories`
--

CREATE TABLE `audiocategories` (
  `acId` int(11) NOT NULL,
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiocircle`
--

CREATE TABLE `audiocircle` (
  `audioCircleId` int(11) NOT NULL,
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audiorequest`
--

CREATE TABLE `audiorequest` (
  `requestAudioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audios`
--

CREATE TABLE `audios` (
  `audioId` int(11) NOT NULL COMMENT 'multiple=users,useraudio;multiple=coaches,coachaudio;multiple=categories,audiocategories;multiple=circles,audiocircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `audio` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannerhome`
--

CREATE TABLE `bannerhome` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `buttonText` varchar(200) NOT NULL,
  `buttonUrl` varchar(200) NOT NULL COMMENT 'liga'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannerhome`
--

INSERT INTO `bannerhome` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`, `buttonText`, `buttonUrl`) VALUES
(8, 7, 'RENT SPACE', 'Rent our facilities for retreats, workshops and special events. Nierika is situated in a peaceful and quiet river valley where the song of birds and the whisper of the wind inspire tranquility and introspection. Our ecological facilities and permaculture gardens help you feel immersed in nature and community.', 'pagoda-bed-room-pic.jpg', NULL, 'Learn more', '/about'),
(3, 3, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(4, 3, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore'),
(5, 4, 'FOR EVERY SPACE, AT ANY PACE\r\n', 'From the comfort of your living room to outside spaces, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you.\r\n', 'for-every-ephata.jpg', NULL, 'Explore classes', '/explore'),
(6, 4, 'HIGHLY ENGAGING CONTENT\r\n', 'We understand the needs of children and designed our programs accordingly in byte size videos, personalized live sessions, engaging group workshops and fun exercises.\r\n', 'highly-engaging-ephata.jpg', NULL, 'Explore classes', '/explore'),
(12, 7, 'Retreats', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'img_6822-will-need-cropping2.jpg', NULL, 'Learn more', '/explore'),
(13, 8, 'HIGHLY ENGAGING CONTENT', 'We understand the needs of a corporate setting and designed our content accordingly in byte size videos, personalized live sessions, engaging group workshops and thought provoking exercises.\r\n', 'highly-engaging-content.jpg', NULL, 'Explore classes', '/explore'),
(14, 8, 'FOR EVERY SPACE, AT ANY PACE', 'From the comfort of your living room to hotel rooms across the globe, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you. Or let our guides come to your office to hold customized on-location classes.\r\n', 'for-ever-space.jpg', NULL, 'Explore classes', '/explore');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannersomos`
--

CREATE TABLE `bannersomos` (
  `bannerId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(10000) NOT NULL COMMENT 'html',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannersomos`
--

INSERT INTO `bannersomos` (`bannerId`, `clientId`, `title`, `subtitle`, `image`, `video`) VALUES
(7, 7, ' ', 'At Nierika, we provide a natural path to recovery and healing that helps you grow from your current challenge into the new phase in life.\n\nWe create an environment for deep self-exploration that allows new insights to emerge from within.\n\nOur client-centered approach has demonstrated effectiveness in conditions such as chronic depression, substance abuse, eating disorders, anxiety, loss and grief issues, PTSD, spiritual emergency and other mental, emotional and behavioral challenges.', 'pond.jpg', NULL),
(8, 7, 'Our History', 'Over the last decade, Centro Nierika has hosted an impressive range of events.  We provide a safe, beautiful and ecological space for holistic retreats, workshops, therapies, ceremonies, family gatherings, staff retreats and other special events.\n\nIndigenous healers from North & South America, internationally recognized psychologists, educators and persons seeking alternative paths to healing find Nierika the perfect setting for deep and transformative work.', 'centre-int-view-through-mirror-1-copy.jpg', NULL),
(9, 7, 'The Region', 'Our sanctuary is nested in a lush subtropical valley, at about 5,577′ (1,700 m) elevation in the Sierra Mountains.  The climate is moderate with mild humidity, pleasantly warm day temperatures that cool down at night. Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally.', 'img_7398.jpg', NULL),
(10, 7, 'Getting here', 'Nierika Center is only 2 hours away (depending on traffic) from Mexico city, 1 hour away from Toluca and 1 hour away from Cuernavaca.  We will be happy to arrange for your transportation from anywhere in Mexico or internationally. You can also take Uber, Didi or local taxi from the airport or the city to arrive here. Fares range from 700 to1500 pesos for a one way trip depending on demand. We recommend avoiding night time travel due to poor visibility on the mountainous roads.', 'mappng.png', NULL),
(11, 7, 'The Facilities', 'The center provides  individual and/or shared comfortable accommodations, architectural spaces, hot solar showers, wireless internet, healthy and delicious meals prepared with organic produce, use of an art studio and yoga/meditation facilities as well as many acres of organic orchards and gardens. You will experience the serenity of the surrounding nature reserve setting, high standards of cleanliness, comfort and a unique style.', 'dinning-room-main-pic-6.jpg', NULL),
(12, 7, 'ACCOMMODATION', 'We can lodge up to 40 people in various types of spaces. Our dormitories are a thermal adobe building with 2 sections (Men & Women) with a capacity of 8 beds each. We also have individual or shared rooms (2 beds), either with personal or shared bathroom. Our suites are queen size, private bathroom, some with and extra individual bed. We also have two bedroom (one queen and two singles) apartment with bathroom, with small living room and kitchen.', 'pool-suite-pic-4-new.jpg', NULL),
(13, 7, 'Food and Water', 'We provide a mostly organic menu with lots of fresh farm-to-table ingredients. Our cooks are masterful in preparing family-style Mexican food, that is healthy, flavorful and vegetarian-friendly. Meals are included in the cost of accommodation.\n\nNierika’s permaculture gardens are thriving with avocado, citrus, and coffee orchards, fresh greens and other vegetables as well as medicinal herbs. All our water comes straight from Mountain springs and goes through a chemical-free filtration process.', 'alfresco-shot-1.jpg', NULL),
(14, 7, 'Workspaces', 'Center Space with terrace – An architecturally designed roofted terrace with sliding doors connecting to the altar and music room.\n\nThe Rooftop – Terrace with amazing valley view, suitable for group yoga and meditation.\n\nThe Cave of Visions – Inspire that a rock we could not move, we created a man-made cave with rock paintings that transport you to a mystical and magical space. It also has a fire place.', 'home_n_0007_zen_6897-edit-edit-scaled.jpg', NULL),
(15, 7, 'Teocali', 'A sacred geometry designed circular prayer & ceremonial room, with the possibility of having a fire inside, where countless ceremonies of different traditions have been held.', 'tucali-wide-shot.jpg', NULL),
(16, 7, 'Our Gardens', 'Immerse yourself in the beautiful natural landscapes filled with exotic flowers and ancient trees, suited for a process of profound inner growth and contemplation. Enjoy practicing organic gardening, seeding edible plants and farming in our lovely gardens and orchards, allowing life to grow in the natural path. Our gardens also provide a wonderful opportunity for giving back to Mother Earth by learning how to grow your own food while you help us harvest for our kitchen or sow new seeds.', 'gardens.jpg', NULL),
(17, 7, 'Temictla ', 'The Nierika project extends over 30 acres and is comprised by two sections divided by a river. Temictla, in nahuatl, means the place which is beyond dreams. It holds a reconstructed pyramid and an amphitheater field extends into the mountains where diverse public events have been held, such as Council of Visions, musical concerts and sundance, children camp, ideal for bigger groups and festivals. Temictla is excellent for morning walks up the mountain, and for outdoor group activities.', 'img_7398.jpg', NULL),
(18, 7, 'Nearby Nierika', 'Discover Historical Towns, Sacred Sites & Mythical Surroundings. Nestled in the Chichinautzin natural reserve 2 hours south of Mexico city, we are located in a lush subtropical watershed rich valley with springs and waterfalls. Ancient myths and archetypes live on in nearby historic world-heritage sites, temples and spiritual pilgrimage routes.', 'surroundings.jpg', NULL),
(20, 8, 'Section 1', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', '42-15216471.jpg', NULL),
(21, 8, 'Section 2', 'Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla&nbsp;', 'azulik_web_cover.jpg', 'MTEwNzI1NA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `callcategories`
--

CREATE TABLE `callcategories` (
  `ccId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls`
--

CREATE TABLE `calls` (
  `callId` int(11) NOT NULL COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;multiple=circles,callscircle;multiple=categories,callcategories;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `allMembers` char(1) DEFAULT NULL COMMENT 'activar',
  `allCoaches` char(1) DEFAULT NULL COMMENT 'activar',
  `image` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `zoomId` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(2000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL COMMENT 'liga',
  `tipoId` int(11) NOT NULL COMMENT 'opciones=tipollamada',
  `fecha` datetime NOT NULL,
  `timezone` varchar(200) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '60'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls`
--

INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `zoomId`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(2, 3, 1, 1, NULL, NULL, NULL, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/92589029676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXdGOUh2VXRRbFpkTklxNm9TZ0xYSzFOSkdkWWFNSWJVdXZoaElGRERiZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhYThpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE3OTMsImlhdCI6MTYwOTM0NDU5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DJJ4yaMyKdFUbHeKB-e-eENfqwIzbdGMNrnoYFBKqB4', 2, '2020-12-30 13:14:20', NULL, 60),
(4, 3, 1, 1, NULL, NULL, NULL, NULL, 'Call Test Individual', NULL, 'https://zoom.us/s/95415883734?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiU3R2U3FZRFRGN0h5a2w3YmJqU09RNFFsWDdLaXlnX0hhWmxCU05wMzVBQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBhdXBSQUJKMUFBQUEiLCJleHAiOjE2MDkzNTE4NzQsImlhdCI6MTYwOTM0NDY3NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cYvkrWMZfJ8CPJd5VbtBNsIESW2SDtpMC5k0kYDFXUg', 2, '2020-12-30 11:11:10', NULL, 60),
(5, 3, 1, 1, NULL, NULL, NULL, NULL, 'Call Test Set', NULL, 'https://zoom.us/s/97839866400?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiekZqeV85X0JoclpzWldDRmV0b0tJRTBpYjQwZV83akJFZ1htMV9QZE1PTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBiWXdpQUJKMUFBQUEiLCJleHAiOjE2MDkzNTIwNDYsImlhdCI6MTYwOTM0NDg0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.568iWC_NSRtrEk35lAKeKCGKDq8k0cTDA1-QG-Q7gb4', 1, '2020-12-30 11:14:02', NULL, 60),
(6, 3, 1, 1, NULL, NULL, NULL, NULL, 'Yoga class', NULL, 'https://zoom.us/s/93979434891?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidmVzRmM0UlctVGxnN3d3WUJrMUluZU4tcmdaY3NHR2F2Q0Y3X3ZEVWNYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwRGZ2QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU2MjksImlhdCI6MTYwOTM0ODQyOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.kls1b_sCbUZYBv6KyLw7rBGo2rS_yEy2R4gMK0S-NSs', 2, '2020-12-30 12:14:00', NULL, 60),
(7, 3, 1, 1, NULL, NULL, NULL, NULL, 'Test Global', NULL, 'https://zoom.us/s/93734417213?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNU8zMTNuc0paV0J4OWdpNVlDNEQwNGpqLWtmU1Bub2prbWN4SEY3VnhnZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBwWEZ3QUJKMUFBQUEiLCJleHAiOjE2MDkzNTU3MTAsImlhdCI6MTYwOTM0ODUxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RftmAUITz5rnehnjCRPM-7Ytl4bBPd4tF4PD-zBuLVE', 1, '2020-12-31 12:14:54', NULL, 60),
(8, 3, 1, 1, NULL, NULL, NULL, NULL, 'Test Global test', NULL, 'https://zoom.us/s/95152785050?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNFpLZV83NzcyR3pmMlo4anc4MVMwSk1TbGFqc0pGaUJhZUl0S3R3SDlTay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTBzN19YQUJKMUFBQUEiLCJleHAiOjE2MDkzNTY2NDcsImlhdCI6MTYwOTM0OTQ0NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.QtFFjV6iADF3HXvQa-gae3Tq0TjDGMKhjuydjwxnb0I', 1, '2020-12-31 00:00:00', NULL, 60),
(9, 3, 1, 1, NULL, NULL, NULL, NULL, 'Private Yoga Class', NULL, 'https://zoom.us/s/93360745123?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiY203RmVCQ3NOaUlsSkcyTDJJRnRsbWJCZHh0dWtiLWlSb2pyMm1fblZacy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA3MF85QUJKMUFBQUEiLCJleHAiOjE2MDkzNjA1NTEsImlhdCI6MTYwOTM1MzM1MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ufxODqV6_RdyIGJsY5jKA1ZnUpbyXqO8IxbXgjKNIY0', 2, '2020-12-30 13:35:00', NULL, 60),
(10, 3, 1, 1, NULL, NULL, NULL, NULL, 'Private Class for Tomorrow', NULL, 'https://zoom.us/s/96485903856?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibDBWWTFCZ0pRbDBSeHpfbF95X0JuZk1xVXFKVURmcFMwbzBXYUF3QWJKdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTA4T3lBQUJKMUFBQUEiLCJleHAiOjE2MDkzNjA2NTYsImlhdCI6MTYwOTM1MzQ1NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.z4jNyIbpTLfzprVVKitK1hho8qDacy58IAW09luV5Is', 2, '2020-12-31 11:30:00', NULL, 60),
(12, 3, 1, 1, NULL, NULL, NULL, NULL, 'Yoga Beginners', NULL, 'https://zoom.us/s/93845368332?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVllGMGQ4eVFRcEgyLU93eEh2eDJDVTBwblhiZEw2Si0wbGV5cklBZDhWMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYTAtX3FYQUJKMUFBQUEiLCJleHAiOjE2MDkzNjEzODEsImlhdCI6MTYwOTM1NDE4MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.os9YSdyOiUyXEoBJdvPc9AdwpaSVG0_gx9M7wKs0xKo', 1, '2020-12-30 13:55:00', NULL, 60),
(13, 3, 3, 1, NULL, NULL, NULL, NULL, 'Workshop', NULL, 'https://zoom.us/s/97563617492?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoia05rSlJ0Q0sxdi1aeDFfUVZESi1oeXRZZ1VNT3h1NVlmTGdZV0xVbDJUUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpWai1WQUJKMUFBQUEiLCJleHAiOjE2MTAxMzkwNDksImlhdCI6MTYxMDEzMTg0OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ532tN12H1MdALQ1uXbewIsy4j6brhBMqBPP-Umyi0', 1, '2021-01-08 14:00:00', NULL, 60),
(14, 3, 3, 1, NULL, NULL, NULL, NULL, 'Intro', NULL, 'https://zoom.us/s/93574307471?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoieDVVVDR4a2RuTG1IdXZCWkIwOFl4bXVXSjE4TkRoY0JNeU9kblQ4NE5KMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYmpYZkIzQUJKMUFBQUEiLCJleHAiOjE2MTAxMzk1NTMsImlhdCI6MTYxMDEzMjM1MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.dHC_tOib4tV0EcxRlY66jcLQF_0WAn_y49MQZ1Petj0', 2, '2021-01-08 14:00:00', NULL, 60),
(15, 3, 1, 0, NULL, NULL, NULL, NULL, 'Testing call', NULL, 'https://zoom.us/s/92511603346?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibHdzZlAzMjh5NDBKemRITUUzaWFkWVN0WlJlYzhkUEE4MnctcHlRbVdSYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNRdGFMQUJKMUFBQUEiLCJleHAiOjE2MTA0NzMzMjEsImlhdCI6MTYxMDQ2NjEyMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JHA5cmEiCQF8nVFnk9f04nsUS5brj0u_mx4ZXcf-iKY', 2, '2021-01-15 11:00:00', NULL, 60),
(44, 3, 9, 4, NULL, NULL, NULL, NULL, 'Sound Healing ', 'The sound of the Gong affects the mind so that you end up in a state between sleep and awakeness, which stills the mind. The vibrations, which goes deep into the body, helps the body\'s self-healing ability to solve tension and stress. After a Gong Relaxation, you often feel relaxed, get better sleep and new energy!', 'https://zoom.us/s/99428845676?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWHVRalVCLW9tMFBWdWtkRDAwMEdQdlVoVFh3UnJleU82T0JBcGVnZEVBby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZG45aTJwQUJKMUFBQUEiLCJleHAiOjE2MTIzNjQxMjIsImlhdCI6MTYxMjM1NjkyMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DQ9Tqr4sVWM1KG825lRAPnj8UONvXoA_VdJXeK2TVEQ', 1, '2021-02-04 08:00:00', NULL, 45),
(17, 3, 1, 1, NULL, NULL, NULL, NULL, 'Public Call For Now', 'Testing call', 'https://zoom.us/s/97018231393?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYUdEZDctcnljOE1uZGxPVThRT1NwbUJtMEFwelBTZU96cG1vaFdRX2xQUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYjNteHFqQUJKMUFBQUEiLCJleHAiOjE2MTA0NzkxMDUsImlhdCI6MTYxMDQ3MTkwNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.47s_8lJyH8LOPfJcnsw8ldKiadMXzRbSIxILNftX5kI', 1, '2021-01-12 12:15:00', NULL, 15),
(18, 3, 15, 1, NULL, NULL, NULL, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/91585109126?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0lMWVRQU2hMZlhlQ1Y4bnB0OVhjUG5rdFRDc2Vtb2pyd2JXbHVOSTBvNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY1ZfVnhoQUJKMUFBQUEiLCJleHAiOjE2MTA5ODg4NjEsImlhdCI6MTYxMDk4MTY2MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.eRfEN9p6VbtOaErGGtHzlCwNcV-nI0KJMdIvvT6R_jQ', 1, '2021-01-18 10:00:00', NULL, 30),
(19, 3, 3, 1, NULL, NULL, NULL, NULL, 'Intro class', 'Desc', 'https://zoom.us/s/99792834871?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicmxIM2lOYXdyeDU0SVRINkJTazhVdGFYYXVFRGlra002LVQ2Zk1aWGdFay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JHd3FJQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ2OTMsImlhdCI6MTYxMTA2NzQ5MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.O5FoUKEjo9qXIoFIN8lRdqzz26HyZmQc8KmLoUnpOEc', 2, '2021-01-20 12:30:00', NULL, 30),
(20, 3, 3, 0, NULL, NULL, NULL, NULL, 'Consultation', NULL, 'https://zoom.us/s/96228600796?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZmcyd3lwZDFHZWdia2VuSWRJUU5mN1p5Q1ZVOFVRcVVCWHREeFB0RGVBUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JITjJEQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ4MTIsImlhdCI6MTYxMTA2NzYxMiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ILj3fwYSfXzyLqXDtQzjYudTJCTiOsQ98IZE0ACVYfs', 2, '2021-01-20 18:00:00', NULL, 60),
(21, 3, 3, 1, NULL, NULL, NULL, NULL, 'Breathwork for Innovation', 'Desc', 'https://zoom.us/s/93007611295?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXV2c2REMk5pS1Q3d1VtNlR5OWxwSllaMlFmaXFUbVphaFlMS1NHMk02OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY2JIa3RVQUJKMUFBQUEiLCJleHAiOjE2MTEwNzQ5MDYsImlhdCI6MTYxMTA2NzcwNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.LNGXXRB3OKMHPaK4I466gKp0uTzb4bE9x2uQkM3vHEY', 1, '2021-01-19 10:00:00', NULL, 30),
(23, 3, 3, 2, NULL, NULL, NULL, NULL, 'Public Session', 'Test', 'https://zoom.us/s/94332087831?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicUVCV09yNGRYcWNaQmdnOFF1ai12dUpRcHBsZzBNR19kNWRJcWtsTXgxYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYY24wYmVoQUJKMUFBQUEiLCJleHAiOjE2MTEyODc5OTEsImlhdCI6MTYxMTI4MDc5MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.DoeF-sRkk2ss-Kj4VVr-eYrmwIAv0z9tKOH0122ynNo', 1, '2021-01-21 19:00:00', NULL, 60),
(30, 3, 3, 1, NULL, NULL, 'roman-videopng.png', NULL, 'Breathwork for Innovation ', 'Test', 'https://zoom.us/s/99732096017?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWW9wRlBzYzNucnBLQlVUeU11V21IZzJiWEtidXFSWE81TFZIeVJyUmtYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJYZU8zQUJKMUFBQUEiLCJleHAiOjE2MTE1MzIwNTgsImlhdCI6MTYxMTUyNDg1OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PSGjJ5rhfBE-rHLjacizH7OuV3S_dk2f3t5LS9w8OzI', 1, '2021-01-24 17:00:00', NULL, 30),
(26, 3, 3, 1, NULL, NULL, 'roman-videopng.png', NULL, 'Breathwork for Memory and Focus', 'Join this 20 minute session to transcend your state of mind and achieve high levels of memory and focus', 'https://zoom.us/s/97025013577?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZVZuNVBMV0lkamJyakZIejFaSmhNQ2VTVmMyV1REd3hhN2ROa2hpb29OVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzJFdHlBQUJKMUFBQUEiLCJleHAiOjE2MTE1MjcxNDEsImlhdCI6MTYxMTUxOTk0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gFeRyHl4YGbz6Hb1ozDJCnK5ek0Mhgeax_MEhKmcLeY', 1, '2021-01-24 15:30:00', NULL, 30),
(35, 3, 3, 4, NULL, NULL, 'iw_demopng.png', NULL, 'Innerworks Platform Demo', 'Exclusive presentation for the coach team to demonstrate features in the first version of the Innerworks platfrom.', 'https://zoom.us/s/93689176598?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoid1E2RzJUSVprZ1RHMDJ5S0U4RFREVDVHQ2RITmRUZmlJS0FsamtyaGpSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzV0bXNsQUJKMUFBQUEiLCJleHAiOjE2MTE1ODgxOTIsImlhdCI6MTYxMTU4MDk5MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ZDoClv4qr5KvX0BbZYE1FH1v28WiNy-NvcnUFhhqh3I', 1, '2021-01-30 06:30:00', NULL, 60),
(36, 3, 2, 0, NULL, NULL, NULL, NULL, 'Consultation', NULL, NULL, 2, '2021-01-26 13:30:00', NULL, 60),
(37, 3, 3, 1, NULL, NULL, NULL, NULL, 'User Onboarding', 'Onboarding session for coaches and students to join the Innerworks program', 'https://zoom.us/s/92731884092?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiWmUyOEZnMDhMM1h3ME9UREl2NGJTS0dyd293UzdKX2ZkdzlMSHVKNlNEdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYYzU1OFhLQUJKMUFBQUEiLCJleHAiOjE2MTE1OTE0MjYsImlhdCI6MTYxMTU4NDIyNiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.8VsbQ9B8pCwq-pspewAzemABM4PmqXyvpo_Gw7bAFlM', 1, '2021-01-31 18:00:00', NULL, 60),
(38, 4, 18, 5, NULL, NULL, 'highly_engaging_content.jpg', NULL, 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/97092458414?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSjAzdWJCS2xiTHJ4V0lIRWVVVmRvZnpUdGd0UW1rbGN5eTJRZ093aEhoZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtYX1h5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3MzgsImlhdCI6MTYxMTg2MDUzOCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.JG3ngE2Klb6yIcbs8_IxTDyEyQOxMOQ-8HpiE0_zEKA', 1, '2021-01-28 12:00:00', NULL, 30),
(39, 4, 18, 5, NULL, NULL, 'highly_engaging_content.jpg', NULL, 'Hero\'s Journey Workshop', 'Test', 'https://zoom.us/s/96476527234?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiLV9EYXZNRlVLVHoxS01nZ1R4cnlRVG9Cc3JLZDV1S0RyQmMteXRNS0RaZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtZQUU5QUJKMUFBQUEiLCJleHAiOjE2MTE4Njc3NDEsImlhdCI6MTYxMTg2MDU0MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.94iOjlG0MAyYcWtwH08GC0YdOPepOswWGCCHosHe5ZU', 1, '2021-01-28 12:00:00', NULL, 30),
(40, 4, 19, 5, NULL, NULL, NULL, NULL, 'First Session', 'This is a test for sessions', 'https://zoom.us/s/94600546115?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiam5uWkMxaVFKY2pxdnVNeHF0NENKNjUyNmZkYk5XOVRxM3VXc2U1a2RIcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZEtqUEtMQUJKMUFBQUEiLCJleHAiOjE2MTE4NzA2ODcsImlhdCI6MTYxMTg2MzQ4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.I1rwWT1Zg9Qk0cGkhWNFPZGZ-zJ6bLdQCMTWlTInrAI', 1, '2021-01-29 02:00:00', NULL, 60),
(41, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/99729884957?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDVoZHFGSjFPNGdMMlFTVm93MERqdXVVbEp5MDA4Nm9YS210WTVnVEVFMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMEw3QUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Nvzqu35EA6WRjqnRIGFM4Ffl7jwr7kv0Tnj0hRj8_Nc', 1, '2021-02-16 02:00:00', NULL, 60),
(42, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/95030990013?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoib0FjRXFkc0hjR3NHWENnUmRnYnRfYUJtekIyOW5GS1Z6bExvUXpZckV4by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZGptMFBWQUJKMUFBQUEiLCJleHAiOjE2MTIyOTEwNTUsImlhdCI6MTYxMjI4Mzg1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.zWmvF41PErMh0aqgz9YFPyXdnziSVd_8xXrxpGNSRAw', 1, '2021-02-16 02:00:00', NULL, 60),
(43, 3, 1, 1, NULL, NULL, 'skärmavbild-2021-02-02-kl-165152.png', NULL, 'The Loop 4: Embrace your shadow (LIVE)', 'Session 4 of The Loop series, offered as a live session for course participants. 35-45 minutes of Shadow breathwork live.', 'https://zoom.us/s/96112508917?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNXpSS0NMVTVad2t5RmRZdlZPRTJCS054d2x5YVFuOWNxSVJxY19XRlNsTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalNJUmtMQUJKMUFBQUEiLCJleHAiOjE2MTg0NDAyODcsImlhdCI6MTYxODQzMzA4NywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.24uc757ZB0XUsrlPTBjpeu96VJwaIOS97w0uTHTgHUc', 2, '2021-04-16 12:00:00', 'America/Adak', 60),
(45, 3, 3, 2, NULL, NULL, 'img_3153jpg.jpg', NULL, 'Demo Schedule', 'Bla bla bla', 'https://zoom.us/s/98430952016?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQlRGbTM5WmdoeTZVR2lkVFNfeXRIQXZXbkt5eDlpNV9aRGNaNUJFZklXby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4SmRrQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3NDMsImlhdCI6MTYxMjg5MDU0MywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.4R08l4-g8dQCsjqsc4QDfoQi63iulelNcxiAWhbpa0U', 1, '2021-02-09 00:30:00', NULL, 30),
(46, 3, 3, 1, NULL, NULL, 'img_3154jpg.jpg', NULL, 'Demo test', 'bla bla', 'https://zoom.us/s/98236149012?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOHNIZFZXSkV4amFTRVpiTFZ5NWlOZ3AxVEZDajJ6SWY3czMtTFllcW5Ldy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZUh4VzFoQUJKMUFBQUEiLCJleHAiOjE2MTI4OTc3OTgsImlhdCI6MTYxMjg5MDU5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.HAzgvFAeHKDcxmjJdpKHicWEqq3ZqtbXL0RYkl4O7sA', 1, '2021-02-09 12:30:00', NULL, 30),
(47, 3, 3, 0, NULL, NULL, NULL, NULL, 'Yoga Intro 1', NULL, NULL, 2, '2021-02-10 06:00:00', NULL, 60),
(48, 3, 3, 1, NULL, NULL, 'dy001484.jpg', NULL, 'Feb 16 Session', 'Testing session', 'https://zoom.us/s/93142513286?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT0ItQzc0cU14bDdsMkhmS0o2aGx5OWtKU0JNR3VlR0FCOE92LWFOZDhGay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZXIySmJOQUJKMUFBQUEiLCJleHAiOjE2MTM1MDMwMzQsImlhdCI6MTYxMzQ5NTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gLGFEYvoiOyKXeUYJCVVulGabFDFjkfwnrxPqxKv-ew', 1, '2021-02-16 12:30:00', NULL, 30),
(49, 3, 3, 2, NULL, NULL, 'img_3153jpg.jpg', NULL, 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96232917912?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVB4S0lUM3hrdWhvYThSVkZ5Z05oMklLclFOd3ZHaG9vakJwUHZoSnc3SS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDcFY2QUJKMUFBQUEiLCJleHAiOjE2MTM4NDE4NTUsImlhdCI6MTYxMzgzNDY1NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.xQhfPLbBk6OGkmS1mYN8SmUXOVeJX8k4ceqOPXyIbTs', 2, '2021-02-24 02:00:00', NULL, 30),
(50, 3, 3, 2, NULL, NULL, 'img_3154jpg.jpg', NULL, 'Feb 20 Test', 'Feb 20 Test', 'https://zoom.us/s/96365592232?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNE9sMUZTWnBicWZBWWl4VDM1VUl1SkZ6S0p0VHptZVZZaEowWVdFLTU5NC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZkFDLXZWQUJKMUFBQUEiLCJleHAiOjE2MTM4NDE5NDIsImlhdCI6MTYxMzgzNDc0MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.cvc7_GEYd53DEePa7SvyqeBlaiw7aZNcfkK8JdKiiqU', 1, '2021-02-23 14:30:00', NULL, 30),
(51, 3, 3, 2, NULL, NULL, NULL, NULL, 'February 23', 'This is a description test', 'https://zoom.us/s/98979398236?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNTZlTURiRWlWVXV6ZlJCU2V4bFhNeWlwaGt2M0VtbGdDblRRUXI2bWVCSS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFVQmZIQUJKMUFBQUEiLCJleHAiOjE2MTQxMTQ4NDUsImlhdCI6MTYxNDEwNzY0NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.gdUVNvT5aQwVH7isWmRK9dF5fYPcVPjvQhw1SYv9I1w', 1, '2021-02-23 13:30:00', NULL, 60),
(52, 3, 30, 3, NULL, NULL, 'img_0007jpg.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(53, 3, 30, 3, NULL, NULL, 'img_0007jpg.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', NULL, 1, '2021-02-24 22:00:00', NULL, 60),
(54, 3, 30, 3, NULL, NULL, '325692_3417.jpg', NULL, 'Feb 23 Live', 'Feb 23 Live', 'https://zoom.us/s/92701510519?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiX2Z3QUJIYTAxR2JfbG5DZnBSbTBwOXdmNzl5Q3V5ZVdkWWZsdm5DbmYyQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiaE56QUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4MTAsImlhdCI6MTYxNDEwOTYxMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.m2DU2aY3PevRhw1Vy6zPfhvZL7lUEBL9qLBK-YnesnM', 1, '2021-02-24 14:00:00', NULL, 60),
(55, 3, 3, 4, NULL, NULL, 'cb067372.jpg', NULL, 'Feb 23 Session', 'Feb 23 Session', 'https://zoom.us/s/99410747909?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiN0hCM0stWWlZc1pqZXd5cWYwOGhQYkM5SGszS3Q1MTFJX3FhQ3hONjEwZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZlFiMmhkQUJKMUFBQUEiLCJleHAiOjE2MTQxMTY4OTgsImlhdCI6MTYxNDEwOTY5OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Lp62CuODwpHnVwJjv22YsuVKjz3wXicgtkRnEzx3AwE', 1, '2021-02-24 17:00:00', NULL, 60),
(56, 3, 3, 4, NULL, NULL, 'cool20080526_001.jpg', NULL, 'Test Feb 26', 'Test', 'https://zoom.us/s/91456256719?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYkZXeHQyOWZzTDVJaUZMVHBETnVHTUNia09Idkx3Q1dxcGQtVkNuYUJTcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZmY3b09wQUJKMUFBQUEiLCJleHAiOjE2MTQzNzY4ODYsImlhdCI6MTYxNDM2OTY4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NDSuy6MNRxRWSpkPVjh1JMUaLWeqEggBNvK4tVKpAb0', 1, '2021-02-27 18:00:00', NULL, 60),
(57, 3, 3, 1, NULL, NULL, 'huge.jpg', NULL, 'Big Image', 'test', 'https://zoom.us/s/92286786524?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiV0F4R19YX0RPemQyVW5Sa3RwSnB4Nk9zTWlLVmk0RG5kdGdJSjFSZWJwdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnQzV0hCQUJKMUFBQUEiLCJleHAiOjE2MTQ2MTA2NDQsImlhdCI6MTYxNDYwMzQ0NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Ause1FBZXJe47EMpqIyEWOa7dYDH8_NhqFpafxWn19g', 1, '2021-03-03 01:00:00', NULL, 60),
(58, 3, 3, 1, NULL, NULL, 'tlm120720kg-138-small.jpg', NULL, 'Breath of Fire', 'To begin, take a comfortable seated position. \n\nClose your eyes, chin parallel to the floor, face and shoulders relaxed.  \n\nThis exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. \n\nAfter the sharp exhale, relax and allow the breath to enter all on its own. \n\nFocus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/95107350138?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiQk1iaWcyYVZjUzZaWWJGZXhveV92R1RodEhWWFdTLUoweWZ0TTl4RTF0TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZnp5ejhmQUJKMUFBQUEiLCJleHAiOjE2MTQ3MTAxMTksImlhdCI6MTYxNDcwMjkxOSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.l8HImoZ9lOkcXySCtxwu4H5u-JJCy3sDlXUVgCP592g', 0, '2021-03-31 13:00:00', NULL, 30),
(59, 3, 3, 2, NULL, NULL, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', NULL, 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome. ', 'https://zoom.us/s/94619340642?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiVDQySDE1VzcyRmYtQnVJNmdxQ1hMMzd0eGtOaE9MMDE2aE9GaThjY0pBZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjBFNTA5QUJKMUFBQUEiLCJleHAiOjE2MTQ3MTQ4NjIsImlhdCI6MTYxNDcwNzY2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.hcRxbXkGzi0LWM5QZKzzCzHFeTRXorXhkVE1FXeCkrQ', 0, '2021-03-29 12:00:00', NULL, 30),
(60, 3, 3, 1, NULL, NULL, 'tlm120720kg-138-small.jpg', NULL, 'Weekly Meditation', 'This class includes 1/2 hour of Vipassana meditation instruction and guided meditation followed by an hour-long teaching talk. Beginners through advanced students are all welcome.', 'https://zoom.us/s/94210581725?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUVgwS1R3aGxVbHBqbnBObXdDdUU3RkFYTmZJczFIeGNrZzVYWGdZZkZvMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdDa05aQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzE2ODksImlhdCI6MTYxNDgyNDQ4OSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.lXTQZERJ97gpIypW5dRYSKJQyc7HMU8dHIENt8W_lTc', 1, '2021-03-08 09:00:00', NULL, 30),
(61, 3, 3, 1, NULL, NULL, 'top-5-scientific-findings-on-meditationmindfulness-small.jpeg', NULL, 'Breath of Fire', 'To begin, take a comfortable seated position. Close your eyes, chin parallel to the floor, face and shoulders relaxed. This exercise wakes up and energizes the body. To practice, we’ll exhale by engaging the muscles of your lower abdomen and forcing the breath out with a sneezing sensation. After the sharp exhale, relax and allow the breath to enter all on its own. Focus on sharp, strong exhales, relax and allow the inhale to happen on its own.', 'https://zoom.us/s/93208341462?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoibWdHVVhZd1llbEZRaHBqa2ZxNl9FYW9TRWpNbzlkSFFUR0dTTWF3SlRWZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZjdFSXZhQUJKMUFBQUEiLCJleHAiOjE2MTQ4MzIxMDEsImlhdCI6MTYxNDgyNDkwMSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.NpC8J3khl4pZrV-UIhZ5B4rmUA_N4uKOfRlWeTVRB94', 1, '2021-03-15 09:00:00', NULL, 30),
(62, 7, 32, 10, NULL, NULL, 'cueva-venados-1-small.jpg', NULL, 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99083961397?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOUdtUmNWY1lPbWRzZElvMDVwNzE4ZTVWdEROUVJkZVNEMUZrc1E2U0pXdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl83ZzhPQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM3MjcsImlhdCI6MTYxNDkwNjUyNywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OhcMwfpoWJKTAXwbEv0oj0MAtuyIID4-LZtEkcJ0AVo', 1, '2021-03-06 18:00:00', NULL, 30),
(63, 7, 32, 10, NULL, NULL, 'entrada-fmm-003_temazcal.jpg', NULL, 'Amazonian ceremony', 'The synergy of integral psychology and indigenous wisdom helps you identify the root causes of imbalance and become aware of how they prevent you from experiencing joy in your daily life. This is an important step towards developing a clear and simple strategy for restoring wholeness. Our team of holistic psychologists are world-recognized experts with over 20 years of experience bridging  psychotherapy, indigenous medicine and nature.', 'https://zoom.us/s/99753362893?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoidHpiNjZqYW9BcW40b2htck9KelB1ZmRtODhQckdEek9nSUlPaW1yNmdYUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl84R1lvQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTM4ODAsImlhdCI6MTYxNDkwNjY4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.tR2XCG06_woHLOsKAKz-aorU6eBSXTE2zWm9rKZpfH8', 1, '2021-03-09 18:00:00', NULL, 30),
(64, 7, 33, 9, NULL, NULL, 'img_6822-will-need-cropping.jpg', NULL, 'Sound Healing', 'Sound healing works on vibration. Everything is a vibration and you tune your body like you tune an instrument. Different instruments are set to certain frequencies. Sound healing allows your body to heal itself by slowing down your brain waves, which affect every cell in your body, shifting them from diseased to being in ease. It’s just aligning it with whatever you need.', 'https://zoom.us/s/95606205299?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNzNkSE15YWhIU3JMZnA2RWI5UDZKdjRneWIzbmNWQ19sZlJPTWQ5ZGM2RS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl85UTY1QUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQxODYsImlhdCI6MTYxNDkwNjk4NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.-h0YU96soev1JT63E6u-dYExTbWbHRG_u5a0UHoG0fc', 1, '2021-03-08 19:00:00', NULL, 30),
(65, 7, 33, 10, NULL, NULL, 'gws-temazcalero-or-mexican-shaman.jpg', NULL, 'Temezcal / Sweatlodge', 'A temazcal is a traditional Mexican steam bath that is in many ways similar to the Native American sweat lodge. Besides promoting physical well-being and healing, the temazcal is also a ritual and spiritual practice in which traditional healing methods are used to encourage reflection and introspection. While the body rids itself of toxins through sweating, the spirit is renewed through ritual. The temazcal is thought to represent the womb and people coming out of the bath are, in a symbolic sense, re-born.', 'https://zoom.us/s/91081953418?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUUd6RVB2b0hTb0dCY09NVngxZ3hrbF9DalZNclBGNlNtVnNhaTlfMEl1by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZl8tV1JzQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTQ0NzAsImlhdCI6MTYxNDkwNzI3MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.d5RPnKjj3A8FyixJSHewQL2ymjqo2vLwIykH7AoiIU4', 1, '2021-04-13 16:44:06', NULL, 30),
(68, 7, 34, 9, NULL, NULL, 'arttherapy3png.png', NULL, 'Art Therapy', 'Art therapy is founded on the belief that self-expression through artistic creation has therapeutic value for those who are healing or seeking deeper understanding of themselves and their personalities. According to the American Art Therapy Association, art therapists are trained to understand the roles that color, texture, and various art media can play in the therapeutic process and how these tools can help reveal one’s thoughts, feelings, and psychological disposition. Art therapy integrates psychotherapy and some form of visual arts as a specific, stand-alone form of therapy, but it is also used in combination with other types of therapy.', 'https://zoom.us/s/94586952459?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTFp6anZWNGFPYk5FX2VBYjVxSzZoejVqNVhEanp1Q2JNcmNYZjJkcUZHZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYZ0FEV2lfQUJKMUFBQUEiLCJleHAiOjE2MTQ5MTU3ODIsImlhdCI6MTYxNDkwODU4MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.efQkT-LwkZVszGw0t9TDOQQkJJID7ecKM_JXXRvMu1E', 1, '2021-03-06 15:00:00', NULL, 30),
(72, 8, 29, 21, NULL, NULL, 'ih212972.jpg', NULL, 'Live session', 'bla blabla', 'https://zoom.us/s/93474495279?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSzJaMHJJaG9xdXY3cTJSV0Q1cnRpbkpEZEpRR091S0JBb2dEOUlyU1ZpZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaURfZzYzQUJKMUFBQUEiLCJleHAiOjE2MTcxMjkzNjgsImlhdCI6MTYxNzEyMjE2OCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.5kp9Ns_qAPHp9ehqKFotD5YE8dfPsOdlZfPKhVKMAdg', 1, '2021-04-01 09:00:00', NULL, 30),
(73, 3, 44, 4, NULL, NULL, 'student-using.jpg', NULL, 'Onboarding of Guides', 'We will introduce the latest version of the digital platform to our guide community and outline what we are building ahead so you can get started with creating and sharing content.', 'https://zoom.us/s/91932063976?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiYnFfM3NPYVV0cFdCNFZNNDNqZnRRWXo2MUY4LTVtWVVwZmhnUDBXM0YzVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajdraTJMQUJKMUFBQUEiLCJleHAiOjE2MTkxMzU1NjQsImlhdCI6MTYxOTEyODM2NCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.Vm-La3a8rnHO-tGPIDDH9q4M_rZwkHWcnjcZoGEuP5o', 1, '2021-04-23 14:30:00', 'Europe/Stockholm', 90),
(71, 8, 29, 21, NULL, NULL, 'video-final.png', NULL, 'Updated call', 'This is a call test after edited', 'https://zoom.us/s/92279692885?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiOXF0MnpYc0hUTTM0cUpBSjYyNVBPTUxMZjRmQ252MVMxUU5GbHZzLUpTMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYaGY0XzJPQUJKMUFBQUEiLCJleHAiOjE2MTY1MjM2ODAsImlhdCI6MTYxNjUxNjQ4MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.yCJ1TLQDrdZfVokvp5uSsxSpEoK-YeKfCGY1cKJ_GE0', 1, '2021-03-24 08:30:00', NULL, 90),
(95, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1623224042.jpg', NULL, 'Meditation Sweco', 'Meditation time :)', 'https://zoom.us/s/94450722881?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJwV1RxaXd1b0c1U3BNTUpyTHJiUFN2SnlQYks1OHlWS2pBMTY5RkFMczBnLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhudnNVU2lBQkoxQUFBQSIsImV4cCI6MTYyMzIzMTI0MiwiaWF0IjoxNjIzMjI0MDQyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.UOcnzCIj4vez2vycZrLzAwmHkbRJ9BZhozAurPtUhso', 1, '2021-06-09 11:30:00', 'Europe/Stockholm', 30),
(75, 3, 37, 4, NULL, NULL, 'vials-of-samples-with-positive-results-from-patien-r2rhlfz.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/94486212662?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNVlHNVRIUTh2NGNwZl95X1RWYmxLV3JYbnV2R2dkdmhXSFZkeXlpREFtby5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1MdTE1QUJKMUFBQUEiLCJleHAiOjE2MTgzNDA1MzAsImlhdCI6MTYxODMzMzMzMCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.WEHLdgTEx_LfOQ6PJYwVEWdee5nzr9JvyZVjR2I2oAY', 3, '2021-04-14 09:00:00', NULL, 30),
(76, 3, 37, 4, NULL, NULL, 'video-conference-dd9bdje.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/98664949383?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUU5iR3R2T0hoNEU0eFVKU2g3VXc1Y3A4b0k0elFtQ05nck4zWnJEak5kYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1QX0c0QUJKMUFBQUEiLCJleHAiOjE2MTgzNDE2NDYsImlhdCI6MTYxODMzNDQ0NiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.PivqL82KElgtp7yC51y5E_J-hPeEOnnfwbSNqzIpub4', 2, '2021-04-14 09:00:00', NULL, 30);
INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `zoomId`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(77, 8, 29, 21, NULL, NULL, '42-16624306.jpg', NULL, 'Session test 1', 'Session test 1 Session test 1 Session test 1', 'https://zoom.us/s/91903233225?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiSXRDQk5XT1hKbXBNeEdnd3BkOU9LYTVVTEF0NW1rbzN6VlYyOG84VEE2Zy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYak1YTWFHQUJKMUFBQUEiLCJleHAiOjE2MTgzNDM1MzUsImlhdCI6MTYxODMzNjMzNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.pAXJygXfjQ3G2jBoRpCZ1HFPlIctaaV7Go4jXnGkLts', 3, '2021-04-14 09:30:00', NULL, 30),
(78, 3, 37, 4, NULL, NULL, 'video-conference-dd9bdje.jpg', NULL, 'COVID-19 in Workplace', 'TBD', 'https://zoom.us/s/99889707394?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiT1cya1JmV25pUEpxX2ExT205d3VOZGpwUzR2Wno5SF9ZM0Z3eGZSdEVicy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYalI1ck5TQUJKMUFBQUEiLCJleHAiOjE2MTg0MzY0NjAsImlhdCI6MTYxODQyOTI2MCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.OV8pih55NLxVea4sZ34sbeOc7d3OEi66eCTbaCw58j4', 3, '2021-04-16 08:00:00', 'America/Adak', 30),
(87, 3, 44, 1, NULL, NULL, NULL, NULL, 'Breathwork for Creativity', 'This is a special breathwork session designed to move your mind and body into a state of high creativity', 'https://zoom.us/s/91804820837?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoieTRPbE4xM213ZGRpZmI3b2hfSTFHdmlxN1N1VTBHSGltcHc0ZWVsYk9QVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbGRocVVxQUJKMUFBQUEiLCJleHAiOjE2MjA3Nzg5NzUsImlhdCI6MTYyMDc3MTc3NSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.9yS6q5GldoSLqUAf5D18ssscW1OPX-2vwnEv8YR6ISc', 1, '2021-05-19 07:30:00', 'Europe/Stockholm', 30),
(81, 3, 37, 1, NULL, NULL, NULL, NULL, 'Test', 'TBD', 'https://zoom.us/s/92351401707?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiTVVXd0VHNDBMRzFNTGktZmRFeUtkUl9Hd2VETl93Zjh5NXFzODV1TWNHcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYajNUWjJRQUJKMUFBQUEiLCJleHAiOjE2MTkwNjM5NjIsImlhdCI6MTYxOTA1Njc2MiwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.RLfL6gR4U6-Qx3AOH_yb4WY9ke4vrcfU-NUNdbzfmhk', 2, '2021-04-22 07:00:00', 'Europe/Stockholm', 30),
(82, 3, 44, 2, NULL, NULL, NULL, NULL, 'Art Therapy', 'tbd', 'https://zoom.us/s/96927082395?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiNW9rdWlVem5wS1NDTDAzSDZnRV9XNVJ3R19WYnFUZFZmc29hcHktWkNBYy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYai05cW5uQUJKMUFBQUEiLCJleHAiOjE2MTkxOTI0ODEsImlhdCI6MTYxOTE4NTI4MSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.sqNJU3j58DZfQjZ5Oi2QeZvF_4tid_hgFomQALHtaas', 2, '2021-04-28 08:00:00', 'Europe/Stockholm', 30),
(83, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx.jpg', NULL, 'Weekly Meditation', 'This is a weekly series for all levels to explore themselves and move into stillness for body and mind through different styles of meditation. Welcome!  ', 'https://zoom.us/s/94539032093?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiMWtmSzhnd2x4LUYyZWZWTkxZMmNNalkxbGJzM0VfTm41ZGFNTTY2RU02dy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbGNHbW1kQUJKMUFBQUEiLCJleHAiOjE2MjA3NTUxMDUsImlhdCI6MTYyMDc0NzkwNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.-08Vf6J969j3bPfpIsDm-iazmKeZBFDt9fx5lWvrPhE', 1, '2021-05-12 11:30:00', 'Europe/Stockholm', 30),
(90, 3, 6, 2, NULL, NULL, 'papaioannou-kostas-tysecum5hja-unsplash1622016434.jpg', NULL, 'Meditation Sweco', 'Meditation for anyone.', 'https://zoom.us/s/91046077015?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiUHd1RGo4anNZalk5MUx4VVVzOWJ1M05UVFh1Vnp5VHZLQ0hxazh6WW41QS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbW50cVB0QUJKMUFBQUEiLCJleHAiOjE2MjIwMjM2MzUsImlhdCI6MTYyMjAxNjQzNSwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.q0KFpK0jPwlOM_wLEjE0QsPAupQBv0Sd5gdqtAkHHGA', 1, '2021-05-26 11:30:00', 'Europe/Stockholm', 30),
(89, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1621412313.jpg', NULL, 'Meditation Sweco', 'Weekly meditation. Not earlier experience required.', 'https://zoom.us/s/97736079098?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoicFpqX2hSOGtfYmhSbXp4SEVQZXFUSEhneVZhOVQ0Z3hidlBQbEdwVm94by5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbUR0SHFFQUJKMUFBQUEiLCJleHAiOjE2MjE0MTk1MTMsImlhdCI6MTYyMTQxMjMxMywiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9.ABm86K811RgSMY-MC8_nwV4ezTGs2Vb-kNJFGyyH9Cc', 1, '2021-05-19 11:30:00', 'Europe/Stockholm', 30),
(88, 3, 6, 2, NULL, NULL, NULL, NULL, 'Meditation Sweco', 'Borde funka nu. Heja oss!', 'https://zoom.us/s/94637281366?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJlODl6QzdGb1NfMm93MnpqWGhKblFBIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZlJYcWlMZGw3ZnBkVjJIaGJTc2ZJeE5Cbmd1SnVka2dQUEZMS3VCcW14TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFBMkYzTVFBQUFYbGY2ZVI5QUJKMUFBQUEiLCJleHAiOjE2MjA4MTkwMzQsImlhdCI6MTYyMDgxMTgzNCwiYWlkIjoiVmpaOWp1NVNTX3VsWllYMUdqUGc3dyIsImNpZCI6IiJ9._Fi3K4k-XLYX5CQPraDbpXDlFaYbW34EGNybdxwwFQc', 1, '2021-05-12 11:30:00', 'Europe/Stockholm', 30),
(92, 3, 6, 2, NULL, NULL, NULL, NULL, 'Meditation Sweco', 'Meditation', 'https://zoom.us/s/98261832057?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJra0cwdHhKSjJGVjdkT2lLM3BaR1FOV2d3eFBMTjlCdmZEejgxMXlTM3JVLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhuTUQ3UThBQkoxQUFBQSIsImV4cCI6MTYyMjYzMzQ1MSwiaWF0IjoxNjIyNjI2MjUxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.a9C_rb5v1zlc-NwsgcoF4ImEznnw_xWmJ6n3qCM8cPE', 1, '2021-06-02 11:30:00', 'Europe/Stockholm', 30),
(98, 3, 44, 1, NULL, NULL, 'sunset-background-for-meditation-bjn4r8m1624039147.jpg', NULL, 'Breathwork for Innovation', 'This is a breathwork practice designed to invoke a state of creativity', 'https://zoom.us/s/99339982933?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJDa3FBUlpuNW5RdU9rZHQ2VmZ4RVJ5cGN2WGRxQVIyVmFFMzVaUHdodzVvLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhvZ1JzcndBQkoxQUFBQSIsImV4cCI6MTYyNDA0NjM0OCwiaWF0IjoxNjI0MDM5MTQ4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.mUJG7Z6wAIFTd0izMNYbjGOEuPRbBIRW0wC-e_I02vE', 1, '2021-06-20 17:00:00', 'Europe/Stockholm', 30),
(97, 3, 6, 2, NULL, NULL, 'group-of-people-making-yoga-exercises-at-studio-pywpcsx1623835629.jpg', NULL, 'Meditation', 'Meditation för alla :)', 'https://zoom.us/s/92538477316?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJ0STlxZ1Bld0dueFNqSXB1WDlZR09DNVNHSTJmdkpCVlVvSi1fc09NbU9VLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhvVUpWZ2xBQkoxQUFBQSIsImV4cCI6MTYyMzg0MjgyOSwiaWF0IjoxNjIzODM1NjI5LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.eaG2mc9UnHe7MmoM6WCOcxalY4wAYtdluvEUuYi9eCY', 1, '2021-06-16 11:30:00', 'Europe/Stockholm', 30),
(100, 3, 6, 2, NULL, NULL, '759fd534-f056-4148-9249-3bba5bddee3f1624440137.jpeg', NULL, 'Meditation', 'För en stunds vila för sinne och kropp', 'https://zoom.us/s/94580801471?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoiYXcxIiwiY2x0IjowLCJzdGsiOiJCeEcxcjBabDVYMXZ1bEx6RE5KUVZGczU2WjRVd1E5bmtlMGhIS2h4SkJnLkJnWWdiR2xHVldoVGNqQldhR3BJWWpCSlF6WmFPVVJQWWtGcE5sbzJVbEJQU1hwQU5qSTRaVFF6WkRVMllqTmhZVEE1TXpZMFptWmtNV0k0TlRJd1pXVmxPREU0TURkbFpUWmpZalV5T1RZeE9UQXlOamd5Wm1NeFpqY3hZams1T0RNek5BQWdiekpEZDFOV1NUSnBSM2s1Ym5SNGVFUlpVVEJGWkZGSU1WTkRWM041VkZBQUEyRjNNUUFBQVhvNExXclNBQkoxQUFBQSIsImV4cCI6MTYyNDQ0NzMzOCwiaWF0IjoxNjI0NDQwMTM4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.8rq3YDlnyVgRPmu-mhAGgvGBoOWEdaANYyNuw4TDnXU', 1, '2021-06-23 11:30:00', 'Europe/Stockholm', 30),
(101, 9, 38, 18, NULL, NULL, 'shala-new-logo1628974645.png', NULL, 'Platform Introduction', 'This is an introductory session for new platform members', 'https://us06web.zoom.us/s/83291805997?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWlZMc3RFVFNTUzRtTmI0SHVGZzNXWlpxRkhEVUVMX0pSdkRpaGF3bEI4WS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdSblNESVFBU2RRQUFBQSIsImV4cCI6MTYyODk4MTg0NiwiaWF0IjoxNjI4OTc0NjQ2LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.1skClPOZWuEmfK13P62THcixLlyEVbBDFbnEBKFeATE', 1, '2021-08-15 16:00:00', 'Europe/Stockholm', 30),
(102, 3, 44, 4, NULL, NULL, 'stress1629991005.jpg', NULL, 'Managing the Stress of Deadlines', 'As professionals, we always have deadlines to meet. They are like an immovable object that we have to manage our uncertain lives around. How can we manage the stress of dealing with deadlines so our lives outside of work is not affected? ', 'https://us06web.zoom.us/s/83823234557?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiaDVhZFN2Vkp4NHZyRnVINXZVZGtwUWl0aUNLOURMbkktN0JyWl9FZlZ6QS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdnd2p0d3dBU2RRQUFBQSIsImV4cCI6MTYyOTk5ODIwNSwiaWF0IjoxNjI5OTkxMDA1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.AMnGO5Bq-PNHLdrZzk9-ZAbRCF_wraXtp5FLGNVmLBI', 1, '2021-08-26 20:00:00', 'Europe/Stockholm', 30),
(103, 3, 44, 1, NULL, NULL, 'stress1630085810.jpg', NULL, 'Breathwork for Creativity', 'This is a test session', 'https://us06web.zoom.us/s/89054361629?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiM1h3V2lTTkl2d29yVktPZDJxaVpBLXJJVnlrOUtTM3NLcFRjc1dtbzFpay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdpSy1Ld0FBU2RRQUFBQSIsImV4cCI6MTYzMDA5MzAxMCwiaWF0IjoxNjMwMDg1ODEwLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.p8tZe-VBGuP_wE2TUiqFaduwkzzMCfx8hpe8EvwHTxM', 1, '2020-00-00 00:00:00', 'Europe/Stockholm', 30),
(104, 3, 44, 1, NULL, NULL, 'stress1630085950.jpg', NULL, 'Breathwork for Creativity', 'This is a test', 'https://us06web.zoom.us/s/86222247795?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoidzJwNTBCZUI1dklsLUVFZDF5WHJRaFppT19lMG52YmFDVjRzMXp2bERYTS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdpTEd2ZWdBU2RRQUFBQSIsImV4cCI6MTYzMDA5MzE1MSwiaWF0IjoxNjMwMDg1OTUxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.GYSqkQohGwyGYablIOH_C-4iKtetZ-OebmXbL3Kxwcc', 1, '2013-00-00 00:00:00', 'Europe/Stockholm', 30),
(105, 3, 44, 1, NULL, NULL, 'stress1630085997.jpg', NULL, 'Breathwork for Creativity', 'This is a test session', 'https://us06web.zoom.us/s/88950698324?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiX3gyNElPaWJUdU9kMzZFZVhHWjB5bm1kQ1Vmclo4TkkzbVplWEQxNFBHUS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdpTEpqaEFBU2RRQUFBQSIsImV4cCI6MTYzMDA5MzE5NywiaWF0IjoxNjMwMDg1OTk3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.L89kXGda1onG337MJXnfvIG9XHsna6eF1yI2WgN97E4', 1, '2021-08-27 13:00:00', 'Europe/Stockholm', 30),
(106, 3, 44, 1, NULL, NULL, 'screenshot_1-shell1630086353.png', NULL, 'Discussion with Nils', 'Test session', 'https://us06web.zoom.us/s/83083704186?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiS1BWYk9ZVUFRalpMM2JVUmZEM2dRVjVnc3FNNmViUkpGODRxcldZT0ZFZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdpTGZVVFFBU2RRQUFBQSIsImV4cCI6MTYzMDA5MzU1MywiaWF0IjoxNjMwMDg2MzUzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.QuTz7OTnfHMnd3cBCz30k_88vtkhEDrvBwbTx-WLOM8', 2, '2021-08-27 20:00:00', 'Europe/Stockholm', 30),
(107, 9, 38, 17, NULL, NULL, 'stress1630462937.jpg', NULL, 'Intro to Platform', 'Test', 'https://us06web.zoom.us/s/82605480824?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiZ1FSVURSR09XdUdSNmc2Z3pRdjU2bEpwSGlZcmFqLXNpa2RfQWF0YWVSOC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdueW9NRkFBU2RRQUFBQSIsImV4cCI6MTYzMDQ3MDEzOCwiaWF0IjoxNjMwNDYyOTM4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.CAvnFQ-x-1MH9PqyZvztMoOM3O_fexBbMZJBNEwG1RY', 1, '2006-00-00 00:00:00', 'Europe/Stockholm', 30),
(108, 9, 38, 17, NULL, NULL, 'stress1630463014.jpg', NULL, 'Onboarding of Guides', 'This is a test', 'https://us06web.zoom.us/s/89327392713?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoielJQQUlmU09idzhlV19EbXMyVm9jenpZcGZnOGRLLTJhWW1qVlc0TktmNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjdueXM1S3dBU2RRQUFBQSIsImV4cCI6MTYzMDQ3MDIxNSwiaWF0IjoxNjMwNDYzMDE1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.BmeqNX6RPW5EaAntLjs4MiGvtml-B3zjKlsnvuVCkmU', 1, '2021-09-01 06:00:00', 'Europe/Stockholm', 30),
(109, 9, 38, 17, NULL, NULL, 'stress1630955948.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/82331269788?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoicHBNSW5KN05LNF9wcGVFVl9KOWxJYkl0aGh3X0xXd2VMTzl3c0R2UE1pVS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2SXpMdndBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzE0OCwiaWF0IjoxNjMwOTU1OTQ4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ncHW-fIUJWTLfq3wTAc1hcfdhdUsYbmAMeWVx96zNHE', 2, '2022-00-00 00:00:00', 'Europe/Stockholm', 30),
(110, 9, 38, 17, NULL, NULL, 'stress1630956001.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/84331962860?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWTFFdG5QOGhCRi1DbURieWd0MHFSeVdKdm1jbkNCRkNReUF2cFdjTDMxRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2STJja2dBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzIwMiwiaWF0IjoxNjMwOTU2MDAyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.w_ausJpNhdWN8VcJiQG_8dJoeOnATdqAvIemuCffUIw', 2, '2022-00-00 00:00:00', 'Europe/Stockholm', 30),
(111, 9, 38, 17, NULL, NULL, 'stress1630956072.jpg', NULL, 'Platform Introduction', 'Test', 'https://us06web.zoom.us/s/89591381958?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiQ3lxVWkxaHVYdV92QWNpYXVTdmtPZjF2aE02Xzd0Snd5eFk4ZnZ5NGxORS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2STZ4UVFBU2RRQUFBQSIsImV4cCI6MTYzMDk2MzI3MywiaWF0IjoxNjMwOTU2MDczLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.h1yXEnMOiRx2IvxFXNfzgZIhJ7Jt9tRdsOs4eehkZqk', 1, '2021-09-06 22:00:00', 'Europe/Stockholm', 30),
(112, 9, 38, 17, NULL, NULL, 'stress1630959467.jpg', NULL, 'Session with Evolvia', 'Test', 'https://us06web.zoom.us/s/87254955165?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiM3VnbGVZUGJQd3FTamk5QVE2N21uTEZMRHQ3VGpQcWE2d0hxaHF4RmRmQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TUo4NndBU2RRQUFBQSIsImV4cCI6MTYzMDk2NjY2NywiaWF0IjoxNjMwOTU5NDY3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.UQgw-iqmWr0n4afJNAVQtli73CR9EfzdLoITk0qqf1k', 1, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(113, 9, 38, 17, NULL, NULL, 'stress1630959542.jpg', NULL, 'Evolvia Test', 'Test', 'https://us06web.zoom.us/s/88643371788?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiR3hOQzdUV2ZNOHBMSjBGUlFrTWZ4d1l2YUNRUGRFVE8tUWpKNFRwZDhVRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TU9pTGdBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njc0MiwiaWF0IjoxNjMwOTU5NTQyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.xQofqh08G70TVK0BUiRV8C4miGiguCVzDaJMKoCVTVc', 2, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(114, 9, 38, 17, NULL, NULL, 'stress1630959684.jpg', NULL, 'Art Therapy', 'test', 'https://us06web.zoom.us/s/83744067165?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiSktFRF9YcW1GME55cGlrVjMwaFJVdVZsQ25oblBPbHdFaWpaLWVESnlfRS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TVhPRHdBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njg4NSwiaWF0IjoxNjMwOTU5Njg1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.oFsUmtyuXRPLT9KvNkY8N0gyikweBY9i_nArPMp6mQA', 2, '0000-00-00 00:00:00', 'Europe/Stockholm', 30),
(115, 9, 38, 17, NULL, NULL, 'stress1630959740.jpg', NULL, 'Onboarding of Guides', 'Test', 'https://us06web.zoom.us/s/88590392491?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoibUx5bzRJMkZKQTFHV09PeDRBQmloWmVJMmtFZy1wRXdoOVhISDlfVF9lWS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd2TWFuU3dBU2RRQUFBQSIsImV4cCI6MTYzMDk2Njk0MCwiaWF0IjoxNjMwOTU5NzQwLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.sTjym8cdMhjo3xNG2Fyxo0hkb07bxTkfhNgfMa94siE', 2, '2021-09-06 22:30:00', 'Europe/Stockholm', 30),
(119, 9, 38, 17, NULL, NULL, 'blogger-conducting-online-yoga-class-y27lxr61631192790.jpg', NULL, 'Test Session', 'Test', 'https://us06web.zoom.us/s/85352150503?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoid0RVdmFBSkl3ZHl4azNvTWhNZGYzSU1KTndKY3dsVl9nSE1CUmQzWWk1TS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjd5cXE0SmdBU2RRQUFBQSIsImV4cCI6MTYzMTE5OTk5MSwiaWF0IjoxNjMxMTkyNzkxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.TA4o4pWtIMLImcqzMl1BqMukLLU4Cm3fVTU290SJlt0', 2, '2021-09-09 15:30:00', 'Europe/Stockholm', 30),
(120, 9, 38, 17, NULL, NULL, 'meditation-fs9suqr1631193026.jpg', NULL, 'Another Test', 'Test', 'https://us06web.zoom.us/s/81263843596?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiemhRc3BySm5JYTFEWjdLVXBYU0pUbWlzeEs2RDZZTWhrRERpSElKSzRSMC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2SVZuUkFBU2RRQUFBQSIsImV4cCI6MTYzMTcwMDg2MiwiaWF0IjoxNjMxNjkzNjYyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.Qfrd0TNtLQxmdmZa12HjtRAX5C791gQFbHTVwnOe-Bc', 2, '2021-09-15 13:00:00', 'Asia/Kolkata', 30),
(121, 9, 38, 17, NULL, NULL, 'category-connection1631449372.jpg', NULL, 'Art Therapy', 'Test', 'https://us06web.zoom.us/s/82751602454?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiTThyWnRQN0NKTkU2TC1JMmNRaTNzRXRDQUwxSlpuekxUQTZTLVN3VXpGdy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjcyZlhaUkFBU2RRQUFBQSIsImV4cCI6MTYzMTQ1NjU3MiwiaWF0IjoxNjMxNDQ5MzcyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0._o_RM_JEHoRI9PFaFQ3QchV0ShGlxosCKwJRsnZJgbM', 2, '2021-09-12 14:30:00', 'Europe/Stockholm', 30),
(122, 9, 38, 17, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1631583089.jpg', NULL, 'Test 09/14', 'Test', 'https://us06web.zoom.us/s/84490839034?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiNkVFcERsN0x5NDdjREZTMklvMUxnMmNPaG02OGt3Y2xGOHpwSDk3NWs4cy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc0ZTQyVmdBU2RRQUFBQSIsImV4cCI6MTYzMTU5MDI5MCwiaWF0IjoxNjMxNTgzMDkwLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.zVPvmDKOl3fqQW3LkYgyHs99M8k1kQUsbKo35HXTGd0', 1, '2021-09-14 06:00:00', 'Europe/Stockholm', 30),
(123, 9, 38, 17, NULL, NULL, 'meditation-fs9suqr1631622770.jpg', NULL, 'Test 09/14 Part 2', 'Test', 'https://us06web.zoom.us/s/89053567217?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiWXFfZUVCbXhfV19BaVhPdU9YZHphZ0k2dEQxelIxbVR5a1ZqZUpxb0FzQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RXhhSWdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDAxNCwiaWF0IjoxNjMxNjIyODE0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ZAUdT6K9oVeNM5EBXDZqywo_athUlgT-BcNOXVFRgIU', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(124, 9, 38, 17, NULL, NULL, NULL, NULL, 'Art Therapy', 'Test', 'https://us06web.zoom.us/s/85642676650?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoibnBRNWZudzZZWUVoZTVlc1ZSWmdpRFhKUWxneTlHWEtFSDVISWJWMTY3OC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RTBqRXdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDA2NSwiaWF0IjoxNjMxNjIyODY1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.VvrJH-h3jKrLP7aL8qWbp4YHjOi7vrWuqIvBmsqyURY', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(125, 9, 38, 17, NULL, NULL, NULL, NULL, 'Test 09/14 Part 3', 'Test', 'https://us06web.zoom.us/s/83342086340?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiX3ZEVXIycm1jaE1BMWJGQXlQNklGdTFKeFJ1OWNYUEFoZGRMcGtiUzJBQS5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc1RTRsWmdBU2RRQUFBQSIsImV4cCI6MTYzMTYzMDEzMSwiaWF0IjoxNjMxNjIyOTMxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0._CGp8XBRI0pyDlOLsOn3PRLqu6ijMLBJODNk6i84MoI', 2, '2021-09-14 15:00:00', 'Europe/Stockholm', 30),
(126, 9, 38, 18, NULL, NULL, '41nn6rihmzl_ac_ul260_sr200260_1631683239.jpg', NULL, 'test1', 'test1', 'https://us06web.zoom.us/s/81374592146?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiclhvY1lYQ0FTSS1LdW9LemNXR29WemthUVZXQTViN015OHFoSlZGV2hzcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2RlA4U0FBU2RRQUFBQSIsImV4cCI6MTYzMTY5NzYyMywiaWF0IjoxNjMxNjkwNDIzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.fvRE1E89qxjzNI3cSLcnO3QhdeBiCjGWJhqaszAEP-s', 1, '2021-09-15 13:00:00', 'Asia/Kolkata', 60),
(127, 9, 38, 17, NULL, NULL, 'r1631694226.jpg', NULL, 'test2', 'test2', 'https://us06web.zoom.us/s/81532045595?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgxNTMyMDQ1NTk1Iiwic3RrIjoiZVJFbFdiYnhLaDd0Ui0xaWxVdC1LNEh6NzJCM09JcDVZRVRQRVVDWXBqSS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhGbjZJUWdBU2RRQUFBQSIsImV4cCI6MTYzMjQ3MjE2MywiaWF0IjoxNjMyNDY0OTYzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.AFH3VtPg1UwcoPbgA9CXu-rUxWXom3lQ73XzVhb4LqQ', 2, '2021-09-24 12:00:00', 'Asia/Kolkata', 30),
(128, 9, 38, 17, NULL, NULL, 'oip1631698766.jpg', NULL, 'test3', 'test3', 'https://us06web.zoom.us/s/83654539252?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiLV9HLVp3ZDc1d0JwX2ppVUNWUUQ5WVJWZTM2b1phVHNvSlZydGY2R1VGNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2Tk5PTndBU2RRQUFBQSIsImV4cCI6MTYzMTcwNTk2NywiaWF0IjoxNjMxNjk4NzY3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.c3etGSl7QBwxyO2_T2Yo4cW8gQANZiSXVTKR51kGev0', 1, '2021-09-15 16:30:00', 'Asia/Kolkata', 30),
(129, 3, 1, 1, NULL, NULL, 'r1631706935.jpg', NULL, 'test1', 'test1', 'https://us06web.zoom.us/s/89214569863?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiUks5SlFGQUZBM3Nramdwakx2cnlicDZ6cV9NVVQ4YVpqX01rRGI1QWFTNC5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc2Vlp1X1FBU2RRQUFBQSIsImV4cCI6MTYzMTcxNDU2MSwiaWF0IjoxNjMxNzA3MzYxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.6ld5XVxo3CsmPksfBVh7sw7eolY3WJp2OJC7A_d-Tew', 2, '2021-09-15 15:00:00', 'Europe/Stockholm', 30),
(130, 9, 38, 17, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1631710214.jpg', NULL, 'Test 09/15', 'This is a test session. Our highest online bonus points offer ever. Earn 50,000 bonus points worth up to $1,250 in Amtrak travel. Must apply here for this offer. Offers vary elsewhere.Our highest online bonus points offer ever. Earn 50,000 bonus points worth up to $1,250 in Amtrak travel. Must apply here for this offer. Offers vary elsewhere.', 'https://us06web.zoom.us/s/86048914099?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiQXU2Sk1POXUyZGNQM2xBdndrWGs2TGtJbEZidTZuSE5VcG1TamRHT3Z2Zy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3bVBTSVFBU2RRQUFBQSIsImV4cCI6MTYzMTc5OTMyNCwiaWF0IjoxNjMxNzkyMTI0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.45Ll99rrZU9rejTayxN9Z1uynp2aET4yGhIJVm71EKw', 2, '2021-09-16 15:00:00', 'Europe/Stockholm', 30),
(131, 9, 38, 17, NULL, NULL, 'online-photography-school-nt5kr6f1631794257.jpg', NULL, 'Test 09/16', 'Test', 'https://us06web.zoom.us/s/87194214754?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiMGNtRV9MWkxiOHV3N1RfMmFCZWRUdVFRT2JtbFU1RjFhRUtwVmxGRl9Uay5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3b1JnbmdBU2RRQUFBQSIsImV4cCI6MTYzMTgwMTQ1OCwiaWF0IjoxNjMxNzk0MjU4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.hxP9GwaBN2l7Iej6KN3gwl0VtsIoE6IJAVYDPwr2I_c', 2, '2021-09-16 14:00:00', 'Europe/Stockholm', 30),
(132, 9, 38, 17, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1631795556.jpg', NULL, 'Test 09/16 Part 2', 'Test', 'https://us06web.zoom.us/s/87617802899?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiSTNKcXBqdHpuVkQ1MzRfeFF2ZUZfSXR2ZEM1RUtiRkNkS3ptdVd5NVlXZy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc3cGd3bEFBU2RRQUFBQSIsImV4cCI6MTYzMTgwMjc1NiwiaWF0IjoxNjMxNzk1NTU2LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.ZWP4FjnQ79-UXaZf3HNMqM2fIGnYec-6ETcghnB5Itg', 2, '2021-09-16 15:00:00', 'Europe/Stockholm', 30),
(133, 9, 38, 17, NULL, NULL, 'profile-portrait-of-young-african-online-shop-mana-c2ucmh61631880187.jpg', NULL, 'Test 09/17 ', 'Test', 'https://us06web.zoom.us/s/85680690304?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwic3RrIjoiOFp1NzRvRExWRDdsU2x4d1lWa0ZIc2Q5WnZ6NkNqc2NFMXVNZHJvWGxBcy5CZ1lnYkdsR1ZXaFRjakJXYUdwSVlqQkpRelphT1VSUFlrRnBObG8yVWxCUFNYcEFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnYnpKRGQxTldTVEpwUjNrNWJuUjRlRVJaVVRCRlpGRklNVk5EVjNONVZGQUFCSFZ6TURZQUFBRjc4Nk9RQlFBU2RRQUFBQSIsImV4cCI6MTYzMTg4NzM4NywiaWF0IjoxNjMxODgwMTg3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.MKZrjCYmULwb7KycerS9zAP6gLRMbttmojo6ronRmLU', 2, '2021-09-17 14:00:00', 'Europe/Stockholm', 30),
(134, 3, 1, 4, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1632139219.jpg', NULL, 'Innerworks Guides Test Session', 'This is a test session for Innerworks Guides to finalize the My Journey feature before releasing it to real users.', 'https://us06web.zoom.us/s/85404350992?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg1NDA0MzUwOTkyIiwic3RrIjoiR2RuU3gwSG1vMTA2UXVJcGlxcVEwMjZ4Y3h3T3ZFOUlfRU1FRFFLMEZTdy5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhBeFFVRWdBU2RRQUFBQSIsImV4cCI6MTYzMjE0NjQxOSwiaWF0IjoxNjMyMTM5MjE5LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.cORMRXI2REgHwIe6I2-R7RZTM3H5uMIjdB_ewdkYF-M', 2, '2021-09-20 14:00:00', 'Europe/Stockholm', 30),
(135, 3, 1, 4, NULL, NULL, 'female-coach-showing-project-management-studies-ov-pqzw4981632225552.jpg', NULL, 'Innerworks Guides Test Session 1', 'My work is organizing an anniversary event and asked for volunteers. I was eager to help and also encourage others to join in the effort. My enthusiasm for a non-compensatory contribution paid off with superiors commending me and getting to know me personally', 'https://us06web.zoom.us/s/81647746914?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgxNjQ3NzQ2OTE0Iiwic3RrIjoiczZJd2tfTjNWbWN1amdUc0JMVGxObzdjQUtSQUFpbHdFcExPSFN2Yk83ay5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhDSjNONWdBU2RRQUFBQSIsImV4cCI6MTYzMjIzOTMzMiwiaWF0IjoxNjMyMjMyMTMyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.pkvh_heMy7CfVDOyI1J-2HxwgxQ_X4Sf-BiOjIiJMgE', 2, '2021-09-21 16:00:00', 'Europe/Stockholm', 30),
(136, 3, 1, 4, NULL, NULL, 'img-20210915-wa00131632257808.jpg', NULL, 'Innerworks Core Team Meeting ', 'This is a core team meeting to test the MY JOURNEY view. ', 'https://us06web.zoom.us/s/89325733817?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg5MzI1NzMzODE3Iiwic3RrIjoicVMwQ2VoTXpLOGZRREltdjdLakowMmF5R1g5VlhZS2J5N3BMMHdTNkJ0VS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhDaWZsNHdBU2RRQUFBQSIsImV4cCI6MTYzMjI2NTE1OSwiaWF0IjoxNjMyMjU3OTU5LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.L0TI3w0l6dHHhpzNdv1P0Gx8c29I1uGz7rm3EEPGDRo', 2, '2021-09-21 23:00:00', 'Europe/Stockholm', 30),
(137, 3, 1, 2, NULL, NULL, 'mini_fabian-14jpg1632259796.jpg', NULL, 'Test with Roman', 'testitltest', 'https://us06web.zoom.us/s/85111601541?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg1MTExNjAxNTQxIiwic3RrIjoicFpqT2pGVktzOGZXaVhoWHFMVEYybU5Va2FVSzNTeExCUFdnNi1sSGwtVS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhDa2NYY0FBU2RRQUFBQSIsImV4cCI6MTYzMjI2NzIwMywiaWF0IjoxNjMyMjYwMDAzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.s0XXmQC2FiFMM_jZCGDMa8WI-NphercSx9pMJ3lNnN4', 2, '2021-09-21 23:30:00', 'Europe/Stockholm', 30),
(138, 3, 1, 4, NULL, NULL, 'software-engineers-working-on-project-and-programm-xzp2tp71632314947.jpg', NULL, 'Innerworks Guide Test Session 2', 'This is a test session. The candidate will support in\n- assessing the market size for used EV batteries\n- identifying potential application areas\n- defining a market entry / growth strategy and business model\n- defining the clients\' positioning in the market', 'https://us06web.zoom.us/s/83044676532?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgzMDQ0Njc2NTMyIiwic3RrIjoiODJ6SW43WlFrRThyQzVOX3prUzV4UE5wRkYtWXVnVW9DTnhHMzZ2c1pDNC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhEWTE2T2dBU2RRQUFBQSIsImV4cCI6MTYzMjMyMjE0OCwiaWF0IjoxNjMyMzE0OTQ4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.xS9JAx1uRsaeUm7rcOfRItpcN76VeRUU99rJmklFpzM', 2, '2021-09-22 14:30:00', 'Europe/Stockholm', 30),
(140, 3, 1, 4, NULL, NULL, 'closeup-of-hand-holding-mobile-phone-with-computer-p9u5xyc1632486780.jpg', NULL, 'Onboarding of Guides Test', 'This is a test session', 'https://us06web.zoom.us/s/87689002967?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg3Njg5MDAyOTY3Iiwic3RrIjoiYWdsS0VOTkdPZEhPMUJqOWtYRmptdVNqZ2NmNE80dFVDanFmNVZJUUpYQS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhGOHR4cmdBU2RRQUFBQSIsImV4cCI6MTYzMjQ5Mzk4MSwiaWF0IjoxNjMyNDg2NzgxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.uVouPpiiDWurbjkaaBZgOUypnb0jaA4xA0JC3rBk58w', 2, '2021-09-24 14:30:00', 'Europe/Stockholm', 30),
(139, 3, 1, 4, NULL, NULL, 'laptop-with-empty-screen-on-wooden-desktop-with-ph-vuuh7t6jpg1632448383.jpg', NULL, 'Breathwork for Innovation', 'Test', 'https://us06web.zoom.us/s/87698719045?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg3Njk4NzE5MDQ1Iiwic3RrIjoiS0J4S0tXZlE5enYwZzVoLWExNGZDcjE4THpRS1E3aF9yN0dQekR4b2FRTS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhGWUdLZFFBU2RRQUFBQSIsImV4cCI6MTYzMjQ1NTU4MywiaWF0IjoxNjMyNDQ4MzgzLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.g4H_-Lxj4uyDmptslkbEtEqbVQ6iGLfiG3SKCxtnAuM', 2, '2021-09-24 06:00:00', 'Europe/Stockholm', 30),
(141, 3, 1, 1, NULL, NULL, 'education-and-reading-concept-group-of-old-colorfu-t573dehjpg1632492446.jpg', NULL, 'Art Therapy', 'Test', 'https://us06web.zoom.us/s/82581443976?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgyNTgxNDQzOTc2Iiwic3RrIjoiMkZ3UU9VWDR6NjhlU2dHYmFqOUtEamxCRHhZTVQ0dDZhb0JYd1NTVnl0Zy5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhHQ0hxTlFBU2RRQUFBQSIsImV4cCI6MTYzMjQ5OTY0OCwiaWF0IjoxNjMyNDkyNDQ4LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.v72RZsoyXFARjcofGysHyaEF2f-1aVrjXJzq4LkCjKY', 2, '2021-09-24 14:30:00', 'Europe/Stockholm', 30),
(142, 3, 1, 1, NULL, NULL, 'talk-show-at-online-radio-station-9fmcxz41632492715.jpg', NULL, 'Test 09/25 01', 'Test', 'https://us06web.zoom.us/s/82960394890?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgyOTYwMzk0ODkwIiwic3RrIjoiNHktRmxTMXRBbmxxTmtEZ1JvdG5kRkxwWG9sbDVKVFRodjlaeWUwRXpWNC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhHQ1gtbGdBU2RRQUFBQSIsImV4cCI6MTYzMjQ5OTkxNSwiaWF0IjoxNjMyNDkyNzE1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.mJv6lbE8rnPeduQP018cGvajhdNEjbZr39im43txS8M', 1, '2021-09-24 16:30:00', 'Europe/Stockholm', 30),
(143, 3, 1, 1, NULL, NULL, 'online-photography-school-nt5kr6f1632495581.jpg', NULL, 'Test 09/24 Part 2', 'Test', 'https://us06web.zoom.us/s/81448439027?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgxNDQ4NDM5MDI3Iiwic3RrIjoibWtQRENQLVRmck1vQVpxbTUzaWxJUkF3U0pfeGtqZUJ0bk9NNUlaRF9mMC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhHRkc3dHdBU2RRQUFBQSIsImV4cCI6MTYzMjUwMjc4MiwiaWF0IjoxNjMyNDk1NTgyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.0JOMck7qgwNhAVK8BZb3I5J4um-OFo-EGOf3qasNyw0', 3, '2021-09-24 17:00:00', 'Europe/Stockholm', 30),
(144, 3, 1, 1, NULL, NULL, 'books-with-hardcovers-near-white-brick-wall-v3kqvsm1632496368.jpg', NULL, 'Test Session ', 'Test session', 'https://us06web.zoom.us/s/88074006385?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg4MDc0MDA2Mzg1Iiwic3RrIjoicHdQUkdHWVpUNDJUdnNNQTNob1pjWXdYT0pWN2h4RXNBbklkV1haS2YxRS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhHRjJfQndBU2RRQUFBQSIsImV4cCI6MTYzMjUwMzU2OSwiaWF0IjoxNjMyNDk2MzY5LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.7RLhZPYHCMcaOejDklDV8_tzH36PuJ9WtfqEqa5dGuU', 1, '2021-09-24 17:30:00', 'Europe/Stockholm', 30);
INSERT INTO `calls` (`callId`, `clientId`, `coachId`, `categoryId`, `allMembers`, `allCoaches`, `image`, `zoomId`, `title`, `description`, `url`, `tipoId`, `fecha`, `timezone`, `duration`) VALUES
(145, 3, 1, 1, NULL, NULL, 'online-shopping-k7wvx321632496432.jpg', NULL, 'Test Session 2', 'Test', 'https://us06web.zoom.us/s/89470868437?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg5NDcwODY4NDM3Iiwic3RrIjoiQ3daYU9wRHpucmlIVUtIMi0ySDl6OVB4RWdWdFRqUmpjRkQ1V0FHN2ZxMC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhHRjYyVGdBU2RRQUFBQSIsImV4cCI6MTYzMjUwMzYzMiwiaWF0IjoxNjMyNDk2NDMyLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.jezAnc1fX85LfSGIZP1tb-k0OqPDXLdDYjpv2Y71Y7Q', 3, '2021-09-24 18:00:00', 'Europe/Stockholm', 30),
(146, 3, 1, 4, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1632709704.jpg', NULL, 'Test Session 2', 'This is a test', 'https://us06web.zoom.us/s/89656050957?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg5NjU2MDUwOTU3Iiwic3RrIjoiR1VRQzF2VmFlUWJ5RlhNYnlXcVc1RkpfckFJdVRzZ3VQVVFxLU5ILTJoQS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhKUlQ4OGdBU2RRQUFBQSIsImV4cCI6MTYzMjcxNjkwNCwiaWF0IjoxNjMyNzA5NzA0LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.GRnpzg9fC1FP7lpS-K1lVxz-3kGAtDl1pbMiJPji8jc', 2, '2021-09-27 06:00:00', 'Europe/Stockholm', 30),
(147, 3, 1, 1, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1632848043.jpg', NULL, 'Test Session September 28', 'This is a test', 'https://us06web.zoom.us/s/84349578536?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0MzQ5NTc4NTM2Iiwic3RrIjoieGdtNG1fZjZRUHBsUm5sM0ZhLXJPWWVWMzluLTE0STJTQzdJblhNYXJFRS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhMVnl4Z0FBU2RRQUFBQSIsImV4cCI6MTYzMjg1NTgyMSwiaWF0IjoxNjMyODQ4NjIxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.HFYNoYSVvI-MDUUN2GKtelo9jwx0QokHVPXDLRDFKQo', 2, '2021-09-28 19:00:00', 'Europe/Stockholm', 30),
(148, 3, 1, 1, NULL, NULL, 'tasks-for-business-development-8v2bzghjpg1632859115.jpg', NULL, 'Test Session September 28', 'This is a test', 'https://us06web.zoom.us/s/83381784875?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6IjgzMzgxNzg0ODc1Iiwic3RrIjoiRUMwUGJJZHd4c2hQMGw2WWZrVFNPVWYtN18xT19XX21UOGhmZlh6ODMwWS5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhMZnpSWHdBU2RRQUFBQSIsImV4cCI6MTYzMjg2NjMxNSwiaWF0IjoxNjMyODU5MTE1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.MbQCLQsc4XUnBlwTD1Wt72xJmCzSybWyRwt8hxWrkLc', 3, '2021-09-28 22:00:00', 'Europe/Stockholm', 30),
(149, 3, 1, 1, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1632921983.jpg', NULL, 'Test Public 09/29', 'This is a test', 'https://us06web.zoom.us/s/84987027982?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0OTg3MDI3OTgyIiwic3RrIjoiQlRhOWhaRnBwbE1wZjNodkVQMDFMNjg2QjhKQ1ktcEo3Z0RKTlVROXpOMC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhNY0dSMGdBU2RRQUFBQSIsImV4cCI6MTYzMjkyOTU0MSwiaWF0IjoxNjMyOTIyMzQxLCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.YmTYj3kRrRt7JwVOdYcCmzAFvrPWAiSnFqf6EKPWJ4A', 1, '2021-09-29 15:30:00', 'Europe/Stockholm', 30),
(150, 3, 1, 1, NULL, NULL, 'schoolgirl-at-laptop-having-online-class-with-teac-xzy9ttp1632922135.jpg', NULL, 'Test Private 09/29', 'This is a test', 'https://us06web.zoom.us/s/84258386472?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg0MjU4Mzg2NDcyIiwic3RrIjoiTXc3cUxsZHZNU0pPemV2ZmhtVnpQcS1Hd0xrNHVIQkRQVFVaaXBTTEo4US5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhNY0hRVmdBU2RRQUFBQSIsImV4cCI6MTYzMjkyOTU1NywiaWF0IjoxNjMyOTIyMzU3LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.Y9wSZVaVaW5D33CumVbeAEJaIMrdiI0bAS8k-ZhrETc', 2, '2021-09-29 15:30:00', 'Europe/Stockholm', 30),
(151, 3, 1, 1, NULL, NULL, 'diverse-teens-hands-together-concept-pbw4jmj1633018833.jpg', NULL, 'Art Therapy', 'This is a test', 'https://us06web.zoom.us/s/85645939109?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImU4OXpDN0ZvU18yb3cyempYaEpuUUEiLCJpc3MiOiJ3ZWIiLCJzdHkiOjEwMCwid2NkIjoidXMwNiIsImNsdCI6MCwibW51bSI6Ijg1NjQ1OTM5MTA5Iiwic3RrIjoieVlnUl9kOTN2Um5UNkdpM0lYOXM2cnJKdnhkcVBLa0FRWThXbnNZdkpvNC5CZ1lnVEZSeFZsVnNkV1ZGZGs5emJ6aDNNRXh2TjNad2RtRjZRbVl5VGtNcmJ6TkFOakk0WlRRelpEVTJZak5oWVRBNU16WTBabVprTVdJNE5USXdaV1ZsT0RFNE1EZGxaVFpqWWpVeU9UWXhPVEF5TmpneVptTXhaamN4WWprNU9ETXpOQUFnT1dWQmJXZENRbTQyU1hFeGRHbFJXbGh1WWxoT1VtdHhVMWhCTVhsRE5rb0FCSFZ6TURZQUFBRjhONEh3LUFBU2RRQUFBQSIsImV4cCI6MTYzMzAyNjAzNSwiaWF0IjoxNjMzMDE4ODM1LCJhaWQiOiJWalo5anU1U1NfdWxaWVgxR2pQZzd3IiwiY2lkIjoiIn0.S0ERX_Q3AfBOPNjvERpialh5qGYRjiSuwtRwtvtm7WM', 2, '2021-09-30 18:00:00', 'Europe/Stockholm', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `callscircle`
--

CREATE TABLE `callscircle` (
  `callCircleId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfil`
--

CREATE TABLE `cambioperfil` (
  `cambioId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioperfilmiembro`
--

CREATE TABLE `cambioperfilmiembro` (
  `cambioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `descripcion` varchar(1000) NOT NULL,
  `imagen` varchar(200) NOT NULL COMMENT 'archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`categoryId`, `clientId`, `nombre`, `descripcion`, `imagen`) VALUES
(1, 3, 'Breathwork', 'Learn breathwork exercises that calm your mind, balance your nervous system and move you into a state of creativity and productivity', 'category-breathwork.jpg'),
(2, 3, 'Meditation', 'Practice meditation exercises that cultivate a state of equanimity and openness, bringing you in alignment with others and closer to your purpose', 'category-meditation.jpg'),
(3, 3, 'Psychotherapy', 'Consult with us and connect to your true authentic self through personal healing, focused and results oriented psychotherapy. ', 'category-psychotherapy.jpg'),
(4, 3, 'Connection', 'Discover the tools that help you relate, empathize and build trust in relationships so that share and co-create boldly and freely', 'category-connection.jpg'),
(5, 4, 'Purpose Approach', 'Children discover their individuality and explore what they want for themselves and the world through this series of classes and workshops.', 'challenge-mission-and-achievement-a-little-girl-is-v9dvn29.jpg'),
(6, 4, 'Passion based learning', 'We work with children to identify their interests and curate personalized exercises that lead them to seek learning out of passion. ', 'reading-is-her-big-passion-smp242b.jpg'),
(7, 4, 'Self-Efficacy', 'Self-efficacy classes develop the self-management skills that enable children to become resourceful, persistent and confident.', 'father-with-small-children-working-outdoors-in-gar-kkd5msl.jpg'),
(8, 4, 'Academics', 'Our academics classes explores art and culture and connects them with science, language, math and social studies taught at school.', 'lovely-little-girl-is-dancing-in-headphones-tqsuaje.jpg'),
(9, 7, 'Integral therapy', 'Gestalt, Integral and Transpersonal Psychotherapy, Naturopathic medicine, Therapeutic Massages, Horticultural therapy, Drug Addiction Treatment & Harm Reduction', 'cueva-venados-1-small.jpg'),
(11, 7, 'Specialized Workshops', 'Yoga, Meditation, Breathwork, Dreamwork, Family Constellation, Permaculture & Farming, Team Building and Art Therapy', 'yoga-terrace-space-2-small.jpg'),
(10, 7, 'Indigenous medicine', 'Traditional medicinal plants, Medicinal Herbs, Temazcal / Sweatlodge, Healing ceremonies, Medicine Wheel, Diets and Detox', 'entrada-fmm-003_temazcal.jpg'),
(12, 7, 'Consultations', 'Individual psychotherapy sessions for depression, anxiety, addiction, eating disorders, PTSD, existential crisis, difficult grief issues and life transitions', 'nierika-int-_img_5866.jpg'),
(20, 9, 'Business Development', 'Develop a business plan, find the right talent, build a marketing funnel, scale users, monetize your service, reach beyond digital and realize your vision!', 'tasks-for-business-development-8v2bzghjpg.jpg'),
(19, 9, 'Brand and Design', 'Build brand strategy and design language, connect them with how you think, speak, act and appear, and merge online with offline experiences ', 'hand-holding-notebook-with-drew-brand-logo-creativ-p8htw8w.jpg'),
(18, 9, 'Content Creation', 'Write compelling content, shoot your first video, speak confidently in front of the camera, create explainer videos and manage your audience on Zoom!', 'talk-show-at-online-radio-station-9fmcxz4.jpg'),
(17, 9, 'Platform Setup', 'Set up your platform, customize your content, administer your users, start LIVE programming and publish your website with your own domain name!', 'software-engineers-working-on-project-and-programm-xzp2tp7.jpg'),
(21, 8, 'Test', 'This a category test', 'video-final.gif');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlecategories`
--

CREATE TABLE `circlecategories` (
  `ccId` int(11) NOT NULL,
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circlecategories`
--

INSERT INTO `circlecategories` (`ccId`, `circleId`, `categoryId`) VALUES
(46, 27, 2),
(44, 28, 1),
(47, 29, 4),
(49, 30, 2),
(51, 31, 4),
(52, 32, 4),
(53, 33, 4),
(56, 34, 4),
(57, 35, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlecoachrequest`
--

CREATE TABLE `circlecoachrequest` (
  `requestCircleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circlerequest`
--

CREATE TABLE `circlerequest` (
  `requestCircleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circles`
--

CREATE TABLE `circles` (
  `circleId` int(11) NOT NULL COMMENT 'multiple=users,usercircle;multiple=coaches,coachcircle;multiple=categories,circlecategories;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `public` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circles`
--

INSERT INTO `circles` (`circleId`, `clientId`, `coachId`, `parentId`, `public`, `nombre`, `description`, `photo`, `video`) VALUES
(28, 3, 44, 0, 'S', 'Breathwork for Creativity Circle', 'This is a group dedicated to developing a breathwork practice that fosters innovation and creativity. ', 'group-of-people-making-yoga-exercises-at-studio-pywpcsx.jpg', NULL),
(30, 3, 6, 0, 'S', 'Meditation All-Inclusive', 'This circle is for you who want to keep meditation as an essential part of your life to use as a tool to come back to a relaxed body, clear mind and a loving relationship to life. \n\nIt is a place to get or give support in making the meditation practice happen, share and inspire each other and also to give feedback to the online meditations so I get a feel for what is happening in you.', 'papaioannou-kostas-tysecum5hja-unsplash1622024552.jpg', NULL),
(31, 3, 1, 0, 'S', 'Innerworks Guides Test Circle', 'This is a circle of all the guides at Innerworks', 'diverse-teens-hands-together-concept-pbw4jmj1632229269.jpg', NULL),
(32, 3, 55, 0, 'S', 'Fraendi', 'Frændi is a group of consulting professionals, thinkers, educators, doers, and optimists who are committed to the practice of unlocking potential—in people, teams, systems, and organizations.\n\nOur goal is to instill confidence in individuals, leaders, governments, and organizations and together help create a roadmap to a prosperous, planet-sustaining future.\n\nIn a world that faces unprecedented challenges from social justice to environmental degradation, Frændi is working with leaders who are moving their organizations forward with clarity, confidence, empathy, agility, and resilience.', 'fraendi_logo_ohne_rand-removebg1634693671.png', NULL),
(33, 3, 55, 0, 'S', 'Evolutesix', 'Evolutesix\'s Startup Factory is designed to build startups making the future work for all.\nComposed of our Startup University (developing people and teams; creating and investing in startups) and Accelerator; followed by our membership of our regenerative startup ecosystem.\n\n \n\nWe build WHOLE, connected economic models (not disconnected business models); ecosystems of inherently regenerative startups, teams, and people; all making tomorrow\'s world whole by solving our biggest challenges.', 'logo1634749532.png', NULL),
(34, 3, 55, 0, 'S', 'Eleutheria', 'We enable decision makers to safely embark on probably the most challenging yet powerful journey of their life.\n\nOur programs are designed for personal transformation by combining modern science and ancient wisdom. \n', 'eleutheria-background21634749977.png', NULL),
(35, 3, 55, 0, 'S', 'Innerworks Core', 'We’re a community of changemakers and coaches\nfrom around the world, facilitating customised\nprograms for individual thriving, team effectiveness and organisational resilience', 'innerworks1634750244.jpg', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientpages`
--

CREATE TABLE `clientpages` (
  `clientPageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pageId` int(11) NOT NULL COMMENT 'opciones=pages'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientpages`
--

INSERT INTO `clientpages` (`clientPageId`, `clientId`, `pageId`) VALUES
(104, 9, 5),
(99, 3, 2),
(9, 1, 1),
(10, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 2, 1),
(14, 2, 2),
(15, 2, 3),
(16, 2, 4),
(17, 4, 1),
(18, 4, 2),
(19, 4, 3),
(20, 4, 4),
(21, 7, 1),
(22, 7, 2),
(23, 7, 3),
(24, 7, 4),
(62, 8, 5),
(61, 8, 4),
(60, 8, 3),
(59, 8, 2),
(103, 9, 4),
(102, 9, 3),
(101, 9, 2),
(100, 9, 1),
(40, 10, 4),
(39, 10, 3),
(38, 10, 2),
(37, 10, 1),
(58, 8, 1),
(63, 11, 1),
(64, 11, 2),
(65, 11, 3),
(66, 11, 4),
(67, 12, 2),
(68, 12, 3),
(69, 12, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `clientId` int(11) NOT NULL COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;',
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `accessPassword` varchar(200) DEFAULT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `memberQuestionnaire` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `zoomPublic` varchar(200) DEFAULT NULL,
  `zoomSecret` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`clientId`, `platformId`, `nombre`, `accessPassword`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `pass`, `memberQuestionnaire`, `zoomPublic`, `zoomSecret`) VALUES
(1, 1, 'Shopify', NULL, 'logo-inner.png', 'logo-inner.png', NULL, NULL, 'archive', 'client@test.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(2, 2, 'EphataClient', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'clients', 'client@ephata.com', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(3, 1, 'Innerworks', NULL, 'innerworks_logo_5-innerworks-university.png', 'innerworks_logo_black-innerworks-university.png', NULL, NULL, 'innerworks', 'roman@romanf.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'N', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(4, 2, 'Ephata University', NULL, 'ephata.png', 'ephata.png', NULL, NULL, 'ephata', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(7, 3, 'Nierika', NULL, 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', NULL, NULL, 'nierika', 'universidad@nierika.com', 'dmlHaXNmUTN6bVZreEpHMURQQkxCYnhRUEJNTjEyRG51Z3NybEtPaC81Yz0=', 'S', NULL, NULL),
(8, 4, 'Test University', 'testpassword', 'logo-azulik-black.png', 'zenrise_logo_03-test-university.png', NULL, NULL, 'test', 'elopez@junkyard.mx', 'eDBDbEhRdVdoVDdJR0s3Nk5IODRqQT09', 'S', 'FjyVrQ7KTFyw1IR93GluPw', 'cwT3mT01sl4chWFaOVnjA4ZzzMpzik9gisZI'),
(9, 5, 'Shala', NULL, 'shala-new-logo-shala.png', 'shala-new-icon-shala.png', NULL, NULL, 'shala', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'N', NULL, NULL),
(10, 6, 'Eleutheria', NULL, 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', NULL, 'lexendmega-regular.ttf', 'eleutheria', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coacharticle`
--

CREATE TABLE `coacharticle` (
  `coachArticleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coacharticleserie`
--

CREATE TABLE `coacharticleserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coacharticleserie`
--

INSERT INTO `coacharticleserie` (`coachSerieId`, `coachId`, `serieId`) VALUES
(3, 44, 3),
(9, 52, 1),
(8, 44, 1),
(7, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachaudio`
--

CREATE TABLE `coachaudio` (
  `coachAudioId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachcall`
--

CREATE TABLE `coachcall` (
  `coachCallId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `callId` int(11) NOT NULL COMMENT 'opciones=calls'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachcall`
--

INSERT INTO `coachcall` (`coachCallId`, `coachId`, `callId`) VALUES
(6, 44, 86),
(5, 6, 85),
(4, 44, 81),
(8, 6, 83),
(9, 6, 91),
(10, 6, 94),
(12, 1, 106),
(155, 38, 110),
(16, 38, 119),
(19, 38, 120),
(109, 44, 134),
(108, 1, 134),
(27, 44, 128),
(30, 1, 129),
(117, 52, 135),
(110, 52, 134),
(116, 44, 135),
(115, 1, 135),
(121, 44, 136),
(120, 1, 136),
(122, 52, 136),
(134, 52, 137),
(133, 44, 137),
(132, 1, 137),
(135, 1, 138),
(136, 44, 138),
(137, 52, 138),
(138, 1, 139),
(139, 44, 139),
(140, 52, 139),
(141, 44, 140),
(142, 1, 146),
(143, 44, 146),
(144, 52, 146),
(149, 44, 147),
(148, 1, 147),
(161, 52, 150),
(160, 44, 150),
(159, 1, 150),
(162, 1, 151),
(163, 44, 151);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachcircle`
--

CREATE TABLE `coachcircle` (
  `coachCircleId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachcircle`
--

INSERT INTO `coachcircle` (`coachCircleId`, `coachId`, `circleId`) VALUES
(4, 6, 30),
(12, 52, 31),
(11, 44, 31),
(10, 6, 31),
(9, 1, 31),
(13, 56, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coaches`
--

CREATE TABLE `coaches` (
  `coachId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `phrase` varchar(1000) DEFAULT NULL,
  `biografia` varchar(1000) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `header` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `facebook` varchar(200) DEFAULT NULL COMMENT 'liga',
  `twitter` varchar(200) DEFAULT NULL COMMENT 'liga',
  `instagram` varchar(200) DEFAULT NULL COMMENT 'liga',
  `linkedin` varchar(200) DEFAULT NULL COMMENT 'liga',
  `web` varchar(200) DEFAULT NULL COMMENT 'liga',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `showHome` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `zoomAuth` varchar(200) DEFAULT NULL,
  `zoomToken` varchar(2000) DEFAULT NULL,
  `zoomRefresh` varchar(2000) DEFAULT NULL,
  `zoomPublic` varchar(200) DEFAULT NULL,
  `zoomPrivate` varchar(200) DEFAULT NULL,
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coaches`
--

INSERT INTO `coaches` (`coachId`, `statusId`, `nombre`, `apellido`, `phrase`, `biografia`, `foto`, `header`, `video`, `facebook`, `twitter`, `instagram`, `linkedin`, `web`, `email`, `pass`, `showHome`, `emailConfirmed`, `zoomAuth`, `zoomToken`, `zoomRefresh`, `zoomPublic`, `zoomPrivate`, `fecha`) VALUES
(1, 2, 'Nils', 'von Heijne', 'Holistic coach', 'Nils is a holistic coach, advisor, shaman and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker, energy worker and sound healer. More info can be found at nilsvonheijne.com.', 'dsc0864116202183561951631050233.jpeg', 'frank-mckenna-od9eozfsoh0-unsplash1630072029.jpg', 'MTA3MTY3NA==', 'https://www.facebook.com/nilsvonheijne', 'https://twitter.com/nilsvonheijne', 'https://www.instagram.com/nilsvonheijne/', 'https://www.linkedin.com/in/nilsvonheijne/', 'https://nilsvonheijne.com', 'nils@innerworks.io', 'TnlwYUZVV01lNEUxTjRCNDNZT3dHQT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-09-07'),
(2, 1, 'Roshi', 'Derakshan', NULL, 'Licensed Psychotherapist and dedicated to helping people connect to their true authentic self through personal healing, focused and results oriented psychotherapy. Licenced Life coach and work as a ”hybrid” where you get the best of a therapist and coach to break free from what’s holding you back to push towards where you want to be.', 'roshi-2png.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rome7f-deactivated-2021-04-22@gmail.com', 'VjJxelJkZC9VSUIwK0kyTHo4QUZyUT09', 'N', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-23'),
(5, 2, 'Jessika', 'Klingspor', NULL, 'Jessika is Swedish entrepreneur and business owner living in Barcelona since 2001. She is currently collaborating with the Barcelona City Council building new alliances between The Nordic\r\nCountries and Barcelona within the sector of innovation. She is co-creator within the Innerworks Community with aim to bridge the gap between the corporate world and the spiritual world. Planning to launch Innerworks Barcelona and World of Wisdom Barcelona 2021.', 'jessklingspor1.jpg', NULL, NULL, 'null', 'null', 'null', 'null', 'null', 'Jessika.klingspor@nordicsinbarcelona.com', 'WWY5STRKWUNnc0t1L1kvWjF0NG1QZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-22'),
(38, 2, 'Roman', 'Fernandez', NULL, 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'linkedin.jpg', 'tlm120720kg-112-small.jpg', 'MTE1Njg0NA==', 'https://www.facebook.com/romanspade/', NULL, NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'roman@romanf.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-20'),
(4, 2, 'Nils', 'von Heijne', NULL, 'Nils is a holistic coach, advisor and changemaker. Beyond being one of Innerworks co-founders, he also serves the Innerworks community as a coach, acupressure bodyworker and sound healer.', 'nilspng.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nils@wishful.se', 'UnZQaGdVYUdqdVBDTlYxS0JhUVhiUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-17'),
(6, 2, 'Mattis', 'Norrvidd', 'Meditation coach', 'Mattis is a heartdriven environmental engineer who supports individuals, groups and companies to thrive through courses in self leadership, inspirational talks, meditations and coaching. He uses curiosity and clearity of mind to develop inner and outer connection.', 'mattis.png', 'giuseppe-peletti-mgg1xks9tjo-unsplash1630575886.jpg', NULL, 'https://www.facebook.com/mattis.hansson.1', NULL, NULL, NULL, 'https://insighttimer.com/mattis.norrvidd', 'mattis@comecloser.nu', 'cG5oOTNKb1ZZOGlHbnN2d3ZQUXJSQT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-09-02'),
(7, 2, 'Therese', 'Lyander', NULL, 'Therese is a transformational life coach, space holder and emotional guide. In her toolbox you will find\ndetox, fasting, breathwork, trauma therapy, meditation, creative feminine leadership and inspiration', 'teresa.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'therese@innerworks.io', 'elV5VzRzYnYreGVmQXo0OGJROFlIdz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-18'),
(8, 2, 'Johan', 'Reunanen', NULL, 'Johan is a coach, business advisor and changemaker. Besides being a partner at Scandinavian managament consultancy, Cordial, focusing on sustainable transformation, Johan shares his message of sobriety and loving leadership through talks, podcasts and writing', 'johan-r-1.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'johan@innerworks.io', 'WGJ6aiswWW5IMmhSR1o0Rzh2aTFCUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-18'),
(9, 2, 'Faviana', 'Vangelius', NULL, 'Within the InnerWorks community Faviana does Sound Healing and Reiki. Beyond being one of InnerWorks co-founders, she also does Community Building and Explorer in Chief (Experience Manager).', 'faviana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'faviana@innerworks.io', 'WHc4UGNrckIyZTFrbWlJMFdsdFlTUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-23'),
(10, 2, 'Daniel', 'Mueller-Gonzalez', NULL, 'Daniel is a Wim Hof Method instructor, breath guide and human evolution coach based in Stockholm.', 'daniel.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hello@danielmueller.se', 'QUtQTU56RGltQUZ4WWhnS0FUbExzUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-22'),
(11, 2, 'Alexander', 'Holmberg', NULL, 'Alexander is a personal coach in Stockholm.', 'holmberg.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alexander@innerworks.io', 'S296aXBoNk9uNVR6NjlKZTVMWm1iQT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-18'),
(12, 2, 'Fredrik', 'Edlund', NULL, 'Fredrik Edlund is a yoga and meditation teacher based in Stockholm.', 'edlund1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fredrik@innerworks.io', 'Z2sxWmRJVDFjNVNvK3d1QjJkbm4rZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-18'),
(13, 2, 'Kim', 'Gajraj', NULL, 'Kim is a holistic voice therapist based in Stockholm.', 'gajraj.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kim@kimgajraj.com', 'RzJySUZEUWFod3M2UCtuc1ZoZkxMQT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-22'),
(18, 2, 'Valeria', 'Payton', NULL, 'Valeria Payton is the founder of Tulum International School and the co-founder of Ephata.', 'valeria_profile_square2.jpg', NULL, 'MTA2OTkzMQ==', NULL, NULL, NULL, NULL, NULL, 'valeria@ephata.me', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-01-29'),
(29, 2, 'Eduardo Raymundo', 'Villagran', 'Personal Phrase Hello', NULL, NULL, NULL, NULL, 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'eduardo.raymundo8787@gmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-30'),
(32, 2, 'Anja', 'Loizaga-Velder, PhD', NULL, 'Anja Katharina Loizaga-Velder is a German-Mexican clinical psychologist and psychotherapist, who has been investigating the therapeutic potential of the ritual use of psychedelic plants for over 25 years, in collaboration with indigenous healers. She earned a PhD degree in Medical Psychology from Heidelberg University in Germany with a doctoral dissertation on: The therapeutic uses of ayahuasca in addiction treatment. She is a founding member and director of research and psychotherapy of the Nierika Institute for Intercultural Medicine in Mexico and is an adjunct professor and researcher at the National Autonomous University of Mexico, where she researches the therapeutic potential of psychedelics in intercultural therapeutic settings. Additionally, she works as a psychotherapist with humanistic and transpersonal orientation in private practice.', 'anja_profilepng.png', 'anja_cover2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'nierika@gmx.net', 'c0JJbHcyaHllazMwdFNsQmxBVXdCdz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-04'),
(33, 2, 'Armando', 'Loizaga', NULL, 'Armando Loizaga is the President of Nierika AC, a non-profit NGO based in Mexico. He has dedicated over 20 years to the study of addictions and sacred plants. The Peyote Conservation Project works hand in hand with the Wixarika and lays out a strategy to reconsider peyote as a cultural and spiritual patrimony of indigenous peoples that calls for a sustainable development approach to sacred plants and tending to the natural environments where they grow. His work currently centers on advancing clinical research protocols that employ sacred plants and participating closely in traditional uses and psychedelic drug policy discussions with different levels of Mexican government and international agencies.', 'armando-loizaga_profile.jpg', 'armando_coverpng.png', NULL, NULL, NULL, NULL, NULL, NULL, 'armando@centronierika.net', 'TDBXTGNvYWRxNld4TFNrYjVDL2h4Zz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-04'),
(34, 2, 'Hannah', 'Rapp', NULL, 'My name is Hannah Rapp. I am a Licensed Mental Health Counselor (LMHC) and Creative Healing Arts Therapist with a passion for utilizing creativity and nature to embody our wildest authentic expressions and healing. My path and life\'s work emerged from a deep exploration of my emotional body, wild adventures, free-form movement and expansive transformation. It is my greatest joy and calling to be witness to the human experience.\n \nAs a therapist I help individuals to remember their purpose, connect to and trust their intuition, listen to the wisdom of their body, identify and understand their feelings, and live authentically. Through creativity, ceremony, movement and mindfulness I guide individuals to express themselves fully, to create a loving relationship to themselves and the Earth, and to nurture their inner child.   ', 'hannah_profile2.jpg', 'hannah_cover.webp', NULL, 'https://www.facebook.com/hannah.rapp.19', NULL, NULL, NULL, 'https://www.playfulpassages.com/', 'hannah@centronierika.net', 'a1FZWWhiZ05jVEczb2xEK05TNlJEZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-04'),
(35, 2, 'Valeria', 'Payton', NULL, 'Valeria is the founder of Ephata and a passionate educator of several years ', 'valeria_profile_square2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'valeriapayton@gmail.com', 'NWJ4YU9meXJibC8yekV2YjgwQ2lhZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-13'),
(36, 2, 'Sanja Guide', 'Exanimo', NULL, 'jksdjkfsdjk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 's@exanimo.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-17'),
(37, 2, 'Roshi', 'Derakshan', NULL, 'I am a licenced Psychotherapist and Life Coach, with 10+ years of experience in treating disorders such as depression, burnout, anxiety, life crises and relational difficulties. \n\n \n\nI\'m not your \"typical therapist\". I\'m very honest in my assessments and goal oriented to get my clients to where they want to be. Unfortunately there are no shortcuts when doing self- work. Personal develoment and growth is hard work and there are no “cutting corners”, which is why I work online, with clients that are genuinely motivated and want to put in the work to transform their lives.  ', 'roshipng.png', 'roshi_coverpng.png', NULL, 'https://www.facebook.com/therapywithroshi', NULL, '@therapywithroshi', NULL, 'https://www.roshiderakshan.com/', 'roshi.derakhshan@gmail.com', 'MDR6aVV0SSswOGdXOEFLK2hJQ0V5dz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-23'),
(39, 2, 'Amber', 'Coffman', NULL, 'Amber is a growth strategist and the CEO of Orogamis, a team of strategists, marketers, engineers, and designers working together toward goals of growth. Orogamis fosters and embracse growth in all of its forms: professional, personal, and on behalf of their partners. Growth is the central theme of Orogamis, the mission, commitments, and company culture.', 'amber2.jpg', 'amber_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/amber-lee-coffman-81b43725/', 'amber@shala.us', 'YVN1WjhoYllpNGY4cEpnWkw4eGpCZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-20'),
(40, 2, 'Ivan', 'Martin Maseda', NULL, 'Ivan is the Chief Technology Officer at Binfluencer and Professor in Machine Learning at IE Business School in Madrid, Spain. The use of technology to reduce human intervention in repetitive tasks should lead to an environment where brains could dedicate their time to more fulfilling endeavors. His current goal is to become an excellent bridge between business needs and the technical information challenges. Translating business needs into technical requirements is only half of the work, he will make sure the technical results obtained are completely understood and communicated to all team members and shareholders. ', 'ivan2.jpg', 'ivan_cover2.jpg', NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/ivanmartinmaseda/', 'ivan@shala.us', 'UnRmYUJsZVJOZis1UDBZMHFLSHl3dz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-20'),
(41, 2, 'Vincent', 'Guntrum', NULL, 'I am a content and video marketing coach for trainers, coaches and healers. In the last few years I have struggled - mostly impatiently - off the beaten path through the jungle of self-discovery. Just to learn that unbelievable paths suddenly open up when I manage to relax and connect with my deeper resource. Life has something special in store for everyone. I have the honor to guide you to your resource, to support you to take the decisive steps and to show you to the world.', 'vincent2png.png', 'vincent2_coverpng.png', NULL, NULL, NULL, NULL, NULL, 'https://www.wild-games.net/', 'vincent@shala.us', 'YzRIeEEvM3JYbm9BWVdSQndhcG80UT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-20'),
(42, 2, 'Benjamin', 'Cuenod', NULL, 'All-rounder creative at heart, I offer passionate creative thinking, strategic planning, and analytical problem-solving. I believe in the brand story as a seed to systemic change within corporations, setting brands towards a sustainable and equitable framework, establishing resilience, and provoking evolution.\n\nServices:\nStrategy\nBranding\nDesign\nCreative Technology ', '02-benjamin-cuénod.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/in/benjamin-cu%C3%A9nod-b47b401/', 'ben@shala.us', 'OUpidDUwQ3dyZkZPZWRSWnhualFxdz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-22'),
(43, 2, 'Dmitrij', 'Achelrod', NULL, ' ', 'dimi.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dimi@shala.us', 'bXh6Z0VRNHRLekhLVFRmU3RjY0x1dz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-03-24'),
(44, 2, 'Roman', 'Fernandez', 'Innovation Coach', 'Roman has more than 20 years of experience in various roles, all contributing to his expertise in business strategy, digital innovation and venture building. To further support industries in mastering the digital transformation, Roman worked at Spielfeld in Berlin, a digital innovation hub created by Roland Berger and VISA. He also  worked in product development at Ford Motor Company and Robert Bosch in the U.S., Germany, Austria, Brazil, China, Turkey and India. Roman also volunteers his time for non-profit and diversity projects in education in Detroit, U.S. and Cape Town, South Africa.', 'profile2.jpg', 'tlm120720kg-112.jpg', NULL, 'https://www.facebook.com/romanspade', NULL, NULL, 'https://www.linkedin.com/in/roman-fernandez/', 'www.romanf.com', 'roman@alumni.ie.edu', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-08-26'),
(45, 2, 'Levin', 'Dihard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'levin@dihard.com', 'cmRtM2I2aXFSeHRnbW1IN2pHMUd6QT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-07'),
(46, 2, 'Ryan', 'Monahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'roman@eleutheria.world', 'RTQ1SFVEWm1HVGFRNm1UMW1IRTRRZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-13'),
(47, 2, 'Shalaland', 'Dhalaland', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shalaland@shalaland.com', 'OXE1R1JXQ2wzVlhydFQ0Z3JlTndYUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-13'),
(48, 2, 'New', 'Guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'romedetroit@outlook.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-14'),
(49, 2, 'Gabo', 'Test guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gabofc+testguide@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', 'N', NULL, NULL, NULL, NULL, NULL, '2021-04-14'),
(50, 2, 'Ismelda', 'Lopez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ismelda@lopez.net', 'c1NqVVpFMEdnL0lQalhVanhoSUl1dz09', 'S', 'N', NULL, NULL, NULL, NULL, NULL, '2021-04-15'),
(54, 2, 'Hanna', 'Kuznetsova', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hanna.kuznetsova@gmx.de', 'aGZSdVBzWmJwMGIwT2wzN3dBUXVWUT09', 'N', 'S', NULL, NULL, NULL, NULL, NULL, '2021-08-29'),
(51, 2, 'Sanna', 'Norrvidd', NULL, 'Sanna teach how to shift the focus from an exhausting fight with the body to a loving an empathic teamwork. Her methodology is transformational coaching that includes inner child work, meditation, breath work and somatic tools that creates more self love and safety from within.\r\n\r\nShe is working as coach for both individuals and groups to support them in developing body awareness and the ability to stay and be with the bodily sensations in this moment, regardless how challenging they may be. This work is a foundation in getting your sense of power and aliveness back and stay clear and resilient in the face of challenging emotions like fear and anger that often subconscious control our life and keeps us from getting what we truly desire.', '1d5d4c50-c910-4b00-9385-9c27e5ee6cae.jpeg', '62f8ba19-6d3f-4875-bc88-65d7d0b1b7cc.jpeg', NULL, 'https://m.facebook.com/sannanorrvidd/', NULL, '@sannanorrvidd', NULL, 'www.sannanorrvidd.com', 'sanna@sannanorrvidd.com', 'YTlsQmhkN01oV1Z0RnpFd2RCOCtrdz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-23'),
(52, 2, 'Amit', 'Paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'amitpaul.com', 'hello@amitpaul.com', 'ekNMbXRNQk5RbHBvZG9ZMldsT1NvdE1kVVRBWllIenUxWElwYnI4bUFtbz0=', 'N', 'S', NULL, NULL, NULL, NULL, NULL, '2021-08-29'),
(53, 2, 'Christian ', 'Haag ', 'Don\'t forget to breath', NULL, '24252b5d-4d4d-4863-816c-d43a8634f510.jpeg', NULL, NULL, 'https://www.facebook.com/crillehaag', NULL, 'Instagram.com/crillehaag', NULL, 'Www.cowbellproductions.se', 'crillehaag@gmail.com', 'UTVqcms0RDNoRkE5TVlWZURXSHJEUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-04-27'),
(55, 2, 'Team', 'Fraendi', 'Kinship | Companionship | Partnership', 'Fraendi is an international network of consultants and leading practitioners in Leadership Development, Consulting, Coaching and Organisational Development. \n\nWe bring together a wealth of expertise and experience from a wide range of organizational and societal perspectives into one single vision:\nWE PARTNER WITH LEADERS ACROSS ORGANIZATIONS TO DEVELOP CAPACITIES FOR COMPLEXITY AND TRANSFORMATION\n\nWe are living in disruptive times. We face multiple crisis and wicked problems. Especially people in leadership positions need to be able to meet these challenges. Fraendi is specialising in methods and approaches that senior leaders need for learning and transformation individually for their teams across the organisation as part of multi-stakeholder or whole sector learning', 'fraendi_logo_ohne_rand-removebg1630507531.png', 'fraendi-coverpng1630507531.png', NULL, NULL, NULL, NULL, NULL, 'https://fraendi.org/', 'rome7f@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-09-03'),
(56, 2, 'Fraendi', 'Team', NULL, 'Fraendi is an international network of consultants and leading practitioners in Leadership Development, Consulting, Coaching and Organisational Development. We bring together a wealth of expertise and experience from a wide range of organizational and societal perspectives into one single vision: \nWE PARTNER WITH LEADERS ACROSS ORGANIZATIONS TO DEVELOP CAPACITIES FOR COMPLEXITY AND TRANSFORMATION\n\nWe are living in disruptive times. We face multiple crisis and wicked problems. Especially people in leadership positions need to be able to meet these challenges. Fraendi is specialising in methods and approaches that senior leaders need for learning and transformation individually for their teams across the organisation as part of multi-stakeholder or whole sector learning', 'fraendi_logo_ohne_rand-removebg1630506796.png', 'fraendi-coverpng1630506796.png', NULL, NULL, NULL, NULL, NULL, 'https://fraendi.org/', 'fraendi@innerworks.io', 'OXp3Nld3SGhOYmpyd2VYdWNQakl0UT09', 'S', 'N', NULL, NULL, NULL, NULL, NULL, '2021-09-01'),
(57, 2, 'Peter', 'Wallberg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pwallberg@gmail.com', 'YVZycnVvRFN6QkJJcXdJVmVuak1TZz09', 'S', 'S', NULL, NULL, NULL, NULL, NULL, '2021-09-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachesclients`
--

CREATE TABLE `coachesclients` (
  `coachClientId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachesclients`
--

INSERT INTO `coachesclients` (`coachClientId`, `coachId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(15, 2, 3, 'R', NULL),
(30, 3, 3, 'N', NULL),
(33, 5, 3, 'S', NULL),
(34, 6, 3, 'N', NULL),
(35, 7, 3, 'R', 'Not in onboarding meeting'),
(36, 8, 3, 'S', 'Not in onboarding meeting'),
(38, 10, 3, 'S', NULL),
(39, 11, 3, 'R', 'Not in meeting'),
(40, 12, 3, 'R', 'Not in onboarding meeting'),
(41, 13, 3, 'S', NULL),
(42, 14, 3, 'N', NULL),
(43, 15, 3, 'N', NULL),
(46, 16, 3, 'N', NULL),
(48, 17, 3, 'N', NULL),
(50, 18, 4, 'N', NULL),
(52, 19, 4, 'N', NULL),
(58, 9, 3, 'S', NULL),
(59, 20, 3, 'R', 'You are a bad teacher'),
(60, 21, 3, 'S', NULL),
(61, 22, 1, 'S', NULL),
(62, 23, 1, 'S', NULL),
(63, 24, 1, 'N', NULL),
(64, 25, 1, 'R', 'porue si porque quise y porque puedo'),
(65, 26, 1, 'N', NULL),
(66, 27, 1, 'R', 'asfasf'),
(67, 28, 1, 'S', 'test reject'),
(68, 29, 1, 'N', 'porque puedo'),
(69, 3, 4, 'N', NULL),
(70, 30, 3, 'N', 'Because we dont like you'),
(71, 31, 3, 'N', 'Your are not a true coach'),
(72, 1, 1, 'S', NULL),
(73, 32, 7, 'N', NULL),
(74, 33, 7, 'N', NULL),
(75, 34, 7, 'N', NULL),
(76, 35, 2, 'N', NULL),
(77, 18, 2, 'S', NULL),
(112, 48, 8, 'N', NULL),
(114, 50, 3, 'S', NULL),
(111, 47, 8, 'S', NULL),
(108, 44, 3, 'S', NULL),
(107, 43, 10, 'S', NULL),
(106, 42, 9, 'N', NULL),
(105, 29, 8, 'N', NULL),
(104, 41, 9, 'N', NULL),
(103, 40, 9, 'N', NULL),
(102, 39, 9, 'N', NULL),
(101, 38, 9, 'N', NULL),
(100, 37, 3, 'S', NULL),
(99, 37, 1, 'S', NULL),
(113, 49, 8, 'S', NULL),
(115, 51, 3, 'S', NULL),
(116, 52, 3, 'S', NULL),
(117, 53, 3, 'S', NULL),
(118, 54, 3, 'S', NULL),
(119, 44, 9, 'S', NULL),
(120, 55, 3, 'N', NULL),
(121, 56, 3, 'S', NULL),
(122, 57, 3, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachserie`
--

CREATE TABLE `coachserie` (
  `coachSerieId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachserie`
--

INSERT INTO `coachserie` (`coachSerieId`, `coachId`, `serieId`) VALUES
(59, 44, 31),
(4, 52, 32),
(7, 44, 14),
(58, 6, 31),
(69, 44, 29),
(68, 6, 29),
(67, 5, 29),
(66, 1, 29),
(64, 44, 30),
(63, 6, 30),
(62, 5, 30),
(61, 1, 30),
(57, 5, 31),
(56, 1, 31),
(51, 55, 33),
(50, 44, 33),
(60, 52, 31),
(65, 52, 30),
(70, 52, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coachvideo`
--

CREATE TABLE `coachvideo` (
  `coachVideoId` int(11) NOT NULL,
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coachvideo`
--

INSERT INTO `coachvideo` (`coachVideoId`, `coachId`, `videoId`) VALUES
(322, 52, 57),
(321, 44, 57),
(320, 9, 57),
(319, 6, 57),
(318, 5, 57),
(345, 52, 54),
(344, 44, 54),
(343, 9, 54),
(342, 6, 54),
(341, 5, 54),
(206, 55, 56),
(205, 44, 56),
(175, 52, 55),
(174, 44, 55),
(173, 9, 55),
(172, 6, 55),
(171, 5, 55),
(208, 55, 58),
(207, 44, 58),
(328, 52, 59),
(327, 44, 59),
(326, 9, 59),
(325, 6, 59),
(324, 5, 59),
(187, 52, 61),
(186, 44, 61),
(185, 9, 61),
(184, 6, 61),
(183, 5, 61),
(244, 44, 20),
(245, 44, 21),
(246, 44, 22),
(247, 44, 23),
(248, 44, 24),
(249, 44, 25),
(250, 44, 26),
(251, 44, 27),
(252, 44, 28),
(253, 44, 29),
(198, 44, 30),
(316, 52, 52),
(315, 44, 52),
(314, 9, 52),
(313, 6, 52),
(312, 5, 52),
(317, 1, 57),
(340, 1, 54),
(170, 1, 55),
(323, 1, 59),
(182, 1, 61),
(351, 52, 63),
(350, 44, 63),
(349, 9, 63),
(348, 6, 63),
(347, 5, 63),
(346, 1, 63),
(371, 52, 60),
(370, 44, 60),
(369, 9, 60),
(368, 6, 60),
(367, 5, 60),
(366, 1, 60),
(377, 52, 62),
(376, 44, 62),
(375, 9, 62),
(374, 6, 62),
(373, 5, 62),
(372, 1, 62),
(383, 52, 64),
(382, 44, 64),
(381, 9, 64),
(380, 6, 64),
(379, 5, 64),
(378, 1, 64),
(311, 1, 52),
(215, 44, 31),
(310, 52, 65),
(309, 44, 65),
(308, 9, 65),
(307, 5, 65),
(306, 1, 65),
(333, 52, 66),
(357, 52, 67),
(339, 52, 68),
(389, 52, 69),
(356, 44, 67),
(355, 9, 67),
(354, 6, 67),
(353, 5, 67),
(352, 1, 67),
(388, 44, 69),
(387, 9, 69),
(386, 6, 69),
(385, 5, 69),
(384, 1, 69),
(272, 1, 70),
(273, 5, 70),
(274, 9, 70),
(275, 44, 70),
(276, 52, 70),
(332, 44, 66),
(331, 6, 66),
(330, 5, 66),
(329, 1, 66),
(282, 1, 71),
(283, 5, 71),
(284, 6, 71),
(285, 9, 71),
(286, 44, 71),
(287, 52, 71),
(395, 52, 72),
(394, 44, 72),
(393, 9, 72),
(392, 6, 72),
(391, 5, 72),
(390, 1, 72),
(338, 44, 68),
(337, 9, 68),
(336, 6, 68),
(335, 5, 68),
(334, 1, 68),
(359, 1, 73),
(360, 5, 73),
(361, 6, 73),
(362, 9, 73),
(363, 44, 73),
(364, 52, 73),
(402, 1, 74),
(396, 1, 75),
(397, 5, 75),
(398, 6, 75),
(399, 9, 75),
(400, 44, 75),
(401, 52, 75),
(403, 5, 74),
(404, 6, 74),
(405, 9, 74),
(406, 44, 74),
(407, 52, 74),
(408, 1, 76),
(409, 5, 76),
(410, 6, 76),
(411, 9, 76),
(412, 44, 76),
(413, 52, 76);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorcatalog`
--

CREATE TABLE `colorcatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorcatalog`
--

INSERT INTO `colorcatalog` (`catalogId`, `nombre`) VALUES
(1, 'Main Color'),
(2, 'Secondary Color'),
(3, 'Text Color'),
(4, 'Text Hover Color');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `colorId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorsclient`
--

CREATE TABLE `colorsclient` (
  `colorClientId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=colorcatalog',
  `color` varchar(10) NOT NULL COMMENT 'color'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colorsclient`
--

INSERT INTO `colorsclient` (`colorClientId`, `clientId`, `catalogId`, `color`) VALUES
(1, 3, 1, '#e3913a'),
(2, 3, 2, '#815e39'),
(3, 3, 3, '#918a83'),
(4, 3, 4, '#e8ddd2'),
(5, 8, 1, '#fb2449');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

CREATE TABLE `diccionario` (
  `iddiccionario` int(11) NOT NULL,
  `campo` varchar(200) NOT NULL,
  `etiqueta` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diccionario`
--

INSERT INTO `diccionario` (`iddiccionario`, `campo`, `etiqueta`) VALUES
(8, 'tipousuario', 'User\'s Type'),
(9, 'pass', 'Password'),
(14, 'statuspago', 'Payment Status'),
(28, 'usuario', 'User'),
(27, 'tipoadmin', 'Admin Type'),
(23, 'category', 'Categories'),
(26, 'nombre', 'Name'),
(29, 'accessadmin', 'Admin Access'),
(30, 'logotipo', 'Logo'),
(31, 'icono', 'Icon'),
(32, 'biografia', 'Biography'),
(33, 'foto', 'Photo'),
(34, 'telefono', 'Phone'),
(35, 'colorcatalog', 'Color\'s Catalog'),
(36, 'servicescatalog', 'Service\'s Catalog'),
(37, 'colorsclient', 'Clients Colors'),
(38, 'descripcion', 'Description'),
(39, 'imagen', 'Image'),
(40, 'titulo', 'Title'),
(41, 'apellido', 'Last Name'),
(42, 'fecha', 'Date'),
(43, 'users', 'Members'),
(44, 'bannerhome', 'Home Banners'),
(45, 'slidehome', 'Home Slider'),
(46, 'bannersomos', 'About Content'),
(47, 'enviar', 'Send'),
(48, 'manda', 'Send'),
(49, 'tipollamada', 'Session Access'),
(50, 'postcategory', 'Category'),
(51, 'posicion', 'Position'),
(52, 'imagenMovil', 'Mobile image'),
(53, 'boton', 'Button'),
(54, 'botonMovil', 'Button Mobile'),
(55, 'botonMobile', 'Button Mobile'),
(56, 'statusaccount', 'Status Account'),
(57, 'coach', 'Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email`
--

CREATE TABLE `email` (
  `emailId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `typeId` int(11) NOT NULL COMMENT 'opciones=typemail',
  `subject` varchar(200) NOT NULL,
  `generalText` varchar(20000) NOT NULL COMMENT 'html'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `excluded`
--

CREATE TABLE `excluded` (
  `excludedId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE `gallery` (
  `imageId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `title` varchar(200) NOT NULL,
  `altText` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL COMMENT 'liga',
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`imageId`, `clientId`, `image`, `title`, `altText`, `link`, `position`) VALUES
(1, 8, '42-17452823.jpg', 'Image 1', 'Image 1', NULL, 2),
(2, 8, '42-17454853.jpg', 'Image 2', 'Image 2', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generals`
--

CREATE TABLE `generals` (
  `generalId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `hideCoachContent` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `homeMenuText` varchar(200) NOT NULL,
  `exploreMenuText` varchar(200) NOT NULL,
  `coachesMenuText` varchar(200) NOT NULL,
  `aboutMenuText` varchar(200) NOT NULL,
  `coachText` varchar(200) NOT NULL,
  `memberText` varchar(200) NOT NULL,
  `liveMenuText` varchar(200) NOT NULL,
  `liveSessionText` varchar(200) NOT NULL,
  `processingText` varchar(200) NOT NULL,
  `signUpText` varchar(200) NOT NULL,
  `headerTitle` varchar(200) NOT NULL,
  `subtitleHeader` varchar(500) NOT NULL,
  `headerButtonShow` char(1) NOT NULL DEFAULT 'S' COMMENT 'activar',
  `headerButtonFontSize` varchar(10) DEFAULT NULL COMMENT 'fontSize',
  `headerButtonText` varchar(200) DEFAULT NULL,
  `headerButtonUrl` varchar(200) DEFAULT NULL COMMENT 'liga',
  `headerButtonTextColor` varchar(10) NOT NULL DEFAULT '#000000' COMMENT 'color',
  `headerButtonBackgroundColor` varchar(10) NOT NULL DEFAULT '#ffffff' COMMENT 'color',
  `headerButtonRounded` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `videoHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `videoMobileHeader` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `titleCategories` varchar(200) NOT NULL,
  `subtitleCategories` varchar(500) NOT NULL,
  `titleCoaches` varchar(200) NOT NULL,
  `subtitleCoaches` varchar(500) NOT NULL,
  `titleClasses` varchar(200) NOT NULL,
  `subtitleClasses` varchar(500) NOT NULL,
  `trialDaysText` varchar(200) NOT NULL,
  `trialDescriptionText` varchar(500) NOT NULL,
  `blogText` varchar(200) NOT NULL DEFAULT 'Blog',
  `signUpCoachText` varchar(200) NOT NULL DEFAULT 'Become a Guide'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `generals`
--

INSERT INTO `generals` (`generalId`, `clientId`, `hideCoachContent`, `homeMenuText`, `exploreMenuText`, `coachesMenuText`, `aboutMenuText`, `coachText`, `memberText`, `liveMenuText`, `liveSessionText`, `processingText`, `signUpText`, `headerTitle`, `subtitleHeader`, `headerButtonShow`, `headerButtonFontSize`, `headerButtonText`, `headerButtonUrl`, `headerButtonTextColor`, `headerButtonBackgroundColor`, `headerButtonRounded`, `videoHeader`, `videoMobileHeader`, `titleCategories`, `subtitleCategories`, `titleCoaches`, `subtitleCoaches`, `titleClasses`, `subtitleClasses`, `trialDaysText`, `trialDescriptionText`, `blogText`, `signUpCoachText`) VALUES
(1, 3, 'N', 'Home', 'Journeys', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, '/dashboard/login', '#000000', '#ffffff', 'N', 'gradientwithtext_4096x1728.mp4', 'mobile_text_new.mp4', 'UPGRADE YOUR HUMAN SOFTWARE', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'START WITH FREE LIMITED ACCESS', 'Get started with limited access to our breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(2, 4, 'N', 'Home', 'Classes', 'Teachers', 'About', 'Teacher', 'Student', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Faith. Wisdom. Goodness', 'Prepare our children for the future with passion based learning', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'header-ephata.mp4', NULL, 'FALL IN LOVE WITH LEARNING', 'Find the live classes, educational videos and exercises that nurture children beyond the curriculums of regular school', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'CLASSES', 'FIND A CLASS TO GET STARTED', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited purpose approach, passion based learning, self efficacy, academics, environmental awareness, social responsibility and spiritual balance classes', 'Blog', 'Become a Guide'),
(3, 7, 'N', 'Home', 'Programs', 'Facilitators', 'Location', 'Facilitators', 'Guests', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'HEAL - CONNECT - GROW', 'Holistic eco-center for retreats and programs in personal growth, spirituality, healing arts and therapeutic sciences', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', '', NULL, 'Our programs', 'Nierika offers therapeutic programs that integrate elements of traditional indigenous medicine and modern psychotherapy facilitated by multidisciplinary professionals\n', 'Meet our facilitators', 'Heal, connect and grow through programs facilitated by our world class therapists, instructors and mentors both in-person at our eco-center and online.', 'PROGRAMS', 'FIND A PROGRAM THAT WORKS FOR YOU', 'Sign-up for our programs', 'Tell us about yourself and what brings you to Nierika. We respond to everyone with an open heart.', 'Blog', 'Become a Guide'),
(4, 8, 'N', 'Home', 'Retreats', 'Guides', 'About', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'FIND PROFOUND', 'Exponential transformation by combining mind, body and spirit', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', NULL, 'WORK ON YOUR INNER self', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.\n', 'MEET OUR GUIDES', 'Commit to heal, connect with others to align and challenge yourselves to act through classes offered by world class coaches both online and in-person.', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime', 'Blog', 'Become a Guide'),
(5, 9, 'N', 'Home', 'Tutorials', 'Experts', 'About', 'Expert', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', ' ', ' ', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', '', '', 'LEARN OUR SECRETS', 'Everything we applied to build and grow Shala is here for you to use – Learn how to create strong content, prepare for camera, build your marketing strategy, find talent, design your brand, scale users and a lot more!', 'MEET OUR EXPERTS', 'Everyone who advised or contributed to Shala are now part of our expert team on our platform. They are here to help you build your platform, create winning content, scale your users and make your vision a reality!', 'TUTORIALS', 'FIND A TUTORIAL TO HELP REALIZE THE FULL POTENTIAL OF YOUR PLATFORM', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited access to our courses and building out your test platform. Cancel anytime.', 'Blog', 'Become a Guide'),
(6, 10, 'S', 'Home', 'Programs', 'Team', 'About Us', 'Team', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Explore. Realize. Transform.', 'The only journey is the one within', 'S', NULL, 'Learn More', '/explore', '#000000', '#ffffff', 'N', 'elegant-gold-yegr8hs.mp4', '', 'START YOUR JOURNEY WITH US', 'We enable leaders to safely embark on probably the most challenging and yet powerful journey of their lives\n', 'MEET OUR TEAM', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide'),
(7, 11, 'N', 'Home', 'Classes', 'Guides', 'About Us', 'Guide', 'Member', 'Live', 'Live Sessions', 'Processing', 'Sign Up', 'Align. Heal. Act.', 'Upgrade your human software to unlock your full potential and find your flow', 'S', NULL, NULL, NULL, '#000000', '#ffffff', 'N', 'video-header.mp4', 'video-header.mp4', 'WORK ON YOUR INNER SELF', 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.', 'MEET OUR TEACHERS', 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person', 'PROGRAMS', 'FIND A PROGRAM TO ACHIEVE YOUR GOALS', 'TRY IT FREE FOR 14 DAYS', 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.', 'Blog', 'Become a Guide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home`
--

CREATE TABLE `home` (
  `homeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `home`
--

INSERT INTO `home` (`homeId`, `nombre`) VALUES
(1, 'Header'),
(2, 'Categories'),
(3, 'Coaches'),
(4, 'Banners'),
(5, 'TrialText'),
(6, 'LiveSessions');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homeplatform`
--

CREATE TABLE `homeplatform` (
  `homePlatformId` int(11) NOT NULL,
  `homeId` int(11) NOT NULL COMMENT 'opciones=home',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `homeplatform`
--

INSERT INTO `homeplatform` (`homePlatformId`, `homeId`, `clientId`) VALUES
(137, 6, 9),
(136, 5, 9),
(135, 4, 9),
(134, 3, 9),
(133, 2, 9),
(90, 6, 8),
(89, 5, 8),
(88, 4, 8),
(87, 3, 8),
(86, 2, 8),
(16, 1, 7),
(17, 2, 7),
(18, 3, 7),
(19, 4, 7),
(20, 5, 7),
(21, 1, 4),
(22, 2, 4),
(23, 3, 4),
(24, 4, 4),
(25, 5, 4),
(57, 6, 10),
(56, 5, 10),
(55, 4, 10),
(54, 3, 10),
(53, 2, 10),
(131, 1, 3),
(41, 6, 4),
(42, 6, 7),
(85, 1, 8),
(132, 1, 9),
(52, 1, 10),
(91, 1, 11),
(92, 2, 11),
(93, 3, 11),
(94, 4, 11),
(95, 5, 11),
(96, 6, 11),
(97, 2, 12),
(98, 3, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `levelId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`levelId`, `clientId`, `nombre`, `posicion`) VALUES
(1, 3, 'Easy', 1),
(2, 8, 'Easy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pages`
--

INSERT INTO `pages` (`pageId`, `nombre`) VALUES
(1, 'Classes'),
(2, 'About Us'),
(3, 'Live Session'),
(4, 'Coaches'),
(5, 'Gallery');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passcambio`
--

CREATE TABLE `passcambio` (
  `passId` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `passcambio`
--

INSERT INTO `passcambio` (`passId`, `tipo`, `id`, `token`) VALUES
(3, 1, 5, 'T1lyVlFod3ladlhzTWpHaGVYK2k4RlN5Vzg2ZVp0ZHRzcytpd05qMmlMND0='),
(4, 1, 6, 'cURZQSsxYU01elA0T2lMVnJPZWJDS0NFUW1vc2ljSWFuNkZpcldhOUIxZz0='),
(35, 1, 85, 'NjBKVC9PZHc1Z25rK292Z08zcmMwM0dDbFE3SjUxU2Yvcjk3ZURmTU1OMD0='),
(33, 1, 68, 'NUpWb3g1QkE2eTZHbUVFdkRVaWp3SHhWdUhiRXR4d29hczFKZkZhU1hNND0='),
(29, 1, 52, 'TnlCV2lhcGlhR25oMnlLNWQxUTRmUTNoQ2trc2VWTnpKd0NYenRHUDFLTT0='),
(27, 1, 49, 'VDhtdXpsSkpDeFpjY0lCMGNEOG9yTU5ETjBKVHYyZFJxcERMcEJDdFk3OD0='),
(11, 2, 25, 'RkR4dUQ2WDgvWU51eG92eFh3Zk14dmx4QmFaaC9RQVVXbGduT3ZGQ3kzcz0='),
(12, 2, 25, 'bC82dDVvTUMyaWEvTUExSzNmTi9aZGcrNkIyekt2by9VNnNxRDZvZGFtST0='),
(13, 2, 26, 'dG1xcmFIcUtHOXI1STlVRVpLZWcvdXNMdi9wM2xhUEZNeDdZZGJ0eTduRT0='),
(19, 1, 3, 'czkzeVRWUjZrSUtsZkl3WDZ2dVg3WHRzaGpWUVNCd0tFQW5HZWsxWkxaRT0='),
(36, 1, 85, 'bjZ5d1hNbmtUc1B2ZkxPVW5iTmNTZnR4bjFDTTcxN1M0QUNaeEJ4enFEdz0='),
(37, 1, 85, 'cThRN3ZpK3p6R0NiSlAwTFRTZm44STFBWTdTRDdnek9nVzJHRUdlSllUMD0='),
(38, 1, 85, 'V0ZDaDM4aWlTL1dJUUpqT0dicE4rdlZUS3BwOHdSMnd1ZVI3S3JXdVVNRT0='),
(39, 1, 85, 'djBvZHJwc0JIYzRCbkJId0w2R1JuQ3BhSHh1Um1Hc0FWRmkyWGtha2lvcz0='),
(40, 1, 85, 'emtLWVdEcVdOc0xMQm1Ob3g5VVZDWFhaZkNkU20wcUh2SmFKMFVTQVloQT0='),
(41, 3, 10, 'ZmdIMXp0eTBOZVJHcGM4VVhXb2FjclFOTWRadG9iSGwyMnRSbUY0UFUzZz0='),
(42, 2, 38, 'QUozYXJJL1lSRFRsZENRR0Rkd3FibktyV0xXUWJLSFRIMVdvYU5uLzZKZz0='),
(43, 1, 70, 'cWN4a1p3dHZDZkc4SkhxeWoyamt0WDRqcVhyQjF4M2VITVRXd0gxci9Hbz0='),
(50, 1, 54, 'bTVZSTJFWEVMT3RmV0o0dXliUzR1ZmNtMzR1dkdSWGFpTDNmNWM4d2lxOD0='),
(49, 1, 54, 'T0NHcjZrVW5OUENTRVJiczVGbHZqSDR1QktGRHZ1RWRKN3AwRzFzRGVyRT0='),
(48, 1, 54, 'WkJqdS90YmcvNzhhRFFKR0trT01VTFJibHVmRHozSGdWL1RYb01NK0xlQT0='),
(51, 1, 54, 'b3JJSnlCZVZPU3ZoYWNMWXBqa1lkZzFxWHI3QVRadTJxN0EycEdwYmV6UT0='),
(52, 1, 54, 'MW5ScE13VGpja2NkQ09hbnNwaWZ0WHUrN2Fid2o1dXpvMDJQbHBwY1RuWT0='),
(53, 1, 54, 'cHV0aGtORldrWnZnYUE5cXBGWTEvZ3c1QnZycmRsU0dYZGdVd2J0dTgzbz0='),
(54, 1, 54, 'Ly81L0pHZE1xUHhWNExPOGwrTi8rcDdFYXJHWEhjdkh0TW5tRWFKWEZFMD0='),
(55, 1, 54, 'TXhveTlQTFNPbitxdmIwZzZvY3RxdkhWVWdldytLWXB1ZkNKTWdDcUQyST0='),
(56, 1, 54, 'RE9NcE9ydFUrWnNNOTNNSm1tQnBuSkFQOFUxSGt4Y0ZtclJkNUM0THQwTT0='),
(57, 1, 54, 'bVlsL0M4TUpnclpiTzk3aVRoanFqSTh0SGdWTjExcHFzQzVKNlNVQngrdz0='),
(58, 1, 54, 'cCs0eHRGTGUxNU9HajVpbGx5VTlIeG5Xb1RLUUltMkFRYU14MXVJSmt0bz0='),
(59, 1, 54, 'STN3NWxWZmFndVZnNGxYaU5saTNkTUc5TS9EcVJjWFQwU2ZseCs1OGpLTT0='),
(60, 1, 54, 'Z0l2Qi9LQWEwUFRGRTZlRVJhOVdiWGJDd09BYTdxNnJPMzJUUFdwWUNjWT0='),
(61, 1, 54, 'MXphLzVJZ25idVhrRXZKL0Q0ejlwMTI1MzBodTROWmZ5bDRrRWl1OEVYST0='),
(62, 1, 54, 'QllBUVZXMGdLTUhicWlCUGJtNjhJSVBQTTJaNGE5VTd0SVE4SXBrY3dtaz0='),
(63, 1, 92, 'RXp6STh5ZzA5eE5uclFRT1RpYU1kUGtoWEFaNzh5NmlKS05CYlZ3MDNaZz0='),
(64, 1, 92, 'b3ByVHFraVNUWk1RYk1Cd1AvNklIeUhtUjBiVzQwcTF5SHBVL09oeTJ4cz0='),
(65, 1, 92, 'dDhlNms3WDRLVFNRYmR1NWEwbzJHZ3hmaHNOWjZ0Z3NkMUZ1cWZkUUs3VT0='),
(83, 1, 76, 'b3pSMUtOaFlmNjhUYUthS2YraUtuTlI0STd3UW5EZXJTUTlnZ1FIWkJBdz0='),
(82, 1, 76, 'bmcwZDgrcWlpS0pjcFVFQ1RVcjl0T09KYjJ6T2xTTGliQ2FFdE14WW1hdz0='),
(81, 1, 76, 'aFpYMVVMZXpBQzY2QStibEEvbDRaRnBwaUZxYWRsZVU5N1ZNcEZLak03ST0='),
(80, 1, 76, 'MFAvQVZ2NzRhYlRocWhOTjM5YkYzL1hiaUJZWkc1VEJNc1p6MjlCMDdpND0='),
(79, 1, 76, 'ODlKbzlYd01JNDlCYlgvc2JYbDd6UmpSS0dYM05YK05DWFRidnJ5c0VtTT0='),
(78, 1, 76, 'Q3JqUmV5MGlwYUxZQWMxYmhGek9pdXczNjR0anQxZnowT2NhSnA1bVB2az0='),
(77, 1, 76, 'dWxQL3hzZ25Wdk8wc2JuV2tuakhqWFlHdkNjS3paSmxNcUZVQ01UcTEvQT0='),
(76, 1, 76, 'NTlqUTJTTU1Cd2xNaVRiSjg3Q1ZLeUl0UzlpUFYxUXdiRVMwc2lyNzdIZz0='),
(75, 1, 76, 'b3BFVk8xR3ZyVzljamw1bU1hUGRtRUFSUm54R1I5ZzBFTjFmNXV5ZUJHMD0='),
(84, 1, 76, 'YUs5Wkl5dFlMc0hjUVdxYkRGU01vR1Iza29EOHh1SDFmMjFlQ3FWdFk1UT0='),
(85, 1, 76, 'UDRVdXlOQ2FuUG1oU0JUYjlTU1BkSUgxWkJEME42NzhmTkkwN2ZlaEF2az0='),
(86, 1, 76, 'ay82Wm1KUUJmRDdqenduSHV5OGFuUE1TN2N6eHZ0YUdLaTJ3Q1N2bytZND0='),
(87, 1, 76, 'Ykx0K1ZvaEo4QXIyYUFkam9FRWczYjJ2TWVMaW9uanlWWm9CZ3pocWs5UT0='),
(88, 1, 76, 'Y1IrTklMYWpINTR3L2cvYU5DaE1pcmNoNFQ5MlVBRFZlUkpOS1JFTWgvQT0='),
(89, 1, 76, 'T1NzZnlwZXV5SEhuMlVYcWIrdG5VdXlEWWtqZjFOTE9KVVZXWFUyZW1IND0='),
(90, 1, 76, 'eVJQc1R2OEFUYmdSL0hwWk1ndmcrQmpUZ1FzM25peUNWUEdjUENUcEJEaz0='),
(91, 1, 76, 'NDN3SDFia1NnTjBlS25kSGdaV0dtOXBjTEtNTnFLdmlEeGl0UXN0ajYvbz0='),
(92, 1, 76, 'alhWZUJaYUZ0VHF2emU1bTd5LytvdTVHNVlnZm94TjQyUU1naDhJTmw0OD0='),
(93, 1, 76, 'cFRPVmN6L3BxRkhiK1Rsb0RWTmNJRXFaLy94NFJLQVBJaVlMRjVYTVIzaz0='),
(94, 1, 76, 'Rk5obXBnZ2xydlhLcHIxN1oyMnVPcEtQVTlQTGcrWHZpczNoNTc0UHdPMD0='),
(95, 2, 44, 'Q1lTZnhrLzJ4WHhWTEd6MjZvOG5vaWpWUWJZYVp5bzArb3NXaTRhTjdvOD0='),
(96, 2, 44, 'WXhJSzZFWUIrOUhHY0NJWndoMjZRMjhNaW9RRmNRS3pPTGhqV0Y1ZTZLQT0='),
(97, 2, 44, 'SW1ka093SUpjQWMzZk81MXNUWmIvVnVRWkRjOHNtMXZLeGRMTFd3amw5VT0='),
(98, 2, 44, 'TUV1YXpMZFJWRVZaWEFlTWZ3RW55YzhpWGl1bEx1eW9EWUJ2S3pzSU1sbz0='),
(99, 2, 44, 'czUwbTZGOGdkcVM2UUhKWjdzR2FBRCs0OEtRekx3eExFT1R4a0hCYUlFdz0='),
(100, 2, 44, 'THZnaXY1MFhBQjFqTkx4UnJCTlI3em5aSThLZEg2YWJFWGJFb1pXSk55MD0='),
(101, 2, 44, 'WWZUd0dFbXptR3lhYXViK2dkUlBBLzVSSURNTzZ0d2tCMHFPZjBaeVlJMD0='),
(102, 1, 68, 'ZDRjdkZDSmtDeUxKQ25HL0FsWEkwamwrMTdBbkEvTW12Y0FSVTllRUJ6MD0='),
(103, 1, 68, 'UTJTNkdBZ0FmN2Y5bnNqdytSdkMrbHNZdWcxd3dlUHRKRW9MOHRDMDdnYz0='),
(104, 1, 68, 'SGRmUEVSNXoySTUwZ2E3OHorb29Va2N2QitzdDZSNGJzcFlYOGRaaG9aND0='),
(105, 1, 68, 'Z3h4VWlObWxnWXdBNnlpL2lhUGhsZURWTmhBamJhcUp6aTd5SXRnWjVmbz0='),
(106, 1, 68, 'bTRaMS8zd0IvSmxYY3Q3S2ZlYUVmQWJUSEJJVzI3ZEtUbnBhR0tqSEFPQT0='),
(107, 1, 68, 'clNaT3Y0WHVXcGpKM0d2ckgrMm1XZEhEekw1MG9iU2RmS0kycUNSYjZIQT0='),
(108, 1, 68, 'UCs4TGlQWXFaMzFUMHpKeEdyQXg3Tnlwa2JDbmJjZWlWNUxLb2lXM3ZpND0='),
(109, 1, 68, 'TFZkcTIvTFRBYVZUdFp2U1Vvc1h1dm1oMUpCS3NQaEtYWnN6R3VuaGRTbz0='),
(110, 1, 68, 'enV5WmZvbmNhYzlqVGNGU3BXTjZrRGNpekFqQmNmcS9ENmp1VUFvbFpqWT0='),
(111, 1, 68, 'ZEpyUStpWnk5a2tJVDVEVEdXRTAxVHhua3ZrZ2VVQ05QbnRwTnI0c1hmVT0='),
(112, 1, 68, 'eDVHUGZ0djllZy9RZDduUXVOeVdWRzUrN0lLYVdoajYvVEwxMENEYi9yaz0='),
(113, 1, 68, 'Rk1DcWVOQittM29aaWtuZVVuamt2SWt2cHVldGFNUElnUVlIdzE5bzhFYz0='),
(114, 1, 68, 'Rk02OWN3UjFBeUtQK1J3Q3J3YUdvN3lxdStlWm5oL3VqRjE2SklGMnlJQT0='),
(115, 1, 68, 'R0lVSVNpQXozaGxJYnV0NTNzOGs1UWpHVk9mZ3NjK2kzTmwyMmplR09YQT0='),
(116, 1, 68, 'NnFsa2N6bTJKWFF6WEFDdkVSd1dyQXVqTXZWckUvTDgrYzZRemdTOGVDVT0='),
(117, 1, 68, 'NzNtcWV3ZUFtWlMxc1doUUJqOTk3YmJIczhMR0h4NWRENkROR3JCYWpNOD0='),
(118, 1, 70, 'UzlFRXphUVQyTUd5SnhHQzZLSHRtR1dISXE0UDM2Y1JxbGJXdm5SN1doYz0='),
(119, 1, 70, 'aWp0RmYrSlN4b21kRkhmV29JS1B1YlJURmU5eFNzVHdGUi9sRWhrclV6Zz0='),
(120, 1, 70, 'N010Y2IvMzBoNFgveGJqRy9kUXdoQVZoWkdodzF6RnBEbEFBSFo0Y1J5TT0='),
(121, 1, 68, 'ZEl4UEZSMFBCV1kzTjRTVURkNHpZM1d6WSthN3NTZU9ySCtncTF1MVJjYz0='),
(122, 1, 68, 'ZG9ZN0UxN3JQYWJVRWdLRndNQ0ZHRFg1Sk5CSWl1SnBza2ZFaDZZd1A1TT0='),
(123, 1, 68, 'cGN6V3hKcERycTREUWhvb1JIQ01oUk93Vk9TZ21Rdlp3NDAvMHhveGpXWT0='),
(124, 1, 68, 'cFpMUW4ycGtvL3p6U2NldkJJMzZjUzR4VjBpeVhYYWx3elppTTVpalFhVT0='),
(125, 1, 70, 'dStPVHNQSkl0NGdhdXFha3pQT3kxVkZnZ1k4ZzgxNVQ3bEFEMHJqazZITT0='),
(126, 1, 37, 'b3k3c1hiR1hxRDc1RmVzUnY4MTUvZlBxc1R4NjVIM2NjU3ltcWFSS3ZmTT0=');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platforms`
--

CREATE TABLE `platforms` (
  `platformId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `fontFile` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `fontFileHead` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `url` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL COMMENT 'email',
  `clientId` int(11) DEFAULT NULL COMMENT 'opciones=clients'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platforms`
--

INSERT INTO `platforms` (`platformId`, `nombre`, `logotipo`, `icono`, `fontFile`, `fontFileHead`, `url`, `email`, `clientId`) VALUES
(1, 'Innerworks', 'innerworks_logo_5-innerworks.png', 'innerworks_logo_black-innerworks.png', 'hero-new-light-innerworks.otf', NULL, 'innerworks', 'roman@romanf.com', 3),
(2, 'Ephata', 'ephata.png', 'ephata.png', 'roboto-regular.ttf', NULL, 'ephata', 'elopez@junkyard.mx', 4),
(3, 'Nierika', 'nierika-logo-nierika.png', 'nierika-logo-nierika.png', 'roboto-regular-nierika.ttf', NULL, 'nierika', 'universidad@nierika.com', 7),
(4, 'Test', 'zenrise_logo_01_black_background-test.png', 'zenrise_logo_01_black_background-test.png', 'hero-new-light-test.otf', NULL, 'test', 'elopez@junkyard.mx', 8),
(5, 'Shala', 'shala-new-logo-2-shala.png', 'shala-new-logo-2-shala.png', 'roboto-regular-shala.ttf', NULL, 'shala', 'rome7f@gmail.com', 9),
(6, 'Eleutheria', 'editables_eleutheria-01-eleutheria.png', 'editables_eleutheria-04-eleutheria.png', 'roboto-regular-eleutheria.ttf', NULL, 'Eleutheria', 'rome7f@gmail.com', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platformservices`
--

CREATE TABLE `platformservices` (
  `psId` int(11) NOT NULL,
  `platformId` int(11) NOT NULL COMMENT 'opciones=platforms',
  `catalogId` int(11) NOT NULL COMMENT 'opciones=servicescatalog'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platformservices`
--

INSERT INTO `platformservices` (`psId`, `platformId`, `catalogId`) VALUES
(80, 1, 5),
(79, 1, 4),
(78, 1, 3),
(77, 1, 2),
(76, 1, 1),
(35, 2, 5),
(34, 2, 4),
(33, 2, 3),
(32, 2, 2),
(31, 2, 1),
(46, 3, 1),
(47, 3, 2),
(48, 3, 3),
(49, 3, 4),
(50, 3, 5),
(105, 4, 5),
(104, 4, 4),
(103, 4, 3),
(102, 4, 2),
(101, 4, 1),
(100, 5, 5),
(99, 5, 4),
(98, 5, 3),
(97, 5, 2),
(96, 5, 1),
(106, 6, 1),
(107, 6, 2),
(108, 6, 3),
(109, 6, 4),
(110, 6, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postcategory`
--

CREATE TABLE `postcategory` (
  `categoryId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `name` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `postcategory`
--

INSERT INTO `postcategory` (`categoryId`, `clientId`, `name`) VALUES
(1, 3, 'Category One'),
(2, 3, 'Category Two'),
(3, 8, 'Category One'),
(4, 8, 'Category Two');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `postId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories',
  `title` varchar(200) NOT NULL COMMENT 'nombre',
  `image` varchar(200) NOT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `content` text NOT NULL COMMENT 'html',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`postId`, `clientId`, `categoryId`, `title`, `image`, `video`, `content`, `fecha`) VALUES
(1, 3, 1, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(2, 3, 1, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T FORCE IT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">KEEP IT SHORT</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">TRY SOME BREATHWORK</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">DON’T GET HUNG UP ON FORM</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATE ON THE MOVE</strong></h2><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"font-size: 18px; letter-spacing: 0.2em; line-height: 1.3em; text-transform: uppercase; color: rgba(28, 28, 28, 0.88); white-space: pre-wrap; font-family: &quot;Arquitecta Heavy&quot; !important;\"><strong style=\"overflow-wrap: break-word;\">MEDITATION ISN’T PERFECT!</strong></h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-03-15'),
(3, 3, 2, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">7 a.m.</strong> <br style=\"overflow-wrap: break-word;\">Wake up. This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-03-16'),
(4, 8, 3, '3 CLASSES TO GET YOU INTO MIDDLE SPLITS', 'talia-sutra-leading-a-class-in-middle-splits.png', NULL, '<p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\">We’ve been getting a lot of questions lately in our facebook Group about the coveted Middle Splits (also called Center Splits), so today we’re talking about what Middle Splits are, the benefits, and three Alo Moves classes to help you practice getting into it. As you practice, remember to take it slow without forcing anything and enjoy the journey. Middle splits take time to achieve, but watching your progress unfold over time is worth the wait and effort.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">What Are Middle Splits?<br style=\"overflow-wrap: break-word;\"></strong>Middle splits are when your legs extend out from your body, forming a 180-degree angle with your torso. In the full expression, your ankles, knees, and hips are aligned.</p><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">Benefits of Doing Middle Splits</strong></p><ul data-rte-list=\"default\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Stretches and lengthens your hips, thighs, and groin</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Opens your hips and hip flexors</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Improves joint health, flexibility and balance</p></li><li style=\"list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Prevents injury and can reduce pain</p></li></ul><p class=\"\" style=\"overflow-wrap: break-word; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px; white-space: pre-wrap;\"><strong style=\"overflow-wrap: break-word;\">How to Do Middle Splits</strong></p><ol data-rte-list=\"default\" style=\"overflow-wrap: break-word; margin-bottom: 0px; color: rgba(28, 28, 28, 0.81); font-family: proxima-nova; font-size: 16px; letter-spacing: 0.96px;\"><li style=\"counter-increment: rte-list 1; list-style-type: none; counter-reset: rte-list 0; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Come into a Wide Forward Fold with your hands resting on the mat. Take your legs out to the side as far as they can go.&nbsp;</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Pigeon toe your feet so your inner arch and outer foot are both on the floor. When you feel ready, stay on your hands or a block and pull up on your knees, or lower onto your elbows. It’s helpful to gently rock your hips back and forth to settle in.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">When it becomes available to you, sink your hips down as low to the floor as possible. Continue rocking back and forth with your hips.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">Finally, come into stillness. Pull up with your knees and engage the fronts of your thighs.</p></li><li style=\"counter-increment: rte-list 1; list-style-type: none; overflow-wrap: break-word;\"><p class=\"\" style=\"margin-top: 0.5em; margin-bottom: 0.5em; overflow-wrap: break-word; white-space: pre-wrap;\">You can also practice Middle Splits against a wall, letting your glutes rest on the wall and taking your legs out wide.</p></li></ol>', '2021-03-14'),
(5, 8, 21, 'HOW TO MEDITATE IN THE MORNING WHEN YOU’RE NOT A MORNING PERSON', 'koya-webb-with-her-hands-at-her-heart.jpg', NULL, '<p class=\"\" style=\"overflow-wrap: break-word;\">Meditation is a great practice for starting your day with clarity, focus, and calm. But for those of us who aren’t great with mornings — not just a little cranky, but barely able to pull on a pair of pants — starting a day with meditation can seem unattainable, especially on a tight schedule. Fortunately, it doesn’t take a lot to build in some balance and quell that morning rush.</p><p class=\"\" style=\"overflow-wrap: break-word;\">Here’s a little guidance on carving out a practice that fits into your unique kind of morning, even if it is borderline impossible to feel grateful and/or blessed with each sunrise.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T FORCE IT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You don’t have to start each day with gratitude. You don’t have to wake up an hour earlier. If some of the typical morning meditation guidance feels alienating, you don’t have to follow it. Start with a meditation that resonates with you. Meditation is not a chore, nor should it be.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">KEEP IT SHORT</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Consistency is more important than how many minutes of meditation you can squeeze into your day. Even if you can only grab a minute or two out of your morning, it’s better than zero minutes. Even full, guided meditations come in small packages: Each class in series is just over five minutes long.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">TRY SOME BREATHWORK</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Breathwork is a concrete mindfulness practice you can do from anywhere, whether you’re in bed or taking a shower, and it doesn’t take a lot of forethought. Connecting your mind and body with your breath can help get your head on straight, and an invigorating exercise might help you wake up a little bit, too.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">FIND WHERE YOU’RE ALREADY PAUSING IN THE MORNING</h2><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">DON’T GET HUNG UP ON FORM</h2><p class=\"\" style=\"overflow-wrap: break-word;\">You’re not doing this for Instagram — even people who post about meditation on Instagram are posing for those photos! Meditation is a deeply personal practice, and you only need what serves you. A meditation space can be helpful, but isn’t necessary. You don’t need to light incense or face a certain direction. Don’t let setup become a distraction from actually doing the practice.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATE ON THE MOVE</h2><p class=\"\" style=\"overflow-wrap: break-word;\">Meditation takes many forms, and transforming going from Point A to Point B into a moment of calm can be especially useful to those of us that are always rushing out the door.</p><p class=\"\" style=\"overflow-wrap: break-word;\">If part of your morning typically includes a walk, try. You can even practice meditation on the bus! A few different meditation apps have guided practices specifically for commuting, or you could try firing up your favorite guided meditation to see if it works for you in that setting.</p><p class=\"\" style=\"overflow-wrap: break-word;\">You can also spend these transition times by trying to objectively observe the setting around you — the breeze moving through the trees, bus chatter, doors opening and closing. Just remember to stay safe and continue to look both ways before you cross the street.</p><h2 style=\"line-height: 1.3em;\" arquitecta=\"\" heavy\"=\"\" !important;\"=\"\">MEDITATION ISN’T PERFECT!</h2><p class=\"\" style=\"margin-bottom: 0px; overflow-wrap: break-word;\">Even the sunniest early birds and most experienced meditation practitioners have plenty of trouble staying present sometimes. So you didn’t achieve total stillness — you still practiced today, and that’s enough.</p>', '2021-04-08'),
(6, 8, 21, 'MY MORNING ROUTINE: DYLAN WERNER', 'dylan-werner-meditating.jpg', 'MTEwNzI1MQ==', '<p>Keeping a real routine has been a challenge for me since my life usually revolves around traveling. And, actually, because I traveled so much, keeping certain daily practices was so important to me so that I felt that I had some consistency and stability in my life. Most of these routines centered around my daily yoga practice. But since COVID and being unable to travel and teach, I have had a chance to really embrace a morning routine. And as time goes on, my routine seems to get longer and longer. Here\'s what the typical morning routine currently looks like for me.</p><p><span style=\"font-weight: bold;\">7 a.m. Wake up.</span></p><p>This is just the natural time I wake up. I rarely to never use an alarm clock. Sometimes it\'s a little earlier, in which case I will lay in bed and enjoy the sounds of the birds for a while before getting up.</p>', '2021-04-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionnaire`
--

CREATE TABLE `questionnaire` (
  `questionId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `question` varchar(1000) NOT NULL COMMENT 'nombre',
  `answerOne` varchar(200) NOT NULL,
  `answerTwo` varchar(200) NOT NULL,
  `answerThree` varchar(200) DEFAULT NULL,
  `answerFour` varchar(200) DEFAULT NULL,
  `answerFive` varchar(200) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `questionnaire`
--

INSERT INTO `questionnaire` (`questionId`, `clientId`, `question`, `answerOne`, `answerTwo`, `answerThree`, `answerFour`, `answerFive`, `position`) VALUES
(1, 3, 'How did you learn about Innerworks?', 'Invited by a Guide/Coach', 'Online search', 'Through company HR', 'Referred by a friend', 'Other', 1),
(6, 4, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(7, 7, 'What is your interest in visiting Nierika?', 'Lodging', 'Therapy', 'Volunteer', 'Retreat', 'Event space', 1),
(8, 7, 'How did you find out about us?', 'Referral', 'Webpage', 'Web search', 'Other', NULL, 2),
(9, 8, 'Question One', 'One', 'Two', 'Three', 'Four', 'Five', 1),
(10, 8, 'Question Two', 'One', 'Two', 'Three', 'Four', 'Five', 2),
(11, 8, 'Question Three', 'One', 'Two', 'Three', 'Four', 'Five', 3),
(12, 8, 'Question Four', 'One', 'Two', 'Three', 'Four', 'Five', 4),
(13, 8, 'Question Five', 'One', 'Two', 'Three', 'Four', 'Five', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seriecircle`
--

CREATE TABLE `seriecircle` (
  `serieCircleId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serierequest`
--

CREATE TABLE `serierequest` (
  `requestSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `serieId` int(11) NOT NULL COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;multiple=circles,seriecircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `description` varchar(5000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`serieId`, `clientId`, `coachId`, `nombre`, `description`, `photo`, `video`, `accessId`) VALUES
(14, 3, 1, 'The Loop', 'The Loop is a course in mastering and manifesting change and transformation in your life. The course entails a total of 10 episodes, out of which 4 are optional as live sessions together with Nils.', 'yinyang.jpeg', 'MTEwNzIwNA==', 2),
(16, 4, 18, 'Hero\'s Journey', NULL, NULL, NULL, 3),
(17, 4, 18, 'Ephata Summary', NULL, NULL, NULL, 3),
(18, 3, 6, 'Meditations', 'This series will help you get started with meditation through my short guided exercises.', 'top-5-scientific-findings-on-meditationmindfulness.jpeg', 'MTA3NDU2MA==', 2),
(22, 4, 3, 'Ephata Series', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '42-15833533.jpg', 'MTA4NTMxMg==', 1),
(31, 3, 1, 'Sircle Collection', 'Welcome!', 'skärmavbild-2021-08-30-kl-0001201630274515.png', NULL, 2),
(32, 3, 0, 'Test Video', 'Test', 'stress1630332544.jpg', 'MTE1NjQwMQ==', 2),
(29, 3, 1, 'Guided Meditations', 'Guided meditations for self-understanding, self-grounding and self-leadership.', 'isabell-winter-lzyzedj8fbo-unsplash1630071361.jpg', NULL, 2),
(30, 3, 1, 'Flow, Growth & Direction (FGD)', 'FGD is a series of hands-on tools, strategies and exercises for self-understanding, self-grounding and self-leadership.', 'artem-kovalev-fk3xucftavk-unsplash1630071431.jpg', NULL, 2),
(33, 3, 55, 'Action Learning', 'Fraendi Pilot Program', 'fraendi_logo_ohne_rand-removebg1630508856.png', NULL, 2),
(34, 3, 6, 'Sircle Collection', 'Meditations hosted by Mattis', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seriescategories`
--

CREATE TABLE `seriescategories` (
  `scId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL COMMENT 'opciones=series',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seriescategories`
--

INSERT INTO `seriescategories` (`scId`, `serieId`, `categoryId`) VALUES
(69, 14, 4),
(17, 19, 2),
(13, 20, 3),
(18, 21, 2),
(23, 13, 1),
(19, 21, 4),
(24, 22, 7),
(96, 31, 4),
(29, 25, 2),
(30, 23, 4),
(31, 26, 13),
(68, 14, 2),
(67, 14, 1),
(42, 28, 4),
(99, 29, 2),
(98, 30, 4),
(97, 30, 2),
(63, 32, 2),
(91, 33, 4),
(94, 18, 2),
(95, 34, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicescatalog`
--

CREATE TABLE `servicescatalog` (
  `catalogId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicescatalog`
--

INSERT INTO `servicescatalog` (`catalogId`, `nombre`) VALUES
(1, 'Schedulling Calls'),
(2, 'Pre-Recorded Videos'),
(3, 'Live Video Conferences'),
(4, 'Interactive Exercises & Tests'),
(5, 'Reporting');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessionrecording`
--

CREATE TABLE `sessionrecording` (
  `sessionRecordingId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shala`
--

CREATE TABLE `shala` (
  `shalaId` int(11) NOT NULL,
  `logotipo` varchar(200) NOT NULL COMMENT 'archivo',
  `icono` varchar(200) NOT NULL COMMENT 'archivo',
  `spotlightrApiKey` varchar(200) DEFAULT NULL,
  `stripePublicKey` varchar(200) DEFAULT NULL,
  `stripePrivateKey` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shala`
--

INSERT INTO `shala` (`shalaId`, `logotipo`, `icono`, `spotlightrApiKey`, `stripePublicKey`, `stripePrivateKey`) VALUES
(1, '', '', 'c53bcd228d6d73879a567d77fa35c5368d390ecb', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slidehome`
--

CREATE TABLE `slidehome` (
  `slideId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `imagen` varchar(200) NOT NULL COMMENT 'archivo',
  `imagenMovil` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `altText` varchar(200) DEFAULT NULL,
  `posicion` int(11) NOT NULL COMMENT 'posicion'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slidehome`
--

INSERT INTO `slidehome` (`slideId`, `clientId`, `imagen`, `imagenMovil`, `altText`, `posicion`) VALUES
(1, 7, 'slide1.jpg', 'slide1movil.jpg', NULL, 1),
(2, 7, 'slide2.jpg', 'slide2movil.jpg', NULL, 2),
(3, 7, 'slide3.jpg', 'slide3movil.jpg', NULL, 3),
(4, 7, 'slide4.jpg', 'slide4movil.jpg', NULL, 4),
(5, 7, 'slide5.jpg', 'slide5movil.jpg', NULL, 5),
(6, 9, 'slider1png.png', NULL, 'All-in-one website builder for secured platforms with LIVE content and more …', 1),
(11, 9, 'slider6png.png', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statusaccount`
--

CREATE TABLE `statusaccount` (
  `statusId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `statusaccount`
--

INSERT INTO `statusaccount` (`statusId`, `nombre`) VALUES
(1, 'Inactive'),
(2, 'Active');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoadmin`
--

CREATE TABLE `tipoadmin` (
  `tipoId` int(11) NOT NULL COMMENT ' multiple=accessadmin,accessuseradmin',
  `nombre` varchar(100) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoadmin`
--

INSERT INTO `tipoadmin` (`tipoId`, `nombre`) VALUES
(2, 'Global Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipollamada`
--

CREATE TABLE `tipollamada` (
  `tipoId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipollamada`
--

INSERT INTO `tipollamada` (`tipoId`, `nombre`) VALUES
(1, 'Public'),
(2, 'Private'),
(3, 'Registered Users'),
(4, 'Circles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `todos`
--

CREATE TABLE `todos` (
  `todoId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `userId` int(11) DEFAULT NULL,
  `coachId` int(11) DEFAULT NULL,
  `videoId` int(11) DEFAULT NULL,
  `audioId` int(11) DEFAULT NULL,
  `articleId` int(11) DEFAULT NULL,
  `markStatus` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'unmark=0,marked=1',
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `todos`
--

INSERT INTO `todos` (`todoId`, `clientId`, `userId`, `coachId`, `videoId`, `audioId`, `articleId`, `markStatus`, `datetime`) VALUES
(6, 3, NULL, 44, NULL, NULL, 2, 1, '2021-11-03 08:07:32'),
(8, 3, NULL, 1, NULL, NULL, 2, 0, '2021-11-03 10:21:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typemail`
--

CREATE TABLE `typemail` (
  `typeId` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL COMMENT 'nombre'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typemail`
--

INSERT INTO `typemail` (`typeId`, `nombre`) VALUES
(1, 'Password Recovery'),
(2, 'Contact Web'),
(3, 'Rejected Coach'),
(4, 'Approved Coach'),
(5, 'Registered Coach'),
(6, 'Requested Series Access'),
(7, 'Requested Coach'),
(8, 'Approved Member'),
(9, 'Rejected Member'),
(10, 'Approval coach request '),
(11, 'Approval member request'),
(12, 'Email Confirmation');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userarticle`
--

CREATE TABLE `userarticle` (
  `userArticleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `articleId` int(11) NOT NULL COMMENT 'opciones=articles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userarticleserie`
--

CREATE TABLE `userarticleserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=articleseries'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userarticleserie`
--

INSERT INTO `userarticleserie` (`userSerieId`, `userId`, `serieId`) VALUES
(2, 73, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `useraudio`
--

CREATE TABLE `useraudio` (
  `userAudioId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `audioId` int(11) NOT NULL COMMENT 'opciones=audios'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercall`
--

CREATE TABLE `usercall` (
  `userCallId` int(11) NOT NULL,
  `callId` int(11) NOT NULL COMMENT 'opciones=calls',
  `userId` int(11) NOT NULL COMMENT 'opciones=users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercall`
--

INSERT INTO `usercall` (`userCallId`, `callId`, `userId`) VALUES
(1, 2, 3),
(2, 3, 3),
(3, 4, 3),
(4, 6, 3),
(5, 7, 3),
(6, 9, 4),
(7, 10, 4),
(8, 11, 4),
(9, 14, 7),
(10, 15, 3),
(11, 16, 3),
(12, 19, 4),
(13, 20, 4),
(14, 22, 11),
(15, 36, 15),
(16, 47, 3),
(17, 49, 3),
(18, 70, 24),
(19, 76, 4),
(20, 43, 3),
(21, 79, 34),
(22, 80, 3),
(23, 80, 4),
(24, 80, 26),
(25, 80, 34),
(30, 89, 40),
(29, 83, 40),
(31, 90, 54),
(32, 109, 70),
(55, 110, 70),
(34, 114, 70),
(35, 115, 70),
(38, 116, 70),
(39, 117, 70),
(40, 121, 70),
(42, 123, 70),
(43, 125, 70),
(54, 132, 70),
(53, 131, 70),
(56, 133, 70),
(63, 137, 73),
(59, 134, 73),
(65, 138, 73),
(66, 139, 73),
(67, 140, 73),
(69, 147, 73),
(72, 150, 73),
(73, 151, 73);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercircle`
--

CREATE TABLE `usercircle` (
  `userCircleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usercircle`
--

INSERT INTO `usercircle` (`userCircleId`, `userId`, `circleId`) VALUES
(7, 35, 28),
(6, 9, 28),
(41, 54, 30),
(40, 53, 30),
(39, 52, 30),
(38, 51, 30),
(37, 50, 30),
(36, 49, 30),
(35, 48, 30),
(34, 47, 30),
(33, 46, 30),
(32, 45, 30),
(31, 44, 30),
(30, 43, 30),
(29, 42, 30),
(28, 41, 30),
(27, 40, 30),
(26, 39, 30),
(25, 38, 30),
(42, 55, 30),
(43, 56, 30),
(44, 57, 30),
(45, 60, 32),
(46, 87, 32),
(47, 88, 32),
(48, 89, 32),
(49, 92, 32),
(50, 72, 33),
(51, 73, 33),
(57, 73, 34),
(56, 72, 34),
(58, 72, 35),
(59, 73, 35);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userclient`
--

CREATE TABLE `userclient` (
  `userClientId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `pending` char(1) NOT NULL DEFAULT 'S',
  `notes` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userclient`
--

INSERT INTO `userclient` (`userClientId`, `userId`, `clientId`, `pending`, `notes`) VALUES
(1, 1, 3, 'N', NULL),
(2, 1, 3, 'N', NULL),
(9, 3, 3, 'N', NULL),
(8, 2, 3, 'N', NULL),
(10, 4, 3, 'N', NULL),
(11, 5, 3, 'N', NULL),
(12, 6, 3, 'N', NULL),
(13, 3, 3, 'N', NULL),
(14, 3, 3, 'N', NULL),
(15, 7, 3, 'N', NULL),
(16, 8, 3, 'N', NULL),
(17, 4, 3, 'N', NULL),
(18, 9, 3, 'R', NULL),
(19, 10, 3, 'N', NULL),
(20, 11, 3, 'N', NULL),
(21, 12, 3, 'N', NULL),
(22, 12, 3, 'N', NULL),
(23, 13, 3, 'N', NULL),
(24, 13, 3, 'N', NULL),
(25, 14, 3, 'N', NULL),
(26, 15, 3, 'N', NULL),
(27, 16, 3, 'N', NULL),
(28, 17, 4, 'N', NULL),
(29, 18, 4, 'N', NULL),
(30, 19, 3, 'N', NULL),
(31, 3, 3, 'N', NULL),
(32, 11, 1, 'N', NULL),
(33, 20, 1, 'N', NULL),
(34, 21, 3, 'N', NULL),
(35, 22, 7, 'N', NULL),
(36, 23, 7, 'N', NULL),
(66, 4, 8, 'N', NULL),
(65, 3, 8, 'N', NULL),
(64, 25, 3, 'N', NULL),
(63, 10, 8, 'N', NULL),
(67, 26, 3, 'N', 'test'),
(68, 27, 8, 'N', NULL),
(69, 28, 8, 'N', 'ascasc'),
(70, 30, 3, 'S', NULL),
(71, 31, 8, 'N', NULL),
(72, 32, 8, 'S', NULL),
(73, 33, 8, 'S', NULL),
(74, 34, 3, 'N', NULL),
(75, 35, 3, 'N', NULL),
(76, 36, 3, 'S', NULL),
(77, 37, 3, 'N', NULL),
(78, 38, 3, 'S', NULL),
(79, 39, 3, 'S', NULL),
(80, 40, 3, 'N', NULL),
(81, 41, 3, 'N', NULL),
(82, 42, 3, 'N', NULL),
(83, 43, 3, 'N', NULL),
(84, 44, 3, 'S', NULL),
(85, 45, 3, 'N', NULL),
(86, 46, 3, 'N', NULL),
(87, 47, 3, 'N', NULL),
(88, 48, 3, 'S', NULL),
(89, 49, 3, 'S', NULL),
(90, 50, 3, 'N', NULL),
(91, 51, 3, 'N', NULL),
(92, 52, 3, 'N', NULL),
(93, 53, 3, 'S', NULL),
(94, 54, 3, 'N', NULL),
(95, 55, 3, 'N', NULL),
(96, 56, 3, 'N', NULL),
(97, 57, 3, 'N', NULL),
(98, 58, 3, 'N', NULL),
(99, 59, 9, 'S', NULL),
(100, 60, 3, 'N', NULL),
(101, 61, 9, 'S', NULL),
(102, 62, 3, 'S', NULL),
(103, 63, 3, 'S', NULL),
(104, 64, 3, 'S', NULL),
(105, 65, 9, 'S', NULL),
(106, 66, 9, 'S', NULL),
(107, 67, 3, 'N', NULL),
(108, 68, 9, 'S', NULL),
(109, 69, 9, 'S', NULL),
(110, 68, 3, 'N', NULL),
(111, 70, 9, 'N', NULL),
(112, 71, 9, 'S', NULL),
(113, 72, 3, 'S', NULL),
(114, 73, 3, 'N', NULL),
(115, 74, 3, 'N', NULL),
(116, 75, 3, 'N', NULL),
(117, 76, 3, 'N', NULL),
(118, 77, 3, 'N', NULL),
(119, 78, 3, 'N', NULL),
(120, 79, 3, 'N', NULL),
(121, 80, 3, 'N', NULL),
(122, 81, 3, 'N', NULL),
(123, 82, 3, 'N', NULL),
(124, 83, 3, 'N', NULL),
(125, 84, 3, 'N', NULL),
(126, 85, 3, 'S', NULL),
(127, 86, 3, 'S', NULL),
(128, 87, 3, 'N', NULL),
(129, 88, 3, 'S', NULL),
(130, 89, 3, 'S', NULL),
(131, 90, 3, 'N', NULL),
(132, 91, 3, 'S', NULL),
(133, 92, 3, 'N', NULL),
(134, 93, 9, 'S', NULL),
(135, 94, 9, 'S', NULL),
(136, 95, 9, 'S', NULL),
(137, 96, 9, 'S', NULL),
(138, 97, 9, 'S', NULL),
(139, 98, 9, 'S', NULL),
(140, 99, 9, 'S', NULL),
(141, 100, 3, 'S', NULL),
(142, 101, 9, 'S', NULL),
(143, 102, 9, 'S', NULL),
(144, 103, 9, 'S', NULL),
(145, 104, 9, 'S', NULL),
(146, 105, 9, 'S', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '2' COMMENT 'opciones=statusaccount',
  `nombre` varchar(200) NOT NULL COMMENT 'nombre',
  `apellido` varchar(200) NOT NULL,
  `foto` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `email` varchar(200) NOT NULL COMMENT 'email',
  `pass` varchar(200) NOT NULL COMMENT 'pass',
  `emailConfirmed` char(1) NOT NULL DEFAULT 'N' COMMENT 'activar',
  `fecha` date NOT NULL COMMENT 'hoy'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`userId`, `statusId`, `nombre`, `apellido`, `foto`, `email`, `pass`, `emailConfirmed`, `fecha`) VALUES
(3, 2, 'Eduardo ', 'Lopez', 'teaser-uhmay.jpg', 'elopez@junkyard.mx', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-05'),
(4, 2, 'Gabo Student', 'Student', NULL, 'gabofc@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-17'),
(5, 2, 'Gabo', 'T1', NULL, 'gabofc+t1@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-05'),
(6, 2, 'Gabo ', 'T2', NULL, 'gabofc+t2@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-01-05'),
(7, 2, 'Ismelda', 'Coco', NULL, 'coco@romanf.com', 'WDgyUmpZbjQwUVlLSVlkRzBMQkZ3dz09', 'S', '2021-01-08'),
(8, 2, 'John', 'Doe', NULL, 'johndoe@test.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'S', '2021-01-12'),
(9, 1, 'Thalia', 'Wilson', NULL, 'thalia-deactivated-2021-05-03@gmail.com', 'SG5POGY5amJ2QU1INVFzK2tHWWVOdz09', 'S', '2021-01-18'),
(30, 1, 'Eduardo Raymundo', 'Villagran', NULL, 'eduardo_raymundo-deactivated-2021-05-03@hotmail.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-13'),
(11, 1, 'David', 'Lindberg', NULL, 'david-deactivated-2021-05-03@innerworks.io', 'ckRpNTNmbkRmL243TEM4eGFON1MyQT09', 'S', '2021-03-01'),
(12, 1, 'bb2', 'bb2', NULL, 'test-deactivated-2021-05-03@me.com', 'cXZLWUs2a3V5aDF0aE9PT2xUUDQzdz09', 'S', '2021-01-19'),
(13, 1, 'Iván', 'Martin', '4880-327.jpg', 'martin.maseda-deactivated-2021-05-03@gmail.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(14, 1, 'Ivan test', 'Test', NULL, 'ivan-deactivated-2021-05-03@test.com', 'K0FkOTNuYzFqRjdSSW9SYUhRcmxudz09', 'S', '2021-01-22'),
(15, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-25'),
(16, 2, 'Nils', 'von Heijne 2', NULL, 'nils@worldofwisdom.io', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-01-26'),
(17, 2, 'EDUARDO', 'LOPEZ', NULL, 'prueba@estudiante.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-01-28'),
(18, 2, 'Roman', 'Fernandez', NULL, 'roman@ephata.me', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-01-28'),
(19, 1, 'Gabo', 'FC', NULL, 'gabofc+t100-deactivated-2021-05-03@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-02-09'),
(20, 2, 'Johnathan', 'Meade', NULL, 'john@innerworks.io', 'YW9WODhydnBMTThSVUhINyszWkkvZz09', 'S', '2021-03-03'),
(22, 2, 'Roman', 'Fernandez', NULL, 'rome@rome.net', 'ZUYyL2VzcmZMT1RLU01OTzhGOUtZUT09', 'S', '2021-03-04'),
(23, 2, 'john ', 'doe', NULL, 'babaji@juice.net', 'dWMwTEVJL1UzVnozUU5aM1hZWnBJQT09', 'S', '2021-03-04'),
(24, 2, 'StudentSanja', 'Menicanin', NULL, 'hello@sanjamenicanin.com', 'QVMyN2FZNlN4V2lMR0RLSnZzZTFyUT09', 'S', '2021-03-17'),
(25, 1, 'John', 'Doe', NULL, 'johndoe-deactivated-2021-05-03@email.com', 'bkpYdm05ZUJRaUJ5NkZ6bUVXbjdvUT09', 'S', '2021-03-26'),
(26, 1, 'Test', 'User', NULL, 'test-deactivated-2021-05-03@aprobado.com', 'YW4vMFRMQW45STFEUm1lb295YjBsdz09', 'S', '2021-04-09'),
(27, 2, 'Gabriel', 'Test Estudiante', NULL, 'gabofc+student_test1@gmail.com', 'MjcvWTFkK3p0eWkwaUl5Z25iRjZGZz09', 'S', '2021-04-11'),
(28, 2, 'Eduardo Raymundo', 'Villagran', NULL, 'lourdes.serrano.tun@gmail.com', 'Um0wYlVaNEZtRkd4OGFWWGQ2Mk1OUT09', 'S', '2021-04-12'),
(31, 2, 'test gabo ', 'gabo test', NULL, 'gabofc+testvalidation@gmail.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'S', '2021-04-14'),
(32, 2, 'Gabo 2', 'Gabo 2', NULL, 'web@neotegra.com', 'SkRrYkZqeGg3cDRTNkNrQWdXWFBLZz09', 'N', '2021-04-14'),
(33, 2, 'dasda', 'asdas', NULL, 'asdasda@asdadds.com', 'clJmM2xTUTcyVVNJTFlseEEwWm95U2lSdENyN1FOVmpVUG4wbFZ4SGhkcz0=', 'N', '2021-04-14'),
(34, 1, 'Test', 'User', NULL, 'rinz7-deactivated-2021-05-03@hotmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-14'),
(35, 1, 'Stephanie', 'Wilson', NULL, 'rome7f-deactivated-2021-05-03@gmail.com', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-04-30'),
(36, 2, 'eris', 'diel', NULL, 'eris@diel.com', 'ZFB2Skl4dmNldVlML0lWc1lGS2U4dz09', 'N', '2021-05-01'),
(37, 2, 'Gunilla', 'Rubin', NULL, 'gunilla.rubin@sweco.se', 'NnRtUnVjZTZ2NC9lRXlWeEhqaXY5Zz09', 'S', '2021-08-10'),
(38, 2, 'Anja', 'Thernström', NULL, 'anja.thernstrom@sweco.se', 'eWhuVGxIU1U5S2F0bUZ4RTV0cUFydz09', 'N', '2021-05-02'),
(39, 2, 'Cecilia ', 'Engvall', NULL, 'cissiande@hotmail.com', 'aStnelp2TFN5Y2owclBUS2paanFsUT09', 'N', '2021-05-02'),
(40, 2, 'Malin', 'Magnusson', NULL, 'malin.k.magnusson@gmail.com', 'VU55U3BSTmYxc1V1WFhnQjdWOERadz09', 'S', '2021-05-02'),
(41, 2, 'Dagmara', 'Fajkis-Lau', NULL, 'dagmara.fajkis@gmail.com', 'OXBFbGFrRVA5UE4wZUdLMWZ6ZDdjUT09', 'S', '2021-05-02'),
(42, 2, 'Moa', 'Andreasson Gistedt', NULL, 'moa.gistedt@gmail.com', 'NE5aTFUzV2xTSGxEczcvOHZtZkNSdz09', 'S', '2021-05-02'),
(43, 2, 'Theres', 'Kvarnström', NULL, 'theres.kvarnstrom@sweco.se', 'SEhreDc5V3dDWFJUMTR3YkZoZnpPUT09', 'S', '2021-05-03'),
(44, 2, 'Mari', 'Wennlund', NULL, 'mari.wennlund@sweco.se', 'S2pNeE9yU1RocmFHbzAwT2hXLzFMUT09', 'N', '2021-05-03'),
(45, 2, 'Peter', 'Westerberg', NULL, 'peterwesterberg2@gmail.com', 'VWViNklTZ0RBby9FOGJmTzlhWGhXdz09', 'S', '2021-05-03'),
(46, 2, 'Isa', 'Brisby', NULL, 'isa.brisby@sweco.se', 'djk3dXp4K3ozeW0xc3Mwd0hrTkhiQT09', 'S', '2021-05-03'),
(47, 2, 'Helena', 'Törmä', NULL, 'haetii@gmail.com', 'ZTkzbWVpS21SVW1WL2VEeTN5Q0R0ZkZONlA1Mm9JZjNvbXdjTk1SV3NDTT0=', 'S', '2021-05-04'),
(48, 2, 'Marie', 'Alke', NULL, 'marie.alke@hotmail.com', 'M1ZId3VVZUo3STFrcUVTQkRtUnljZz09', 'N', '2021-05-04'),
(49, 2, 'Caroline', 'Hansson', NULL, 'hansson_120@hotmail.com', 'cm1GeU9VTDZKbFB2QWdFbVcweXpKQT09', 'N', '2021-05-05'),
(50, 2, 'Niklas', 'Gripenstam', NULL, 'niklas.gripenstam@hotmail.com', 'L1NsY1dyWDdIcFBBeHhjWDRnamE5dz09', 'S', '2021-05-05'),
(51, 2, 'Helia', 'Tehranchi', NULL, 'helia.tehranchi@sweco.se', 'MU1XVkU3QVhvSnlZa0Jlc2JlRVIzUT09', 'S', '2021-05-05'),
(52, 2, 'Sofia ', 'Gärde', NULL, 'sofia.m.garde@gmail.com', 'NWFyY1hWdTFXQnVPQUJ6eW9wdTJhdz09', 'S', '2021-05-05'),
(53, 2, 'Monika', 'Harvey', NULL, 'monika.harvey@hotmail.se', 'VUFEMzhSVlZJbEk4eXJBRXZWVVJBZz09', 'N', '2021-05-07'),
(54, 2, 'Monika', 'Harvey', NULL, 'monika.harvey@hotmail.com', 'T0tsSTEyT0JGT1lQY1RCclFtMjZhZz09', 'S', '2021-05-07'),
(55, 2, 'Maria', 'Strannelind', NULL, 'miastrannelind@hotmail.com', 'WmZMM0V6dG9IeDJla2pXN2tSWExXUT09', 'S', '2021-05-11'),
(56, 2, 'caroline', 'hansson', NULL, 'caroline.hansson@sweco.se', 'cm1GeU9VTDZKbFB2QWdFbVcweXpKQT09', 'S', '2021-05-12'),
(57, 2, 'Anna', 'Hanerud', NULL, 'anna.hanerud@gmail.com', 'NkdrVXdvbnR4N3JRZUhrQWtLY3JKZz09', 'S', '2021-05-19'),
(58, 2, 'Vincent', 'Guntrum', NULL, 'vincent.guntrum@posteo.de', 'dkppclBCUlFaNi9MSUNXZVV0a2lndz09', 'S', '2021-05-21'),
(59, 2, 'Switchable', 'compelling', NULL, 'Judah22@yahoo.com', 'VnBhbGhaUXJIcjU3WTVMVDh4Q3JTdz09', 'N', '2021-05-26'),
(60, 2, 'Rainer', 'von Leoprechting', 'rainer20201629630997.jpg', 'rainer@fraendi.org', 'Wkx2SWZyTnhzbklFU1luaFhlcmJQdz09', 'S', '2021-08-22'),
(61, 2, 'actuating', 'frame', NULL, 'konradinio@gmail.com', 'SWl1L092UUdQa0JJdWxLL05GVkFIUT09', 'N', '2021-06-04'),
(62, 2, 'Gabriela', 'Dolejsi', NULL, 'g.dolejsi@gmail.com', 'RE9CTWUwbWxUQ3B5THNEUm5kcUE3Zz09', 'N', '2021-06-16'),
(63, 2, 'Marit', 'Lissdaniels', NULL, 'marit.lissdaniels@gmail.com', 'WmUzRjZ2NzRJblVFYWNwQnFTbXpHUT09', 'N', '2021-06-16'),
(64, 2, 'Fredrik', 'Bränström', NULL, 'branstrom@gmail.com', 'SWgxOG5KR3YxckRtTjF3OEpKUnNKK3BBb1FuaEI1UkFSSi9Nb3RpaHdxcz0=', 'N', '2021-06-16'),
(65, 2, 'scale', 'Product', NULL, 'marius_stratulat@yahoo.com', 'VVF6K3FSQjVLQ3BnOUVRdEVEd0pZdz09', 'N', '2021-06-27'),
(66, 2, 'RAM', 'Mandatory', NULL, 'ute.dieter.jungclaus@t-online.de', 'RDBaY0FqKzBOSUxnSXd6bG9KclNDUT09', 'N', '2021-07-13'),
(67, 2, 'Vipin', 'Paul', NULL, 'vipin@braintechnosys.com', 'ZGxDcUdMMHE0NGJONE1LdG45Q203QT09', 'S', '2021-07-19'),
(68, 2, 'Rachana', 'Dhakern', '1630078059.jpg', 'rachana@braintechnosys.com', 'NDVKei9uaUkvU1M0cnRJVmJmWXo3UT09', 'S', '2021-07-27'),
(69, 2, 'SMS', 'deposit', NULL, 'jebbmanning@hotmail.com', 'eHNKVTZLNXVack1tYURBbm52c3crQT09', 'N', '2021-08-03'),
(70, 2, 'Evolvia', 'Test', NULL, 'roman@evolvia.mx', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-08-15'),
(71, 2, 'incubate', 'Frozen', NULL, 'barreraruperto13@gmail.com', 'blR0N0VoVklXblppYm5OQnJlSHFYQT09', 'N', '2021-08-18'),
(72, 2, 'Bernadette', 'Wesley', 'fraendi-logo21635511497.png', 'roman@innerworks.io', 'U1d4NDJGQmwrRWpjNVF0S2NFVmNFUT09', 'S', '2021-10-29'),
(73, 2, 'Nils', 'Test', NULL, 'nils@svh.vc', 'OUFYRzJQT081NDBPQzBtYWtEOUlpUT09', 'S', '2021-08-29'),
(74, 2, ' Myriel', 'Walter', NULL, 'Myriel.Walter@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(75, 2, 'Raquel', 'Corredoira', NULL, 'Raquel.Corredoira@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(76, 2, 'Adrien', 'Geenens', NULL, 'Adrien.Geenens@sircleclub.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(77, 2, 'Jana', 'Frank', NULL, 'Jana.Frank@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(78, 2, 'Manon', 'Majoor', NULL, 'Manon.Majoor@siradamhotel.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(79, 2, 'Emily', 'Orth', NULL, 'emily.orth@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(80, 2, 'Joe', 'Woods', NULL, 'J.Woods@siradamhotel.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(81, 2, 'Juliet', 'Piersma', NULL, 'juliet.piersma@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(82, 2, 'Lauren', 'Smit', NULL, 'Lauren.Smit@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(83, 2, 'Laura', 'Nolte', NULL, 'laura.nolte@sirclecollection.com', 'cHVKM25ka1hOVEkrV0dvQWhHR0xVQT09', 'S', '2021-08-29'),
(84, 2, 'Amit', 'Paul 2', NULL, 'amitpaul@mac.com', 'c0tjaitXQ21pMXpFVExIekdaS0lhUT09', 'S', '2021-08-30'),
(85, 2, 'Servant', 'Conscious', NULL, 'servant@consciousmission.com', 'TG1nUVF6RDZCWk8wTXd6VFZNMFNZQT09', 'N', '2021-08-30'),
(86, 2, 'Steven', 'White', NULL, 'elenchikos@gmail.com', 'b2owQUtmbVg5RG95ZXR4MCt5OStndz09', 'N', '2021-08-31'),
(87, 2, 'Kaa', 'Faensen', NULL, 'kaa@fraendi.org', 'SHFuanJPdHl1blZwMkxpdUgrbmxiUT09', 'S', '2021-08-31'),
(88, 2, 'Sepp', 'Kirchengast', NULL, 'sepp@fraendi.org', 'VUtRUVdSS0ltbDIwMU96RFMxd2s4Zz09', 'N', '2021-08-31'),
(89, 2, 'Hendrik', 'Berberich', NULL, 'hendrik@fraendi.org', 'Zm5JWlM1eGVVSGw2TFdIdU4wYzBmdz09', 'N', '2021-08-31'),
(90, 2, 'Chris', 'Chapman', NULL, 'chrischapmaninireland@gmail.com', 'NDJ3cFh6V3Y5TTFERFo1R2ZHbkRZdz09', 'S', '2021-08-31'),
(91, 2, 'Bernhard', 'Possert', NULL, 'bernhard@possert.at', 'RlpEOFQ0cDcxYlVWaW1GL2pwdU9Jdz09', 'N', '2021-08-31'),
(92, 2, 'Jeroen', 'Vermeer', '1630673170.jpeg', 'jeroen@fraendi.org', 'dHdYb3dKUGRKSFI3dmV2UGthdFFCUT09', 'S', '2021-08-31'),
(93, 2, 'Buckinghamshire', 'Incredible', NULL, 'info@koucingopaslaugos.lt', 'S1ZRWFQzMTNUbFhva1NCYTA2S04yQT09', 'N', '2021-09-10'),
(94, 2, 'Nepal', 'Associate', NULL, 'waltandcori@gmail.com', 'Rkd5bjdxZlhqczhCZGcybmZiTnZ6dz09', 'N', '2021-09-11'),
(95, 2, 'Baby', 'Home Loan Account', NULL, 'scottieb@sympatico.ca', 'N1dEWHozVVlHNzRnNFRHNXo2eGJkUT09', 'N', '2021-09-15'),
(96, 2, 'Investment', 'Keyboard', NULL, 'stacyglover@live.ca', 'UTFZY25kRzFZNlBMNHVOUlgvWWs1Zz09', 'N', '2021-09-15'),
(98, 2, 'Rachana', 'Dhaker', NULL, 'errachana718@gmail.com', 'NDVKei9uaUkvU1M0cnRJVmJmWXo3UT09', 'N', '2021-09-16'),
(99, 2, 'Lead', 'Awesome', NULL, 'lynette.holliday@aol.com', 'NitMN3RrZWxqSUlHb0dRVGY2NUlXZz09', 'N', '2021-09-16'),
(100, 2, 'Adam', 'McKenty', NULL, 'adam@photosynthesis.ca', 'MzVMMDJPcFRXZkVaZk8xM0gzL2tZUT09', 'S', '2021-09-22'),
(101, 2, 'e-business', 'Account', NULL, 'et2710@aol.com', 'NS9mK2VJdmp0STJsYk05Y01SNzVKQT09', 'N', '2021-10-18'),
(102, 2, 'Borders', 'syndicate', NULL, 'mjgrady11@gmail.com', 'cVBDVXVxUVVvREVBbFRINHU2MEMrZz09', 'N', '2021-10-19'),
(103, 2, 'Rubber', 'Wooden', NULL, 'merlinbbs@yahoo.com', 'd3ZBK3NHelk4ZHJ5T3FyejJtVW1EZz09', 'N', '2021-10-24'),
(104, 2, 'Buckinghamshire', 'RAM', NULL, 'alfonsogomez575@gmail.com', 'UExOcmhnRXBBQ3VuQ2p1VFFwQUhmUT09', 'N', '2021-10-29'),
(105, 2, 'Graphic', 'generation', NULL, 'coxecqzot@yahoo.com', 'RW8wTXlPRzNwR1hmeGpHTDBZeHdDQT09', 'N', '2021-11-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userserie`
--

CREATE TABLE `userserie` (
  `userSerieId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `serieId` int(11) NOT NULL COMMENT 'opciones=series'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `userserie`
--

INSERT INTO `userserie` (`userSerieId`, `userId`, `serieId`) VALUES
(1, 3, 20),
(54, 92, 33),
(7, 16, 32),
(53, 91, 33),
(52, 90, 33),
(51, 89, 33),
(50, 88, 33),
(49, 87, 33),
(48, 86, 33),
(47, 73, 33),
(46, 60, 33),
(58, 73, 31),
(57, 3, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uservideo`
--

CREATE TABLE `uservideo` (
  `userVideoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `uservideo`
--

INSERT INTO `uservideo` (`userVideoId`, `userId`, `videoId`) VALUES
(260, 92, 56),
(259, 91, 56),
(258, 90, 56),
(257, 89, 56),
(256, 88, 56),
(255, 87, 56),
(254, 86, 56),
(253, 73, 56),
(252, 60, 56),
(221, 83, 55),
(269, 92, 58),
(268, 91, 58),
(267, 90, 58),
(266, 89, 58),
(265, 88, 58),
(264, 87, 58),
(263, 86, 58),
(262, 73, 58),
(261, 60, 58),
(241, 83, 61),
(280, 3, 31),
(420, 83, 52),
(419, 82, 52),
(418, 81, 52),
(417, 80, 52),
(416, 79, 52),
(415, 78, 52),
(414, 77, 52),
(413, 76, 52),
(412, 75, 52),
(411, 74, 52),
(430, 83, 57),
(429, 82, 57),
(428, 81, 57),
(427, 80, 57),
(426, 79, 57),
(425, 78, 57),
(424, 77, 57),
(423, 76, 57),
(422, 75, 57),
(421, 74, 57),
(470, 83, 54),
(469, 82, 54),
(468, 81, 54),
(467, 80, 54),
(466, 79, 54),
(465, 78, 54),
(464, 77, 54),
(463, 76, 54),
(462, 75, 54),
(461, 74, 54),
(220, 82, 55),
(219, 81, 55),
(218, 80, 55),
(217, 79, 55),
(216, 78, 55),
(215, 77, 55),
(214, 76, 55),
(213, 75, 55),
(212, 74, 55),
(440, 83, 59),
(439, 82, 59),
(438, 81, 59),
(437, 80, 59),
(436, 79, 59),
(435, 78, 59),
(434, 77, 59),
(433, 76, 59),
(432, 75, 59),
(431, 74, 59),
(240, 82, 61),
(239, 81, 61),
(238, 80, 61),
(237, 79, 61),
(236, 78, 61),
(235, 77, 61),
(234, 76, 61),
(233, 75, 61),
(232, 74, 61),
(480, 83, 63),
(479, 82, 63),
(478, 81, 63),
(477, 80, 63),
(476, 79, 63),
(475, 78, 63),
(474, 77, 63),
(473, 76, 63),
(472, 75, 63),
(471, 74, 63),
(510, 83, 60),
(509, 82, 60),
(508, 81, 60),
(507, 80, 60),
(506, 79, 60),
(505, 78, 60),
(504, 77, 60),
(503, 76, 60),
(502, 75, 60),
(501, 74, 60),
(520, 83, 62),
(519, 82, 62),
(518, 81, 62),
(517, 80, 62),
(516, 79, 62),
(515, 78, 62),
(514, 77, 62),
(513, 76, 62),
(512, 75, 62),
(511, 74, 62),
(530, 83, 64),
(529, 82, 64),
(528, 81, 64),
(527, 80, 64),
(526, 79, 64),
(525, 78, 64),
(524, 77, 64),
(523, 76, 64),
(522, 75, 64),
(521, 74, 64),
(410, 83, 65),
(409, 82, 65),
(408, 81, 65),
(407, 80, 65),
(406, 79, 65),
(405, 78, 65),
(404, 77, 65),
(403, 76, 65),
(402, 75, 65),
(401, 74, 65),
(490, 83, 67),
(489, 82, 67),
(488, 81, 67),
(487, 80, 67),
(486, 79, 67),
(485, 78, 67),
(484, 77, 67),
(483, 76, 67),
(482, 75, 67),
(481, 74, 67),
(540, 83, 69),
(539, 82, 69),
(538, 81, 69),
(537, 80, 69),
(536, 79, 69),
(535, 78, 69),
(534, 77, 69),
(533, 76, 69),
(532, 75, 69),
(531, 74, 69),
(341, 74, 70),
(342, 75, 70),
(343, 76, 70),
(344, 77, 70),
(345, 78, 70),
(346, 79, 70),
(347, 80, 70),
(348, 81, 70),
(349, 82, 70),
(350, 83, 70),
(450, 83, 66),
(449, 82, 66),
(448, 81, 66),
(447, 80, 66),
(446, 79, 66),
(445, 78, 66),
(444, 77, 66),
(443, 76, 66),
(442, 75, 66),
(441, 74, 66),
(361, 74, 71),
(362, 75, 71),
(363, 76, 71),
(364, 77, 71),
(365, 78, 71),
(366, 79, 71),
(367, 80, 71),
(368, 81, 71),
(369, 82, 71),
(370, 83, 71),
(550, 83, 72),
(549, 82, 72),
(548, 81, 72),
(547, 80, 72),
(546, 79, 72),
(545, 78, 72),
(544, 77, 72),
(543, 76, 72),
(542, 75, 72),
(541, 74, 72),
(460, 83, 68),
(459, 82, 68),
(458, 81, 68),
(457, 80, 68),
(456, 79, 68),
(455, 78, 68),
(454, 77, 68),
(453, 76, 68),
(452, 75, 68),
(451, 74, 68),
(491, 74, 73),
(492, 75, 73),
(493, 76, 73),
(494, 77, 73),
(495, 78, 73),
(496, 79, 73),
(497, 80, 73),
(498, 81, 73),
(499, 82, 73),
(500, 83, 73),
(551, 74, 75),
(552, 75, 75),
(553, 76, 75),
(554, 77, 75),
(555, 78, 75),
(556, 79, 75),
(557, 80, 75),
(558, 81, 75),
(559, 82, 75),
(560, 83, 75),
(561, 74, 74),
(562, 75, 74),
(563, 76, 74),
(564, 77, 74),
(565, 78, 74),
(566, 79, 74),
(567, 80, 74),
(568, 81, 74),
(569, 82, 74),
(570, 83, 74),
(571, 74, 76),
(572, 75, 76),
(573, 76, 76),
(574, 77, 76),
(575, 78, 76),
(576, 79, 76),
(577, 80, 76),
(578, 81, 76),
(579, 82, 76),
(580, 83, 76);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocategories`
--

CREATE TABLE `videocategories` (
  `vcId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `categoryId` int(11) NOT NULL COMMENT 'opciones=categories'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videocategories`
--

INSERT INTO `videocategories` (`vcId`, `videoId`, `categoryId`) VALUES
(166, 52, 2),
(167, 57, 2),
(179, 64, 2),
(171, 54, 2),
(112, 56, 4),
(75, 55, 2),
(113, 58, 4),
(172, 63, 2),
(168, 59, 2),
(177, 60, 2),
(77, 61, 2),
(115, 31, 2),
(127, 20, 4),
(126, 20, 2),
(125, 20, 1),
(130, 21, 4),
(129, 21, 2),
(128, 21, 1),
(133, 22, 4),
(132, 22, 2),
(131, 22, 1),
(136, 23, 4),
(135, 23, 2),
(134, 23, 1),
(139, 24, 4),
(138, 24, 2),
(137, 24, 1),
(142, 25, 4),
(141, 25, 2),
(140, 25, 1),
(145, 26, 4),
(144, 26, 2),
(143, 26, 1),
(148, 27, 4),
(147, 27, 2),
(146, 27, 1),
(151, 28, 4),
(150, 28, 2),
(149, 28, 1),
(154, 29, 4),
(153, 29, 2),
(152, 29, 1),
(110, 30, 4),
(109, 30, 2),
(108, 30, 1),
(178, 62, 2),
(165, 65, 2),
(169, 66, 2),
(173, 67, 2),
(170, 68, 2),
(180, 69, 2),
(159, 70, 2),
(161, 71, 2),
(181, 72, 2),
(175, 73, 2),
(183, 74, 2),
(182, 75, 2),
(184, 76, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videocircle`
--

CREATE TABLE `videocircle` (
  `videoCircleId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos',
  `circleId` int(11) NOT NULL COMMENT 'opciones=circles'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videorequest`
--

CREATE TABLE `videorequest` (
  `requestVideoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL COMMENT 'opciones=users',
  `videoId` int(11) NOT NULL COMMENT 'opciones=videos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `videoId` int(11) NOT NULL COMMENT 'multiple=users,uservideo;multiple=coaches,coachvideo;multiple=categories,videocategories;multiple=circles,videocircle;',
  `clientId` int(11) NOT NULL COMMENT 'opciones=clients',
  `coachId` int(11) NOT NULL COMMENT 'opciones=coaches',
  `video` varchar(200) DEFAULT NULL COMMENT 'spotlightr',
  `titulo` varchar(200) NOT NULL COMMENT 'nombre',
  `imagen` varchar(200) DEFAULT NULL COMMENT 'archivo',
  `description` varchar(1000) NOT NULL,
  `accessId` int(11) NOT NULL DEFAULT '3' COMMENT 'opciones=access',
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`videoId`, `clientId`, `coachId`, `video`, `titulo`, `imagen`, `description`, `accessId`, `orden`) VALUES
(31, 3, 6, 'MTA3NDU2NA==', 'Life as a gift or a struggle', 'sanna_glad.jpg', 'Are you the doer or is life really happening without any effort? Try this meditation and see for your self. ', 2, 0),
(20, 3, 1, 'MTA3MTU0NQ==', 'The Loop 1: Understanding the change you\'re going through', 'skärmavbild-2021-02-02-kl-163034.png', 'Introduction to The Loop as a core pattern of how change plays out in life, how to master the change process and manifest the change you need.', 2, 1),
(15, 4, 18, 'MTA2ODg2Mg==', 'Intro to Hero\'s Journey', 'heros-journey-thumbnail.jpg', 'Hero\'s journey is a workshop that takes children on a journey to understanding fear and building courage. It utilizes basic language and comprehension skills and builds on lessons learned at school with fun exercises.', 3, 0),
(18, 4, 18, 'MTA3MDk3Nw==', 'Meet Valeria', 'valeria_profile_square2.jpg', 'Meet Valeria, the co-founder of Ephata and get to know her story that led to building schools and complementary educational curriculums for children.', 3, 0),
(16, 4, 18, 'MTA2ODg5OA==', '7 Pillars of Ephata', 'ephata-7-pillarspng.png', 'This short video explains the 7 pillars of Ephata and how they come together to guide children towards a path of faith, wisdom and goodness.', 3, 0),
(17, 4, 18, 'MTA2ODkzOA==', 'What is Ephata?', 'tlm121720kg-19.jpg', 'We learn about the story of Ephata and how it can nurture children in many ways beyond home and school.', 3, 0),
(21, 3, 1, 'MTA3MTU0OA==', 'The Loop 2: Find your starting point', 'skärmavbild-2021-02-02-kl-164639.png', 'Explore who you are in this moment of your life, and find the starting point of your current transformation loop.\n\nHave pen and paper ready!', 2, 2),
(22, 3, 1, 'MTA3MTU2MQ==', 'The Loop 3: Find your shadow', 'skärmavbild-2021-02-02-kl-165002.png', 'Explore your shadow by mapping your psychological pain, anger, anxiety and other challenging emotions in your life.', 2, 3),
(23, 3, 1, 'MTA3MTU2NQ==', 'The Loop 4: Embrace your shadow', 'skärmavbild-2021-02-02-kl-165152.png', 'Instructions for shadow breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 2, 4),
(24, 3, 1, 'MTA3MTU3Mw==', 'The Loop 5: Find your light', 'skärmavbild-2021-02-02-kl-165320.png', 'Connect with your inner light and map your pride, passion and curiosity.', 2, 5),
(25, 3, 1, 'MTA3MTU4Mw==', 'The Loop 6: Embrace your light', 'skärmavbild-2021-02-02-kl-165432.png', 'Visualisation meditation to connect with your path of light', 2, 6),
(26, 3, 1, 'MTA3MTU4Nw==', 'The Loop 7: Find your challenge', 'skärmavbild-2021-02-02-kl-165529.png', 'Explore what is holding you back from changing, and map your fears and energy drainers', 2, 7),
(27, 3, 1, 'MTA3MTU5Mg==', 'The Loop 8: Embrace your challenge', 'skärmavbild-2021-02-02-kl-165635.png', 'Instructions for Hero\'s breathwork. This session is also offered as an optional live experience. \n\nPlease read the safety guidelines first: http://tiny.cc/breathesafely\n\nPlaylist to shuffle: http://tiny.cc/innerworksplaylist', 2, 8),
(28, 3, 1, 'MTA3MTU5NA==', 'The Loop 9: Manifest the change you need', 'skärmavbild-2021-02-02-kl-165748.png', 'Step into manifestation and transform your life. Link to your agreement: https://tiny.cc/myagreement', 2, 9),
(29, 3, 1, 'MTA3MTU5NQ==', 'The Loop 10: Integration and follow up', 'skärmavbild-2021-02-02-kl-165846.png', 'Closing integration session, also offered as an optional live experience', 2, 10),
(30, 3, 1, 'MTA3MTY1NA==', 'BONUS: 8D sound healing', 'skärmavbild-2021-02-02-kl-170013.png', 'As a little bonus to celebrate your completion of The Loop, we\'re giving you a sound healing session in 8D. Bring your headphones!', 2, 11),
(65, 3, 1, 'MTE2MjYzMw==', 'Meditation: Grounding Triad', 'iw-covers0011633283546.jpeg', 'This meditation invites you to ground yourself, calm your central nervous system and silence your mind, by combining boxed breathing, sounding and focusing. Feel free to take some time after the meditation to write down any insights or sensations.', 2, NULL),
(52, 3, 1, 'MTE1NTgyNQ==', 'Meditation: Body Scan', 'iw-covers0021633283574.jpeg', 'A short meditation to relax and connect with your body, while deepening your understanding of where you\'re holding opportunity to release tensions and step into your power. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL),
(57, 3, 1, 'MTE1ODU5Nw==', 'Meditation: Grateful Past & Hopeful Future', 'iw-covers0031633283589.jpeg', 'A short meditation for exploring what we value in life and where we wish to go next. The intention is to connect with gratitude and give ourselves a sense of direction. Rooted in neuroplasticity and the science of gratitude and intention. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL),
(54, 3, 1, 'MTE1NTgyOQ==', 'Tool: Anchoring Yourself', 'iw-covers-20011633284151.jpeg', 'This session offers simple guidance to find your own anchor for dealing with challenging, stressful or uncomfortable situations in life. This practice is rooted in Somatic Experiencing and NLP coaching.', 2, NULL),
(56, 3, 55, 'MTE1NzgyNw==', 'Reg Revans talks about Action Learning', 'fraendi_logo_ohne_rand-removebg1630509102.png', 'Fraendi will provide this description and a thumbnail picture to update shortly', 2, NULL),
(55, 3, 1, 'MTE1NjIxMw==', 'Welcome to Reclaim Your Flow', 'skärmavbild-2021-09-07-kl-2135351631485358.png', 'This is a brief welcoming message from your guides Nils and Mattis.', 2, NULL),
(58, 3, 55, 'MTE1ODY3OA==', 'Action Learning Training 1', 'fraendi_logo_ohne_rand-removebg1630647505.png', 'Description to be updated', 2, NULL),
(59, 3, 1, 'MTE1OTMzNQ==', 'Tool: Embracing Your Inner Critic', 'iw-covers0111633283625.jpeg', 'Voicing your inner critic is a tool for acknowledging, embracing and exploring any judgmental inner narratives that might be limiting you. This practice is rooted in NLP coaching. Make sure you have pen and paper ready for taking notes.', 2, NULL),
(60, 3, 6, 'MTE1OTM0Mg==', 'Meditation: Inner Child & Elder', 'iw-covers0041633285081.jpeg', 'A meditation inviting you to connect with your inner child and inner elder. This is a practice of deepening self-understanding and connecting with your underlying needs. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL),
(61, 3, 1, 'MTE1OTQ3NA==', 'Live Session Recording: Sept 8, 2021', 'video-frames-iw0011631485422.jpeg', 'This is a recording of the Sircle Collection live session from Sept 8th 2021.', 2, NULL),
(62, 3, 6, 'MTE1OTkwNQ==', 'Meditation: Inner Best Friend', 'iw-covers0051633285105.jpeg', 'A brief meditation inviting you to explore yourself through the concept of self-friendship.', 2, NULL),
(63, 3, 1, 'MTE2MDE2OQ==', 'Tool: Reversing the Movement', 'iw-covers-20031633284169.jpeg', 'This is a simple tool for grounding yourself in a challenging or stressful situation. This practice is commonly used by athletes and performers, and is grounded in NLP coaching.', 2, NULL),
(64, 3, 6, 'MTE2MDE3MA==', 'Meditation: Grounding in Being', 'iw-covers0061633285124.jpeg', 'This guided meditation invites you to ground yourself simply by being. Feel free to take a few moments after the meditation to write down any thoughts, sensations or insights.', 2, NULL),
(66, 3, 1, 'MTE2MjY2MA==', 'Meditation: Shedding Your Layers', 'iw-covers0101633283682.jpeg', 'A meditation inviting you to explore who you are without your layers of identity. Feel free to take a moment after the meditation to write down any reflections or insights.\n', 2, NULL),
(67, 3, 1, 'MTE2MjY2NQ==', 'Tool: Self Inquiry', 'iw-covers-20021633284185.jpeg', 'This is a short exercise in using self inquiry to explore any thought patterns and beliefs that might be limiting you. This practice is rooted in The Work by Byron Katie. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL),
(68, 3, 1, 'MTE2MjcwMA==', 'Meditation: Purpose Walk', 'iw-covers0071633283708.jpeg', 'This meditation invites you to explore, and connect with, your personal path and purpose in this life. This is a practice rooted in NLP coaching. Feel free to take a moment after the meditation to write down any reflections or insights.', 2, NULL),
(69, 3, 6, 'MTE2Mjg2MQ==', 'Meditation: Tiny Sensations', 'iw-covers0081633285148.jpeg', 'This grounding meditation invites you to notice the tiny sensations within you and around you. Feel free to take a few moments after the meditation to write down any reflections or insights.', 2, NULL),
(70, 3, 1, 'MTE2NDgzOQ==', 'Live Session Recording: Sept 22, 2021', 'sircle-0011632419045.jpeg', 'A recording of the Sircle Collection live session, hosted on Sept. 22, 2021.', 2, NULL),
(71, 3, 1, 'MTE2NTQyNA==', 'Meditation: I DO WE BE', 'idowebe0011632682842.jpeg', 'This is a very simple journaling meditation and tool for self-leadership and sensing into your short-term direction. Have pen and paper handy if you can. Feel free to take a few moments after the meditation and notice any thoughts or insights emerging.', 2, NULL),
(72, 3, 6, 'MTE2NTQ1Mw==', 'Tool: Begin With the End', 'iw-covers0091633285183.jpeg', 'This is a simple exercise allowing you to connect with your inner sense of direction in life and manifest your path forward. You\'ll need pen and paper. The exercise is rooted in NLP coaching. Feel free to take a few moments after the exercise to write down any thoughts or insights.', 2, NULL),
(73, 3, 1, 'MTE3NjI2NA==', 'Introduction: My Agreement with Myself', 'iw-covers0141633284521.jpeg', 'This is a brief introduction to the exercise of writing an agreement with yourself. This practice is rooted in NLP coaching and focuses on setting intentions and making commitments for your path ahead. \n\nYou will need at about 30 minutes to complete the agreement, which is available on the following link: https://tiny.cc/myagreement ', 2, NULL),
(74, 3, 1, 'MTE3NjI2Ng==', 'Tool: Lifelining', 'iw-covers0121633284961.jpeg', 'This tool invites you to experience different future paths in your life. Before doing this exercise, make sure you have a clear insight, intention or commitment for how you wish move forward in your life, or a specific change you wish to make. This exercise is rooted in NLP coaching. Feel free to take a moment after the exercise to write down any thoughts or insights.', 2, NULL),
(75, 3, 6, 'MTE3NjI2OA==', 'Meditation: Open Eyes', 'iw-covers0131633285433.jpeg', 'This is a meditation inviting you to lead yourself in life by inviting meditation into any moment of your everyday life. ', 2, NULL),
(76, 3, 1, 'MTE3NzMzNA==', 'Live Session Recording, Oct 6 2021', 'iw-live0011633683302.jpeg', 'Recording of the Sircle Collection live session on Oct 6, 2021', 2, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  ADD PRIMARY KEY (`accessId`);

--
-- Indices de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  ADD PRIMARY KEY (`accessUserId`);

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answerId`);

--
-- Indices de la tabla `appearance`
--
ALTER TABLE `appearance`
  ADD PRIMARY KEY (`appearanceId`);

--
-- Indices de la tabla `articlecircle`
--
ALTER TABLE `articlecircle`
  ADD PRIMARY KEY (`articleCircleId`);

--
-- Indices de la tabla `articlerequest`
--
ALTER TABLE `articlerequest`
  ADD PRIMARY KEY (`requestArticleId`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`);

--
-- Indices de la tabla `articleseriecircle`
--
ALTER TABLE `articleseriecircle`
  ADD PRIMARY KEY (`serieArticlesCircleId`);

--
-- Indices de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `audiocategories`
--
ALTER TABLE `audiocategories`
  ADD PRIMARY KEY (`acId`);

--
-- Indices de la tabla `audiocircle`
--
ALTER TABLE `audiocircle`
  ADD PRIMARY KEY (`audioCircleId`);

--
-- Indices de la tabla `audiorequest`
--
ALTER TABLE `audiorequest`
  ADD PRIMARY KEY (`requestAudioId`);

--
-- Indices de la tabla `audios`
--
ALTER TABLE `audios`
  ADD PRIMARY KEY (`audioId`);

--
-- Indices de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indices de la tabla `callcategories`
--
ALTER TABLE `callcategories`
  ADD PRIMARY KEY (`ccId`);

--
-- Indices de la tabla `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`callId`);

--
-- Indices de la tabla `callscircle`
--
ALTER TABLE `callscircle`
  ADD PRIMARY KEY (`callCircleId`);

--
-- Indices de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  ADD PRIMARY KEY (`cambioId`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `circlecategories`
--
ALTER TABLE `circlecategories`
  ADD PRIMARY KEY (`ccId`);

--
-- Indices de la tabla `circlecoachrequest`
--
ALTER TABLE `circlecoachrequest`
  ADD PRIMARY KEY (`requestCircleId`);

--
-- Indices de la tabla `circlerequest`
--
ALTER TABLE `circlerequest`
  ADD PRIMARY KEY (`requestCircleId`);

--
-- Indices de la tabla `circles`
--
ALTER TABLE `circles`
  ADD PRIMARY KEY (`circleId`);

--
-- Indices de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  ADD PRIMARY KEY (`clientPageId`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientId`);

--
-- Indices de la tabla `coacharticle`
--
ALTER TABLE `coacharticle`
  ADD PRIMARY KEY (`coachArticleId`);

--
-- Indices de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `coachaudio`
--
ALTER TABLE `coachaudio`
  ADD PRIMARY KEY (`coachAudioId`);

--
-- Indices de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  ADD PRIMARY KEY (`coachCallId`);

--
-- Indices de la tabla `coachcircle`
--
ALTER TABLE `coachcircle`
  ADD PRIMARY KEY (`coachCircleId`);

--
-- Indices de la tabla `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`coachId`);

--
-- Indices de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  ADD PRIMARY KEY (`coachClientId`);

--
-- Indices de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  ADD PRIMARY KEY (`coachSerieId`);

--
-- Indices de la tabla `coachvideo`
--
ALTER TABLE `coachvideo`
  ADD PRIMARY KEY (`coachVideoId`);

--
-- Indices de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`colorId`);

--
-- Indices de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  ADD PRIMARY KEY (`colorClientId`);

--
-- Indices de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  ADD PRIMARY KEY (`iddiccionario`);

--
-- Indices de la tabla `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`emailId`);

--
-- Indices de la tabla `excluded`
--
ALTER TABLE `excluded`
  ADD PRIMARY KEY (`excludedId`);

--
-- Indices de la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`imageId`);

--
-- Indices de la tabla `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`generalId`);

--
-- Indices de la tabla `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`homeId`);

--
-- Indices de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  ADD PRIMARY KEY (`homePlatformId`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`levelId`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`);

--
-- Indices de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  ADD PRIMARY KEY (`passId`);

--
-- Indices de la tabla `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`platformId`);

--
-- Indices de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  ADD PRIMARY KEY (`psId`);

--
-- Indices de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`postId`);

--
-- Indices de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`questionId`);

--
-- Indices de la tabla `seriecircle`
--
ALTER TABLE `seriecircle`
  ADD PRIMARY KEY (`serieCircleId`);

--
-- Indices de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  ADD PRIMARY KEY (`requestSerieId`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`serieId`);

--
-- Indices de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  ADD PRIMARY KEY (`scId`);

--
-- Indices de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  ADD PRIMARY KEY (`catalogId`);

--
-- Indices de la tabla `sessionrecording`
--
ALTER TABLE `sessionrecording`
  ADD PRIMARY KEY (`sessionRecordingId`);

--
-- Indices de la tabla `shala`
--
ALTER TABLE `shala`
  ADD PRIMARY KEY (`shalaId`);

--
-- Indices de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  ADD PRIMARY KEY (`slideId`);

--
-- Indices de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  ADD PRIMARY KEY (`statusId`);

--
-- Indices de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  ADD PRIMARY KEY (`tipoId`);

--
-- Indices de la tabla `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`todoId`);

--
-- Indices de la tabla `typemail`
--
ALTER TABLE `typemail`
  ADD PRIMARY KEY (`typeId`);

--
-- Indices de la tabla `userarticle`
--
ALTER TABLE `userarticle`
  ADD PRIMARY KEY (`userArticleId`);

--
-- Indices de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `useraudio`
--
ALTER TABLE `useraudio`
  ADD PRIMARY KEY (`userAudioId`);

--
-- Indices de la tabla `usercall`
--
ALTER TABLE `usercall`
  ADD PRIMARY KEY (`userCallId`);

--
-- Indices de la tabla `usercircle`
--
ALTER TABLE `usercircle`
  ADD PRIMARY KEY (`userCircleId`);

--
-- Indices de la tabla `userclient`
--
ALTER TABLE `userclient`
  ADD PRIMARY KEY (`userClientId`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indices de la tabla `userserie`
--
ALTER TABLE `userserie`
  ADD PRIMARY KEY (`userSerieId`);

--
-- Indices de la tabla `uservideo`
--
ALTER TABLE `uservideo`
  ADD PRIMARY KEY (`userVideoId`);

--
-- Indices de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  ADD PRIMARY KEY (`vcId`);

--
-- Indices de la tabla `videocircle`
--
ALTER TABLE `videocircle`
  ADD PRIMARY KEY (`videoCircleId`);

--
-- Indices de la tabla `videorequest`
--
ALTER TABLE `videorequest`
  ADD PRIMARY KEY (`requestVideoId`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`videoId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access`
--
ALTER TABLE `access`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `accessadmin`
--
ALTER TABLE `accessadmin`
  MODIFY `accessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `accessuseradmin`
--
ALTER TABLE `accessuseradmin`
  MODIFY `accessUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `answerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT de la tabla `appearance`
--
ALTER TABLE `appearance`
  MODIFY `appearanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articlecircle`
--
ALTER TABLE `articlecircle`
  MODIFY `articleCircleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articlerequest`
--
ALTER TABLE `articlerequest`
  MODIFY `requestArticleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticle;multiple=coaches,coacharticle;multiple=circles,articlecircle;', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `articleseriecircle`
--
ALTER TABLE `articleseriecircle`
  MODIFY `serieArticlesCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articleserierequest`
--
ALTER TABLE `articleserierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articleseries`
--
ALTER TABLE `articleseries`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userarticleserie;multiple=coaches,coacharticleserie;multiple=categories,articleseriescategories;multiple=circles,articleseriecircle;', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articleseriescategories`
--
ALTER TABLE `articleseriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `audiocategories`
--
ALTER TABLE `audiocategories`
  MODIFY `acId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `audiocircle`
--
ALTER TABLE `audiocircle`
  MODIFY `audioCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `audiorequest`
--
ALTER TABLE `audiorequest`
  MODIFY `requestAudioId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `audios`
--
ALTER TABLE `audios`
  MODIFY `audioId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,useraudio;multiple=coaches,coachaudio;multiple=categories,audiocategories;multiple=circles,audiocircle;', AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `bannerhome`
--
ALTER TABLE `bannerhome`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `bannersomos`
--
ALTER TABLE `bannersomos`
  MODIFY `bannerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `callcategories`
--
ALTER TABLE `callcategories`
  MODIFY `ccId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `calls`
--
ALTER TABLE `calls`
  MODIFY `callId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercall;multiple=coaches,coachcall;multiple=circles,callscircle;multiple=categories,callcategories;', AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT de la tabla `callscircle`
--
ALTER TABLE `callscircle`
  MODIFY `callCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cambioperfil`
--
ALTER TABLE `cambioperfil`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cambioperfilmiembro`
--
ALTER TABLE `cambioperfilmiembro`
  MODIFY `cambioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `circlecategories`
--
ALTER TABLE `circlecategories`
  MODIFY `ccId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `circlecoachrequest`
--
ALTER TABLE `circlecoachrequest`
  MODIFY `requestCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `circlerequest`
--
ALTER TABLE `circlerequest`
  MODIFY `requestCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `circles`
--
ALTER TABLE `circles`
  MODIFY `circleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,usercircle;multiple=coaches,coachcircle;multiple=categories,circlecategories;', AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `clientpages`
--
ALTER TABLE `clientpages`
  MODIFY `clientPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=home,homeplatform;multiple=pages,clientpages;', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `coacharticle`
--
ALTER TABLE `coacharticle`
  MODIFY `coachArticleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coacharticleserie`
--
ALTER TABLE `coacharticleserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `coachaudio`
--
ALTER TABLE `coachaudio`
  MODIFY `coachAudioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `coachcall`
--
ALTER TABLE `coachcall`
  MODIFY `coachCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT de la tabla `coachcircle`
--
ALTER TABLE `coachcircle`
  MODIFY `coachCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `coaches`
--
ALTER TABLE `coaches`
  MODIFY `coachId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `coachesclients`
--
ALTER TABLE `coachesclients`
  MODIFY `coachClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT de la tabla `coachserie`
--
ALTER TABLE `coachserie`
  MODIFY `coachSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla `coachvideo`
--
ALTER TABLE `coachvideo`
  MODIFY `coachVideoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=414;

--
-- AUTO_INCREMENT de la tabla `colorcatalog`
--
ALTER TABLE `colorcatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `colorId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `colorsclient`
--
ALTER TABLE `colorsclient`
  MODIFY `colorClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  MODIFY `iddiccionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `email`
--
ALTER TABLE `email`
  MODIFY `emailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `excluded`
--
ALTER TABLE `excluded`
  MODIFY `excludedId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gallery`
--
ALTER TABLE `gallery`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `generals`
--
ALTER TABLE `generals`
  MODIFY `generalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `home`
--
ALTER TABLE `home`
  MODIFY `homeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `homeplatform`
--
ALTER TABLE `homeplatform`
  MODIFY `homePlatformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `levelId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `passcambio`
--
ALTER TABLE `passcambio`
  MODIFY `passId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `platforms`
--
ALTER TABLE `platforms`
  MODIFY `platformId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `platformservices`
--
ALTER TABLE `platformservices`
  MODIFY `psId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT de la tabla `postcategory`
--
ALTER TABLE `postcategory`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `postId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `questionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `seriecircle`
--
ALTER TABLE `seriecircle`
  MODIFY `serieCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `serierequest`
--
ALTER TABLE `serierequest`
  MODIFY `requestSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `series`
--
ALTER TABLE `series`
  MODIFY `serieId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,userserie;multiple=coaches,coachserie;multiple=categories,seriescategories;multiple=circles,seriecircle;', AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `seriescategories`
--
ALTER TABLE `seriescategories`
  MODIFY `scId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT de la tabla `servicescatalog`
--
ALTER TABLE `servicescatalog`
  MODIFY `catalogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sessionrecording`
--
ALTER TABLE `sessionrecording`
  MODIFY `sessionRecordingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `shala`
--
ALTER TABLE `shala`
  MODIFY `shalaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `slidehome`
--
ALTER TABLE `slidehome`
  MODIFY `slideId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `statusaccount`
--
ALTER TABLE `statusaccount`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoadmin`
--
ALTER TABLE `tipoadmin`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT COMMENT ' multiple=accessadmin,accessuseradmin', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipollamada`
--
ALTER TABLE `tipollamada`
  MODIFY `tipoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `todos`
--
ALTER TABLE `todos`
  MODIFY `todoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `typemail`
--
ALTER TABLE `typemail`
  MODIFY `typeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `userarticle`
--
ALTER TABLE `userarticle`
  MODIFY `userArticleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userarticleserie`
--
ALTER TABLE `userarticleserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `useraudio`
--
ALTER TABLE `useraudio`
  MODIFY `userAudioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `usercall`
--
ALTER TABLE `usercall`
  MODIFY `userCallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de la tabla `usercircle`
--
ALTER TABLE `usercircle`
  MODIFY `userCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `userclient`
--
ALTER TABLE `userclient`
  MODIFY `userClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `userserie`
--
ALTER TABLE `userserie`
  MODIFY `userSerieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `uservideo`
--
ALTER TABLE `uservideo`
  MODIFY `userVideoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=581;

--
-- AUTO_INCREMENT de la tabla `videocategories`
--
ALTER TABLE `videocategories`
  MODIFY `vcId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT de la tabla `videocircle`
--
ALTER TABLE `videocircle`
  MODIFY `videoCircleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `videorequest`
--
ALTER TABLE `videorequest`
  MODIFY `requestVideoId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `videoId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'multiple=users,uservideo;multiple=coaches,coachvideo;multiple=categories,videocategories;multiple=circles,videocircle;', AUTO_INCREMENT=77;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
