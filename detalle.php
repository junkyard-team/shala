<?php
	include( 'template_header.php' );
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
	$id = ( isset( $_REQUEST[ 'id' ] ) ) ? $_REQUEST[ 'id' ] : 0;
	$folder = $tabla;
	$tituloSeccion = palabraDiccionario( $tabla, $con );
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="../panel/assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/core.css">
		<script src="https://amrak.cdn.spotlightr.com/assets/vooplayer.js"></script>
		<style>
			.divInput { padding: 8px; background-color: #ccc; margin: 5px 0; color: #000; }
			#respuestaDiv hr { margin: 15px 0; }
		</style>
	</head>
	<body class="<?php echo $classBody; ?>">
		<div id="main">
			<?php include( 'template_sidebar.php' ); ?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title"><?php echo ucfirst( $tituloSeccion ); ?></span></div>
						<div class="panel-body pn">
							<div class="tab-block mb25">
								<ul class="nav tabs-right tabs-border">
									<li class="active"><a href="#edicion" data-toggle="tab" id="editcampo"><i class="fa fa-eye text-purple pr5"></i></a></li>
									<?php if ( $tabla == 'users' ) : ?>
									<li><a href="#preguntas" data-toggle="tab"><i class="fa fa-question text-purple pr5"></i></a></li>
									<?php endif; ?>
									<?php if ( $tabla == 'coaches' ) : ?>
									<li><a href="#videos" data-toggle="tab"><i class="fa fa-video-camera text-purple pr5"></i></a></li>
									<?php endif; ?>
									<?php if ( $tabla == 'users' || $tabla == 'coaches' ) : ?>
									<li><a href="#llamadas" data-toggle="tab"><i class="fa fa-phone text-purple pr5"></i></a></li>
									<?php endif; ?>
								</ul>
								<div class="tab-content">
									<div id="edicion" class="tab-pane active">
										<?php armaDetalle( $tabla, $clientId, $con ); ?>
									</div>
									<?php if ( $tabla == 'users' ) : ?>
									<div id="preguntas" class="tab-pane">
										<div id="respuestaDiv"></div>
									</div>
									<?php endif; ?>
									<?php if ( $tabla == 'coaches' ) : ?>
									<div id="videos" class="tab-pane admin-form">
										<label for="nombreUser">Video series</label>
										<label class="field select">
											<select id="serieCoach" onchange="cargaVideos()">
												<option value="">Pick your serie</option>
											</select>
											<i class="arrow"></i>
										</label>
										<hr>
										<table class="table table-bordered mbn">
											<thead>
												<tr>
													<th>Video</th>
													<th>Title</th>
													<th>Description</th>
												</tr>
											</thead>
											<tbody id="videoTabla"></tbody>
										</table>
									</div>
									<?php endif; ?>
									<?php if ( $tabla == 'users' || $tabla == 'coaches' ) : ?>
									<div id="llamadas" class="tab-pane">
										<?php if ( $tabla == 'users' ) : ?>
											<table class="table table-bordered mbn">
												<thead>
													<tr>
														<th>Meeting Title</th>
														<th>Status</th>
														<th>Date</th>
														<th>Time</th>
													</tr>
												</thead>
												<tbody id="contenidoTabla"></tbody>
											</table>
										<?php else: ?>
											<table class="table table-bordered mbn">
												<thead>
													<tr>
														<th>Meeting Title</th>
														<th>Description</th>
														<th>Student Name</th>
														<th>Status</th>
														<th>Type</th>
														<th>Date</th>
														<th>Time</th>
														<th>Duration</th>
													</tr>
												</thead>
												<tbody id="contenidoTabla"></tbody>
											</table>
										<?php endif; ?>
									</div>
									<?php endif; ?>
								</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<?php if ( $tabla == 'coaches' ) :?>
		<div id="modal-coaches" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Rejection Reason</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="form-group mb10 row">
							<div class="form-group mb10 row">
								<input type="hidden" id="coachIdReason">
								<textarea id="coachReason" class="gui-input" placeholder="Enter the reject reason"></textarea>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="button btn-primary" onclick="rechazaTexto()">Send</button>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div id="videoPopup" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Video</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<iframe src="" data-playerId="" allow="autoplay" class="video-player-container vooplayer" allowtransparency="true" name="vooplayerframe" allowfullscreen="true" watch-type="" url-params="" frameborder="0" scrolling="no" style="z-index: 999999999;"></iframe>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php
			include('template_footer_scripts.php');
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkwYo-1e42rL_fS43hLbGYBUYBhxqrS-A"></script>
		<script src="../panel/assets/js/demo/widgets.js"></script>
		<script src="../panel/assets/js/signature_pad.min.js"></script>
		<script src="../panel/plugins/doublescroll.js"></script>
		<script src="../panel/assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					<?php
						if ( $folder != '' ) {
							echo 'localStorage.setItem( \'folderActivo\', \'' . $folder . '\' );';
						}
						echo 'localStorage.setItem( \'tabla\', \'' . $tabla . '\' );';
						echo 'localStorage.setItem( \'id\', \'' . $id . '\' );';
					?>
					$( '.select2-multiple' ).select2( {
						placeholder: "Elige Opciones",
						allowClear: true
					} );
					checaUsuario();
					listaDetalle();
					<?php if ( $tabla == 'users' ) : ?>
					respuestaAlumno( <?php echo $id; ?>, 2 );
					traeLlamadas();
					<?php endif; ?>
					<?php if ( $tabla == 'coaches' ) : ?>
					llamadasCoach();
					listaSeries();
					<?php endif; ?>
				} );
			} );
		</script>
	</body>
</html>
<?php $con->CierraConexion(); ?>