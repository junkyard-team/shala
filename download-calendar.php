<?php
	$ical = 'BEGIN:VCALENDAR
	VERSION:2.0
	PRODID:-//shala/handcal//NONSGML v1.0//EN
	CALSCALE:GREGORIAN
	X-WR-TIMEZONE: ' . $_REQUEST[ 'timezone' ] . '
	BEGIN:VEVENT
	UID:' . md5( date( "YmdHis" ) ) . '
	DTSTAMP:' . time() . '
	DTSTART:' . date( 'Ymd\THis', strtotime( $_REQUEST[ 'fecha' ] ) ) . '
	DTEND:' . date( 'Ymd\THis', strtotime( $_REQUEST[ 'fecha' ] . '+' . $_REQUEST[ 'duration' ] . ' minutes' ) ) . '
	LOCATION: ' . addslashes( $_REQUEST[ 'location' ] ) . '
	DESCRIPTION:' . addslashes( $_REQUEST[ 'description' ] ) . '
	SUMMARY:' . strip_tags( $_REQUEST[ 'summary' ] ) . '
	END:VEVENT
	END:VCALENDAR';
	header( 'Content-Type: text/calendar; charset=utf-8' );
	header( 'Content-Disposition: attachment; filename=' . $_REQUEST[ 'client' ] . '-session.ics' );
	echo $ical;
	exit();
?>