<?php include 'cabecera.php'; ?>
<div class="pageHead">
	<h1><?php echo $tituloClasses; ?></h1>
	<h2><?php echo $subTituloClasses; ?></h2>
</div>
<!-- <?php include 'sessionlist.php'; ?> -->
<div class="seccion productoSerie">
	<div class="principal">
		<div class="fila listaFiltrados">
			<div class="veinte">
				<h3>Categories</h3>
				<div class="lf">
					<div class="filtro"><input type="radio" name="categoriaFiltro" id="0" filtro="all" checked>All</div>
					<?php
						$queryCat = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
						while( $CAT = $con->Resultados( $queryCat ) ) {
							echo '<div class="filtro"><input type="radio" name="categoriaFiltro" id="' . $CAT[ 'categoryId' ] . '" filtro="' . limpialo( $CAT[ 'nombre' ], 'min' ) . '"> ' . $CAT[ 'nombre' ] . '</div>';
						}
					?>
				</div>
			</div>
			<div class="ochenta">
				<div class="filaFlex">
				<?php
					$query = $con->Consulta( 'select s.*, c.apellido, c.nombre as coachName from videos s inner join coaches c on(s.coachId=c.coachId) where accessId<>4 and ( s.clientId=' . $clientId . ' or s.clientId=' . $clientPlatformId . ' ) order by orden asc, videoId desc' );
					if ( $con->Cuantos( $query ) > 0 ) {
						while( $S = $con->Resultados( $query ) ) {
							$imagenSerie = ( !is_null( $S[ 'imagen' ] ) && $S[ 'imagen' ] != '' ) ? 'images/videos/' . $S[ 'imagen' ] : 'images/video-general.jpg';
							$categoria = '';
							$filtros = '';
							$descriptionText = ( strlen( $S[ 'description' ] ) > 120 ) ? substr( $S[ 'description' ], 0, 120 ) . '...' : $S[ 'description' ];
							$queryCat = $con->Consulta( 'select c.nombre from videocategories s inner join categories c on(s.categoryId=c.categoryId) where s.videoId=' . $S[ 'videoId' ] );
							while( $CAT = $con->Resultados( $queryCat ) ) {
								$categoria = '<span>' . $CAT[ 'nombre' ] . '</span>';
								$filtros .= ' ' . limpialo( $CAT[ 'nombre' ], 'min' );
							}
							$usersAproved = array();
							$queryUser = $con->Consulta( 'select * from uservideo where videoId=' . $S[ 'videoId' ] );
							while( $U = $con->Resultados( $queryUser ) ) {
								$usersAproved[] = $U[ 'userId' ];
							}
							$coachesAproved = array();
							$queryUser = $con->Consulta( 'select * from coachvideo where videoId=' . $S[ 'videoId' ] );
							while( $U = $con->Resultados( $queryUser ) ) {
								$coachesAproved[] = $U[ 'coachId' ];
							}
							$usersCirculo = array();
							$coachesCirculo = array();
							$queryUser = $con->Consulta( 'select * from videocircle where videoId=' . $S[ 'videoId' ] );
							while( $V = $con->Resultados( $queryUser ) ) {
								$queryUser = $con->Consulta( 'select * from usercircle where circleId=' . $V[ 'circleId' ] );
								while( $U = $con->Resultados( $queryUser ) ) {
									$usersCirculo[] = $U[ 'userId' ];
								}
								$queryUser = $con->Consulta( 'select * from coachcircle where circleId=' . $V[ 'circleId' ] );
								while( $U = $con->Resultados( $queryUser ) ) {
									$coachesCirculo[] = $U[ 'coachId' ];
								}
							}
							$onclick = 'muestraVideo( \'' . $S[ 'video' ] . '\' )';
							if ( $S[ 'accessId' ] != 1 ) {
								if ( $S[ 'accessId' ] == 4 ) {
									$usersString = implode( '-', $usersCirculo );
									$coachesString = implode( '-', $coachesCirculo );
								} else {
									$usersString = implode( '-', $usersAproved );
									$coachesString = implode( '-', $coachesAproved );
								}
								$onclick = 'validaNivel( \'' . $S[ 'video' ] . '\', ' . $S[ 'coachId' ] . ', ' . $S[ 'accessId' ] . ', \'' . $usersString . '\', \'' . $coachesString . '\' )';
							}
							echo
							'<div class="triple video cPad' . $filtros . ' all">
								' . $categoria . '
								<a href="#" onclick="' . $onclick . '" class="noHref">
									<img src="thumb?src=' . $imagenSerie . '&size=400x240" class="fullImg" style="border: 1px solid #000">
								</a>
								<h3 class="tituloSerie"><a href="#" onclick="' . $onclick . '" class="noHref">' . $S[ 'titulo' ] . '</a></h3>
								<h4 class="tituloCoachSeria"><a href="coach/' . limpialo( $S[ 'apellido' ], 'min' ) . '-' . limpialo( $S[ 'coachName' ], 'min' ) . '">' . $S[ 'coachName' ] . ' ' . $S[ 'apellido' ] . '</a></h4>
								<p cuantos="80">' . $descriptionText . '</p>
							</div>';
						}
					}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>
