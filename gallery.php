<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<div class="pageHead">
	<h1>Gallery</h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<div class="images-grid images-grid-1 zoom-gallery">
				<?php
					$query = $con->Consulta( 'select * from gallery where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
					while( $R = $con->Resultados( $query ) ) {
						echo
						'<a href="images/gallery/' . $R[ 'image' ] . '" class="image-wrapper">
							<img class="image-thumb" src="images/gallery/' . $R[ 'image' ] . '" />
						</a>';
					}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.imagesGrid.js"></script>
<script type="text/javascript">
	$( document ).ready( function() {
		$( '.images-grid-1' ).imagesGrid();
		$( '.zoom-gallery' ).magnificPopup( {
			delegate: 'a',
			type: 'image',
			closeOnContentClick: false,
			closeBtnInside: false,
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			image: { verticalFit: true, },
			gallery: { enabled: true },
			zoom: { enabled: true, duration: 300, opener: function(element) { return element.find('img'); } }
		} );
	} );
</script>
<?php include 'pie.php'; ?>