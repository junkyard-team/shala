<?php
	ini_set( 'max_execution_time', 3000 );
	ini_set( 'max_input_time ', 3000 );
	ini_set( 'post_max_size ', '200M' );
	ini_set( 'upload_max_filesize ', '200M' );
	include( 'template_header.php' );
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
	$folder = $tabla;
	$album = ( isset( $_REQUEST[ 'album' ] ) ) ? limpialo( $_REQUEST[ 'album' ], 'min' ) : '';
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="../panel/assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="../panel/plugins/css/core.css">
	</head>
	<body class="<?php echo $classBody; ?>">
		<div class="iconoPicker">
			<i class="fa fa-close cierra" onclick="popIconPicker( 2 )"></i>
			<div id="iconosMuestra"></div>
		</div>
		<div id="main">
			<?php include( 'template_sidebar.php' );?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title"><?php echo ucfirst( $tabla ); ?></span></div>
						<div class="panel-body pn">
							<div class="row">
								<div role="form" class="admin-form" style="float: left; width: 220px;">
									<label class="field prepend-icon append-button file" for="imagen">
										<span class="button btn-primary">Images</span>
										<form contiene="imagen">
											<input type="file" id="imagenGaleria" onchange="$( '#uploader-imagen' ).val( this.value ); subeGaleria()" class="gui-file" multiple>
											<input id="uploader-imagen" placeholder="Elige imagenes" class="gui-input noPelar" type="text" style="text-align: right;">
											<label class="field-icon"><i class="fa fa-upload"></i></label>
										</form>
									</label>
								</div>
								<div role="form" class="admin-form" style="float: right; width: 300px;">
									<label class="field select"><select id="album" onchange="listaAlbum( this.value )"></select><i class="arrow"></i></label>
								</div>
							</div>
							<div class="row">
								<div class="pull-right" id="paginacionTabla" style="position: relative; margin-top: 0;"></div>
							</div>
							<div class="tablaScroll">
								<table data-page-navigation=".pagination" data-page-size="20" class="table footable">
									<thead>
										<tr>
											<th>Accion</th>
											<th>ID</th>
											<th>Imagen</th>
											<th>Posicion</th>
										</tr>
									</thead>
									<tbody id="contenidoTabla"></tbody>
									<tfoot class="footer-menu">
										<tr>
											<td colspan="8">
												<nav class="text-right">
													<ul class="pagination hide-if-no-paging"></ul>
												</nav>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<?php
			include('template_footer_scripts.php');
			$con->CierraConexion();
		?>
		<script src="../panel/assets/js/demo/widgets.js"></script>
		<script src="../panel/plugins/doublescroll.js"></script>
		<script src="../panel/assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					<?php
						if ( $folder != '' ) {
							echo 'localStorage.setItem( \'folderActivo\', \'' . $folder . '\' );';
						}
						echo 'localStorage.setItem( \'tabla\', \'' . $tabla . '\' );';
						echo 'localStorage.setItem( \'album\', \'' . $album . '\' );';
					?>
					checaUsuario();
					opcionesLista( '<?php echo $album; ?>', '', '', 'album' );
				} );
			} );
		</script>
	</body>
</html>