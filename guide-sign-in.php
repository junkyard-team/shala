<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1><?php echo $registroText; ?></h1>
</div>
<div class="seccion signUp">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<h2 class="abreForm" form="coachForm"><?php echo $coachText; ?></h2>
				<form method="post" id="coachForm" class="cPad" tipo="1" enctype="multipart/form-data">
					<input type="hidden" id="coachId" value="0">
					<!-- <label for="categoryId">Category</label>
					<select id="categoryId" class="obligatorio" required>
					<?php
						echo '<option value="">Pick an option</option>';
						$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' order by categoryId' );
						while( $R = $con->Resultados( $query ) ) {
							echo '<option value="' . $R[ 'categoryId' ] . '">' . $R[ 'nombre' ] . '</option>';
						}
					?>
					</select> -->
					<label for="nombre">First Name</label>
					<input type="text" id="nombre" class="obligatorio" required>
					<label for="nombre">Last Name</label>
					<input type="text" id="apellido" class="obligatorio" required>
					<!-- <label for="biografia">Biography</label>
					<textarea id="biografia" class="obligatorio" required></textarea> -->
					<label for="email">Email</label>
					<input type="email" id="email" class="email obligatorio" required>
					<label for="pass">Password</label>
					<input type="password" id="pass" class="obligatorio" required>
					<br><br>
					<button type="submit" class="boton">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>