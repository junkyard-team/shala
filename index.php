<?php include 'cabecera.php'; ?>
<?php if ( checaSeccion( 'header', $clientId, $con ) ) : ?>
<div class="videoHead">
	<?php if ( !is_null( $videoUrl ) ) : ?>
	<video autoplay loop playsinline muted id="videoHome">
	<?php if ( isMobile() ) : ?>
		<source src="images/<?php echo $videoMobileUrl; ?>" type="video/mp4">
	<?php else : ?>
		<source src="images/<?php echo $videoUrl; ?>" type="video/mp4">
	<?php endif; ?>
	</video>
	<?php else : ?>
	<ul class="bxslider" id="mainSlide">
	<?php
		$query = $con->Consulta( 'select * from slidehome where clientId=' . $clientId . ' or clientId=' . $clientPlatformId . ' order by posicion asc' );
		while( $R = $con->Resultados( $query ) ) {
			$imgFile = 'images/slidehome/' . $R[ 'imagen' ];
			$imgMovilFile = 'images/slidehome/' . $R[ 'imagenMovil' ];
			if ( !is_null( $R[ 'imagenMovil' ] ) && file_exists( $imgMovilFile ) ) {
				echo '<li><img src="' . $imgFile . '?v=1" alt="Deorno Slide" pc="' . $imgFile . '" movil="' . $imgMovilFile . '" class="fullImg pcMovil"></li>';
			} else {
				echo '<li><img src="' . $imgFile . '?v=1" alt="Deorno Slide" pc="' . $imgFile . '" movil="' . $imgFile . '" class="fullImg"></li>';
			}
		}
	?>
	</ul>
	<?php endif; ?>
	<div class="mensaje <?php if ( !is_null( $botonUrlHeader ) && $botonUrlHeader != '' && ( is_null( $botonTextHeader ) || $botonTextHeader == '' ) ) { echo 'cPointer'; } ?>" <?php if ( !is_null( $botonUrlHeader ) && $botonUrlHeader != '' && ( is_null( $botonTextHeader ) || $botonTextHeader == '' ) ) { echo 'onclick="manda( \'' . $botonUrlHeader . '\' );"'; } ?>>
		<div class="textos">
			<h1><?php echo $tituloHeader; ?></h1>
			<h3><?php echo $subTituloHeader; ?></h3>
			<?php if ( $showButton ) : ?>
			<?php if ( !is_null( $botonUrlHeader ) && $botonUrlHeader != '' && !is_null( $botonTextHeader ) && $botonTextHeader != '' ) : ?>
			<a class="boton claro botonHead" href="<?php echo $botonUrlHeader; ?>"><?php echo $botonTextHeader; ?></a>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if ( checaSeccion( 'categories', $clientId, $con ) ) : ?>
<div class="seccion categoriasCuadros">
	<div class="principal">
		<h1 class="titulo tCenter centraDiv"><?php echo $tituloCategoria; ?></h1>
		<h3 class="subtitulo tCenter centraDiv"><?php echo $subTituloCategoria; ?></h3>
		<div class="fila mTop">
		<?php
			$query = $con->Consulta( 'select * from categories where clientId=' . $clientId . ' or clientId=' . $clientPlatformId . ' order by categoryId asc limit 4' );
			while( $R = $con->Resultados( $query ) ) {
				echo
				'<div class="veinteCinco cPad categoriaHome" onclick="manda( \'categoria/' . limpialo( $R[ 'nombre' ], 'min' ). '\' )">
					<img src="thumb?src=images/categories/' . $R[ 'imagen' ] . '&size=297x167" class="fullImg">
					<h4>' . $R[ 'nombre' ] . '</h4>
					<p>' . $R[ 'descripcion' ] . '</p>
				</div>';
			}
		?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if ( $muestraCoach == 'S' ) : ?>
<?php if ( checaSeccion( 'coaches', $clientId, $con ) ) : ?>
<div class="seccion">
	<div class="principal">
		<h1 class="titulo tCenter centraDiv"><?php echo $tituloInstructores; ?></h1>
		<h3 class="subtitulo tCenter centraDiv"><?php echo $subTituloInstructores; ?></h3>
	</div>
	<div class="swiper-container">
		<div class="swiper-wrapper">
		<?php
			$query = $con->Consulta( 'select DISTINCT c.* from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where ( cc.clientId=' . $clientId . ' or cc.clientId=' . $clientPlatformId . ' ) and pending="N" and showHome="S" order by c.apellido asc, c.nombre asc' );
			while( $R = $con->Resultados( $query ) ) {
				$categorias = '';
				echo
				'<div class="swiper-slide" onclick="manda( \'coach/' . limpialo( $R[ 'apellido' ], 'min' ) . '-' . limpialo( $R[ 'nombre' ], 'min' ) . '\' )">
					<img src="thumb?src=images/coaches/' . $R[ 'foto' ] . '&size=240x290" class="fullImg">
					<h4>' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</h4>
					<p class="smallP">' . $R[ 'phrase' ] . '</p>
				</div>';
			}
		?>
		</div>
		<div class="swiper-button-prev botonesSwipe"><i class="fas fa-chevron-left"></i></div>
    	<div class="swiper-button-next botonesSwipe"><i class="fas fa-chevron-right"></i></div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>
<?php if ( checaSeccion( 'banners', $clientId, $con ) ) : ?>
<?php
	$i = 0;
	$query = $con->Consulta( 'select * from bannerhome where clientId=' . $clientId . ' or clientId=' . $clientPlatformId );
	while( $R = $con->Resultados( $query ) ) {
		$imgClass = ( $i % 2 ) ? 'left' : 'right';
		$txtClass = ( $i % 2 ) ? 'right' : 'left';
		$contentImg = ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) ? '<div class="playVideo" onclick="muestraVideo( \'' . $R[ 'video' ] . '\' )"><img src="thumb?src=images/bannerhome/' . $R[ 'image' ] . '&size=802x452"></div>' : '<img src="thumb?src=images/bannerhome/' . $R[ 'image' ] . '&size=802x452">';
		echo
		'<div class="seccion margen">
			<div class="contentImage ' . $imgClass . '">' . $contentImg . '</div>
			<div class="contentText">
				<div class="principal">
					<div class="texto ' . $txtClass . '">
						<h1 class="titulo">' . $R[ 'title' ] . '</h1>
						<h3>' . $R[ 'subtitle' ] . '</h3>
						<a class="boton claro" href="' . $R[ 'buttonUrl' ] . '">' . $R[ 'buttonText' ] . '</a>
					</div>
				</div>
			</div>
		</div><br>';
		$i++;
	}
?>
<?php endif; ?>
<?php if ( checaSeccion( 'banners', $clientId, $con ) ) : ?>
<?php include 'sessionlist.php'; ?>
<?php endif; ?>
<?php if ( checaSeccion( 'trialtext', $clientId, $con ) ) : ?>
<div class="newsletter">
	<div class="principal">
		<div class="texto">
			<h1 class="titulo"><?php echo $pruebaDias; ?></h1>
			<h3 class="subtitulo"><?php echo $iniciaPrueba; ?></h3>
		</div>
		<div class="campo">
			<a class="boton claro" href="/sign-in">Start your free trial</a>
			<a class="boton" href="/explore">Explore</a>
		</div>
	</div>
</div>
<?php endif; ?>
<?php include 'pie.php'; ?>

