<?php include 'cabecera.php'; ?>
<div class="pageHead">
	<h1><?php echo $instructores; ?></h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="couchList">
		<?php
			$query = $con->Consulta( 'select DISTINCT c.* from coaches c inner join coachesclients cc on(c.coachId=cc.coachId) where ( cc.clientId=' . $clientId . ' or cc.clientId=' . $clientPlatformId . ' ) and cc.pending="N" order by c.apellido asc, c.nombre asc' );
			while( $R = $con->Resultados( $query ) ) {
				$photo = ( !is_null( $R[ 'foto' ] ) && $R[ 'foto' ] != '' ) ? 'images/coaches/' . $R[ 'foto' ] : 'images/silueta.png';
				echo
				'<div class="couch" onclick="manda( \'coach/' . limpialo( $R[ 'apellido' ], 'min' ) . '-' . limpialo( $R[ 'nombre' ], 'min' ) . '\' )">
					<div class="img"><img src="thumb?src=' . $photo . '&size=240x290" class="fullImg"></div>
					<h4>' . $R[ 'nombre' ] . ' ' . $R[ 'apellido' ] . '</h4>
				</div>';
			}
		?>
		</div>
	</div>
</div>
<!-- <?php include 'sessionlist.php'; ?> -->
<?php include 'pie.php'; ?>