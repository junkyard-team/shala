var ajaxLlamada;
var tabContentAbierto = '';
var mediaIdSelected = '';
var nuevoSlide;
var destacadoSlide;
var popularSlide;
var alturaInicial = 460;
var map;
var markers = [];
var funcionRefresca = '';
$( window ).on( 'load', function() {
    if ( $( window ).width() > 628 ) {
		$( '.checaAlto' ).each( function() {
			var altoPx = $( this ).find( '.baseAltura' ).height();
			$( this ).find( '.aumenta' ).height( altoPx );
		} );
	}
	if ( $( window ).width() > 420 ) {
		$( '.cuadrantes .treintaTres' ).each( function() {
			var imagenAlto = $( '.cuadrantes .treintaTres img' ).height() + 5;;
			$( this ).height( imagenAlto );
		} );
	}
} );
$( document ).ready( function() {
	$( '.dMenu .sociales a' ).blur( function( event ) {
		console.log( 'focus' );
	} );
	if ( archivoUsado == 'circulos.php' ) {
		if ( checaSesion( 'studentId' ) ) {
			var usuarios = $( '.circuloLink' ).attr( 'usuarios' );
			if ( usuarios != '' ) {
				var usuariosArray = usuarios.split( ',' );
				$.each( usuariosArray, function( index, el ) {
					if ( obtenSesion( 'studentId' ) == el ) {
						$( '.circuloLink' ).hide();
					}
				} );
			}
		}
		if ( checaSesion( 'coachId' ) ) {
			if ( obtenSesion( 'coachId' ) == $( '.circuloLink' ).attr( 'coachId' ) ) {
				$( '.circuloLink' ).hide();
			} else {
				var coaches = $( '.circuloLink' ).attr( 'coaches' );
				if ( coaches != '' ) {
					var coachesArray = coaches.split( ',' );
					$.each( coachesArray, function( index, el ) {
						if ( obtenSesion( 'coachId' ) == el ) {
							$( '.circuloLink' ).hide();
						}
					} );
				}
			}
		}
	}
	checaAccesoSitio();
	borraSesion( 'serieSelected' );
	if ( tipoCliente == 2 ) {
		vaciaSesion();
	}
	if ( tipoAcceso == 1 ) {
		if ( $( '.videoHead video' ).length ) {
			$( '.videoHead' ).height( $( window ).height() - 63 );
		}
	}
	if ( checaSesion( 'studentId' ) || checaSesion( 'coachId' ) ) {
		$( '.sesionData' ).show();
		if ( checaSesion( 'coachId' ) ) {
			$( '.sessionList[coach=' + obtenSesion( 'coachId' ) + ']' ).show();
			$( '.meetings.coach-' + obtenSesion( 'coachId' ) ).show();
			$( '.privateSesion.coach-' + obtenSesion( 'coachId' ) ).show();
		} else if ( checaSesion( 'studentId' ) ) {
			$( '.privateSesion.member-' + obtenSesion( 'studentId' ) ).show();
		}
	}
	$( '.linkAnchor' ).click( function( e ) {
		e.preventDefault();
		var target = '#' + $( this ).attr( 'destino' );
		$( 'html, body' ).animate( {
			scrollTop: ( $( target ).offset().top ) - 50
		}, 800 );
	} );
	$( '.noHref' ).click( function( event ) {
		event.preventDefault();
	} );
	$( '.abreForm' ).click( function() {
		var formId = $( this ).attr( 'form' );
		if ( !$( '#' + formId ).is( ':visible' ) ) {
			$( '.formHide' ).hide();
		}
		$( '#' + formId ).slideToggle();
	} );
	$( '#coachForm' ).submit( function( e ) {
		e.preventDefault();
		guardaCoach();
	} );
	$( '#studentForm' ).submit( function( e ) {
		e.preventDefault();
		guardaStudent();
	} );
	$( '#loginSite' ).submit( function( e ) {
		e.preventDefault();
		var parametros = { 'accessPassword': $( '#accessPassword' ).val(), 'clientId': clientId };
		ajaxRequest( 'POST', parametros, 'admin/querys/login-site', iniciaSitio );
	} );
	$( '#loginForm' ).submit( function( e ) {
		e.preventDefault();
		var parametros = { 'email': $( '#emailUser' ).val(), 'pass': $( '#passUser' ).val(), 'clientId': clientId };
		ajaxRequest( 'POST', parametros, 'admin/querys/login-cliente', iniciaSesion );
	} );
	$( '#passForm' ).submit( function( e ) {
		e.preventDefault();
		if ( $( '#passUser' ).val() == $( '#passRepeat' ).val() ) {
			var parametros = { 'id': $( '#id' ).val(), 'tipo': $( '#tipo' ).val(), 'pass': $( '#passUser' ).val(), 'clientId': clientId };
			ajaxRequest( 'POST', parametros, 'admin/querys/cambia-pass', passCambiado );
		} else {
			alerta( '', 'Both pass fields must match', 'error' );
		}
	} );
	if ( checaSesion( 'coachId' ) || checaSesion( 'studentId' ) ) {
		$( '#cuentaLink a' ).attr( 'href', 'account' );
		$( '#cuentaLink a' ).html( 'Dashboard<span></span>' );
		$( '#iniciaLink' ).hide();
		$( '#cierraLink' ).show();
	}
	$( '#preguntasForm' ).submit( function( e ) {
		e.preventDefault();
		guardaExamen();
	} );
	$( '#videoForm' ).submit( function( e ) {
		e.preventDefault();
		guardaVideo();
	} );
	$( '#audioForm' ).submit( function( e ) {
		e.preventDefault();
		guardaAudio();
	} );
	$( '#articleForm' ).submit( function( e ) {
		e.preventDefault();
		guardaArticle();
	} );
	$( '#contactForm' ).submit( function( e ) {
		e.preventDefault();
		var parametros = {
			'name': $( '#nombre' ).val(),
			'phone': $( '#telefono' ).val(),
			'email': $( '#email' ).val(),
			'message': $( '#mensaje' ).val(),
			'clientId': clientId
		};
		ajaxRequest( 'POST', parametros, 'admin/querys/enviar', respuestaEnviar );
	} );
	$( '#coachCallForm' ).submit( function( e ) {
		e.preventDefault();
		if ( $( '#fecha' ).val() == '' || $( '#time' ).val() == '' ) {
			alertaTmp( '', 'You must to choose a date and time', 'error' );
		} else {
			cargando( true );
			var formData = new FormData();
			if ( $( '#fotoCall' ).length ) {
				if ( $( '#fotoCall' )[0].files.length ) {
					var files = $( '#fotoCall' ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( 'image', value );
					} );
				}
			}
			var allMembers = $( '#userCallAll' ).is( ':checked' ) ? 'S' : 'N';
			var allCoaches = $( '#coachCallAprovedAll' ).is( ':checked' ) ? 'S' : 'N';
			formData.append( 'callId', $( '#callId' ).val() );
			formData.append( 'coachId', obtenSesion( 'coachId' ) );
			formData.append( 'title', $( '#titleCall' ).val() );
			formData.append( 'description', $( '#descriptionCall' ).val() );
			formData.append( 'tipoId', $( '#tipoId' ).val() );
			var tiempoFormat = $( '#time' ).val();
			formData.append( 'fecha', $( '#fecha' ).val() + ' ' + tiempoFormat );
			formData.append( 'duration', $( '#durationCall' ).val() );
			formData.append( 'categoryId', 0 );
			formData.append( 'timezone', $( '#timezone' ).val() );
			formData.append( 'clientId', clientId );
			formData.append( 'tabla', 'calls' );
			formData.append( 'folder', 'calls' );
			formData.append( 'folderActivo', 'calls' );
			formData.append( 'allMembers', allMembers );
			formData.append( 'allCoaches', allCoaches );
			var formMultiple = [];
			var info = { 'destino': 'callcategories', 'ids': $( '#categoryCall' ).val().join( '-' ) };
			formMultiple.push( info );
			if ( $( '#tipoId' ).val() == 2 ) {
				var info = { 'destino': 'usercall', 'ids': $( '#userCall' ).val().join('-') };
				formMultiple.push( info );
				var info = { 'destino': 'coachcall', 'ids': $( '#coachCallAproved' ).val().join('-') };
				formMultiple.push( info );
				var info = { 'destino': 'callscircle', 'ids': $( '#circlesAsignedCall' ).val().join( '-' ) };
				formMultiple.push( info );
			} else if ( $( '#tipoId' ).val() == 4 ) {
				var info = { 'destino': 'callscircle', 'ids': $( '#circlesAsignedCall' ).val().join( '-' ) };
				formMultiple.push( info );
			}
			formData.append( 'multiples', JSON.stringify( formMultiple ) );
			ajaxLlamada = $.ajax( {
				type: 'POST',
				url: 'admin/querys/guarda',
				data: formData,
				processData: false,
				contentType: false,
				async: true,
				xhr: function() {
					var xhr = $.ajaxSettings.xhr();
					xhr.upload.onprogress = function(e) {
						porcentaje( Math.floor(e.loaded / e.total *100) );
					};
					return xhr;
				},
				success: function( response ) {
					cargando( false );
					datos = JSON.parse( response );
					if ( datos.status == 'Success' ) {
						alertaTmp( '', 'Call request sent successfully', 'success' );
						if ( checaSesion( 'coachId' ) ) {
							statusCall( 1, datos.id );
							if ( $( '#inviteCall' ).is( ':checked' ) ) {
								sendInvitation( datos.id );
							}
							popup( 'coachCall', false );
							llamadasCoach();
							traeSchedule();
						} else {
							popup( 'studentCall', false );
							traeLlamadas();
							traeSchedule();
						}
						$( '#coachCallForm' ).trigger( 'reset' );
					} else {
						errorCallback();
					}
				}
			} );
		}
	} );
	$( '#studentCallForm' ).submit( function( e ) {
		e.preventDefault();
		var formMultiple = [];
		var info = { 'destino': 'usercall', 'ids': obtenSesion( 'studentId' ) };
		formMultiple.push( info );
		var tiempoFormat = $( '#timeCall' ).val();
		var parametros = {
			'callId': $( '#callIdUser' ).val(),
			'coachId': $( '#coachCallPick' ).val(),
			'title': $( '#titleUser' ).val(),
			'description': $( '#descriptionCallUser' ).val(),
			'timezone': $( '#timezoneStudent' ).val(),
			'tipoId': 2,
			'fecha': $( '#fechaCall' ).val() + ' ' + tiempoFormat,
			'clientId': clientId,
			'multiples': JSON.stringify( formMultiple ),
			'tabla': 'calls',
			'folder': 'calls',
			'folderActivo': 'calls'
		};
		ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaAgenda );
	} );
	$( '#serieArticleForm' ).submit( function( e ) {
		e.preventDefault();
		cargando( true );
		var formData = new FormData();
		formData.append( 'coachId', obtenSesion( 'coachId' ) );
		formData.append( 'serieId', $( '#serieArticleId' ).val() );
		formData.append( 'tabla', 'articleseries' );
		formData.append( 'folder', 'articleseries' );
		formData.append( 'folderActivo', 'articleseries' );
		formData.append( 'nombre', $( '#serieArticleNombre' ).val() );
		formData.append( 'accessId', $( '#accessArticleSerie' ).val() );
		formData.append( 'description', $( '#descriptionSerieArticle' ).val() );
		formData.append( 'clientId', clientId );
		var formMultiple = [];
		var info = { 'destino': 'articleseriescategories', 'ids': $( '#categoryIdSerieArticle' ).val().join( '-' ) };
		formMultiple.push( info );
		info = { 'destino': 'userarticleserie', 'ids': $( '#studentArticleAproved' ).val().join( '-' ) };
		formMultiple.push( info );
		info = { 'destino': 'coacharticleserie', 'ids': $( '#coachArticleAproved' ).val().join( '-' ) };
		formMultiple.push( info );
		info = { 'destino': 'articleseriecircle', 'ids': $( '#circlesAsignedArticle' ).val().join( '-' ) };
		formMultiple.push( info );
		formData.append( 'multiples', JSON.stringify( formMultiple ) );
		if ( $( '#photoArticle' ).length ) {
			if ( $( '#photoArticle' )[0].files.length ) {
				var files = $( '#photoArticle' ).prop( 'files' );
				$.each( files, function( key, value ) {
					formData.append( 'photo', value );
				} );
			}
		}
		ajaxLlamada = $.ajax( {
			type: 'POST',
			url: 'admin/querys/guarda',
			data: formData,
			processData: false,
			contentType: false,
			async: true,
			xhr: function() {
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function(e) {
					porcentaje( Math.floor(e.loaded / e.total *100) );
				};
				return xhr;
			},
			success: function( response ) {
				var respuesta = JSON.parse( response );
				cargando( false );
				if ( 'Success' === respuesta.status ) {
					if ( $( '#serieArticleId' ).val() == 0 ) {
						seteaSesion( 'nombreSerieArticle', $( '#serieArticleNombre' ).val() );
						popup( 'serieArticleInfo', false );
						alertaTmp( '', 'Successfully saved', 'success' );
						$( '#serieArticleCoach' ).append( '<option value="' + respuesta.id + '">' + $( '#serieArticleNombre' ).val() + '</option>' )
					} else {
						popup( 'serieArticleInfo', false );
						alertaTmp( '', 'Updated', 'success' );
						listaSeriesArticles();
					}
				}
			}
		} );
	} );
	$( '#circleForm' ).submit( function( e ) {
		e.preventDefault();
		if ( $( '#videoCircle' ).length && $( '#videoCircle' )[0].files.length ) {
			if ( $( '#categoryIdCircle' ).val().length > 0 ) {
				cargando( true );
				var formData = new FormData();
				formData.append( 'vooKey', vooKey );
				formData.append( 'create', 1 );
				formData.append( 'customS3', 0 );
				var files = $( '#videoCircle' ).prop( 'files' );
				$.each( files, function( key, value ) {
					formData.append( 'file', value );
					formData.append( 'name', $( '#circleNombre' ).val() + ' Intro' );
				} );
				ajaxLlamada = $.ajax( {
					type: 'POST',
					url: 'https://api.vooplayer.com/api/createVideo',
					data: formData,
					async: true,
					processData: false,
					contentType: false,
					xhr: function() {
						var xhr = $.ajaxSettings.xhr();
						xhr.upload.onprogress = function(e) {
							porcentaje( Math.floor(e.loaded / e.total *100) );
						};
						return xhr;
					},
					success: function( response ) {
						var videoId = response.split( '/publish/' )[1];
						porcentaje( 100 );
						var formData = new FormData();
						formData.append( 'coachId', obtenSesion( 'coachId' ) );
						formData.append( 'circleId', $( '#circleId' ).val() );
						formData.append( 'tabla', 'circles' );
						formData.append( 'folder', 'circles' );
						formData.append( 'folderActivo', 'circles' );
						formData.append( 'nombre', $( '#circleNombre' ).val() );
						formData.append( 'description', $( '#descriptionCircle' ).val() );
						formData.append( 'video', videoId );
						formData.append( 'clientId', clientId );
						var publicCircle = $( '#publicCircle' ).is( ':checked' ) ? 'S' : 'N';
						formData.append( 'public', publicCircle );
						formData.append( 'parentId', $( '#circleParentId' ).val() );
						var formMultiple = [];
						var info = { 'destino': 'circlecategories', 'ids': $( '#categoryIdCircle' ).val().join( '-' ) };
						formMultiple.push( info );
						info = { 'destino': 'usercircle', 'ids': $( '#studentCircleAproved' ).val().join( '-' ) };
						formMultiple.push( info );
						info = { 'destino': 'coachcircle', 'ids': $( '#coachCircleAproved' ).val().join( '-' ) };
						formMultiple.push( info );
						formData.append( 'multiples', JSON.stringify( formMultiple ) );
						if ( $( '#photoCircle' ).length ) {
							if ( $( '#photoCircle' )[0].files.length ) {
								var files = $( '#photoCircle' ).prop( 'files' );
								$.each( files, function( key, value ) {
									formData.append( 'photo', value );
								} );
							}
						}
						ajaxLlamada = $.ajax( {
							type: 'POST',
							url: 'admin/querys/guarda',
							data: formData,
							processData: false,
							contentType: false,
							async: true,
							success: function( response ) {
								var respuesta = JSON.parse( response );
								cargando( false );
								if ( 'Success' === respuesta.status ) {
									if ( $( '#circleId' ).val() == 0 ) {
										seteaSesion( 'nombreCircle', $( '#circleNombre' ).val() );
										popup( 'circleInfo', false );
										alertaTmp( '', 'Successfully saved', 'success' );
										$( '#circleCoach' ).append( '<option value="' + respuesta.id + '">' + $( '#circleNombre' ).val() + '</option>' )
									} else {
										popup( 'circleInfo', false );
										alertaTmp( '', 'Updated', 'success' );
										if ( obtenSesion( 'cargandoUsuarios' ) ) {
											listaCircles();
											borraSesion( 'cargandoUsuarios' )
										} else {
											console.log( 'cargando info circles' );
											cargaCircle();
										}
									}
								}
							}
						} );
					}
				} );
			} else {
				alerta( '', 'Please choose at least one category', 'error' );
			}
		} else {
			if ( $( '#categoryIdCircle' ).val().length > 0 ) {
				cargando( true );
				var formData = new FormData();
				formData.append( 'coachId', obtenSesion( 'coachId' ) );
				formData.append( 'circleId', $( '#circleId' ).val() );
				formData.append( 'tabla', 'circles' );
				formData.append( 'folder', 'circles' );
				formData.append( 'folderActivo', 'circles' );
				formData.append( 'nombre', $( '#circleNombre' ).val() );
				formData.append( 'description', $( '#descriptionCircle' ).val() );
				formData.append( 'clientId', clientId );
				var publicCircle = $( '#publicCircle' ).is( ':checked' ) ? 'S' : 'N';
				formData.append( 'public', publicCircle );
				formData.append( 'parentId', $( '#circleParentId' ).val() );
				var formMultiple = [];
				var info = { 'destino': 'circlecategories', 'ids': $( '#categoryIdCircle' ).val().join( '-' ) };
				formMultiple.push( info );
				info = { 'destino': 'usercircle', 'ids': $( '#studentCircleAproved' ).val().join( '-' ) };
				formMultiple.push( info );
				info = { 'destino': 'coachcircle', 'ids': $( '#coachCircleAproved' ).val().join( '-' ) };
				formMultiple.push( info );
				formData.append( 'multiples', JSON.stringify( formMultiple ) );
				if ( $( '#photoCircle' ).length ) {
					if ( $( '#photoCircle' )[0].files.length ) {
						var files = $( '#photoCircle' ).prop( 'files' );
						$.each( files, function( key, value ) {
							formData.append( 'photo', value );
						} );
					}
				}
				ajaxLlamada = $.ajax( {
					type: 'POST',
					url: 'admin/querys/guarda',
					data: formData,
					processData: false,
					contentType: false,
					async: true,
					xhr: function() {
						var xhr = $.ajaxSettings.xhr();
						xhr.upload.onprogress = function(e) {
							porcentaje( Math.floor(e.loaded / e.total *100) );
						};
						return xhr;
					},
					success: function( response ) {
						var respuesta = JSON.parse( response );
						cargando( false );
						if ( 'Success' === respuesta.status ) {
							if ( $( '#circleId' ).val() == 0 ) {
								seteaSesion( 'nombreCircle', $( '#circleNombre' ).val() );
								popup( 'circleInfo', false );
								alertaTmp( '', 'Successfully saved', 'success' );
								$( '#circleCoach' ).append( '<option value="' + respuesta.id + '">' + $( '#circleNombre' ).val() + '</option>' )
							} else {
								popup( 'circleInfo', false );
								alertaTmp( '', 'Updated', 'success' );
								if ( obtenSesion( 'cargandoUsuarios' ) ) {
									cargaCircle();
								} else {
									listaCircles();
									borraSesion( 'cargandoUsuarios' );
								}
							}
						}
					}
				} );
			} else {
				alerta( '', 'Please choose at least one category', 'error' );
			}
		}
	} );
	$( '.addCircle' ).click(function(event) {
		vacia( 'circleForm' );
		$( '#imgShowCircle' ).html( '' );
		popup( 'circleInfo', true );
	} );
	$( '.editaCircle' ).click(function(event) {
		if ( $( '#circleCoach' ).val() != '' ) {
			$( '#imgShowCircle' ).html( '' );
			if ( $( this ).attr( 'edicion' ) == 2 ) {
				seteaSesion( 'cargandoUsuarios', true );
			}
			traeCircleData( $( '#circleCoach' ).val() );
		}
	} );
	$( '.borraCircle' ).click(function(event) {
		if ( $( '#circleCoach' ).val() != '' ) {
			confirmacion( '', 'Are you sure to delete this circle?', borraCircle )
		}
	} );
	$( '.addSerie' ).click(function(event) {
		//ingresaDato( 'Enter the name of your serie', guardaSerie );
		vacia( 'serieForm' );
		$( '#imgShowSerie' ).html( '' );
		popup( 'serieInfo', true );
	} );
	$( '.editaSerie' ).click(function(event) {
		if ( $( '#serieCoach' ).val() != '' ) {
			$( '#imgShowSerie' ).html( '' );
			//ingresaDatoPrevio( 'Enter the new name of this serie', $( '#serieCoach option:selected' ).text(), actualizaSerie );
			traeSerieData( $( '#serieCoach' ).val() );
		}
	} );
	$( '.borraSerie' ).click(function(event) {
		if ( $( '#serieCoach' ).val() != '' ) {
			confirmacion( '', 'Are you sure to delete this serie?', borraSerie )
		}
	} );
	$( '.addArticleSerie' ).click(function(event) {
		//ingresaDato( 'Enter the name of your serie', guardaSerie );
		vacia( 'serieArticleForm' );
		$( '#imgShowArticleSerie' ).html( '' );
		popup( 'serieArticleInfo', true );
	} );
	$( '.editaArticleSerie' ).click(function(event) {
		if ( $( '#serieArticleCoach' ).val() != '' ) {
			$( '#imgShowSerie' ).html( '' );
			//ingresaDatoPrevio( 'Enter the new name of this serie', $( '#serieCoach option:selected' ).text(), actualizaSerie );
			traeArticleSerieData( $( '#serieArticleCoach' ).val() );
		}
	} );
	$( '.borraArticleSerie' ).click(function(event) {
		if ( $( '#serieArticleCoach' ).val() != '' ) {
			confirmacion( '', 'Are you sure to delete this serie?', borraArticleSerie )
		}
	} );
	$( '.tabs div' ).click( function( e ) {
		$( '.tabs div' ).removeClass( 'selected' );
		$( this ).addClass( 'selected' );
		var abreDiv = $( this ).attr( 'abre' );
		$( '#' + tabContentAbierto ).slideToggle( 'slow', function() {
			console.log( tabContentAbierto );
			tabContentAbierto = abreDiv;
			console.log( tabContentAbierto );
			$( '#' + tabContentAbierto ).slideToggle();
		} );
	} );
	$( '.stopUpload' ).click( function( event ) {
		ajaxLlamada.abort();
		if ( $( '.popup' ).hasClass( 'popupActivo' ) ) {
			$( '.cargando' ).fadeOut( "slow" );
			$( '#porcentajeValor' ).html( '' );
			$( '#porcentajeValor' ).hide();
			$( '.stopUpload' ).hide();
		} else {
			cargando( false );
			cierraPop();
		}
	} );
	$( '.bMobile, .cierraMobile' ).click( function( e ) {
		e.preventDefault();
		$( '.dMenu' ).slideToggle();
	} );
	$( '.filtro input' ).click( function() {
		var filtro = $( this ).attr( 'filtro' );
		$( '.triple.video' ).hide();
		$( '.' + filtro ).show();
	} );
	$( '#mainSlide' ).bxSlider( {
		auto: true,
		nextText: '<i class="fas fa-angle-right"></i>',
		prevText: '<i class="fas fa-angle-left"></i>'
	} );
	if ( $( window ).width() < 664 ) {
		console.log( 'movil' );
		$( '.pcMovil' ).each( function() {
			var imgMovil = $( this ).attr( 'movil' );
			if ( $( this ).attr( 'src' ) != imgMovil ) {
				$( this ).attr( 'src', imgMovil );
			}
		} );
	} else {
		console.log( 'pc' );
		$( '.pcMovil' ).each( function() {
			var imgPc = $( this ).attr( 'pc' );
			if ( $( this ).attr( 'src' ) != imgPc ) {
				$( this ).attr( 'src', imgPc );
			}
		} );
	}
	$( window ).resize( function() {
		if ( $( window ).width() < 664 ) {
			console.log( 'movil' );
			$( '.pcMovil' ).each( function() {
				var imgMovil = $( this ).attr( 'movil' );
				if ( $( this ).attr( 'src' ) != imgMovil ) {
					$( this ).attr( 'src', imgMovil );
				}
			} );
		} else {
			console.log( 'pc' );
			$( '.pcMovil' ).each( function() {
				var imgPc = $( this ).attr( 'pc' );
				if ( $( this ).attr( 'src' ) != imgPc ) {
					$( this ).attr( 'src', imgPc );
				}
			} );
		}
	} );
	if ( archivoUsado == 'sessions.php' ) {
		sessionList( '' );
	} else if ( archivoUsado == 'maestro.php' ) {
		sessionList( $( '#coachCallPick' ).val() );
	} else if( archivoUsado == 'circulos.php' ) {
		sessionListCircle( $( '#circleIdActive' ).val() );
	}
	if ( $( '.swiper-container' ).length ) {
		var swiper = new Swiper( '.swiper-container', {
			slidesPerView: 5,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				320: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				550: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
				780: {
					slidesPerView: 4,
					spaceBetween: 40,
				},
				1080: {
					slidesPerView: 5,
	      			spaceBetween: 30
				}
			}
		} );
	}
} );
function passCambiado( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		$( '#passForm' ).html( '<h2 class="tCenter">The new password was assigned successfully</h2>' );
	} else {
		alerta( '', 'There was a problem, try again', 'error' );
	}
}
function checaAprobados( accesoId ) {
	$( '.eligeEstudiante' ).hide();
	$( '.eligeCircle' ).hide();
	if ( accesoId == 2 ) { $( '.eligeEstudiante' ).show(); }
	if ( accesoId == 4 ) { $( '.eligeCircle' ).show(); }
}
function checaLlamada( accesoId ) {
	console.log( accesoId );
	$( '#divUserCall' ).hide();
	$( '.divUserCircle' ).hide();
	if ( accesoId == 2 ) { $( '#divUserCall' ).show(); }
	if ( accesoId == 4 ) { $( '.divUserCircle' ).show(); }
}
function enviar() {
	if ( validaTodo( 'contactoForm' ) ) {
		var parametros = { 'nombre': $( '#nombre' ).val(), 'email': $( '#email' ).val(), 'telefono': $( '#telefono' ).val(), 'mensaje': $( '#mensaje' ).val(), 'clientId': clientId };
		ajaxRequest( 'POST', parametros, 'panel/querys/enviar', respuestaEnviar );
	}
}
function respuestaEnviar( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		var titulo = ( idiomaActual == 'ES' ) ? 'Éxito' : 'Success';
		var mensaje = ( idiomaActual == 'ES' ) ? 'Enviado con exito, en breve te contactaremos' : 'Sent successfully, we will contact you shortly';
		alerta( titulo, mensaje, 'success' )
		vacia( 'contactForm' );
	} else {
		errorCallback();
	}
}
function guardaCoach() {
	if ( $( '#video' ).length && $( '#video' )[0].files.length ) {
		cargando( true );
		var formData = new FormData();
		formData.append( 'vooKey', vooKey );
		formData.append( 'create', 1 );
		formData.append( 'customS3', 0 );
		var files = $( '#video' ).prop( 'files' );
		$.each( files, function( key, value ) {
			formData.append( 'file', value );
			formData.append( 'name', $( '#nombre' ).val() );
		} );
		ajaxLlamada = $.ajax( {
			type: 'POST',
			url: 'https://api.vooplayer.com/api/createVideo',
			data: formData,
			processData: false,
			contentType: false,
			async: true,
			xhr: function() {
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function(e) {
					porcentaje( Math.floor(e.loaded / e.total *100) );
				};
				return xhr;
			},
			success: function( response ) {
				var videoId = response.split( '/publish/' )[1];
				porcentaje( 100 );
				var formData = new FormData();
				formData.append( 'tabla', 'coaches' );
				formData.append( 'folderActivo', 'coaches' );
				$( '#coachForm input[type="text"], #coachForm input[type="email"], #coachForm input[type="password"], #coachForm input[type="hidden"], #coachForm textarea, #coachForm select' ).each( function() {
					if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
						formData.append( $( this ).attr( 'id' ), $( this ).val() );
					}
				} );
				formData.append( 'video', videoId );
				formData.append( 'clientId', clientId );
				if ( $( '#header' ).length ) {
					if ( $( '#header' )[0].files.length ) {
						var files = $( '#header' ).prop( 'files' );
						$.each( files, function( key, value ) {
							formData.append( 'header', value );
						} );
					}
				}
				if ( $( '#foto' ).length ) {
					if ( $( '#foto' )[0].files.length ) {
						var files = $( '#foto' ).prop( 'files' );
						$.each( files, function( key, value ) {
							formData.append( 'foto', value );
						} );
					}
				}
				ajaxLlamada = $.ajax( {
					type: 'POST',
					url: 'admin/querys/guarda',
					data: formData,
					processData: false,
					contentType: false,
					async: true,
					success: function( response ) {
						var respuesta = JSON.parse( response );
						cargando( false );
						if ( 'Success' === respuesta.status ) {
							vaciaSesion();
							if ( $( '#coachId' ).val() == 0 ) {
								vacia( 'coachForm' );
								confirmacionRegistro();
							} else {
								alerta( 'Success', 'Successfully stored', 'success' );
								seteaSesion( 'coachId', respuesta.id );
								seteaSesion( 'nombre', $( '#nombre' ).val() );
								seteaSesion( 'email', $( '#email' ).val() );
								desmarca( 'coachForm' );
								traeCoach();
							}
						} else if ( 'Repetido' === respuesta.status ) {
							alerta( 'Error', 'This email is already registereded', 'error' );
						} else {
							alerta( 'Error', 'Something happened try again', 'error' );
						}
					}
				} );
			}
		} );
	} else {
		var formData = new FormData();
		formData.append( 'tabla', 'coaches' );
		formData.append( 'folderActivo', 'coaches' );
		$( '#coachForm input[type="text"], #coachForm input[type="email"], #coachForm input[type="password"], #coachForm input[type="hidden"], #coachForm textarea, #coachForm select' ).each( function() {
			if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
				formData.append( $( this ).attr( 'id' ), $( this ).val() );
			}
		} );
		formData.append( 'clientId', clientId );
		if ( $( '#foto' ).length ) {
			if ( $( '#foto' )[0].files.length ) {
				var files = $( '#foto' ).prop( 'files' );
				$.each( files, function( key, value ) {
					formData.append( 'foto', value );
				} );
			}
		}
		if ( $( '#header' ).length ) {
			if ( $( '#header' )[0].files.length ) {
				var files = $( '#header' ).prop( 'files' );
				$.each( files, function( key, value ) {
					formData.append( 'header', value );
				} );
			}
		}
		ajaxLlamada = $.ajax( {
			type: 'POST',
			url: 'admin/querys/guarda',
			data: formData,
			processData: false,
			contentType: false,
			async: true,
			success: function( response ) {
				var respuesta = JSON.parse( response );
				cargando( false );
				if ( 'Success' === respuesta.status ) {
					vaciaSesion();
					if ( $( '#coachId' ).val() == 0 ) {
						vacia( 'coachForm' );
						confirmacionRegistro();
					} else {
						alerta( 'Success', 'Successfully stored', 'success' );
						seteaSesion( 'coachId', respuesta.id );
						seteaSesion( 'nombre', $( '#nombre' ).val() );
						seteaSesion( 'email', $( '#email' ).val() );
						desmarca( 'coachForm' );
						traeCoach();
					}
				} else if ( 'Repetido' === respuesta.status ) {
					alerta( 'Error', 'This email is already registered', 'error' );
				} else {
					alerta( 'Error', 'Something happened try again', 'error' );
				}
			}
		} );
	}
	if ( obtenSesion( 'pending' ) == 'R' ) {
		var parametros = { 'clientId': clientId, 'coachId': obtenSesion( 'coachId' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/cambio-perfil', respuestaPerfil );
	}
}
function confirmacionRegistro() {
	swal(
		'Confirm you email address',
		'Thank you for registering. We sent a confirmation link to your email. Please click that link to confirm your account before you can start using it.',
		'success'
	).then( ( value ) => {
		manda( '/' );
	} );
}
function respuestaPerfil( respuesta ) {
	cargando( false );
	console.log( respuesta );
}
function guardaStudent() {
	var formData = new FormData();
	formData.append( 'tabla', 'users' );
	formData.append( 'folderActivo', 'users' );
	$( '#studentForm input[type="text"], #studentForm input[type="email"], #studentForm input[type="password"], #studentForm input[type="hidden"], #studentForm textarea, #studentForm select' ).each( function() {
		if ( typeof $( this ).attr( 'campo' ) != 'undefined' && $( this ).val() != '' ) {
			formData.append( $( this ).attr( 'campo' ), $( this ).val() );
		}
	} );
	formData.append( 'clientId', clientId );
	if ( $( '#fotoUser' ).length ) {
		if ( $( '#fotoUser' )[0].files.length ) {
			var files = $( '#fotoUser' ).prop( 'files' );
			$.each( files, function( key, value ) {
				formData.append( 'foto', value );
			} );
		}
	}
	ajaxLlamada = $.ajax( {
		type: 'POST',
		url: 'admin/querys/guarda',
		data: formData,
		processData: false,
		contentType: false,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			cargando( false );
			if ( 'Success' === respuesta.status ) {
				alerta( 'Success', 'Successfully stored', 'success' );
				vaciaSesion();
				if ( $( '#userId' ).val() == 0 ) {
					vacia( 'studentForm' );
					seteaSesion( 'studentTmp', respuesta.id );
					if ( cuestionarioActivo == 'S' ) {
						manda( 'questionary' );
					} else {
						confirmacionRegistro();
					}
				} else {
					seteaSesion( 'studentId', respuesta.id );
					seteaSesion( 'nombre', $( '#nombreUser' ).val() );
					seteaSesion( 'email', $( '#emailUser' ).val() );
					desmarca( 'studentForm' );
					traeStudent();
				}
			} else if ( 'Repetido' === respuesta.status ) {
				alerta( 'Error', 'This email is already registered', 'error' );
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		}
	} );
	if ( obtenSesion( 'pending' ) == 'R' ) {
		var parametros = { 'clientId': clientId, 'studentId': obtenSesion( 'studentId' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/cambio-perfil', respuestaPerfil );
	}
}
function vaciaSesion() {
	console.log( 'vaciando sesion' );
	borraSesion( 'coachId' );
	borraSesion( 'studentId' );
	borraSesion( 'nombre' );
	borraSesion( 'email' );
	borraSesion( 'pending' );
	borraSesion( 'tokenZoom' );
}
function logout() {
	vaciaSesion();
	manda( '/' );
}
function porcentaje( valor ) {
	console.log( valor );
	$( '#porcentajeValor' ).show();
	$( '.stopUpload' ).show();
	$( '#porcentajeValor' ).html( valor + '% Complete' );
}
function guardaExamen() {
	var i = 0;
	var questions = [];
	$( '#preguntasForm select' ).each( function() {
		questions[ i ] = { 'questionId': $( this ).attr( 'preguntaId' ), 'respuesta': $( this ).val() };
		i++;
	} );
	var parametros = { 'clientId': clientId, 'userId': obtenSesion( 'studentTmp' ), 'questions': JSON.stringify( questions ) };
	ajaxRequest( 'POST', parametros, 'admin/querys/respuestas', respuestaCuestionario );
}
function respuestaCuestionario( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		borraSesion( 'studentTmp' );
		confirmacionRegistro();
	} else {
		errorCallback();
	}
}
function editaArticle( articleId ) {
	var parametros = { 'tabla': 'articles', 'folder': 'articles', 'id': articleId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaEditaArticle );
}
function respuestaEditaArticle( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#articleId' ).val( datos.articleId );
		$( '#titleArticle' ).val( datos.titulo );
		/*$( tinymce.get( 'descriptionArticle' ).getBody() ).html( datos.description );*/
		$( '#descriptionArticle' ).val( datos.description );
		if ( datos.imagen != null && datos.imagen != '' ) {
			$( '#imgShowArticle' ).html( '<img src="images/articles/' + datos.imagen + '" class="thumbCoach">' );
		}
		if ( datos.pdf != null && datos.pdf != '' ) {
			$( '#pdfShowArticle' ).html( '<a href="images/articles/' + datos.pdf + '" target="_blank" style="font-size: 8px;">' + datos.pdf + '</a>' );
		}
		$( '#accessArticleSerie' ).val( datos.accessId );
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachArticleAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#circlesAsignedArticle' ).val( multiples ).trigger( 'change' );
				} else {
					$( '#studentArticleAproved' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		checaAprobados( datos.accessId );
		popup( 'articleAlta', true );
	} else {
		errorCallback();
	}
}
function respuestaSerieMuestra( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#serieId' ).val( datos.serieId );
		$( '#accessSerie' ).val( datos.accessId );
		$( '#serieNombre' ).val( datos.nombre );
		$( '#descriptionSerie' ).val( datos.description );
		if ( datos.video != null && datos.video != '' ) {
			$( '#serieForm .currentVideo' ).attr( 'onclick', 'muestraVideo( \'' + datos.video + '\' )' );
			$( '#serieForm .currentVideo' ).show();
		}
		if ( datos.photo != null && datos.photo != '' ) {
			$( '#imgShowSerie' ).html( '<img src="images/series/' + datos.photo + '" class="thumbCoach">' );
		}
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdSerie' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-0' ) {
					$( '#studentAproved' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		popup( 'serieInfo', true );
		checaAprobados( datos.accessId );
	} else {
		errorCallback();
	}
}
function editaAudio( audioId ) {
	var parametros = { 'tabla': 'audios', 'folder': 'audios', 'id': audioId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaEditaAudio );
}
function respuestaEditaAudio( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#audioId' ).val( datos.audioId );
		$( '#accessAudio' ).val( datos.accessId );
		$( '#tituloAudio' ).val( datos.titulo );
		$( '#descriptionAudio' ).val( datos.description );
		if ( datos.imagen != null && datos.imagen != '' ) {
			$( '#imgShowAudio' ).html( '<img src="images/audios/' + datos.imagen + '" class="thumbCoach">' );
		}
		$( '#audioPlay' ).html( '<audio controls><source src="images/audios/' + datos.audio + '" type="audio/mp3"></audio>' );
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachAprovedAudio' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdAudio' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-0' ) {
					$( '#studentAprovedAudio' ).val( multiples ).trigger( 'change' );
				} else {
					$( '#circlesAsignedAudio' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		checaAprobados( datos.accessId );
		popup( 'audioAlta', true );
	} else {
		errorCallback();
	}
}
function editaVideo( videoId ) {
	var parametros = { 'tabla': 'videos', 'folder': 'videos', 'id': videoId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaEditaVideo );
}
function respuestaEditaVideo( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#videoPop' ).val( '' );
		$( '#videoId' ).val( datos.videoId );
		$( '#accessSerie' ).val( datos.accessId );
		$( '#titulo' ).val( datos.titulo );
		$( '#description' ).val( datos.description );
		$( '#videoPop' ).removeAttr( 'required' );
		$( '#videoAlta .currentVideo' ).attr( 'onclick', 'muestraVideo( \'' + datos.video + '\' )' );
		$( '#videoAlta .currentVideo' ).show();
		if ( datos.imagen != null && datos.imagen != '' ) {
			$( '#imgShowVideo' ).html( '<img src="images/videos/' + datos.imagen + '" class="thumbCoach">' );
		}
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdSerie' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-0' ) {
					$( '#studentAproved' ).val( multiples ).trigger( 'change' );
				} else {
					$( '#circlesAsigned' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		checaAprobados( datos.accessId );
		popup( 'videoAlta', true );
	} else {
		errorCallback();
	}
}
function traeStudent() {
	var parametros = { 'tabla': 'users', 'folder': 'users', 'id': obtenSesion( 'studentId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaEstudiante );
}
function respuestaEstudiante( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#userId' ).val( datos.userId );
		$( '#nombreUser' ).val( datos.nombre );
		$( '#apellidoUser' ).val( datos.apellido );
		$( '#emailUser' ).val( datos.email );
		$( '#passUser' ).val( datos.pass );
		$( '#nombrePerfil' ).html( datos.nombre + ' ' + datos.apellido );
		//$( '#fotoUser' ).val( datos.foto );
		if ( datos.foto != null && datos.foto != '' ) {
			$( '#imgShowUser' ).html( '<img src="images/users/' + datos.foto + '" class="thumbCoach">' );
		}
		if ( !$( '.studentData' ).is( ':visible' ) ) {
			$( '.studentData' ).slideToggle();
		}
		verificaAutorizacionMember();
	} else {
		errorCallback();
	}
}
function traeArticleSerieData( serieId ) {
	var parametros = { 'tabla': 'articleseries', 'folder': 'articleseries', 'id': serieId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaSerieArticleMuestra );
}
function respuestaSerieArticleMuestra( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#serieArticleId' ).val( datos.serieId );
		$( '#accessArticleSerie' ).val( datos.accessId );
		$( '#serieArticleNombre' ).val( datos.nombre );
		$( '#descriptionSerieArticle' ).val( datos.description );
		if ( datos.photo != null && datos.photo != '' ) {
			$( '#imgShowArticleSerie' ).html( '<img src="images/articleseries/' + datos.photo + '" class="thumbCoach">' );
		}
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachArticleAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdSerieArticle' ).val( multiples ).trigger( 'change' );
				} else {
					$( '#studentArticleAproved' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		popup( 'serieArticleInfo', true );
		checaAprobados( datos.accessId );
	} else {
		errorCallback();
	}
}
function traeCircleData( circleId ) {
	var parametros = { 'tabla': 'circles', 'folder': 'circles', 'id': circleId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaCircleMuestra );
}
function respuestaCircleMuestra( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#circleId' ).val( datos.circleId );
		$( '#circleNombre' ).val( datos.nombre );
		$( '#descriptionCircle' ).val( datos.description );
		if ( datos.video != null && datos.video != '' ) {
			$( '#circleForm .currentVideo' ).attr( 'onclick', 'muestraVideo( \'' + datos.video + '\' )' );
			$( '#circleForm .currentVideo' ).show();
		}
		if ( datos.photo != null && datos.photo != '' ) {
			$( '#imgShowCircle' ).html( '<img src="images/circles/' + datos.photo + '" class="thumbCoach">' );
		}
		var publicCircle = ( datos.public == 'S' ) ? true : false;
		$( '#publicCircle' ).prop( 'checked', publicCircle );
		$( '#circleParentId' ).val( datos.parentId );
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachCircleAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdCircle' ).val( multiples ).trigger( 'change' );
				} else {
					$( '#studentCircleAproved' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		popup( 'circleInfo', true );
	} else {
		errorCallback();
	}
}
function traeSerieData( serieId ) {
	var parametros = { 'tabla': 'series', 'folder': 'series', 'id': serieId, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaSerieMuestra );
}
function respuestaSerieMuestra( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#serieId' ).val( datos.serieId );
		$( '#accessSerie' ).val( datos.accessId );
		$( '#serieNombre' ).val( datos.nombre );
		$( '#descriptionSerie' ).val( datos.description );
		if ( datos.video != null && datos.video != '' ) {
			$( '#serieForm .currentVideo' ).attr( 'onclick', 'muestraVideo( \'' + datos.video + '\' )' );
			$( '#serieForm .currentVideo' ).show();
		}
		if ( datos.photo != null && datos.photo != '' ) {
			$( '#imgShowSerie' ).html( '<img src="images/series/' + datos.photo + '" class="thumbCoach">' );
		}
		$.each( datos, function( key, value ) {
			console.log( key );
			if ( key.indexOf( 'multiple' ) != -1 ) {
				var multiples = value.split( '-' );
				if ( key == 'multiple-1' ) {
					$( '#coachAproved' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-2' ) {
					$( '#categoryIdSerie' ).val( multiples ).trigger( 'change' );
				} else if ( key == 'multiple-0' ) {
					$( '#studentAproved' ).val( multiples ).trigger( 'change' );
				}
			}
		} );
		popup( 'serieInfo', true );
		checaAprobados( datos.accessId );
	} else {
		errorCallback();
	}
}
function traeCoach() {
	var parametros = { 'tabla': 'coaches', 'folder': 'coaches', 'id': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaCoach );
}
function respuestaCoach( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		$( '#coachId' ).val( datos.coachId );
		$( '#categoryId' ).val( datos.categoryId );
		$( '#nombre' ).val( datos.nombre );
		$( '#apellido' ).val( datos.apellido );
		$( '#phrase' ).val( datos.phrase );
		$( '#biografia' ).val( datos.biografia );
		$( '#email' ).val( datos.email );
		$( '#pass' ).val( datos.pass );
		$( '#nombrePerfil' ).html( datos.nombre + ' ' + datos.apellido );
		if ( datos.video != null && datos.video != '' ) {
			$( '#coachForm .currentVideo' ).attr( 'onclick', 'muestraVideo( \'' + datos.video + '\' )' );
			$( '#coachForm .currentVideo' ).show();
		}
		if ( datos.foto != null && datos.foto != '' ) {
			$( '#imgShowCoach' ).html( '<img src="images/coaches/' + datos.foto + '" class="thumbCoach">' );
		}
		if ( datos.header != null && datos.header != '' ) {
			$( '#imgShowCoachHead' ).html( '<img src="images/coaches/' + datos.header + '" class="thumbCoach">' );
		}
		$( '#facebook' ).val( datos.facebook );
		$( '#twitter' ).val( datos.twitter );
		$( '#web' ).val( datos.web );
		if ( !$( '.coachData' ).is( ':visible' ) ) {
			$( '.coachData' ).slideToggle();
		}
		verificaAutorizacion();
		if ( datos.zoomToken != null && datos.zoomToken != '' ) {
			$( '.zoomActivar' ).hide();
			$( '#zoomSecret' ).val( datos.token );
			seteaSesion( 'tokenZoom', datos.zoomToken );
			seteaSesion( 'tokenAuth', datos.zoomAuth );
			$( '.zoomLogout' ).show();
		}
	} else {
		errorCallback();
	}
}
function verificaAutorizacionMember() {
	var parametros = { 'studentId': obtenSesion( 'studentId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/verifica-miembro', respuestaVerifica );
}
function verificaAutorizacion() {
	var parametros = { 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/verifica', respuestaVerifica );
}
function respuestaVerifica( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		seteaSesion( 'pending', datos.pendiente );
		if ( datos.pendiente != 'R' ) {
			borraSesion( 'razones' );
		} else {
			if ( datos.razones != '' ) {
				seteaSesion( 'razones', datos.razones );
			} else {
				borraSesion( 'razones' );
			}
		}
		checaPendientes();
	}
}
function iniciaSesion( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		vaciaSesion();
		if ( datos.tipo == 1 ) {
			seteaSesion( 'coachId', datos.id );
			seteaSesion( 'pending', datos.pendiente );
			if ( datos.token != '' ) {
				seteaSesion( 'tokenZoom', datos.token );
			}
		} else {
			seteaSesion( 'studentId', datos.id );
		}
		seteaSesion( 'nombre', $( '#nombreUser' ).val() );
		seteaSesion( 'email', $( '#emailUser' ).val() );
		if ( datos.permitido == 1 ) {
			if ( checaSesion( 'llamadaPendiente' ) ) {
				manda( 'coach/' + obtenSesion( 'llamadaPendiente' ) );
			} else {
				manda( 'account' );
			}
		} else {
			confirmacion( '', 'Your shala account doesn\'t have currently access to this platform, do you want to add it to your platforms access?', confirmaPlataforma )
		}
	} else if ( datos.status == 'Email' ) {
		vaciaSesion();
		alertaTmp( '', 'We are still waiting for your email confirmation. Please check your email and confirm through the link sent.', 'error' );
	} else {
		alertaTmp( '', 'Incorrect Access Data', 'error' );
	}
}
function checaAccesoSitio() {
	if ( securePass && checaSesion( 'siteSesion' ) ) {
		$( '.loginSite' ).hide();
	} else {
		$( '#cuadroSesion' ).show();
	}
}
function iniciaSitio( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alerta( '', 'Site access granted', 'success' );
		seteaSesion( 'siteSesion', true );
		checaAccesoSitio();
	} else {
		alerta( '', 'Incorrect Access Data', 'error' );
	}
}
function confirmaPlataforma( status ) {
	if ( status ) {
		var tablaPlatform = ( checaSesion( 'coachId' ) ) ? 'coachesclients': 'userclient';
		var parametros = { 'tabla': tablaPlatform, 'folderActivo': tablaPlatform, 'coachClientId ': 0, 'userClientId': 0, 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId, 'userId': obtenSesion( 'studentId' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaPlataforma );
	} else {
		logout();
	}
}
function respuestaPlataforma( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		manda( 'account' );
	} else {
		alertaTmp( '', 'Somehting goes wrong try again', 'error' );
		logout();
	}
}
function actualizaSerie( nombreSerie ) {
	if ( nombreSerie != '' && nombreSerie != null && $( '#serieCoach' ).val() != '' ) {
		if ( $( '#serieCoach option:selected' ).text() != nombreSerie ) {
			seteaSesion( 'serieActiva', $( '#serieCoach' ).val() );
			var parametros = { 'tabla': 'series', 'folderActivo': 'series', 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId, 'serieId': $( '#serieCoach' ).val(), 'nombre': nombreSerie };
			ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaSerieNew );
		}
	}
}
function guardaSerie( nombreSerie ) {
	if ( nombreSerie != '' && nombreSerie != null ) {
		seteaSesion( 'nombreSerie', nombreSerie );
		var parametros = { 'tabla': 'series', 'folderActivo': 'series', 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId, 'serieId': 0, 'nombre': nombreSerie };
		ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaSerie );
	}
}
function borraSerie( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'series', 'folder': 'series', 'id': $( '#serieCoach' ).val() };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaSerieNew );
	}
}
function borraCircle( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'circles', 'folder': 'circles', 'id': $( '#circleCoach' ).val() };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaCircleNew );
	}
}
function respuestaCircleNew( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		popup( 'circleInfo', false );
		alertaTmp( '', 'Updated', 'success' );
		listaCircles();
	} else {
		alerta( '', 'Somehting goes wrong try again', 'error' );
	}
}
function respuestaSerieNew( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		popup( 'serieInfo', false );
		alertaTmp( '', 'Updated', 'success' );
		listaSeries();
		$( '#videoTable' ).hide();
	} else {
		alerta( '', 'Somehting goes wrong try again', 'error' );
	}
}
function respuestaSerie( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		popup( 'serieInfo', false );
		alertaTmp( '', 'Successfully saved', 'success' );
		$( '#serieCoach' ).append( '<option value="' + datos.id + '">' + obtenSesion( 'nombreSerie' ) + '</option>' )
	} else {
		alerta( '', 'Somehting goes wrong try again', 'error' );
	}
	borraSesion( 'nombreSerie' );
}
function listaCircles() {
	var parametros = { 'tabla': 'circles', 'padre': 'coachId', 'idPadre': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/arregloopciones', respuestaLCircle );
}
function respuestaLCircle( respuesta ) {
	cargando( false );
	var opciones = '<option value="">Pick your circle</option>';
	datos = JSON.parse( respuesta );
	$.each( datos.elementos, function(index, el) {
		opciones += '<option value="' + el.id + '">' + el.nombre + '</option>';
	} );
	$( '#circleCoach' ).html( opciones );
	if ( checaSesion( 'circleActiva' ) ) {
		$( '#circleCoach' ).val( obtenSesion( 'circleActiva' ) );
		borraSesion( 'circleActiva' );
		cargaCircle();
	}
}
function listaSeries() {
	var parametros = { 'tabla': 'series', 'padre': 'coachId', 'idPadre': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/arregloopciones', respuestaLSerie );
}
function respuestaLSerie( respuesta ) {
	cargando( false );
	var opciones = '<option value="">Pick your series</option>';
	datos = JSON.parse( respuesta );
	$.each( datos.elementos, function(index, el) {
		opciones += '<option value="' + el.id + '">' + el.nombre + '</option>';
	} );
	$( '#serieCoach' ).html( opciones );
	if ( checaSesion( 'serieActiva' ) ) {
		$( '#serieCoach' ).val( obtenSesion( 'serieActiva' ) );
		borraSesion( 'serieActiva' );
		cargaVideos();
	}
}
function listaSeriesArticles() {
	var parametros = { 'tabla': 'articleseries', 'padre': 'coachId', 'idPadre': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/arregloopciones', respuestaLASerie );
}
function respuestaLASerie( respuesta ) {
	cargando( false );
	var opciones = '<option value="">Pick your article series</option>';
	datos = JSON.parse( respuesta );
	$.each( datos.elementos, function(index, el) {
		opciones += '<option value="' + el.id + '">' + el.nombre + '</option>';
	} );
	$( '#serieArticleCoach' ).html( opciones );
	if ( checaSesion( 'serieArtActiva' ) ) {
		$( '#serieArticleCoach' ).val( obtenSesion( 'serieArtActiva' ) );
		borraSesion( 'serieArtActiva' );
		cargaArticulos();
	}
}
function borraArticle( articleId ) {
	seteaSesion( 'articleBorra', articleId );
	confirmacion( '', 'Are you sure to delete this article?', continuaBorrandoArticle );
}
function continuaBorrandoArticle( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'articles', 'folder': 'articles', 'id': obtenSesion( 'articleBorra' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaArVideo );
	}
}
function respuestaArVideo( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		borraSesion( 'articleBorra' )
		cargaArticulos();
	}
}
function cargaArticulos() {
	var parametros = { 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/cargaarticulos', respuestaArticulos );
}
function respuestaArticulos( respuesta ) {
	cargando( false );
	var filas = '';
	datos = JSON.parse( respuesta );
	console.log( datos.articulos );
	$.each( datos.articulos, function(index, el) {
		var imagen = ( el.imagen == null ) ? '<td></td>' : '<td align="center"><img src="thumb?src=images/articles/' + el.imagen + '&size=70x50"></td>';
		var pdf = ( el.pdf == null ) ? '<td>Sin archivo</td>' : '<td align="center"><a href="images/articles/' + el.pdf + '" target="_blank">' + el.pdf + '</a></td>';
		filas += '<tr campoId="' + el.id + '">';
		filas += '<td><i class="fas fa-sort" style="cursor: grab; margin-right: 10px"></i><i class="fas fa-trash cPointer" onclick="borraArticle( ' + el.id + ' )"></i> <i class="fas fa-edit cPointer" onclick="editaArticle( ' + el.id + ' )"></i></td>';
		filas += imagen;
		filas += pdf;
		filas += '<td>' + el.titulo + '</td>';
		filas += '<td style="max-width: 300px;">' + el.descripcion + '</td>';
		filas += '</tr>';
	} );
	if ( filas == '' ) {
		filas = '<tr><td colspan="4">There is no article on this serie</td></tr>';
	}
	$( '#contenidoArticle' ).html( filas );
	$( '#articuloTable' ).show();
	filas = '';
	console.log( datos.request );
	$.each( datos.request, function(index, el) {
		filas += '<tr requestId="' + el.id + '">';
		filas += '<td align="center"><i class="fas fa-check cPointer" onclick="userAccesSerie( 1, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i> <i class="fas fa-times cPointer" onclick="userAccesSerie( 2, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i></td>';
		filas += '<td align="center">' + el.nombre + '</td>'
		filas += '<td align="center">' + el.email + '</td>';
		filas += '</tr>';
	} );
	if ( filas != '' ) {
		$( '#contenidoArticleRequest' ).html( filas );
		$( '#requestArticleTable' ).show();
	} else {
		$( '#requestArticleTable' ).hide();
	}
}
function nuevoArticulo() {
	vacia( 'articleAlta' );
	checaAprobados( 0 );
	/*$( tinymce.get( 'descriptionArticle' ).getBody() ).html( '' );*/
	popup( 'articleAlta', true );
}
function nuevoAudio() {
	vacia( 'audioAlta' );
	$( '#audioPlay' ).html( '' );
	popup( 'audioAlta', true );
}
function nuevoVideo() {
	if ( $( '#serieCoach' ).val() != '' ) {
		vacia( 'videoAlta' );
		popup( 'videoAlta', true );
	} else {
		alertaTmp( '', 'You must to select a serie first', 'error' );
	}
}
function borraAudio( audioId ) {
	seteaSesion( 'audioBorra', audioId );
	confirmacion( '', 'Are you sure to delete this audio?', continuaBorrandoAudio );
}
function continuaBorrandoAudio( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'audios', 'folder': 'audios', 'id': obtenSesion( 'audioBorra' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaBAudio );
	}
}
function respuestaBAudio( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		cargaAudios();
	}
}
function borraVideo( videoId ) {
	seteaSesion( 'videoBorra', videoId );
	confirmacion( '', 'Are you sure to delete this video?', continuaBorrando );
}
function continuaBorrando( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'videos', 'folder': 'videos', 'id': obtenSesion( 'videoBorra' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaBVideo );
	}
}
function respuestaBVideo( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		cargaVideos();
	}
}
function cargaCircle() {
	if ( $( '#circleCoach' ).val() != '' ) {
		var parametros = { 'circleId': $( '#circleCoach' ).val() };
		ajaxRequest( 'POST', parametros, 'admin/querys/cargacircle', respuestaCircle );
	} else {
		$( '#coachNameCircle' ).html( '' );
		$( '#usersCircle tbody' ).html( '' );
		$( '#coachesCircle tbody' ).html( '' );
		$( '#requestCircleUser' ).hide();
		$( '#requestCircleCoaches' ).hide();
	}
}
function respuestaCircle( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		$( '#coachNameCircle' ).html( 'Created by ' + datos.coach );
		var circleId = $( '#circleCoach' ).val();
		var filas = '';
		$.each( datos.users, function(index, el) {
			var foto = ( el.photo == null ) ? 'images/silueta.png' : 'images/users/' + el.photo;
			filas += '<tr campoId="' + el.id + '">';
			filas += '<td><i class="fas fa-trash cPointer" onclick="quitaCircle( 1, ' + el.id + ', ' + circleId + ' )"></i></td>';
			filas += '<td align="center"><img src="thumb?src=' + foto + '&size=70x70"></td>';
			filas += '<td>' + el.nombre + '</td>';
			filas += '</tr>';
		} );
		if ( filas == '' ) {
			filas = '<tr><td colspan="3">There is no users on this circle</td></tr>';
		}
		$( '#usersCircle tbody' ).html( filas );
		filas = '';
		$.each( datos.coaches, function(index, el) {
			var foto = ( el.photo == null ) ? 'images/silueta.png' : 'images/coaches/' + el.photo;
			filas += '<tr campoId="' + el.id + '">';
			filas += '<td><i class="fas fa-trash cPointer" onclick="quitaCircle( 2, ' + el.id + ', ' + circleId + ' )"></i></td>';
			filas += '<td align="center"><img src="thumb?src=' + foto + '&size=70x70"></td>';
			filas += '<td>' + el.nombre + '</td>';
			filas += '</tr>';
		} );
		if ( filas == '' ) {
			filas = '<tr><td colspan="3">There is no coaches on this circle</td></tr>';
		}
		$( '#coachesCircle tbody' ).html( filas );
		filas = '';
		$.each( datos.userRequest, function(index, el) {
			filas += '<tr campoId="' + el.id + '">';
			filas += '<td><i class="fas fa-trash cPointer" onclick="quitaCircle( 3, ' + el.id + ', ' + circleId + ' )"></i></td>';
			filas += '<td align="center"><img src="thumb?src=images/users/' + el.photo + '&size=70x50"></td>';
			filas += '<td>' + el.nombre + '</td>';
			filas += '<td class="tCenter"><a onclick="apruebaCircle( 1, ' + el.id + ', ' + circleId + ' )"><i class="fas fa-check cPointer"></i> Approve</a></td>';
			filas += '</tr>';
		} );
		if ( filas == '' ) {
			$( '#requestCircleUser' ).hide();
		} else {
			$( '#requestCircleUser' ).show();
			$( '#requestCircleUser tbody' ).html( filas );
		}
		filas = '';
		$.each( datos.coachRequest, function(index, el) {
			filas += '<tr campoId="' + el.id + '">';
			filas += '<td><i class="fas fa-trash cPointer" onclick="quitaCircle( 4, ' + el.id + ', ' + circleId + ' )"></i></td>';
			filas += '<td align="center"><img src="thumb?src=images/coaches/' + el.photo + '&size=70x50"></td>';
			filas += '<td>' + el.nombre + '</td>';
			filas += '<td class="tCenter"><a onclick="apruebaCircle( 2, ' + el.id + ', ' + circleId + ' )"><i class="fas fa-check cPointer"></i> Approve</a></td>';
			filas += '</tr>';
		} );
		if ( filas == '' ) {
			$( '#requestCircleCoaches' ).hide();
		} else {
			$( '#requestCircleCoaches' ).show();
			$( '#requestCircleCoaches tbody' ).html( filas );
		}
	}
}
function cargaAudios() {
	ajaxRequest( 'POST', { 'coachId': obtenSesion( 'coachId' ) }, 'admin/querys/cargaaudios', respuestaAudios );
}
function respuestaAudios( respuesta ) {
	cargando( false );
	var filas = '';
	datos = JSON.parse( respuesta );
	console.log( datos.audios );
	$.each( datos.audios, function(index, el) {
		filas += '<tr campoId="' + el.id + '">';
		filas += '<td><i class="fas fa-sort" style="cursor: grab; margin-right: 10px"></i><i class="fas fa-trash cPointer" onclick="borraAudio( ' + el.id + ' )"></i> <i class="fas fa-edit cPointer" onclick="editaAudio( ' + el.id + ' )"></i></td>';
		filas += '<td align="center"><img src="thumb?src=images/audios/' + el.imagen + '&size=70x50"></td>'
		filas += '<td><audio controls><source src="images/audios/' + el.audio + '" type="audio/mp3"></audio></td>';
		filas += '<td>' + el.titulo + '</td>'
		filas += '<td style="max-width: 300px;">' + el.descripcion.substring( 0, 100 ) + '...</td>';
		filas += '</tr>';
	} );
	if ( filas == '' ) {
		filas = '<tr><td colspan="4">There is no audios</td></tr>';
	}
	$( '#contenidoAudio' ).html( filas );
	$( '#audioTable' ).show();
	filas = '';
	console.log( datos.request );
	$.each( datos.request, function(index, el) {
		filas += '<tr requestId="' + el.id + '">';
		filas += '<td align="center"><i class="fas fa-check cPointer" onclick="userAccesAudio( 1, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i> <i class="fas fa-times cPointer" onclick="userAccesAudio( 2, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i></td>';
		filas += '<td align="center">' + el.nombre + '</td>'
		filas += '<td align="center">' + el.email + '</td>';
		filas += '</tr>';
	} );
	if ( filas != '' ) {
		$( '#contenidoRequestAudio' ).html( filas );
		$( '#requestAudioTable' ).show();
	} else {
		$( '#requestAudioTable' ).hide();
	}
}
function cargaVideos() {
	ajaxRequest( 'POST', { 'coachId': obtenSesion( 'coachId' ) }, 'admin/querys/cargavideos', respuestaVideos );
}
function respuestaVideos( respuesta ) {
	cargando( false );
	var filas = '';
	datos = JSON.parse( respuesta );
	console.log( datos.videos );
	$.each( datos.videos, function(index, el) {
		filas += '<tr campoId="' + el.id + '">';
		filas += '<td><i class="fas fa-sort" style="cursor: grab; margin-right: 10px"></i><i class="fas fa-trash cPointer" onclick="borraVideo( ' + el.id + ' )"></i> <i class="fas fa-edit cPointer" onclick="editaVideo( ' + el.id + ' )"></i></td>';
		filas += '<td align="center"><img src="thumb?src=images/videos/' + el.imagen + '&size=70x50"></td>'
		filas += '<td><a onclick="muestraVideo( \'' + el.videoId + '\' )" class="resaltaLink">Play the video</a></td>';
		filas += '<td>' + el.titulo + '</td>'
		filas += '<td style="max-width: 300px;">' + el.descripcion.substring( 0, 100 ) + '...</td>';
		filas += '</tr>';
	} );
	if ( filas == '' ) {
		filas = '<tr><td colspan="4">There is no video on this serie</td></tr>';
	}
	$( '#contenidoVideo' ).html( filas );
	$( '#videoTable' ).show();
	filas = '';
	console.log( datos.request );
	$.each( datos.request, function(index, el) {
		filas += '<tr requestId="' + el.id + '">';
		filas += '<td align="center"><i class="fas fa-check cPointer" onclick="userAccesSerie( 1, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i> <i class="fas fa-times cPointer" onclick="userAccesSerie( 2, ' + el.id + ', ' + el.userId + ', \'' + el.nombre + '\', \'' + el.email + '\' )"></i></td>';
		filas += '<td align="center">' + el.nombre + '</td>'
		filas += '<td align="center">' + el.email + '</td>';
		filas += '</tr>';
	} );
	if ( filas != '' ) {
		$( '#contenidoRequest' ).html( filas );
		$( '#requestTable' ).show();
	} else {
		$( '#requestTable' ).hide();
	}
}
function guardaArticle() {
	var formData = new FormData();
	var formMultiple = [];
	var hayMultiple = false;
	formData.append( 'tabla', 'articles' );
	formData.append( 'folderActivo', 'articles' );
	formData.append( 'titulo', $( '#titleArticle' ).val() );
	formData.append( 'articleId', $( '#articleId' ).val() );
	formData.append( 'description', $( '#descriptionArticle' ).val() );
	formData.append( 'clientId', clientId );
	formData.append( 'coachId', obtenSesion( 'coachId' ) );
	formData.append( 'accessId', $( '#accessArticleSerie' ).val() );
	var formMultiple = [];
	info = { 'destino': 'userarticle', 'ids': $( '#studentArticleAproved' ).val().join( '-' ) };
	formMultiple.push( info );
	info = { 'destino': 'coacharticle', 'ids': $( '#coachArticleAproved' ).val().join( '-' ) };
	formMultiple.push( info );
	info = { 'destino': 'articlecircle', 'ids': $( '#circlesAsignedArticle' ).val().join( '-' ) };
	formMultiple.push( info );
	formData.append( 'multiples', JSON.stringify( formMultiple ) );
	if ( $( '#pdfArticle' ).length ) {
		if ( $( '#pdfArticle' )[0].files.length ) {
			var files = $( '#pdfArticle' ).prop( 'files' );
			$.each( files, function( key, value ) {
				formData.append( 'pdf', value );
			} );
		}
	}
	if ( $( '#fotoArticle' ).length ) {
		if ( $( '#fotoArticle' )[0].files.length ) {
			var files = $( '#fotoArticle' ).prop( 'files' );
			$.each( files, function( key, value ) {
				formData.append( 'imagen', value );
			} );
		}
	}
	ajaxLlamada = $.ajax( {
		type: 'POST',
		url: 'admin/querys/guarda',
		data: formData,
		processData: false,
		contentType: false,
		async: true,
		success: function( response ) {
			var respuesta = JSON.parse( response );
			cargando( false );
			if ( 'Success' === respuesta.status ) {
				alertaTmp( '', 'Successfully stored', 'success' );
				desmarca( 'articleForm' );
				if ( $( '#articleId' ).val() == 0 ) {
					vacia( 'articleForm' );
				}
				popup( 'articleAlta', false );
				cargaArticulos();
			} else {
				alerta( 'Error', 'Something happened try again', 'error' );
			}
		}
	} );
}
function guardaAudio() {
	if ( $( '#categoryIdAudio' ).val().length > 0 ) {
		var idVideo = ( $( '#audioId' ).val() != '' ) ? $( '#audioId' ).val() : 0;
		if ( idVideo != 0 || ( idVideo == 0 && $( '#audioFile' )[0].files.length > 0 ) ) {
			var formData = new FormData();
			var hayMultiple = false;
			formData.append( 'tabla', 'audios' );
			formData.append( 'folderActivo', 'audios' );
			$( '#audioForm input[type="text"], #audioForm input[type="email"], #audioForm input[type="password"], #audioForm input[type="hidden"], #audioForm textarea, #audioForm select' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
					var campoId = ( $( this ).attr( 'id' ) == 'accessAudio' ) ? 'accessId' : $( this ).attr( 'campo' );
					formData.append( campoId, $( this ).val() );
				}
			} );
			if ( $( '#fotoAudio' ).length ) {
				if ( $( '#fotoAudio' )[0].files.length ) {
					var files = $( '#fotoAudio' ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( 'imagen', value );
					} );
				}
			}
			if ( $( '#audioFile' ).length ) {
				if ( $( '#audioFile' )[0].files.length ) {
					var files = $( '#audioFile' ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( 'audio', value );
					} );
				}
			}
			var idVideo = ( $( '#audioId' ).val() != '' ) ? $( '#audioId' ).val() : 0;
			formData.append( 'audioId', idVideo );
			formData.append( 'clientId', clientId );
			formData.append( 'coachId', obtenSesion( 'coachId' ) );
			var formMultiple = [];
			var info = { 'destino': 'audiocategories', 'ids': $( '#categoryIdAudio' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'useraudio', 'ids': $( '#studentAprovedAudio' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'coachaudio', 'ids': $( '#coachAprovedAudio' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'audiocircle', 'ids': $( '#circlesAsignedAudio' ).val().join( '-' ) };
			formMultiple.push( info );
			formData.append( 'multiples', JSON.stringify( formMultiple ) );
			ajaxLlamada = $.ajax( {
				type: 'POST',
				url: 'admin/querys/guarda',
				data: formData,
				processData: false,
				contentType: false,
				async: true,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alertaTmp( '', 'Successfully stored', 'success' );
						desmarca( 'audioForm' );
						if ( $( '#audioId' ).val() == 0 ) {
							vacia( 'audioForm' );
						}
						popup( 'audioAlta', false );
						cargaAudios();
					} else {
						alerta( 'Error', 'Something happened try again', 'error' );
					}
				}
			} );
		} else {
			alerta( '', 'Please choose an audio file', 'error' );
		}
	} else {
		alerta( '', 'Please choose at least one category', 'error' );
	}
}
function guardaVideo() {
	cargando( true );
	var formData = new FormData();
	formData.append( 'vooKey', vooKey );
	formData.append( 'create', 1 );
	formData.append( 'customS3', 0 );
	var files = $( '#videoPop' ).prop( 'files' );
	var cuantosVideos = 0;
	$.each( files, function( key, value ) {
		formData.append( 'file', value );
		formData.append( 'name', $( '#titulo' ).val() );
		cuantosVideos++;
	} );
	if ( cuantosVideos > 0 ) {
		if ( $( '#categoryIdSerie' ).val().length > 0 ) {
			ajaxLlamada = $.ajax( {
				type: 'POST',
				url: 'https://api.vooplayer.com/api/createVideo',
				data: formData,
				processData: false,
				contentType: false,
				async: true,
				xhr: function() {
					var xhr = $.ajaxSettings.xhr();
					xhr.upload.onprogress = function(e) {
						porcentaje( Math.floor(e.loaded / e.total *100) );
					};
					return xhr;
				},
				success: function( response ) {
					var videoId = response.split( '/publish/' )[1];
					porcentaje( 100 );
					var formData = new FormData();
					var hayMultiple = false;
					formData.append( 'tabla', 'videos' );
					formData.append( 'folderActivo', 'videos' );
					$( '#videoForm input[type="text"], #videoForm input[type="email"], #videoForm input[type="password"], #videoForm input[type="hidden"], #videoForm textarea, #videoForm select' ).each( function() {
						if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
							var campoId = ( $( this ).attr( 'id' ) == 'accessSerie' ) ? 'accessId' : $( this ).attr( 'id' );
							formData.append( campoId, $( this ).val() );
						}
					} );
					if ( $( '#fotoVideo' ).length ) {
						if ( $( '#fotoVideo' )[0].files.length ) {
							var files = $( '#fotoVideo' ).prop( 'files' );
							$.each( files, function( key, value ) {
								formData.append( 'imagen', value );
							} );
						}
					}
					var idVideo = ( $( '#videoId' ).val() != '' ) ? $( '#videoId' ).val() : 0;
					formData.append( 'videoId', idVideo );
					formData.append( 'video', videoId );
					formData.append( 'clientId', clientId );
					formData.append( 'coachId', obtenSesion( 'coachId' ) );
					var formMultiple = [];
					var info = { 'destino': 'videocategories', 'ids': $( '#categoryIdSerie' ).val().join( '-' ) };
					formMultiple.push( info );
					info = { 'destino': 'uservideo', 'ids': $( '#studentAproved' ).val().join( '-' ) };
					formMultiple.push( info );
					info = { 'destino': 'coachvideo', 'ids': $( '#coachAproved' ).val().join( '-' ) };
					formMultiple.push( info );
					info = { 'destino': 'videocircle', 'ids': $( '#circlesAsigned' ).val().join( '-' ) };
					formMultiple.push( info );
					formData.append( 'multiples', JSON.stringify( formMultiple ) );
					ajaxLlamada = $.ajax( {
						type: 'POST',
						url: 'admin/querys/guarda',
						data: formData,
						processData: false,
						contentType: false,
						async: true,
						success: function( response ) {
							var respuesta = JSON.parse( response );
							cargando( false );
							if ( 'Success' === respuesta.status ) {
								alertaTmp( '', 'Successfully stored', 'success' );
								desmarca( 'videoForm' );
								if ( $( '#videoId' ).val() == 0 ) {
									vacia( 'videoForm' );
								}
								popup( 'videoAlta', false );
								cargaVideos();
							} else {
								alerta( 'Error', 'Something happened try again', 'error' );
							}
						}
					} );
				}
			} );
		} else {
			alerta( '', 'Please choose at least one category', 'error' );
		}
	} else {
		if ( $( '#categoryIdSerie' ).val().length > 0 ) {
			var formData = new FormData();
			var hayMultiple = false;
			formData.append( 'tabla', 'videos' );
			formData.append( 'folderActivo', 'videos' );
			$( '#videoForm input[type="text"], #videoForm input[type="email"], #videoForm input[type="password"], #videoForm input[type="hidden"], #videoForm textarea, #videoForm select' ).each( function() {
				if ( typeof $( this ).attr( 'id' ) != 'undefined' && $( this ).val() != '' ) {
					var campoId = ( $( this ).attr( 'id' ) == 'accessSerie' ) ? 'accessId' : $( this ).attr( 'id' );
					formData.append( campoId, $( this ).val() );
				}
			} );
			var idVideo = ( $( '#videoId' ).val() != '' ) ? $( '#videoId' ).val() : 0;
			formData.append( 'videoId', idVideo );
			formData.append( 'clientId', clientId );
			formData.append( 'coachId', obtenSesion( 'coachId' ) );
			formData.append( 'serieId', $( '#serieCoach' ).val() );
			var formMultiple = [];
			var info = { 'destino': 'videocategories', 'ids': $( '#categoryIdSerie' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'uservideo', 'ids': $( '#studentAproved' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'coachvideo', 'ids': $( '#coachAproved' ).val().join( '-' ) };
			formMultiple.push( info );
			info = { 'destino': 'videocircle', 'ids': $( '#circlesAsigned' ).val().join( '-' ) };
			formMultiple.push( info );
			formData.append( 'multiples', JSON.stringify( formMultiple ) );
			if ( $( '#fotoVideo' ).length ) {
				if ( $( '#fotoVideo' )[0].files.length ) {
					var files = $( '#fotoVideo' ).prop( 'files' );
					$.each( files, function( key, value ) {
						formData.append( 'imagen', value );
					} );
				}
			}
			ajaxLlamada = $.ajax( {
				type: 'POST',
				url: 'admin/querys/guarda',
				data: formData,
				processData: false,
				contentType: false,
				async: true,
				success: function( response ) {
					var respuesta = JSON.parse( response );
					cargando( false );
					if ( 'Success' === respuesta.status ) {
						alertaTmp( '', 'Successfully stored', 'success' );
						desmarca( 'videoForm' );
						if ( $( '#videoId' ).val() == 0 ) {
							vacia( 'videoForm' );
						}
						popup( 'videoAlta', false );
						cargaVideos();
					} else {
						alerta( 'Error', 'Something happened try again', 'error' );
					}
				}
			} );
		} else {
			alerta( '', 'Please choose at least one category', 'error' );
		}
	}
}
function traeRespuestas() {
	var parametros = { 'userId': obtenSesion( 'studentId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/respuestas-alumno', respuestasEstudiante );
}
function respuestasEstudiante( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		$.each( datos.respuestas, function( index, answer ) {
			$( '#pregunta-' + answer.id ).val( answer.respuesta );
		} );
	}
}
function llamadasCoach() {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId, 'timeZone': timeZone };
	ajaxRequest( 'POST', parametros, 'admin/querys/llamadas', respuestasLlamada );
}
function sendInvitation( callId ) {
	var parametros = { 'callId': callId, 'clientId': clientId, 'baseUrl': baseUrl };
	ajaxRequest( 'POST', parametros, 'admin/querys/invitaciones', respuestaInvitacion );
}
function respuestaInvitacion( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Invitations sent successfully', 'success' );
	} else if ( datos.status == 'Users' ) {
		alertaTmp( '', 'There are no users or coaches assigned to this call to be invited', 'warning' );
	} else {
		errorCallback();
	}
}
function traeLlamadas() {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'userId': obtenSesion( 'studentId' ), 'clientId': clientId, 'timeZone': timeZone };
	ajaxRequest( 'POST', parametros, 'admin/querys/llamadas', respuestasLlamada );
}
function respuestasLlamada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		if ( checaSesion( 'coachId' ) ) {
			$( '#contenidoCall' ).html( datos.html );
		} else {
			$( '#contentCall' ).html( datos.html );
		}
	} else {
		errorCallback();
	}
}
function respuestaAgenda( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Call request sent successfully', 'success' );
		if ( checaSesion( 'coachId' ) ) {
			statusCall( 1, datos.id );
			popup( 'coachCall', false );
			llamadasCoach();
			$( '#coachCallForm' ).trigger( 'reset' );
		} else {
			if ( $( '#callIdUser' ).val() != 0 ) {
				statusCall( 2, datos.id );
			}
			popup( 'studentCall', false );
			traeLlamadas();
		}
	} else {
		errorCallback();
	}
}
function statusCall( tipo, callId ) {
	borraSesion( 'inicioLlamada' );
	if ( tipo == 1 ) {
		seteaSesion( 'inicioLlamada', 1 );
	}
	if ( checaSesion( 'tokenZoom' ) ) {
		var parametros = { 'callId': callId, 'clientId': clientId, 'tipo': tipo, 'token': obtenSesion( 'tokenZoom' ) };
	} else {
		var parametros = { 'callId': callId, 'clientId': clientId, 'tipo': tipo };
	}
	ajaxRequest( 'POST', parametros, 'admin/querys/statuscall', seteoLlamada );
}
function seteoLlamada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( 'seteada correctamente' );
		if ( checaSesion( 'coachId' ) ) {
			llamadasCoach();
		}
	} else {
		if ( checaSesion( 'inicioLlamada' ) ) {
			alertaTmp( '', 'Call request cannot be sent. Please check your zoom login and password in your account settings', 'error' );
			borraSesion( 'inicioLlamada' );
		}
		console.log( 'error al setear' );
	}
}
function checaSesionVideo( videoId ) {
	if ( checaSesion( 'studentId' ) ) {
		muestraVideo( videoId );
	} else {
		alertaTmp( '', 'Please login with your account first', 'error' );
		if ( tipoCliente == 1 ) {
			setTimeout( function() { manda( '/login' ); }, 1500 );
		}
	}
}
function checaSesionVideoCoach( videoId, coachId ) {
	if ( checaSesion( 'studentId' ) ) {
		muestraVideo( videoId );
	} else {
		if ( checaSesion( 'coachId' ) ) {
			if ( obtenSesion( 'coachId' ) == coachId ) {
				muestraVideo( videoId );
			} else {
				alertaTmp( '', 'Please login with your account first', 'error' );
				if ( tipoCliente == 1 ) {
					setTimeout( function() { manda( '/login' ); }, 1500 );
				}
			}
		} else {
			alertaTmp( '', 'Please login with your account first', 'error' );
			if ( tipoCliente == 1 ) {
				setTimeout( function() { manda( '/login' ); }, 1500 );
			}
		}
	}
}
function validaNivel( videoId, coachId, accesoId, usuarios, coaches ) {
	if ( accesoId == 3 ) {
		if ( checaSesion( 'studentId' ) || checaSesion( 'coachId' ) ) {
			muestraVideo( videoId );
		} else {
			alertaTmp( '', 'You don\'t have access to this video\nPlease login with your account first.', 'error' );
		}
	} else {
		if ( usuarios == '' && coaches == '' ) {
			alertaTmp( '', 'You don\'t have access to this video\nPlease contact the content creator or support for access.', 'error' );
		} else {
			if ( checaSesion( 'studentId' ) ) {
				var permitidos = usuarios.split( '-' );
				if ( permitidos.includes( obtenSesion( 'studentId' ) ) ) {
					muestraVideo( videoId );
				} else {
					alertaTmp( '', 'You don\'t have access to this video\nPlease contact the content creator or support for access.', 'error' );
				}
			} else {
				if ( checaSesion( 'coachId' ) ) {
					if ( obtenSesion( 'coachId' ) == coachId ) {
						muestraVideo( videoId );
					} else {
						var permitidos = coaches.split( '-' );
						if ( permitidos.includes( obtenSesion( 'coachId' ) ) ) {
							muestraVideo( videoId );
						} else {
							alertaTmp( '', 'You don\'t have access to this video\nPlease contact the content creator or support for access.', 'error' );
						}
					}
				} else {
					alertaTmp( '', 'You don\'t have access to this video\nPlease login with your account first', 'error' );
				}
			}
		}
	}
}
function muestraVideo( videoId ) {
	$( '#videoPopup iframe' ).attr( 'src', 'https://amrak.cdn.spotlightr.com/publish/' + videoId + '?fallback=true' );
	$( '#videoPopup iframe' ).attr( 'data-playerId', videoId );
	popup( 'videoPopup', true );
}
function formatoTiempo( tiempo ) {
	var tiempoNuevo;
	if ( tiempo.indexOf( 'am' ) != -1 ) {
		tiempoNuevo = tiempo.replace( 'am', '' );
		var piezas = tiempoNuevo.split( ':' );
		var hora = parseInt( piezas[0] );
		hora = ( hora == 12 ) ? '00' : hora;
		tiempoNuevo = hora + ':' + piezas[1] + ':00'
	} else {
		tiempoNuevo = tiempo.replace( 'pm', '' );
		var piezas = tiempoNuevo.split( ':' );
		var hora = parseInt( piezas[0] );
		hora = hora + 12;
		hora = ( hora == 24 ) ? 12 : hora;
		tiempoNuevo = hora + ':' + piezas[1] + ':00'
	}
	console.log( tiempoNuevo );
	return tiempoNuevo;
}
function checaPendientes() {
	if ( ( checaSesion( 'pending' ) && obtenSesion( 'pending' ) == 'N' ) || !checaSesion( 'pending' ) ) {
		$( '.pendientes' ).show();
		$( '.rechazo' ).hide();
		$( '.pendienteStatus' ).hide();
	} else {
		$( '.pendientes' ).hide();
		if ( checaSesion( 'coachId' ) ) {
			$( 'div[abre="coachForm' ).click();
			tabContentAbierto = 'coachForm';
		} else {
			$( 'div[abre="studentForm' ).click();
			tabContentAbierto = 'studentForm';
		}
		if ( obtenSesion( 'pending' ) == 'R' ) {
			$( '.pendienteStatus' ).hide();
			if ( checaSesion( 'razones' ) && obtenSesion( 'razones' ) != '' ) {
				if ( checaSesion( 'coachId' ) ) {
					$( '#rechazoData p' ).html( 'Reason for rejection: ' + obtenSesion( 'razones' ) );
				} else {
					$( '#rechazoDataMiembro p' ).html( 'Reason for rejection: ' + obtenSesion( 'razones' ) );
				}
				$( '.rechazo' ).show();
			} else {
				$( '.rechazo' ).hide();
			}
		} else {
			$( '.pendienteStatus' ).show();
			$( '.rechazo' ).hide();
		}
	}
	if ( !$( '#' + tabContentAbierto ).is( ':visible' ) ) {
		$( '#' + tabContentAbierto ).slideToggle();
	}
}
function solicitaAprobacion() {
	var parametros = { 'coachId': obtenSesion( 'coachId' ), 'studentId': obtenSesion( 'studentId' ), 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/envia-solicitud', respuestaSol );
}
function respuestaSol( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Cambios' ) {
		alertaTmp( '', 'You must to made changes before re-send you application', 'error' );
	} else if ( datos.status == 'Success' ) {
		seteaSesion( 'pending', 'S' );
		borraSesion( 'razones' );
		checaPendientes();
		alertaTmp( '', 'Approval request sent successfully', 'success' );
	}
}
function actualizaPosiciones() {
	var posicionamiento = [];
	$( '#contenidoVideo tr' ).each( function() {
		var posicion = $( this ).index() + 1;
		var elementoId = $( this ).attr( 'campoId' );
		posicionamiento.push( { 'id': elementoId, 'posicion': posicion } );
	} );
	var parametros = { 'posicionamiento': posicionamiento, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/actualiza-orden', respuestaPosicion );
}
function actualizaPosicionesArticle() {
	var posicionamiento = [];
	$( '#contenidoArticle tr' ).each( function() {
		var posicion = $( this ).index() + 1;
		var elementoId = $( this ).attr( 'campoId' );
		posicionamiento.push( { 'id': elementoId, 'posicion': posicion } );
	} );
	var parametros = { 'posicionamiento': posicionamiento, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/actualiza-orden-article', respuestaPosicion );
}
function respuestaPosicion( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( 'posiciones actualizadas' );
	} else {
		console.log( 'error al actualizar posiciones' );
	}
}
function traeLlamada( callId ) {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'tabla': 'calls', 'folder': 'calls', 'id': callId, 'clientId': clientId, 'timeZone': timeZone };
	if ( checaSesion( 'studentId' ) ) { parametros[ 'studentId' ] = obtenSesion( 'studentId' ); }
	if ( checaSesion( 'coachId' ) ) { parametros[ 'coachId' ] = obtenSesion( 'coachId' ); }
	if ( $( '#circleIdActive' ).length ) { parametros[ 'circleId' ] = $( '#circleIdActive' ).val(); }
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', popupLlamada );
}
function popupLlamada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		var go = true;
		if ( datos.tipoId == 2 ) {
			if ( checaSesion( 'studentId' ) ) {
				var found = 0;
				var usuariosPermitidos = datos[ 'multiple-0' ].split( '-' );
				for ( var i = 0; i < usuariosPermitidos.length; i++ ) {
					if ( usuariosPermitidos[ i ] == obtenSesion( 'studentId' ) ) { found++; }
				}
				go = ( found == 0 ) ? false : true;
			} else {
				if ( checaSesion( 'coachId' ) && datos.coachId == obtenSesion( 'coachId' ) ) {
					go = true;
				} else {
					if ( checaSesion( 'coachId' ) ) {
						var coachesPermitidos = datos[ 'multiple-1' ].split( '-' );
						for ( var i = 0; i < coachesPermitidos.length; i++ ) {
							if ( coachesPermitidos[ i ] == obtenSesion( 'coachId' ) ) { found++; }
						}
						go = ( found == 0 ) ? false : true;
					} else {
						go = false;
					}
				}
			}
		} else if ( datos.tipoId == 4 ) {
			if ( checaSesion( 'coachId' ) && datos.coachId == obtenSesion( 'coachId' ) ) {
				go = true;
			} else {
				go = eval( datos.circuloAcceso );
			}
		} else if ( datos.tipoId == 3 ) {
			if ( checaSesion( 'studentId' ) || checaSesion( 'coachId' ) ) {
				go = true;
			} else {
				go = false;
			}
		}
		if ( go ) {
			var texto = ( datos.suscrito ) ? 'Unregister' : 'Register';
			var html = '<p class="tiempoMeet">' + datos.fechaNormal + '</p>';
			html += '<p class="timeZoneMeet">' + datos.timezone + '</p>';
			html += '<h1>' + datos.title + '</h1>';
			html += '<h2>with ' + datos.coach + '</h2>';
			html += '<p>' + datos.description + '</p>';
			html += datos.link;
			if ( datos.tipoId != 4 ) {
				html += '<br><a href="#" class="noHref boton secundario" id="registroLlamada" onclick="registraLlamada( ' + datos.callId + ' )">' + texto + '</a>';
			}
			$( '#llamadaPopup .infoCall' ).html( html );
			if ( datos.image != '' && datos.image != null && datos.image != 'null' ) {
				$( '#llamadaPopup .fondo' ).css( 'background-image', 'url( \'images/calls/' + datos.image + '\' )' );
			} else {
				$( '#llamadaPopup .fondo' ).css( 'background-image', 'url( \'images/coach-call.jpg\' )' );
			}
			$( '#llamadaPopup .fondo' ).css( 'filter', 'blur(50px)' );
			if ( datos.image != '' && datos.image != null && datos.image != 'null' ) {
				$( '#llamadaPopup .imagenCall img' ).attr( 'src', 'images/calls/' + datos.image );
			} else {
				$( '#llamadaPopup .imagenCall img' ).attr( 'src', 'images/coach-call.jpg' );
			}
			//$( '#llamadaPopup .imagenCall span' ).html( datos.fechaFormateada );
			$('#llamadaPopup .imagenCall span').countdown( datos.fecha, function( event ) {
	  			$( this ).html( 'Starts in ' + event.strftime('%D days %H hours %M minutes %S seconds'));
			} );
			popup( 'llamadaPopup', true );
			$( '.noHref' ).click( function( event ) { event.preventDefault(); } );
		} else {
			if ( datos.tipoId == 3 ) {
				alertaTmp( '', 'You don\'t have access to this call, you must login/register first', 'error' );
			} else {
				alertaTmp( '', 'You don\'t have access to this private call', 'error' );
			}
		}
	} else {
		console.log( 'error al setear' );
	}
}
function registraLlamada( callId ) {
	if ( checaSesion( 'studentId' ) || checaSesion( 'coachId' ) ) {
		var parametros = { 'callId': callId, 'clientId': clientId };
		if ( checaSesion( 'studentId' ) ) { parametros[ 'studentId' ] = obtenSesion( 'studentId' ); }
		if ( checaSesion( 'coachId' ) ) { parametros[ 'coachId' ] = obtenSesion( 'coachId' ); }
		ajaxRequest( 'POST', parametros, 'admin/querys/registra-llamada', registroLlamada );
	} else {
		alertaTmp( '', 'Please log in first', 'error' );
	}
}
function registroLlamada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Updated successfuly', 'success' );
		$( '#registroLlamada' ).html( datos.respuesta );
		traeSchedule();
	}
}
function llamadaAntes() {
	alertaTmp( '', 'Session will be available to join 5 minutes before start', 'error' );
}
function accesoSerie( serieId, coachId, accesoId, serieUrl ) {
	if ( !checaSesion( 'coachId' ) ) {
		if ( accesoId == 1 || ( accesoId == 3 && checaSesion( 'studentId' ) ) ) {
			manda( 'serie/' + serieUrl );
		} else {
			if ( !checaSesion( 'studentId' ) && accesoId != 2 ) {
				alertaTmp( '', 'You don\'t have access to this serie,<br>Please login with your account first.', 'error' );
			} else {
				if ( accesoId == 2 && checaSesion( 'studentId' ) ) {
					seteaSesion( 'serieSelected', serieId );
					var parametros = { 'serieId': serieId, 'serieUrl': serieUrl, 'studentId': obtenSesion( 'studentId' ), 'clientId': clientId };
					ajaxRequest( 'POST', parametros, 'admin/querys/checa-acceso', respuestaAcceso );
				} else {
					alertaTmp( '', 'You don\'t have access to this series<br>Please contact the content creator or support for access.', 'error' );
				}
			}
		}
	} else {
		if ( obtenSesion( 'coachId' ) == coachId || accesoId == 1 ) {
			manda( 'serie/' + serieUrl );
		} else {
			alertaTmp( '', 'You don\'t have access to this serie', 'error' );
		}
	}
}
function respuestaAcceso( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		borraSesion( 'serieSelected' );
		manda( 'serie/' + datos.url );
	} else {
		if ( datos.solicita == 'S' ) {
			alertaTmp( '', 'You don\'t have access to this private serie, we already receive your access request, we are working on that', 'error' );
		} else {
			confirmacion( '', 'You don\'t have access to this private serie, Do you want to request it to the coach?', confirmaAcceso );
		}
	}
}
function confirmaAcceso( status ) {
	if ( status ) {
		var parametros = { 'tabla': 'serierequest', 'folderActivo': 'serierequest', 'requestSerieId': 0, 'userId': obtenSesion( 'studentId' ), 'serieId': obtenSesion( 'serieSelected' ), 'clientId': clientId };
		ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaRequest );
	} else {
		borraSesion( 'serieSelected' );
	}
}
function respuestaRequest( respuesta ) {
	cargando( false );
	borraSesion( 'serieSelected' );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Access requested successfully, when you access be granted we let you know', 'success' );
	} else {
		alertaTmp( '', 'There was a problem requesting access, please try again later', 'error' );
	}
}
function userAccesSerie( status, requestId, userId, nombre, email ) {
	var parametros = { 'serieId': $( '#serieCoach' ).val(), 'requestId': requestId, 'userId': userId, 'status': status, 'clientId': clientId };
	ajaxRequest( 'POST', parametros, 'admin/querys/status-serie', respuestaStatusRequest );
}
function respuestaStatusRequest( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Access asigned successfully', 'success' );
		cargaVideos();
	} else {
		alertaTmp( '', 'There was a problem changing tha access status, please try again later', 'error' );
	}
}
function editaLlamada( llamadaId ) {
	$( '.horaElige' ).removeClass( 'marcada' );
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'tabla': 'calls', 'folder': 'calls', 'id': llamadaId, 'clientId': clientId, 'timeZone': timeZone };
	ajaxRequest( 'POST', parametros, 'admin/querys/listado', respuestaEditaLlamada );
}
function respuestaEditaLlamada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		console.log( datos );
		var fechaPartes = datos.fecha.split( ' ' );
		if ( checaSesion( 'coachId' ) ) {
			$( '#callId' ).val( datos.callId );
			$( '#titleCall' ).val( datos.title );
			$( '#descriptionCall' ).val( datos.description );
			$( '#durationCall' ).val( datos.duration );
			$( '#descriptionCall' ).val( datos.description );
			$( '#tipoId' ).val( datos.tipoId );
			$( '#fecha' ).val( fechaPartes[0] );
			$( '#time' ).val( fechaPartes[1] );
			$( '#datepicker' ).datepicker().datepicker( 'setDate', new Date( fechaPartes[0] ) );
			$( '.horaElige[tiempo="' + fechaPartes[1] + '"]' ).addClass( 'marcada' );
			var allMembers = ( datos.allMembers == 'S' ) ? true : false;
			var allCoaches = ( datos.allCoaches == 'S' ) ? true : false;
			$( '#userCallAll' ).prop( 'checked', allMembers );
			$( '#coachCallAprovedAll' ).prop( 'checked', allCoaches );
			if ( datos.image != null && datos.image != '' ) {
				$( '#imgShowCall' ).html( '<img src="images/calls/' + datos.image + '" class="thumbCoach">' );
			}
			$.each( datos, function( key, value ) {
				console.log( key );
				if ( key.indexOf( 'multiple' ) != -1 ) {
					var multiples = value.split( '-' );
					if ( key == 'multiple-1' ) {
						$( '#coachCallAproved' ).val( multiples ).trigger( 'change' );
					} else if ( key == 'multiple-2' ) {
						$( '#circlesAsignedCall' ).val( multiples ).trigger( 'change' );
					} else if ( key == 'multiple-0' ) {
						$( '#userCall' ).val( multiples ).trigger( 'change' );
					} else {
						$( '#categoryCall' ).val( multiples ).trigger( 'change' );
					}
				}
			} );
			validaMiembros();
			checaLlamada( datos.tipoId );
			obtenCuentaData();
			popup( 'coachCall', true );
		} else {
			$( '#callIdUser' ).val( datos.callId );
			$( '#titleUser' ).val( datos.title );
			$( '#descriptionCallUser' ).val( datos.description );
			$( '#coachCallPick' ).val( datos.coachId );
			$( '#durationCall' ).val( datos.duration );
			$( '#descriptionCall' ).val( datos.description );
			$( '#fechaCall' ).val( fechaPartes[0] );
			$( '#timeCall' ).val( fechaPartes[1] );
			$( '#datepickerCall' ).datepicker().datepicker( 'setDate', new Date( fechaPartes[0] ) );
			$( '.horaElige[tiempo="' + fechaPartes[1] + '"]' ).addClass( 'marcada' );
			popup( 'studentCall', true );
		}
	} else {
		errorCallback();
	}
}
function validaMiembros() {
	if ( $( '#coachCallAprovedAll' ).is( ':checked' ) ) {
		$( '#divCoachCallAproved' ).hide();
	} else {
		$( '#divCoachCallAproved' ).show();
	}
	if ( $( '#userCallAll' ).is( ':checked' ) ) {
		$( '#divUserCallSelect' ).hide();
	} else {
		$( '#divUserCallSelect' ).show();
	}
}
function borraLlamada( llamadaId ) {
	seteaSesion( 'llamadaBorra', llamadaId );
	confirmacion( '', 'Are you sure to delete this call?', borraLlamadaContinua )
}
function borraLlamadaContinua( respuesta ) {
	if ( respuesta ) {
		var parametros = { 'tabla': 'calls', 'folder': 'calls', 'id': obtenSesion( 'llamadaBorra' ) };
		ajaxRequest( 'POST', parametros, 'admin/querys/eliminado', respuestaLlamadaBorrada );
	}
}
function iniciaLlamada() {
	obtenCuentaData();
	checaLlamada( 1 );
	$( '#inviteCall' ).prop( 'checked', false );
	$( '#userCallAll' ).prop( 'checked', false );
	$( '#coachCallAprovedAll' ).prop( 'checked', false );
	validaMiembros();
	popup( 'coachCall', true );
}
function respuestaLlamadaBorrada( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		llamadasCoach();
		alertaTmp( '', 'Deleted', 'success' );
	} else {
		alerta( '', 'Somehting goes wrong try again', 'error' );
	}
}
function requestConsultation() {
	if ( checaSesion( 'studentId' ) ) {
		borraSesion( 'llamadaPendiente' );
		popup( 'studentCall', true )
	} else {
		seteaSesion( 'llamadaPendiente', $( '#coachURL' ).val() );
		manda( 'login' );
	}
}
function borraCuenta() {
	confirmacion( '', 'Are you sure you want to delete this account?', borraCuentaConfirma )
}
function borraCuentaConfirma( respuesta ) {
	if ( respuesta ) {
		var tabla = ( checaSesion( 'studentId' ) ) ? 'users' : 'coaches';
		var id = ( checaSesion( 'studentId' ) ) ? obtenSesion( 'studentId' ) : obtenSesion( 'coachId' );
		var parametros = { 'tabla': tabla, 'id': id };
		ajaxRequest( 'POST', parametros, 'admin/querys/borra-cuenta', respuestaBorrado );
	}
}
function respuestaBorrado( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'This account has been deleted', 'success' );
		setTimeout( function() { manda( '/' ); }, 3000 );
	} else {
		alerta( '', 'Somehting goes wrong try again', 'error' );
	}
}
function traeSchedule() {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'clientId': clientId, 'timeZone': timeZone };
	if ( checaSesion( 'studentId' ) ) { parametros[ 'studentId' ] = obtenSesion( 'studentId' ); }
	if ( checaSesion( 'coachId' ) ) { parametros[ 'coachId' ] = obtenSesion( 'coachId' ); }
	console.log( parametros );
	ajaxRequest( 'POST', parametros, 'admin/querys/schedule', respuestasSchedule );
}
function respuestasSchedule( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		if ( checaSesion( 'coachId' ) ) {
			$( '#contenidoSchedule' ).html( datos.html );
		} else {
			$( '#contentSchedule' ).html( datos.html );
		}
	} else {
		errorCallback();
	}
}
function recuerdaPass() {
	ingresaDato( 'Enter your email address to recover your password access', mandaPass );
}
function mandaPass( email ) {
	if ( email != '' && email != null ) {
		if ( validaEmail( email ) ) {
			var parametros = { 'clientId': clientId, 'email': email };
			ajaxRequest( 'POST', parametros, 'admin/querys/recupera-cliente', enviaPass );
		}
	}
}
function enviaPass( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alerta( '', 'Password sent correctly to email', 'success' );
	} else if ( datos.status == 'Encontrado' ) {
		alerta( 'Error', 'We cannot find an account with the email entered', 'error' );
	} else if ( datos.status == 'Pass' ) {
		alerta( 'Error', 'There was a problem assined the new pass, try again', 'error' );
	} else {
		alerta( 'Error', 'Something happened try again', 'error' );
	}
}
function quitaCircle( tipo, id, circleId ) {
	var tabla = '';
	switch( tipo ) {
		case 1 : tabla = 'usercircle'; break;
		case 2 : tabla = 'coachcircle'; break;
		case 3 : tabla = 'circlerequest'; break;
		case 4 : tabla = 'circlecoachrequest'; break;
	}
	var campoId = '';
	switch( tipo ) {
		case 1 : campoId = 'userId'; break;
		case 2 : campoId = 'coachId'; break;
		case 3 : campoId = 'userId'; break;
		case 4 : campoId = 'coachId'; break;
	}
	var parametros = { 'tabla': tabla, 'campoId': campoId, 'id': id, 'circleId': circleId };
	ajaxRequest( 'POST', parametros, 'admin/querys/elimina-circle', eliminadoCircle );
}
function eliminadoCircle( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		cargaCircle();
	}
}
function requestCircle() {
	if ( !checaSesion( 'studentId' ) && !checaSesion( 'coachId' ) ) {
		alertaTmp( '', 'Please login with your account first', 'error' );
	} else {
		var circleId = $( '#circleIdActive' ).val();
		var parametros = { 'requestCircleId': 0, 'clientId': clientId, 'circleId': circleId };
		if ( checaSesion( 'studentId' ) ) {
			parametros[ 'userId' ] = obtenSesion( 'studentId' );
			parametros[ 'tabla' ] = 'circlerequest';
			parametros[ 'folder' ] = 'circlerequest';
			parametros[ 'folderActivo' ] = 'circlerequest';
		}
		if ( checaSesion( 'coachId' ) ) {
			parametros[ 'coachId' ] = obtenSesion( 'coachId' );
			parametros[ 'tabla' ] = 'circlecoachrequest';
			parametros[ 'folder' ] = 'circlecoachrequest';
			parametros[ 'folderActivo' ] = 'circlecoachrequest';
		}
		ajaxRequest( 'POST', parametros, 'admin/querys/guarda', respuestaRequestCircle );
	}
}
function respuestaRequestCircle( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		alertaTmp( '', 'Join request sent successfully', 'success' );
		$( '.circuloLink' ).hide();
	} else {
		errorCallback();
	}
}
function apruebaCircle( tipo, id, circleId ) {
	var tabla = ( tipo == 1 ) ? 'circlerequest' : 'circlecoachrequest';
	var tablaCirculo = ( tipo == 1 ) ? 'usercircle' : 'coachcircle';
	var tablaId = ( tipo == 1 ) ? 'userId' : 'coachId';
	var parametros = { 'tabla': tabla, 'id': id, 'circleId': circleId, 'tablaCirculo': tablaCirculo, 'tablaId': tablaId };
	ajaxRequest( 'POST', parametros, 'admin/querys/aprobar-circulo', respuestaCirculo );
}
function respuestaCirculo( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		cargaCircle();
	} else {
		errorCallback();
	}
}
function sessionList( coachId ) {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'clientId': clientId, 'platformId': clientPlatformId, 'timeZone': timeZone, 'coachId': coachId };
	ajaxRequest( 'POST', parametros, 'admin/querys/lista-llamadas', respuestaSession );
}
function sessionListCircle( circleId ) {
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var parametros = { 'clientId': clientId, 'platformId': clientPlatformId, 'timeZone': timeZone, 'circleId': circleId };
	ajaxRequest( 'POST', parametros, 'admin/querys/lista-llamadas-circle', respuestaSession );
}
function respuestaSession( respuesta ) {
	cargando( false );
	datos = JSON.parse( respuesta );
	if ( datos.status == 'Success' ) {
		$( '#sesionListAjax' ).html( datos.html );
		var swiper = new Swiper( '.swiper-container', {
			slidesPerView: 5,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				320: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				550: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
				780: {
					slidesPerView: 4,
					spaceBetween: 40,
				},
				1080: {
					slidesPerView: 5,
	      			spaceBetween: 30
				}
			}
		} );
	} else {
		errorCallback();
	}
}
function iniciaZoom() {
    cargando( true );
	escuchaZoom();
	var urlReturn = encodeURI( baseUrl + '/zoom-activate?coachId=' + obtenSesion( 'coachId' ) );
	window.open( 'https://zoom.us/oauth/authorize?response_type=code&client_id=go3Ws0HIQa6AuY1P4wIZDg&redirect_uri=' + urlReturn, '_blank' );
}
function cierraZoom() {
	confirmacion( '', 'Are you sure you want to remove this zoom account? Any live sessions scheduled with this zoom account will also be deleted', cierraZoomConfirma )
}
function cierraZoomConfirma( respuesta ) {
	if ( respuesta ) {
		$.ajax( {
			type: 'POST',
			url: 'admin/querys/zoom-close',
			data: { 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId },
			async: true,
			success: function( respuesta ) {
				var datos = JSON.parse( respuesta );
				if ( datos.status == 'Success' ) {
					$( '.zoomActivar' ).show();
					$( '#zoomSecret' ).val( '' );
					borraSesion( 'tokenZoom' );
					borraSesion( 'tokenAuth' );
					$( '.zoomLogout' ).hide();
					alertaTmp( '', 'Zoom account closed successfuly', 'success' );
				} else {
					alertaTmp( '', 'There was a problem closing zoom session on shala platform, try again later', 'error' );
				}
			},
		} );
	}
}
function escuchaZoom() {
	$.ajax( {
		type: 'POST',
		url: 'admin/querys/zoom-token',
		data: { 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId },
		async: true,
		success: function( respuesta ) {
			var datos = JSON.parse( respuesta );
			if ( datos.token == '' || datos.token == 'null' || datos.token == null ) {
				setTimeout( function() {
					escuchaZoom();
				} , 3000 );
			} else {
			    cargando( false );
				alertaTmp( '', 'Zoom account linked successfuly', 'success' );
				$( '.zoomActivar' ).hide();
				$( '#zoomSecret' ).val( datos.token );
				seteaSesion( 'tokenZoom', datos.token );
				seteaSesion( 'tokenAuth', datos.auth );
				seteaSesion( 'tokenRefresh', datos.refresh );
				$( '.zoomLogout' ).show();
			}
		},
	} );
}
function refrescaToken() {
	if ( checaSesion( 'tokenZoom' ) ) {
		$.ajax( {
			type: 'POST',
			url: 'admin/querys/zoom-refresh',
			data: { 'token': obtenSesion( 'tokenRefresh' ), 'coachId': obtenSesion( 'coachId' ), 'clientId': clientId },
			async: true,
			success: function( respuesta ) {
				var datos = JSON.parse( respuesta );
				if ( datos.status == 'Success' ) {
					console.log( 'refrescado' );
					seteaSesion( 'tokenZoom', datos.token );
					seteaSesion( 'tokenRefresh', datos.refresh );
					if ( funcionRefresca != '' ) {
						eval( funcionRefresca );
						funcionRefresca = '';
					}
				} else {
					console.log( 'ERROR refrescando' );
				}
			},
		} );
	}
}
function obtenCuentaData() {
	if ( checaSesion( 'tokenZoom' ) ) {
		$.ajax( {
			type: 'POST',
			url: 'admin/querys/zoom-cuenta',
			data: { 'token': obtenSesion( 'tokenZoom' ) },
			async: true,
			success: function( respuesta ) {
				var datos = JSON.parse( respuesta );
				if ( datos.status == 'Success' ) {
					console.log( datos.datos );
					$( '.sessionInfo' ).html( 'Your session will be scheduled using the Zoom account <b>' + datos.datos.email + '</b>' );
				} else {
					console.log( 'ERROR obteniendo' );
					console.log( datos.response );
					if ( datos.datos.code == 124 ) {
						funcionRefresca = 'obtenCuentaData()';
						refrescaToken();
					}
				}
			},
		} );
	} else {
		$( '.sessionInfo' ).html( '' );
		$( '.sessionInfo' ).html( 'Your session will be scheduled using the default Zoom account <b>roman@innerworks.io</b>' );
	}
}