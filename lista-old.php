<?php
	include 'cabecera.php';
	$R = obtenSerieData( $_REQUEST[ 'url' ], $con )
?>
<div class="pageHead" style="background-image: url( 'images/series/<?php echo $R[ 'photo' ]; ?>' );">
	<h1><?php echo $R[ 'nombre' ]; ?></h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="centroContent">
			<div class="tCenter"><?php echo $R[ 'description' ]; ?></div>
		</div>
		<br><hr><br>
		<h2 class="serieTitulo">Videos</h2>
		<div class="fila">
		<?php
			$usersAproved = array();
			$queryUser = $con->Consulta( 'select * from userserie where serieId=' . $R[ 'serieId' ] );
			while( $U = $con->Resultados( $queryUser ) ) {
				$usersAproved[] = $U[ 'userId' ];
			}
			$queryVideo = $con->Consulta( 'select * from videos where serieId=' . $R[ 'serieId' ] . ' order by titulo asc' );
			while( $V = $con->Resultados( $queryVideo ) ) {
				$imagen = ( !is_null( $V[ 'imagen' ] ) && $V[ 'imagen' ] != '' ) ? 'images/videos/' . $V[ 'imagen' ] : 'images/video-general.jpg';
				$onclick = 'muestraVideo( \'' . $V[ 'video' ] . '\' )';
				if ( $R[ 'accessId' ] != 1 ) {
					$onclick = 'validaNivel( \'' . $V[ 'video' ] . '\', ' . $V[ 'coachId' ] . ', ' . $R[ 'accessId' ] . ', \'' . implode( '-', $usersAproved ) . '\' )';
				}
				echo
				'<div class="treintaTres cPad categoriaHome videoCoach">
					<a onclick="' . $onclick . '"><img src="thumb?src=' . $imagen . '&size=400x240" class="fullImg" style="border: 1px solid #000"></a>
					<h4>' . $V[ 'titulo' ] . '</h4>
					<p>' . $V[ 'description' ] . '</p>
				</div>';
			}
		?>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>