<?php
	include 'cabecera.php';
	$R = obtenSerieData( $_REQUEST[ 'url' ], $con );
	$categoria = '';
	$queryCat = $con->Consulta( 'select c.nombre from seriescategories s inner join categories c on(s.categoryId=c.categoryId) where s.serieId=' . $R[ 'serieId' ] );
	while( $CAT = $con->Resultados( $queryCat ) ) { $categoria = '<h4>' .$CAT[ 'nombre' ] . '</h4>'; }
?>
<div class="seccion">
	<div class="principal">
		<div class="filaFlex">
			<div class="doble">
				<?php
					if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) {
						echo
						'<iframe allowtransparency="true" class="videoFrame" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' . $R[ 'video' ] . '?fallback=true" frameborder="0" scrolling="no"> </iframe>';
					} else {
						$imagenSerie = ( !is_null( $R[ 'photo' ] ) && $R[ 'photo' ] != '' ) ? 'images/series/' . $R[ 'photo' ] : 'images/video-general.jpg';
						echo '<img src="' . $imagenSerie . '" class="fullImg">';
					}
				?>
			</div>
			<div class="doble datosSerie">
				<h1><?php echo $R[ 'nombre' ]; ?></h1>
				<?php echo $categoria; ?>
				<div class="lineaGris"></div>
				<p><?php echo $R[ 'description' ]; ?></p>
			</div>
		</div>
		<br><hr><br>
		<h2 class="serieTitulo">Videos</h2>
		<div class="filaFlex">
		<?php
			$usersAproved = array();
			$queryUser = $con->Consulta( 'select * from userserie where serieId=' . $R[ 'serieId' ] );
			while( $U = $con->Resultados( $queryUser ) ) {
				$usersAproved[] = $U[ 'userId' ];
			}
			$coachesAproved = array();
			$queryUser = $con->Consulta( 'select * from coachserie where serieId=' . $R[ 'serieId' ] );
			while( $U = $con->Resultados( $queryUser ) ) {
				$coachesAproved[] = $U[ 'coachId' ];
			}
			$usersCirculo = array();
			$coachesCirculo = array();
			$queryUser = $con->Consulta( 'select * from seriecircle where serieId=' . $R[ 'serieId' ] );
			while( $V = $con->Resultados( $queryUser ) ) {
				$queryUser = $con->Consulta( 'select * from usercircle where circleId=' . $V[ 'circleId' ] );
				while( $U = $con->Resultados( $queryUser ) ) {
					$usersCirculo[] = $U[ 'userId' ];
				}
				$queryUser = $con->Consulta( 'select * from coachcircle where circleId=' . $V[ 'circleId' ] );
				while( $U = $con->Resultados( $queryUser ) ) {
					$coachesCirculo[] = $U[ 'coachId' ];
				}
			}
			$queryVideo = $con->Consulta( 'select * from videos where serieId=' . $R[ 'serieId' ] . ' order by orden asc,titulo asc' );
			while( $V = $con->Resultados( $queryVideo ) ) {
				$imagen = ( !is_null( $V[ 'imagen' ] ) && $V[ 'imagen' ] != '' ) ? 'images/videos/' . $V[ 'imagen' ] : 'images/video-general.jpg';
				$onclick = 'muestraVideo( \'' . $V[ 'video' ] . '\' )';
				if ( $R[ 'accessId' ] != 1 ) {
					if ( $R[ 'accessId' ] == 4 ) {
						$usersString = implode( '-', $usersCirculo );
						$coachesString = implode( '-', $coachesCirculo );
					} else {
						$usersString = implode( '-', $usersAproved );
						$coachesString = implode( '-', $coachesAproved );
					}
					$onclick = 'validaNivel( \'' . $V[ 'video' ] . '\', ' . $V[ 'coachId' ] . ', ' . $R[ 'accessId' ] . ', \'' . $usersString . '\', \'' . $coachesString . '\' )';
				}
				echo
				'<div class="triple categoriaHome videoCoach">
					<a onclick="' . $onclick . '"><img src="thumb?src=' . $imagen . '&size=400x240" class="fullImg" style="border: 1px solid #000"></a>
					<h4>' . $V[ 'titulo' ] . '</h4>
					<p>' . $V[ 'description' ] . '</p>
				</div>';
			}
		?>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>