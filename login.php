<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1>Login</h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<form method="post" id="loginForm" class="formShow" enctype="multipart/form-data">
					<label for="emailUser">Email</label>
					<input type="email" id="emailUser" campo="email" class="email obligatorio" required>
					<label for="passUser">Password</label>
					<input type="password" id="passUser" campo="pass" class="obligatorio" required>
					<br>
					<a href="#" class="noHref" onclick="recuerdaPass()">Recover password</a>
					<br><br>
					<button type="submit" class="boton">Login</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>