<?php
	include 'cabecera.php';
	$R = obtenCoachData( $_REQUEST[ 'url' ], $con )
?>
<div class="pageHead">
	<h1><?php echo $R[ 'apellido' ] . ' ' . $R[ 'nombre' ]; ?></h1>
</div>
<div class="seccion">
	<div class="principal">
		<div class="imgCentro"><img src="thumb?src=images/coaches/<?php echo $R[ 'foto' ]; ?>&size=240x290" class="fullImg"></div>
		<div class="centroContent">
			<div><?php echo $R[ 'biografia' ]; ?></div>
			<br>
			<div>
			<?php if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) : ?>
				<div class="tCenter"><a class="tCenter" onclick="muestraVideo( '<?php echo $R[ 'video' ]; ?>' )">View Video Profile</a></div>
			<?php endif; ?>
			</div>
			<br>
		</div>
		<?php
			$query = $con->Consulta( 'select * from series where coachId=' . $R[ 'coachId' ] . ' and accessId=1 order by nombre asc' );
			if ( $con->Cuantos( $query ) > 0 ) {
				echo '<hr><h3 class="tCenter">AVAILABLE PUBLIC CLASSES</h3><hr>';
				while( $S = $con->Resultados( $query ) ) {
					echo '<h4>' . $S[ 'nombre' ] . '</h4><div class="fila">';
					$queryVideo = $con->Consulta( 'select * from videos where coachId=' . $R[ 'coachId' ] . ' and serieId=' . $S[ 'serieId' ] . ' order by titulo asc' );
					while( $V = $con->Resultados( $queryVideo ) ) {
						$imagen = ( !is_null( $V[ 'imagen' ] ) && $V[ 'imagen' ] != '' ) ? 'images/videos/' . $V[ 'imagen' ] : 'images/video-general.jpg';
						echo
						'<div class="veinteCinco cPad categoriaHome videoCoach">
							<a onclick="muestraVideo( \'' . $V[ 'video' ] . '\' )"><img src="thumb?src=' . $imagen . '&size=400x240" class="fullImg" style="border: 1px solid #000"></a>
							<h4>' . $V[ 'titulo' ] . '</h4>
							<p>' . $V[ 'description' ] . '</p>
						</div>';
					}
					echo '</div>';
				}
			}
		?>
		<div class="row sesionData" coach="<?php echo $R[ 'coachId' ]; ?>">
			<?php
				$res = $con->Consulta( 'select *, IF(fecha>=now() AND fecha<=DATE_ADD(NOW(), INTERVAL 1 HOUR),"S","N") as disponible from calls where fecha>=DATE_SUB(NOW(), INTERVAL 1 HOUR) and tipoId=1 and clientId=' . $clientId . ' and coachId=' . $R[ 'coachId' ] );
				if ( $con->Cuantos( $res ) > 0 ) {
					echo '<hr><h3 class="tCenter">Upcoming Sessions</h3><hr><div class="fila">';
					while( $S = $con->Resultados( $res ) ) {
						$enlace = ( is_null( $S[ 'url' ] ) ) ? 'Pending Approval' : ( ( $S[ 'disponible' ] == 'S' ) ? '<a href="' . $S[ 'url' ] . '" target="_blank">Join now</a>' : 'Waiting for Sessions\'s time' );
						$imagen = ( !is_null( $S[ 'image' ] ) && $S[ 'image' ] != '' ) ? 'images/calls/' . $S[ 'image' ] : 'images/coach-call.jpg';
					echo
					'<div class="veinteCinco cPad categoriaHome meetings">
						<img src="thumb?src=' . $imagen . '&size=330x220" class="fullImg" style="border: 1px solid #000">
						<h4>' . $S[ 'title' ] . '</h4>
						<p>' . $S[ 'fecha' ] . '</p>
						<p class="link">' . $enlace . '</p>
					</div>';
					}
					echo '</div>';
				}
			?>
			<hr><h3 class="tCenter">AVAILABLE RECORDED CLASSES</h3><hr>
			<?php
				$query = $con->Consulta( 'select * from series where coachId=' . $R[ 'coachId' ] . ' and accessId<>1 order by nombre asc' );
				while( $S = $con->Resultados( $query ) ) {
					$usersAproved = array();
					$queryUser = $con->Consulta( 'select * from userserie where serieId=' . $S[ 'serieId' ] );
					while( $U = $con->Resultados( $queryUser ) ) {
						$usersAproved[] = $U[ 'userId' ];
					}
					echo '<h4>' . $S[ 'nombre' ] . '</h4><div class="fila">';
					$queryVideo = $con->Consulta( 'select * from videos where coachId=' . $R[ 'coachId' ] . ' and serieId=' . $S[ 'serieId' ] . ' order by titulo asc' );
					while( $V = $con->Resultados( $queryVideo ) ) {
						$imagen = ( !is_null( $V[ 'imagen' ] ) && $V[ 'imagen' ] != '' ) ? 'images/videos/' . $V[ 'imagen' ] : 'images/video-general.jpg';
						echo
						'<div class="veinteCinco cPad categoriaHome videoCoach">
							<a onclick="validaNivel( \'' . $V[ 'video' ] . '\', ' . $V[ 'coachId' ] . ', ' . $S[ 'accessId' ] . ', \'' . implode( '-', $usersAproved ) . '\' )"><img src="thumb?src=' . $imagen . '&size=400x240" class="fullImg" style="border: 1px solid #000"></a>
							<h4>' . $V[ 'titulo' ] . '</h4>
							<p>' . $V[ 'description' ] . '</p>
						</div>';
					}
					echo '</div>';
				}
			?>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>