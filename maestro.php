<?php
	include 'cabecera.php';
	$R = obtenCoachData( $_REQUEST[ 'url' ], $clientId, $con );
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$( document ).ready( function() {
		if ( checaSesion( 'studentId' ) ) {
			$( '#datepickerCall' ).datepicker( {
				dateFormat: 'yy-mm-dd',
				minDate: 0,
				onSelect: function( dateText ) {
					$( '#fechaCall' ).val( dateText );
				}
			} );
			$( '.horaElige' ).click( function( e ) {
				$( '#timeCall' ).val( $( this ).attr( 'tiempo' ) );
				$( '.horaElige' ).removeClass( 'marcada' );
				$( this ).addClass( 'marcada' );
			} );
			if ( checaSesion( 'llamadaPendiente' ) ) {
				requestConsultation();
			}
		}
	} );
</script>
<input type="hidden" id="coachURL" value="<?php echo $_REQUEST[ 'url' ]; ?>">
<div class="popup" id="studentCall">
	<a class="cierra" onclick="popup( 'studentCall', false )"><i class="fas fa-times"></i></a>
	<form method="post" id="studentCallForm">
		<div class="fila">
			<div class="treintaTres">
				<h3>Request a Live Session</h3>
				<label for="titleUser">Session Description</label>
				<input type="text" id="titleUser" class="obligatorio" required>
				<label for="descriptionCallUser">Session Description</label>
				<textarea id="descriptionCallUser" class="obligatorio" required></textarea>
				<input type="hidden" id="coachCallPick" value="<?php echo $R[ 'coachId' ]; ?>">
			</div>
			<div class="treintaTres">
				<label for="fecha">Date</label>
				<div id="datepickerCall"></div>
				<input type="hidden" id="fechaCall">
			</div>
			<div class="treintaTres cPad">
				<label for="horarioScroll">Start Time</label>
				<div class="horarioScroll">
					<div class="horaElige" tiempo="06:00:00">6:00 am</div>
					<div class="horaElige" tiempo="06:30:00">6:30 am</div>
					<div class="horaElige" tiempo="07:00:00">7:00 am</div>
					<div class="horaElige" tiempo="07:30:00">7:30 am</div>
					<div class="horaElige" tiempo="08:00:00">8:00 am</div>
					<div class="horaElige" tiempo="08:30:00">8:30 am</div>
					<div class="horaElige" tiempo="09:00:00">9:00 am</div>
					<div class="horaElige" tiempo="09:30:00">9:30 am</div>
					<div class="horaElige" tiempo="10:00:00">10:00 am</div>
					<div class="horaElige" tiempo="10:30:00">10:30 am</div>
					<div class="horaElige" tiempo="11:00:00">11:00 am</div>
					<div class="horaElige" tiempo="11:30:00">11:30 am</div>
					<div class="horaElige" tiempo="12:00:00">12:00 pm</div>
					<div class="horaElige" tiempo="12:30:00">12:30 pm</div>
					<div class="horaElige" tiempo="13:00:00">1:00 pm</div>
					<div class="horaElige" tiempo="13:30:00">1:30 pm</div>
					<div class="horaElige" tiempo="14:00:00">2:00 pm</div>
					<div class="horaElige" tiempo="14:30:00">2:30 pm</div>
					<div class="horaElige" tiempo="15:00:00">3:00 pm</div>
					<div class="horaElige" tiempo="15:30:00">3:30 pm</div>
					<div class="horaElige" tiempo="16:00:00">4:00 pm</div>
					<div class="horaElige" tiempo="16:30:00">4:30 pm</div>
					<div class="horaElige" tiempo="17:00:00">5:00 pm</div>
					<div class="horaElige" tiempo="17:30:00">5:30 pm</div>
					<div class="horaElige" tiempo="18:00:00">6:00 pm</div>
					<div class="horaElige" tiempo="18:30:00">6:30 pm</div>
					<div class="horaElige" tiempo="19:00:00">7:00 pm</div>
					<div class="horaElige" tiempo="19:30:00">7:30 pm</div>
					<div class="horaElige" tiempo="20:00:00">8:00 pm</div>
					<div class="horaElige" tiempo="20:30:00">8:30 pm</div>
					<div class="horaElige" tiempo="21:00:00">9:00 pm</div>
					<div class="horaElige" tiempo="21:30:00">9:30 pm</div>
					<div class="horaElige" tiempo="22:00:00">10:00 pm</div>
					<div class="horaElige" tiempo="22:30:00">10:30 pm</div>
					<div class="horaElige" tiempo="23:00:00">11:00 pm</div>
					<div class="horaElige" tiempo="23:30:00">11:30 pm</div>
				</div>
				<input type="hidden" id="timeCall">
			</div>
		</div>
		<br><br>
		<div>
			<button type="submit" class="boton inline">Submit</button>
		</div>
	</form>
</div>
<div class="coachHead" style="background-image: url( 'images/coaches/<?php echo $R[ 'header' ]; ?>' );">
	<div class="contenidoHead">
		<h1><?php echo $R[ 'nombre' ] . ' ' . $R[ 'apellido' ]; ?></h1>
		<?php if ( !is_null( $R[ 'phrase' ] ) && $R[ 'phrase' ] != '' ) : ?>
		<p class="fraseText"><?php echo $R[ 'phrase' ]; ?></p>
		<?php endif; ?>
		<div class="socialNetwork">
		<?php if ( !is_null( $R[ 'facebook' ] ) && $R[ 'facebook' ] != '' ) : ?>
			<a href="<?php echo $R[ 'facebook' ]; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
		<?php endif; ?>
		<?php if ( !is_null( $R[ 'twitter' ] ) && $R[ 'twitter' ] != '' ) : ?>
			<a href="<?php echo $R[ 'twitter' ]; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
		<?php endif; ?>
		<?php if ( !is_null( $R[ 'linkedin' ] ) && $R[ 'linkedin' ] != '' ) : ?>
			<a href="<?php echo $R[ 'linkedin' ]; ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
		<?php endif; ?>
		<?php if ( !is_null( $R[ 'instagram' ] ) && $R[ 'instagram' ] != '' ) : ?>
			<a href="<?php echo $R[ 'instagram' ]; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
		<?php endif; ?>
		<a href="mailto:<?php echo $R[ 'email' ]; ?>" target="_blank"><i class="fas fa-envelope"></i></a>
		<?php if ( !is_null( $R[ 'web' ] ) && $R[ 'web' ] != '' ) : ?>
			<a href="<?php echo $R[ 'web' ]; ?>" target="_blank"><i class="fas fa-globe"></i></a>
		<?php endif; ?>
		</div>
	</div>
</div>
<div class="seccion borderBottom">
	<div class="principal">
		<div class="infoCoach">
			<div class="imagenProfile">
				<div class="imgCentro">
					<?php if ( !is_null( $R[ 'foto' ] ) && $R[ 'foto' ] != '' ): ?>
					<img src="thumb?src=images/coaches/<?php echo $R[ 'foto' ]; ?>&size=200x200" class="fullImg">
					<?php else: ?>
					<img src="thumb?src=images/silueta.png&size=200x200" class="fullImg">
					<?php endif; ?>
				</div>
			</div>
			<div class="textoCoach">
				<div><?php echo $R[ 'biografia' ]; ?></div>
				<br>
				<div>
				<?php if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) : ?>
				<a class="tCenter coachLink" onclick="muestraVideo( '<?php echo $R[ 'video' ]; ?>' )">View Video Profile</a>
				<?php endif; ?>
				<a class="tCenter coachLink" onclick="requestConsultation()">Request Consultation</a>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>
<div class="seccion margen padShort">
	<div class="principal">
		<?php getCircles( $R[ 'coachId' ], $R[ 'nombre' ], $clientId, $clientPlatformId, $con ); ?>
	</div>
</div>
<div class="seccion margen sessionList padShort sinMargen">
	<div class="principal">
		<div id="sesionListAjax"></div>
	</div>
</div>
<div class="seccion">
	<div class="principal">
		<?php
			$query = $con->Consulta( 'select * from videos where coachId=' . $R[ 'coachId' ] . ' and clientId=' . $clientId . ' and accessId<>4 order by orden asc, videoId desc' );
			if ( $con->Cuantos( $query ) > 0 ) {
				echo '<h2 class="serieTitulo">' . $R[ 'nombre' ] . '\'s Videos</h2><div class="fila">';
				while( $S = $con->Resultados( $query ) ) {
					$imagenSerie = ( !is_null( $S[ 'imagen' ] ) && $S[ 'imagen' ] != '' ) ? 'images/videos/' . $S[ 'imagen' ] : 'images/video-general.jpg';
					$categoria = '';
					$queryCat = $con->Consulta( 'select c.nombre from videocategories s inner join categories c on(s.categoryId=c.categoryId) where s.videoId=' . $S[ 'videoId' ] );
					while( $CAT = $con->Resultados( $queryCat ) ) { $categoria = '<span>' .$CAT[ 'nombre' ] . '</span>'; }
					$descriptionText = ( strlen( $S[ 'description' ] ) > 100 ) ? substr( $S[ 'description' ], 0, 100 ) . '...' : $S[ 'description' ];
					$usersAproved = array();
					$queryUser = $con->Consulta( 'select * from uservideo where videoId=' . $S[ 'videoId' ] );
					while( $U = $con->Resultados( $queryUser ) ) {
						$usersAproved[] = $U[ 'userId' ];
					}
					$coachesAproved = array();
					$queryUser = $con->Consulta( 'select * from coachvideo where videoId=' . $S[ 'videoId' ] );
					while( $U = $con->Resultados( $queryUser ) ) {
						$coachesAproved[] = $U[ 'coachId' ];
					}
					$usersCirculo = array();
					$coachesCirculo = array();
					$queryUser = $con->Consulta( 'select * from videocircle where videoId=' . $S[ 'videoId' ] );
					while( $V = $con->Resultados( $queryUser ) ) {
						$queryUser = $con->Consulta( 'select * from usercircle where circleId=' . $V[ 'circleId' ] );
						while( $U = $con->Resultados( $queryUser ) ) {
							$usersCirculo[] = $U[ 'userId' ];
						}
						$queryUser = $con->Consulta( 'select * from coachcircle where circleId=' . $V[ 'circleId' ] );
						while( $U = $con->Resultados( $queryUser ) ) {
							$coachesCirculo[] = $U[ 'coachId' ];
						}
					}
					$onclick = 'muestraVideo( \'' . $S[ 'video' ] . '\' )';
					if ( $S[ 'accessId' ] != 1 ) {
						if ( $S[ 'accessId' ] == 4 ) {
							$usersString = implode( '-', $usersCirculo );
							$coachesString = implode( '-', $coachesCirculo );
						} else {
							$usersString = implode( '-', $usersAproved );
							$coachesString = implode( '-', $coachesAproved );
						}
						$onclick = 'validaNivel( \'' . $S[ 'video' ] . '\', ' . $S[ 'coachId' ] . ', ' . $S[ 'accessId' ] . ', \'' . $usersString . '\', \'' . $coachesString . '\' )';
					}
					echo
					'<div class="cincuenta cPadBig categoriaHome videoCoach">
						' . $categoria . '
						<a href="#" onclick="' . $onclick . '" class="noHref">
							<img src="thumb?src=' . $imagenSerie . '&size=400x240" class="fullImg" style="border: 1px solid #000">
						</a>
						<div class="fila">
							<div class="sesenta"><h3 class="tituloSerie"><a href="#" onclick="' . $onclick . '" class="noHref">' . $S[ 'titulo' ] . '</a></h3></div>
						</div>
						<p>' . $descriptionText . '</p>
					</div>';
				}
				echo '</div>';
			}
		?>
	</div>
</div>
<?php include 'pie.php'; ?>