$( function() {
	var ul = $( '#upload ul' );
	var valido;
	var imagen;
	$( '#drop a' ).click( function() {
		$( this ).parent().find( 'input' ).click();
	} );
	$( '#upload' ).fileupload( {
		dropZone: $( '#drop' ),
		add: function ( e, data ) {
			if ( data.files[0].size < 2000000 ) {
				imagen = data.files[0].name;
				imagen = imagen.toLowerCase();
				imagen = imagen.replace( ' ','-' );
				imagen = imagen.replace( '#', '' );
				imagen = imagen.replace( '@', '' );
				imagen = imagen.replace( '*', '' );
				imagen = imagen.replace( ',', '' );
				imagen = imagen.replace( '&', '' );
				imagen = imagen.replace( '=', '' );
				imagen = imagen.replace( '(', '' );
				imagen = imagen.replace( ')', '' );
				imagen = imagen.replace( 'ñ', 'n' );
				imagen = imagen.replace( '�', '' );
				imagen = imagen.replace( ' ', '-' );
				var ext = imagen.split( '.' );
				var pt = ext.length - 1;
				if ( 'jpg' === ext[pt] || 'gif' === ext[pt] || 'png' === ext[pt] ) {
					validaExiste( imagen );
					if ( valido ) {
						localStorage.setItem( 'imagen', imagen );
						var jqXHR = data.submit();
					} else {
						if( confirm( 'El nombre de dicha imagen ya existe, desea reemplazarla?' ) ) {
							localStorage.setItem( 'imagen', imagen );
							var jqXHR = data.submit();
						}
					}
				} else {
					top.alerta( 'Formato Invalido, debe ser una Imagen', 'danger' );
				}
			} else {
				top.alerta( 'Imagen supera el tamaño limite', 'danger' );
			}
		},
		progress: function( e, data ) {
			var progress = parseInt( data.loaded / data.total * 100, 10 );
			porcentaje( progress );
		},
		fail:function( e, data ){
			data.context.addClass( 'error' );
		},
		done: function( e, data ) {
			var respuesta = JSON.parse( data.result );
			if ( typeof respuesta.imagen != 'undefined' ) {
				localStorage.setItem( 'imagen', respuesta.imagen );
			} else {
				localStorage.setItem( 'imagen', imagen );
			}
			if ( $( '.showImg' ).length ) {
				$( '.showImg' ).html( '<img src="../images/' + localStorage.getItem( 'folderActivo' ) + '/' + localStorage.getItem( 'imagen' ) + '" class="fullimg">' );
			}
		}
	} );
	$( document ).on( 'drop dragover', function ( e ) {
		e.preventDefault();
	} );
	function formatFileSize( bytes ) {
		if ( typeof bytes !== 'number' ) {
			return '';
		}
		if ( bytes >= 1000000000 ) {
			return ( bytes / 1000000000 ).toFixed( 2 ) + ' GB';
		}
		if ( bytes >= 1000000 ) {
			return ( bytes / 1000000 ).toFixed( 2 ) + ' MB';
		}
		return ( bytes / 1000 ).toFixed( 2 ) + ' KB';
	}
	function validaExiste( archivo ) {
		$.ajax( {
			type: 'GET',
			async: false,
			url: 'querys/comprueba.php',
			data: 'archivo=' + archivo + '&folder=' + localStorage.getItem( 'folderActivo' ),
			success:function( response ) {
				if ( 'Existe' === response ) {
					valido = false;
				} else {
					valido = true;
				}
			}
		} );
	}
} );