<?php
	function encripta( $q ) {
		$qEncoded = encriptaDesencripta( 'encrypt', $q );
		return( $qEncoded );
	}
	function desencripta( $q ) {
		$qDecoded = encriptaDesencripta( 'decrypt', $q );
		return( $qDecoded );
	}
	function encriptaDesencripta( $action, $string ) {
		$output = false;
		$encrypt_method = 'AES-256-CBC';
		$secret_key = 'qJB0rGtIn5UB1xG03efyCp';
		$secret_iv = 'qJB0rGtIn5UB1xG03efyCpqJB0rGtIn5UB1xG03efyCp';
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
		if ( $action == 'encrypt' ) {
			$output = openssl_encrypt( $string, $encrypt_method, $key, 0, $iv );
			$output = base64_encode( $output );
		} else if ( $action == 'decrypt' ) {
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}
		return $output;
	}
	function mesTexto( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'Enero';
			case '02': return 'Febrero';
			case '03': return 'Marzo';
			case '04': return 'Abril';
			case '05': return 'Mayo';
			case '06': return 'Junio';
			case '07': return 'Julio';
			case '08': return 'Agosto';
			case '09': return 'Septiembre';
			case '10': return 'Octubre';
			case '11': return 'Noviembre';
			case '12': return 'Diciembre';
			default: return '--';
		}
	}
	function monthText( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'January';
			case '02': return 'February';
			case '03': return 'March';
			case '04': return 'April';
			case '05': return 'May';
			case '06': return 'June';
			case '07': return 'July';
			case '08': return 'August';
			case '09': return 'September';
			case '10': return 'Octuber';
			case '11': return 'November';
			case '12': return 'December';
			default: return '--';
		}
	}
	function moisText( $numMes ) {
		switch ( $numMes ) {
			case '01': return 'janvier';
			case '02': return 'février';
			case '03': return 'mars';
			case '04': return 'avril';
			case '05': return 'mai';
			case '06': return 'juin';
			case '07': return 'juillet';
			case '08': return 'août';
			case '09': return 'septembre';
			case '10': return 'octobre';
			case '11': return 'novembre';
			case '12': return 'décembre';
			default: return '--';
		}
	}
	function formatoFecha( $fecha, $lang ) {
		$mifecha = explode( '-', $fecha );
		if ( $lang == 'ES' ) {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = mesTexto( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		} else if ( $lang == 'FR' ) {
			if ( moisText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = moisText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		} else {
			if ( monthText( $mifecha[ 1 ] ) == '--' ) {
				return '--';
			} else {
				$lafecha = monthText( $mifecha[ 1 ] ) . ' ' . $mifecha[ 2 ] . ', ' . $mifecha[ 0 ];
				return $lafecha;
			}
		}
	}
	function obtenHora( $fecha ) {
		ereg( '([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})', $fecha, $hora );
		$lahora = $hora[ 2 ] . ':' . $hora[ 3 ];
		return $lahora;
	}
	function formatoFechaHora( $arg ) {
		$fecha = $arg;
		ereg( '([0-9]{2,2})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})', $fecha, $mifecha );
		$fechahora = $mifecha[ 3 ] . '.' . mesTexto( $mifecha[ 2 ] ) . '.' . $mifecha[ 1 ] . ' ' . $mifecha[ 4 ] . ':' . $mifecha[ 5 ];
		if ( mesTexto( $mifecha[ 2 ] ) == 'Err' ) {
			return '--';
		} else {
			return $fechahora;
		}
	}
	function limpialo( $cadena, $tipo ) {
		$cadenaLimpia = preg_replace( '[\s+]','-', trim( stripslashes( $cadena ) ) );
		$cadenaLimpia = str_replace( ' ', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '#', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '@', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '*', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( ',', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '&', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '=', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '(', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( ')', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '.', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '’', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( "''", '', $cadenaLimpia );
		$cadenaLimpia = str_replace( "'", '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '”', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( 'ñ', 'n', $cadenaLimpia );
		$cadenaLimpia = str_replace( 'Ñ', 'N', $cadenaLimpia );
		$cadenaLimpia = str_replace( '"', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '/', '', $cadenaLimpia );
		$cadenaLimpia = str_replace( '�','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '¿','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '?','', $cadenaLimpia );
		$cadenaLimpia = str_replace( '+','-', $cadenaLimpia );
		$cadenaLimpia = str_replace( ' ', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '°', '', $cadenaLimpia );
		$cadenaLimpia = str_replace(
			array( 'á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú' ),
			array( 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' ),
			$cadenaLimpia
		);
		$cadenaLimpia = str_replace( '--', '-', $cadenaLimpia );
		$cadenaLimpia = str_replace( '--', '-', $cadenaLimpia );
		$cadenaLimpia = ( $tipo == 'min' ) ? strtolower( $cadenaLimpia ) : ( ( $tipo == 'may' ) ? strtoupper( $cadenaLimpia ) : $cadenaLimpia );
		$cadenaLimpia = mb_substr( $cadenaLimpia, 0, 490, 'UTF-8' );
		return $cadenaLimpia;
	}
	$palabrasReservadas = array( 'archivo', 'html', 'activar', 'nombre', 'fecha', 'pass', 'editable', 'rfc', 'email', 'idioma', 'icono', 'liga', 'color', 'hoy', 'gps', 'posicion', 'mapa', 'dibujo', 'spotlightr' );
	function armaOpciones( $tabla, $con ) {
		$campoId = '';
		$campoNombre = '';
		$html = '';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'S' ) {
				$campoId = $R[ 'columna' ];
			}
			if ( $R[ 'comentarios' ] == 'nombre' ) {
				$campoNombre = $R[ 'columna' ];
			}
		}
		if ( $campoId != '' && $campoNombre != '' ) {
			$str = 'select ' . $campoId . ', ' . $campoNombre . ' from ' . $tabla . ' order by ' . $campoId . ' asc';
			$res = $con->Consulta( $str );
			while( $R = $con->Resultados( $res ) ) {
				$html .= '<option value="' . $R[ $campoId ] . '">' . palabraDiccionario( $R[ $campoNombre ], $con ) . '</option>';
			}
		}
		return $html;
	}
	function getCampoId( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_KEY = "PRI"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function getCampoNombre( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT="nombre"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function getCampoPosicion( $tabla, $con ) {
		$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT = "posicion"';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			return $R[ 'columna' ];
		}
		return false;
	}
	function armaTabla( $tabla, $con ) {
		global $palabrasReservadas;
		$html = '<th class="tcenter">Action</th><th class="tcenter">ID</th>';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by ORDINAL_POSITION asc';
		$res = $con->Consulta( $str );
		$campoPosicion = getCampoPosicion( $tabla, $con );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' && ( $campoPosicion == false || $campoPosicion != $R[ 'columna' ] ) ) {
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					$titulo = palabraDiccionario( $R[ 'columna' ], $con );
					$html .= '<th>' . ucfirst( $titulo ). '</th>';
				} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
					$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
					$titulo = palabraDiccionario( $tablaCatalogo, $con );
					$html .= '<th>' . ucfirst( $titulo ). '</th>';
				}
			}
		}
		if ( $campoPosicion != false ) {
			$html .= '<th>Order</th>';
		}
		echo $html;
	}
	function armaFormulario( $tabla, $con ) {
		global $palabrasReservadas;
		$camposHtml = '';
		$comboMultiple = '';
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and ( COLUMN_COMMENT<>"hoy" and COLUMN_COMMENT<>"oculto" )';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' ) {
				$claseObligatorio = ( $R[ 'obligatoria' ] == 'S' ) ? 'obligatorio' : '';
				$asterisko = ( $R[ 'obligatoria' ] == 'S' ) ? ' *' : '';
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					if ( $R[ 'comentarios' ] != 'idioma' ) {
						$titulo = palabraDiccionario( $R[ 'columna' ], $con );
						if ( $R[ 'comentarios' ] == 'activar' ) {
							$titulo = ( $R[ 'columna' ] == 'estado' ) ? 'Autorizado' : $titulo;
							$tipoActiva = ( $R[ 'tipoDatos' ] == 'char' ) ? 'SN' : 'TF';
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="inputStandard" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field tcenter" style="padding-top: 8px;">
											<label class="switch block">
												<input type="checkbox" id="' . $R[ 'columna' ] . '" checked="" tipo="' . $tipoActiva . '">
												<label for="' . $R[ 'columna' ] . '" data-on="SI" data-off="NO"></label>
											</label>
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'archivo' ) {
							$camposHtml .=
							'<div class="col-lg-12">
								<div class="form-group mb10 row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-2 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-8">
										<label class="field prepend-icon append-button file" for="imagen"><span class="button btn-primary">' . ucfirst( $titulo ) . '</span>
											<form contiene="' . $R[ 'columna' ] . '">
												<input type="file" id="' . $R[ 'columna' ] . '" onchange="$( \'#uploader-' . $R[ 'columna' ] . '\' ).val( this.value );" class="gui-file ' . $claseObligatorio . ' archivo">
												<input id="uploader-' . $R[ 'columna' ] . '" placeholder="Choose a file" class="gui-input noPelar" type="text" style="text-align: right;">
												<label class="field-icon"><i class="fa fa-upload"></i></label>
											</form>
										</label>
									</div>
									<div class="col-lg-2 capaimg showImg" columna="' . $R[ 'columna' ] . '" obligado="' . $claseObligatorio . '"></div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'html' ) {
							$camposHtml .=
							'<div class="col-lg-12">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<div style="height: 400px;" class="summernote" id="' . $R[ 'columna' ] . '"></div>
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'fecha' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' fecha">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'color' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field sfcolor">
											<input id="' . $R[ 'columna' ] . '" type="text" placeholder="Choose a color" class="gui-input color ' . $claseObligatorio . '" onchange="seteaToHex( $( this ) )">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'icono' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . '" readonly onclick="muestraIcono( \'' . $R[ 'columna' ] . '\' )">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'rfc' || $R[ 'comentarios' ] == 'email' ) {
							$camposHtml .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
									<div class="col-lg-12">
										<label class="field">
											<input type="text" id="' . $R[ 'columna' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' ' . $R[ 'comentarios' ] . '" maxlength="' . $R[ 'tamano' ]. '">
										</label>
									</div>
								</div>
							</div>';
						} else if ( $R[ 'comentarios' ] == 'mapa' ) {
							$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<textarea id="' . $R[ 'columna' ] . '" placeholder="Ingrese ' . ucfirst( $titulo ) . '" rows="2" class="form-control ' . $claseObligatorio . ' buscaMapa" latitud="" longitud="" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '></textarea>
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="section row">
										<div style="height: 200px;" class="googleMaps" id="mapa-' . $R[ 'columna' ] . '" campoMapa="' . $R[ 'columna' ] . '"></div>
									</div>
								</div>';
						} else if ( $R[ 'comentarios' ] == 'pass' ) {
							$camposHtml .=
								'<div class="col-lg-6">
									<div class="section row nuevoPass">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Enter a ' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . ' pass" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '>
											</label>
										</div>
									</div>
									<div class="section row oldPass">
										<label for="liga-' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<a onclick="" id="liga-' . $R[ 'columna' ] . '">Re-send Password</a>
										</div>
									</a>
								</div>';
						} else if ( $R[ 'comentarios' ] == 'dibujo' ) {
							$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<a href="#" class="noHref borraDibujo" onclick="borraDibujo( \'' . $R[ 'columna' ] . '\' )">Delete ' . ucfirst( $titulo ) . '</a>
										<canvas id="' . $R[ 'columna' ] . '" class="dibujaCanvas ' . $claseObligatorio . '" creado="0" width="512" height="200"></canvas>
									</div>
								</div>';
						} else if ( $R[ 'comentarios' ] == 'spotlightr' ) {
							$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-6 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-6">
											<label class="field">
												<br><button type="button" class="button btn-primary mr10 pull-right text-uppercase text-white ls100 fw700" onclick="obtenVideos()">Elegir ' . ucfirst( $titulo ) . '</button>
												<div class="videoElegido ' . $claseObligatorio . '" id="' . $R[ 'columna' ] . '" videoId="">
											</label>
										</div>
									</div>
								</div>';
						} else {
							$funcionOn = ( $R[ 'tipoDatos' ] == 'int' || $R[ 'tipoDatos' ] == 'decimal' ) ? 'onkeypress="return soloNumeros( event );"' : '';
							if ( $R[ 'tamano' ] > 200 ) {
								$camposHtml .=
								'<div class="col-lg-12">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<textarea id="' . $R[ 'columna' ] . '" placeholder="Ingrese ' . ucfirst( $titulo ) . '" rows="4" class="form-control ' . $claseObligatorio . '" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '></textarea>
											</label>
										</div>
									</div>
								</div>';
							} else {
								$camposHtml .=
								'<div class="col-lg-6">
									<div class="section row">
										<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
										<div class="col-lg-12">
											<label class="field">
												<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Ingrese ' . ucfirst( $titulo ) . '" class="gui-input ' . $claseObligatorio . '" maxlength="' . $R[ 'tamano' ]. '" ' . $funcionOn . '>
											</label>
										</div>
									</div>
								</div>';
							}
						}
					}
				} else {
					if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
						$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
						$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tablaCatalogo );
						$titulo = ( $tabla == 'sucursal' && $tablaCatalogo == 'negocio' ) ? 'Negocio Slide Principal' : $titulo;
						$titulo = ( $tabla == 'miembros' && $tablaCatalogo == 'tipoviajero' ) ? 'Tipo de Viajero' : $titulo;
						$titulo = palabraDiccionario( $titulo, $con );
						$camposHtml .=
						'<div class="col-lg-6">
							<div class="section row">
								<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . $asterisko . '</label>
								<div class="col-lg-12">
									<label class="field select">
										<select id="' . $R[ 'columna' ] . '" class="' . $claseObligatorio . ' opcion">
											<option value="-1">Pick an option</option>
											' . armaOpciones( $tablaCatalogo, $con ) . '
										</select>
										<i class="arrow"></i>
									</label>
								</div>
							</div>
						</div>';
					}
				}
			} else {
				if ( preg_match('/\beditable\b/', $R[ 'comentarios' ] ) ) {
					$titulo = palabraDiccionario( $R[ 'columna' ], $con );
					$camposHtml .=
					'<div class="col-lg-12">
						<div class="section row">
							<label for="' . $R[ 'columna' ] . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . ' *</label>
							<div class="col-lg-12">
								<label class="field">
									<input type="text" id="' . $R[ 'columna' ] . '" placeholder="Ingrese ' . ucfirst( $titulo ) . '" class="gui-input oblitagorio" maxlength="' . $R[ 'tamano' ]. '">
									<span style="font-size: 10px;">Verifique esta información al crearla ya que no podra ser editada</span>
								</label>
							</div>
						</div>
					</div>';
				} else {
					$camposHtml .= '<input type="hidden" id="' . $R[ 'columna' ] . '" class="id" value="0">';
				}
				if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
					$comentarioLimpio = str_replace( 'editable;', '', $R[ 'comentarios' ] );
					$variosMultiples = explode( ';', $comentarioLimpio );
					for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
						if ( $variosMultiples[ $i ] != '' ) {
							$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
							$tablas = explode( ',', $infoTablas );
							$tablaCatalogo = $tablas[0];
							$tablaDestino = $tablas[1];
							$titulo = palabraDiccionario( $tablaCatalogo, $con );
							$titulo = ( $tabla == 'sucursal' ) ? 'Negocios Listado Inferior' : $titulo;
							$comboMultiple .=
							'<div class="col-lg-6">
								<div class="section row">
									<label for="multiple-' . $i . '" class="col-lg-12 control-label">' . ucfirst( $titulo ) . '</label>
									<div class="col-lg-12">
										<select multiple="multiple" destino="' . $tablaDestino . '" class="select2-multiple form-control select-primary obligatorio" id="multiple-' . $i . '">
											' . armaOpciones( $tablaCatalogo, $con ) . '
										</select>
									</div>
								</div>
							</div>';
						}
					}
				}
			}
		}
		echo $camposHtml . $comboMultiple;
	}
	function armaAcordeones( $tabla, $con ) {
		global $palabrasReservadas;
		$grupoAnterior = '';
		$i = 0;
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
		$res = $con->Consulta( $str );
		while( $R = $con->Resultados( $res ) ) {
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
				if ( !in_array( $R[ 'comentarios' ], $palabrasReservadas ) && $R[ 'comentarios' ] != '' && !preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) && !preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) && !preg_match('/\bimagenes\b/', $R[ 'comentarios' ] ) ) {
					if ( $grupoAnterior != $R[ 'comentarios' ] ) {
						$grupoAnterior = $R[ 'comentarios' ];
						$i = 0;
					}
					$miembros[ $grupoAnterior ][ $i ] = array( 'campo' => $R[ 'columna' ], 'tipo' => $R[ 'tipoDatos' ], 'tamano' => $R[ 'tamano' ] );
					$i++;
				}
			}
		}
		if ( isset( $miembros ) ) {
			foreach ( $miembros as $grupo => $elementos ) {
				$miembroId = limpialo( $grupo, 'min' );
				$html =
				'<div id="' . $miembroId . '" class="panel-group accordion">
					<div class="panel">
						<div class="panel-heading"><a data-toggle="collapse" data-parent="#' . $miembroId . '" href="#' . $miembroId . '-cont" class="accordion-toggle accordion-icon link-unstyled collapsed" aria-expanded="false">' . $grupo . '</a></div>
						<div id="' . $miembroId . '-cont" class="panel-collapse collapse" aria-expanded="false">
							<div class="panel-body">
								<div role="form" class="admin-form" id="' . $miembroId . 'Info">';
								for ( $i = 0; $i < sizeof( $elementos ); $i++ ) {
									$funcionOn = ( $elementos[ $i ][ 'tipo' ] == 'int' || $elementos[ $i ][ 'tipo' ] == 'decimal' ) ? 'onkeypress="return soloNumeros( event );"' : '';
									$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $elementos[ $i ][ 'campo' ] );
									$isFecha = ( $elementos[ $i ][ 'tipo' ] == 'date' || $elementos[ $i ][ 'tipo' ] == 'datetime' || $elementos[ $i ][ 'tipo' ] == 'timestamp' ) ? 'fecha' : '';
									$html .=
									'<div class="form-group row">
										<label for="' . $elementos[ $i ][ 'campo' ] . '" class="col-lg-3 control-label">' . ucfirst( $titulo ) . '</label>
										<div class="col-lg-9">
											<label class="field">
												<input type="text" id="' . $elementos[ $i ][ 'campo' ] . '" placeholder="' . ucfirst( $titulo ) . '" class="gui-input alineado ' . $isFecha . '" ' . $funcionOn . ' maxlength="' . $elementos[ $i ][ 'tamano' ]. '">
											</label>
										</div>
									</div>';
								}
								$html .=
								'</div>
							</div>
						</div>
					</div>
				</div>';
			}
			echo $html;
		}
	}
	function mandaMail( $destinatarios, $asunto, $html, $con ) {
		if ( !class_exists( 'PHPMailer' ) ) { include 'class.phpmailer.php'; }
		$dominio = 'shala.us';
		$res1 = $con->Consulta( 'select * from sucursal' );
		$T = $con->Resultados( $res1 );
		$body =
		'<style> @import url( "https://fonts.googleapis.com/css?family=Montserrat:400,700" );</style>
		<div style="font-family: \'Montserrat\', sans-serif !important;" text-align: center; background-color: #eef0f3; color: #7F878D; margin-top: 0px!important; margin-bottom: 0px!important; margin-right: 0px!important; margin-left: 0px!important; padding-top: 0px; padding-bottom: 0px; padding-right: 0px; padding-left: 0px; font-size: 14px;">
			<center style="width: 100%; table-layout: fixed; background-color: #eef0f3">
				<br>
				<div style="padding: 5px 10px; background-color: #F2DFD1; color: #000; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%;" width="600">
					<table border="0" width="100%" style="width: 100%; background-color: #F2DFD1; color: #000;padding: 8px;">
						<tr valign="middle" style="background-color: #F2DFD1; color: #000;padding: 8px;">
							<td align="center" style="text-align: center; background-color: #F2DFD1; color: #000; padding: 8px;"><h3 align="center">SHALA</h3></td>
						</tr>
					</table>
				</div>
				<div style="background-color: #fff; color: #000; text-align: left; padding: 10px; width: 600px; margin-top: 0px; margin-bottom: 0px; margin-right: auto; margin-left: auto; max-width: 90%; font-size: 14px;" width="600">' . $html . '</div>
				<br>
			</center>
		</div>';
		$mail = new PHPMailer();
		$mail->Host = $dominio . "/";
		$mail->addCustomHeader( "X-Mailer: " . $dominio . "/ PHP/" . phpversion(), "Message-ID: <" . gmdate( 'YmdHs' ) . "@" . $dominio . "/>", "Sender: " . $dominio . "/", "Sent: " . date( 'd-m-Y' ) );
		$mail->From = 'info@' . $dominio;
		$mail->FromName = 'Shala Server';
		$mail->Subject = $asunto;
		$mail->AddAddress( $destinatarios[ 'email' ], $destinatarios[ 'nombre' ] );
		$mail->AddBCC( 'elopez@junkyard.mx' );
		$mail->Body = $body;
		$mail->IsHTML( true );
		$mail->CharSet = 'UTF-8';
		$mail->AltBody = $asunto;
		if ( $mail -> Send() ) {
			$status = 'Enviado';
		} else {
			$status = $mail->ErrorInfo;
		}
		return $status;
	}
	function archivoCorrecto( $archivo, $rutaArchivo ) {
		if ( !is_null( $archivo ) && $archivo != '' && file_exists( $rutaArchivo ) ) {
			return true;
		} else {
			return false;
		}
	}
	function palabraDiccionario( $palabra, $con ) {
		$texto = $palabra;
		$Query = $con->Consulta( 'select * from diccionario' );
		while( $U = $con->Resultados( $Query ) ) {
			if ( strtoupper( $palabra ) == strtoupper( $U[ 'campo' ] ) ) {
				$texto = $U[ 'etiqueta' ];
			}
		}
		$texto = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $texto );
		return $texto;
	}
	function obtenEmbedUrl( $url ) {
		$finalUrl = $url;
		if ( strpos( $url, 'facebook.com' ) !== false ) {
			$finalUrl = 'https://www.facebook.com/plugins/video.php?href=' . rawurlencode( $url ) . '&show_text=1&width=200';
		} else if ( strpos( $url, 'vimeo.com' ) !== false ) {
			$videoId = explode( 'vimeo.com/', $url )[1];
			if ( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://player.vimeo.com/video/' . $videoId;
		} else if( strpos( $url, 'youtube.com' ) !== false ) {
			$videoId = explode( 'v=',$url )[1];
			if( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
		} else if ( strpos( $url, 'youtu.be' ) !== false ) {
			$videoId = explode( 'youtu.be/', $url )[1];
			if ( strpos( $videoId, '&' ) !== false ) {
				$videoId = explode( '&', $videoId )[0];
			}
			$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
		}
		return $finalUrl;
	}
	function obtenCodigoVideo( $url ) {
		$finalUrl = $url;
		if ( strpos( $url, 'iframe' ) == false ) {
			if ( strpos( $url, 'facebook.com' ) !== false ) {
				$finalUrl = 'https://www.facebook.com/plugins/video.php?href=' . rawurlencode( $url ) . '&show_text=1&width=200';
			} else if ( strpos( $url, 'vimeo.com' ) !== false ) {
				$videoId = explode( 'vimeo.com/', $url )[1];
				if ( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = 'https://player.vimeo.com/video/' . $videoId;
			} else if( strpos( $url, 'youtube.com' ) !== false ) {
				$videoId = explode( 'v=',$url )[1];
				if( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
			} else if ( strpos( $url, 'youtu.be' ) !== false ) {
				$videoId = explode( 'youtu.be/', $url )[1];
				if ( strpos( $videoId, '&' ) !== false ) {
					$videoId = explode( '&', $videoId )[0];
				}
				$finalUrl = 'https://www.youtube.com/embed/' . $videoId;
			} else {
				$finalUrl = $url;
			}
		}
		return $finalUrl;
	}
	function obtenProductoData( $url, $con ) {
		$info = array();
		$strQuery = 'select * from productos';
		$Query = $con->Consulta( $strQuery );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'nombre' ], 'min' ) == $url ) {
				$info = $U;
				$archivos = array();
				$QueryArchivo = $con->Consulta( 'select * from productoarchivo where productoId=' . $U[ 'productoId' ] );
				while( $A = $con->Resultados( $QueryArchivo ) ) {
					$archivos[] = $A[ 'archivo' ];
				}
				$info[ 'archivos' ] = $archivos;
			}
		}
		return $info;
	}
	function obtenPostId( $url, $con ) {
		$postId = array();
		$Query = $con->Consulta( 'select * from blog' );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'titulo' ], 'min' ) == $url ) {
				$postId = $U;
			}
		}
		return $postId;
	}
	function obtenCategoriaId( $url, $con ) {
		$postId = array();
		$Query = $con->Consulta( 'select * from categorias' );
		while( $U = $con->Resultados( $Query ) ) {
			if ( limpialo( $U[ 'nombre' ], 'min' ) == $url ) {
				$postId = $U;
			}
		}
		return $postId;
	}
	function obtenCompras( $clienteId, $con ) {
		$html = '';
		$Query = $con->Consulta( 'select p.* from ventas v inner join productos p on(v.productoId=p.productoId) where v.clienteId=' . $clienteId . ' and v.statusId=2' );
		while( $R = $con->Resultados( $Query ) ) {
			$extras = '';
			/*$QueryDocs = $con->Consulta( 'select * from productoarchivo where productoId=' . $R[ 'productoId' ] );
			if ( $con->Cuantos( $QueryDocs ) ) {
                while( $D = $con->Resultados( $QueryDocs ) ) {
					if( strpos( $D[ 'archivo' ] , 'youtube.com' ) !== false ) {
						$extras .= '<a class="noHref" onclick="cargaVideo( ' . obtenCodigoVideo( $D[ 'archivo' ] ) . ')"><span class="ti-video-camera"></span></a>';
					} else {
						$extras .= '<a href="img/productos/' . $D[ 'archivo' ] .'" target="_blank"><span class="ti-folder"></span></a>';
					}
				}
			}*/
			$archivoPrincipal = '';
			/*if( !is_null( $R[ 'youtubeUrl' ] ) && $R[ 'youtubeUrl' ] != '' ) {
				$archivoPrincipal = '<a class="noHref" onclick="cargaVideo( \'' . obtenCodigoVideo( $R[ 'youtubeUrl' ] ) . '\' )"><span class="ti-video-camera"></span></a>';
			}*/
			$html .=
			'<div class="col-md-6 col-lg-6">
				<div class="single-blog-card card border-0 shadow-sm">
					<div class="card-body">
						<h3 class="h5 mb-2 card-title"><a href="producto/' . limpialo( $R[ 'nombre' ], 'min' ) . '">' . $R[ 'nombre' ] . '</a></h3>
						<p class="card-text">' . mb_substr( strip_tags( $R[ 'descripcion' ] ), 0, 150, 'utf-8' ) . '</p>
					</div>
					<!-- <div class="card-footer border-0 d-flex align-items-center justify-content-between">
						<h6>Archivos</h6>
						<br><br><br>
						' . $archivoPrincipal . '
						' . $extras . '
					</div>-->
				</div>
			</div>';
		}
		return $html;
	}
	function is_decimal( $val ) {
    	return is_numeric( $val ) && floor( $val ) != $val;
	}
	function getDefaultText() {
		$defaultText = array(
			'homeMenuText' => 'Home',
			'exploreMenuText' => 'Classes',
			'coachesMenuText' => 'Guides',
			'aboutMenuText' => 'About Us',
			'liveMenuText' => 'Live',
			'liveSessionText' => 'Live Sessions',
			'processingText' => 'Processing',
			'signUpText' => 'Sign Up',
			'memberText' => 'Member',
			'blogText' => 'Blog',
			'coachText' => 'Guide',
			'headerTitle' => 'Align. Heal. Act.',
			'subtitleHeader' => 'Upgrade your human software to unlock your full potential and find your flow',
			'trialDaysText' => 'TRY IT FREE FOR 14 DAYS',
			'trialDescriptionText' => 'Get started with two free weeks of unlimited breathwork, meditation, psychotherapy and connection programs. Cancel anytime.',
			'titleClasses' => 'PROGRAMS',
		    'subtitleClasses' => 'FIND A PROGRAM TO ACHIEVE YOUR GOALS',
			'videoHeader' => 'video-header.mp4',
			'videoMobileHeader' => 'video-header.mp4',
			'titleCategories' => 'WORK ON YOUR INNER SELF',
			'subtitleCategories' => 'Find the live classes, educational videos and exercises that take you on your own personal journey towards healing, bring you in alignment with others and set you on a path of action to fulfill your purpose.',
			'titleCoaches' => 'MEET OUR TEACHERS',
			'subtitleCoaches' => 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person'
		);
		return $defaultText;
	}
	function setTextDefault( $clientId, $con ) {
		$defaultText = getDefaultText();
		$str =
		'insert into generals (
			clientId,
			homeMenuText,
			exploreMenuText,
			coachesMenuText,
			aboutMenuText,
			liveMenuText,
			liveSessionText,
			processingText,
			signUpText,
			memberText,
			coachText,
			headerTitle,
			subtitleHeader,
			trialDaysText,
			trialDescriptionText,
			titleClasses,
			subtitleClasses,
			videoHeader,
			videoMobileHeader,
			titleCategories,
			subtitleCategories,
			titleCoaches,
			subtitleCoaches
		) values (
			' . $clientId . ',
			"' . $con->escapaTexto( $defaultText[ 'homeMenuText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'exploreMenuText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'coachesMenuText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'aboutMenuText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'liveMenuText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'liveSessionText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'processingText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'signUpText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'memberText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'coachText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'headerTitle' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'subtitleHeader' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'trialDaysText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'trialDescriptionText' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'titleClasses' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'subtitleClasses' ] ) . '",
			"' . $defaultText[ 'videoHeader' ] . '",
			"' . $defaultText[ 'videoMobileHeader' ] . '",
			"' . $con->escapaTexto( $defaultText[ 'titleCategories' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'subtitleCategories' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'titleCoaches' ] ) . '",
			"' . $con->escapaTexto( $defaultText[ 'subtitleCoaches' ] ) . '"
		)';
		$res = $con->Consulta( $str );
	}
?>