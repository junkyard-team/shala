<?php
	include( 'template_header.php' );
	include 'querys/conexion.php';
	include 'querys/functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
	$folder = $tabla;
	$tituloSeccion = palabraDiccionario( $tabla, $con );
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/core.css">
	</head>
	<body class="<?php echo $classBody; ?>">
		<div class="iconoPicker">
			<i class="fa fa-close cierra" onclick="popIconPicker( 2 )"></i>
			<div id="iconosMuestra"></div>
		</div>
		<div id="main">
			<?php include('template_sidebar.php');?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title"><?php echo ucfirst( $tituloSeccion ); ?></span></div>
						<div class="panel-body pn">
							<div class="tablaScroll">
								<div class="pull-right" id="paginacionTabla"></div>
								<table data-page-navigation=".pagination" data-page-size="20" class="table footable">
									<thead>
										<tr><?php armaTabla( $tabla, $con ); ?></tr>
									</thead>
									<tbody id="contenidoTabla"></tbody>
									<tfoot class="footer-menu">
										<tr>
											<td colspan="8">
												<nav class="text-right">
													<ul class="pagination hide-if-no-paging"></ul>
												</nav>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<div id="modal-data" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Información</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="row">
							<?php armaFormulario( $tabla, $con ); ?>
						</div>
						<?php armaAcordeones( $tabla, $con ); ?>
					</div>
					<div class="panel-footer">
						<button class="button btn-primary" onclick="guardaFormulario()">Guardar</button>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php if ( $tabla == 'pedidos' ) : ?>
		<div id="modalDetalle" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-search"></i>Detalle de Compra</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<table class="table">
							<thead>
								<tr>
									<th>Producto</th>
									<th>Cantidad</th>
									<th>Precio al momento</th>
								</tr>
							</thead>
							<tbody id="detalleTabla"></tbody>
						</table>
						<hr>
						<div style="text-align: right;"><b>Total del pedido</b> <span id="totalPedido"></span></div>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php endif; ?>
		<?php
			include('template_footer_scripts.php');
			$con->CierraConexion();
		?>
		<script src="assets/js/demo/widgets.js"></script>
		<script src="plugins/doublescroll.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					$( 'a[data-event="fontSize"]' ).click( function( e ) {
						e.preventDefault();
						e.stopPropagation();
						return false;
					} );
					<?php
						if ( $folder != '' ) {
							echo 'localStorage.setItem( \'folderActivo\', \'' . $folder . '\' );';
						}
						echo 'localStorage.setItem( \'tabla\', \'' . $tabla . '\' );';
					?>
					localStorage.setItem( 'contenedor', 'modal-data' );
					$( '.fecha' ).datetimepicker( { format: "YYYY-MM-DD" } );
					$( '.summernote' ).summernote( {
						height: 300,
						focus: false,
						toolbar: [
							[ 'font', [ 'bold', 'italic', 'underline', 'clear' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
							[ 'height', [ 'height' ] ],
							[ 'insert', [ 'link', 'picture', 'hr' ] ]
						],
						popover: {
							image: [
								[ 'custom', [ 'imageAttributes' ] ],
								[ 'imagesize', [ 'imageSize100', 'imageSize50', 'imageSize25' ] ],
								[ 'float', [ 'floatLeft', 'floatRight', 'floatNone' ] ],
								[ 'remove', [ 'removeMedia' ] ]
							],
						},
						lang: 'es-ES',
						imageAttributes:{
							icon:'<i class="fa fa-edit"/>',
							removeEmpty:false,
							disableUpload: false
						},
						onImageUpload: function( image ) {
							console.log( 'subiendo imagen' );
							subeSummerImg( image[ 0 ], $( this ).attr( 'id' ) );
						}
					} );
					$( '.select2-multiple' ).select2( {
						placeholder: "Elige Opciones",
						allowClear: true
					} );
					$( '.color' ).spectrum( {
						color: bgInfo,
						appendTo: $( '.color' ).parents('.sfcolor').parent(),
						containerClassName: 'sp-left'
					} );
					$( '.color' ).show();
					checaUsuario();
					listado();
				} );
			} );
		</script>
	</body>
</html>