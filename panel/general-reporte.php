<?php
	include( 'template_header.php' );
	include 'querys/functions.php';
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/core.css">
	</head>
	<body class="<?php echo $classBody; ?>">
		<div id="main">
			<?php include('template_sidebar.php');?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title">Reporte de <?php echo ucfirst( $tabla ); ?></span></div>
						<div class="panel-body pn">
							<iframe src="pdf/general.php?tabla=<?php echo $tabla; ?>" class="reportesFrame"></iframe>
						</div>
					</div>
				</section>
				<?php include( 'template_footer.php' );?>
			</section>
		</div>
		<?php
			include('template_footer_scripts.php');
		?>
		<script src="assets/js/demo/widgets.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					<?php
						echo 'localStorage.setItem( \'tabla\', \'' . $tabla . '\' );';
					?>
					checaUsuario();
				} );
			} );
		</script>
	</body>
</html>