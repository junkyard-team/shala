<?php
	include( 'template_header.php' );
	include 'querys/conexion.php';
	include 'querys/functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = ( isset( $_REQUEST[ 'tabla' ] ) ) ? limpialo( $_REQUEST[ 'tabla' ], 'min' ) : '';
	$folder = $tabla;
	$tituloSeccion = palabraDiccionario( $tabla, $con );
	$funcionGuarda = 'guardaFormulario()';
?>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/footable.core.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/summernote.css?v=1">
		<link rel="stylesheet" type="text/css" href="plugins/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="plugins/css/core.css">
		<link rel="stylesheet" href="plugins/css/dropzone.css">
	</head>
	<body class="<?php echo $classBody; ?>">
		<div class="iconoPicker">
			<i class="fa fa-close cierra" onclick="popIconPicker( 2 )"></i>
			<div id="iconosMuestra"></div>
		</div>
		<div id="main">
			<?php include( 'template_sidebar.php' ); ?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div id="spy3" class="panel">
						<div class="panel-heading"><span class="panel-title"><?php echo ucfirst( $tituloSeccion ); ?></span></div>
						<div class="panel-body pn">
							<div class="row">
								<div role="form" class="admin-form" style="float: left">
									<button class="button btn-primary" onclick="iniciaFormulario()">Add</button>
								</div>
							</div>
							<div class="tablaScroll">
								<div class="pull-right" id="paginacionTabla"></div>
								<table data-page-navigation=".pagination" data-page-size="20" class="table footable">
									<thead>
										<tr><?php armaTabla( $tabla, $con ); ?></tr>
									</thead>
									<tbody id="contenidoTabla"></tbody>
									<tfoot class="footer-menu">
										<tr>
											<td colspan="8">
												<nav class="text-right">
													<ul class="pagination hide-if-no-paging"></ul>
												</nav>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<div id="modal-data" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i><?php echo ucfirst( $tituloSeccion ); ?> Info</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="row">
							<?php armaFormulario( $tabla, $con ); ?>
						</div>
						<?php armaAcordeones( $tabla, $con ); ?>
					</div>
					<div class="panel-footer">
						<button class="button btn-primary" onclick="<?php echo $funcionGuarda; ?>">Save</button>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<div id="videosPopup" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Video List</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<input type="text" class="buscaVideo form-control" placeholder="Videos Filter">
						<ul class="listaVideos" id="contenidoVideo">
						</ul>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php if ( $tabla == 'productos' || $tabla == 'videos' ) : ?>
		<div id="modalGaleria" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-picture-o"></i>Archivos</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="row">
							<form id="dropZone" action="querys/galerias.php" class="dropzone dropzone-sm">
								<div class="fallback">
									<input name="upl" type="file">
								</div>
							</form>
						</div>
						<div class="row" id="galeriaListDiv"></div>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php endif; ?>
		<?php if ( $tabla == 'videos' ) :?>
		<div id="modal-filtros" class="popup-basic admin-form mfp-with-anim mfp-hide miModal">
			<div class="panel">
				<div class="panel-heading"><span class="panel-title"><i class="fa fa-edit"></i>Filters</span></div>
				<div id="comment">
					<div class="panel-body p25">
						<div class="form-group mb10 row">
							<div class="form-group mb10 row">
								<label for="inputSelect" class="col-lg-3 control-label">Filter</label>
								<div class="col-lg-8">
									<input type="hidden" id="videoFiltro" class="id" value="0">
									<label class="field select setenta">
										<select name="propiedades" id="propiedades" class="obligatorio" onchange="listaPropiedadValor( this.value )"></select><i class="arrow"></i>
									</label>
									<a class="addi" onclick="editaPropiedad( $( '#propiedades' ).val() )"><i class="fa fa-edit"></i></a>
									<a class="addi" onclick="eliminaPropiedad( $( '#propiedades' ).val() )"><i class="fa fa-minus"></i></a>
									<a class="addi" onclick="agregaPropiedad()"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="row">
							<a class="boton" onclick="guardaInfo()"><i class="fa fa-save"></i> Save Changes</a>
							<a class="boton" onclick="addElement()"><i class="fa fa-plus"></i> Add Element</a>
						</div>
						<div class="form-group mb10 row" id="listaPropiedad">
						</div>
					</div>
				</div>
			</div>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div>
		<?php endif; ?>
		<?php
			include('template_footer_scripts.php');
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkwYo-1e42rL_fS43hLbGYBUYBhxqrS-A"></script>
		<script src="assets/js/demo/widgets.js"></script>
		<script src="assets/js/signature_pad.min.js"></script>
		<script src="plugins/doublescroll.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery( document ).ready( function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					$( 'a[data-event="fontSize"]' ).click( function( e ) {
						e.preventDefault();
						e.stopPropagation();
						return false;
					} );
					<?php
						if ( $folder != '' ) {
							echo 'localStorage.setItem( \'folderActivo\', \'' . $folder . '\' );';
						}
						echo 'localStorage.setItem( \'tabla\', \'' . $tabla . '\' );';
					?>
					<?php if ( $tabla == 'videos' ) {
						echo 'localStorage.setItem( \'videoKey\', \'' . obtenVideoKey( $con ) . '\' );';
					} ?>
					<?php if ( $tabla == 'platforms' ) {
						echo '$( \'label[for="clientId"]\' ).parent().parent().hide();';
					} ?>
					localStorage.setItem( 'contenedor', 'modal-data' );
					$( '.fecha' ).datetimepicker( { format: "YYYY-MM-DD" } );
					$( '.summernote' ).summernote( {
						height: 300,
						focus: false,
						toolbar: [
							['view', ['codeview']],
							[ 'font', [ 'bold', 'italic', 'underline', 'clear' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
							[ 'height', [ 'height' ] ],
							[ 'insert', [ 'link', 'picture', 'hr' ] ]
						],
						popover: {
							image: [
								[ 'custom', [ 'imageAttributes' ] ],
								[ 'imagesize', [ 'imageSize100', 'imageSize50', 'imageSize25' ] ],
								[ 'float', [ 'floatLeft', 'floatRight', 'floatNone' ] ],
								[ 'remove', [ 'removeMedia' ] ]
							],
						},
						lang: 'es-ES',
						imageAttributes:{
							icon:'<i class="fa fa-edit"/>',
							removeEmpty:false,
							disableUpload: false
						},
						onImageUpload: function( image ) {
							console.log( 'subiendo imagen' );
							subeSummerImg( image[ 0 ], $( this ).attr( 'id' ) );
						},
						fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
					} );
					$( '.select2-multiple' ).select2( {
						placeholder: "Elige Opciones",
						allowClear: true
					} );
					$( '.opcion' ).select2();
					$( '.color' ).spectrum( {
						color: bgInfo,
						appendTo: $( '.color' ).parents('.sfcolor').parent(),
						containerClassName: 'sp-left'
					} );
					$( '.color' ).show();
					if ( $( '.googleMaps' ).length ) {
						var mapaId = $( '.googleMaps' ).attr( 'id' );
						console.log( mapaId );
						geocoder = new google.maps.Geocoder();
						var latlng = new google.maps.LatLng( -34.397, 150.644 );
						var mapOptions = {
							zoom: 14,
							center: latlng
						}
						map = new google.maps.Map( document.getElementById( mapaId ), mapOptions );
						$( '.buscaMapa' ).change( function() {
							if ( $( this ).val() != '' ) {
								buscaDireccion( $( this ).val() );
							}
						} );
						$( '.buscaMapa' ).keyup( function() {
							clearTimeout( typingTimer );
							if ( $( this ).val() != '' ) {
								typingTimer = setTimeout( buscaDireccion( $( this ).val() ), doneTypingIntervalMaps );
							}
						} );
					}
					if ( $( 'canvas' ).length ) {
						signaturePad = new SignaturePad(
							document.querySelector( 'canvas' ),
							{
								maxWidth: 2,
								backgroundColor: 'rgb(255,255,255)'
							}
						);
					}
					checaUsuario();
					listado();
					<?php if ( $tabla == 'productos' || $tabla == 'videos' ) : ?>
					myDropzone = new Dropzone( "#dropZone" );
					myDropzone.options.dropZone = {
						autoProcessQueue: true,
						uploadMultiple: true,
						parallelUploads: 100,
						maxFiles: 0,
						addRemoveLinks: false,
						dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> <span class="main-text"><b>Arrastra tus archivos</b> para subir</span>',
						dictResponseError: 'Problemas Al Subir intente de nuevo',
					};
					myDropzone.on( 'success', function( response ) {
						listaGaleria( localStorage.getItem( 'elementoId' ) );
					} );
					<?php endif; ?>
				} );
			} );
		</script>
	</body>
</html>
<?php $con->CierraConexion(); ?>