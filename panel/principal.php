<?php include('template_header.php');?>
		<link rel="stylesheet" type="text/css" href="plugins/css/fullcalendar.css">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arimo:400,700%7CRoboto:400,300,500,700">
		<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
	</head>
	<body class="<?php echo $classBody; ?>">
		<div id="main">
			<?php include('template_sidebar.php');?>
			<section id="content_wrapper">
				<section id="content" class="table-layout animated fadeIn">
					<div class="tray tray-center">
						<div class="admin-panels">
							<div class="row">
								<div class="col-md-12 admin-grid tcenter ptop">
									<!-- <img src="assets/img/logos/logo-negro.png" class="logoPrincipal"> -->
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php include('template_footer.php');?>
			</section>
		</div>
		<?php include('template_footer_scripts.php');?>
		<script src="assets/js/demo/widgets.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				"use strict";
				Core.init();
				$( document ).ready(function () {
					checaUsuario();
				} );
			} );
		</script>
	</body>
</html>