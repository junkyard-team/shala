<?php
	class Conexion {
		public $conexion;
		private $database;
		private $server;
		private $usr;
		private $pass;
		public function __construct() {
			$this->server='localhost';
			$this->user = 'root';
			$this->pass = '';
			$this->database = 'shala';
			$this->conexion = mysqli_connect( $this->server, $this->user, $this->pass, $this->database );
		}
		public function AbreConexion() {
			$this->conexion = mysqli_connect( $this->server, $this->user, $this->pass, $this->database );
			if ( $this->conexion ) {
				mysqli_set_charset( $this->conexion, 'utf8' );
				return true;
			} else {
				return mysqli_error();
			}
		}
		public function Consulta( $query ) {
			$res = mysqli_query( $this->conexion, $query );
			return $res;
		}
		public function Resultados( $res ) {
			$result = mysqli_fetch_array( $res );
			return $result;
		}
		public function Ultimo() {
			$res = mysqli_insert_id( $this->conexion );
			return $res;
		}
		public function Cuantos( $res ) {
			$dato = mysqli_num_rows( $res );
			return $dato;
		}
		public function CierraConexion() {
			mysqli_close( $this->conexion );
		}
		public function MuestraError() {
			mysqli_error( $this->conexion );
		}
		public function escapaTexto( $texto ) {
			return mysqli_real_escape_string( $this->conexion, $texto );
		}
		public function getDataBaseName() {
			return $this->database;
		}
	}
?>
