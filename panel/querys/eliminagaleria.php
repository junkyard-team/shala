<?php
	include 'conexion.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = ( $_REQUEST[ 'tabla' ] == 'videos' ) ? 'galeria' : 'galeriaproducto';
	$campo = ( $_REQUEST[ 'tabla' ] == 'videos' ) ? 'videoId' : 'productoId';
	$str1 = 'select * from ' . $tabla . ' where galeriaId=' . $_REQUEST[ 'id' ];
	$res1 = $con->Consulta( $str1 );
	$R = $con->Resultados( $res1 );
	$str = 'delete from ' . $tabla . ' where galeriaId=' . $_REQUEST[ 'id' ];
	$res = $con->Consulta( $str );
	if ( $res ) {
		$archivo = '../../images/' . $tabla . '/' . $R[ 'imagen' ];
		if ( file_exists( $archivo ) ) {
			unlink( $archivo );
		}
		$status = array( 'status' => 'Success', 'elemento' => $R[ $campo ] );
	} else {
		$status = array( 'status' => 'Error');
	}
	echo json_encode( $status );
	$con->CierraConexion();
?>