<?php
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$body = '<h2>Solicitud de Contacto del Sitio</h2><br><table><tr><th>Campo</th><th>Valor</th></tr>';
	foreach ($_REQUEST as $param_name => $param_val) {
		if ( strpos( $param_name, '_' ) == false && strpos( $param_name, 'PHP' ) == false ) {
			$valor = ( $param_val == 'undefined' ) ? '' : $param_val;
			$body .= '<tr><td>' . strtoupper( $param_name ) . '</td><td>' . $valor . '</td></tr>';
		}
	}
	$emailsTienda = array();
	$str = 'select email from nosotros';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( !in_array( $R[ 'email' ], $emailsTienda)) {
			$emailsTienda[] = $R[ 'email' ];
		}
	}
	foreach ( $emailsTienda as $correoManda ) {
		$destinatario[] = array( 'email' => $correoManda, 'nombre' => 'Contacto Deorno' );
	}
	$statusMail = mandaMail( $destinatario, 'Solicitud de Contacto Pagina Web', $body, $con );
	if ( $statusMail == 'Enviado' ) {
		$status = array( 'status' => 'Success' );
	} else {
		$status = array( 'status' => 'Error', 'error' => $statusMail );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>