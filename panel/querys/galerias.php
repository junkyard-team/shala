<?php
	include 'functions.php';
	$allowed = array( 'png', 'jpg', 'gif', 'jpeg', 'pdf', 'mp4' );
	foreach ( $_FILES as $archivo ) {
		if ( $archivo[ 'error' ] == 0 ) {
			$extension = strtolower( pathinfo( $archivo[ 'name' ], PATHINFO_EXTENSION ) );
			if ( !in_array( $extension, $allowed ) ) {
				echo '{"status":"extension"}';
			} else {
				$ruta = '../../images/productos/';
				if ( !file_exists( $ruta ) ) {
					mkdir( $ruta, 0777 );
				}
				$temporal = str_replace( $extension, '', $archivo[ 'name' ] );
				$nombre_fichero = limpialo( $temporal, 'min' ) . '.' . $extension;
				if ( move_uploaded_file( $archivo[ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
					include 'conexion.php';
					$con = new Conexion();
					$con->AbreConexion();
					$str = 'insert into productoarchivo( productoId, archivo ) values( ' . $_REQUEST[ 'elementoId' ] . ', "' . $nombre_fichero . '" )';
					$res = $con->Consulta( $str );
					$con->CierraConexion();
					echo '{"status":"success","archivo":"' . $nombre_fichero . '","elemento":"' . $_REQUEST[ 'elementoId' ] . '"}';
				} else {
					echo '{"status":"guardado"}';
				}
			}
		} else {
			echo '{"status":"error"}';
		}
	}
	exit;
?>