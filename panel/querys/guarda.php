<?php
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = '';
	$folderActivo = '';
	$archivoSubida = 'No Trae';
	$multiples = '';
	if ( $_REQUEST ) {
		foreach ( $_REQUEST as $key => $value ) {
			if ( $key == 'tabla' ) {
				$tabla = $value;
			} else if ( $key == 'folderActivo' ) {
				$folderActivo = $value . '/';
			} else if ( $key == 'multiples' ) {
				$multiples = json_decode( $value );
			}
		}
	}
	if ( $_FILES ) {
		foreach ( $_FILES as $file => $array ) {
			$allowed = array( 'png', 'jpg', 'gif', 'pdf', 'jpeg', 'webp', 'mp4', 'ttf', 'otf', 'woff', 'eot' );
			if ( isset( $_FILES[ $file ] ) && $_FILES[ $file ][ 'error' ] == 0 ) {
				$extension = strtolower( pathinfo( $_FILES[ $file ][ 'name' ], PATHINFO_EXTENSION ) );
				if ( !in_array( $extension, $allowed ) ) {
					$archivoSubida = 'Extension no permitida';
				} else {
					$ruta = '../../images/' . $folderActivo;
					if ( !file_exists( $ruta ) ) {
    					mkdir( $ruta, 0777 );
    				}
					$temporal = str_replace( $extension, '', $_FILES[ $file ][ 'name' ] );
					if ( isset( $_REQUEST[ 'nombre' ] ) ) {
						$nombre_fichero = limpialo( $temporal, 'min' ) . '-' . limpialo( $_REQUEST[ 'nombre' ], 'min' ) . '.' . $extension;
					} else {
						$nombre_fichero = limpialo( $temporal, 'min' ) . '.' . $extension;
					}
					if ( move_uploaded_file( $_FILES[ $file ][ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
						$archivos[ $file ] = $nombre_fichero;
						$archivoSubida = 'OK';
					} else {
						$archivoSubida = 'No Guardo Archivo';
					}
				}
			} else {
				$archivoSubida = 'El archivo subio mal';
			}
		}
	}
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '"';
	$res = $con->Consulta( $str );
	$insert = 'insert into ' . $tabla. ' (';
	$update = 'update ' . $tabla . ' set ';
	$fieldId = '';
	$values = '';
	$updateIniciado = 0;
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'N' ) {
			if ( isset( $archivos[ $R[ 'columna' ] ] ) ) {
				if ( $values == '' ) {
					$values = '"' . $archivos[ $R[ 'columna' ] ] . '"';
					$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' );
					$updateIniciado++;
					$insert .= $R[ 'columna' ];
				} else {
					$values .= ', "' . $archivos[ $R[ 'columna' ] ] . '"';
					$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $archivos[ $R[ 'columna' ] ] . '" ' );
					$updateIniciado++;
					$insert .= ', ' . $R[ 'columna' ];
				}
			} else {
				if ( $R[ 'comentarios' ] != 'archivo' ) {
					if ( isset( $_REQUEST[ $R[ 'columna' ] ] ) ) {
						$valorRecibido = ( $R[ 'comentarios' ] == 'html' ) ? htmlentities( $_REQUEST[ $R[ 'columna' ] ] ) : $_REQUEST[ $R[ 'columna' ] ];
						$valorRecibido = ( $R[ 'comentarios' ] == 'pass' ) ? encripta( $_REQUEST[ $R[ 'columna' ] ] ) : $_REQUEST[ $R[ 'columna' ] ];
						if ( $values == '' ) {
							$insert .= $R[ 'columna' ];
							if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'mediumtext' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'text' ) {
								$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' );
								$values = '"' . $con->escapaTexto( $valorRecibido ) . '"';
								$updateIniciado++;
							} else {
								$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $valorRecibido . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $valorRecibido . ' ' );
								$values = $valorRecibido;
								$updateIniciado++;
							}
						} else {
							$insert .= ', ' . $R[ 'columna' ];
							if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'mediumtext' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'text' ) {
								$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $con->escapaTexto( $valorRecibido ) . '" ' );
								$updateIniciado++;
								$values .= ', "' . $con->escapaTexto( $valorRecibido ) . '"';
							} else {
								$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $valorRecibido . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $valorRecibido . ' ' );
								$updateIniciado++;
								$values .= ', ' . $valorRecibido;
							}
						}
					} else {
						if ( $R[ 'obligatoria' ] == 'S' && is_null( $R[ 'valorDefault' ] ) ) {
							if ( $R[ 'tipoDatos' ] == 'varchar' || $R[ 'tipoDatos' ] == 'char' || $R[ 'tipoDatos' ] == 'text' || $R[ 'tipoDatos' ] == 'longtext' || $R[ 'tipoDatos' ] == 'mediumtext' ) {
								if ( $values == '' ) {
									$insert .= $R[ 'columna' ];
									$values = '""';
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="" ' ) : ( ', ' . $R[ 'columna' ] . '="" ' );
									$updateIniciado++;
								} else {
									$insert .= ', ' . $R[ 'columna' ];
									$values .= ', ""';
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="" ' ) : ( ', ' . $R[ 'columna' ] . '="" ' );
									$updateIniciado++;
								}
							} else if ( $R[ 'tipoDatos' ] == 'date' || $R[ 'tipoDatos' ] == 'datetime' ) {
								$fecha = ( $R[ 'tipoDatos' ] == 'date' ) ? 'Curdate()' : 'now()';
								if ( $values == '' ) {
									$insert .= $R[ 'columna' ];
									$values = $fecha;
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $fecha . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $fecha . ' ' );
									$updateIniciado++;
								} else {
									$insert .= ', ' . $R[ 'columna' ];
									$values .= ', ' . $fecha;
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=' . $fecha . ' ' ) : ( ', ' . $R[ 'columna' ] . '=' . $fecha . ' ' );
									$updateIniciado++;
								}
							} else {
								if ( $values == '' ) {
									$insert .= $R[ 'columna' ];
									$values .= '0';
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=0 ' ) : ( ', ' . $R[ 'columna' ] . '=0 ' );
									$updateIniciado++;
								} else {
									$insert .= ', ' . $R[ 'columna' ];
									$values .= ', 0';
									$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '=0 ' ) : ( ', ' . $R[ 'columna' ] . '=0 ' );
									$updateIniciado++;
								}
							}
						}
					}
				} else {
					if ( isset( $_REQUEST[ $R[ 'columna' ] ] ) ) {
						$insert .= ', ' . $R[ 'columna' ];
						$update .= ( $updateIniciado == 0 ) ? ( $R[ 'columna' ] . '="' . $_REQUEST[ $R[ 'columna' ] ] . '" ' ) : ( ', ' . $R[ 'columna' ] . '="' . $_REQUEST[ $R[ 'columna' ] ] . '" ' );
						$values = ( $values == '' ) ? '"' . $_REQUEST[ $R[ 'columna' ] ] . '"' : $values . ',"' . $_REQUEST[ $R[ 'columna' ] ] . '"';
						$updateIniciado++;
					} else {
						if ( $R[ 'obligatoria' ] == 'S' && is_null( $R[ 'valorDefault' ] ) ) {
							if ( $values == '' ) {
								$insert .= $R[ 'columna' ];
								$values = '""';
							} else {
								$insert .= ', ' . $R[ 'columna' ];
								$values .= ', ""';
							}
						}
					}
				}
			}
		} else {
			$id = $_REQUEST[ $R[ 'columna' ] ];
			$fieldId = $R[ 'columna' ];
			$tipoId = $R[ 'tipoDatos' ];
			if ( preg_match('/\beditable\b/', $R[ 'comentarios' ] ) ) {
				if ( $values == '' ) {
					$insert .= $R[ 'columna' ];
					if ( $tipoId == 'int' ) {
						$values .= $_REQUEST[ $R[ 'columna' ] ];
					} else {
						$values .= '"' . $_REQUEST[ $R[ 'columna' ] ] . '"';
					}
				} else {
					$insert .= ', ' . $R[ 'columna' ];
					if ( $tipoId == 'int' ) {
						$values .= ', ' . $_REQUEST[ $R[ 'columna' ] ];
					} else {
						$values .= ', "' . $_REQUEST[ $R[ 'columna' ] ] . '"';
					}
				}
			}
		}
	}
	if ( $_REQUEST[ 'tabla' ] == 'pedidos' ) {
		$resPedido = $con->Consulta( 'select * from pedidos where idpedido=' . $_REQUEST[ 'idpedido' ] );
		$P = $con->Resultados( $resPedido );
	}
	$insert .= ' ) values( ' . $values . ')';
	if ( $id == 0 ) {
		$strNew = $insert;
	} else {
		$strNew = $update . ' where ' . $fieldId . '=' . $id;
	}
	$resNew = $con->Consulta( $strNew );
	if ( $resNew ) {
		$id = ( $id == 0 ) ? $con->Ultimo() : $id;
		if ( $tabla == 'platforms' && $_REQUEST[ 'platformId' ] == 0 ) {
			$fontFile = ( isset( $archivos[ 'fontFile' ] ) ) ? '"' . $archivos[ 'fontFile' ] . '"' : 'null';
			$fontFileHead = ( isset( $archivos[ 'fontFileHead' ] ) ) ? '"' . $archivos[ 'fontFileHead' ] . '"' : 'null';
			$str = 'insert into clients ( platformId, nombre, logotipo, icono, fontFile, fontFileHead, url, email, pass ) values( ' . $id . ', "' . $con->escapaTexto( $_REQUEST[ 'nombre' ] ) . '", "' . $archivos[ 'logotipo' ] . '", "' . $archivos[ 'icono' ] . '", ' . $fontFile . ', ' . $fontFileHead . ', "' . $_REQUEST[ 'url' ] . '", "' . $_REQUEST[ 'email' ] . '", "' . encripta( $_REQUEST[ 'email' ] ) . '")';
			$res = $con->Consulta( $str );
			if ( $res ) {
				$clientId = $con->Ultimo();
				$str = 'update platforms set clientId=' . $clientId . ' where platformId=' . $id;
				$res = $con->Consulta( $str );
				$str = 'insert into clientpages (clientId, pageId) values (' . $clientId . ', 1), (' . $clientId . ', 2), (' . $clientId . ', 3), (' . $clientId . ', 4)';
				$res = $con->Consulta( $str );
				$str = 'insert into homeplatform (clientId, homeId) values (' . $clientId . ', 1), (' . $clientId . ', 2), (' . $clientId . ', 3), (' . $clientId . ', 4), (' . $clientId . ', 5), (' . $clientId . ', 6)';
				$res = $con->Consulta( $str );
				setTextDefault( $clientId, $con );
				if ( $_FILES ) {
					$rutaPlatform = '../../images/platforms/';
					$rutaClient = '../../images/clients/';
					copy( $rutaPlatform . $archivos[ 'logotipo' ], $rutaClient . $archivos[ 'logotipo' ] );
					copy( $rutaPlatform . $archivos[ 'icono' ], $rutaClient . $archivos[ 'icono' ] );
					if ( $fontFile != 'null' ) { copy( $rutaPlatform . $archivos[ 'fontFile' ], $rutaClient . $archivos[ 'fontFile' ] ); }
					if ( $fontFileHead != 'null' ) { copy( $rutaPlatform . $archivos[ 'fontFileHead' ], $rutaClient . $archivos[ 'fontFileHead' ] ); }
				}
			}
		}
		if ( $multiples != '' ) {
			foreach ( $multiples as $multiple ) {
				$str = 'delete from ' . $multiple->destino . ' where ' . $fieldId . '=' . $id;
				$res = $con->Consulta( $str );
				$strMultiple = 'insert into ' . $multiple->destino . '( ' . $fieldId;
				$str = 'select COLUMN_NAME as columna from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $multiple->destino . '" and COLUMN_KEY<>"PRI" and COLUMN_NAME<>"' . $fieldId . '"';
				$res = $con->Consulta( $str );
				while( $R = $con->Resultados( $res ) ) {
					$strMultiple .= ', ' . $R[ 'columna' ];
				}
				$strMultiple .= ' ) values( ';
				$idsCatalogo = explode( '-', $multiple->ids );
				for ( $i = 0; $i < sizeof( $idsCatalogo ); $i++ ) {
					$str = $strMultiple . $id . ', ' . $idsCatalogo[ $i ] . ' )';
					$res = $con->Consulta( $str );
				}
			}
		}
		$status = array( 'status' => 'Success', 'archivo' => $archivoSubida, 'id' => $id );
	} else {
		$status = array( 'status' => 'Error', 'archivo' => $archivoSubida );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>