<?php
	ini_set( 'max_execution_time', 3000 );
	ini_set( 'max_input_time ', 3000 );
	ini_set( 'post_max_size ', '200M' );
	ini_set( 'upload_max_filesize ', '200M' );
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$folderActivo = $tabla . '/';
	$album = $_REQUEST[ 'album' ];
	$campoAlbumId = getCampoId( $album, $con );
	$albumId = $_REQUEST[ 'albumId' ];
	if ( $_FILES ) {
		foreach ( $_FILES as $file => $array ) {
			$allowed = array( 'png', 'jpg', 'gif', 'pdf', 'jpeg' );
			if ( isset( $_FILES[ $file ] ) && $_FILES[ $file ][ 'error' ] == 0 ) {
				$extension = strtolower( pathinfo( $_FILES[ $file ][ 'name' ], PATHINFO_EXTENSION ) );
				if ( !in_array( $extension, $allowed ) ) {
					$archivoSubida = 'Extension no permitida';
				} else {
					$ruta = '../../images/' . $folderActivo;
					if ( !file_exists( $ruta ) ) {
    					mkdir( $ruta, 0777 );
    				}
					$temporal = str_replace( $extension, '', $_FILES[ $file ][ 'name' ] );
					$nombre_fichero = limpialo( $temporal, 'min' ) . '.' . $extension;
					if ( move_uploaded_file( $_FILES[ $file ][ 'tmp_name' ], $ruta . $nombre_fichero ) ) {
						$str = 'insert into ' . $tabla . ' ( ' . $campoAlbumId . ', imagen, ' . getCampoPosicion( $tabla, $con ) . ' ) values( ' . $albumId . ', \'' . $nombre_fichero . '\', 99 )';
						$res = $con->Consulta( $str );
					}
				}
			}
		}
	}
	if ( $res ) {
		$status = array( 'status' => 'Success' );
	} else {
		$status = array( 'status' => 'Error' );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>