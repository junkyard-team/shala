<?php
	ini_set( 'default_charset', 'utf-8' );
	header( 'Content-Type: text/html; charset=utf-8' );
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$multiplesComment = '';
	$posicionCampo = '';
	$idiomaCampo = '';
	$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" and COLUMN_COMMENT<>"hoy" order by COLUMN_KEY desc, COLUMN_COMMENT asc';
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		if ( $R[ 'esLlave' ] == 'S' ) {
			$campoId = $R[ 'columna' ];
			$tipoId = $R[ 'tipoDatos' ];
			if ( preg_match('/\bmultiple\b/', $R[ 'comentarios' ] ) ) {
				$multiplesComment = $R[ 'comentarios' ];
			}
		}
		if ( $R[ 'comentarios' ] == 'html' ) {
			$tipoHtml[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'icono' ) {
			$tipoIcono[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'pass' ) {
			$tipoPass[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'liga' ) {
			$tipoLiga[] = $R[ 'columna' ];
		}
		if ( $R[ 'comentarios' ] == 'posicion' ) {
			$posicionCampo = $R[ 'columna' ];
		}
		if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
			$tipoOpciones[] = $R[ 'columna' ];
		}
		if ( preg_match('/\bidioma\b/', $R[ 'comentarios' ] ) ) {
			$idiomaCampo = $R[ 'columna' ];
		}
	}
	if ( isset( $_REQUEST[ 'id' ] ) ) {
		if ( $tipoId == 'int' ) {
			$str = 'select * from ' . $tabla. ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
		} else {
			$str = 'select * from ' . $tabla. ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
		}
		$res = $con->Consulta( $str );
		$R = $con->Resultados( $res );
		$status = array(
			'status' => 'Success'
		);
		foreach ( $R as $key => $value ) {
			if ( !is_numeric( $key ) ) {
				if ( isset( $tipoHtml ) ) {
					$valor = ( in_array( $key, $tipoHtml ) ) ? html_entity_decode( $value ) : $value;
				} else {
					$valor = $value;
				}
				if ( isset( $tipoPass ) && $value != '' ) {
					$valor = ( in_array( $key, $tipoPass ) ) ? desencripta( $value ) : $value;
				}
				$status[ $key ] = $valor;
			}
		}
		if ( $multiplesComment != '' ) {
			$variosMultiples = explode( ';', $multiplesComment );
			for ( $i = 0; $i < sizeof( $variosMultiples ); $i++ ) {
				if ( $variosMultiples[ $i ] != '' ) {
					$infoTablas = explode( '=', $variosMultiples[ $i ] )[1];
					$tablas = explode( ',', $infoTablas );
					$tablaCatalogo = $tablas[0];
					$tablaDestino = $tablas[1];
					$catalogoId = getCampoId( $tablaCatalogo, $con );
					if ( $catalogoId != '' ) {
						if ( $tipoId == 'int' ) {
							$str = 'select ' . $catalogoId . ' from ' . $tablaDestino . ' where ' . $campoId . '=' . $_REQUEST[ 'id' ];
						} else {
							$str = 'select ' . $catalogoId . ' from ' . $tablaDestino . ' where ' . $campoId . '="' . $_REQUEST[ 'id' ] . '"';
						}
						$res = $con->Consulta( $str );
						$mt = 0;
						while( $R = $con->Resultados( $res ) ) {
							$multiples[ $mt ] = $R[ $catalogoId ];
							$mt++;
						}
						$multipleList = '';
						if ( isset( $multiples ) ) {
							$multipleList = implode( '-', $multiples );
						}
						$status[ 'multiple-' . $i ] = $multipleList;
						unset( $multiples );
					}
				}
			}
		}
	} else {
		global $palabrasReservadas;
		$strList = 'select ' . $campoId;
		$filas = 0;
		$opcionesCampos = array();
		$str = 'select COLUMN_COMMENT as comentarios, if ( COLUMN_KEY = "PRI", "S", "N" ) as esLlave, DATA_TYPE as tipoDatos, COLUMN_NAME as columna, if( IS_NULLABLE="NO", "S", "N" ) as obligatoria, COLUMN_DEFAULT as valorDefault, CHARACTER_MAXIMUM_LENGTH as tamano from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA="' . $con->getDataBaseName() . '" and TABLE_NAME="' . $tabla . '" order by ORDINAL_POSITION asc';
		$res = $con->Consulta( $str );
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			echo $str .'<hr><br>';
		}
		while( $R = $con->Resultados( $res ) ) {
			if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
				echo 'COLUMNA = ' . $R[ 'columna' ] . ' | COMENTARIO = ' . $R[ 'comentarios' ] . ' | ' . ' OBLIGATORIA = ' . $R[ 'obligatoria' ] . ' | LLAVE = ' . $R[ 'esLlave' ];
			}
			if ( $R[ 'esLlave' ] == 'N' && $R[ 'obligatoria' ] == 'S' ) {
				if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
					echo '<br>Entro';
				}
				if ( in_array( $R[ 'comentarios' ], $palabrasReservadas ) || $R[ 'comentarios' ] == '' ) {
					if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
						echo '<br>Entro a palabras reservadas o sin comentarios';
					}
					$strList .= ', ' . $R[ 'columna' ];
					$columnas[ $filas ] = $R[ 'columna' ];
					$filas++;
				} else if ( preg_match('/\bopciones\b/', $R[ 'comentarios' ] ) ) {
					if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
						echo '<br>Entro a opciones';
					}
					$tablaCatalogo = explode( '=', $R[ 'comentarios' ] )[1];
					$titulo = preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tablaCatalogo );
					$opcionesCampos[ $R[ 'columna' ] ] = array( 'tabla' => $tablaCatalogo, 'titulo' => ucfirst( $titulo ), 'campo' => $R[ 'columna' ] );
					$strList .= ', ' . $R[ 'columna' ];
					$columnas[ $filas ] = $R[ 'columna' ];
					$filas++;
				}
			}
			if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
				echo '<hr>';
			}
		}
		if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
			var_dump( $tipoOpciones );
			var_dump( $opcionesCampos );
			var_dump( $columnas );
		}
		$ordenamiento = ( $idiomaCampo != '' ) ? $idiomaCampo . ' asc, ' : '';
		$ordenamiento = ( $tabla == 'pantallas' ) ? 'servicioId asc, posicion asc, ' : '';
		$ordenamiento .= ( $posicionCampo != '' ) ? $posicionCampo . ' asc, ' : '';
		$strList .= ' from ' . $tabla . ' order by ' . $ordenamiento . $campoId . ' desc';
		$res = $con->Consulta( $strList );
		$html = '';
		$i = 0;
		while( $R = $con->Resultados( $res ) ) {
			$ligaTiene = false;
			$fila = '<tr id="' . $tabla . '-' . $R[ $campoId ] . '" campoId="' . $R[ $campoId ] . '">';
			if ( $tabla == 'videos' ) {
				$eliminado = 'borraVideo( ' . $R[ $campoId ] . ', \'' . $R[ 'archivo' ] . '\' )';
			} else {
				$eliminado = ( $tipoId == 'int' ) ? 'eliminado( ' . $R[ $campoId ] . ' )' : 'eliminado( \'' . $R[ $campoId ] . '\' )';
			}
			$elegido = ( $tipoId == 'int' ) ? 'elegido( ' . $R[ $campoId ] . ' )' : 'elegido( \'' . $R[ $campoId ] . '\' )';
			$catPadre = '';
			$iconoDetalle = ( $tabla == 'pedidos' ) ? '<i class="fa fa-search" onclick="verDetalle( ' . $R[ $campoId ] . ' )" title="Ver detalle"></i>' : '';
			$iconoGaleria = ( $tabla == 'productos' ) ? '<i class="fa fa-upload" onclick="addGaleria( ' . $R[ $campoId ] . ' )" title="Subir Archivos"></i>' : '';
			$fila .=
				'<td class="tcenter iconos">
					<i class="fa fa-trash" onclick="' . $eliminado . '" title="Eliminar elemento"></i>
					<i class="fa fa-edit" onclick="' . $elegido . '" title="Editar Elemento"></i>
					' . $iconoDetalle . '
					' . $iconoGaleria . '
				</td>
				<td class="tcenter">' . $R[ $campoId ] . '</td>' . $catPadre;
			if ( isset( $columnas ) ) {
				for ( $indi = 0; $indi < sizeof( $columnas ); $indi++ ) {
					if ( $posicionCampo != $columnas[ $indi ] ) {
						if ( $tabla == 'videos' && $columnas[ $indi ] == 'archivo' ) {
							$fila .= '<td><script src="https://amrak.cdn.spotlightr.com/assets/vooplayer.js"></script><a class="fancyboxIframe vooplayer" href="https://amrak.cdn.spotlightr.com/publish/' . $R[ $columnas[ $indi ] ] . '" data-playerId="' . $R[ $columnas[ $indi ] ] . '" data-fancybox-type="iframe">Play the video</a></td>';
						} else {
							if ( isset( $tipoHtml ) ) {
								$informacion = ( in_array( $columnas[ $indi ], $tipoHtml ) ) ? '<div class="contentHTML">' . mb_substr( strip_tags( html_entity_decode( $R[ $columnas[ $indi ] ] ) ), 0, 200, 'utf-8' ) . '</div>' : $R[ $columnas[ $indi ] ];
								if ( !in_array( $columnas[ $indi ], $tipoHtml ) && ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) || preg_match('/\jpeg\b/', $informacion ) || preg_match('/\JPG\b/', $informacion ) ) ) {
									$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
									$fila .= '<td class="tcenter">' . $informacion . '</td>';
								} else {
									if ( isset( $tipoPass ) && $informacion != '' ) {
										if ( in_array( $columnas[ $indi ], $tipoPass ) && ( $tabla == 'coaches' || $tabla == 'users' ) ) {
											$informacion = '<a onclick="enviaPass( \'' . $R[ 'email' ] . '\', \'' . $tabla . '\' )">Re-send Password</a>';
										} else {
											$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
										}
									}
									if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
										$ligaTiene = true;
										$informacion = '<a href="' . $informacion . '" target="_blank" class="ligaAdmin">' . $informacion . '</a>';
									}
									if ( isset( $tipoIcono ) && in_array( $columnas[ $indi ], $tipoIcono ) ) {
										$informacion = '<i class="' . $informacion . '"></i>';
									}
									if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
										$datos = $opcionesCampos[ $columnas[ $indi ] ];
										if ( $informacion > 0 ) {
											$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>Campo Opciones Actual';
												var_dump( $datos );
												echo $campoNombre . '<br>';
											}
											if ( $campoNombre != false ) {
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo 'Si trae campo nombre<br>';
												}
												$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
												$resOpcion = $con->Consulta( $strOpcion );
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo $strOpcion;
												}
												if ( $con->Cuantos( $resOpcion ) > 0 ) {
													$OP = $con->Resultados( $resOpcion );
													$informacion = $OP[ $campoNombre ];
												}
											}
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>';
											}
										} else {
											$informacion = 'Unassigned';
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>No se asigno valor a campo opciones<hr>';
											}
										}
									}
									$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . mb_substr( $informacion, 0, 200, 'utf-8' ) . ' ...</div>' ) : $informacion;
									if ( $columnas[ $indi ] == 'precio' ) {
										$informacion = '$ ' . $informacion . ' USD';
									}
									$fila .= '<td>' . $informacion . '</td>';
								}
							} else {
								$informacion = $R[ $columnas[ $indi ] ];
								if ( preg_match('/\bjpg\b/', $informacion ) || preg_match('/\bgif\b/', $informacion ) || preg_match('/\bpng\b/', $informacion ) ) {
									$informacion = '<div class="capaimg"><img src="../images/' . $_REQUEST[ 'folder' ] . '/' . $informacion . '" style="height: 40px;"></div>';
									$fila .= '<td class="tcenter">' . $informacion . '</td>';
								} else {
									if ( isset( $tipoPass ) && $informacion != '' ) {
										if ( in_array( $columnas[ $indi ], $tipoPass ) && ( $tabla == 'coaches' || $tabla == 'users' || $tabla == 'clients' ) ) {
											$informacion = '<a onclick="enviaPass( \'' . $R[ 'email' ] . '\', \'' . $tabla . '\' )">Re-send Password</a>';
										} else {
											$informacion = ( in_array( $columnas[ $indi ], $tipoPass ) ) ? desencripta( $informacion ) : $informacion;
										}
									}
									if ( isset( $tipoLiga ) && in_array( $columnas[ $indi ], $tipoLiga ) ) {
										$ligaTiene = true;
										$informacion = '<a href="' . $informacion . '" target="_blank" class="ligaAdmin">' . $informacion . '</a>';
									}
									if ( isset( $tipoIcono ) && in_array( $columnas[ $indi ], $tipoIcono ) ) {
										$informacion = '<i class="' . $informacion . '"></i>';
									}
									if ( isset( $tipoOpciones ) && in_array( $columnas[ $indi ], $tipoOpciones ) ) {
										$datos = $opcionesCampos[ $columnas[ $indi ] ];
										if ( $informacion > 0 ) {
											$campoNombre = getCampoNombre( $datos[ 'tabla' ], $con );
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>Campo Opciones Actual';
												var_dump( $datos );
												echo $campoNombre . '<br>';
											}
											if ( $campoNombre != false ) {
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo 'Si trae campo nombre<br>';
												}
												$strOpcion = 'select ' . $campoNombre . ' from ' . $datos[ 'tabla' ] . ' where ' . $datos[ 'campo' ] . '=' . $informacion;
												$resOpcion = $con->Consulta( $strOpcion );
												if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
													echo $strOpcion;
												}
												if ( $con->Cuantos( $resOpcion ) > 0 ) {
													$OP = $con->Resultados( $resOpcion );
													$informacion = $OP[ $campoNombre ];
												}
											}
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>';
											}
										} else {
											$informacion = 'Unassigned';
											if ( isset( $_REQUEST[ 'pruebas' ] ) ) {
												echo '<hr>No se asigno valor a campo opciones<hr>';
											}
										}
									}
									$informacion = ( strlen( $informacion ) > 200 && !$ligaTiene ) ? ( '<div style="width: 250px;">' . mb_substr( $informacion, 0, 200, 'utf-8' ) . ' ...</div>' ) : $informacion;
									if ( $columnas[ $indi ] == 'precio' ) {
										$informacion = '$ ' . $informacion . ' USD';
									}
									$fila .= '<td>' . $informacion . '</td>';
								}
							}
						}
					}
				}
			}
			if ( $posicionCampo != '' ) {
				$fila .= '<td>' . ( $i + 1 ) . '</td>';
			}
			$fila .= '</tr>';
			$i++;
			$html .= $fila;
		}
		if( $i == 0 ) {
			$html = '<td colspan="' . $filas . '">No hay datos agregados actualmente</td>';
		}
		$status = array( 'status' => 'Success', 'html' => $html, 'posicionCampo' => $posicionCampo );
	}
	$con->CierraConexion();
	echo json_encode( $status, JSON_UNESCAPED_UNICODE );
	exit();
?>