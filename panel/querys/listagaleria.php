<?php
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$str = 'select * from productoarchivo where productoId=' . $_REQUEST[ 'id' ];
	$res = $con->Consulta( $str );
	while( $R = $con->Resultados( $res ) ) {
		$html .=
		'<div class="col-lg-12" id="archivo-' . $R[ 'archivoId' ] . '">
			<i class="fa fa-close iconoBorra" onclick="eliminaGaleria( ' . $R[ 'archivoId' ] . ', \'' . $_REQUEST[ 'tabla' ] . '\' )"></i>
			<a href="../images/productos/' . $R[ 'archivo' ] . '" target="_blank">' . $R[ 'archivo' ] . '</a>
		</div>';
	}
	if( $html == '' ) {
		$html = '<h3 style="text-align: center;">No hay Imagenes agregadas</h3>';
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>