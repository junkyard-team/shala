<?php
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$html = '';
	$tabla = $_REQUEST[ 'tabla' ];
	$albumId = getCampoId( $_REQUEST[ 'album' ], $con );
	$campoId = getCampoId( $tabla, $con );
	$order = ( getCampoPosicion( $tabla, $con ) ) ? getCampoPosicion( $tabla, $con ) : getCampoId( $tabla, $con );
	$i = 1;
	$html = '';
	$strQuery = 'select * from ' . $tabla . ' where ' . $albumId . '=' . $_REQUEST[ 'id' ] . ' order by ' . $order . ' asc';
	$query = $con->Consulta( $strQuery );
	while( $R = $con->Resultados( $query ) ) {
		$html .=
		'<tr id="' . $tabla . '-' . $R[ $campoId ] . '" campoId="' . $R[ $campoId ] . '">
			<td class="tcenter iconos">
				<i class="fa fa-trash" onclick="eliminaGaleria( ' . $R[ $campoId ] . ' )"></i>
			</td>
			<td class="tcenter">' . $R[ $campoId ] . '</td>
			<td><div class="capaimg"><img src="../images/' . $tabla . '/' . $R[ 'imagen' ] . '" style="height: 40px;"></div></td>
			<td>' . $i . '</td>
		</tr>';
		$i++;
	}
	$status = array( 'status' => 'Success', 'html' => $html );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>