<?php
	session_start();
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$usuarioData = array();
	$str = 'select * from admin where usuario="' . $_REQUEST[ 'usuario' ] . '" and pass="' . encripta( $_REQUEST[ 'pass' ] ) . '"';
	$res = $con->Consulta($str);
	while( $R = $con->Resultados( $res ) ) {
		$usuarioData = array(
			'id' => $R[ 'adminId' ],
			'nombre' => $R[ 'nombre' ]
		);
		$accesos = array();
		$res1 = $con->Consulta( 'select a.nombre from accessuseradmin ac inner join accessadmin a on(ac.accessId=a.accessId) where ac.tipoId=' . $R[ 'tipoId' ] );
		while( $T = $con->Resultados( $res1 ) ) {
			$accesos[] = $T[ 'nombre' ];
		}
		$usuarioData[ 'accesos' ] = $accesos;
		$accesosList = implode( ',', $accesos );
		$_SESSION[ 'accesos' ] = $accesosList;
		$res1 = $con->Consulta( 'select * from shala' );
		while( $T = $con->Resultados( $res1 ) ) {
			$usuarioData[ 'api' ] = array(
				'vooKey' => $T[ 'spotlightrApiKey' ],
				'stripePublic' => $T[ 'stripePublicKey' ],
				'stripePrivate' => $T[ 'stripePrivateKey' ]
			);
		}
	}
	if( empty( $usuarioData ) ) {
		$status = array( 'status' => 'Error' );
	} else {
		$status = array( 'status' => 'Success', 'usuario' => $usuarioData );
	}
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>