<?php
	include 'conexion.php';
	include 'functions.php';
	$con = new Conexion();
	$con->AbreConexion();
	$tabla = $_REQUEST[ 'tabla' ];
	$campo = getCampoPosicion( $tabla, $con );
	$campoId = getCampoId( $tabla, $con );
	$posicionData = json_decode( $_REQUEST[ 'posiciones' ] );
	$bien = 0;
	$mal = 0;
	foreach ( $posicionData as $posicion ) {
		$str = 'update ' . $tabla . ' set ' . $campo . '=' . $posicion->posicion . ' where ' . $campoId . '=' . $posicion->id;
		$res = $con->Consulta( $str );
		if ( $res ) {
			$bien++;
		} else {
			$mal++;
		}
	}
	$status = array( 'status' => 'Success', 'bien' => $bien, 'mal' => $mal );
	$con->CierraConexion();
	echo json_encode( $status );
	exit();
?>