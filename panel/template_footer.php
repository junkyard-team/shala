		<div class="cargando">
			<i class="fa fa-circle-o-notch fa-spin"></i><br>
			<h3>PROCESANDO</h3>
			<div style="display: none; width: 50%; height: 100px; margin: 0 auto;" id="porcentajeDiv">
				<div class="progress mt10 mbn">
					<div role="progressbar" id="porcentaje" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%" class="progress-bar progress-bar-info progress-bar-striped active">
						<span class="sr-only" id="porcentajeValor" style="display: block; width: 100%; height: 20px; text-align: center; color: #000; position: relative;">0% Completo</span>
					</div>
				</div>
			</div>
		</div>
		<div class="capanegra"></div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6 text-right"><span class="footer-legal">&copy; <?php echo date( 'Y' ); ?>. Derechos Reservados. Panel de Administación, Junkyard</span></div>
			</div>
		</footer>