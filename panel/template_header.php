<?php
	session_start();
	$http = 'http';
	if ( isset( $_SERVER[ 'HTTPS' ] ) ) {
		$http = 'https';
	}
	$url = $http . '://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ];
	$base = $http . '://' . $_SERVER[ 'HTTP_HOST' ] . '/panel/';
    $sideBarType = 2; // 1 = Extendido | 2 = Minificado
    $classBody = ( $sideBarType == 2 ) ? 'sb-l-o' : 'sb-l-m';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Panel de Administración</title>
    <base href="<?php echo $base; ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <style>
        :root {
            --primario: #49bb04;
            --secundario: #3ea003;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css?v=5">