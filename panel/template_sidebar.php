			<?php $accesos = explode( ',', $_SESSION[ 'accesos' ] ); ?>
			<aside id="sidebar_left" class="nano nano-light affix">
				<div class="navbar-branding"><a href="principal" class="navbar-brand"><img src="assets/img/logos/logo-blanco.png" class="miLogo"></a><span id="toggle_sidemenu_l" class="ad ad-lines"></span></div>
				<div class="sidebar-left-content nano-content">
					<header class="sidebar-header">
						<div class="sidebar-widget author-widget">
							<div class="media">
								<div class="media-body">
									<div class="media-links"></div>
									<div class="media-author" id="usuarion"></div><a href="#" onclick="logout()">Logout</a>
								</div>
							</div>
						</div>
					</header>
					<ul class="nav sidebar-menu">
						<li class="sidebar-label pt30">Menu</li>
						<?php if ( in_array( 'webContent', $accesos ) ): ?>
						<?php
							$subMenus = array();
							$menuStatus = ( isset( $_REQUEST[ 'tabla' ] ) && in_array( $_REQUEST[ 'tabla' ], $subMenus ) ) ? ' menu-open' : '';
						?>
						<li id="contenidoWeb"><a href="#" class="accordion-toggle<?php echo $menuStatus; ?>"><span class="fa fa-globe"></span><span class="sidebar-title">Web Content</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								
							</ul>
						</li>
						<?php endif; ?>
						<?php if ( in_array( 'platformsManage', $accesos ) ): ?>
						<?php
							$subMenus = array( 'platforms', 'clients', 'colorsclient', 'questionnaire' );
							$menuStatus = ( isset( $_REQUEST[ 'tabla' ] ) && in_array( $_REQUEST[ 'tabla' ], $subMenus ) ) ? ' menu-open' : '';
						?>
						<li id="manejoProductos"><a href="#" class="accordion-toggle<?php echo $menuStatus; ?>"><span class="fa fa-building"></span><span class="sidebar-title">Platforms</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="tabla/platforms">Platforms</a></li>
								<li><a href="tabla/clients">Clients</a></li>
								<li><a href="tabla/colorsclient">Clients Colors</a></li>
								<li><a href="tabla/questionnaire">Clients Questionnaire</a></li>
								
							</ul>
						</li>
						<?php endif; ?>
						<?php if ( in_array( 'platformUsers', $accesos ) ): ?>
						<?php
							$subMenus = array( 'users', 'coaches' );
							$menuStatus = ( isset( $_REQUEST[ 'tabla' ] ) && in_array( $_REQUEST[ 'tabla' ], $subMenus ) ) ? ' menu-open' : '';
						?>
						<li id="manejoServicios"><a href="#" class="accordion-toggle<?php echo $menuStatus; ?>"><span class="fa fa-users"></span><span class="sidebar-title">Users</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="tabla/coaches">Guides</a></li>
								<li><a href="tabla/categories">Guide Categories</a></li>
								<li><a href="tabla/levels">Contents Levels</a></li>
								<li><a href="tabla/users">Members</a></li>
								<li><a href="tabla/answers">Members Answers</a></li>
							</ul>
						</li>
						<?php endif; ?>
						<?php if ( in_array( 'generalSetup', $accesos ) ): ?>
						<?php
							$subMenus = array( 'shala', 'colorcatalog', 'servicescatalog' );
							$menuStatus = ( isset( $_REQUEST[ 'tabla' ] ) && in_array( $_REQUEST[ 'tabla' ], $subMenus ) ) ? ' menu-open' : '';
						?>
						<li id="manejoConfiguracion"><a href="#" class="accordion-toggle<?php echo $menuStatus; ?>"><span class="fa fa-gear"></span><span class="sidebar-title">Setup</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="edita/shala">Shala Info</a></li>
								<li><a href="tabla/servicescatalog">Services</a></li>
								<li><a href="tabla/colorcatalog">Colors</a></li>
							</ul>
						</li>
						<?php endif; ?>
						<?php if ( in_array( 'userManage', $accesos ) ): ?>
						<?php
							$subMenus = array( 'admin', 'tipoadmin' );
							$menuStatus = ( isset( $_REQUEST[ 'tabla' ] ) && in_array( $_REQUEST[ 'tabla' ], $subMenus ) ) ? ' menu-open' : '';
						?>
						<li id="manejoUsuarios"><a href="#" class="accordion-toggle<?php echo $menuStatus; ?>"><span class="fa fa-user"></span><span class="sidebar-title">Admin</span><span class="caret"></span></a>
							<ul class="nav sub-nav">
								<li><a href="tabla/admin">Admin Users</a></li>
								<li><a href="tabla/tipoadmin">Admin Type</a></li>
							</ul>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</aside>