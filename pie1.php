		</main>
		<footer class="tCenter">
			<div class="principal">
				<?php if ( $nombreArchivo != 'contra.php' && $nombreArchivo != 'confirma.php' ) : ?>
				<ul class="menu">
					<li><a href="#">FAQ<span></span></a></li>
					<li><a href="blog">BLOG<span></span></a></li>
					<li><a href="contact">CONTACT<span></span></a></li>
					<li><a href="guide-sign-in"><?php echo $signUpCoachText; ?><span></span></a></li>
				</ul>
				<?php endif; ?>
				<div class="copyRight">
					&copy; <?php echo date( 'Y' ); ?> <?php echo $T[ 'plataforma' ]; ?>. All Rights Reserved<br>Powered by Shala<br>
					<a href="#">Terms</a>
					<a href="#">Privacy</a>
				</div>
			</div>
		</footer>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
		<script src="js/jquery.bxslider.js"></script>
		<script src="js/jquery.countdown.js"></script>
		<script src="js/funciones.js?v=7"></script>
		<script src="js/script.js?v=<?php echo time(); ?>"></script>
	</body>
</html>
<?php $con->CierraConexion(); ?>