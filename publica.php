<?php
	include 'cabecera.php';
	$R = obtenPostData( $_REQUEST[ 'url' ], $clientId, $con );
?>
<div class="pageHead">
	<h2><?php echo $R[ 'title' ]; ?></h2>
</div>
<div class="seccion">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<?php
					if ( !is_null( $R[ 'video' ] ) && $R[ 'video' ] != '' ) {
						echo
						'<div class="video-container iq-main-slider">
							<iframe allowtransparency="true" class="videoFramePost" allowfullscreen="true" src="https://amrak.cdn.spotlightr.com/publish/' . $R[ 'video' ] . '?fallback=true" frameborder="0" scrolling="no"> </iframe>
						</div>';
					} else {
						echo '<img src="images/posts/' . $R[ 'image' ] . '" class="fullImg">';
					}
				?>
				<?php echo $R[ 'content' ]; ?>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>