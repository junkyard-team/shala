<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1><?php echo $registroText; ?> Questionnaire</h1>
</div>
<div class="seccion">
	<div class="principal">
		<form method="post" id="preguntasForm">
		<?php
			$i = 1;
			$query = $con->Consulta( 'select * from questionnaire where clientId=' . $clientId . ' order by position asc' );
			while( $R = $con->Resultados( $query ) ) {
				echo
				'<div class="pregunta fila">
					<div class="setenta cPad">
						<label for="pregunta-' . $R[ 'questionId' ] . '">' . $i . ' - ' . $R[ 'question' ] . '</label>
					</div>
					<div class="treinta cPad">
						<select id="pregunta-' . $R[ 'questionId' ] . '" preguntaId="' . $R[ 'questionId' ] . '" required>
							<option value="">Pick an option</option>
							<option value="' . $R[ 'answerOne' ] . '">' . $R[ 'answerOne' ] . '</option>
							<option value="' . $R[ 'answerTwo' ] . '">' . $R[ 'answerTwo' ] . '</option>
							<option value="' . $R[ 'answerThree' ] . '">' . $R[ 'answerThree' ] . '</option>
							<option value="' . $R[ 'answerFour' ] . '">' . $R[ 'answerFour' ] . '</option>
							<option value="' . $R[ 'answerFive' ] . '">' . $R[ 'answerFive' ] . '</option>
						</select>
					</div>
				</div>';
				$i++;
			}
		?>
		<button type="submit" class="boton">Submit</button>
		</form>
	</div>
</div>
<?php include 'pie.php'; ?>