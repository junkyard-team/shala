<?php include 'cabecera.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="pageHead">
	<h1><?php echo $registroText; ?></h1>
</div>
<div class="seccion signUp">
	<div class="principal">
		<div class="fila">
			<div class="cincuenta cuentaCentrada">
				<h2 class="abreForm" form="studentForm"><?php echo $miembroText; ?></h2>
				<form method="post" id="studentForm" class="cPad" tipo="1" enctype="multipart/form-data">
					<input type="hidden" id="userId" campo="userId" value="0">
					<label for="nombreUser">First Name</label>
					<input type="text" id="nombreUser" campo="nombre" class="obligatorio" required>
					<label for="nombreUser">Last Name</label>
					<input type="text" id="apellidoUser" campo="apellido" class="obligatorio" required>
					<label for="emailUser">Email</label>
					<input type="email" id="emailUser" campo="email" class="email obligatorio" required>
					<label for="passUser">Password</label>
					<input type="password" id="passUser" campo="pass" class="obligatorio" required>
					<button type="submit" class="boton">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php include 'pie.php'; ?>