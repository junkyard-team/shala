<?php
	$idiomaId = array( 'ES' => 2, 'EN' => 1, 'FR' => 3 );
	$idiomaActual = array( 'ES' => 'Español', 'EN' => 'English', 'FR' => 'Français' );
	if ( !isset( $_REQUEST[ 'lang' ] ) ) {
		if ( !isset( $_SESSION[ 'lang' ] ) ) {
			$lang = 'EN';
			$_SESSION[ 'lang' ] = $lang;
		} else {
			$lang = $_SESSION[ 'lang' ];
		}
	} else {
		$lang = $_REQUEST[ 'lang' ];
		$_SESSION[ 'lang' ] = $lang;
    }
    $_SESSION[ 'lang' ] = 'EN';
    $_SESSION[ 'langId' ] = $idiomaId[ $_SESSION[ 'lang' ] ];
    $menuText = 'Menu';
	$homeText = 'Home';
	$exploraText = 'Classes';
	$instructores = 'Teachers';
	$procesando = 'Processing';
	$registroText = 'Sign Up';
	$miembroText = 'Student';
	$coachText = 'Teacher';
	$tituloHeader = 'Faith. Wisdom. Goodness';
	$subTituloHeader = 'Prepare our children for the future with passion based learning';
	$tituloCategoria = 'FALL IN LOVE WITH LEARNING';
	$subTituloCategoria = 'Find the live classes, educational videos and exercises that nurture children beyond the curriculums of regular school';
	$tituloInstructores = 'MEET OUR TEACHERS';
	$subTituloInstructores = 'Get started on a journey that inspires creativity and connection through classes offered by world class teachers both online and in-person';
	$pruebaDias = 'TRY IT FREE FOR 14 DAYS';
	$iniciaPrueba = 'Get started with two free weeks of unlimited purpose approach, passion based learning, self efficacy, academics, environmental awareness, social responsibility and spiritual balance classes';
	$tituloBannerUno = 'HIGHLY ENGAGING CONTENT';
	$subtituloBannerUno = 'We understand the needs of children and designed our prorgams accordingly in byte size videos, personalized live sessions, engaging group workshops and fun exercises.';
    $tituloBannerDos = 'FOR EVERY SPACE, AT ANY PACE';
    $subtituloBannerDos = 'From the comfort of your living room to outside spaces, we put the best classes at your fingertips. No WiFi? Download videos offline for a personalized journey that moves with you.';
    $tituloClasses = 'CLASSES';
    $subTituloClasses = 'FIND A CLASS TO GET STARTED';
    if ( $_SESSION[ 'lang' ] == 'ES' ) {
		$menuText = 'Menú';
		$homeText = 'Inicio';
		$exploraText = 'Explorar';
		$instructores = 'Guides';
		$procesando = 'Procesando';
		$registroText = 'Registrarse';
		$tituloCategoria = 'ENCUENTRA LO QUE TE MUEVE';
		$subTituloCategoria = 'Ya sea que sea un principiante o quiera intensificar su rutina, obtenga la experiencia de estudio completa en casa con miles de clases para el cuerpo, la mente y el espíritu.';
		$tituloInstructores = 'CONOCE A NUESTROS GUIDES';
		$subTituloInstructores = 'Desafía tu fuerza. Estire su cuerpo. Respira fácil. Nuestro equipo de instructores de clase mundial le permitirá crecer y alcanzar todos sus objetivos de fitness y bienestar.';
		$tituloBannerUno = 'Apto para tu estilo de vida';
		$subtituloBannerUno = 'Despiértese con una meditación al amanecer, sude con HIIT a la hora del almuerzo o relájese con un flujo vespertino. Encontrarás movimiento para cada estado de ánimo con clases ordenadas por tiempo, estilo y nivel de habilidad.';
		$pruebaDias = 'PRUÉBALO GRATIS POR 14 DÍAS';
		$tituloBannerDos = 'PARA CADA ESPACIO, A CUALQUIER RITMO';
    	$subtituloBannerDos = 'Desde la comodidad de su sala de estar hasta una habitación de hotel en todo el mundo, ponemos las mejores clases a su alcance. ¿No wifi? Descarga videos sin conexión para una práctica que se mueve contigo.';
		$iniciaPrueba = 'Comienza con dos semanas gratuitas de clases ilimitadas de yoga, fitness y meditación. Cancela en cualquier momento.';
	} else if ( $_SESSION[ 'lang' ] == 'FR' ) {
		$menuText = 'Menu';
		$homeText = 'Accueil';
		$exploraText = 'Explorer';
		$instructores = 'Guides';
		$procesando = 'En traitement';
		$registroText = 'Se Connecter';
		$tituloCategoria = 'TROUVEZ CE QUI VOUS MOUVENT';
		$subTituloCategoria = 'Que vous soyez un débutant complet ou que vous souhaitiez intensifier votre routine, vivez l\'expérience studio complète à la maison avec des milliers de cours pour le corps, l\'esprit et l\'esprit.';
		$tituloInstructores = 'RENCONTREZ NOS GUIDES';
		$subTituloInstructores = 'Mettez votre force à l\'épreuve. Étirez votre corps. Respirez doucement. Notre équipe d\'instructeurs de classe mondiale vous permettra de grandir et d\'atteindre tous vos objectifs de fitness et de bien-être.';
		$tituloBannerUno = 'ADAPTÉ À VOTRE STYLE DE VIE';
		$subtituloBannerUno = 'Réveillez-vous avec une méditation au lever du soleil, transpirez avec HIIT pour le déjeuner ou détendez-vous avec un flux du soir. Vous trouverez du mouvement pour chaque humeur avec des cours triés par temps, style et niveau de compétence.';
		$pruebaDias = 'ESSAYEZ-LE GRATUITEMENT PENDANT 14 JOURS';
		$tituloBannerDos = 'POUR CHAQUE ESPACE, À TOUT RYTHME';
    	$subtituloBannerDos = 'Du confort de votre salon à une chambre d\'hôtel à travers le monde, nous mettons les meilleurs cours à portée de main. Pas de wifi? Téléchargez des vidéos hors ligne pour une pratique qui bouge avec vous.';
		$iniciaPrueba = 'Commencez avec deux semaines gratuites de cours illimités de yoga, de fitness et de méditation. Annulez à tout moment.';
    }
?>
