<?php
	$idiomaId = array( 'ES' => 2, 'EN' => 1, 'FR' => 3 );
	$idiomaActual = array( 'ES' => 'Español', 'EN' => 'English', 'FR' => 'Français' );
	if ( !isset( $_REQUEST[ 'lang' ] ) ) {
		if ( !isset( $_SESSION[ 'lang' ] ) ) {
			$lang = 'EN';
			$_SESSION[ 'lang' ] = $lang;
		} else {
			$lang = $_SESSION[ 'lang' ];
		}
	} else {
		$lang = $_REQUEST[ 'lang' ];
		$_SESSION[ 'lang' ] = $lang;
    }
    $_SESSION[ 'lang' ] = 'EN';
    $_SESSION[ 'langId' ] = $idiomaId[ $_SESSION[ 'lang' ] ];
    $queryGral = $con->Consulta( 'select * from generals where clientId=' . $clientId );
	if ( $con->Cuantos( $queryGral ) > 0 ) {
		$GRAL = $con->Resultados( $queryGral );
		$homeText = $GRAL[ 'homeMenuText' ];
		$liveMenuText = $GRAL[ 'liveMenuText' ];
		$liveText = $GRAL[ 'liveSessionText' ];
		$exploraText = $GRAL[ 'exploreMenuText' ];
		$instructores = $GRAL[ 'coachesMenuText' ];
		$aboutText = $GRAL[ 'aboutMenuText' ];
		$procesando = $GRAL[ 'processingText' ];
		$registroText = $GRAL[ 'signUpText' ];
		$miembroText = $GRAL[ 'memberText' ];
		$coachText = $GRAL[ 'coachText' ];
		$tituloHeader = $GRAL[ 'headerTitle' ];
		$blogText = $GRAL[ 'blogText' ];
		$subTituloHeader = $GRAL[ 'subtitleHeader' ];
		$signUpCoachText = $GRAL[ 'signUpCoachText' ];
		$botonTextHeader = trim( $GRAL[ 'headerButtonText' ] );
		$botonUrlHeader = trim( $GRAL[ 'headerButtonUrl' ] );
		$botonTextColor = $GRAL[ 'headerButtonTextColor' ];
		$botonBackgroundColor = $GRAL[ 'headerButtonBackgroundColor' ];
		$botonRounded = ( $GRAL[ 'headerButtonRounded' ] == 'S' ) ? '15px' : '0px';
		$buttonSize = ( !is_null( $GRAL[ 'headerButtonFontSize' ] ) && $GRAL[ 'headerButtonFontSize' ] != '' ) ? $GRAL[ 'headerButtonFontSize' ] : '20px';
		$showButton = ( $GRAL[ 'headerButtonShow' ] == 'S' ) ? true : false;
		$pruebaDias = $GRAL[ 'trialDaysText' ];
		$iniciaPrueba = $GRAL[ 'trialDescriptionText' ];
		$tituloClasses = $GRAL[ 'titleClasses' ];
    	$subTituloClasses = $GRAL[ 'subtitleClasses' ];
    	$tituloCategoria = $GRAL[ 'titleCategories' ];
		$subTituloCategoria = $GRAL[ 'subtitleCategories' ];
		$tituloInstructores = $GRAL[ 'titleCoaches' ];
		$subTituloInstructores = $GRAL[ 'subtitleCoaches' ];
		$muestraCoach = ( $GRAL[ 'hideCoachContent' ] == 'N' ) ? 'S' : 'N';
		$videoUrl = ( !is_null( $GRAL[ 'videoHeader' ] ) && $GRAL[ 'videoHeader' ] != '' ) ? 'generals/' . $GRAL[ 'videoHeader' ] : null;
		$videoMobileUrl = ( !is_null( $GRAL[ 'videoMobileHeader' ] ) && $GRAL[ 'videoMobileHeader' ] != '' ) ? 'generals/' . $GRAL[ 'videoMobileHeader' ] : $videoUrl;
	} else {
		$textosGral = getDefaultText();
		$homeText = $textosGral[ 'homeMenuText' ];
		$liveMenuText = $textosGral[ 'liveMenuText' ];
		$liveText = $textosGral[ 'liveSessionText' ];
		$exploraText = $textosGral[ 'exploreMenuText' ];
		$instructores = $textosGral[ 'coachesMenuText' ];
		$aboutText = $textosGral[ 'aboutMenuText' ];
		$blogText = $textosGral[ 'blogText' ];
		$procesando = $textosGral[ 'processingText' ];
		$registroText = $textosGral[ 'signUpText' ];
		$miembroText = $textosGral[ 'memberText' ];
		$coachText = $textosGral[ 'coachText' ];
		$tituloHeader = $textosGral[ 'headerTitle' ];
		$subTituloHeader = $textosGral[ 'subtitleHeader' ];
		$signUpCoachText = $textosGral[ 'signUpCoachText' ];
		$buttonSize = '20px';
		$showButton = true;
		$botonTextHeader = null;
		$botonUrlHeader = null;
		$botonTextColor = '#000';
		$botonBackgroundColor = '#fff';
		$botonRounded = '0px';
		$pruebaDias = $textosGral[ 'trialDaysText' ];
		$iniciaPrueba = $textosGral[ 'trialDescriptionText' ];
		$tituloClasses = $textosGral[ 'titleClasses' ];
    	$subTituloClasses = $textosGral[ 'subtitleClasses' ];
    	$tituloCategoria = $textosGral[ 'titleCategories' ];
		$subTituloCategoria = $textosGral[ 'subtitleCategories' ];
		$tituloInstructores = $textosGral[ 'titleCoaches' ];
		$subTituloInstructores = $textosGral[ 'subtitleCoaches' ];
		$videoUrl = $textosGral[ 'videoHeader' ];
		$videoMobileUrl = $textosGral[ 'videoMobileHeader' ];
		$muestraCoach = 'S';
	}
?>
