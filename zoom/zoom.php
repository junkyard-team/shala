<?php
    //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkZqeVZyUTdLVEZ5dzFJUjkzR2x1UHciLCJleHAiOjE5MjQ5NjY4MDAsImlhdCI6MTYwOTMzNzE3NH0.b-9DQj4sJs1afK3tC9myutuVWT21R8IE2631nOXYxZI
    use \Firebase\JWT\JWT;
    include 'vendor/autoload.php';
    class Zoom_Api {
        private $zoom_api_key = '';
        private $zoom_api_secret = '';
        public function __construct( $public, $private ) {
            $this->zoom_api_key = $public;
            $this->zoom_api_secret = $private;
        }
        private function getToken() {
            $request_url = 'https://zoom.us/oauth/token?grant_type=client_credentials';
            $basicAuth = base64_encode( $this->zoom_api_key . ':' . $this->zoom_api_secret );
            $headers = array(
                'Authorization: Basic ' . $basicAuth,
                'content-type: application/json'
            );
            $data = array(
                'grant_type' => 'client_credentials',
                'client_id' => $this->zoom_api_key,
                'client_secret' => $this->zoom_api_secret
            );
            $ch = curl_init();
            $postFields = json_encode( $data );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
            curl_setopt( $ch, CURLOPT_URL, $request_url );
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            $response = curl_exec( $ch );
            $err = curl_error( $ch );
            curl_close( $ch );
            if( !$response ) {
                return $err;
            }
            return $response;
        }
        protected function sendRequest( $data ) {
           $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkZqeVZyUTdLVEZ5dzFJUjkzR2x1UHciLCJleHAiOjE5MjQ5NjY4MDAsImlhdCI6MTYwOTMzNzE3NH0.b-9DQj4sJs1afK3tC9myutuVWT21R8IE2631nOXYxZI';
           $request_url = 'https://api.zoom.us/v2/users/me/meetings';
           $headers = array(
               'authorization: Bearer ' . $token,
               'content-type: application/json'
           );
           $postFields = json_encode( $data );
           $ch = curl_init();
           curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
           curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
           curl_setopt( $ch, CURLOPT_URL, $request_url );
           curl_setopt( $ch, CURLOPT_POST, 1);
           curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );
           curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
           $response = curl_exec( $ch );
           $err = curl_error( $ch );
           curl_close( $ch );
           if( !$response ) {
               return $err;
           }
           return json_decode( $response );
		}
        public function deleteMeeting( $meetingId ) {
            try {
                $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkZqeVZyUTdLVEZ5dzFJUjkzR2x1UHciLCJleHAiOjE5MjQ5NjY4MDAsImlhdCI6MTYwOTMzNzE3NH0.b-9DQj4sJs1afK3tC9myutuVWT21R8IE2631nOXYxZI';
                $request_url = 'https://api.zoom.us/v2/meetings/' . $meetingId;
                $headers = array(
                    'Authorization: Bearer ' . $token,
                    'content-type: application/json'
                );
                $ch = curl_init();
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
                curl_setopt( $ch, CURLOPT_URL, $request_url );
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                $response = curl_exec( $ch );
                $httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
                $err = curl_error( $ch );
                curl_close( $ch );
                if ( $response ) {
                    if ( $httpcode == 204 ) {
                        return array( 'status' => true, 'response' => $datos );
                    } else {
                        return array( 'status' => false, 'response' => $datos );
                    }
                } else {
                    return array( 'status' => false, 'response' => $response );
                }
            } catch ( Exception $ex ) {
                return array( 'status' => false, 'response' => $ex );
            }
        }
        private function generateJWTKey() {
            $key = $this->zoom_api_key;
            $secret = $this->zoom_api_secret;
            $token = array(
                "iss" => $key,
                "exp" => time() + 3600
            );
            return JWT::encode( $token, $secret );
        }
		public function createAMeeting( $data = array() ) {
            $post_time = $data[ 'start_date' ];
            $start_time = gmdate( "Y-m-d\TH:i:s", strtotime( $post_time ) );
            $createAMeetingArray = array();
            if ( !empty( $data[ 'alternative_host_ids' ] ) ) {
                if ( count( $data[ 'alternative_host_ids' ] ) > 1 ) {
                    $alternative_host_ids = implode( ",", $data[ 'alternative_host_ids' ] );
                } else {
                    $alternative_host_ids = $data[ 'alternative_host_ids' ][0];
                }
            }
            $createAMeetingArray[ 'topic' ] = $data['meetingTopic'];
            $createAMeetingArray[ 'agenda' ] = ! empty( $data['agenda'] ) ? $data['agenda'] : "";
            $createAMeetingArray[ 'type' ] = ! empty( $data['type'] ) ? $data['type'] : 2; //Scheduled
            $createAMeetingArray[ 'start_time' ] = $start_time;
            $createAMeetingArray[ 'timezone' ] = $data['timezone'];
            $createAMeetingArray[ 'password' ] = ! empty( $data['password'] ) ? $data['password'] : "";
            $createAMeetingArray[ 'duration' ] = ! empty( $data['duration'] ) ? $data['duration'] : 60;
            $createAMeetingArray[ 'settings' ] = array(
                'join_before_host' => ! empty( $data['join_before_host'] ) ? true : false,
                'host_video' => ! empty( $data['option_host_video'] ) ? true : false,
                'participant_video' => ! empty( $data['option_participants_video'] ) ? true : false,
                'mute_upon_entry' => ! empty( $data['option_mute_participants'] ) ? true : false,
                'enforce_login' => ! empty( $data['option_enforce_login'] ) ? true : false,
                'auto_recording' => ! empty( $data['option_auto_recording'] ) ? $data['option_auto_recording'] : "none",
                'alternative_hosts' => isset( $alternative_host_ids ) ? $alternative_host_ids : ""
            );
            return $this->sendRequest( $createAMeetingArray );
        }
	}
?>